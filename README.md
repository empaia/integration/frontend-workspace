# EMPAIA Frontend Monorepo

This project is using Nx workspaces - [Nx Documentation](https://nx.dev/angular)

For readability this project uses [Conventional Commits](https://www.conventionalcommits.org/en/v1.0.0/). Please follow the commit message format!

## Requirements

1. Install Node.js (lts version, currently 20) and npm (lts version, currently 10) on your machine: https://nodejs.org/en/
   - if you using a Linux System: https://nodejs.org/en/download/package-manager/#debian-and-ubuntu-based-linux-distributions

Check your installation with `node -v` and `npm -v`

Run `npm install` in the project root to download and setup all dependencies needed for development. Dependencies are defined in the `package.json` file.

# Projects

This nx workspace contains several projects. A Project can either be an app or a library.

Apps are served at http://localhost:4200 in dev mode by default if not otherwise noted.

In general apps are started via `npm run nx serve <project name>`.

> If you install nx globally (`npm i -g @nrwl/cli`) you can remove `npm run` from all nx commands commands

## Apps

### Workbench Client

#### Location: [/apps/workbench-client](/apps/workbench-client)

#### Description:

An Angular application for pathologists to examine cases with the support of specialized Apps for various indications, stains and tissue types. This application is for research purposes only!

#### Start:

`npm run nx serve workbench-client`

Different configurations are available:

- to use the iron deployment run: `npm run nx serve workbench-client`
- to use your local platform deployment run: `npm run nx serve workbench-client -c docker-dev`

##### Releases:

[Docker Container](https://gitlab.com/empaia/integration/frontend-workspace/container_registry/2212914) on Gitlab

### Workbench Client 2.0

#### Location: [apps/workbench-client-v2](apps/workbench-client-v2)

#### Description:

An Angular application for pathologists to examine cases with whole slide images. The Workbench Client 2.0 is a modularized version of the Workbench Client 1.0. It includes dynamically loading vendor applications with an ui for diagnostic purposes.

#### Start:

`npm run nx serve workbench-client-v2`

Different configurations are available:

- to use the iron deployment run: `npm run nx serve workbench-client-v2`
- to use your local platform deployment run: `npm run nx serve workbench-client-v2 -c docker-dev`

##### Releases:

[Docker Container](https://gitlab.com/empaia/integration/frontend-workspace/container_registry/2760163) on Gitlab

### Workbench Client 3.0

#### Location: [apps/workbench-client-v3](apps/workbench-client-v3)

#### Description:

An Angular application for pathologists to examine cases with whole slide images. The Workbench Client 3.0 is an improved version of Workbench Client 2.0. It streamlined the use of examinations in form of binding only one app to one examination.

#### Start:

`npm run nx serve workbench-client-v3`

Different configurations are available:

- to use iron deployment run: `npm run nx serve workbench-client-v3`
- to use your local platform deployment run: `npm run nx serve workbench-client-v3 -c docker-dev`

### Sample App V2

#### Location: [/apps/sample-app-v2](/apps/sample-app-v2)

#### Description:

An Angular application to demonstrate how a vendor application could look like, how to use the `Vendor App Communication Interface` library and how to use the basic REST interfaces of the Workbench Service backend. It's optimized for Version 2 API calls to the Workbench Service.

[Docker Container](https://gitlab.com/empaia/integration/frontend-workspace/container_registry/3661372) on Gitlab

#### Start:

`npm run nx serve sample-app-v2`

### Sample App V3

#### Location: [/apps/sample-app-v3](/apps/sample-app-v3)

#### Description:

An Angular application to demonstrate how a vendor application could look like, how to use the `Vendor App Communication Interface` library and how to use the basic REST interfaces of the Workbench Service backend. It's optimized for Version 3 API calls to the Workbench Service.

[Docker Container](https://gitlab.com/empaia/integration/frontend-workspace/container_registry/3661475) on Gitlab

#### Start:

`npm run nx serve sample-app-v3`

### Generic App Ui V2

#### Location: [/apps/generic-app-ui-v2](/apps/generic-app-ui-v2)

#### Decription:

An Angular application for vendor apps which dosn't have an app ui by there own. It's optimized for Version 2 API calls to the Workbench Service.

[Docker Container](https://gitlab.com/empaia/integration/frontend-workspace/container_registry/3661376) on GitLab

#### Start:

`npm run nx serve generic-app-ui-v2`

### Generic App Ui V3

#### Location: [/apps/generic-app-ui-v3](/apps/generic-app-ui-v3)

#### Decription:

An Angular application for vendor apps which dosn't have an app ui by there own. It's optimized for Version 3 API calls to the Workbench Service.

[Docker Container](https://gitlab.com/empaia/integration/frontend-workspace/container_registry/3661377) on GitLab

#### Start:

`npm run nx serve generic-app-ui-v3`

### Organization Landing Page

#### Location: [/apps/organization-landing-page](/apps/organization-landing-page)

#### Description:

An Angular application to guide users to all provided applications, like the Portal, Workbench-Client and Data Manager.

#### Releases:

[Docker Container](https://gitlab.com/empaia/integration/frontend-workspace/container_registry/2362988) on Gitlab

#### Start:

`npm run nx serve organization-landing-page`

### Security Test

#### Location: [/apps/security-test](/apps/security-test)

#### Description:

An Angular application to test security measurements and attacks against the Workbench-Client 2.0 and certain settings of the Workbench Service.

#### Start:

`npm run nx serve security-test`

### Eye Tracking App

#### Location: [/apps/eye-tracking-app](/apps/eye-tracking-app)

#### Description:

An Angular application for research purposes to track the exe movement of a pathologist while investigating whole slide images. It only displays a WSIs without the possibility to draw or display annotations. The app uses the Slide Viewer Module to display WSIs.

#### Start:

`npm run nx serve eye-tracking-app`

## Libraries

### Slide Viewer Module

#### Location: [/libs/slide-viewer](/libs/slide-viewer)

##### Docs:

[Readme.md](/libs/slide-viewer/README.md) - [Changelog.md](/libs/slide-viewer/CHANGELOG.md)

##### Description:

An Angular library that uses OpenLayers to visualize whole slide images and create or display annotations.

##### Releases

https://www.npmjs.com/package/@empaia/slide-viewer

[npm package](https://gitlab.com/empaia/integration/frontend-workspace/-/packages?type=&orderBy=created_at&sort=desc&search[]=slide-viewer&search[]=) on Gitlab

#### Build:

`npm run nx build slide-viewer`

### Vendor App Communication Interface

#### Location: [/libs/vendor-app-communication-interface](/libs/vendor-app-communication-interface)

#### Docs:

[Readme.md](/libs/vendor-app-communication-interface/README.md)

#### Description:

A JavaScript library for an easy to use communication interface between the Workbench Client 2.0 and an embedded Vendor App, which is dynamically loaded by the Workbench Client 2.0 into a sandboxed iFrame.

##### Releases

https://www.npmjs.com/package/@empaia/vendor-app-communication-interface

[npm package](https://gitlab.com/empaia/integration/frontend-workspace/-/packages?type=&orderBy=created_at&sort=desc&search[]=vendor-app-communication-interface&search[]=) on Gitlab

#### Build:

`npm run nx build vendor-app-communication-interface`

### Vendor App Integration

#### Location: [/libs/vendor-app-integration](/libs/vendor-app-integration)

#### Docs:

[Readme.md](/libs/vendor-app-integration/README.md)

#### Description:

An Angular library to integrate Vendor Apps into the Workbench Client 2.0. It is used to transmit data like the scope id to the corresponding vendor app.

#### Build:

`npm run nx build vendor-app-integration`

##### Releases

[npm package](https://gitlab.com/empaia/integration/frontend-workspace/-/packages/?type=&orderBy=created_at&sort=desc&search[]=vendor-app-integration&search[]=) on Gitlab

## How to use

To build and start apps provided in this repository it is necessary to download the [Platform Deployment](https://gitlab.com/empaia/integration/platform-deployment). Before setting up the deployment and starting the backend services navigate to `platform-deployment/platform_tests/resources/` and open up the `sample_apps.json`. Search for the app entry that contains `sample_frontend_app_01` and change the `app_ui_url` to `http://host.docker.internal:4300`.

Afterwards follow the instructions given in the platform-deployment repo. [Readme](https://gitlab.com/empaia/integration/platform-deployment/-/blob/main/README.md): `System Setup` to `Import Test Data`.

### Sample App with Workbench Client 2.0

```console
npm run nx serve sample-app --host 0.0.0.0 --port 4300 --allowed-hosts host.docker.internal
```

The Sample App will run under port 4300.\
Make sure that the backend services of the platform deployment are up and running.

Open a browser at `http://localhost:10055`.

### Workbench Client 2.0 with Security Test

```console
npm run nx serve security-test --host 0.0.0.0 --port 4300 --allowed-hosts host.docker.internal
```

The Security Test App will run under port 4300.\
Optionally run a second Security Test App. You have to set a new app with an ui in the `sample_apps.json` under `platform_deployment/platform_tests/resources/` and set the app_ui_url to `http://host.docker.internal:4400`. After that open a second terminal and run:

```console
npm run nx serve security-test ---host 0.0.0.0 --port 4400 --allowed-hosts host.docker.internal
```

Make sure that the backend services of the platform deployment are up and running.

Open a browser window with `http://localhost:10055`.

## Create Playwright Test for an app

To create e2e test with playwright for an app. The plugins `@nx-extend/playwright` and `@nx-extend/e2e-runner`.\
The following steps create playwright test for an app:

- Create an playwright project for your app to test. Run

```console
nx g @nx-extend/playwright:init
```

- You will be ask for an name follow this naming convention `${app-name}-e2e` e.g: `workbench-client-v2-e2e`
- Go to `apps/your-app-name-e2e/` and open the `project.json`
- Change:

```json
"e2e": {
      "executor": "@nx-extend/playwright:test",
      "options": {}
    },
```

- To:

```json
"e2e": {
      "executor": "@nx-extend/e2e-runner:run",
      "options": {
        "runner": "playwright",
        "targets": [
          {
            "target": "your-app:serve",
            "checkUrl": "http://localhost:4200",
            "checkMaxTries": 10000
          }
        ]
      }
    },
```

- The target for the Workbench Client 2.0 would look like this `workbench-client-v2:serve:docker-dev`
- Provide a high value for `checkMaxTries` because the GitLab CI needs a long time to serve the targeted app
- Adjust the `playwright.config.ts` to:

```ts
import { type PlaywrightTestConfig } from '@playwright/test';

const TIMEOUT = 300000;

const config: PlaywrightTestConfig = {
  outputDir: '../../dist/apps/your-app-e2e',
  timeout: TIMEOUT,
  expect: {
    timeout: TIMEOUT,
  },

  use: {
    headless: true,
    viewport: { width: 1280, height: 720 },
    ignoreHTTPSErrors: true,
    video: 'on',
    screenshot: 'only-on-failure',
  },

  reporter: [
    ['list'],
    ['json', { outputFile: 'report/test-results.json' }],
    ['html', { open: 'never', outputFolder: 'report' }],
  ],
};

export default config;
```

- This will provide standardized report outputs which will later be provided as artifacts in the pipeline

## GitLab CI

The Continuos Delivery (CD) process the Gitlab pipeline implements needs regular version bumps of projects.

**🛑 ❗❗
If the child pipeline of a project fails at the version-verify job please have a look at the job logs! You probably forgot to increase the version number of projects you worked on or projects that depend on these.**

The GitLab CI Pipeline is configured to run only on projects that changed since the last release. Every commit on the default branch (master) creates releases for every changed project. Apps are released as docker images to the gitlab docker registry. Libraries are released as npm packages to the gitlab npm package registry. The vendor-app-communication-library is also published to npmjs.com

The Git and therefore CI release workflow looks like this:

1. Create or pick an existing issue
2. Create a branch for that issue (i.e. via GitLab Web UI)
3. Work on that branch (on your feature / fix / etc.)
4. **Bump the version of the projects you have been working on** before pushing changes to GitLab. Do this only once per branch! The version bump should roughly match the [semver spec](https://semver.org/#summary)
   - A valid version follows the semver format `<major>.<minor>.<patch>` i.e `2.1.3`.
   - Apps are versioned in the `version.json` file in the app root dir. Example: `app/workbench-client/version.json`.
   - Libs are versioned in their `package.json` file. Example: `libs/slide-viewer/package.json`)
5. If a version bump is missing the pipeline will fail at the `version-verify` step. Bump the version and push again.
6. Create a merge request to merge into main/master.
7. Only merge into master/main if the CI pipeline succeeds!
8. Pipelines running successfully on master/main always create a new release and a corresponding tag for all affected libs and apps.

### Details:

#### How can I preview which projects need version bumps?

Use the patch affected script. From project root run:

`ts-node ./tools/ci/scripts/projects-patch-affected.ts check`

The script will output the current version & the next version if only a patch bump is needed.

Additionally the last tag from gitlab is retrieved and checked against the current version.

#### How does the pipeline determine which projects changed?

Changes are detected via the `nx affected` command ([docs](https://nx.dev/cli/affected)). To preview locally which projects will be affected run `npm run nx print-affected -- --base=origin/master --select=projects`.

The CI Pipeline will parse affected projects via the `tools/scripts/parse-affected.mjs` script into environment variables.
To test this locally run:

`npm run nx print-affected -- --base=origin/master --select=projects | node ./tools/scripts/parse-affected.mjs`

This will compare the current HEAD with the base branch (main/master).

Optionally also add commit hashes as arguments to specify commits to compare

`npm run nx print-affected -- --base=302f279c26296d034869fa8043747ed7851c995b --head=5e5b00eeb2aa91e1bcddb6da6680305d1c1907ad --select=projects | node ./tools/scripts/parse-affected.mjs`

In the CI pipeline the affected command is run against the last fork-point. The compare is done between branch base (commit on master where we branched of) and the last commit on the current branch.

The affected command also takes workspace project dependencies into account.

> Example: Changing the Slide Viewer lib will also affect all projects using the library (workbench-client,workbench-client-v2 and sample-app). A new release will be created for all affected apps!

Based on the affected projects the CI pipeline creates child pipelines for each project in the workspace. Note that always all child pipelines are created and a dummy job is run - this is due to a limitation in the GitLab CI. The jobs that are run depend on the output of the affected command and the current branch (release and tag jobs are only done on the default branch).

#### How does the pipeline check if the version was bumped?

The check is done via the `/tools/scripts/check-version.mjs` script. The compares the last tagged version against the version currently given in the `version.json` / `package.json` file. Tags are retrieved via the [GitLab API](https://docs.gitlab.com/ee/api/tags.html).

The script expects tags to exactly follow this format `<project-name>-<major>.<minor>.<patch>`. Example: `workbench-client-2.1.3`. Tagging is automatically done via the ci pipeline!

To test the script locally run `node .\tools\scripts\check-version.mjs <path/to/version/file.json> <projectId> <instance url>`.

> Example `node .\tools\scripts\check-version.mjs .\apps\workbench-client\version.json 28967218 'https://gitlab.com'`

The gitlab project id can be found in the web ui 'setting -> general -> Project ID'.

#### How to publish an initial first release?

If there is no tagged version to check against the `check-version` job will fail and therefore no release will be published.

It is possible to skip the version check and publish an initial first version if the following two conditions are met:

1. there is no previous versions published (no tags for project in repo)
2. by setting the semver inside the version.json / package.json to `0.0.1`

#### Automatic Release via CI Pipeline

Automatic releases are done via the CI pipeline if a commit happens on the default branch and if the version was bumped for the project since the last tagged release.

First a release is created than a new tag is pushed. No CI pipeline is run for new tags that are pushed. as for a tag to happen a pipeline must have been run successfully.

## Dependency updates

Install or update dependencies locally by running `npm install <package@version>` ,`npm install <package@version> --save-dev` or by updating the package.json and running `npm install`. See npm docs [here](https://docs.npmjs.com/specifying-dependencies-and-devdependencies-in-a-package-json-file).

After installation / update the lockfile (`package-lock.json`) has changed! Pushing this change to gitlab will trigger a rebuild of the docker ci-base-image. The ci-base-image is versioned using the package-lock.json sha256 hash value.

The first pipeline run with new dependencies will take a bit longer as a new docker image with the updated npm dependency tree is build by the pipeline.

#### Patching ci-base-image version

A gitlab pre-commit hook is used to patch the current package-lock.json hash as version for all dockerfiles. This is done locally on every commit with the script `node ./tools/ci/scripts/out/base-image-hash.js patch-current`.

The pre-commit hook is initialized after running `npm install`.
If the hashes are not patched on commit check that the local git config point to the `/tools/ci/git-hooks` directory by running `git config --local core.hookspath`.

#### internal process of ci pipeline & base image creation

- user runs npm install
- npm automatically runs postinstall script
- postinstall inits git hooks
- user commits
- git hooks runs and
  - calculate hash of new lockfile
  - patches all relevant files with new baseimage:version-sha256 string
  - relevant are root .gitlab-ci & Dockerfiles for production builds
- user pushes
- ci-script calculates sha256 of lockfile
  - (a) checks if image with lockfile hash exists (using gitlab docker api to query image tags)
    - if not new base image is build - using the new dependencies
    - if image exists continue
  - (b) checks if all docker image strings are using this hash as version (gitlab-ci baseimage and Dockerfiles build stages)
    - if not pipeline fails
    - if all versions are correct continue
- all further pipeline steps wait for (a) & (b) to finish successfully
  - pipeline uses image build in (a) for all jobs as base image

#### Debugging ci-base-image

You can calculate the hash locally by running: `node ./tools/ci/scripts/out/base-image-lockfile-hash.ts` from project root.

You can build & run the CI Image locally using the docker-compose.yml.

1. copy env - `cp sample.env .env`
2. docker-compose -f ./tools/ci/base-image/docker-compose.yml build

##### Inspect

1. `docker run -it registry.gitlab.com/empaia/integration/ci-examples-frontend/ci-base-image /bin/bash`
2. install packages: `apt update; apt-get install -y --no-install-recommends bash curl apt-transport-https ca-certificates git; update-ca-certificates`
3. clone repo: `cd /root` ; `git clone https://gitlab.com/empaia/integration/frontend-workspace.git` ; `cd frontend-workspace`
4. link npm dependencies `ln -s ${NPM_MODULES_FOLDER} ./node_modules`
5. run some tests: `npm run nx e2e workbench-client-e2e` - or any other npm / nx script

##### Push

if for some reason a new base image is needed without running the ci pipeline:

`docker push registry.gitlab.com/empaia/integration/frontend-workspace/ci-base-image:<INSERT_VALID_HASH>`

get the hash with the `base-image-lockfile-hash.ts`

Don't forget to convert line endings of the `package-lock.json` if the docker build is run on windows!

## Cypress e2e tests

e2e-tests are currently only used by workbench client. The cypress project can be found in `/apps/workbench-client-e2e/`.

Project templates are setup for all apps in `/apps/<app-name>-e2e` directories.

See the project readme located at: [/apps/workbench-client-e2e/CYPRESS.md](/apps/workbench-client-e2e/CYPRESS.md) for further information.

## Commit Message Format

A commit message consists of a header, body and footer. The header has a type, scope and subject:

```
<type>(<scope>): <subject>
<BLANK LINE>
<body>
<BLANK LINE>
<footer>
```

#### Type

If the prefix is `feat`, `fix`, `perf` or `!` for `BREAKING CHANGE` it will appear in the changelog.

The header is mandatory and the scope of the header is optional.

### Example

Appears under "Features" header, pencil subheader:

`feat(wbc2): add 'delete annotations' option`

`BREAKING CHANGE` can also be indicated by a footer:

Commit message with description and breaking change footer

```
feat: allow provided config object to extend other configs

BREAKING CHANGE: `extends` key in config file is now used for extending other config files
```

see for more: https://github.com/conventional-changelog/conventional-changelog/tree/master/packages/conventional-changelog-angular

## Environment Variables

`WBC_AUTHENTICATION_ON`: If normal authentication should be enabled. Options: `true` or `false`.

`WBC_WBS_SERVER_API_URL`: The URL to the wbs backend service.

`WBC_WBS_USER_ID`: A user id must be set.

`WBC_IDP_URL`: Url to the authentication portal

`WBC_IDP_USER_URL`: Url to the user portal

`WBC_CLIENT_ID`: A client id must be set.

`WBC_LOGGING`: Logging has 6 possible stages.

- `0`: No logging
- `1`: Asserts
- `2`: Errors
- `3`: Warnings
- `4`: Info
- `5`: Unused
- `6`: Verbose

`WBC_IDP_HTTPS_NOT_REQUIRED` If the portal should reroute to HTTPS or HTTP. Options: `true` or `false`.

`COMPOSE_RESTART`: If the docker composer should restart. Options: `yes` or `no`

`COMPOSE_NETWORK`: Sets the network for the docker composer.

`COMPOSE_WBC_HOST`: Sets the local address of the service of the docker composer, e.g.: `127.0.0.1`.

`COMPOSE_WBC_PORT`: Sets the port in which the wbc can be reached.

## Docker Setup

All apps have a corresponding dockerfile and docker-compose setup.

These are located at: `tools/docker/<project-name>`

To run the compose files first create an .env file - sample envs are provided inside the docker folder.

Run `docker-compose up --build -d` inside the folder containing the compose file to startup a local container serving the app in production mode via nginx.

### Summary

```bash
cd tools/docker/<project-name>
cp sample.env .env
docker-compose up --build -d
```

## Generate License File

Generate a license.json file with license information of all used dependencies.

```bash
npm run check-licenses
```

You can find all extracted license information under `/licenses/license.json`.

## Code scaffolding

Run `ng g component my-component --project=my-app` to generate a new component.

With spectator tests: `ng g spectator-component components/test-comp --project workbench-client --change-detection OnPush --jest`

- see docs here: https://github.com/ngneat/spectator/blob/master/README.md#schematics

### nx scaffolding

module directory needs a **file** - e.g.; `apps/workbench-client/src/app/app.module.ts`

## Build

Run `ng build my-app` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `npm run test:prepare` once before runnning tests that depend on OpenLayers.
The script compiles OpenLayers into `node_modules/.compiled/ol` folder

> this issue comment describes this solution: https://github.com/openlayers/openlayers/issues/7401#issuecomment-761740162

`test:prepare` script must be run if openlayers dependency is updated or compiled folder is deleted

Run `nx test <my-app>` to execute the unit tests via [Jest](https://jestjs.io).

Run `nx affected:test` to execute the unit tests affected by a change.

### Test file by pattern:

Run `nx test <project-name> --test-file <file-name-regex>`

- example `nx test vendor-app-integration --test-file listener.service*`
- project name are defined in jest config `displayName` property

### Update Snapshot tests

run `npm run jest <path_to_testfile> -- -- --u`

example: `npm run jest /libs/slide-viewer/src/lib/store/annotations/annotation.reducer.tmp-spec.ts -- -- --u`

## Running end-to-end tests

Run `ng e2e my-app` to execute the end-to-end tests via [Cypress](https://www.cypress.io).

Run `nx affected:e2e` to execute the end-to-end tests affected by a change.

## Generate an application

Run `ng g @nx/angular:app my-app` to generate an application.

> You can use any of the plugins above to generate applications as well.

When using Nx, you can create multiple applications and libraries in the same workspace.

## Generate a library

Run `ng g @nx/angular:lib my-lib` to generate a library.

> You can also use any of the plugins above to generate libraries as well.

Libraries are shareable across libraries and applications. They can be imported from `@empaia/mylib`.

## Generate a feature Store

Feature Stores have been generated with:
`ng generate @nx/angular:ngrx --name=<FeatureName> --module=path/to/module/file.module.ts --directory=directory/for/feature/relative/to/module --barrels --skipImport --skipPackageJson --useDataPersistence --dry-run`

eg for classes sub feature:

`ng generate @nx/angular:ngrx --name=classes --module=apps/workbench-client/src/app/annotations/annotations.module.ts --directory=store/classes --barrels --skipImport --skipPackageJson --useDataPersistence --no-interactive --dry-run`

you need to add the `index.ts` file by hand.

## Further help

Visit the [Nx Documentation](https://nx.dev/angular) to learn more.
