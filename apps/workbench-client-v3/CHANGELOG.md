# Workbench Client 3.0

# 3.2.16 (2025-02-12)

### Fixes

* fixes vendor logo position in apps panel

# 3.2.14 (2024-07-16)

### Refactor

* upgraded to angular 17

# 3.2.13 (2024-03-20)

### Fixes

* reopening the slide tray view displays properly

# 3.2.11 (2024-01-19)

### Refactor

* removed mock preview images from new apps panel
* removed description and expansion section in apps panel

# 3.2.10 (2024-01-11)

### Refactor

* updated to angular 16

# 3.2.6 (2023-09-19)

### Refactor

* cut optional tailing slash in env urls

# 3.2.5 (2023-09-18)

### Refactor

* change api for optional user profile picture

# 3.2.4 (2023-09-18)

### Refactor

* positioned tooltips above mouse cursor when ever possible

# 3.2.0 (2023-08-02)

### Refactor

* optimized slide pagination speed

# 3.1.7 (2023-07-21)

### Fixes

* apps open properly when apps cache discards apps

# 3.1.6 (2023-07-03)

### Fixes

* parse iframe arguments from snack case to kebab case

# 3.1.5 (2023-06-16)

### Refactor

* reverted back to the old loading bar, instead of the loading spinner in apps panel

### Fixes

* loading bar is now visible on selected panel labels

# 3.1.3 (2023-06-09)

### Fixes

* new apps panel handles empty apps lists

# 3.1.2 (2023-06-08)

### Refactor

* replaced old logo for new one
* refactored tooltips

# 3.1.0 (2023-06-06)

### Features

* added frontend pagination for slides in slides-tray-view and slides-list

# 3.0.7 (2023-04-20)

### Fixes

* new apps panel list will not flash on reopen
* apps in app list will be highlighted when hovering over in new apps panel

# 3.0.5 (2023-03-31)

### Fixes

* use correct api version for tile url

# 3.0.3 (2023-03-24)

### Refactor

* use css for truncate text

# 3.0.0 (2023-03-20)

### Feature

* updated to Angular 15

### Refactor

* changed partition for cases panel elements
* uses css in cases panel for truncate
* changed preprocessing icons

### Fixes

* logos in apps- and new apps panel are properly displayed

# 0.3.2 (2023-02-24)

### Refactor

* apps list is now scrollable
* set concurrent vendor apps limit from 3 to 5

# 0.3.1 (2023-01-20)

### Fixes

* Side-Nav adjust to the menu correctly

# 0.3.0 (2023-01-06)

### Refactor

* The app url and iframe sandbox settings are stored after both request where made
* If the backend has no iframe settings the default settings will be set
* The wbc will allways add "allow-scripts" as default settings

# 0.2.7 (2022-12-06)

### Fixes

* Preprocessing trigger menu can also be accessed via drop down menu in no authentication deployment

### Refactor

* Removed `has_frontend` filter in apps effect

# 0.2.6 (2022-11-28)

### Refactor

* Apps for post preprocessing trigger will be filter via backend

# 0.2.3 (2022-11-22)

### Refactor

* Removed DataPersistence Object

# 0.2.2 (2022-11-18)

### Bugs

* Tags will be shown correctly in New Apps Panel under details

# 0.2.1 (2022-11-16)

### Refactor

* Apps-Panel has now a white background for vendor logos and doesn't break at long vendor names

# 0.2.0 (2022-11-15)

### Features

* Implemented WBS V3 Routes

### Refactor

* Examination are not longer available in case panel, it's integrated in the apps panel

# 0.1.0 (2022-11-07)

### Features

* User which is in the wrong organization can now logout

# 0.0.2 (2022-11-07)

### Refactor

* Removed Angular fxLayout

# 0.0.1 (2022-11-03)

### Refactor

* Initial commit for Workbench Client 3.0 with old V2 Routes (for now)

### Features

* Added preprocessing trigger menu (uses mocks)
