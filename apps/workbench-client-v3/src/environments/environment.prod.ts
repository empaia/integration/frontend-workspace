export const environment = {
  production: true,
  authenticationOn: window['env']['authenticationOn'],
  wbsServerApiUrl: window['env']['wbsServerApiUrl'],
  idpUrl: window['env']['idpUrl'],
  idpUserUrl: window['env']['idpUserUrl'],
  solutionStoreUrl: window['env']['solutionStoreUrl'],
  wbsUserId: window['env']['wbsUserId'],
  clientId: window['env']['clientId'],
  idpHttpsNotRequired: window['env']['idpHttpsNotRequired'],
  organizationName: window['env']['organizationName'],
  aaaApiUrl: window['env']['aaaApiUrl'],
  logging: window['env']['logging'],
};
