// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  authenticationOn: 'true',

  // iron deployment:
  wbsServerApiUrl: 'https://empaia-plat03.charite.de/iron/wbs-api',
  idpUrl: 'https://login-nightly.empaia.org/auth/realms/empaia',
  idpUserUrl: 'https://auth-nightly.empaia.org/api/users',
  solutionStoreUrl: 'https://portal-nightly.empaia.org',
  wbsUserId: 'fbcf0904-e681-4b20-8d17-a7470745719c',
  clientId: 'iron-wbc-18e570af-a5ce-402c-88d6-37f6320615ca',
  idpHttpsNotRequired: 'false',
  organizationName: 'EMPAIA',
  aaaApiUrl: '',

  // logger
  logging: 6,
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
