import * as fromWbsUrl from './wbs-url.actions';

describe('loadWbsUrls', () => {
  it('should return an action', () => {
    expect(fromWbsUrl.loadWbsUrl().type).toBe('[WbsUrl] Load Wbs Url');
  });
});
