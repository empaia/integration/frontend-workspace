import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'runtime'
})
export class RuntimePipe implements PipeTransform {

  transform(runtimeInSeconds: number | undefined): string {
    if(runtimeInSeconds === undefined || runtimeInSeconds === null) {
      return '-';
    }
  
    const days = Math.floor(runtimeInSeconds / (3600 * 24));
    const hrs  = Math.floor(runtimeInSeconds % (3600 * 24) / 3600);
    const mins = Math.floor(runtimeInSeconds % 3600 / 60);
    const secs = Math.floor(runtimeInSeconds % 60);

    const dDisplay = days > 0 ? days + (days == 1 ? " day, " : " days, ") : "";
    const hDisplay = hrs > 0 ? hrs + (hrs == 1 ? " hr, " : " hrs, ") : "";
    const mDisplay = mins > 0 ? mins + (mins == 1 ? " min, " : " mins, ") : "";
    const sDisplay = secs > 0 ? secs + (secs == 1 ? " sec" : " secs") : "";

    return dDisplay + hDisplay + mDisplay + sDisplay;   
  }
}