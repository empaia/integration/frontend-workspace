import { DatePipe } from '@angular/common';
import { Injectable, Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'customDate'
})
@Injectable()
export class CustomDatePipe implements PipeTransform {

  transform(value: number | undefined, locale = 'en-US', dateFormat = 'MMM d, y, h:mm:ss'): string | null {
    if (value !== undefined) {
      const pipe = new DatePipe(locale);
      return pipe.transform(new Date(value * 1000), dateFormat);
    }
    return null;
  }

}
