import { Pipe, PipeTransform } from '@angular/core';
import { WbcV3TextTranslation } from 'empaia-api-lib';

@Pipe({
  name: 'translate'
})
export class TranslatePipe implements PipeTransform {

  transform(value: WbcV3TextTranslation[] | undefined, language = 'EN'): string {
    return this.getText(language, value);
  }

  private getText(language: string, translations: WbcV3TextTranslation[] | undefined): string {
    if (translations) {
      for (const translation of translations) {
        if (language.toLowerCase() === translation.lang.toLowerCase()) {
          return translation.text;
        }
      }
    }

    return 'missing_translation';
  }

}
