import { EntityCachingState } from '@shared/entity/models/caching.models';
import { DEFAULT_ENTITY_CACHE_SIZE } from '@shared/entity/models/caching.constants';

export class CachingState<T> implements EntityCachingState<T> {
  addedEntityIds: string[] | undefined = undefined;
  cachedSizeConstraint: number;
  entityCache: Map<string, T> = new Map<string, T>();

  constructor(cacheSize?: number) {
    this.cachedSizeConstraint = cacheSize ? cacheSize : DEFAULT_ENTITY_CACHE_SIZE;
  }
}
