import { IdSelector } from '@ngrx/entity';

interface MinimalEntity {
  id: string;
}

interface EntityCachingState<T> {
  entityCache: Map<string, T>;
  addedEntityIds: string[] | undefined;
  cachedSizeConstraint: number;
}

interface EntityCachingStateAdapter<T> {
  addOne<S extends EntityCachingState<T>>(entity: T, state: S): S;
  addMany<S extends EntityCachingState<T>>(entities: T[], state: S): S;
  removeOne<S extends EntityCachingState<T>>(key: string, state: S): S;
  removeMany<S extends EntityCachingState<T>>(keys: string[], state: S): S;
  removeToIndex<S extends EntityCachingState<T>>(index: number, state: S): S;
  removeAll<S extends EntityCachingState<T>>(state: S): S;
  updateOne<S extends EntityCachingState<T>>(entity: T, state: S): S;
}

interface EntityCachingSelectors<T, V extends EntityCachingState<T>> {
  selectIds: (state: V) => string[];
  selectAll: (state: V) => T[];
  selectTotal: (state: V) => number;
}

interface EntityCachingAdapter<T> extends EntityCachingStateAdapter<T> {
  selectedId: IdSelector<T>;
  getInitialState(cacheSize?: number): EntityCachingState<T>;
  getInitialState<S>(state: S, cacheSize?: number): EntityCachingState<T> & S;
  getSelectors(): EntityCachingSelectors<T, EntityCachingState<T>>;
}

export {
  MinimalEntity,
  EntityCachingState,
  EntityCachingStateAdapter,
  EntityCachingSelectors,
  EntityCachingAdapter,
};
