import { CdkAccordionItem } from '@angular/cdk/accordion';
import {
  Component,
  ChangeDetectionStrategy,
  Input,
} from '@angular/core';
import { matExpansionAnimations, MatExpansionPanelState } from '@angular/material/expansion';

type ExpansionPosition = 'before' | 'after';

@Component({
  selector: 'app-expansion-item',
  templateUrl: './expansion-item.component.html',
  styleUrls: ['./expansion-item.component.scss'],
  animations: [
    matExpansionAnimations.bodyExpansion,
    matExpansionAnimations.indicatorRotate
  ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ExpansionItemComponent extends CdkAccordionItem {
  @Input() position: ExpansionPosition = 'after';

  @Input() headerClass!: string;
  @Input() descriptionClass!: string;
  @Input() expansionClass!: string;
  @Input() footerClass!: string;
  @Input() selected = false;
  @Input() hideDescription = false;
  @Input() hideExpansionButton = false;

  toggleAccordion(): void {
    this.toggle();
  }

  getExpandedState(): MatExpansionPanelState {
    return this.expanded ? 'expanded' : 'collapsed';
  }

}
