import { MaterialModule } from '@material/material.module';
import { Spectator, createComponentFactory } from '@ngneat/spectator/jest';
import { MockComponents } from 'ng-mocks';
import { ExpansionItemComponent } from './expansion-item.component';
import { ExpansionButtonComponent } from '@shared/components/expansion-button/expansion-button.component';

describe('ExpansionItemComponent', () => {
  let spectator: Spectator<ExpansionItemComponent>;
  const createComponent = createComponentFactory({
    component: ExpansionItemComponent,
    imports: [
      MaterialModule,
    ],
    declarations: [
      MockComponents(
        ExpansionButtonComponent
      ),
    ]
  });

  beforeEach(() => spectator = createComponent({ props: {
    get disabled(): boolean {
      return false;
    }}}));

  it('should create', () => {
    expect(spectator.component).toBeTruthy();
  });
});
