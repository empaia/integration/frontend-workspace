import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { matExpansionAnimations, MatExpansionPanelState } from '@angular/material/expansion';

@Component({
  selector: 'app-expansion-button',
  templateUrl: './expansion-button.component.html',
  styleUrls: ['./expansion-button.component.scss'],
  animations: [
    matExpansionAnimations.bodyExpansion,
    matExpansionAnimations.indicatorRotate
  ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ExpansionButtonComponent {
  @Input() disabled = false;
  @Input() selected = false;
  @Input() expansionState!: MatExpansionPanelState;

  @Output() toggle = new EventEmitter<void>();

  public onToggleClicked(event: Event): void {
    if (!this.disabled) {
      this.toggle.emit();
    }
    event.stopPropagation();
  }
}
