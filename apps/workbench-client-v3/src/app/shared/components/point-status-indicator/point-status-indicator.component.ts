/* eslint-disable @typescript-eslint/no-explicit-any */
import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';

@Component({
  selector: 'app-point-status-indicator',
  templateUrl: './point-status-indicator.component.html',
  styleUrls: ['./point-status-indicator.component.scss']
})
export class PointStatusIndicatorComponent implements OnChanges, OnInit {
  @Input() open!: boolean;
  @Input() width = 15;
  @Input() height = 15;
  @Input() radius = 5;
  @Input() centerX = 8;
  @Input() centerY = 8;
  @Input() title?: string;
  @Input() titleColor?: string;
  titleStyle: any = {};

  ngOnInit() {
    if (this.titleColor) {
      this.titleStyle['color'] = this.titleColor;
    }
  }

  ngOnChanges(changes: SimpleChanges) {
    if ('titleColor' in changes) {
      this.titleStyle['color'] = this.titleColor;
    }
  }
}
