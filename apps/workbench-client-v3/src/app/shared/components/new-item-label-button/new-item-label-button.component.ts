import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-new-item-label-button',
  templateUrl: './new-item-label-button.component.html',
  styleUrls: ['./new-item-label-button.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class NewItemLabelButtonComponent {
  @Input() public disabled!: boolean;
  @Output() public clicked = new EventEmitter<MouseEvent>();
}
