import { NewItemLabelButtonComponent } from './new-item-label-button.component';
import { createComponentFactory, Spectator } from '@ngneat/spectator';
import { MaterialModule } from '@material/material.module';

describe('NewItemLabelButtonComponent', () => {
  let spectator: Spectator<NewItemLabelButtonComponent>;
  const createComponent = createComponentFactory({
    component: NewItemLabelButtonComponent,
    imports: [
      MaterialModule,
    ]
  });

  beforeEach(() => spectator = createComponent());

  it('should create', () => {
    expect(spectator.component).toBeTruthy();
  });
});
