import { Component, ChangeDetectionStrategy, Inject } from '@angular/core';
import { MatSnackBarRef, MAT_SNACK_BAR_DATA } from '@angular/material/snack-bar';
import { ErrorSnackbarData } from '@menu/models/ui.models';

@Component({
  selector: 'app-error-snackbar',
  templateUrl: './error-snackbar.component.html',
  styleUrls: ['./error-snackbar.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ErrorSnackbarComponent {

  constructor(
    @Inject(MAT_SNACK_BAR_DATA) public data: ErrorSnackbarData,
    private snackBarRef: MatSnackBarRef<ErrorSnackbarComponent>,
  ) { }

  close() {
    this.snackBarRef.dismiss();
  }

}
