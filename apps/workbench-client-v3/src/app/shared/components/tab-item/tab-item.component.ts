import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

@Component({
  selector: 'app-tab-item',
  templateUrl: './tab-item.component.html',
  styleUrls: ['./tab-item.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TabItemComponent {
  @Input() id!: string;
  @Input() checked!: boolean;
  @Input() labelName!: string;

  @Input() tabClass!: string;
  @Input() tabSwitchClass!: string;
  @Input() tabLabelClass!: string;
  @Input() tabContentClass!: string;
}
