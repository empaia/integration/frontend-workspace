export interface ConfirmationDialogData {
  headlineText: string;
  contentText: string;
  acceptButtonText: string;
  cancelButtonText: string;
}
