import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MaterialModule } from '@material/material.module';
import { Spectator, createComponentFactory } from '@ngneat/spectator';
import { ConfirmationDialogComponent } from './confirmation-dialog.component';

describe('ConfirmationDialogComponent', () => {
  let spectator: Spectator<ConfirmationDialogComponent>;
  const createComponent = createComponentFactory({
    component: ConfirmationDialogComponent,
    imports: [
      MaterialModule
    ],
    providers: [
      { provide: MAT_DIALOG_DATA, useValue: {} },
    ]
  });

  beforeEach(() => spectator = createComponent());

  it('should create', () => {
    expect(spectator.component).toBeTruthy();
  });
});
