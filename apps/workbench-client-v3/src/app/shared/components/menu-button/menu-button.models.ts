import { DiagnosticMenuItem } from '@menu/models/menu.models';

export interface MenuButtonEventModel {
  mouse: MouseEvent;
  value: DiagnosticMenuItem;
}
