import { MenuLabelComponent } from './menu-label.component';
import { createComponentFactory, Spectator } from '@ngneat/spectator';
import { MockComponents } from 'ng-mocks';
import { MenuLoadingComponent } from '@shared/components/menu-loading/menu-loading.component';

describe('MenuLabelComponent', () => {
  let spectator: Spectator<MenuLabelComponent>;
  const createComponent = createComponentFactory({
    component: MenuLabelComponent,
    declarations: [
      MockComponents(
        MenuLoadingComponent,
      )
    ]
  });

  beforeEach(() => spectator = createComponent());

  it('should create', () => {
    expect(spectator.component).toBeTruthy();
  });
});
