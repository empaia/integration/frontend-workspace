import { Spectator, createComponentFactory } from '@ngneat/spectator/jest';

import { NewItemButtonComponent } from './new-item-button.component';

describe('NewItemButtonComponent', () => {
  let spectator: Spectator<NewItemButtonComponent>;
  const createComponent = createComponentFactory(NewItemButtonComponent);

  it('should create', () => {
    spectator = createComponent();

    expect(spectator.component).toBeTruthy();
  });
});
