import { ErrorMessageCardComponent } from './error-message-card.component';
import { createComponentFactory, Spectator } from '@ngneat/spectator';

describe('ErrorMessageCardComponent', () => {
  let spectator: Spectator<ErrorMessageCardComponent>;
  const createComponent = createComponentFactory({
    component: ErrorMessageCardComponent,
  });

  beforeEach(() => spectator = createComponent());

  it('should create', () => {
    expect(spectator.component).toBeTruthy();
  });
});
