import { Component, ChangeDetectionStrategy, Input } from '@angular/core';
import { ERROR_MESSAGE_TABLE } from '@shared/components/error-message-card/error-message-card.models';
import { HttpError } from '@core/store/authentication/authentication.model';

@Component({
  selector: 'app-error-message-card',
  templateUrl: './error-message-card.component.html',
  styleUrls: ['./error-message-card.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ErrorMessageCardComponent {
  public readonly ERROR_TABLE = ERROR_MESSAGE_TABLE;

  @Input() public error!: HttpError;
}
