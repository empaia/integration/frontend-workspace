import { MenuItemLayoutComponent } from './menu-item-layout.component';
import { createComponentFactory, Spectator } from '@ngneat/spectator';
import { provideMockStore } from '@ngrx/store/testing';

describe('MenuItemLayoutComponent', () => {
  let spectator: Spectator<MenuItemLayoutComponent>;
  const createComponent = createComponentFactory({
    component: MenuItemLayoutComponent,
    providers: [
      provideMockStore()
    ]
  });

  beforeEach(() => spectator = createComponent());

  it('should create', () => {
    expect(spectator.component).toBeTruthy();
  });
});
