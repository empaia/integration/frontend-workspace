import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SafeUrlPipe } from './pipes/safe-url.pipe';
import { CustomDatePipe } from './pipes/custom-date.pipe';
import { LocalSafeUrlPipe } from './pipes/local-safe-url.pipe';
import { DictParserPipe } from './pipes/dict-parser.pipe';
import { CopyToClipboardDirective } from './directives/copy-to-clipboard.directive';
import { MaterialModule } from '@material/material.module';
import { MissingSelectionErrorComponent } from './components/missing-selection-error/missing-selection-error.component';
import { ExpansionButtonComponent } from './components/expansion-button/expansion-button.component';
import { ExpansionItemComponent } from './components/expansion-item/expansion-item.component';
import { NewItemButtonComponent } from './components/new-item-button/new-item-button.component';
import { PointStatusIndicatorComponent } from './components/point-status-indicator/point-status-indicator.component';
import { ErrorSnackbarComponent } from '@shared/components/error-snackbar/error-snackbar.component';
import { MenuButtonComponent } from './components/menu-button/menu-button.component';
import { MenuLabelComponent } from './components/menu-label/menu-label.component';
import { MenuLoadingComponent } from './components/menu-loading/menu-loading.component';
import { MenuItemLayoutComponent } from './containers/menu-item-layout/menu-item-layout.component';
import { NewItemLabelButtonComponent } from './components/new-item-label-button/new-item-label-button.component';
import { TabItemComponent } from './components/tab-item/tab-item.component';
import { TabGroupComponent } from './components/tab-group/tab-group.component';
import { CloseButtonComponent } from './components/close-button/close-button.component';
import { ErrorMessageCardComponent } from './components/error-message-card/error-message-card.component';
import { PaginationComponent } from './components/pagination/pagination.component';
import { TranslatePipe } from './pipes/translate.pipe';
import { ConfirmationDialogComponent } from './components/confirmation-dialog/confirmation-dialog.component';

const COMPONENTS = [
  MissingSelectionErrorComponent,
  ExpansionButtonComponent,
  ExpansionItemComponent,
  NewItemButtonComponent,
  PointStatusIndicatorComponent,
  ErrorSnackbarComponent,
  MenuButtonComponent,
  MenuLabelComponent,
  MenuLoadingComponent,
  MenuItemLayoutComponent,
  NewItemLabelButtonComponent,
  TabItemComponent,
  TabGroupComponent,
  CloseButtonComponent,
  ErrorMessageCardComponent,
  PaginationComponent,
  ConfirmationDialogComponent,
];

const PIPES = [
  SafeUrlPipe,
  CustomDatePipe,
  LocalSafeUrlPipe,
  DictParserPipe,
  TranslatePipe,
];

const DIRECTIVES = [
  CopyToClipboardDirective,
];

@NgModule({
  declarations: [
    COMPONENTS,
    PIPES,
    DIRECTIVES,
  ],
  imports: [
    CommonModule,
    MaterialModule,
  ],
  exports: [
    COMPONENTS,
    PIPES,
    DIRECTIVES,
  ]
})
export class SharedModule { }
