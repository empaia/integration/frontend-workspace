import * as fromScope from './scope.actions';

describe('loadScopes', () => {
  it('should return an action', () => {
    expect(fromScope.loadSelectedAppScope().type).toBe('[Scope] Load Selected App Scope');
  });
});
