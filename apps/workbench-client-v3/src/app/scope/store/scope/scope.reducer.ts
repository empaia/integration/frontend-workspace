import { createReducer, on } from '@ngrx/store';
import { HttpErrorResponse } from '@angular/common/http';
import * as ScopeActions from './scope.actions';
import { createEntityAdapter, EntityAdapter, EntityState } from '@ngrx/entity';
import { ScopeEntity } from '@scope/store/scope/scope.models';


export const SCOPE_FEATURE_KEY = 'scope';

export interface State extends EntityState<ScopeEntity> {
  error?: HttpErrorResponse;
}

export const scopeAdapter: EntityAdapter<ScopeEntity> = createEntityAdapter<ScopeEntity>();

export const initialState: State = scopeAdapter.getInitialState();


export const reducer = createReducer(
  initialState,
  on(ScopeActions.loadAppScope, (state, { examinationId }): State =>
    scopeAdapter.upsertOne({ id: examinationId, loaded: false }, {
      ...state
    })
  ),
  on(ScopeActions.loadScopeSuccess, (state, { scope }): State =>
    scopeAdapter.upsertOne(scope, {
      ...state,
    })
  ),
  on(ScopeActions.loadScopeFailure, (state, { examinationId, error }): State =>
    scopeAdapter.upsertOne({ id: examinationId, loaded: false, error }, {
      ...state
    })
  ),
  on(ScopeActions.deleteScope, (state, { examinationId }): State =>
    scopeAdapter.removeOne(examinationId, {
      ...state,
    })
  ),
  on(ScopeActions.clearScopes, (state): State =>
    scopeAdapter.removeAll({
      ...state,
      ...initialState,
    })
  ),
);

