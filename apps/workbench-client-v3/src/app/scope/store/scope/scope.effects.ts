import { Injectable } from '@angular/core';
import { Actions, concatLatestFrom, createEffect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import * as ScopeActions from './scope.actions';
import * as ExaminationsSelectors from '@examinations/store/examinations/examinations.selectors';
import { distinctUntilChanged, map, retry } from 'rxjs/operators';
import { Scope, SCOPE_TYPE } from 'vendor-app-integration';
import { fetch } from '@ngrx/router-store/data-persistence';
import { WbcV3ExaminationsService } from 'empaia-api-lib';
import { DEFAULT_SCOPE_RETRIES, ScopeEntity } from '@scope/store/scope/scope.models';
import { filterNullish } from '@helper/rxjs-operators';
import { State } from './scope.reducer';

@Injectable()
export class ScopeEffects {
  prepareSelectedAppScope$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ScopeActions.loadSelectedAppScope),
      concatLatestFrom(() => [
        this.store
          .select(ExaminationsSelectors.selectSelectedId)
          .pipe(filterNullish()),
      ]),
      map(([, examinationId]) => examinationId),
      distinctUntilChanged(),
      map((examinationId) => ScopeActions.loadAppScope({ examinationId }))
    );
  });

  loadScope$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ScopeActions.loadAppScope),
      fetch({
        run: (
          action: ReturnType<typeof ScopeActions.loadAppScope>,
          _state: State
        ) => {
          return this.examinationsPanelService
            .examinationsExaminationIdScopePut({
              examination_id: action.examinationId,
            })
            .pipe(
              retry(DEFAULT_SCOPE_RETRIES),
              map((response) => response.scope_id),
              map((scopeId) => {
                const scope: Scope = {
                  id: scopeId,
                  type: SCOPE_TYPE,
                };
                return scope;
              }),
              map((scope) => {
                const scopeEntity: ScopeEntity = {
                  id: action.examinationId,
                  loaded: true,
                  scope,
                };
                return scopeEntity;
              }),
              map((scope) => ScopeActions.loadScopeSuccess({ scope }))
            );
        },
        onError: (
          action: ReturnType<typeof ScopeActions.loadAppScope>,
          error
        ) => {
          return ScopeActions.loadScopeFailure({
            examinationId: action.examinationId,
            error,
          });
        },
      })
    );
  });

  constructor(
    private readonly actions$: Actions,
    private readonly store: Store,
    private readonly examinationsPanelService: WbcV3ExaminationsService
  ) {}
}
