import { TestBed } from '@angular/core/testing';

import { Observable } from 'rxjs';

import { provideMockActions } from '@ngrx/effects/testing';
import { provideMockStore } from '@ngrx/store/testing';

import { ExaminationsEffects } from '@examinations/store';
import * as ExaminationsActions from './examinations.actions';
import { HttpClient } from '@angular/common/http';
import { hot } from 'jasmine-marbles';

describe('ExaminationsEffects', () => {
  let actions: Observable<unknown>;
  let effects: ExaminationsEffects;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        ExaminationsEffects,
        HttpClient,
        provideMockActions(() => actions),
        provideMockStore(),
      ],
    });

    effects = TestBed.inject(ExaminationsEffects);
  });

  describe('load$', () => {
    it('should work', () => {
      actions = hot('-a-|', {
        a: ExaminationsActions.loadExaminations({ caseId: 'test-id' }),
      });

      const expected = hot('-a-|', {
        a: ExaminationsActions.loadExaminationsSuccess({ examinations: [] }),
      });

      expect(effects.loadExaminations$).toBeObservable(expected);
    });
  });
});
