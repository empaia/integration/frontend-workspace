import { WbcV3WorkbenchServiceApiV3CustomModelsExaminationsExamination } from 'empaia-api-lib';
import { CLOSE_EXAMINATION_ERROR, CREATE_EXAMINATION_ERROR, EXAMINATIONS_LOAD_ERROR } from '@menu/models/ui.models';
export {
  WbcV3ExaminationCreatorType as ExaminationCreatorType,
  WbcV3ExaminationState as ExaminationState
} from 'empaia-api-lib';

/* export interface CaseExaminationSelection {
  caseId: string;
  examinationId: string;
} */

export type Examination = WbcV3WorkbenchServiceApiV3CustomModelsExaminationsExamination;

export interface ExaminationExpansion {
  id: string;
}

export const DIALOG_WIDTH = '500px';

export const EXAMINATIONS_POLLING_PERIOD = 30000;

export const ExaminationErrorMap = {
  '[Examinations] Load Examinations Failure': EXAMINATIONS_LOAD_ERROR,
  '[Examinations] Create Examinations Failure': CREATE_EXAMINATION_ERROR,
  '[Examinations] Close Examination Failure': CLOSE_EXAMINATION_ERROR,
};

export const OPEN_EXAMINATION_HEADLINE = 'Open new examination';
export const OPEN_EXAMINATION_CONTENT_TEXT = `
A new examination of the selected case will be opened. New examinations allow diagnostic results to be created.
Previously closed examinations are still available for selection and existing results can be viewed but not created
or altered. A list of available examinations is accessible when expanding the case (arrow symbol in case list).
`;
export const OPEN_EXAMINATION_ACCEPT_BUTTON_TEXT = 'open';
export const OPEN_EXAMINATION_CANCEL_BUTTON_TEXT = 'cancel';

export const CLOSE_EXAMINATION_HEADLINE = 'Close examination';
export const CLOSE_EXAMINATION_CONTENT_TEXT = `
The open examination of the selected app will be closed, such that existing diagnostic results can still be viewed
but not created or altered. Closed examinations are available for selection and it is possible to open a new examination
to create further diagnostic results. A new examination can be created by adding a new app to the apps list via the
"+ NEW" button.
`;
export const CLOSE_EXAMINATION_ACCEPT_BUTTON = 'close';
export const CLOSE_EXAMINATION_CANCEL_BUTTON = 'cancel';
