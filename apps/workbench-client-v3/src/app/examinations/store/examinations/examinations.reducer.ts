import { createEntityAdapter, EntityState } from '@ngrx/entity';
import { createReducer, on, Action } from '@ngrx/store';
import * as ExaminationsActions from './examinations.actions';
import { HttpErrorResponse } from '@angular/common/http';
import { Examination, ExaminationExpansion } from './examinations.models';

export const EXAMINATIONS_FEATURE_KEY = 'examinations';

export interface State extends EntityState<Examination> {
  loaded: boolean;
  preselected: string | undefined;
  expansion: EntityState<ExaminationExpansion>;
  error?: HttpErrorResponse;
}

export const examinationsAdapter = createEntityAdapter<Examination>();
export const examinationExpansionAdapter = createEntityAdapter<ExaminationExpansion>();

export const initialState: State = examinationsAdapter.getInitialState({
  loaded: true,
  preselected: undefined,
  expansion: examinationExpansionAdapter.getInitialState(),
  error: undefined,
});

const examinationsReducer = createReducer(
  initialState,
  on(ExaminationsActions.loadExaminations, (state): State => ({
    ...state,
    loaded: false,
  })),
  on(ExaminationsActions.loadExaminationsSuccess, (state, { examinations }): State =>
    examinationsAdapter.setAll(examinations, {
      ...state,
      loaded: true,
    })
  ),
  on(ExaminationsActions.loadExaminationsFailure, (state, { error }): State => ({
    ...state,
    loaded: true,
    error,
  })),
  on(ExaminationsActions.createExamination, (state): State => ({
    ...state,
    loaded: false,
  })),
  on(ExaminationsActions.createExaminationFailure, (state, { error }): State => ({
    ...state,
    loaded: true,
    error
  })),
  on(ExaminationsActions.closeExamination, (state): State => ({
    ...state,
    loaded: false,
  })),
  on(ExaminationsActions.closeExaminationFailure, (state, { error }): State => ({
    ...state,
    loaded: true,
    error
  })),
  on(ExaminationsActions.clearExaminations, (state): State =>
    examinationsAdapter.removeAll({
      ...state,
      ...initialState,
    })
  ),
  on(ExaminationsActions.preSelectExamination, (state, { examinationId }): State => ({
    ...state,
    preselected: examinationId
  })),
  on(ExaminationsActions.expandExamination, (state, { id }): State => ({
    ...state,
    expansion: examinationExpansionAdapter.upsertOne({ id }, {
      ...state.expansion,
    })
  })),
  on(ExaminationsActions.collapseExamination, (state, { id }): State => ({
    ...state,
    expansion: examinationExpansionAdapter.removeOne(id, {
      ...state.expansion,
    })
  })),
  on(ExaminationsActions.collapseAllExaminations, (state): State => ({
    ...state,
    expansion: examinationExpansionAdapter.removeAll({
      ...state.expansion
    })
  }))
);

export function reducer(state: State | undefined, action: Action) {
  return examinationsReducer(state, action);
}
