import { SelectionError } from '@menu/models/ui.models';
import { createSelector } from '@ngrx/store';
import { RouterSelectors } from '@router/store';
import {
  State as ModuleState,
  selectExaminationFeatureState,
} from '../examinations-feature.state';
import {
  examinationsAdapter,
  examinationExpansionAdapter,
} from './examinations.reducer';

const {
  selectAll,
  selectEntities,
} = examinationsAdapter.getSelectors();

export const selectExaminationsState = createSelector(
  selectExaminationFeatureState,
  (state: ModuleState) => state.examinations
);

export const selectAllExaminations = createSelector(
  selectExaminationsState,
  selectAll,
);

export const selectExaminationsEntities = createSelector(
  selectExaminationsState,
  selectEntities,
);

export const selectSelectedId = createSelector(
  RouterSelectors.selectExaminationIdParam,
  (id) => id
);

export const selectSelected = createSelector(
  selectExaminationsEntities,
  selectSelectedId,
  (entities, examinationId) => examinationId && entities[examinationId]
);

export const selectExaminationsLoaded = createSelector(
  selectExaminationsState,
  (state) => state.loaded
);

export const selectPreSelected = createSelector(
  selectExaminationsState,
  (state) => state.preselected
);

export const selectExpandedExaminationEntities = createSelector(
  selectExaminationsState,
  (state) => examinationExpansionAdapter.getSelectors().selectEntities(state.expansion)
);

export const selectSelectionError = createSelector(
  RouterSelectors.selectCaseIdParam,
  (caseId: string | undefined): SelectionError | undefined => !caseId ? 'NO_CASE_SELECTION' : undefined
);
