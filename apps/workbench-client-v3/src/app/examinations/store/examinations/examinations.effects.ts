import {
  CLOSE_EXAMINATION_ACCEPT_BUTTON,
  CLOSE_EXAMINATION_CANCEL_BUTTON,
  CLOSE_EXAMINATION_CONTENT_TEXT,
  CLOSE_EXAMINATION_HEADLINE,
  DIALOG_WIDTH,
  ExaminationErrorMap,
} from './examinations.models';
import { Injectable } from '@angular/core';
import { createEffect, Actions, ofType, concatLatestFrom } from '@ngrx/effects';
import { pessimisticUpdate, fetch } from '@ngrx/router-store/data-persistence';

import * as ExaminationsFeature from './examinations.reducer';
import * as ExaminationsActions from './examinations.actions';
import * as ExaminationsSelectors from './examinations.selectors';
import * as CasesActions from '@cases/store/cases/cases.actions';
import * as CasesSelectors from '@cases/store/cases/cases.selectors';
import * as VendorAppSurfacesActions from '@apps/store/vendor-app-surfaces/vendor-app-surfaces.actions';
import * as VendorAppSurfacesSelectors from '@apps/store/vendor-app-surfaces/vendor-app-surfaces.selectors';
import { WbcV3ExaminationsService } from 'empaia-api-lib';
import { distinctUntilChanged, exhaustMap, filter, map } from 'rxjs/operators';
import { Store } from '@ngrx/store';
import { Router } from '@angular/router';
import { ROUTE_CASES, ROUTE_EXAMINATIONS } from '@router/routes';
import { ErrorSnackbarComponent } from '@shared/components/error-snackbar/error-snackbar.component';
import { of } from 'rxjs';
import { MatSnackBar } from '@angular/material/snack-bar';
import { filterNullish } from '@helper/rxjs-operators';
import { DiagnosticMenuActions } from '@menu/store';
import { MatDialog } from '@angular/material/dialog';
import { ConfirmationDialogComponent } from '@shared/components/confirmation-dialog/confirmation-dialog.component';

@Injectable()
export class ExaminationsEffects {
  // clear examinations on case selection
  clearExaminations$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(CasesActions.userSelectedCase, CasesActions.navigationSelectCase),
      map(() => ExaminationsActions.clearExaminations())
    );
  });

  // load examinations on case selection
  loadOnCaseSelection$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(CasesActions.userSelectedCase, CasesActions.navigationSelectCase),
      map((action) => action.id),
      map((caseId) => ExaminationsActions.loadExaminations({ caseId }))
    );
  });

  routerSelectedExamination$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ExaminationsActions.navigationSelectExamination),
      map((action) => action.examinationId),
      filterNullish(),
      map((examinationId) =>
        ExaminationsActions.userSelectedExamination({ examinationId })
      )
    );
  });

  examinationSelectedUpdateRoute$ = createEffect(
    () => {
      return this.actions$.pipe(
        ofType(ExaminationsActions.userSelectedExamination),
        map((action) => action.examinationId),
        concatLatestFrom(() =>
          this.store
            .select(CasesSelectors.selectSelectedCaseId)
            .pipe(filterNullish())
        ),
        map(([examinationId, caseId]) =>
          this.router.navigate(
            [ROUTE_CASES, caseId, ROUTE_EXAMINATIONS, examinationId],
            { queryParamsHandling: 'merge' }
          )
        )
      );
    },
    { dispatch: false }
  );

  // reload examination list on create and close
  reloadExaminations$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(
        ExaminationsActions.createExaminationSuccess,
        ExaminationsActions.closeExaminationSuccess
      ),
      concatLatestFrom(() =>
        this.store
          .select(CasesSelectors.selectSelectedCaseId)
          .pipe(filterNullish())
      ),
      map(([, caseId]) => caseId),
      map((caseId) => ExaminationsActions.loadExaminations({ caseId }))
    );
  });

  loadExaminations$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ExaminationsActions.loadExaminations),
      fetch({
        run: (
          action: ReturnType<typeof ExaminationsActions.loadExaminations>,
          _state: ExaminationsFeature.State
        ) => {
          return this.examinationsService
            .examinationsQueryPut({
              body: {
                cases: [action.caseId],
              },
            })
            .pipe(
              map((response) => response.items),
              map((examinations) =>
                ExaminationsActions.loadExaminationsSuccess({ examinations })
              )
            );
        },
        onError: (
          _action: ReturnType<typeof ExaminationsActions.loadExaminations>,
          error
        ) => {
          return ExaminationsActions.loadExaminationsFailure({ error });
        },
      })
    );
  });

  // select preselected examination if already running
  changeExamination$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ExaminationsActions.preSelectExamination),
      map((action) => action.examinationId),
      filterNullish(),
      distinctUntilChanged(),
      concatLatestFrom(() =>
        this.store.select(VendorAppSurfacesSelectors.selectActiveAppsEntities)
      ),
      filter(
        ([examinationId, appEntities]) => !!appEntities.get(examinationId)
      ),
      map(([examinationId]) =>
        ExaminationsActions.userSelectedExamination({ examinationId })
      )
    );
  });

  // unselect preselected examination when an examination was selected
  discardPreSelectedExamination$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(
        ExaminationsActions.userSelectedExamination,
        ExaminationsActions.navigationSelectExamination
      ),
      map(() =>
        ExaminationsActions.preSelectExamination({ examinationId: undefined })
      )
    );
  });

  // add app from New Apps Panel, create new examination
  // if not already closed, otherwise just refresh the list
  addAppFromPanel$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ExaminationsActions.addAppFromPanel),
      map((action) => action.appId),
      concatLatestFrom(() =>
        this.store
          .select(CasesSelectors.selectSelectedCaseId)
          .pipe(filterNullish())
      ),
      map(([appId, caseId]) =>
        ExaminationsActions.createExamination({
          caseId,
          appId,
        })
      )
    );
  });

  createExamination$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ExaminationsActions.createExamination),
      pessimisticUpdate({
        run: (
          action: ReturnType<typeof ExaminationsActions.createExamination>,
          _state: ExaminationsFeature.State
        ) => {
          return this.examinationsService
            .examinationsPut({
              body: {
                case_id: action.caseId,
                app_id: action.appId,
              },
            })
            .pipe(
              map((examination) =>
                ExaminationsActions.createExaminationSuccess({ examination })
              )
            );
        },
        onError: (
          _action: ReturnType<typeof ExaminationsActions.createExamination>,
          error
        ) => {
          return ExaminationsActions.createExaminationFailure({ error });
        },
      })
    );
  });

  closeExamination$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ExaminationsActions.closeExamination),
      pessimisticUpdate({
        run: (
          action: ReturnType<typeof ExaminationsActions.closeExamination>,
          _state: ExaminationsFeature.State
        ) => {
          return this.examinationsService
            .examinationsExaminationIdClosePut({
              examination_id: action.examinationId,
            })
            .pipe(map(() => ExaminationsActions.closeExaminationSuccess()));
        },
        onError: (
          _action: ReturnType<typeof ExaminationsActions.closeExamination>,
          error
        ) => {
          return ExaminationsActions.closeExaminationFailure({ error });
        },
      })
    );
  });

  // remove examination id from router when the current selected examination was closed
  removeExaminationIdOnClose$ = createEffect(
    () => {
      return this.actions$.pipe(
        ofType(ExaminationsActions.closeExaminationApp),
        map((action) => action.examinationId),
        concatLatestFrom(() => [
          this.store
            .select(ExaminationsSelectors.selectSelectedId)
            .pipe(filterNullish()),
          this.store
            .select(CasesSelectors.selectSelectedCaseId)
            .pipe(filterNullish()),
        ]),
        filter(
          ([closed, examinationId]) => !!closed && closed === examinationId
        ),
        map(([_closed, _examinationId, caseId]) =>
          this.router.navigate([ROUTE_CASES, caseId], {
            queryParamsHandling: 'merge',
          })
        )
      );
    },
    { dispatch: false }
  );

  updateExaminationIdOnDiscard$ = createEffect(
    () => {
      return this.actions$.pipe(
        ofType(VendorAppSurfacesActions.discardApps),
        map((action) => action.examinationIds),
        concatLatestFrom(() => [
          this.store.select(VendorAppSurfacesSelectors.selectAllActiveAppIds),
          this.store
            .select(VendorAppSurfacesSelectors.selectFocusedAppId)
            .pipe(filterNullish()),
          this.store
            .select(CasesSelectors.selectSelectedCaseId)
            .pipe(filterNullish()),
        ]),
        filter(
          ([discardedIds, activeAppIds, focusId]) =>
            !!activeAppIds?.length &&
            !!discardedIds.find((id) => id === focusId)
        ),
        map(([, activeAppIds, _focusId, caseId]) =>
          this.router.navigate(
            [
              ROUTE_CASES,
              caseId,
              ROUTE_EXAMINATIONS,
              activeAppIds[activeAppIds.length - 1],
            ],
            { queryParamsHandling: 'merge' }
          )
        )
      );
    },
    { dispatch: false }
  );

  // clear expanded examinations when user left apps panel
  clearExpandedExaminations$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(
        DiagnosticMenuActions.openMenuMidi,
        DiagnosticMenuActions.clickMenuButton
      ),
      map((action) => action.menuItem),
      filter((id) => id !== 'Apps'),
      map(() => ExaminationsActions.collapseAllExaminations())
    );
  });

  openCloseExaminationDialog$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ExaminationsActions.openCloseExaminationDialog),
      map((action) => action.examinationId),
      exhaustMap((examinationId) =>
        this.dialog
          .open(ConfirmationDialogComponent, {
            width: DIALOG_WIDTH,
            data: {
              headlineText: CLOSE_EXAMINATION_HEADLINE,
              contentText: CLOSE_EXAMINATION_CONTENT_TEXT,
              acceptButtonText: CLOSE_EXAMINATION_ACCEPT_BUTTON,
              cancelButtonText: CLOSE_EXAMINATION_CANCEL_BUTTON,
            },
          })
          .afterClosed()
          .pipe(
            filterNullish(),
            map(() => ExaminationsActions.closeExamination({ examinationId }))
          )
      )
    );
  });

  showErrorMessage$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(
        ExaminationsActions.loadExaminationsFailure,
        ExaminationsActions.createExaminationFailure,
        ExaminationsActions.closeExaminationFailure
      ),
      exhaustMap((action) => {
        this.snackBar.openFromComponent(ErrorSnackbarComponent, {
          data: {
            titel: ExaminationErrorMap[action.type],
            error: action.error,
          },
        });
        return of({ type: 'noop' });
      })
    );
  });

  constructor(
    private readonly actions$: Actions,
    private readonly examinationsService: WbcV3ExaminationsService,
    private readonly store: Store,
    private readonly router: Router,
    private readonly snackBar: MatSnackBar,
    private readonly dialog: MatDialog
  ) {}
}
