import { createAction, props } from '@ngrx/store';
import { Examination } from '@examinations/store/examinations/examinations.models';
import { HttpErrorResponse } from '@angular/common/http';

export const loadExaminations = createAction(
  '[Examinations] Load Examinations',
  props<{ caseId: string }>()
);

export const loadExaminationsSuccess = createAction(
  '[Examinations] Load Examinations Success',
  props<{ examinations: Examination[] }>()
);

export const loadExaminationsFailure = createAction(
  '[Examinations] Load Examinations Failure',
  props<{ error: HttpErrorResponse }>()
);

export const createExamination = createAction(
  '[Examinations] Create Examination',
  props<{ caseId: string, appId: string }>()
);

export const createExaminationSuccess = createAction(
  '[Examinations] Create Examinations Success',
  props<{ examination: Examination }>()
);

export const createExaminationFailure = createAction(
  '[Examinations] Create Examinations Failure',
  props<{ error: HttpErrorResponse }>()
);

export const closeExamination = createAction(
  '[Examinations] Close Examination',
  props<{ examinationId: string }>()
);

export const closeExaminationSuccess = createAction(
  '[Examinations] Close Examination Success',
);

export const closeExaminationFailure = createAction(
  '[Examinations] Close Examination Failure',
  props<{ error: HttpErrorResponse }>()
);

export const userSelectedExamination = createAction(
  '[Examinations] User Selected Examination',
  props<{ examinationId: string }>()
);

export const navigationSelectExamination = createAction(
  '[Examinations] Navigation Select Examination',
  props<{ examinationId: string }>()
);

export const clearExaminations = createAction(
  '[Examinations] Clear Examinations',
);

export const preSelectExamination = createAction(
  '[Examinations] Pre Select Examination',
  props<{ examinationId: string | undefined }>()
);

export const closeExaminationApp = createAction(
  '[Examinations] Close Examination App',
  props<{ examinationId: string }>()
);

export const addAppFromPanel = createAction(
  '[Examinations] Add App From Panel',
  props<{ appId: string }>()
);

export const expandExamination = createAction(
  '[Examinations] Expand Examination',
  props<{ id: string }>()
);

export const collapseExamination = createAction(
  '[Examinations] Collapse Examination',
  props<{ id: string }>()
);

export const collapseAllExaminations = createAction(
  '[Examinations] Collapse All Examinations',
);

export const openCloseExaminationDialog = createAction(
  '[Examinations] Open Close Examination Dialog',
  props<{ examinationId: string }>(),
);
