import { Action, combineReducers, createFeatureSelector } from '@ngrx/store';
import * as fromExaminations from '@examinations/store/examinations/examinations.reducer';

export const EXAMINATIONS_MODULE_FEATURE_KEY = 'examinationModuleFeature';

export const selectExaminationFeatureState = createFeatureSelector<State>(
  EXAMINATIONS_MODULE_FEATURE_KEY
);

export interface State {
  [fromExaminations.EXAMINATIONS_FEATURE_KEY]: fromExaminations.State;
}

export function reducers(state: State | undefined, action: Action) {
  return combineReducers({
    [fromExaminations.EXAMINATIONS_FEATURE_KEY]: fromExaminations.reducer,
  })(state, action);
}
