import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ExaminationsEffects } from './store';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { SharedModule } from '@shared/shared.module';
import { MaterialModule } from '@material/material.module';
import { EXAMINATIONS_MODULE_FEATURE_KEY, reducers } from '@examinations/store/examinations-feature.state';

@NgModule({
  imports: [
    CommonModule,
    MaterialModule,
    SharedModule,
    StoreModule.forFeature(
      EXAMINATIONS_MODULE_FEATURE_KEY,
      reducers,
    ),
    EffectsModule.forFeature([ExaminationsEffects]),
  ],
  declarations: []
})
export class ExaminationsModule { }
