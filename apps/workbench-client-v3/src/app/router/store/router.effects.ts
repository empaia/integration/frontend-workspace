import { Injectable } from '@angular/core';
import { filterNullish } from '@helper/rxjs-operators';
import * as DiagnosticMenuActions from '@menu/store/diagnostic-menu/diagnostic-menu.actions';
import * as CasesActions from '@cases/store/cases/cases.actions';
import * as ExaminationsActions from '@examinations/store/examinations/examinations.actions';
import { createEffect, Actions, ofType, concatLatestFrom } from '@ngrx/effects';
import { routerNavigatedAction } from '@ngrx/router-store';
import { Store } from '@ngrx/store';
import { distinctUntilChanged, map } from 'rxjs/operators';
import { selectCaseIdParam, selectExaminationIdParam } from './router.selectors';

@Injectable()
export class RouterEffects {

  autoOpenCasesMenu$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(routerNavigatedAction),
      concatLatestFrom(() => [
        this.store.select(selectCaseIdParam),
        // this.store.select(selectExaminationIdParam)
      ]),
      map(([, caseId]) => caseId
        ? DiagnosticMenuActions.openMenuMidi({ menuItem: 'Apps' })
        : DiagnosticMenuActions.openMenuMax({ menuItem: 'Cases' })
      )
    );
  });

  autoOpenAppsMenu$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(routerNavigatedAction),
      concatLatestFrom(() => this.store.select(selectExaminationIdParam).pipe(filterNullish())),
      map(() => DiagnosticMenuActions.openMenuMidi({ menuItem: 'Apps' }))
    );
  });

  dispatchSelectCaseAction$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(routerNavigatedAction),
      concatLatestFrom(() => this.store.select(selectCaseIdParam)),
      map(([_, id]) => id),
      distinctUntilChanged(),
      filterNullish(),
      map(id => CasesActions.navigationSelectCase({ id })),
    );
  });

  dispatchSelectAppAction$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(routerNavigatedAction),
      concatLatestFrom(() => this.store.select(selectExaminationIdParam)),
      map(([, examinationId]) => examinationId),
      distinctUntilChanged(),
      filterNullish(),
      map(examinationId => ExaminationsActions.navigationSelectExamination({ examinationId }))
    );
  });


  constructor(
    private actions$: Actions,
    private store: Store,
  ) {}
}
