import { SettingsContainerComponent } from '@core/containers/settings-container/settings-container.component';
import { DiagnosticContainerComponent } from '@core/containers/diagnostic-container/diagnostic-container.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { StoreModule } from '@ngrx/store';
import {
  // ROUTE_APPS,
  // ROUTE_APPS_ID,
  ROUTE_CASES,
  ROUTE_CASES_ID,
  ROUTE_EXAMINATIONS,
  ROUTE_EXAMINATIONS_ID,
  ROUTE_SETTINGS,
  ROUTE_SETTINGS_PREPROCESSING,
  ROUTE_TO_CASES,
} from './routes';
import { RouterEffects, RouterFeature, RouterSerializer } from './store';
import { EffectsModule } from '@ngrx/effects';
import { StoreRouterConnectingModule } from '@ngrx/router-store';
import { ROUTER_FEATURE_KEY } from './store/router.reducer';


const routes: Routes = [
  // Diagnostic Routes
  {
    path: ROUTE_CASES,
    component: DiagnosticContainerComponent,
  },
  {
    path: ROUTE_CASES + ROUTE_CASES_ID,
    component: DiagnosticContainerComponent,
  },
  {
    path: ROUTE_TO_CASES + '/' + ROUTE_EXAMINATIONS,
    component: DiagnosticContainerComponent,
  },
  {
    path: ROUTE_TO_CASES + '/' + ROUTE_EXAMINATIONS + ROUTE_EXAMINATIONS_ID,
    component: DiagnosticContainerComponent,
  },
  // {
  //   path: ROUTE_TO_EXAMINATIONS + '/' + ROUTE_APPS,
  //   component: DiagnosticContainerComponent
  // },
  // {
  //   path: ROUTE_TO_EXAMINATIONS + '/' + ROUTE_APPS + ROUTE_APPS_ID,
  //   component: DiagnosticContainerComponent
  // },
  // Settings Routes
  {
    path: ROUTE_SETTINGS + '/' + ROUTE_SETTINGS_PREPROCESSING,
    component: SettingsContainerComponent
  },
  {
    path: ROUTE_SETTINGS,
    redirectTo: ROUTE_SETTINGS + '/' + ROUTE_SETTINGS_PREPROCESSING,
    pathMatch: 'full'
  },
  // Fallback route when no other route matches, to redirect to ROUTE_CASES
  // is use when a user is logged in and uses the base url without the hashtag (#)
  {
    path: '**',
    redirectTo: ROUTE_CASES,
    pathMatch: 'full',
  },
];

@NgModule({
  declarations: [],
  imports: [
    StoreModule.forFeature(
      RouterFeature.ROUTER_FEATURE_KEY,
      RouterFeature.reducer,
    ),
    EffectsModule.forFeature([RouterEffects]),
    StoreRouterConnectingModule.forRoot({
      serializer: RouterSerializer.MergedRouterStateSerializer,
      stateKey: ROUTER_FEATURE_KEY,
    }),
    RouterModule.forRoot(routes,  {
      initialNavigation: 'disabled',
      useHash: true,
      onSameUrlNavigation: 'reload'
    })
  ],
  exports: [RouterModule]
})
export class AppRouterModule { }
