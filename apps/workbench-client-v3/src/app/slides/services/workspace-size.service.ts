import { Injectable, Renderer2, RendererFactory2 } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class WorkspaceSizeService {
  /**
   * Workspace is the middle column between button and open menus
   * overview & toolbar are positioned in this workspace area
   */

  private renderer: Renderer2;
  constructor(
    private renderFactory: RendererFactory2,
  ) {
    this.renderer = this.renderFactory.createRenderer(null, null);
  }

  setSlideViewerOverviewPosition(domRect: DOMRect): void {
    // we only want to update the overview window position if the user has not moved the window
    const elOv = document.body.querySelector('.svm-overview');
    // get slide viewer drag n drop element
    const elDnd = document.body.querySelector('.svm-overview-drag-box');
    const elBody = document.body;

    if (elDnd && elBody && elOv) {
      const transform = getComputedStyle(elDnd).transform;
      const position = getComputedStyle(elDnd).position;

      const bodyRect = elBody.getBoundingClientRect();

      /* if drag n drop box has a transform css property it was moved by the user
       * in this case more advanced calculations would be needed to postion the
       * overview correctly
       * for now we don't want to move the overview if it was moved by the user
       */
      if (transform === 'none') {
        // if pos is 'fixed' user has changed size of overview
        if (position === 'fixed') {
          this.renderer.removeStyle(elDnd, 'left');
          const pos = bodyRect.width - domRect.right;
          this.renderer.setStyle(elDnd, 'right', pos + 'px');
        } else if (position === 'absolute') {
          const pos = bodyRect.width - domRect.right;
          this.renderer.setStyle(elOv, 'right', pos + 'px');
        }
      }
    }
  }

  toggleSlideViewerSliderAndScaleLineVisibility(show: boolean): void {
    const slider = document.body.querySelector('.svm-zoom-slider');
    const scaleLine = document.body.querySelector('.svm-scale-line');

    if (slider && scaleLine) {
      this.renderer.setStyle(slider, 'opacity', show ? '1' : '0');
      this.renderer.setStyle(scaleLine, 'opacity', show ? '1' : '0');
    }
  }
}
