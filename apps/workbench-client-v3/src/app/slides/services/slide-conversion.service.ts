import { Inject, Injectable } from '@angular/core';
import { Color, ImageInfo, Slide, TileResolver } from 'slide-viewer';
import { SlideColor, SlideInfo, SlideLevel } from '@slides/store';
import { UserService } from '@core/services/user.service';
import { HttpHeader, WsiTileRequester } from 'empaia-ui-commons';

@Injectable({
  providedIn: 'root'
})
export class SlideConversionService {
  // number of over zoom layers to be added
  private readonly overZoomLayerCount = 1;

  constructor(
    @Inject('WBS_SERVER_API_URL') private serverUrl: string,
    @Inject('API_VERSION') private apiVersion: string,
    @Inject('AUTHENTICATION_ON') private hasAuth: boolean,
    private userService: UserService,
  ) { }

  public fromWbsApiType(wbsSlide: SlideInfo): Slide {

    const { resolutions, hasArtificialLevel } = this.calcResolutions(wbsSlide.levels);
    const imageInfo = this.createImageInfo(wbsSlide, resolutions, hasArtificialLevel);

    const httpHeaders: HttpHeader[] = this.hasAuth ? [
      {
        key: 'Authorization',
        value: () => {
          const token = sessionStorage.getItem('access_token');
          return `Bearer ${token}`;
        }
      },
      {
        key: 'user-id',
        value: () => {
          const claims = JSON.parse(sessionStorage.getItem('id_token_claims_obj') as string);
          return claims['sub'];
        }
      }
    ] : [
      {
        key: 'user-id',
        value: () => this.userService.userId
      }
    ];

    return {
      slideId: wbsSlide.id,
      resolver: new TileResolver(
        new WsiTileRequester(
          wbsSlide.id,
          this.serverUrl + this.apiVersion,
          httpHeaders,
        ),
        wbsSlide.id,
        imageInfo,
        hasArtificialLevel,
      ),
      imageInfo,
    };
  }

  private createImageInfo(wbsSlide: SlideInfo, resolutions: number[], hasArtificialLevel: boolean): ImageInfo {
    const npp = this.calcNpp(wbsSlide);
    const tileSizes = this.calculateTileSizes(wbsSlide, hasArtificialLevel);

    return {
      width: wbsSlide.extent.x,
      height: wbsSlide.extent.y,
      numberOfLevels: wbsSlide.num_levels,
      tileSizes,
      npp,
      resolutions,
      numberOfOverZoomLevels: this.overZoomLayerCount,
      channels: wbsSlide.channels?.map(it => ({ color: this.convertColor(it.color), id: it.id, name: it.name })) ?? [],
      channel_depth: wbsSlide.channel_depth,
    };
  }


  private calcResolutions(wbsLevels: Array<SlideLevel>): { resolutions: Array<number>, hasArtificialLevel: boolean } {
    // add an artifical level here if our wsi has only levels with a downsample factor below 128
    let hasArtificialLevel = false;
    const resolutions = [];
    const sample_factors = [...wbsLevels.map(it => it.downsample_factor)];
    const maximum = Math.max(...sample_factors);
    const minimum = Math.min(...sample_factors);
    if (maximum < 128) {
      resolutions.push(128);
      hasArtificialLevel = true;
    }
    wbsLevels.reverse().forEach(it => resolutions.push(it.downsample_factor));
    // add an over zoom layer
    // all additional layer are dividing from the base layer (1)
    // with a base of 2
    for (let i = 0; i < this.overZoomLayerCount; i++) {
      resolutions.push(minimum / 2**(i+1));
    }
    return { resolutions, hasArtificialLevel };
  }

  private calculateTileSizes(wbsSlide: SlideInfo, hasArtificialLevel: boolean): number[][] {
    const tileSizes = new Array<number[]>(hasArtificialLevel ? wbsSlide.num_levels + 1 : wbsSlide.num_levels)
      .fill([wbsSlide.tile_extent.x, wbsSlide.tile_extent.y]);
    // the additinal layers don't have there own tiles, in order to use zoomed in tiles of the base layer
    // need bigger tiles so that the coordinates combinted with the zoom factor will call the right tiles
    // and draw a "bigger" version of the tile of the base layer
    for (let i = 0; i < this.overZoomLayerCount; i++) {
      tileSizes.push([wbsSlide.tile_extent.x * 2**(i+1), wbsSlide.tile_extent.y * 2**(i+1)]);
    }
    return tileSizes;
  }

  private calcNpp(wbsSlide: SlideInfo): number {
    return wbsSlide.pixel_size_nm.x ? wbsSlide.pixel_size_nm.x : 0;
  }

  private convertColor(color: SlideColor): Color {
    return {
      r: color.r,
      g: color.g,
      b: color.b,
      a: color.a,
    };
  }
}
