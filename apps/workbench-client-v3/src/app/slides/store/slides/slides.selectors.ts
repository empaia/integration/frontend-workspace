import { createSelector } from '@ngrx/store';
import {
  State as ModuleState,
  selectSlidesFeatureState,
} from '../slides-feature.state';
import { slidesAdapter } from '@slides/store/slides/slides.reducer';
import { SLIDES_LIMIT } from './slides.models';

const {
  selectAll,
  selectEntities,
  selectIds,
} = slidesAdapter.getSelectors();

export const selectSlidesState = createSelector(
  selectSlidesFeatureState,
  (state: ModuleState) => state.slides
);

export const selectSlidesLoaded = createSelector(
  selectSlidesState,
  (state) => state.loaded
);

export const selectSlidesError = createSelector(
  selectSlidesState,
  (state) => state.error
);

export const selectAllSlides = createSelector(
  selectSlidesState,
  selectAll
);

export const selectSlidesEntities = createSelector(
  selectSlidesState,
  selectEntities,
);

export const selectAllSlideIds = createSelector(
  selectSlidesState,
  (state) => selectIds(state) as string[]
);

export const selectSlideSkip = createSelector(
  selectSlidesState,
  (state) => state.skip
);

export const selectMaxSlidesCount = createSelector(
  selectAllSlides,
  (slides) => slides.length
);

export const selectSlideBatch = createSelector(
  selectAllSlides,
  selectSlideSkip,
  (slides, skip) => slides.slice(skip, SLIDES_LIMIT + skip)
);

export const selectSlideBatchIds = createSelector(
  selectAllSlideIds,
  selectSlideSkip,
  (slideIds, skip) => slideIds.slice(skip, SLIDES_LIMIT + skip)
);

export const selectAllSlidesDataView = createSelector(
  selectAllSlides,
  (slides) => slides.map(slide => slide.dataView)
);

export const selectSelectedSlideId = createSelector(
  selectSlidesState,
  (state) => state.selected
);

export const selectSelectedSlide = createSelector(
  selectSlidesEntities,
  selectSelectedSlideId,
  (entities, slideId) => slideId ? entities[slideId] : undefined
);
