import { Injectable } from '@angular/core';
import { Actions, concatLatestFrom, createEffect, ofType } from '@ngrx/effects';
import { fetch } from '@ngrx/router-store/data-persistence';
// import { SlidesService } from '@api/wbs-api/services/slides.service';
// import { CasesService } from '@api/wbs-api/services/cases.service';
import { WbcV3SlidesService, WbcV3CasesService } from 'empaia-api-lib';
import * as SlidesActions from './slides.actions';
import * as SlidesSelectors from './slides.selectors';
import * as CasesActions from '@cases/store/cases/cases.actions';
import * as CasesSelectors from '@cases/store/cases/cases.selectors';
import * as SlidesMenuActions from '@slides/store/slides-menu/slides-menu.actions';
import { SlideEntity, SLIDES_POLLING_PERIOD, SlidesFeature } from '..';
import { SlideConversionService } from '@slides/services/slide-conversion.service';
import { filter, map, switchMap, takeUntil } from 'rxjs/operators';
import { iif, Observable, of, timer } from 'rxjs';
import { ActionCreator, Creator, Store } from '@ngrx/store';
import { filterNullish } from '@helper/rxjs-operators';
// import { SlideList } from '@api/wbs-api/models/slide-list';
import { WbcV3SlideList } from 'empaia-api-lib';

declare type GetSlidesFN = (
  action: ReturnType<typeof SlidesActions.loadSlides>
) => Observable<WbcV3SlideList>;

@Injectable()
export class SlidesEffects {
  // clear slides when a case was selected
  clearSlides$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(CasesActions.userSelectedCase, CasesActions.navigationSelectCase),
      map(() => SlidesActions.clearSlides())
    );
  });

  // clear slides when slides-menu was closed
  clearOnMenuClose$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(SlidesMenuActions.closeSlidesPanel),
      map(() => SlidesActions.clearSlides())
    );
  });

  loadSlides$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(SlidesActions.loadSlides),
      this.loadSlideEffect(
        SlidesActions.loadSlides,
        (action: ReturnType<typeof SlidesActions.loadSlides>) => {
          return this.casesPanelService.casesCaseIdSlidesGet({
            case_id: action.caseId,
          });
        },
        true
      )
    );
  });

  caseSelectionLoadSlides$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(CasesActions.userSelectedCase, CasesActions.navigationSelectCase),
      map((action) => action.id),
      filter((id) => !!id),
      map((caseId) => SlidesActions.loadSlides({ caseId }))
    );
  });

  slideSelection$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(SlidesActions.selectSlide),
      map((action) => action.slideId),
      map((slideId) => SlidesActions.loadSelectedSlideImageInfo({ slideId }))
    );
  });

  loadSlideInfo$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(SlidesActions.loadSelectedSlideImageInfo),
      fetch({
        run: (
          action: ReturnType<typeof SlidesActions.loadSelectedSlideImageInfo>,
          _state: SlidesFeature.State
        ) => {
          return this.slidesPanelService
            .slidesSlideIdInfoGet({
              slide_id: action.slideId,
            })
            .pipe(
              map((slideInfo) =>
                this.slideConversion.fromWbsApiType(slideInfo)
              ),
              map((slideImage) =>
                SlidesActions.loadSelectedSlideImageInfoSuccess({ slideImage })
              )
            );
        },
        onError: (
          _action: ReturnType<typeof SlidesActions.loadSelectedSlideImageInfo>,
          error
        ) => {
          return SlidesActions.loadSelectedSlideImageInfoFailure({ error });
        },
      })
    );
  });

  slidesPolling$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(SlidesActions.startPollingSlides),
      switchMap(() =>
        timer(0, SLIDES_POLLING_PERIOD).pipe(
          takeUntil(
            this.actions$.pipe(ofType(SlidesActions.stopPollingSlides))
          ),
          concatLatestFrom(() =>
            this.store.select(CasesSelectors.selectSelectedCaseId)
          ),
          map(([, caseId]) => caseId),
          filterNullish(),
          map((caseId) => SlidesActions.loadSlides({ caseId }))
        )
      )
    );
  });

  startSlidesPolling$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(SlidesMenuActions.openSlidesPanel),
      map(() => SlidesActions.startPollingSlides())
    );
  });

  stopSlidesPolling$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(SlidesMenuActions.closeSlidesPanel),
      map(() => SlidesActions.stopPollingSlides())
    );
  });

  constructor(
    private readonly actions$: Actions,
    private readonly slidesPanelService: WbcV3SlidesService,
    private readonly casesPanelService: WbcV3CasesService,
    private readonly slideConversion: SlideConversionService,
    private readonly store: Store
  ) {}

  private isAnySlideAddedOrRemoved(
    newSlides: SlideEntity[],
    oldSlides: SlideEntity[]
  ): boolean {
    const oldIds = new Set(oldSlides.map((s) => s.id));
    const newIds = new Set(newSlides.map((s) => s.id));
    const slidesUnchanged =
      oldIds.size === newIds.size && [...oldIds].every((id) => newIds.has(id));
    return !slidesUnchanged;
  }

  private loadSlideEffect(
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    typedAction: ActionCreator<string, Creator<any[], any>>,
    getSlidesFN: GetSlidesFN,
    filterSlidesWithDeletedDataView: boolean
  ) {
    return fetch({
      run: (
        action: ReturnType<typeof typedAction>,
        _state: SlidesFeature.State
      ) => {
        return getSlidesFN(action).pipe(
          map((slides) => slides.items),
          map((slideViews) => [
            ...slideViews.map((v) => ({
              id: v.id as string,
              dataView: v,
              disabled: false,
            })),
          ]),
          switchMap((slides) =>
            iif(
              () => filterSlidesWithDeletedDataView,
              of(slides.filter((slide) => !slide.dataView.deleted)),
              of(slides)
            )
          ),
          concatLatestFrom(() => [
            this.store.select(SlidesSelectors.selectAllSlides),
          ]),
          switchMap(([newSlides, oldSlides]) =>
            iif(
              () => this.isAnySlideAddedOrRemoved(newSlides, oldSlides),
              of(SlidesActions.loadSlidesSuccess({ slides: newSlides })),
              of(SlidesActions.doneLoadingSlides({ loaded: true }))
            )
          )
        );
      },
      onError: (_action: ReturnType<typeof typedAction>, error) => {
        return SlidesActions.loadSlidesFailure({ error });
      },
    });
  }
}
