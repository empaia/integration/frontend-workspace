import {
  WbcV3Slide as SlideView ,
  WbcV3SlideInfo,
  WbcV3SlideLevel,
  WbcV3SlideColor,
  WbcV3TagMapping,
} from 'empaia-api-lib';
import { Slide } from 'slide-viewer';

export type SlideInfo = WbcV3SlideInfo;
export type SlideLevel = WbcV3SlideLevel;
export type SlideColor = WbcV3SlideColor;
export type TagMapping = WbcV3TagMapping;

export type SlideEntity = {
  id: string;
  dataView: SlideView;
  imageView?: Slide;
  disabled: boolean;
};

export const SLIDES_POLLING_PERIOD = 30000;
export const SLIDES_LIMIT = 20;
