import { createReducer, on } from '@ngrx/store';
import { createEntityAdapter, EntityAdapter, EntityState } from '@ngrx/entity';
import { SlideEntity } from '@slides/store/slides/slides.models';
import { HttpErrorResponse } from '@angular/common/http';
import * as SlidesActions from './slides.actions';


export const SLIDES_FEATURE_KEY = 'slides';

export interface State extends EntityState<SlideEntity> {
  loaded: boolean;
  skip: number;
  selected?: string;
  error?: HttpErrorResponse | null;
}

export const slidesAdapter: EntityAdapter<SlideEntity> = createEntityAdapter<SlideEntity>();

export const initialState: State = slidesAdapter.getInitialState({
  loaded: true,
  skip: 0,
  error: null,
  selected: undefined,
});


export const reducer = createReducer(
  initialState,
  on(SlidesActions.loadSlides, (state): State => ({
    ...state,
    loaded: false,
  })),
  on(SlidesActions.loadSlidesSuccess, (state, { slides }): State =>
    slidesAdapter.setAll(slides, {
      ...state,
      loaded: true,
    })
  ),
  on(SlidesActions.loadSlidesFailure, (state, { error }): State => ({
    ...state,
    loaded: true,
    error,
  })),
  on(SlidesActions.loadSelectedSlideImageInfo, (state): State => ({
    ...state,
    loaded: false,
  })),
  on(SlidesActions.loadSelectedSlideImageInfoSuccess, (state, { slideImage }): State =>
    slidesAdapter.updateOne({
      id: slideImage.slideId,
      changes: {
        imageView: slideImage
      }
    }, {
      ...state,
      loaded: true,
    })
  ),
  on(SlidesActions.loadSelectedSlideImageInfoFailure, (state, { error }): State => ({
    ...state,
    loaded: true,
    error,
  })),
  on(SlidesActions.setSlidePage, (state, { skip }): State => ({
    ...state,
    skip,
  })),
  on(SlidesActions.clearSlides, (state): State =>
    slidesAdapter.removeAll({
      ...state,
      ...initialState,
    })
  ),
  on(SlidesActions.selectSlide, (state, { slideId }): State => ({
    ...state,
    selected: slideId,
  })),
  on(SlidesActions.deselectSlideId, (state): State => ({
    ...state,
    selected: undefined,
  })),
  on(SlidesActions.doneLoadingSlides, (state, { loaded }): State => ({
    ...state,
    loaded,
  })),
);

