import { createAction, props } from '@ngrx/store';
import { SlideEntity } from '@slides/store/slides/slides.models';
import { HttpErrorResponse } from '@angular/common/http';
import { Slide } from 'slide-viewer';

export const loadSlides = createAction(
  '[Slides] Load Slides',
  props<{ caseId: string }>()
);

export const loadSlidesSuccess = createAction(
  '[Slides] Load Slides Success',
  props<{ slides: SlideEntity[] }>()
);

export const loadSlidesFailure = createAction(
  '[Slides] Load Slides Failure',
  props<{ error: HttpErrorResponse }>()
);

export const selectSlide = createAction(
  '[Slides] Select Slide',
  props<{ slideId: string }>()
);

export const loadSelectedSlideImageInfo = createAction(
  '[Slides] Load Selected Slide Image Info',
  props<{ slideId: string }>()
);

export const loadSelectedSlideImageInfoSuccess = createAction(
  '[Slides] Load Selected Slide Image Info Success',
  props<{ slideImage: Slide }>()
);

export const loadSelectedSlideImageInfoFailure = createAction(
  '[Slides] Load Selected Slide Image Info Failure',
  props<{ error: HttpErrorResponse }>()
);

export const setSlidePage = createAction(
  '[Slides] Set Slide Page',
  props<{ skip: number }>()
);

export const clearSlides = createAction(
  '[Slides] Clear Slides',
);

export const deselectSlideId = createAction(
  '[Slides] Deselect Slide Id',
);

export const startPollingSlides = createAction(
  '[Slides] Start Polling Slides',
);

export const stopPollingSlides = createAction(
  '[Slides] Stop Polling Slides',
);

export const doneLoadingSlides = createAction(
  '[Slides] Done Loading Slides',
  props<{ loaded: boolean }>()
);
