import { Injectable } from '@angular/core';
import { Actions, concatLatestFrom, createEffect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import * as SlidesMenuActions from './slides-menu.actions';
import * as SlidesMenuSelectors from './slides-menu.selectors';
import * as SlidesActions from '@slides/store/slides/slides.actions';
import * as CasesActions from '@cases/store/cases/cases.actions';
import * as ExaminationsActions from '@examinations/store/examinations/examinations.actions';
import * as DiagnosticMenuActions from '@menu/store/diagnostic-menu/diagnostic-menu.actions';
import * as DiagnosticMenuSelectors from '@menu/store/diagnostic-menu/diagnostic-menu.selectors';
import * as VendorAppSurfacesActions from '@apps/store/vendor-app-surfaces/vendor-app-surfaces.actions';
import * as VendorAppSurfacesSelectors from '@apps/store/vendor-app-surfaces/vendor-app-surfaces.selectors';
import { delay, filter, map } from 'rxjs/operators';
import { MenuState } from '@menu/models/menu.models';
import { WorkspaceSizeService } from '@slides/services/workspace-size.service';


@Injectable()
export class SlidesMenuEffects {

  // open slides panel after case selection
  openSlidesPanelOnCaseSelection$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(
        CasesActions.userSelectedCase,
        CasesActions.navigationSelectCase,
      ),
      map(() => SlidesMenuActions.openSlidesPanel())
    );
  });

  // open slides tray view after case selection
  openSlidesTrayViewOnCaseSelection$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(
        CasesActions.userSelectedCase,
        CasesActions.navigationSelectCase
      ),
      map(() => SlidesMenuActions.openMenuMax())
    );
  });

  // open slides panel after an app was closed
  openSlidesPanelOnAppClosure$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(VendorAppSurfacesActions.discardApps),
      concatLatestFrom(() =>
        this.store.select(VendorAppSurfacesSelectors.selectAllActiveAppIds)
      ),
      filter(([, activeAppIds]) => !activeAppIds?.length),
      map(() => SlidesMenuActions.openSlidesPanel())
    );
  });

  // close slides panel when an app was selected
  closeSlidesPanelOnAppSelection$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(
        ExaminationsActions.userSelectedExamination,
        ExaminationsActions.navigationSelectExamination,
      ),
      concatLatestFrom(() =>
        this.store.select(SlidesMenuSelectors.selectSlidesMenuPanelOpen)
      ),
      filter(([, open]) => open),
      map(() => SlidesMenuActions.closeSlidesPanel())
    );
  });

  // when selecting a slide in tray-view, set label and content
  // to midi view and
  // when hiding the slides menu, set label and content
  // back to midi view
  slideSelection$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(
        SlidesActions.selectSlide,
        SlidesMenuActions.toggleMenu
      ),
      concatLatestFrom(() =>
        this.store.select(SlidesMenuSelectors.selectSlidesMenu)
      ),
      map(([, menu]) => menu),
      filter(menu => menu.labelState === MenuState.MAX),
      map(() => SlidesMenuActions.openMenu())
    );
  });

  // toggle slides-menu after double-click on the slide-viewer
  toggleSlidesMenu$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(SlidesMenuActions.toggleAllMenus),
      map(() => SlidesMenuActions.toggleMenu())
    );
  });

  // toggle main menu after double-click on the slide-viewer
  toggleMainMenu$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(SlidesMenuActions.toggleAllMenus),
      concatLatestFrom(() =>
        this.store.select(SlidesMenuSelectors.selectSlidesMenuVisibilityState)
      ),
      map(([, slideMenu]) => slideMenu),
      map(slideMenu => DiagnosticMenuActions.toggleMenu({ minimizeAll: slideMenu }))
    );
  });

  // hide svm scale line and slider when tray-view is displayed
  // when not set them back to visible
  hideViewerScaleLineAndSlider$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(
        SlidesMenuActions.openMenu,
        SlidesMenuActions.openMenuMax
      ),
      concatLatestFrom(() =>
        this.store.select(SlidesMenuSelectors.selectSlidesMenu)
      ),
      map(([, menu]) => menu),
      map(menu =>
        this.workspaceSizeService.toggleSlideViewerSliderAndScaleLineVisibility(menu.labelState !== MenuState.MAX)
      )
    );
  }, { dispatch: false });

  // add left menu size for calculation of right max menu
  addLeftMenuSize$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(SlidesMenuActions.openMenuMax),
      // delay opening of the slide view tray, to avoid getting an
      // old state of the left nav menu size
      delay(10),
      concatLatestFrom(() =>
        this.store.select(DiagnosticMenuSelectors.selectSideNavSize)
      ),
      map(([, leftMenuSize]) => leftMenuSize),
      map(leftMenuSize => SlidesMenuActions.openMenuMaxReady({ leftMenuSize }))
    );
  });

  // change right max menu when left menu toggles (only if max menu is open)
  onLeftMenuToggle$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(DiagnosticMenuActions.toggleMenu),
      concatLatestFrom(() => [
        this.store.select(SlidesMenuSelectors.selectSlidesMenu),
        this.store.select(DiagnosticMenuSelectors.selectSideNavSize)
      ]),
      filter(([, menu]) => menu.labelState === MenuState.MAX),
      map(([, _menu, leftMenuSize]) => SlidesMenuActions.openMenuMaxReady({ leftMenuSize }))
    );
  });

  constructor(
    private actions$: Actions,
    private store: Store,
    private workspaceSizeService: WorkspaceSizeService,
  ) {}

}
