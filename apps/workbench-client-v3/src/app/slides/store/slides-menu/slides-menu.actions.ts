import { createAction, props } from '@ngrx/store';

export const showLabel = createAction(
  '[SlidesMenu] Show Label',
);

export const openMenu = createAction(
  '[SlidesMenu] Open Menu',
);

export const openMenuMax = createAction(
  '[SlidesMenu] Open Menu Max',
);

export const openMenuMaxReady = createAction(
  '[SlidesMenu] Open Menu Max Ready',
  props<{ leftMenuSize: number }>()
);

export const toggleMenu = createAction(
  '[SlidesMenu] Show Menu',
);

export const toggleAllMenus = createAction(
  '[SlidesMenu] Toggle All Menus',
);

export const openSlidesPanel = createAction(
  '[SlidesMenu] Open Slides Panel',
);

export const closeSlidesPanel = createAction(
  '[SlidesMenu] Close Slides Panel',
);
