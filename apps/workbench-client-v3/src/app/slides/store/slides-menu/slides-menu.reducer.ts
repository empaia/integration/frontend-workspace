import { createReducer } from '@ngrx/store';
import { immerOn } from 'ngrx-immer/store';
import { MAX_SIZE, DIAGNOSTIC_MENU, MenuEntity, MenuState, MENU_BUTTON_WIDTH, MIDI_SIZE, MIN_SIZE } from '@menu/models/menu.models';
import * as SlideMenuActions from './slides-menu.actions';
import { calculateMaxSize, convertPxToVw } from '@helper/dimensional-style-calculation';


export const SLIDE_MENU_FEATURE_KEY = 'slidesMenu';

export interface State {
  slideMenu: MenuEntity;
  visible: boolean;
  panelOpened: boolean;
  slideMenuSize: number;
}

export const initialState: State = {
  slideMenu: DIAGNOSTIC_MENU.Slides,
  visible: true,
  panelOpened: false,
  slideMenuSize: MIDI_SIZE,
};


export const reducer = createReducer(
  initialState,
  immerOn(SlideMenuActions.openMenu, (state) => {
    openItem(MenuState.MIDI, state.slideMenu);
    state.slideMenuSize = Math.max(MIDI_SIZE, convertPxToVw(MIN_SIZE));
  }),
  immerOn(SlideMenuActions.openMenuMaxReady, (state, { leftMenuSize }) => {
    openItem(MenuState.MAX, state.slideMenu);
    state.slideMenuSize = calculateMaxSize(MAX_SIZE, [leftMenuSize, convertPxToVw(MENU_BUTTON_WIDTH) * 2]);
  }),
  immerOn(SlideMenuActions.toggleMenu, (state) => {
    state.visible = !state.visible;
  }),
  immerOn(SlideMenuActions.showLabel, (state) => {
    state.slideMenu.labelState = MenuState.MIDI;
  }),
  immerOn(SlideMenuActions.openSlidesPanel, (state) => {
    state.panelOpened = true;
  }),
  immerOn(SlideMenuActions.closeSlidesPanel, (state) => {
    state.panelOpened = false;
  })
);

function openItem(state: MenuState, menu: MenuEntity) {
  menu.selected = true;
  menu.labelState = state;
  menu.contentState = state;
}
