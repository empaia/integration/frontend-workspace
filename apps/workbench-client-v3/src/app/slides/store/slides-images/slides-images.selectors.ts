import { createSelector } from '@ngrx/store';
import { slidesImagesAdapter } from '@slides/store/slides-images/slides-images.reducer';
import {
  State as ModuleState,
  selectSlidesFeatureState,
} from '../slides-feature.state';

const {
  selectAll,
  selectEntities,
} = slidesImagesAdapter.getSelectors();

export const selectSlidesImagesState = createSelector(
  selectSlidesFeatureState,
  (state: ModuleState) => state.slidesImages
);

export const selectAllSlidesImages = createSelector(
  selectSlidesImagesState,
  (state) => selectAll(state)
);

export const selectSlidesImagesEntities = createSelector(
  selectSlidesImagesState,
  (state) => selectEntities(state)
);

export const selectSlidesImagesError = createSelector(
  selectSlidesImagesState,
  (state) => state.error
);
