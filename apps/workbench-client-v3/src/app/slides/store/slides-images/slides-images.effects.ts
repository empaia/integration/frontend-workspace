import { Injectable } from '@angular/core';
import { Actions, concatLatestFrom, createEffect, ofType } from '@ngrx/effects';
import { WbcV3SlidesService } from 'empaia-api-lib';
import { fetch } from '@ngrx/router-store/data-persistence';
import { Store } from '@ngrx/store';
import * as SlidesImagesActions from './slides-images.actions';
import * as SlidesImagesSelectors from './slides-images.selectors';
import * as CasesActions from '@cases/store/cases/cases.actions';
import * as SlidesActions from '@slides/store/slides/slides.actions';
import * as SlidesMenuActions from '@slides/store/slides-menu/slides-menu.actions';
import {
  MAX_SLIDE_THUMBNAIL_HEIGHT,
  MAX_SLIDE_THUMBNAIL_WIDTH,
  SLIDE_IMAGE_CONCURRENCY,
  SLIDE_PAGINATION_DEBOUNCE_TIME,
  SLIDE_THUMBNAIL_QUALITY,
  SlideImage,
  SlideImageFormat,
  SlideImageStatus,
} from '@slides/store/slides-images/slides-images.models';
import { SlidesImagesFeature, SlidesSelectors } from '..';
import { debounceTime, filter, map, mergeMap, takeUntil } from 'rxjs/operators';
import { filterNullish } from '@helper/rxjs-operators';

@Injectable()
export class SlidesImagesEffects {
  // clear images when the case was changed
  clearOnCaseChange$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(CasesActions.userSelectedCase, CasesActions.navigationSelectCase),
      map(() => SlidesImagesActions.clearImages())
    );
  });

  // clear images when slides-menu was closed
  clearOnMenuClose$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(SlidesMenuActions.closeSlidesPanel),
      map(() => SlidesImagesActions.clearImages())
    );
  });

  // prepare clear slide image. Revoke all stored urls to set the
  // allocated memory free
  prepareClearImages$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(SlidesImagesActions.clearImages),
      concatLatestFrom(() =>
        this.store.select(SlidesImagesSelectors.selectAllSlidesImages)
      ),
      map(([, images]) => images),
      map((images) =>
        images.forEach((img) => {
          if (img.label) {
            URL.revokeObjectURL(img.label);
          }
          if (img.thumbnail) {
            URL.revokeObjectURL(img.thumbnail);
          }
        })
      ),
      map(() => SlidesImagesActions.clearImagesReady())
    );
  });

  // start loading labels and thumbnails, if slides are loaded
  // or a new pagination page was selected
  startLoadingImages$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(SlidesActions.loadSlidesSuccess, SlidesActions.setSlidePage),
      debounceTime(SLIDE_PAGINATION_DEBOUNCE_TIME),
      concatLatestFrom(() => [
        this.store
          .select(SlidesSelectors.selectSlideBatchIds)
          .pipe(filterNullish()),
        this.store.select(SlidesImagesSelectors.selectSlidesImagesEntities),
      ]),
      // don't refetch images, only load images that are not in memory
      // or doesn't have labels or thumbnails, due to the fact that
      // label and thumbnail request can be aborted
      map(([, slideIds, entities]) =>
        slideIds.filter(
          (slideId) =>
            !entities[slideId] ||
            entities[slideId]?.thumbnailStatus === SlideImageStatus.LOADING ||
            entities[slideId]?.labelStatus === SlideImageStatus.LOADING
        )
      ),
      filter((slideIds) => !!slideIds?.length),
      map((slideIds) =>
        SlidesImagesActions.loadManySlidesLabelsAndThumbnails({
          slideIds,
          maxWidth: MAX_SLIDE_THUMBNAIL_WIDTH,
          maxHeight: MAX_SLIDE_THUMBNAIL_HEIGHT,
          imageFormat: SlideImageFormat.JPEG,
          imageQuality: SLIDE_THUMBNAIL_QUALITY,
        })
      )
    );
  });

  loadSlideLabel$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(SlidesImagesActions.loadSlideLabel),
      fetch({
        id: (
          action: ReturnType<typeof SlidesImagesActions.loadSlideLabel>,
          _state: SlidesImagesFeature.State
        ) => {
          return action.slideId + ' ' + action.type;
        },
        run: (
          action: ReturnType<typeof SlidesImagesActions.loadSlideLabel>,
          _state: SlidesImagesFeature.State
        ) => {
          return this.slidesPanelService
            .slidesSlideIdLabelMaxSizeMaxXMaxYGet({
              slide_id: action.slideId,
              max_x: action.maxWidth,
              max_y: action.maxHeight,
              image_format: action.imageFormat,
              image_quality: action.imageQuality,
            })
            .pipe(
              takeUntil(this.actions$.pipe(ofType(SlidesActions.setSlidePage))),
              map((img) => URL.createObjectURL(img)),
              map((img) => {
                const slideImage: SlideImage = {
                  id: action.slideId,
                  labelStatus: SlideImageStatus.LOADED,
                  label: img,
                };
                return slideImage;
              }),
              map((slideImage) =>
                SlidesImagesActions.loadSlideLabelSuccess({ slideImage })
              )
            );
        },
        onError: (
          action: ReturnType<typeof SlidesImagesActions.loadSlideLabel>,
          error
        ) => {
          return SlidesImagesActions.loadSlideLabelFailure({
            slideId: action.slideId,
            error,
          });
        },
      })
    );
  });

  loadSlideThumbnail$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(SlidesImagesActions.loadSlideThumbnail),
      fetch({
        id: (
          action: ReturnType<typeof SlidesImagesActions.loadSlideThumbnail>,
          _state: SlidesImagesFeature.State
        ) => {
          return action.slideId + ' ' + action.type;
        },
        run: (
          action: ReturnType<typeof SlidesImagesActions.loadSlideThumbnail>,
          _state: SlidesImagesFeature.State
        ) => {
          return this.slidesPanelService
            .slidesSlideIdThumbnailMaxSizeMaxXMaxYGet({
              slide_id: action.slideId,
              max_x: action.maxWidth,
              max_y: action.maxHeight,
              image_format: action.imageFormat,
              image_quality: action.imageQuality,
            })
            .pipe(
              takeUntil(this.actions$.pipe(ofType(SlidesActions.setSlidePage))),
              map((img) => URL.createObjectURL(img)),
              map((img) => {
                const slideImage: SlideImage = {
                  id: action.slideId,
                  thumbnailStatus: SlideImageStatus.LOADED,
                  thumbnail: img,
                };
                return slideImage;
              }),
              map((slideImage) =>
                SlidesImagesActions.loadSlideThumbnailSuccess({ slideImage })
              )
            );
        },
        onError: (
          action: ReturnType<typeof SlidesImagesActions.loadSlideThumbnail>,
          error
        ) => {
          return SlidesImagesActions.loadSlideThumbnailFailure({
            slideId: action.slideId,
            error,
          });
        },
      })
    );
  });

  loadManySlidesLabels$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(SlidesImagesActions.loadManySlidesLabels),
      mergeMap(
        (action) =>
          action.slideIds
            .filter((slideId) => !!slideId)
            .map((slideId) =>
              SlidesImagesActions.loadSlideLabel({
                slideId,
                maxWidth: action.maxWidth,
                maxHeight: action.maxHeight,
                imageFormat: action.imageFormat,
                imageQuality: action.imageQuality,
              })
            ),
        SLIDE_IMAGE_CONCURRENCY
      )
    );
  });

  loadManySlidesThumbnails$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(SlidesImagesActions.loadManySlidesThumbnails),
      mergeMap(
        (action) =>
          action.slideIds
            .filter((slideId) => !!slideId)
            .map((slideId) =>
              SlidesImagesActions.loadSlideThumbnail({
                slideId,
                maxWidth: action.maxWidth,
                maxHeight: action.maxHeight,
                imageFormat: action.imageFormat,
                imageQuality: action.imageQuality,
              })
            ),
        SLIDE_IMAGE_CONCURRENCY
      )
    );
  });

  loadManySlidesLabelsAndThumbnails$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(SlidesImagesActions.loadManySlidesLabelsAndThumbnails),
      mergeMap((action) => [
        SlidesImagesActions.loadManySlidesLabels({ ...action }),
        SlidesImagesActions.loadManySlidesThumbnails({ ...action }),
      ])
    );
  });

  constructor(
    private actions$: Actions,
    private slidesPanelService: WbcV3SlidesService,
    private store: Store
  ) {}
}
