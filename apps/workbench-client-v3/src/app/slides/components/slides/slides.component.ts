import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { SlideEntity, SlideImage } from '@slides/store';
import { IdSelection, SelectionError } from '@menu/models/ui.models';
import { DiagnosticMenuItem } from '@menu/models/menu.models';
import { MatSelectionListChange } from '@angular/material/list';
import { Dictionary } from '@ngrx/entity';

@Component({
  selector: 'app-slides',
  templateUrl: './slides.component.html',
  styleUrls: ['./slides.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SlidesComponent {
  @Input() public slideEntities!: SlideEntity[];
  @Input() public slideImages!: Dictionary<SlideImage>;
  @Input() public selectedSlideId!: string;
  @Input() public selectionMissing!: SelectionError;
  @Input() public contentLoaded!: boolean;
  @Input() public slidesSkip!: number;
  @Input() public slidesLimit!: number;
  @Input() public maxSlidesCount!: number;

  @Output() public slideSelected = new EventEmitter<IdSelection>();
  @Output() public errorClicked = new EventEmitter<DiagnosticMenuItem>();
  @Output() public loadNewSlidesPage = new EventEmitter<number>();

  public selectionListChanged(event: MatSelectionListChange): void {
    const id = event.source.selectedOptions.selected[0].value;
    this.slideSelected.emit({ id });
  }

  public onMissingSelectionErrorClick(menuItem: DiagnosticMenuItem): void {
    this.errorClicked.emit(menuItem);
  }
}
