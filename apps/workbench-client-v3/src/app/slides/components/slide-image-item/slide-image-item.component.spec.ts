import { SlideImageItemComponent } from './slide-image-item.component';
import { createComponentFactory, Spectator } from '@ngneat/spectator';
import { LocalSafeUrlPipe } from '@shared/pipes/local-safe-url.pipe';

describe('SlideImageItemComponent', () => {
  let spectator: Spectator<SlideImageItemComponent>;
  const createComponent = createComponentFactory({
    component: SlideImageItemComponent,
    declarations: [
      LocalSafeUrlPipe,
    ]
  });

  beforeEach(() => spectator = createComponent());

  it('should create', () => {
    expect(spectator.component).toBeTruthy();
  });
});
