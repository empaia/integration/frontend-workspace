import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { MenuState } from '@menu/models/menu.models';

@Component({
  selector: 'app-slides-label',
  templateUrl: './slides-label.component.html',
  styleUrls: ['./slides-label.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SlidesLabelComponent {
  @Input() public selected!: boolean;
  @Input() public labelState!: MenuState;
  @Input() public labelId!: string;
  @Input() public displayId!: string;
  @Input() public contentLoaded = true;

  readonly MENU_STATE = MenuState;

  @Output() public toggleTrayViewClick = new EventEmitter<MouseEvent>();
}
