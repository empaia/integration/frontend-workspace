import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { SlideEntity, SlideImage } from '@slides/store';
import { IdSelection, SelectionError } from '@menu/models/ui.models';
import { Dictionary } from '@ngrx/entity';
import { WbcV3Slide } from 'empaia-api-lib';

@Component({
  selector: 'app-slides-tray-view',
  templateUrl: './slides-tray-view.component.html',
  styleUrls: ['./slides-tray-view.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SlidesTrayViewComponent {
  @Input() public slideEntities!: SlideEntity[];
  @Input() public selectedSlideId!: string;
  @Input() public slideImages!: Dictionary<SlideImage>;
  @Input() public selectionMissing!: SelectionError;
  @Input() public contentLoaded!: boolean;
  @Input() public slidesSkip!: number;
  @Input() public slidesLimit!: number;
  @Input() public maxSlidesCount!: number;

  @Output() public slideSelected = new EventEmitter<IdSelection>();
  @Output() public loadNewSlidesPage = new EventEmitter<number>();

  public selectionListChanged(id: string): void {
    this.slideSelected.emit({ id });
  }

  public trackBySlideId(slide: WbcV3Slide): string {
    return slide.id;
  }
}
