import { SlidesTrayViewComponent } from './slides-tray-view.component';
import { createComponentFactory, Spectator } from '@ngneat/spectator';
import { MaterialModule } from '@material/material.module';
import { SlideTrayItemComponent } from '@slides/components/slide-tray-item/slide-tray-item.component';
import { CopyToClipboardDirective } from '@shared/directives/copy-to-clipboard.directive';
import { MockComponents } from 'ng-mocks';
import {
  MissingSelectionErrorComponent
} from '@shared/components/missing-selection-error/missing-selection-error.component';

describe('SlidesTrayViewComponent', () => {
  let spectator: Spectator<SlidesTrayViewComponent>;
  const createComponent = createComponentFactory({
    component: SlidesTrayViewComponent,
    imports: [
      MaterialModule,
    ],
    declarations: [
      CopyToClipboardDirective,
      MockComponents(
        SlideTrayItemComponent,
        MissingSelectionErrorComponent,
      )
    ]
  });

  beforeEach(() => spectator = createComponent({
    props: {
      slideEntities: []
    }
  }));

  it('should create', () => {
    expect(spectator.component).toBeTruthy();
  });
});
