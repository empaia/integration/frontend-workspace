import { CasesContainerComponent } from './cases-container.component';
import { createComponentFactory, Spectator } from '@ngneat/spectator';
import { MockComponents } from 'ng-mocks';
import { CasesListMaxComponent } from '@cases/components/cases-list-max/cases-list-max.component';
import { provideMockStore } from '@ngrx/store/testing';
import { CasesSelectors } from '@cases/store';
import { DiagnosticMenuSelectors } from '@menu/store';
import { MenuItemLayoutComponent } from '@shared/containers/menu-item-layout/menu-item-layout.component';
import { CasesLabelComponent } from '@cases/components/cases-label/cases-label.component';


describe('CasesContainerComponent', () => {
  let spectator: Spectator<CasesContainerComponent>;
  const createComponent = createComponentFactory({
    component: CasesContainerComponent,
    declarations: [
      MockComponents(
        MenuItemLayoutComponent,
        CasesLabelComponent,
        CasesListMaxComponent
      )
    ],
    providers: [
      provideMockStore({
        selectors: [
          {
            selector: CasesSelectors.selectAllCases,
            value: []
          },
          {
            selector: CasesSelectors.selectSelectedCaseId,
            value: ''
          },
          {
            selector: CasesSelectors.selectExpandedCaseEntities,
            value: {}
          },
          {
            selector: DiagnosticMenuSelectors.selectCasesMenu,
            value: {}
          },
          {
            selector: CasesSelectors.selectSelectedCase,
            value: {}
          },
          {
            selector: CasesSelectors.selectCasesLoaded,
            value: true
          }
        ]
      })
    ]
  });

  beforeEach(() => spectator = createComponent());

  it('should create', () => {
    expect(spectator.component).toBeTruthy();
  });
});
