import { createAction, props } from '@ngrx/store';
import { WbcV3Case } from 'empaia-api-lib';
import { HttpResponseError } from '@menu/models/ui.models';

export const loadCasePage = createAction(
  '[Cases] Load Case Page',
  props<{ skip: number }>()
);

export const loadCasePageSuccess = createAction(
  '[Cases] Load Case Page Success',
  props<{ cases: WbcV3Case[], casesCount: number }>()
);

export const loadCasePageFailure = createAction(
  '[Cases] Load Case Page Failure',
  props<{ error:  HttpResponseError }>()
);

export const userSelectedCase = createAction(
  '[Cases] User Selects Case',
  props<{ id: string }>()
);

export const navigationSelectCase = createAction(
  '[Cases] Navigation Case Selection',
  props<{ id: string }>()
);

export const startPollingCases = createAction(
  '[Cases] Start Polling Cases',
);

export const stopPollingCases = createAction(
  '[Cases] Stop Polling Cases',
);

export const expandCase = createAction(
  '[Cases] Expand Case',
  props<{ id: string }>()
);

export const collapseCase = createAction(
  '[Cases] Collapse Case',
  props<{ id: string }>()
);

export const collapseAllCases = createAction(
  '[Cases] Collapse All Cases',
);
