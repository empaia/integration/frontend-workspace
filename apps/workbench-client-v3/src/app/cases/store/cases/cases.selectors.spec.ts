import { Case } from './cases.models';
import { casesAdapter, initialState } from './cases.reducer';
import * as CasesSelectors from './cases.selectors';

describe('Cases Selectors', () => {
  const ERROR_MSG = 'No Error Available';
  const getCasesId = (c: Case) => c.id;
  const createCasesEntity = (id: string, name = '') =>
    ({
      id: id,
      name: name || `name-${id}`,
      blocks: [''],
      created_at: 0,
      creator_id: '',
      creator_type: 'user',
      slides_count: 0,
      updated_at: 0,
      stains:  {},
      tissues: {},
      examinations: [],
      examinations_count: 0,
      preprocessing_progress: 'none',
      deleted: null,
      description: null,
      local_id: null,
      mds_url: null,
    } as Case);

  // eslint-disable-next-line @typescript-eslint/ban-types
  let state: object;

  beforeEach(() => {
    state = {
      casesModuleFeature: {
        cases: casesAdapter.setAll(
          [
            createCasesEntity('PRODUCT-AAA'),
            createCasesEntity('PRODUCT-BBB'),
            createCasesEntity('PRODUCT-CCC'),
          ],
          {
            ...initialState,
            selectedId: 'PRODUCT-BBB',
            error: ERROR_MSG,
            loaded: true,
          }
        ),
      }
    };
  });

  describe('Cases Selectors', () => {
    it('todo: get cases', () => {
      expect(true).toBeTruthy();
    });
    it('getAllCases() should return the list of Cases', () => {
      const results = CasesSelectors.selectAllCases(state);
      const selId = getCasesId(results[1]);

      expect(results.length).toBe(3);
      expect(selId).toBe('PRODUCT-BBB');
    });

    /*it('getSelected() should return the selected Entity', () => {
      const result = CasesSelectors.selectSelectedCase(state);
      const selId = getCasesId(result);

      expect(selId).toBe('PRODUCT-BBB');
    });*/

    it("getCasesLoaded() should return the current 'loaded' status", () => {
      const result = CasesSelectors.selectCasesLoaded(state);

      expect(result).toBe(true);
    });

    it("getCasesError() should return the current 'error' state", () => {
      const result = CasesSelectors.selectCasesError(state);

      expect(result).toBe(ERROR_MSG);
    });
  });
});
