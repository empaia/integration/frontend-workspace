import { WbcV3Case } from 'empaia-api-lib';

export const CASE_POLLING_PERIOD = 30000;
export const CASE_LIST_LIMIT = 50;

export const MAX_CASE_ID_LENGTH = 20;
export const MAX_CASE_DESCRIPTION_LENGTH = 60;
export const MAX_CASE_TISSUE_LENGTH = 28;

export interface CaseExpansion {
  id: string;
}

export type Case = WbcV3Case;
