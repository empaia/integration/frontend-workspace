import { CaseItemComponent } from './case-item.component';
import { createComponentFactory, Spectator } from '@ngneat/spectator';
import { MaterialModule } from '@material/material.module';
import { DictParserPipe } from '@shared/pipes/dict-parser.pipe';
import { MockComponents } from 'ng-mocks';
import { ExpansionItemComponent } from '@shared/components/expansion-item/expansion-item.component';
import { ExpansionButtonComponent } from '@shared/components/expansion-button/expansion-button.component';
import { CustomDatePipe } from '@shared/pipes/custom-date.pipe';
import { MatSelectionList } from '@angular/material/list';
import { CopyToClipboardDirective } from '@shared/directives/copy-to-clipboard.directive';
import { WbcV3PreprocessingProgress } from 'empaia-api-lib';

describe('CaseItemComponent', () => {
  let spectator: Spectator<CaseItemComponent>;
  const createComponent = createComponentFactory({
    component: CaseItemComponent,
    imports: [
      MaterialModule,
    ],
    providers: [
      DictParserPipe,
      MatSelectionList,
    ],
    declarations: [
      MockComponents(
        ExpansionItemComponent,
        ExpansionButtonComponent,
      ),
      CustomDatePipe,
      DictParserPipe,
      CopyToClipboardDirective,
    ]
  });

  beforeEach(() => spectator = createComponent({
    props: {
      caseItem: {
        id: 'TEST_CASE_ID',
        blocks: [],
        creator_id: 'TEST_CREATOR_ID',
        created_at: 1661499468427,
        updated_at: 1661499468427,
        creator_type: 'user',
        slides_count: 0,
        stains: {},
        tissues: {},
        examinations: [],
        preprocessing_progress: WbcV3PreprocessingProgress.None,
        deleted: null,
        description: null,
        local_id: null,
        mds_url: null,
      },
    }
  }));

  it('should create', () => {
    expect(spectator.component).toBeTruthy();
  });
});
