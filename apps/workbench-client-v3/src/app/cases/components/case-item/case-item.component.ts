import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnChanges,
  Output,
  SimpleChanges,
  ViewChild
} from '@angular/core';
import { Case } from '@cases/store';

@Component({
  selector: 'app-case-item',
  templateUrl: './case-item.component.html',
  styleUrls: ['./case-item.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CaseItemComponent implements OnChanges, AfterViewInit {
  @ViewChild('caseId') private caseIdSpan!: ElementRef;
  @ViewChild('caseTissue') private caseTissue!: ElementRef;
  @ViewChild('caseDescription') private caseDescriptionSpan!: ElementRef;

  public disabled = true;

  @Input() public caseItem!: Case;
  @Input() public selected!: boolean;
  @Input() public caseExtended!: boolean;

  @Output() public selectCase = new EventEmitter<string>();
  @Output() public expandCase = new EventEmitter<string>();
  @Output() public collapseCase = new EventEmitter<string>();

  constructor(
    private changeRef: ChangeDetectorRef,
  ) { }

  public ngOnChanges(changes: SimpleChanges): void {
    if (changes.caseItem) {
      this.disabled = this.isDisabled();
    }
  }

  public ngAfterViewInit(): void {
    this.disabled = this.isDisabled();
    // Trigger change detection to inform the view and it's
    // children to update (is especially needed for the expansion button)
    this.changeRef.detectChanges();
  }

  private isDisabled(): boolean {
    return (this.caseIdSpan && this.caseTissue && this.caseDescriptionSpan)
      ? !(
        this.isEllipsisActive(this.caseIdSpan.nativeElement)
        || this.isEllipsisActive(this.caseTissue.nativeElement)
        || this.isEllipsisActive(this.caseDescriptionSpan.nativeElement)
      )
      : true;
  }

  private isEllipsisActive(element: HTMLSpanElement): boolean {
    // the offset height is 1px smaller than the scrollHeight
    // so add one to offset height and check if the text
    // was truncate by css
    return element.offsetHeight + 1 < element.scrollHeight;
  }

  public onExpandCaseClick(expanded: boolean, id: string): void {
    if (expanded) {
      this.expandCase.emit(id);
    } else {
      this.collapseCase.emit(id);
    }
  }

  public onCaseClicked(): void {
    this.selectCase.emit(this.caseItem.id);
  }
}
