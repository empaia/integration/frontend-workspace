import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { Dictionary } from '@ngrx/entity';
import { Case, CaseExpansion } from '@cases/store';

@Component({
  selector: 'app-cases-list-max',
  templateUrl: './cases-list-max.component.html',
  styleUrls: ['./cases-list-max.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CasesListMaxComponent {

  @Input() public casesList!: Case[];
  @Input() public selectedCaseId: string | undefined;
  @Input() public caseExpandedDictionary!: Dictionary<CaseExpansion>;
  @Input() public casesCount!: number;
  @Input() public casesSkip!: number;
  @Input() public casesLimit!: number;

  @Output() public selectCase = new EventEmitter<string>();
  @Output() public expandCase = new EventEmitter<string>();
  @Output() public collapseCase = new EventEmitter<string>();
  @Output() public loadNewCasePage = new EventEmitter<number>();
}
