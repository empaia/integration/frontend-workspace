import { CasesLabelComponent } from './cases-label.component';
import { createComponentFactory, Spectator } from '@ngneat/spectator';
import { MockComponents } from 'ng-mocks';
import { MenuLabelComponent } from '@shared/components/menu-label/menu-label.component';
import { MaterialModule } from '@material/material.module';

describe('CasesLabelComponent', () => {
  let spectator: Spectator<CasesLabelComponent>;
  const createComponent = createComponentFactory({
    component: CasesLabelComponent,
    imports: [
      MaterialModule,
    ],
    declarations: [
      MockComponents(
        MenuLabelComponent,
      )
    ]
  });

  beforeEach(() => spectator = createComponent());

  it('should create', () => {
    expect(spectator.component).toBeTruthy();
  });
});
