import { HttpErrorResponse } from "@angular/common/http";
import { environment } from '@env/environment';

export interface UserData {
  userId: string | undefined;
  userName: string | undefined;
}

export interface HttpError extends HttpErrorResponse {
  error: {
    detail: string;
  }
}

export interface UserProfileEntity {
  profile_picture_url?: string,
  resized_profile_picture_urls?: {
    w60?: string;
    w400?: string;
    w800?: string;
    w1200?: string;
  }
}

export const WRONG_ORGANIZATION_ERROR_MESSAGE = `User is not allowed to access data of the organization ${environment.organizationName.length ? environment.organizationName : 'EMPAIA'}. Please join the organization before login.`;

export const WRONG_ORGANIZATION_ERROR = 'API access denied.';
