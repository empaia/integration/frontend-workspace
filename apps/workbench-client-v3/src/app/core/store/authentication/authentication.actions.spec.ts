import * as fromAuthentication from './authentication.actions';

describe('Authentications', () => {
  it('should return an action', () => {
    expect(fromAuthentication.setIsAuthenticated().type).toBe('[Authentication] Set Is Authenticated');
  });
});
