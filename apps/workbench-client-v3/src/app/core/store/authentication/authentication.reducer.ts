import { Action, ActionReducer, createReducer, INIT, on, UPDATE } from '@ngrx/store';
import { HttpError, UserData } from '@core/store/authentication/authentication.model';
import * as AuthenticationActions from './authentication.actions';


export const AUTHENTICATION_FEATURE_KEY = 'authentication';

export interface State {
  isAuthenticated: boolean;
  isRedirecting: boolean;
  userData: UserData | undefined;
  userProfilePicture: string | undefined;
  error?: HttpError | undefined;
}

export const initialState: State = {
  isAuthenticated: false,
  isRedirecting: false,
  userData: undefined,
  userProfilePicture: undefined,
  error: undefined,
};

const authReducer = createReducer(
  initialState,
  on(AuthenticationActions.setIsAuthenticated, (state): State => ({
    ...state,
    isAuthenticated: true,
    error: undefined,
  })),
  on(AuthenticationActions.setIsNotAuthenticated, (state): State => ({
    ...state,
    isAuthenticated: false,
  })),
  on(AuthenticationActions.setUserId, (state, { userId }): State => ({
    ...state,
    userData: {
      userId: userId,
      userName: 'Testuser'
    },
  })),
  on(AuthenticationActions.setUserData, (state, { userData }): State => ({
    ...state,
    isAuthenticated: true,
    userData,
  })),
  on(AuthenticationActions.loginUser, (state): State => ({
    ...state,
  })),
  on(AuthenticationActions.logoutUser, (state): State => ({
    ...state,
    ...initialState,
  })),
  on(AuthenticationActions.setIsRedirecting, (state, { isRedirecting }): State => ({
    ...state,
    isRedirecting,
  })),
  on(AuthenticationActions.requestUserProfilePictureSuccess, (state, { userProfilePicture }): State => ({
    ...state,
    userProfilePicture,
  })),
  on(AuthenticationActions.setHttpError, (state, { error }): State => ({
    ...state,
    error,
  }))
);

export function authenticationMetaReducer(
  reducer: ActionReducer<State>
): ActionReducer<State> {
  return (state, action) => {
    if (action.type === INIT || action.type === UPDATE) {
      const sv = localStorage.getItem('authState');
      if (sv) {
        try {
          return JSON.parse(sv);
        } catch {
          localStorage.removeItem('authState');
        }
      }
    }
    const authState = reducer(state, action);
    localStorage.setItem('authState', JSON.stringify(authState));
    return authState;
  };
}

export function reducer(state: State | undefined, action: Action) {
  return authReducer(state, action);
}
