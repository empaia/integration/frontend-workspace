/* eslint-disable @typescript-eslint/no-explicit-any */
import { Inject, Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { AuthService } from '@core/services/auth.service';
import { HttpClient } from '@angular/common/http';
import * as AuthenticationActions from './authentication.actions';
import { exhaustMap, filter, map } from 'rxjs/operators';
import { of } from 'rxjs';
import { filterNullish } from '@helper/rxjs-operators';
import { fetch } from '@ngrx/router-store/data-persistence';
import { State } from './authentication.reducer';
import { UserProfileEntity } from './authentication.model';

@Injectable()
export class AuthenticationEffects {
  setIsAuthenticated$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(AuthenticationActions.setUserId),
      map(() => AuthenticationActions.setIsAuthenticated())
    );
  });

  // set http error if set to not authenticated
  setNotAuthenticationError$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(AuthenticationActions.setIsNotAuthenticated),
      map((action) => action.error),
      filterNullish(),
      map((error) => AuthenticationActions.setHttpError({ error }))
    );
  });

  loginUser$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(AuthenticationActions.loginUser),
      exhaustMap((_action) => {
        // make sure user is logged out properly and session is cleared
        this.authService.logout();
        if (!this.authService.isLoggedIn() || this.authService.isTokenValid()) {
          this.authService.initialLogin();
        }
        return of({ type: 'noop' });
      })
    );
  });

  logoutUser$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(AuthenticationActions.logoutUser),
      map(() => this.authService.logout()),
      map(() =>
        AuthenticationActions.setIsNotAuthenticated({ error: undefined })
      )
    );
  });

  // requestUserProfilePicture$ = createEffect(() => {
  //   return this.actions$.pipe(
  //     ofType(AuthenticationActions.requestUserProfilePicture),
  //     switchMap(() =>
  //       (this.wbsIdpUserUrl && this.wbsIdpUserUrl !== '')
  //         ? this.httpClient
  //           .get(`${this.wbsIdpUserUrl}/me`)
  //           .pipe(
  //             filter(userMe => !!userMe && 'user_id' in userMe),
  //             map(userMe => userMe as any),
  //             map(userMe => userMe['picture']),
  //             map(userProfilePicture => AuthenticationActions.requestUserProfilePictureSuccess({ userProfilePicture })),
  //             catchError(error => of(AuthenticationActions.requestUserProfilePictureFailure({ error })))
  //           )
  //         : of(AuthenticationActions.requestUserProfilePictureSuccess({ userProfilePicture: undefined }))
  //     )
  //   );
  // });

  requestUserProfilePicture$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(AuthenticationActions.setUserData),
      filter(() => !!this.aaaApiUrl),
      fetch({
        run: (
          _action: ReturnType<typeof AuthenticationActions.setUserData>,
          _state: State
        ) => {
          return this.httpClient
            .get(`${this.aaaApiUrl}/api/v2/my/profile`)
            .pipe(
              map((userData) => userData as UserProfileEntity),
              map((userData) => userData?.resized_profile_picture_urls?.w60),
              map((userProfilePicture) =>
                AuthenticationActions.requestUserProfilePictureSuccess({
                  userProfilePicture,
                })
              )
            );
        },
        onError: (
          _action: ReturnType<typeof AuthenticationActions.setUserData>,
          error
        ) => {
          return AuthenticationActions.requestUserProfilePictureFailure({
            error,
          });
        },
      })
    );
  });

  constructor(
    private readonly actions$: Actions,
    private readonly authService: AuthService,
    private readonly httpClient: HttpClient,
    // @Inject('IDP_USER_URL') private wbsIdpUserUrl: string,
    @Inject('AAA_API_URL') private aaaApiUrl?: string
  ) {}
}
