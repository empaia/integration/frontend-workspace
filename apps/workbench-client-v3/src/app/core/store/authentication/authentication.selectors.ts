import { createSelector } from '@ngrx/store';
import {
  selectCoreFeatureState,
  State as ModuleState
} from '../core-feature.state';

export const selectAuthenticationState = createSelector(
  selectCoreFeatureState,
  (state: ModuleState) => state.authentication
);

export const selectIsAuthenticated = createSelector(
  selectAuthenticationState,
  (state) => state.isAuthenticated
);

export const selectUserId = createSelector(
  selectAuthenticationState,
  (state) => state.userData?.userId
);

export const selectUserName = createSelector(
  selectAuthenticationState,
  (state) => state.userData?.userName
);

export const selectUserData = createSelector(
  selectAuthenticationState,
  (state) => state.userData
);

export const selectIsRedirecting = createSelector(
  selectAuthenticationState,
  (state) => state.isRedirecting
);

export const selectUserProfilePicture = createSelector(
  selectAuthenticationState,
  (state) => state.userProfilePicture
);

export const selectError = createSelector(
  selectAuthenticationState,
  (state) => state.error
);
