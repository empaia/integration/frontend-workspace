import { Action, combineReducers, createFeatureSelector } from '@ngrx/store';
import * as fromAuthentication from '@core/store/authentication/authentication.reducer';

export const CORE_MODULE_FEATURE_KEY = 'coreModuleFeature';

export const selectCoreFeatureState = createFeatureSelector<State>(
  CORE_MODULE_FEATURE_KEY
);

export interface State {
  [fromAuthentication.AUTHENTICATION_FEATURE_KEY]: fromAuthentication.State;
}

export function reducers(state: State | undefined, action: Action) {
  return combineReducers({
    [fromAuthentication.AUTHENTICATION_FEATURE_KEY]: fromAuthentication.reducer,
  })(state, action);
}
