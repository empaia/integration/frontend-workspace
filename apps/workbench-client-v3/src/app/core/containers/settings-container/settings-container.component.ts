import { ChangeDetectionStrategy, Component } from '@angular/core';
import { AuthenticationActions, AuthenticationSelectors } from '@core/store';
import { HttpError } from '@core/store/authentication/authentication.model';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-settings-container',
  templateUrl: './settings-container.component.html',
  styleUrls: ['./settings-container.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SettingsContainerComponent {
  public loggedIn$: Observable<boolean>;
  public isRedirecting$: Observable<boolean>;
  public error$: Observable<HttpError | undefined>;

  constructor(private readonly store: Store) {
    this.loggedIn$ = this.store.select(AuthenticationSelectors.selectIsAuthenticated);
    this.isRedirecting$ = this.store.select(AuthenticationSelectors.selectIsRedirecting);
    this.error$ = this.store.select(AuthenticationSelectors.selectError);
  }

  public login(): void {
    this.store.dispatch(AuthenticationActions.loginUser());
  }
}
