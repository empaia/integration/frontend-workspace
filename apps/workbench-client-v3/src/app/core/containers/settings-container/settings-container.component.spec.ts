import { AuthenticationSelectors } from '@core/store';
import { provideMockStore } from '@ngrx/store/testing';
import { MockComponents } from 'ng-mocks';
import { createComponentFactory, Spectator } from '@ngneat/spectator/jest';
import { SettingsContainerComponent } from './settings-container.component';
import { SettingsMenuContainerComponent } from '@menu/containers/settings-menu-container/settings-menu-container.component';
import { PreprocessingsContentContainerComponent } from '@preprocessings/containers/preprocessings-content-container/preprocessings-content-container.component';
import { LoginComponent } from '@core/components/login/login.component';

describe('SettingsContainerComponent', () => {
  let spectator: Spectator<SettingsContainerComponent>;
  const createComponent = createComponentFactory({
    component: SettingsContainerComponent,
    declarations: [
      MockComponents(
        SettingsMenuContainerComponent,
        PreprocessingsContentContainerComponent,
        LoginComponent
      )
    ],
    providers: [
      provideMockStore({
        selectors: [
          {
            selector: AuthenticationSelectors.selectIsAuthenticated,
            value: false
          },
          {
            selector: AuthenticationSelectors.selectIsRedirecting,
            value: false
          },
          {
            selector: AuthenticationSelectors.selectError,
            value: undefined
          }
        ]
      })
    ]
  });

  beforeEach(() => spectator = createComponent());

  it('should create', () => {
    expect(spectator.component).toBeTruthy();
  });
});
