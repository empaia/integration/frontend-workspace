import { ChangeDetectionStrategy, Component } from '@angular/core';
import { AuthenticationActions, AuthenticationSelectors } from '@core/store';
import { Store } from '@ngrx/store';

@Component({
  selector: 'app-diagnostic-container',
  templateUrl: './diagnostic-container.component.html',
  styleUrls: ['./diagnostic-container.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DiagnosticContainerComponent {
  public loggedIn$ = this.store.select(AuthenticationSelectors.selectIsAuthenticated);
  public isRedirecting$ = this.store.select(AuthenticationSelectors.selectIsRedirecting);
  public error$ = this.store.select(AuthenticationSelectors.selectError);

  constructor(private readonly store: Store) { }

  public login(): void {
    this.store.dispatch(AuthenticationActions.loginUser());
  }
}
