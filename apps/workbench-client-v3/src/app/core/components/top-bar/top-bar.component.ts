import { UserData } from '@core/store/authentication/authentication.model';
import { ROUTE_CASES, ROUTE_SETTINGS } from '@router/routes';
import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { BLANK_PROFILE_IMAGE } from 'src/app/app.module';

@Component({
  selector: 'app-top-bar',
  templateUrl: './top-bar.component.html',
  styleUrls: ['./top-bar.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TopBarComponent {
  @Input() public userData: UserData | undefined;
  @Input() public userProfilePictureUrl: string | undefined;
  @Input() public withAuthentication = false;
  @Input() public debugMode = false;

  @Output() public toggleHideAllMenus = new EventEmitter<void>();
  @Output() public logout = new EventEmitter<void>();
  @Output() public login = new EventEmitter<void>();
  @Output() public routeToItem = new EventEmitter<string>();

  public readonly CASES_ROUTE = ROUTE_CASES;
  public readonly SETTINGS_ROUTE = ROUTE_SETTINGS;
  public readonly FALLBACK_PROFILE = BLANK_PROFILE_IMAGE;

  public onLogout(): void {
    this.logout.emit();
  }

  public onLogin(): void {
    this.login.emit();
  }
}
