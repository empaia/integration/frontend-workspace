/* eslint-disable @typescript-eslint/no-explicit-any */
import { Injectable, Optional } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor, HttpResponse
} from '@angular/common/http';
import { EMPTY, Observable, throwError } from 'rxjs';
import { OAuthModuleConfig, OAuthService } from 'angular-oauth2-oidc';
import { Store } from '@ngrx/store';
import { AuthenticationActions } from '@core/store';
import { catchError } from 'rxjs/operators';
import {
  HttpError,
} from '@core/store/authentication/authentication.model';

@Injectable()
export class EmpaiaAuthInterceptor implements HttpInterceptor {

  constructor(
    private readonly oAuthService: OAuthService,
    private readonly store: Store,
    @Optional() private readonly moduleConfig: OAuthModuleConfig
  ) {}

  private authenticationRequired(url: string): boolean {
    if (
      !this.moduleConfig
      || !this.moduleConfig.resourceServer
      || !this.moduleConfig.resourceServer.allowedUrls
    ) {
      return false;
    }

    const found = this.moduleConfig.resourceServer.allowedUrls.filter(u => !!u).find(u => url.startsWith(u));
    return !!found;
  }

  private getRequest(request: HttpRequest<unknown>): HttpRequest<unknown> {
    const token = this.oAuthService.getAccessToken();
    const claims:any = this.oAuthService.getIdentityClaims();
    if (token && claims) {
      const headers = request.headers
        .set('Authorization', `Bearer ${token}`)
        .set('user-id', claims['sub']);
      request = request.clone({ withCredentials: false, headers });
    }
    return request;
  }

  private handleAuthenticationError(
    error: HttpResponse<unknown>,
    _request: HttpRequest<unknown>,
    _next: HttpHandler): Observable<HttpEvent<unknown>> {
    if (error.status === 401 || error.status === 403) {
      this.store.dispatch(AuthenticationActions.setIsNotAuthenticated({ error: (error as unknown) as HttpError }));
      return EMPTY;
    } else {
      return throwError(error);
    }
  }

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    const url = request.url.toLowerCase();
    if (!this.authenticationRequired(url)) {
      return next.handle(request).pipe(
        catchError((error: HttpResponse<unknown>) => {
          this.store.dispatch(AuthenticationActions.setIsNotAuthenticated({ error: (error as unknown) as HttpError}));
          return EMPTY;
        })
      );
    }

    const sendAccessToken = this.moduleConfig.resourceServer.sendAccessToken;

    if (sendAccessToken) {
      request = this.getRequest(request);
    }

    return next.handle(request).pipe(
      catchError((error: HttpResponse<unknown>) => {
        return this.handleAuthenticationError(error, request, next);
      })
    );
  }
}
