import { EmpaiaAuthInterceptor } from './empaia-auth.interceptor';
import { createHttpFactory, mockProvider, SpectatorHttp } from '@ngneat/spectator';
import { provideMockStore } from '@ngrx/store/testing';
import { OAuthService } from 'angular-oauth2-oidc';

describe('EmpaiaAuthInterceptor', () => {
  let spectator: SpectatorHttp<EmpaiaAuthInterceptor>;
  const createHttp = createHttpFactory({
    service: EmpaiaAuthInterceptor,
    providers: [
      provideMockStore(),
      mockProvider(OAuthService)
    ]
  });

  beforeEach(() => spectator = createHttp());

  it('should be created', () => {
    const interceptor: EmpaiaAuthInterceptor = spectator.service;
    expect(interceptor).toBeTruthy();
  });
});
