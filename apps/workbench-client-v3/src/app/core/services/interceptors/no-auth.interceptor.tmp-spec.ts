import { TestBed } from '@angular/core/testing';

import { NoAuthInterceptor } from './no-auth.interceptor';

describe('NoAuthInterceptor', () => {
  beforeEach(() => TestBed.configureTestingModule({
    providers: [
      NoAuthInterceptor
    ]
  }));

  it('should be created', () => {
    const interceptor: NoAuthInterceptor = TestBed.inject(NoAuthInterceptor);
    expect(interceptor).toBeTruthy();
  });
});
