import { HttpClientTestingModule } from '@angular/common/http/testing';
import { createServiceFactory, SpectatorService } from '@ngneat/spectator/jest';
import { UserService } from './user.service';

describe('UserService', () => {
  let spectator: SpectatorService<UserService>;
  const createService = createServiceFactory({
    service: UserService,
    imports: [
      HttpClientTestingModule
    ],
    providers: [
      { provide: 'IDP_URL', useValue: 'test-url'},
      { provide: 'WBS_USER_ID', useValue: 'test-user-id' },
      { provide: 'IDP_USER_URL', useValue: 'test-user-url'},
    ],
    entryComponents: [],
    mocks: []
  });

  beforeEach(() => spectator = createService());

  it('should be created', () => {
    expect(spectator.service).toBeTruthy();
  });

  it('should return the user id', () => {
    expect(spectator.service.userId).toBe('test-user-id');
  });

  it('should return the header key', () => {
    expect(spectator.service.headerKey).toBe('user-id');
  });

  it('should return request header', () => {
    expect(spectator.service.getUserIdHeader()).toMatchObject({
      'user-id': 'test-user-id'
    });
  });
});
