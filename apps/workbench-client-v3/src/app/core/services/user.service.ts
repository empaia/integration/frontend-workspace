import { Inject, Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private _userId = 'EMPTY USER ID!';
  private readonly _headerKey = 'user-id';

  constructor(
    @Inject('WBS_USER_ID') private wbsUserId: string,
  ) {
    this._userId = wbsUserId;
  }

  public getUserIdHeader() {
    return { [this.headerKey]: this.userId };
  }

  public get headerKey(): string {
    return this._headerKey;
  }

  public get userId(): string {
    return this._userId;
  }
}
