import { AuthService } from './auth.service';
import { createServiceFactory, mockProvider, SpectatorService } from '@ngneat/spectator';
import { provideMockStore } from '@ngrx/store/testing';
import { OAuthService } from 'angular-oauth2-oidc';
import { RouterTestingModule } from '@angular/router/testing';

describe('AuthService', () => {
  let spectator: SpectatorService<AuthService>;
  const createService = createServiceFactory({
    service: AuthService,
    imports: [
      RouterTestingModule,
    ],
    providers: [
      provideMockStore(),
      mockProvider(OAuthService),
      { provide: 'AUTHENTICATION_ON', useValue: false }
    ]
  });

  beforeEach(() => spectator = createService());

  it('should be created', () => {
    expect(spectator.service).toBeTruthy();
  });
});
