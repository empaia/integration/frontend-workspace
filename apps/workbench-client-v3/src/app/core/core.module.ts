import { PreprocessingsModule } from '@preprocessings/preprocessings.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AppComponent } from './containers/app/app.component';
import { TopBarComponent } from './components/top-bar/top-bar.component';
import { MaterialModule } from '@material/material.module';
import { MenuModule } from '@menu/menu.module';
import { SharedModule } from '@shared/shared.module';
import { CasesModule } from '@cases/cases.module';
import { ExaminationsModule } from '@examinations/examinations.module';
import { AppsModule } from '@apps/apps.module';
import { SlidesModule } from '@slides/slides.module';
import { LoginComponent } from './components/login/login.component';
import { StoreModule } from '@ngrx/store';
import { CORE_MODULE_FEATURE_KEY, reducers } from '@core/store/core-feature.state';
import { EffectsModule } from '@ngrx/effects';
import { AuthenticationEffects } from '@core/store';
import { DiagnosticContainerComponent } from './containers/diagnostic-container/diagnostic-container.component';
import { AppRouterModule } from '@router/app-router.module';
import { SettingsContainerComponent } from './containers/settings-container/settings-container.component';

const COMPONENTS = [
  AppComponent,
  TopBarComponent,
  LoginComponent,
  DiagnosticContainerComponent,
];

@NgModule({
  declarations: [
    ...COMPONENTS,
    SettingsContainerComponent,
  ],
  imports: [
    CommonModule,
    MaterialModule,
    AppRouterModule,
    MenuModule,
    SharedModule,
    CasesModule,
    ExaminationsModule,
    AppsModule,
    SlidesModule,
    PreprocessingsModule,
    StoreModule.forFeature(
      CORE_MODULE_FEATURE_KEY,
      reducers
    ),
    EffectsModule.forFeature([
      AuthenticationEffects
    ])
  ],
  exports: [
    AppComponent,
    LoginComponent,
    DiagnosticContainerComponent,
  ]
})
export class CoreModule { }
