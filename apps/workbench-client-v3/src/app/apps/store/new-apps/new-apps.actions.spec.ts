import * as fromNewApps from './new-apps.actions';

describe('loadNewApps', () => {
  it('should return an action', () => {
    expect(fromNewApps.loadNewApps({}).type).toBe('[NewApps] Load NewApps');
  });
});
