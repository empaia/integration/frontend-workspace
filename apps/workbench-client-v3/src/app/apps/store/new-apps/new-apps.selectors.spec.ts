import { App } from './new-apps.models';
import { appsAdapter, initialState } from './new-apps.reducer';



describe('NewApps Selectors', () => {
  const ERROR_MSG = 'No Error Available';
  //const getAppsId = (it: any) => it['id'];
  const createAppsEntity = (id: string, appVersion = '') =>
    ({
      app_id: id,
      app_version: appVersion || `version-${id}`,
      store_description: 'desc',
      store_docs_url: 'docs',
      store_icon_url: 'icon',
      store_url: 'store_url',
    } as App);

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  let state: unknown = { appsFeature: { newApps: {} } };
  //let store: MockStore<State>;

  beforeEach(() => {
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    state = {
      appsFeature: {
        apps: appsAdapter.setAll(
          [
            createAppsEntity('NEW-APP-AAA'),
            createAppsEntity('NEW-APP-BBB'),
            createAppsEntity('NEW-APP-CCC'),
          ],
          {
            ...initialState,
            // selectedId: 'APP-BBB',
            error: ERROR_MSG,
            loaded: true,
          }
        ),
      }
    };
  });

  describe('Apps Selectors', () => {
  //   it('getAllApps() should return the list of Apps', () => {
  //     const results = NewAppsSelectors.getAllApps(state);
  //     const selId = getAppsId(results[1]);

    //     expect(results.length).toBe(3);
    //     expect(selId).toBe('NEW-APP-BBB');
    //   });

    //   // it('getSelected() should return the selected Entity', () => {
    //   //   const result = AppsSelectors.getSelected(state);
    //   //   const selId = getAppsId(result);

    //   //   expect(selId).toBe('APP-BBB');
    //   // });

    //   it("getAppsLoaded() should return the current 'loaded' status", () => {
    //     const result = AppsSelectors.getAppsLoaded(state);

    //     expect(result).toBe(true);
    //   });

    //   it("getAppsError() should return the current 'error' state", () => {
    //     const result = AppsSelectors.getAppsError(state);

    //     expect(result).toBe(ERROR_MSG);
    //   });
    it.todo('add some NewApps Selectors tests');
  });
});
