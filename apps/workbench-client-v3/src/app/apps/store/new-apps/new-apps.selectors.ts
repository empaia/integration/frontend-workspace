import { createSelector } from '@ngrx/store';
import {
  selectAppsFeatureState,
  State as ModuleState
} from '../apps-feature.state';
import { appsAdapter } from './new-apps.reducer';

const {
  selectAll,
  selectEntities,
} = appsAdapter.getSelectors();

// select feature sub state - rootState.appsFeature.newApps
export const selectNewAppsState = createSelector(
  selectAppsFeatureState,
  (state: ModuleState) => state.newApps
);

export const selectNewAppsPanelOpen = createSelector(
  selectNewAppsState,
  (state) => state.panelOpened
);

export const selectAllNewApps = createSelector(
  selectNewAppsState,
  selectAll
);

export const selectNewAppsEntities = createSelector(
  selectNewAppsState,
  selectEntities
);

export const selectHoverApp = createSelector(
  selectNewAppsState,
  (state) => state.hover
);

export const selectNewAppsLoaded = createSelector(
  selectNewAppsState,
  (state) => state.loaded
);
