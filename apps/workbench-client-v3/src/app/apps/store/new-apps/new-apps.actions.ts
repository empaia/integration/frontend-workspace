import { HttpErrorResponse } from '@angular/common/http';
import { createAction, props } from '@ngrx/store';
import { App, AppQuery } from './new-apps.models';

export const loadNewApps = createAction(
  '[NewApps] Load NewApps',
  props<{ appQuery?: AppQuery }>()
);

export const loadNewAppsSuccess = createAction(
  '[NewApps] Load NewApps Success',
  props<{ apps: App[] }>()
);

export const loadNewAppsFailure = createAction(
  '[NewApps] Load NewApps Failure',
  props<{ error: HttpErrorResponse }>()
);

export const clearNewApps = createAction(
  '[NewApps] Clear'
);

export const openNewAppsPanel = createAction(
  '[NewApps] Open new Apps Panel',
);

export const closeNewAppsPanel = createAction(
  '[NewApps] Close new Apps Panel',
);

export const hoverOverApp = createAction(
  '[NewApps] Hover Over App',
  props<{ appId?: string | undefined }>()
);
