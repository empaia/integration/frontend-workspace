import { HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType, concatLatestFrom } from '@ngrx/effects';
import { filter, map, switchMap } from 'rxjs/operators';
import { of } from 'rxjs';

import * as CasesActions from '@cases/store/cases/cases.actions';
import * as ExaminationsActions from '@examinations/store/examinations/examinations.actions';
import * as NewAppsActions from './new-apps.actions';
import * as DiagnosticMenuActions from '@menu/store/diagnostic-menu/diagnostic-menu.actions';
import * as PreprocessingTriggersActions from '@preprocessings/store/preprocessing-triggers/preprocessing-triggers.actions';
import * as TagsActions from '@preprocessings/store/tags/tags.actions';
import * as TagsSelectors from '@preprocessings/store/tags/tags.selectors';
import { fetch } from '@ngrx/router-store/data-persistence';
import { Store } from '@ngrx/store';
import { ErrorSnackbarComponent } from '@shared/components/error-snackbar/error-snackbar.component';
import { MatSnackBar } from '@angular/material/snack-bar';
import { NEW_APPS_LOAD_ERROR } from '@menu/models/ui.models';
import { WbcV3AppsService, WbcV3JobMode } from 'empaia-api-lib';
import { State } from './new-apps.reducer';

@Injectable()
export class NewAppsEffects {
  loadNewAppsOnCaseSelection$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(CasesActions.userSelectedCase, CasesActions.navigationSelectCase),
      map(() => NewAppsActions.loadNewApps({}))
    );
  });

  loadNewApps$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(NewAppsActions.loadNewApps),
      fetch({
        run: (
          action: ReturnType<typeof NewAppsActions.loadNewApps>,
          _state: State
        ) => {
          return this.appsService
            .appsQueryPut({
              body: action.appQuery ?? {},
            })
            .pipe(
              map((results) => results.items),
              map((apps) => NewAppsActions.loadNewAppsSuccess({ apps }))
            );
        },
        onError: (
          _action: ReturnType<typeof NewAppsActions.loadNewApps>,
          error
        ) => {
          return NewAppsActions.loadNewAppsFailure({ error });
        },
      })
    );
  });

  loadAllPortalApps$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(
        PreprocessingTriggersActions.loadAllPreprocessingTriggers,
        TagsActions.selectTissues,
        TagsActions.selectStains
      ),
      concatLatestFrom(() => [
        this.store.select(TagsSelectors.selectSelectedTissues),
        this.store.select(TagsSelectors.selectSelectedStains),
      ]),
      map(([, tissues, stains]) =>
        NewAppsActions.loadNewApps({
          appQuery: {
            tissues,
            stains,
            job_modes: [WbcV3JobMode.Preprocessing],
          },
        })
      )
    );
  });

  newAppsLoadFailure$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(NewAppsActions.loadNewAppsFailure),
      map((action) => action.error),
      switchMap((error: HttpErrorResponse) => {
        this.snackBar.openFromComponent(ErrorSnackbarComponent, {
          data: { titel: NEW_APPS_LOAD_ERROR, error },
        });
        return of({ type: 'noop' });
      })
    );
  });

  // close new app panel when a menu button was clicked or an app was selected
  closeNewAppsPanel$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(
        ExaminationsActions.userSelectedExamination,
        DiagnosticMenuActions.openMenuMidi,
        DiagnosticMenuActions.openMenuMax
      ),
      filter(
        (action) =>
          !(
            action.type === '[DiagnosticMenu] Open Menu Midi' &&
            action.menuItem === 'Apps'
          )
      ),
      map(() => NewAppsActions.closeNewAppsPanel())
    );
  });

  constructor(
    private readonly actions$: Actions,
    private readonly appsService: WbcV3AppsService,
    private readonly store: Store,
    private readonly snackBar: MatSnackBar
  ) {}
}
