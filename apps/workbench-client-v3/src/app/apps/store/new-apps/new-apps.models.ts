import { WbcV3AppInput, WbcV3AppQuery } from 'empaia-api-lib';

export type App = WbcV3AppInput;
export type AppQuery = WbcV3AppQuery;
