import { Action, combineReducers, createFeatureSelector } from '@ngrx/store';
import * as fromNewApps from '@apps/store/new-apps/new-apps.reducer';
import * as fromVendorAppSurfaces from '@apps/store/vendor-app-surfaces/vendor-app-surfaces.reducer';

export const APPS_MODULE_FEATURE_KEY = 'appsModuleFeature';

export const selectAppsFeatureState = createFeatureSelector<State>(
  APPS_MODULE_FEATURE_KEY
);

export interface State {
  [fromNewApps.NEW_APPS_FEATURE_KEY]: fromNewApps.State;
  [fromVendorAppSurfaces.VENDOR_APP_SURFACES_FEATURE_KEY]: fromVendorAppSurfaces.State;
}

export function reducers(state: State | undefined, action: Action) {
  return combineReducers({
    [fromNewApps.NEW_APPS_FEATURE_KEY]: fromNewApps.reducer,
    [fromVendorAppSurfaces.VENDOR_APP_SURFACES_FEATURE_KEY]: fromVendorAppSurfaces.reducer,
  })(state, action);
}
