import { ExaminationCreatorType, ExaminationState } from '@examinations/store';
import * as fromVendorAppSurfaces from './vendor-app-surfaces.actions';

describe('loadVendorAppSurfaces', () => {
  it('should return an action', () => {
    expect(fromVendorAppSurfaces.addActiveApp({
      examination: {
        id: 'EXAMINATION-ID',
        app_id: 'APP-ID',
        case_id: 'CASE-ID',
        created_at: 0,
        updated_at: 0,
        creator_id: '123',
        creator_type: ExaminationCreatorType.User,
        jobs: [],
        jobs_count: 0,
        jobs_count_finished: 0,
        state: ExaminationState.Open,
        api_version: 'v3',
        app: {
          app_id: 'APP-ID',
          portal_app_id: 'APP-ID',
          app_version: 'v1',
          name_short: 'test-app',
          store_description: 'test app',
          store_icon_url: '',
          store_url: '',
          vendor_name: 'Test Vendor',
          has_frontend: false,
          research_only: false,
        }
      }
    }).type).toBe('[VendorAppSurfaces] Add Active App');
  });
});
