import { createAction, props } from '@ngrx/store';
import { VendorExaminationEntity } from '@apps/store/vendor-app-surfaces/vendor-app-surfaces.models';
import { HttpErrorResponse } from '@angular/common/http';

export const addActiveApp = createAction(
  '[VendorAppSurfaces] Add Active App',
  props<{ examination: VendorExaminationEntity }>()
);

export const setActiveAppFocus = createAction(
  '[VendorAppSurfaces] Set Active App Focus',
  props<{ focus: string | undefined }>()
);

export const discardApps = createAction(
  '[VendorAppSurfaces] Discard Apps',
  props<{ examinationIds: string[] }>()
);

export const clearApps = createAction(
  '[VendorAppSurfaces] Clear Apps',
);

export const loadAppToken = createAction(
  '[VendorAppSurfaces] Load App Token',
  props<{ examination: VendorExaminationEntity }>()
);

export const loadAppTokenSuccess = createAction(
  '[VendorAppSurfaces] Load App Token Success',
  props<{ examination: VendorExaminationEntity }>()
);

export const loadAppTokenFailure = createAction(
  '[VendorAppSurfaces] Load App Token Failure',
  props<{ error: HttpErrorResponse }>()
);

export const loadAppUiConfig = createAction(
  '[VendorAppSurfaces] Load App Ui Config',
  props<{ examination: VendorExaminationEntity }>()
);

export const loadAppUiConfigSuccess = createAction(
  '[VendorAppSurfaces] Load App Ui Config Success',
  props<{ examination: VendorExaminationEntity }>()
);

export const loadAppUiConfigFailure = createAction(
  '[VendorAppSurfaces] Load App Ui Config Failure',
  props<{ error: HttpErrorResponse, examination: VendorExaminationEntity }>()
);
