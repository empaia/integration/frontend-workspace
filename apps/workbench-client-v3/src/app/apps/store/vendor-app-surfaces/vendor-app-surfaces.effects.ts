import {
  DEFAULT_IFRAME_CONFIG,
  VendorExaminationEntity,
} from '@apps/store/vendor-app-surfaces/vendor-app-surfaces.models';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Inject, Injectable } from '@angular/core';
import { Actions, concatLatestFrom, createEffect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { AppMappingService } from '@apps/services/app-mapping.service';
import * as VendorAppSurfacesActions from './vendor-app-surfaces.actions';
import * as VendorAppSurfacesSelectors from './vendor-app-surfaces.selectors';
import * as CasesActions from '@cases/store/cases/cases.actions';
import * as ExaminationsActions from '@examinations/store/examinations/examinations.actions';
import * as ExaminationsSelectors from '@examinations/store/examinations/examinations.selectors';
import { exhaustMap, filter, map } from 'rxjs/operators';
import { filterNullish } from '@helper/rxjs-operators';
import { fetch } from '@ngrx/router-store/data-persistence';
import { Examination } from '@examinations/store/examinations/examinations.models';
import { VendorAppErrorMap } from './vendor-app-surfaces.models';
import { ErrorSnackbarComponent } from '@shared/components/error-snackbar/error-snackbar.component';
import { of } from 'rxjs';
import {
  WbcV3ExaminationsService,
  WbcV3AppUiIframeConfiguration,
} from 'empaia-api-lib';
import { State } from './vendor-app-surfaces.reducer';

@Injectable()
export class VendorAppSurfacesEffects {
  // clear apps when a new case or examination was selected
  clearOnCaseExaminationSelection$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(CasesActions.userSelectedCase, CasesActions.navigationSelectCase),
      map(() => VendorAppSurfacesActions.clearApps())
    );
  });

  // add an app when user selects an app in the menu
  addApp$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(
        ExaminationsActions.userSelectedExamination,
        ExaminationsActions.loadExaminationsSuccess
      ),
      concatLatestFrom(() =>
        this.store
          .select(ExaminationsSelectors.selectSelected)
          .pipe(filterNullish())
      ),
      map(([, examination]) => examination as Examination),
      map((examination) =>
        VendorAppSurfacesActions.addActiveApp({ examination })
      )
    );
  });

  addActiveApp$ = createEffect(
    () => {
      return this.actions$.pipe(
        ofType(VendorAppSurfacesActions.addActiveApp),
        map((action) => action.examination),
        map((examination) => this.appIdSet.add(examination.id))
      );
    },
    { dispatch: false }
  );

  checkCleanUpCache$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(VendorAppSurfacesActions.addActiveApp),
      concatLatestFrom(() => [
        this.store.select(VendorAppSurfacesSelectors.selectCurrentAppCacheSize),
        this.store.select(VendorAppSurfacesSelectors.selectMaxAppCacheSize),
      ]),
      filter(([, cacheSize, maxSize]) => cacheSize > maxSize),
      map(([, cacheSize, maxSize]) =>
        this.appIdSet.discard(cacheSize - maxSize)
      ),
      map((examinationIds) =>
        VendorAppSurfacesActions.discardApps({ examinationIds })
      )
    );
  });

  clearApps$ = createEffect(
    () => {
      return this.actions$.pipe(
        ofType(VendorAppSurfacesActions.clearApps),
        map(() => this.appIdSet.clear())
      );
    },
    { dispatch: false }
  );

  addTokenUrlToApp$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(VendorAppSurfacesActions.addActiveApp),
      map((action) => action.examination),
      concatLatestFrom(() => [
        this.store.select(VendorAppSurfacesSelectors.selectVendorAppMap),
      ]),
      filter(
        ([examination, appMap]) =>
          examination.app.has_frontend &&
          !!appMap.get(examination.id) &&
          !appMap.get(examination.id)?.app?.appUrl
      ),
      map(([examination]) =>
        VendorAppSurfacesActions.loadAppToken({
          examination,
        })
      )
    );
  });

  loadAppToken$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(VendorAppSurfacesActions.loadAppToken),
      fetch({
        run: (
          action: ReturnType<typeof VendorAppSurfacesActions.loadAppToken>,
          _state: State
        ) => {
          return this.examinationsService
            .examinationsExaminationIdFrontendTokenGet({
              examination_id: action.examination.id,
            })
            .pipe(
              map(
                (token) =>
                  `${this.wbsServerUrl}${this.apiVersion}/frontends/${token.access_token}/`
              ),
              map((appUrl) => ({
                ...action.examination,
                app: { ...action.examination.app, appUrl },
              })),
              map((examination) =>
                VendorAppSurfacesActions.loadAppTokenSuccess({ examination })
              )
            );
        },
        onError: (
          _action: ReturnType<typeof VendorAppSurfacesActions.loadAppToken>,
          error
        ) => {
          return VendorAppSurfacesActions.loadAppTokenFailure({ error });
        },
      })
    );
  });

  // discard app on app close
  appClose$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ExaminationsActions.closeExaminationApp),
      map((action) => action.examinationId),
      map((examinationId) =>
        VendorAppSurfacesActions.discardApps({
          examinationIds: [examinationId],
        })
      )
    );
  });

  // set focus to last app pushed into cache after app was discarded
  // or set focus to undefined when list is empty
  unfocusedOnDiscard$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(VendorAppSurfacesActions.discardApps),
      map((action) => action.examinationIds),
      concatLatestFrom(() => [
        this.store
          .select(VendorAppSurfacesSelectors.selectFocusedAppId)
          .pipe(filterNullish()),
        this.store.select(VendorAppSurfacesSelectors.selectAllActiveAppIds),
      ]),
      filter(
        ([discardedIds, focusId]) => !!discardedIds.find((id) => id === focusId)
      ),
      map(([, _, activeAppIds]) =>
        VendorAppSurfacesActions.setActiveAppFocus({
          focus: activeAppIds?.length
            ? activeAppIds[activeAppIds.length - 1]
            : undefined,
        })
      )
    );
  });

  addAppUiConfig$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(VendorAppSurfacesActions.loadAppTokenSuccess),
      map((action) => action.examination),
      map((examination) =>
        VendorAppSurfacesActions.loadAppUiConfig({ examination })
      )
    );
  });

  loadAppUiConfig$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(VendorAppSurfacesActions.loadAppUiConfig),
      fetch({
        run: (
          action: ReturnType<typeof VendorAppSurfacesActions.loadAppUiConfig>,
          _state: State
        ) => {
          return this.examinationsService
            .examinationsExaminationIdAppUiConfigGet({
              examination_id: action.examination.id,
            })
            .pipe(
              map(
                (response) =>
                  ({
                    ...action.examination,
                    app: {
                      ...action.examination.app,
                      csp: response.csp,
                      tested: response.tested,
                      iframe: this.convertIframeConfiguration(
                        DEFAULT_IFRAME_CONFIG,
                        response.iframe
                      ),
                    },
                  } as VendorExaminationEntity)
              ),
              map((examination) =>
                VendorAppSurfacesActions.loadAppUiConfigSuccess({ examination })
              )
            );
        },
        onError: (
          action: ReturnType<typeof VendorAppSurfacesActions.loadAppUiConfig>,
          error
        ) => {
          const examination = {
            ...action.examination,
            app: {
              ...action.examination.app,
              iframe: DEFAULT_IFRAME_CONFIG,
            },
          } as VendorExaminationEntity;
          return VendorAppSurfacesActions.loadAppUiConfigFailure({
            error,
            examination,
          });
        },
      })
    );
  });

  showErrorMessage$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(VendorAppSurfacesActions.loadAppTokenFailure),
      exhaustMap((action) => {
        this.snackBar.openFromComponent(ErrorSnackbarComponent, {
          data: { titel: VendorAppErrorMap[action.type], error: action.error },
        });
        return of({ type: 'noop' });
      })
    );
  });

  constructor(
    private readonly actions$: Actions,
    private readonly store: Store,
    private readonly appIdSet: AppMappingService,
    private readonly examinationsService: WbcV3ExaminationsService,
    private readonly snackBar: MatSnackBar,
    @Inject('WBS_SERVER_API_URL') private readonly wbsServerUrl: string,
    @Inject('API_VERSION') private readonly apiVersion: string
  ) {}

  private convertIframeConfiguration(
    defaultSandbox: string,
    iframe?: WbcV3AppUiIframeConfiguration | null
  ): string {
    return iframe
      ? (
        defaultSandbox +
          ' ' +
          Object.entries(iframe)
            .filter(([_key, value]) => !!value)
            .map(([key]) => key.replace('_', '-'))
            .join(' ')
      ).trim()
      : defaultSandbox;
  }
}
