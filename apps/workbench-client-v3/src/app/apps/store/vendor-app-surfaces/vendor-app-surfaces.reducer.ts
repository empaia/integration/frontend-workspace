/* eslint-disable @typescript-eslint/no-unused-vars */
import { createReducer, on } from '@ngrx/store';
import { createEntityCachingAdapter, EntityCachingAdapter, EntityCachingState } from '@shared/entity';
import * as VendorAppSurfacesActions from './vendor-app-surfaces.actions';
import { DEFAULT_CONCURRENT_APPS, VendorExaminationEntity } from '@apps/store/vendor-app-surfaces/vendor-app-surfaces.models';


export const VENDOR_APP_SURFACES_FEATURE_KEY = 'vendorAppSurfaces';

export interface State extends EntityCachingState<VendorExaminationEntity> {
  focus?: string;
  removed?: string[];
}

export const vendorAppAdapter: EntityCachingAdapter<VendorExaminationEntity> = createEntityCachingAdapter<VendorExaminationEntity>(
  (examination) => examination.id
);

export const initialState: State = vendorAppAdapter.getInitialState({
  focus: undefined,
  removed: undefined,
}, DEFAULT_CONCURRENT_APPS);


export const reducer = createReducer(
  initialState,
  on(VendorAppSurfacesActions.addActiveApp, (state, { examination }): State =>
    vendorAppAdapter.addOne(examination, {
      ...state,
      focus: examination.id,
    }),
  ),
  on(VendorAppSurfacesActions.setActiveAppFocus, (state, { focus }): State => ({
    ...state,
    focus,
  })),
  on(VendorAppSurfacesActions.discardApps, (state, { examinationIds }): State =>
    vendorAppAdapter.removeMany(examinationIds, {
      ...state,
      removed: examinationIds,
    }),
  ),
  on(VendorAppSurfacesActions.clearApps, (state): State =>
    vendorAppAdapter.removeAll({
      ...state,
      ...initialState,
      removed: [...state.entityCache.keys()]
    })
  ),
  /* on(VendorAppSurfacesActions.loadAppTokenSuccess, (state, { examination }): State =>
    vendorAppAdapter.updateOne(examination, {
      ...state
    })
  ), */
  on(VendorAppSurfacesActions.loadAppUiConfigSuccess, (state, { examination }): State =>
    vendorAppAdapter.updateOne(examination, {
      ...state
    })
  ),
  on(VendorAppSurfacesActions.loadAppUiConfigFailure, (state, { error, examination }): State =>
    vendorAppAdapter.updateOne(examination, {
      ...state
    })
  )
);

