import { WbcV3AppUiConfiguration, WbcV3AppUiTested } from 'empaia-api-lib';
import { Examination } from '@examinations/store/examinations/examinations.models';
import { APP_UI_CONFIG_LOAD_ERROR, FRONTEND_TOKEN_LOAD_ERROR } from '@menu/models/ui.models';
import { App } from '../new-apps/new-apps.models';

export type AppUiConfiguration = WbcV3AppUiConfiguration;
export type AppUiTested = WbcV3AppUiTested;

export interface VendorAppEntity extends App {
  appUrl?: string;
  csp?: AppUiConfiguration;
  iframe?: string;
  tested?: AppUiTested[];
}

export interface VendorExaminationEntity extends Examination {
  app: VendorAppEntity;
}

export const VendorAppErrorMap = {
  '[VendorAppSurfaces] Load App Ui Config Failure': APP_UI_CONFIG_LOAD_ERROR,
  '[VendorAppSurfaces] Load App Token Failure': FRONTEND_TOKEN_LOAD_ERROR,

};

export const DEFAULT_IFRAME_CONFIG = 'allow-scripts';
export const DEFAULT_CONCURRENT_APPS = 5;
