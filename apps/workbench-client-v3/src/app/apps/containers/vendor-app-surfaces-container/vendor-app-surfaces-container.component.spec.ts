import { VendorAppSurfacesContainerComponent } from './vendor-app-surfaces-container.component';
import { createComponentFactory, Spectator } from '@ngneat/spectator';
import {
  VendorAppSurfaceListComponent
} from '@apps/components/vendor-app-surface-list/vendor-app-surface-list.component';
import { MockComponents } from 'ng-mocks';
import { provideMockStore } from '@ngrx/store/testing';
import { VendorAppSurfacesSelectors } from '@apps/store';
import { ScopeSelectors } from '@scope/store';
import { TokenSelectors } from '@token/store';
import { WbsUrlSelectors } from '@wbsUrl/store';

describe('VendorAppSurfacesContainerComponent', () => {
  let spectator: Spectator<VendorAppSurfacesContainerComponent>;
  const createComponent = createComponentFactory({
    component: VendorAppSurfacesContainerComponent,
    declarations: [
      MockComponents(
        VendorAppSurfaceListComponent
      )
    ],
    providers: [
      provideMockStore({
        selectors: [
          {
            selector: VendorAppSurfacesSelectors.selectAllActiveApps,
            value: []
          },
          {
            selector: VendorAppSurfacesSelectors.selectFocusedAppId,
            value: undefined,
          },
          {
            selector: ScopeSelectors.selectScopeEntities,
            value: {}
          },
          {
            selector: TokenSelectors.selectTokenEntities,
            value: {}
          },
          {
            selector: WbsUrlSelectors.selectWbsUrl,
            value: undefined
          }
        ]
      })
    ]
  });

  beforeEach(() => spectator = createComponent());

  it('should create', () => {
    expect(spectator.component).toBeTruthy();
  });
});
