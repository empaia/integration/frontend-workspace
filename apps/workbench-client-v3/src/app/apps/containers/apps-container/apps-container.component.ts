import { ChangeDetectionStrategy, Component } from '@angular/core';
import {
  NewAppsActions,
  NewAppsSelectors,
  VendorAppSurfacesSelectors
} from '@apps/store';
import { Examination, ExaminationExpansion, ExaminationsActions, ExaminationsSelectors } from '@examinations/store';
import { IdSelection, SelectionError } from '@menu/models/ui.models';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { DiagnosticMenuItem, MenuEntity } from '@menu/models/menu.models';
import { DiagnosticMenuActions, DiagnosticMenuSelectors } from '@menu/store';
import { VendorExaminationEntity } from '@apps/store/vendor-app-surfaces/vendor-app-surfaces.models';
import { Dictionary } from '@ngrx/entity';

@Component({
  selector: 'app-apps-container',
  templateUrl: './apps-container.component.html',
  styleUrls: ['./apps-container.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AppsContainerComponent {

  public examinations$: Observable<Examination[]>;
  public selectedExamination$: Observable<string | Examination | undefined>;
  public preselectedExamination$: Observable<string | undefined>;
  public appsMenu$: Observable<MenuEntity>;
  public loaded$: Observable<boolean>;
  public examinationExpansionDictionary$: Observable<Dictionary<ExaminationExpansion>>;
  public hoverNewApp$: Observable<string | undefined>;

  public runningApps$: Observable<ReadonlyMap<string, VendorExaminationEntity>>;
  public activeApp$: Observable<string | undefined>;

  public selectionError$: Observable<SelectionError | undefined>;

  constructor(private readonly store: Store) {
    this.examinations$ = this.store.select(ExaminationsSelectors.selectAllExaminations);
    this.selectionError$ = this.store.select(ExaminationsSelectors.selectSelectionError);
    this.selectedExamination$ = this.store.select(ExaminationsSelectors.selectSelected);
    this.appsMenu$ = this.store.select(DiagnosticMenuSelectors.selectAppsMenu);
    this.loaded$ = this.store.select(ExaminationsSelectors.selectExaminationsLoaded);
    this.runningApps$ = this.store.select(VendorAppSurfacesSelectors.selectActiveAppsEntities);
    this.activeApp$ = this.store.select(VendorAppSurfacesSelectors.selectFocusedAppId);
    this.preselectedExamination$ = this.store.select(ExaminationsSelectors.selectPreSelected);
    this.examinationExpansionDictionary$ = this.store.select(ExaminationsSelectors.selectExpandedExaminationEntities);
    this.hoverNewApp$ = this.store.select(NewAppsSelectors.selectHoverApp);
  }

  public selectExamination(selected: IdSelection): void {
    this.store.dispatch(ExaminationsActions.userSelectedExamination({ examinationId: selected.id }));
  }

  public preselectExamination(preselected: IdSelection | undefined): void {
    this.store.dispatch(ExaminationsActions.preSelectExamination({ examinationId: preselected ? preselected.id : undefined }));
  }

  public onExaminationClose(item: IdSelection): void {
    this.store.dispatch(ExaminationsActions.openCloseExaminationDialog({ examinationId: item.id }));
  }

  public addNewApp($event: MouseEvent) {
    $event.stopImmediatePropagation();
    this.store.dispatch(NewAppsActions.openNewAppsPanel());
  }

  public clickMenuButton(menuItem: DiagnosticMenuItem): void {
    this.store.dispatch(DiagnosticMenuActions.clickMenuButton({ menuItem }));
  }

  public onAppClose(item: IdSelection): void {
    this.store.dispatch(ExaminationsActions.closeExaminationApp({ examinationId: item.id }));
  }

  public onExpandApp(id: string): void {
    this.store.dispatch(ExaminationsActions.expandExamination({ id }));
  }

  public onCollapseApp(id: string): void {
    this.store.dispatch(ExaminationsActions.collapseExamination({ id }));
  }
}
