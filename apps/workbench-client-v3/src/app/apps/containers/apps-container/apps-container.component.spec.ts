import { MaterialModule } from '@material/material.module';
import { createComponentFactory, Spectator } from '@ngneat/spectator';
import { StoreModule } from '@ngrx/store';
import { provideMockStore } from '@ngrx/store/testing';
import { CopyToClipboardDirective } from '@shared/directives/copy-to-clipboard.directive';
import { MockComponents } from 'ng-mocks';

import { AppsContainerComponent } from './apps-container.component';
import { AppsListComponent } from '@apps/components/apps-list/apps-list.component';
import { ExaminationsSelectors } from '@examinations/store';
import { DiagnosticMenuSelectors } from '@menu/store';
import { MenuItemLayoutComponent } from '@shared/containers/menu-item-layout/menu-item-layout.component';
import { AppsLabelComponent } from '@apps/components/apps-label/apps-label.component';
import { VendorAppSurfacesSelectors } from '@apps/store';

describe('AppsContainerComponent', () => {
  let spectator: Spectator<AppsContainerComponent>;

  const createComponent = createComponentFactory({
    component: AppsContainerComponent,
    imports: [
      StoreModule.forRoot({}),
      MaterialModule,
    ],
    declarations: [
      MockComponents(
        AppsListComponent,
        MenuItemLayoutComponent,
        AppsLabelComponent,
      ),
      CopyToClipboardDirective,
    ],
    providers: [provideMockStore({
      selectors: [
        {
          selector: ExaminationsSelectors.selectAllExaminations,
          value: []
        },
        {
          selector: ExaminationsSelectors.selectSelectedId,
          value: ''
        },
        {
          selector: ExaminationsSelectors.selectSelectionError,
          value: ''
        },
        {
          selector: ExaminationsSelectors.selectSelected,
          value: {}
        },
        {
          selector: DiagnosticMenuSelectors.selectAppsMenu,
          value: {}
        },
        {
          selector: ExaminationsSelectors.selectExaminationsLoaded,
          value: true
        },
        {
          selector: VendorAppSurfacesSelectors.selectActiveAppsEntities,
          value: {}
        },
        {
          selector: VendorAppSurfacesSelectors.selectFocusedAppId,
          value: ''
        },
        {
          selector: ExaminationsSelectors.selectPreSelected,
          value:''
        },
        {
          selector: ExaminationsSelectors.selectExpandedExaminationEntities,
          value: {}
        }
      ]
    })],
  });

  it('should create', () => {
    spectator = createComponent();
    expect(spectator.component).toBeTruthy();
  });
});
