import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AppsListComponent } from './components/apps-list/apps-list.component';
import { AppsContainerComponent } from './containers/apps-container/apps-container.component';
import { SharedModule } from '@shared/shared.module';
import { MaterialModule } from '@material/material.module';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { AppItemComponent } from './components/app-item/app-item.component';
import { AppHeaderComponent } from './components/app-header/app-header.component';
import { APPS_MODULE_FEATURE_KEY, reducers } from './store/apps-feature.state';
import { NewAppPanelComponent } from './components/new-app-panel/new-app-panel.component';
import {
  NewAppsEffects,
  VendorAppSurfacesEffects,
} from '@apps/store';
import { NewAppContentComponent } from './components/new-app-content/new-app-content.component';
import { NewAppDetailsComponent } from './components/new-app-details/new-app-details.component';
import { NewAppCardComponent } from './components/new-app-card/new-app-card.component';
import { AppsLabelComponent } from './components/apps-label/apps-label.component';
import { NewAppContainerComponent } from './containers/new-app-container/new-app-container.component';
import { VendorAppSurfacesContainerComponent } from './containers/vendor-app-surfaces-container/vendor-app-surfaces-container.component';
import { AppMappingService } from '@apps/services/app-mapping.service';
import { VendorAppIntegrationModule } from 'vendor-app-integration';
import { VendorAppSurfaceComponent } from './components/vendor-app-surface/vendor-app-surface.component';
import { VendorAppSurfaceListComponent } from './components/vendor-app-surface-list/vendor-app-surface-list.component';
import { AppPreselectedComponent } from './components/app-preselected/app-preselected.component';
import { AppTagComponent } from './components/app-tag/app-tag.component';

const COMPONENTS = [
  AppsListComponent,
  AppsContainerComponent,
  AppItemComponent,
  AppHeaderComponent,
  NewAppPanelComponent,
  NewAppContentComponent,
  NewAppDetailsComponent,
  NewAppCardComponent,
  AppsLabelComponent,
  NewAppContainerComponent,
  VendorAppSurfacesContainerComponent,
  VendorAppSurfaceComponent,
  VendorAppSurfaceListComponent,
  AppPreselectedComponent,
  AppTagComponent,
];

@NgModule({
  declarations: [...COMPONENTS],
  imports: [
    CommonModule,
    MaterialModule,
    VendorAppIntegrationModule,
    SharedModule,
    StoreModule.forFeature(APPS_MODULE_FEATURE_KEY, reducers),
    EffectsModule.forFeature([
      NewAppsEffects,
      VendorAppSurfacesEffects,
    ]),
  ],
  providers: [AppMappingService],
  exports: [
    AppsContainerComponent,
    NewAppContainerComponent,
    VendorAppSurfacesContainerComponent,
  ],
})
export class AppsModule {}
