import { Component, ChangeDetectionStrategy, Input, Output, EventEmitter } from '@angular/core';
import { App } from '@apps/store';


@Component({
  selector: 'app-new-app-panel',
  templateUrl: './new-app-panel.component.html',
  styleUrls: ['./new-app-panel.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class NewAppPanelComponent {
  @Input() public apps!: App[];
  @Input() public solutionStoreUrl!: string;
  @Input() public appsLoaded = true;

  @Output() public addAppToExamination = new EventEmitter<string>();
  @Output() public closePanel = new EventEmitter<MouseEvent>();
  @Output() public hoverOverApp = new EventEmitter<string | undefined>();

  public openSolutionStore(): void {
    window.open(this.solutionStoreUrl, '_blank');
  }

  public appsTrackBy(_index: number, app: App): string {
    return app.app_id;
  }
}
