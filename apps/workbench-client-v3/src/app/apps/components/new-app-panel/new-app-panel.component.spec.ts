import { MaterialModule } from '@material/material.module';
import { Spectator, createComponentFactory } from '@ngneat/spectator/jest';

import { NewAppPanelComponent } from './new-app-panel.component';
import { MockComponents } from 'ng-mocks';
import { NewAppCardComponent } from '@apps/components/new-app-card/new-app-card.component';

describe('NewAppPanelComponent', () => {
  let spectator: Spectator<NewAppPanelComponent>;

  const createComponent = createComponentFactory({
    component: NewAppPanelComponent,
    imports: [
      MaterialModule,
    ],
    declarations: [
      MockComponents(
        NewAppCardComponent,
      ),
    ],
  });

  beforeEach(() => spectator = createComponent());

  it('should create', () => {
    expect(spectator.component).toBeTruthy();
  });
});
