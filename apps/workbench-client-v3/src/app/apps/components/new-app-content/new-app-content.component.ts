import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { App } from '@apps/store';

@Component({
  selector: 'app-new-app-content',
  templateUrl: './new-app-content.component.html',
  styleUrls: ['./new-app-content.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class NewAppContentComponent {
  @Input() public app!: App;
}
