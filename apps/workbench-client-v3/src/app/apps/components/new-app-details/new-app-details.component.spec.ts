import { NewAppDetailsComponent } from './new-app-details.component';
import { createComponentFactory, Spectator } from '@ngneat/spectator';
import { MaterialModule } from '@material/material.module';

describe('NewAppDetailsComponent', () => {
  let spectator: Spectator<NewAppDetailsComponent>;
  const createComponent = createComponentFactory({
    component: NewAppDetailsComponent,
    imports: [
      MaterialModule,
    ],
  });

  beforeEach(() => spectator = createComponent());

  it('should create', () => {
    expect(spectator.component).toBeTruthy();
  });
});
