import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { App } from '@apps/store';

@Component({
  selector: 'app-new-app-details',
  templateUrl: './new-app-details.component.html',
  styleUrls: ['./new-app-details.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class NewAppDetailsComponent {
  @Input() public app!: App;

  goToLink(event: MouseEvent, url: string): void {
    window.open(url, '_blank');
    event.stopImmediatePropagation();
  }
}
