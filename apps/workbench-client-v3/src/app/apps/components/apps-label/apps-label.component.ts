import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { App } from '@apps/store';

@Component({
  selector: 'app-apps-label',
  templateUrl: './apps-label.component.html',
  styleUrls: ['./apps-label.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AppsLabelComponent {
  @Input() public app!: App;
  @Input() public selected!: boolean;
  @Input() public contentLoaded!: boolean;
  @Input() public disableNewButton!: boolean;

  @Output() public createItem = new EventEmitter<MouseEvent>();
}
