import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  Output,
} from '@angular/core';
import {
  Request,
  Scope,
  ScopeReady,
  Token,
  TokenReady,
  WbsUrl,
  WbsUrlReady,
} from 'vendor-app-integration';
import { VendorExaminationEntity } from '@apps/store/vendor-app-surfaces/vendor-app-surfaces.models';

@Component({
  selector: 'app-vendor-app-surface',
  templateUrl: './vendor-app-surface.component.html',
  styleUrls: ['./vendor-app-surface.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class VendorAppSurfaceComponent {
  @Input() activeExaminationId!: string;
  @Input() examination!: VendorExaminationEntity;
  @Input() scope!: Scope;
  @Input() token!: Token;
  @Input() wbsUrl!: WbsUrl;
  @Input() removedAppIds!: string[];

  @Output() receiveScopeReady = new EventEmitter<string>();
  @Output() receiveTokenReady = new EventEmitter<string>();
  @Output() receiveWbsUrlReady = new EventEmitter<string>();
  @Output() receiveTokenRequest = new EventEmitter<string>();

  onReceiveScopeReady(scopeReady: ScopeReady): void {
    if (scopeReady) {
      this.receiveScopeReady.emit(this.examination.id);
    }
  }

  onReceiveTokenReady(tokenReady: TokenReady): void {
    if (tokenReady) {
      this.receiveTokenReady.emit(this.examination.id);
    }
  }

  onReceiveWbsUrlReady(wbsUrlReady: WbsUrlReady): void {
    if (wbsUrlReady) {
      this.receiveWbsUrlReady.emit(this.examination.id);
    }
  }

  onReceiveTokenRequest(tokenRequest: Request): void {
    if (tokenRequest) {
      this.receiveTokenRequest.emit(this.examination.id);
    }
  }
}
