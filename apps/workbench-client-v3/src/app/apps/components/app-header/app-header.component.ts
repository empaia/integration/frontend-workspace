import { Component, ChangeDetectionStrategy, Input} from '@angular/core';
import { App } from '@apps/store';

type HeaderWrap = 'no-wrap' | 'wrap';

@Component({
  selector: 'app-app-header',
  templateUrl: './app-header.component.html',
  styleUrls: ['./app-header.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AppHeaderComponent {
  @Input() public appItem!: App;
  @Input() public headerWrap: HeaderWrap = 'no-wrap';
}
