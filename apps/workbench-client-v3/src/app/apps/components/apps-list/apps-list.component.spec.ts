import { AppsListComponent } from './apps-list.component';
import { NewItemButtonComponent } from '@shared/components/new-item-button/new-item-button.component';
import { MockComponent } from 'ng-mocks';
import { MissingSelectionErrorComponent } from '@shared/components/missing-selection-error/missing-selection-error.component';
import { AppHeaderComponent } from '../app-header/app-header.component';
import { AppItemComponent } from '../app-item/app-item.component';
import { CopyToClipboardDirective } from '@shared/directives/copy-to-clipboard.directive';
import { createComponentFactory, Spectator } from '@ngneat/spectator';
import { MaterialModule } from '@material/material.module';

describe('AppsListComponent', () => {
  let spectator: Spectator<AppsListComponent>;
  const createComponent = createComponentFactory({
    component: AppsListComponent,
    imports: [
      MaterialModule
    ],
    declarations: [
      MockComponent(MissingSelectionErrorComponent),
      MockComponent(NewItemButtonComponent),
      MockComponent(AppHeaderComponent),
      MockComponent(AppItemComponent),
      CopyToClipboardDirective,
    ]
  });


  it('should create', () => {
    spectator = createComponent({ props: { examinationList: [] } });

    expect(spectator.component).toBeTruthy();
  });
});
