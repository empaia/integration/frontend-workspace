import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnChanges, Output, SimpleChanges } from '@angular/core';
import { IdSelection, SelectionError } from '@menu/models/ui.models';
import { DiagnosticMenuItem } from '@menu/models/menu.models';
import { Dictionary } from '@ngrx/entity';
import { Examination, ExaminationExpansion } from '@examinations/store';
import { VendorExaminationEntity } from '@apps/store/vendor-app-surfaces/vendor-app-surfaces.models';
import { WbcV3ExaminationState } from 'empaia-api-lib';

@Component({
  selector: 'app-apps-list',
  templateUrl: './apps-list.component.html',
  styleUrls: ['./apps-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AppsListComponent implements OnChanges {

  @Input() public examinationList!: Examination[];
  @Input() public selectedExamination!: string;
  @Input() public preselectedExamination!: string;
  @Input() public selectionMissing!: SelectionError;
  @Input() public disableNewButton!: boolean;
  @Input() public examinationExpansionDictionary!: Dictionary<ExaminationExpansion>;

  @Input() public runningApps!: ReadonlyMap<string, VendorExaminationEntity>;
  @Input() public activeApp!: string;

  @Input() public hoveredNewApp: string | undefined;

  @Output() public examinationPreselected = new EventEmitter<IdSelection>();
  @Output() public cancelSelected = new EventEmitter<void>();
  @Output() public examinationSelected = new EventEmitter<IdSelection>();
  @Output() public closeExamination = new EventEmitter<IdSelection>();
  @Output() public addNewApp = new EventEmitter<MouseEvent>();
  @Output() public closeApp = new EventEmitter<IdSelection>();
  @Output() public errorClicked = new EventEmitter<DiagnosticMenuItem>();
  @Output() public expandApp = new EventEmitter<string>();
  @Output() public collapseApp = new EventEmitter<string>();

  public highlightedApp: Examination | undefined = undefined;

  public ngOnChanges(changes: SimpleChanges): void {
    if (changes['examinationList'] || changes['hoveredNewApp']) {
      this.highlightedApp = this.findHighlightedApp(this.examinationList, this.hoveredNewApp);
    }
  }

  public trackByExaminationId(_index: number, examination: Examination): string {
    return examination.id;
  }

  private findHighlightedApp(examinations: Examination[], hoveredApp?: string | undefined): Examination | undefined {
    return examinations.find(e => e.app_id === hoveredApp && e.state === WbcV3ExaminationState.Open);
  }
}
