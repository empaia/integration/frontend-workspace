import { VendorAppSurfaceListComponent } from './vendor-app-surface-list.component';
import { createComponentFactory, Spectator } from '@ngneat/spectator';
import { MockComponents } from 'ng-mocks';
import { VendorAppSurfaceComponent } from '@apps/components/vendor-app-surface/vendor-app-surface.component';

describe('VendorAppSurfaceListComponent', () => {
  let spectator: Spectator<VendorAppSurfaceListComponent>;
  const createComponent = createComponentFactory({
    component: VendorAppSurfaceListComponent,
    declarations: [
      MockComponents(
        VendorAppSurfaceComponent
      )
    ]
  });

  beforeEach(() => spectator = createComponent());

  it('should create', () => {
    expect(spectator.component).toBeTruthy();
  });
});
