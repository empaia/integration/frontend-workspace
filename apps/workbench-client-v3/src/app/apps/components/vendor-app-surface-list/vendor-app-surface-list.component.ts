import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  Output,
} from '@angular/core';
import { Scope, Token, WbsUrl } from 'vendor-app-integration';
import { ScopeEntity } from '@scope/store';
import { TokenEntity } from '@token/store';
import { Dictionary } from '@ngrx/entity';
import { VendorExaminationEntity } from '@apps/store/vendor-app-surfaces/vendor-app-surfaces.models';

@Component({
  selector: 'app-vendor-app-surface-list',
  templateUrl: './vendor-app-surface-list.component.html',
  styleUrls: ['./vendor-app-surface-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class VendorAppSurfaceListComponent {
  @Input() examinations!: VendorExaminationEntity[];
  @Input() focusExaminationId!: string;
  @Input() scopes!: Dictionary<ScopeEntity>;
  @Input() tokens!: Dictionary<TokenEntity>;
  @Input() wbsUrl!: WbsUrl;
  @Input() removedAppIds!: string[];

  @Output() receiveScopeReady = new EventEmitter<string>();
  @Output() receiveTokenReady = new EventEmitter<string>();
  @Output() receiveTokenRequest = new EventEmitter<string>();
  @Output() receiveWbsUrlReady = new EventEmitter<string>();

  getScope(examination: VendorExaminationEntity): Scope | undefined {
    return this.scopes[examination.id]?.scope;
  }

  getToken(examination: VendorExaminationEntity): Token | undefined {
    return this.tokens[examination.id]?.token;
  }
}
