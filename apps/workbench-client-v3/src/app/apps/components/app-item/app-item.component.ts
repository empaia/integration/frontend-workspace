import { AfterViewInit, ChangeDetectionStrategy, ChangeDetectorRef, Component, ElementRef, EventEmitter, Input, Output, ViewChild } from '@angular/core';
import { Examination } from '@examinations/store/examinations/examinations.models';
import { IdSelection } from '@menu/models/ui.models';
import { isEllipsisActive } from '@helper/html-ellipsis-calculation';

@Component({
  selector: 'app-app-item',
  templateUrl: './app-item.component.html',
  styleUrls: ['./app-item.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AppItemComponent implements AfterViewInit {
  @ViewChild('appDescriptionSpan') private appDescription!: ElementRef;

  @Input() public examination!: Examination;
  @Input() public selectedExamination!: string;
  @Input() public expanded!: boolean;

  @Input() public appActive = false;
  @Input() public appRunning = false;

  @Input() public hoveredNewApp!: boolean;

  @Output() public itemSelected = new EventEmitter<IdSelection>();
  @Output() public closeApp = new EventEmitter<IdSelection>();
  @Output() public expandApp = new EventEmitter<string>();
  @Output() public collapseApp = new EventEmitter<string>();
  @Output() public closeExamination = new EventEmitter<IdSelection>();

  public disabled = false;

  constructor(
    private changeRef: ChangeDetectorRef,
  ) {}

  private isDisabled(): boolean {
    return this.appDescription ? !isEllipsisActive(this.appDescription.nativeElement) : true;
  }

  public ngAfterViewInit(): void {
    this.disabled = this.isDisabled();
    this.changeRef.detectChanges();
  }

  public select() {
    this.itemSelected.emit({ id: this.examination.id });
  }

  public goToLink(url: string | undefined, clickEvent: Event){
    if(url) {
      window.open(url, "_blank");
    }
    clickEvent.stopPropagation();
  }

  public toggleExpansion(expanded: boolean): void {
    if (expanded) {
      this.expandApp.emit(this.examination.id);
    } else {
      this.collapseApp.emit(this.examination.id);
    }
  }

  public validateStringLength(s: string | undefined, length: number): boolean {
    if (s) {
      return s.length <= length;
    } else {
      return true;
    }
  }

  public onCloseApp(id: string): void {
    this.closeApp.emit({ id });
  }

  public onCloseExamination(event: MouseEvent, id: string): void {
    event.stopImmediatePropagation();
    this.closeExamination.emit({ id });
  }
}
