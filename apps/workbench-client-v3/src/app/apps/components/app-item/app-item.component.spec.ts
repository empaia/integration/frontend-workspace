import { MaterialModule } from '@material/material.module';
import { createComponentFactory, Spectator } from '@ngneat/spectator';
import { ExpansionButtonComponent } from '@shared/components/expansion-button/expansion-button.component';
import { ExpansionItemComponent } from '@shared/components/expansion-item/expansion-item.component';
import { CopyToClipboardDirective } from '@shared/directives/copy-to-clipboard.directive';
import { MockComponents } from 'ng-mocks';
import { AppHeaderComponent } from '../app-header/app-header.component';

import { AppItemComponent } from './app-item.component';
import { CloseButtonComponent } from '@shared/components/close-button/close-button.component';
import { Examination, ExaminationCreatorType, ExaminationState } from '@examinations/store';
import { App } from '@apps/store';

describe('AppItemComponent', () => {
  let spectator: Spectator<AppItemComponent>;

  const createExaminationEnity = (id: string, appId: string, appVersion?: string) => ({
    id,
    app_id: appId,
    api_version: 'v3',
    case_id: 'CASE-ID',
    created_at: 0,
    updated_at: 0,
    creator_id: '123',
    creator_type: ExaminationCreatorType.User,
    state: ExaminationState.Open,
    jobs: [],
    jobs_count: 0,
    jobs_count_finished: 0,
    app: createAppsEntity(appId, appVersion)
  } as Examination);

  const createAppsEntity = (id: string, appVersion = '') =>
    ({
      app_id: id,
      app_version: appVersion || `version-${id}`,
      store_description: 'desc',
      store_docs_url: 'docs',
      store_icon_url: 'icon',
      store_url: 'store_url',
    } as App);

  const createComponent = createComponentFactory({
    component: AppItemComponent,
    imports: [
      MaterialModule,
    ],
    declarations: [
      MockComponents(
        ExpansionItemComponent,
        ExpansionButtonComponent,
        AppHeaderComponent,
        CloseButtonComponent,
      ),
      CopyToClipboardDirective,
    ],
  });

  it('should create', () => {
    spectator = createComponent(
      {
        props: {
          examination: createExaminationEnity('1234', '5678')
        }
      }
    );

    expect(spectator.component).toBeTruthy();
  });

  it.todo('provide real tests for AppItemComponent');
});
