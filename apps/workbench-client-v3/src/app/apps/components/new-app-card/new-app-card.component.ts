import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { App } from '@apps/store';

@Component({
  selector: 'app-new-app-card',
  templateUrl: './new-app-card.component.html',
  styleUrls: ['./new-app-card.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class NewAppCardComponent {
  @Input() public app!: App;

  @Output() public addAppToExamination = new EventEmitter<string>();
  @Output() public hoverOverApp = new EventEmitter<string | undefined>();

  public onAppCardClicked(): void {
    this.addAppToExamination.emit(this.app.app_id);
  }
}
