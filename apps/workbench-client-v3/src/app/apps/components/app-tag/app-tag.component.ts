import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { WbcV3AppTagInput } from 'empaia-api-lib';

@Component({
  selector: 'app-app-tag',
  templateUrl: './app-tag.component.html',
  styleUrls: ['./app-tag.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AppTagComponent {
  @Input() public tag!: WbcV3AppTagInput[];
}
