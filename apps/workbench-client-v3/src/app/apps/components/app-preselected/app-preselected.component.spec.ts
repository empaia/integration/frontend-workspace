import { AppPreselectedComponent } from './app-preselected.component';
import { createComponentFactory, Spectator } from '@ngneat/spectator';
import { MaterialModule } from '@material/material.module';
import { MockComponents } from 'ng-mocks';
import { AppHeaderComponent } from '@apps/components/app-header/app-header.component';
import { Examination, ExaminationCreatorType, ExaminationState } from '@examinations/store';
import { App } from '@apps/store';

describe('AppPreselectedComponent', () => {
  const createExaminationEnity = (id: string, appId: string, apiVersion?: string) => ({
    id,
    app_id: appId,
    api_version: apiVersion,
    case_id: 'CASE-ID',
    created_at: 0,
    updated_at: 0,
    creator_id: '123',
    creator_type: ExaminationCreatorType.User,
    state: ExaminationState.Open,
    jobs: [],
    jobs_count: 0,
    jobs_count_finished: 0,
    app: createAppsEntity(appId, apiVersion)
  } as Examination);

  const createAppsEntity = (id: string, appVersion = '') =>
    ({
      app_id: id,
      app_version: appVersion || `version-${id}`,
      store_description: 'desc',
      store_docs_url: 'docs',
      store_icon_url: 'icon',
      store_url: 'store_url',
    } as App);

  let spectator: Spectator<AppPreselectedComponent>;
  const createComponent = createComponentFactory({
    component: AppPreselectedComponent,
    imports: [
      MaterialModule,
    ],
    declarations: [
      MockComponents(
        AppHeaderComponent,
      )
    ]
  });

  beforeEach(() => spectator = createComponent({
    props: {
      examination: createExaminationEnity('1234', '5678')
    }
  }));

  it('should create', () => {
    expect(spectator.component).toBeTruthy();
  });
});
