import { Component, ChangeDetectionStrategy, Input, Output, EventEmitter } from '@angular/core';
import { Examination } from '@examinations/store';
import { IdSelection } from '@menu/models/ui.models';

@Component({
  selector: 'app-app-preselected',
  templateUrl: './app-preselected.component.html',
  styleUrls: ['./app-preselected.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AppPreselectedComponent {
  @Input() examination!: Examination;

  @Output() examinationSelected = new EventEmitter<IdSelection>();
  @Output() cancelSelected = new EventEmitter<void>();

  select(): void {
    this.examinationSelected.emit({ id: this.examination.id });
  }
}
