import { Injectable } from '@angular/core';

@Injectable()
export class AppMappingService {
  private appIdCache: Set<string> = new Set<string>();

  getAll(): Set<string> {
    return this.appIdCache;
  }

  add(id: string): void {
    this.appIdCache.delete(id);
    this.appIdCache.add(id);
  }

  addMany(ids: string[]): void {
    for (const id of ids) {
      this.add(id);
    }
  }

  discard(count: number): string [] {
    const discard = Array.from(this.appIdCache).splice(0, count);
    for (const id of discard) {
      this.appIdCache.delete(id);
    }
    return discard;
  }

  clear(): void {
    this.appIdCache.clear();
  }
}
