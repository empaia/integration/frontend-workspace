import { MatTooltipDefaultOptions } from '@angular/material/tooltip';

export const customTooltipDefaultOptions: MatTooltipDefaultOptions = {
  showDelay: 0,
  hideDelay: 0,
  touchendHideDelay: 1500,
  position: 'above',
};
