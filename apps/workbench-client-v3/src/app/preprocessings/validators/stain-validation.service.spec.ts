import { provideMockStore } from '@ngrx/store/testing';
import { createServiceFactory, SpectatorService, mockProvider } from '@ngneat/spectator/jest';
import { StainValidationService } from './stain-validation.service';
import { TranslatePipe } from '@shared/pipes/translate.pipe';

describe('StainValidatorService', () => {
  let spectator: SpectatorService<StainValidationService>;
  const createService = createServiceFactory({
    service: StainValidationService,
    providers: [
      provideMockStore(),
      mockProvider(TranslatePipe)
    ]
  });

  beforeEach(() => spectator = createService());

  it('should be created', () => {
    expect(spectator.service).toBeTruthy();
  });
});
