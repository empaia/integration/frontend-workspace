/* eslint-disable @ngrx/avoid-mapping-selectors */
import { NewAppsSelectors } from '@apps/store';
import { Store } from '@ngrx/store';
import { AbstractControl, AsyncValidator, ValidationErrors } from '@angular/forms';
import { Injectable } from '@angular/core';
import { Observable, take, map } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AppValidationService implements AsyncValidator {

  constructor(
    private store: Store
  ) { }

  public validate(control: AbstractControl): Observable<ValidationErrors | null> {
    return this.store.select(NewAppsSelectors.selectAllNewApps).pipe(
      // take is importent to let the AbstractControl know that the observable completed
      take(1),
      map(apps =>
        apps?.length && apps.find(app => (app.vendor_name + ' ' + app.name_short + ' ' + app.app_version).toLowerCase() === control.value.toLowerCase())
          ? null
          : { appNotExisting: true } as ValidationErrors
      )
    );
  }
}
