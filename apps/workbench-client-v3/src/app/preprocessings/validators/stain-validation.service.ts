import { TagsSelectors } from '@preprocessings/store';
import { Injectable } from '@angular/core';
import { AbstractControl, AsyncValidator, ValidationErrors } from '@angular/forms';
import { Store } from '@ngrx/store';
import { TranslatePipe } from '@shared/pipes/translate.pipe';
import { Observable, map, take } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class StainValidationService implements AsyncValidator {

  constructor(
    private store: Store,
    private translate: TranslatePipe,
  ) { }

  public validate(control: AbstractControl): Observable<ValidationErrors | null> {
    return this.store.select(TagsSelectors.selectStainTags).pipe(
      // take is importent to let the AbstractControl know that the observable completed
      take(1),
      map(stains =>
        stains?.length && stains.find(stain => this.translate.transform(stain.tag_translations).toLowerCase() === control.value.toLowerCase())
          ? null
          : { stainNotExisting: true } as ValidationErrors
      )
    );
  }
}
