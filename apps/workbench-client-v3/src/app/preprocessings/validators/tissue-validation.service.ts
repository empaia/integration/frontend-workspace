import { take } from 'rxjs/operators';
import { TagsSelectors } from '@preprocessings/store';
import { AbstractControl, ValidationErrors, AsyncValidator } from '@angular/forms';
import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { TranslatePipe } from '@shared/pipes/translate.pipe';
import { Observable, map } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TissueValidationService implements AsyncValidator {

  constructor(
    private store: Store,
    private translate: TranslatePipe
  ) { }

  public validate(control: AbstractControl): Observable<ValidationErrors | null> {
    return this.store.select(TagsSelectors.selectTissueTags).pipe(
      // take is importent to let the AbstractControl know that the observable completed
      take(1),
      map(tissues =>
        tissues?.length && tissues.find(tissue => this.translate.transform(tissue.tag_translations).toLowerCase() === control.value.toLowerCase())
          ? null
          : { tissueNotExisting: true } as ValidationErrors
      )
    );
  }
}
