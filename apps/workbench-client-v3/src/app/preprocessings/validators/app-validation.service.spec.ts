import { provideMockStore } from '@ngrx/store/testing';
import { createServiceFactory, SpectatorService } from '@ngneat/spectator/jest';
import { AppValidationService } from './app-validation.service';

describe('AppValidationService', () => {
  let spectator: SpectatorService<AppValidationService>;
  const createService = createServiceFactory({
    service: AppValidationService,
    providers: [
      provideMockStore()
    ]
  });

  beforeEach(() => spectator = createService());

  it('should be created', () => {
    expect(spectator.service).toBeTruthy();
  });
});
