import { provideMockStore } from '@ngrx/store/testing';
import { createServiceFactory, mockProvider, SpectatorService } from '@ngneat/spectator/jest';
import { TissueValidationService } from './tissue-validation.service';
import { TranslatePipe } from '@shared/pipes/translate.pipe';

describe('TissueValidatorService', () => {
  let spectator: SpectatorService<TissueValidationService>;
  const createService = createServiceFactory({
    service: TissueValidationService,
    providers: [
      provideMockStore(),
      mockProvider(TranslatePipe)
    ]
  });

  beforeEach(() => spectator = createService());

  it('should be created', () => {
    expect(spectator.service).toBeTruthy();
  });
});
