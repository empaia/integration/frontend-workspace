import * as PreprocessingTriggersActions from './preprocessing-triggers/preprocessing-triggers.actions';
import * as PreprocessingTriggersFeature from './preprocessing-triggers/preprocessing-triggers.reducer';
import * as PreprocessingTriggersSelectors from './preprocessing-triggers/preprocessing-triggers.selectors';
export * from './preprocessing-triggers/preprocessing-triggers.effects';

export { PreprocessingTriggersActions, PreprocessingTriggersFeature, PreprocessingTriggersSelectors };

import * as TagsActions from './tags/tags.actions';
import * as TagsFeature from './tags/tags.reducer';
import * as TagsSelectors from './tags/tags.selectors';
export * from './tags/tags.effects';

export { TagsActions, TagsFeature, TagsSelectors };
