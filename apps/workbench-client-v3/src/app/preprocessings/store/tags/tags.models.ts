import { WbcV3TagListInput, WbcV3AppTagInput } from 'empaia-api-lib';
import { TAGS_LOAD_ERROR } from '@menu/models/ui.models';

export type TagList = WbcV3TagListInput;
export type AppTag =WbcV3AppTagInput;

export const TagsErrorMap = {
  '[Tags] Load All Tags Failure': TAGS_LOAD_ERROR,
};
