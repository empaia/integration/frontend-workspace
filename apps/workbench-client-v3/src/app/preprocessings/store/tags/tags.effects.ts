import { map, exhaustMap } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import * as TagsActions from './tags.actions';
import * as TagsFeature from './tags.reducer';
import * as PreprocessingTriggersActions from '@preprocessings/store/preprocessing-triggers/preprocessing-triggers.actions';
import { Store } from '@ngrx/store';
import { WbcV3TagsService } from 'empaia-api-lib';
import { fetch } from '@ngrx/router-store/data-persistence';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ErrorSnackbarComponent } from '@shared/components/error-snackbar/error-snackbar.component';
import { TagsErrorMap } from './tags.models';
import { of } from 'rxjs';

@Injectable()
export class TagsEffects {
  loadAllTags$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(PreprocessingTriggersActions.loadAllPreprocessingTriggers),
      map(() => TagsActions.loadAllTags())
    );
  });

  loadTags$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(TagsActions.loadAllTags),
      fetch({
        run: (
          _action: ReturnType<typeof TagsActions.loadAllTags>,
          _state: TagsFeature.State
        ) => {
          return this.tagsService
            .tagsGet()
            .pipe(
              map((tagList) => TagsActions.loadAllTagsSuccess({ tagList }))
            );
        },
        onError: (
          _action: ReturnType<typeof TagsActions.loadAllTags>,
          error
        ) => {
          return TagsActions.loadAllTagsFailure({ error });
        },
      })
    );
  });

  // clear selected tags when a trigger was posted
  clearSelectedTags$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(PreprocessingTriggersActions.createPreprocessingTrigger),
      map(() => TagsActions.clearSelectedTags())
    );
  });

  showErrorMessage$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(TagsActions.loadAllTagsFailure),
      exhaustMap((action) => {
        this.snackBar.openFromComponent(ErrorSnackbarComponent, {
          data: { titel: TagsErrorMap[action.type], error: action.error },
        });
        return of({ type: 'noop' });
      })
    );
  });

  constructor(
    private readonly actions$: Actions,
    private readonly store: Store,
    private readonly tagsService: WbcV3TagsService,
    private readonly snackBar: MatSnackBar
  ) {}
}
