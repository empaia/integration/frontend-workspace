import { HttpErrorResponse } from '@angular/common/http';
import { createReducer, on } from '@ngrx/store';
import * as TagsActions from './tags.actions';
import { TagList } from './tags.models';


export const TAGS_FEATURE_KEY = 'tags';

export interface State {
  tagList: TagList | undefined;
  loaded: boolean;
  selectedTissues: string[] | undefined;
  selectedStains: string[] | undefined;
  error?: HttpErrorResponse | undefined;
}

export const initialState: State = {
  tagList: undefined,
  loaded: true,
  selectedTissues: undefined,
  selectedStains: undefined,
  error: undefined,
};

export const reducer = createReducer(
  initialState,
  on(TagsActions.loadAllTags, (state): State => ({
    ...state,
    loaded: false,
  })),
  on(TagsActions.loadAllTagsSuccess, (state, { tagList }): State => ({
    ...state,
    tagList,
    loaded: true,
  })),
  on(TagsActions.loadAllTagsFailure, (state, { error }): State => ({
    ...state,
    loaded: true,
    error,
  })),
  on(TagsActions.selectTissues, (state, { tissues }): State => ({
    ...state,
    selectedTissues: tissues,
  })),
  on(TagsActions.selectStains, (state, { stains }): State => ({
    ...state,
    selectedStains: stains,
  }))
);
