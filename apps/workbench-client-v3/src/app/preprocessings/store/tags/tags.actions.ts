import { HttpErrorResponse } from '@angular/common/http';
import { createAction, props } from '@ngrx/store';
import { TagList } from './tags.models';

export const loadAllTags = createAction(
  '[Tags] Load All Tags',
);

export const loadAllTagsSuccess = createAction(
  '[Tags] Load All Tags Success',
  props<{ tagList: TagList }>()
);

export const loadAllTagsFailure = createAction(
  '[Tags] Load All Tags Failure',
  props<{ error: HttpErrorResponse }>()
);

export const selectTissues = createAction(
  '[Tags] Select Tissues',
  props<{ tissues: string[] | undefined }>()
);

export const selectStains = createAction(
  '[Tags] Select Stains',
  props<{ stains: string[] | undefined }>()
);

export const clearSelectedTags = createAction(
  '[Tags] Clear Selected Tags',
);
