import { createSelector } from '@ngrx/store';
import {
  State as ModuleState,
  selectPreprocessingsFeatureState,
} from '../preprocessings-feature.state';

export const selectTagsState = createSelector(
  selectPreprocessingsFeatureState,
  (state: ModuleState) => state.tags
);

export const selectTagList = createSelector(
  selectTagsState,
  (state) => state.tagList
);

export const selectTissueTags = createSelector(
  selectTagList,
  (tagList) => tagList?.tissues
);

export const selectStainTags = createSelector(
  selectTagList,
  (tagList) => tagList?.stains
);

export const selectIndicationTags = createSelector(
  selectTagList,
  (tagList) => tagList?.indications
);

export const selectAnalysis = createSelector(
  selectTagList,
  (tagList) => tagList?.analysis
);

export const selectTagListLoaded = createSelector(
  selectTagsState,
  (state) => state.loaded
);

export const selectTagListError = createSelector(
  selectTagsState,
  (state) => state.error
);

export const selectSelectedTissues = createSelector(
  selectTagsState,
  (state) => state.selectedTissues
);

export const selectSelectedStains = createSelector(
  selectTagsState,
  (state) => state.selectedStains
);
