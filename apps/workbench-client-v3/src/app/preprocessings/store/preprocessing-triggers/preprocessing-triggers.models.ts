import { WbcV3PostPreprocessingTrigger, WbcV3ExtendedPreprocessingTrigger } from 'empaia-api-lib';
import { CREATE_PREPROCESSING_TRIGGER_ERROR, DELETE_PREPROCESSING_TRIGGER_ERROR, PREPROCESSING_TRIGGER_LOAD_ERROR } from '@menu/models/ui.models';

export type PostPreprocessingTrigger = WbcV3PostPreprocessingTrigger;
export type ExtendedPreprocessingTrigger = WbcV3ExtendedPreprocessingTrigger;

export const PreprocessingTriggerErrorMap = {
  '[Preprocessing-Triggers] Load All Preprocessing Triggers Failure': PREPROCESSING_TRIGGER_LOAD_ERROR,
  '[Preprocessing-Triggers] Create Preprocessing Trigger Failure': CREATE_PREPROCESSING_TRIGGER_ERROR,
  '[Preprocessing-Triggers] Delete Preprocessing Trigger Failure': DELETE_PREPROCESSING_TRIGGER_ERROR,
};
