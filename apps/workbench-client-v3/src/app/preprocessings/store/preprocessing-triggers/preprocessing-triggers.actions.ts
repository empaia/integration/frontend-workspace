import { HttpErrorResponse } from '@angular/common/http';
import { createAction, props } from '@ngrx/store';
import { ExtendedPreprocessingTrigger, PostPreprocessingTrigger } from './preprocessing-triggers.models';

export const loadAllPreprocessingTriggers = createAction(
  '[Preprocessing-Triggers] Load All Preprocessing Triggers',
);

export const loadAllPreprocessingTriggersSuccess = createAction(
  '[Preprocessing-Triggers] Load All Preprocessing Triggers Success',
  props<{ preprocessingTriggers: ExtendedPreprocessingTrigger[] }>()
);

export const loadAllPreprocessingTriggersFailure = createAction(
  '[Preprocessing-Triggers] Load All Preprocessing Triggers Failure',
  props<{ error: HttpErrorResponse }>()
);

export const createPreprocessingTrigger = createAction(
  '[Preprocessing-Triggers] Create Preprocessing Trigger',
  props<{ postPreprocessingTrigger: PostPreprocessingTrigger }>()
);

export const createPreprocessingTriggerFailure = createAction(
  '[Preprocessing-Triggers] Create Preprocessing Trigger Failure',
  props<{ error: HttpErrorResponse }>()
);

export const deletePreprocessingTrigger = createAction(
  '[Preprocessing-Triggers] Delete Preprocessing Trigger',
  props<{ preprocessingTriggerId: string }>()
);

export const deletePreprocessingTriggerFailure = createAction(
  '[Preprocessing-Triggers] Delete Preprocessing Trigger Failure',
  props<{ error: HttpErrorResponse }>()
);
