import { HttpErrorResponse } from '@angular/common/http';
import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';
import { createReducer, on } from '@ngrx/store';
import * as PreprocesingTriggersActions from './preprocessing-triggers.actions';
import { ExtendedPreprocessingTrigger } from './preprocessing-triggers.models';

export const PREPROCESSING_TRIGGER_FEATURE_KEY = 'preprocessingTriggers';

export interface State extends EntityState<ExtendedPreprocessingTrigger> {
  loaded: boolean;
  error?: HttpErrorResponse | undefined;
}

export const preprocessingTriggerAdapter: EntityAdapter<ExtendedPreprocessingTrigger> = createEntityAdapter<ExtendedPreprocessingTrigger>();

export const initialState: State = preprocessingTriggerAdapter.getInitialState({
  loaded: true,
  error: undefined,
});

export const reducer = createReducer(
  initialState,
  on(PreprocesingTriggersActions.loadAllPreprocessingTriggers, (state): State => ({
    ...state,
    loaded: false,
  })),
  on(PreprocesingTriggersActions.loadAllPreprocessingTriggersSuccess, (state, { preprocessingTriggers }): State =>
    preprocessingTriggerAdapter.setAll(preprocessingTriggers, {
      ...state,
      loaded: true,
    })
  ),
  on(PreprocesingTriggersActions.loadAllPreprocessingTriggersFailure, (state, { error }): State => ({
    ...state,
    loaded: true,
    error,
  })),
  on(PreprocesingTriggersActions.createPreprocessingTrigger, (state): State => ({
    ...state,
    loaded: false,
  })),
  on(PreprocesingTriggersActions.createPreprocessingTriggerFailure, (state, { error }): State => ({
    ...state,
    loaded: true,
    error,
  })),
  on(PreprocesingTriggersActions.deletePreprocessingTrigger, (state): State => ({
    ...state,
    loaded: false,
  })),
  on(PreprocesingTriggersActions.deletePreprocessingTriggerFailure, (state, { error }): State => ({
    ...state,
    loaded: true,
    error,
  })),
);
