import { Store } from '@ngrx/store';
import { PreprocessingTriggerErrorMap } from '@preprocessings/store/preprocessing-triggers/preprocessing-triggers.models';
import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { map, exhaustMap } from 'rxjs/operators';
import * as PreprocessingTriggersActions from './preprocessing-triggers.actions';
import * as PreprocessingTriggersFeature from './preprocessing-triggers.reducer';
import { fetch, pessimisticUpdate } from '@ngrx/router-store/data-persistence';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ErrorSnackbarComponent } from '@shared/components/error-snackbar/error-snackbar.component';
import { of } from 'rxjs';
import { WbcV3ConfigurationService, WbcV3AppsService } from 'empaia-api-lib';

@Injectable()
export class PreprocessingTriggersEffects {
  loadTriggers$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(PreprocessingTriggersActions.loadAllPreprocessingTriggers),
      fetch({
        run: (
          _action: ReturnType<
            typeof PreprocessingTriggersActions.loadAllPreprocessingTriggers
          >,
          _state: PreprocessingTriggersFeature.State
        ) => {
          return this.configurationService
            .configurationPreprocessingTriggersGet()
            .pipe(
              map((response) => response.items),
              map((preprocessingTriggers) =>
                PreprocessingTriggersActions.loadAllPreprocessingTriggersSuccess(
                  { preprocessingTriggers }
                )
              )
            );
        },
        onError: (
          _action: ReturnType<
            typeof PreprocessingTriggersActions.loadAllPreprocessingTriggers
          >,
          error
        ) => {
          return PreprocessingTriggersActions.loadAllPreprocessingTriggersFailure(
            { error }
          );
        },
      })
    );
  });

  createTigger$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(PreprocessingTriggersActions.createPreprocessingTrigger),
      pessimisticUpdate({
        run: (
          action: ReturnType<
            typeof PreprocessingTriggersActions.createPreprocessingTrigger
          >,
          _state: PreprocessingTriggersFeature.State
        ) => {
          return this.configurationService
            .configurationPreprocessingTriggersPost({
              body: action.postPreprocessingTrigger,
            })
            .pipe(
              map(() =>
                PreprocessingTriggersActions.loadAllPreprocessingTriggers()
              )
            );
        },
        onError: (
          _action: ReturnType<
            typeof PreprocessingTriggersActions.createPreprocessingTrigger
          >,
          error
        ) => {
          return PreprocessingTriggersActions.createPreprocessingTriggerFailure(
            { error }
          );
        },
      })
    );
  });

  deleteTrigger$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(PreprocessingTriggersActions.deletePreprocessingTrigger),
      pessimisticUpdate({
        run: (
          action: ReturnType<
            typeof PreprocessingTriggersActions.deletePreprocessingTrigger
          >,
          _state: PreprocessingTriggersFeature.State
        ) => {
          return this.configurationService
            .configurationPreprocessingTriggersPreprocessingTriggerIdDelete({
              preprocessing_trigger_id: action.preprocessingTriggerId,
            })
            .pipe(
              map(() =>
                PreprocessingTriggersActions.loadAllPreprocessingTriggers()
              )
            );
        },
        onError: (
          _action: ReturnType<
            typeof PreprocessingTriggersActions.deletePreprocessingTrigger
          >,
          error
        ) => {
          return PreprocessingTriggersActions.deletePreprocessingTriggerFailure(
            { error }
          );
        },
      })
    );
  });

  showErrorMessage$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(
        PreprocessingTriggersActions.loadAllPreprocessingTriggersFailure,
        PreprocessingTriggersActions.createPreprocessingTriggerFailure,
        PreprocessingTriggersActions.deletePreprocessingTriggerFailure
      ),
      exhaustMap((action) => {
        this.snackBar.openFromComponent(ErrorSnackbarComponent, {
          data: {
            titel: PreprocessingTriggerErrorMap[action.type],
            error: action.error,
          },
        });
        return of({ type: 'noop' });
      })
    );
  });

  constructor(
    private readonly actions$: Actions,
    private readonly store: Store,
    private readonly configurationService: WbcV3ConfigurationService,
    private readonly appsService: WbcV3AppsService,
    private readonly snackBar: MatSnackBar
  ) {}
}
