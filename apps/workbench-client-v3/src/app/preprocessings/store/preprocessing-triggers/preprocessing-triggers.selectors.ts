import { createSelector } from '@ngrx/store';
import {
  State as ModuleState,
  selectPreprocessingsFeatureState,
} from '../preprocessings-feature.state';
import { preprocessingTriggerAdapter } from './preprocessing-triggers.reducer';

const {
  selectAll,
  selectEntities,
} = preprocessingTriggerAdapter.getSelectors();

export const selectPreprocessingTriggerState = createSelector(
  selectPreprocessingsFeatureState,
  (state: ModuleState) => state.preprocessingTriggers
);

export const selectAllPreprocessingTriggers = createSelector(
  selectPreprocessingTriggerState,
  selectAll
);

export const selectPreprocessingTriggerEntities = createSelector(
  selectPreprocessingTriggerState,
  selectEntities
);

export const selectPreprocessingTriggersLoaded = createSelector(
  selectPreprocessingTriggerState,
  (state) => state.loaded
);

export const selectPreprocessingTriggersError = createSelector(
  selectPreprocessingTriggerState,
  (state) => state.error
);
