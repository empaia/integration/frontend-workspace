import { Action, combineReducers, createFeatureSelector } from '@ngrx/store';
import * as fromPreprocessingTriggers from '@preprocessings/store/preprocessing-triggers/preprocessing-triggers.reducer';
import * as fromTags from '@preprocessings/store/tags/tags.reducer';

export const PREPROCESSINGS_MODULE_FEATURE_KEY = 'preprocessingsModuleFeature';

export const selectPreprocessingsFeatureState = createFeatureSelector<State>(
  PREPROCESSINGS_MODULE_FEATURE_KEY
);

export interface State {
  [fromPreprocessingTriggers.PREPROCESSING_TRIGGER_FEATURE_KEY]: fromPreprocessingTriggers.State;
  [fromTags.TAGS_FEATURE_KEY]: fromTags.State;
}

export function reducers(state: State | undefined, action: Action) {
  return combineReducers({
    [fromPreprocessingTriggers.PREPROCESSING_TRIGGER_FEATURE_KEY]: fromPreprocessingTriggers.reducer,
    [fromTags.TAGS_FEATURE_KEY]: fromTags.reducer,
  })(state, action);
}
