import { PushPipe } from '@ngrx/component';
import { TagsEffects } from '@preprocessings/store/tags/tags.effects';
import { PreprocessingTriggersEffects } from '@preprocessings/store/preprocessing-triggers/preprocessing-triggers.effects';
import { EffectsModule } from '@ngrx/effects';
import { PREPROCESSINGS_MODULE_FEATURE_KEY, reducers } from '@preprocessings/store/preprocessings-feature.state';
import { StoreModule } from '@ngrx/store';
import { ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '@shared/shared.module';
import { MaterialModule } from '@material/material.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PreprocessingsLabelComponent } from './components/preprocessings-label/preprocessings-label.component';
import { PreprocessingsContentContainerComponent } from './containers/preprocessings-content-container/preprocessings-content-container.component';
import { PreprocessingTriggerCreationComponent } from './components/preprocessing-trigger-creation/preprocessing-trigger-creation.component';
import { PreprocessingTriggerListComponent } from './components/preprocessing-trigger-list/preprocessing-trigger-list.component';
import { PreprocessingTriggerItemComponent } from './components/preprocessing-trigger-item/preprocessing-trigger-item.component';



@NgModule({
  declarations: [
    PreprocessingsLabelComponent,
    PreprocessingsContentContainerComponent,
    PreprocessingTriggerCreationComponent,
    PreprocessingTriggerListComponent,
    PreprocessingTriggerItemComponent
  ],
  imports: [
    CommonModule,
    MaterialModule,
    ReactiveFormsModule,
    PushPipe,
    SharedModule,
    StoreModule.forFeature(
      PREPROCESSINGS_MODULE_FEATURE_KEY,
      reducers
    ),
    EffectsModule.forFeature([
      PreprocessingTriggersEffects,
      TagsEffects
    ])
  ],
  exports: [
    PreprocessingsLabelComponent,
    PreprocessingsContentContainerComponent
  ]
})
export class PreprocessingsModule { }
