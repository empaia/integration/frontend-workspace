import { ChangeDetectionStrategy, Component, Input, Output, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';
import { App } from '@apps/store';
import { ExtendedPreprocessingTrigger } from '@preprocessings/store/preprocessing-triggers/preprocessing-triggers.models';

@Component({
  selector: 'app-preprocessing-trigger-item',
  templateUrl: './preprocessing-trigger-item.component.html',
  styleUrls: ['./preprocessing-trigger-item.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PreprocessingTriggerItemComponent implements OnChanges {
  @Input() public trigger!: ExtendedPreprocessingTrigger;

  @Output() public removeTrigger = new EventEmitter<string>();

  public appName = '';

  public ngOnChanges(changes: SimpleChanges): void {
    if (changes.trigger) {
      this.appName = this.createAppName(this.trigger.app);
    }
  }

  private createAppName(app: App | undefined): string {
    return app ? app.vendor_name + ' ' + app.name_short + ' ' + app.app_version : '';
  }
}
