import { MaterialModule } from '@material/material.module';
import { createComponentFactory, Spectator } from '@ngneat/spectator/jest';
import { PreprocessingTriggerItemComponent } from './preprocessing-trigger-item.component';

describe('PreprocessingTriggerItemComponent', () => {
  let spectator: Spectator<PreprocessingTriggerItemComponent>;
  const createComponent = createComponentFactory({
    component: PreprocessingTriggerItemComponent,
    imports: [
      MaterialModule,
    ]
  });

  beforeEach(() => spectator = createComponent({
    props: {
      trigger: {
        id: 'Test_Id',
        tissue: 'Tissue',
        stain: 'Stain',
        portal_app_id: 'Id',
        creator_id: '',
        creator_type: 'user',
        app: {
          app_id: 'TEST_APP_ID',
          app_version: 'v3',
          store_description: 'desc',
          store_docs_url: 'docs',
          store_icon_url: 'icon',
          store_url: 'store_url',
          has_frontend: false,
          name_short: 'TEST_APP',
          portal_app_id: 'TEST_APP_ID',
          vendor_name: 'TEST_VENDOR_NAME',
          research_only: true
        }
      }
    }
  }));

  it('should create', () => {
    expect(spectator.component).toBeTruthy();
  });
});
