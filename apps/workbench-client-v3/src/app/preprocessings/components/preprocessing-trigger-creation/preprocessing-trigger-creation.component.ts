import { tap } from 'rxjs/operators';
import { ALL_MESSAGES } from './preprocessing-trigger-creation.models';
import { map, Observable, startWith } from 'rxjs';
import { ChangeDetectionStrategy, Component, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, FormGroupDirective, Validators, ValidationErrors, FormControl } from '@angular/forms';
import { TranslatePipe } from '@shared/pipes/translate.pipe';
import { Store } from '@ngrx/store';
import { TissueValidationService } from '@preprocessings/validators/tissue-validation.service';
import { StainValidationService } from '@preprocessings/validators/stain-validation.service';
import { AppValidationService } from '@preprocessings/validators/app-validation.service';
import { AppTag } from '@preprocessings/store/tags/tags.models';
import { App } from '@apps/store';
import { PostPreprocessingTrigger } from '@preprocessings/store/preprocessing-triggers/preprocessing-triggers.models';

@Component({
  selector: 'app-preprocessing-trigger-creation',
  templateUrl: './preprocessing-trigger-creation.component.html',
  styleUrls: ['./preprocessing-trigger-creation.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    TranslatePipe,
    { provide: TissueValidationService, deps: [Store, TranslatePipe] },
    { provide: StainValidationService, deps: [Store, TranslatePipe] },
    { provide: AppValidationService, deps: [Store] }
  ]
})
export class PreprocessingTriggerCreationComponent {
  public triggerForm!: FormGroup;
  public filteredTissues!: Observable<AppTag[]> | undefined;
  public filteredStains!: Observable<AppTag[]> | undefined;
  public filteredApps!: Observable<App[]> | undefined;

  @ViewChild('triggerFormDirective') private triggerFormDirective!: FormGroupDirective;

  private _tissues: AppTag[] = [];
  @Input() public set tissues(val: AppTag[]) {
    if (val) {
      this._tissues = val;
      this.filterTissues(val);
    }
  }
  public get tissues() {
    return this._tissues;
  }

  private _stains: AppTag[] = [];
  @Input() public set stains(val: AppTag[]) {
    if (val) {
      this._stains = val;
      this.filterStains(val);
    }
  }
  public get stains() {
    return this._stains;
  }

  private _apps: App[] = [];
  @Input() public set apps(val: App[]) {
    if (val) {
      this._apps = val;
      this.filterApps(val);
    }
  }
  public get apps() {
    return this._apps;
  }

  @Output() public postTrigger = new EventEmitter<PostPreprocessingTrigger>();
  @Output() public selectedTissue = new EventEmitter<string | undefined>();
  @Output() public selectedStain = new EventEmitter<string | undefined>();

  constructor(
    private fb: FormBuilder,
    private translate: TranslatePipe,
    private tvs: TissueValidationService,
    private svs: StainValidationService,
    private avs: AppValidationService,
  ) {
    this.createForm();
  }

  public onSubmit(): void {
    const postTrigger: PostPreprocessingTrigger = {
      tissue: this.findOption(this.tissueControl.value, this.tissues)?.name as string,
      stain: this.findOption(this.stainControl.value, this.stains)?.name as string,
      portal_app_id: this.apps.find(app => this.createAppName(app) === this.appControl.value)?.portal_app_id as string
    };
    this.postTrigger.emit(postTrigger);
    this.triggerFormDirective.resetForm();
    this.triggerForm.reset();
  }

  public createAppName(app: App): string {
    return app.vendor_name + ' ' + app.name_short + ' ' + app.app_version;
  }

  public getErrorMessages(controlName: string, errors: ValidationErrors | null | undefined): string[] | undefined {
    const message = ALL_MESSAGES[controlName];
    if (
      !errors ||
      !message ||
      !this.triggerForm.get(controlName)?.dirty
    ) {
      return undefined;
    }

    return Object.keys(errors).map(error => message[error]);
  }

  public get tissueControl(): FormControl<string> {
    return this.triggerForm.controls.tissue as FormControl<string>;
  }

  public get stainControl(): FormControl<string> {
    return this.triggerForm.controls.stain as FormControl<string>;
  }

  public get appControl(): FormControl<string> {
    return this.triggerForm.controls.app as FormControl<string>;
  }

  private createForm(): void {
    this.triggerForm = this.fb.nonNullable.group({
      tissue: ['', [Validators.required], [this.tvs]],
      stain: ['', [Validators.required], [this.svs]],
      app: ['', [Validators.required], [this.avs]],
    });
  }

  private filterTissues(tissues: AppTag[]): void {
    this.filteredTissues = this.tissueControl.valueChanges.pipe(
      startWith(''),
      map(value => this.filterOptions(value || '', tissues)),
      tap(tissues => this.selectedTissue.emit(this.findOption(this.tissueControl.value, tissues)?.name)),
      tap(() => this.appControl.patchValue(''))
    );
  }

  private filterStains(stains: AppTag[]): void {
    this.filteredStains = this.stainControl.valueChanges.pipe(
      startWith(''),
      map(value => this.filterOptions(value || '', stains)),
      tap(stains => this.selectedStain.emit(this.findOption(this.stainControl.value, stains)?.name)),
      tap(() => this.appControl.patchValue(''))
    );
  }

  private filterApps(apps: App[]): void {
    this.filteredApps = this.appControl.valueChanges.pipe(
      startWith(''),
      map(value => this.filterAppOptions(value || '', apps))
    );
  }

  private filterOptions(value: string, options: AppTag[]): AppTag[] {
    const filterValue = value.toLowerCase();

    return options.filter(option => this.translate.transform(option?.tag_translations).toLowerCase().includes(filterValue));
  }

  private filterAppOptions(value: string, options: App[]): App[] {
    const filterValue = value.toLowerCase();
    return options
      .filter(option =>
        this.createAppName(option).toLowerCase().includes(filterValue)
      );
  }

  private findOption(value: string, options: AppTag[]): AppTag | undefined {
    return options.find(option => this.translate.transform(option?.tag_translations).toLowerCase() === value.toLowerCase());
  }

}
