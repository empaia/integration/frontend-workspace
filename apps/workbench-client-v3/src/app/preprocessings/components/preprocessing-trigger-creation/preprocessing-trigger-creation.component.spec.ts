import { TissueValidationService } from '@preprocessings/validators/tissue-validation.service';
import { TranslatePipe } from '@shared/pipes/translate.pipe';
import { provideMockStore } from '@ngrx/store/testing';
import { FormBuilder, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '@material/material.module';
import { createComponentFactory, Spectator, mockProvider } from '@ngneat/spectator/jest';
import { PreprocessingTriggerCreationComponent } from './preprocessing-trigger-creation.component';
import { StainValidationService } from '@preprocessings/validators/stain-validation.service';
import { AppValidationService } from '@preprocessings/validators/app-validation.service';

describe('PreprocessingTriggerCreationComponent', () => {
  let spectator: Spectator<PreprocessingTriggerCreationComponent>;
  const createComponent = createComponentFactory({
    component: PreprocessingTriggerCreationComponent,
    imports: [
      MaterialModule,
      ReactiveFormsModule,
    ],
    providers: [
      FormBuilder,
      provideMockStore(),
      mockProvider(TranslatePipe),
      mockProvider(TissueValidationService),
      mockProvider(StainValidationService),
      mockProvider(AppValidationService),
    ]
  });

  beforeEach(() => spectator = createComponent());

  it('should create', () => {
    expect(spectator.component).toBeTruthy();
  });
});
