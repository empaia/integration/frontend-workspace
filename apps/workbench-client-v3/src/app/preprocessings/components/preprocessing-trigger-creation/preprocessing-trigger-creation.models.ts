export interface ErrorMessages {
  tissue: {
    required: string;
    tissueNotExisting: string;
  },
  stain: {
    required: string;
    stainNotExisting: string;
  },
  app: {
    required: string;
    appNotExisting: string;
  }
}

export const ALL_MESSAGES: ErrorMessages = {
  tissue: {
    required: 'A tissue must be chosen!',
    tissueNotExisting: 'This is an unsupported tissue!'
  },
  stain: {
    required: 'A stain must be chosen!',
    stainNotExisting: 'This is an unsupported stain!'
  },
  app: {
    required: 'An app must be chosen!',
    appNotExisting: 'This is an unsupported app!'
  }
};
