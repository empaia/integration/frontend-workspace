import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { ExtendedPreprocessingTrigger } from '@preprocessings/store/preprocessing-triggers/preprocessing-triggers.models';

@Component({
  selector: 'app-preprocessing-trigger-list',
  templateUrl: './preprocessing-trigger-list.component.html',
  styleUrls: ['./preprocessing-trigger-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PreprocessingTriggerListComponent {
  @Input() public triggers!: ExtendedPreprocessingTrigger[];

  @Output() public removeTrigger = new EventEmitter<string>();
}
