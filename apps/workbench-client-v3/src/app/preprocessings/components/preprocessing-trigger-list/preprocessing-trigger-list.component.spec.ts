import { MockComponents } from 'ng-mocks';
import { createComponentFactory, Spectator } from '@ngneat/spectator/jest';
import { PreprocessingTriggerListComponent } from './preprocessing-trigger-list.component';
import { PreprocessingTriggerItemComponent } from '../preprocessing-trigger-item/preprocessing-trigger-item.component';

describe('PreprocessingTriggerListComponent', () => {
  let spectator: Spectator<PreprocessingTriggerListComponent>;
  const createComponent = createComponentFactory({
    component: PreprocessingTriggerListComponent,
    declarations: [
      MockComponents(
        PreprocessingTriggerItemComponent
      )
    ]
  });

  beforeEach(() => spectator = createComponent());

  it('should create', () => {
    expect(spectator.component).toBeTruthy();
  });
});
