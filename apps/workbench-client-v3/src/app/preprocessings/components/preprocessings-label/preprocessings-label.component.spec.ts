import { MockComponents } from 'ng-mocks';
import { MenuLabelComponent } from '@shared/components/menu-label/menu-label.component';
import { createComponentFactory, Spectator } from '@ngneat/spectator/jest';
import { PreprocessingsLabelComponent } from './preprocessings-label.component';

describe('PreprocessingsLabelComponent', () => {
  let spectator: Spectator<PreprocessingsLabelComponent>;
  const createComponent = createComponentFactory({
    component: PreprocessingsLabelComponent,
    declarations: [
      MockComponents(
        MenuLabelComponent,
      )
    ]
  });

  beforeEach(() => spectator = createComponent());

  it('should create', () => {
    expect(spectator.component).toBeTruthy();
  });
});
