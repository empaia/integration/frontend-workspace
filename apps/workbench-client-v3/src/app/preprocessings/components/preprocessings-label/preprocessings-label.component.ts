import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { MenuState } from '@menu/models/menu.models';

@Component({
  selector: 'app-preprocessings-label',
  templateUrl: './preprocessings-label.component.html',
  styleUrls: ['./preprocessings-label.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PreprocessingsLabelComponent {
  @Input() public selected!: boolean;
  @Input() public contentLoaded!: boolean;
  @Input() public labelState!: MenuState;
  public MENU_STATE = MenuState;
}
