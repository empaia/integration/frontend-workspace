import { provideMockStore } from '@ngrx/store/testing';
import { MockComponents } from 'ng-mocks';
import { MaterialModule } from '@material/material.module';
import { createComponentFactory, Spectator } from '@ngneat/spectator/jest';
import { PreprocessingsContentContainerComponent } from './preprocessings-content-container.component';
import { PreprocessingTriggerCreationComponent } from '@preprocessings/components/preprocessing-trigger-creation/preprocessing-trigger-creation.component';
import { PreprocessingTriggerListComponent } from '@preprocessings/components/preprocessing-trigger-list/preprocessing-trigger-list.component';
import { PreprocessingTriggersSelectors, TagsSelectors } from '@preprocessings/store';
import { NewAppsSelectors } from '@apps/store';
import { PushPipe } from '@ngrx/component';

describe('PreprocessingsContentContainerComponent', () => {
  let spectator: Spectator<PreprocessingsContentContainerComponent>;
  const createComponent = createComponentFactory({
    component: PreprocessingsContentContainerComponent,
    imports: [
      MaterialModule,
      PushPipe,
    ],
    declarations: [
      MockComponents(
        PreprocessingTriggerCreationComponent,
        PreprocessingTriggerListComponent,
      ),
    ],
    providers: [
      provideMockStore({
        selectors: [
          {
            selector: PreprocessingTriggersSelectors.selectAllPreprocessingTriggers,
            value: []
          },
          {
            selector: TagsSelectors.selectTissueTags,
            value: undefined
          },
          {
            selector: TagsSelectors.selectStainTags,
            value: undefined
          },
          {
            selector: NewAppsSelectors.selectAllNewApps,
            value: []
          },
          {
            selector: NewAppsSelectors.selectNewAppsEntities,
            value: {}
          }
        ]
      })
    ]
  });

  beforeEach(() => spectator = createComponent());

  it('should create', () => {
    expect(spectator.component).toBeTruthy();
  });
});
