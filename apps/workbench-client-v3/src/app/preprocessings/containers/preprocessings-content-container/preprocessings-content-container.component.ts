import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { PreprocessingTriggersActions, PreprocessingTriggersSelectors, TagsActions, TagsSelectors } from '@preprocessings/store';
import { App, NewAppsSelectors } from '@apps/store';
import { AppTag } from '@preprocessings/store/tags/tags.models';
import { ExtendedPreprocessingTrigger, PostPreprocessingTrigger } from '@preprocessings/store/preprocessing-triggers/preprocessing-triggers.models';

@Component({
  selector: 'app-preprocessings-content-container',
  templateUrl: './preprocessings-content-container.component.html',
  styleUrls: ['./preprocessings-content-container.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PreprocessingsContentContainerComponent implements OnInit {
  public triggers$: Observable<ExtendedPreprocessingTrigger[]>;
  public tissues$: Observable<AppTag[] | undefined>;
  public stains$: Observable<AppTag[] | undefined>;
  public appList$: Observable<App[]>;

  constructor(private store: Store) {
    this.triggers$ = this.store.select(PreprocessingTriggersSelectors.selectAllPreprocessingTriggers);
    this.tissues$ = this.store.select(TagsSelectors.selectTissueTags);
    this.stains$ = this.store.select(TagsSelectors.selectStainTags);
    this.appList$ = this.store.select(NewAppsSelectors.selectAllNewApps);
  }

  ngOnInit(): void {
    this.store.dispatch(PreprocessingTriggersActions.loadAllPreprocessingTriggers());
  }

  onPostTrigger(postPreprocessingTrigger: PostPreprocessingTrigger): void {
    this.store.dispatch(PreprocessingTriggersActions.createPreprocessingTrigger({ postPreprocessingTrigger }));
  }

  onTriggerRemove(preprocessingTriggerId: string): void {
    this.store.dispatch(PreprocessingTriggersActions.deletePreprocessingTrigger({ preprocessingTriggerId }));
  }

  onTissueSelection(tissue: string | undefined): void {
    this.store.dispatch(TagsActions.selectTissues({ tissues: tissue ? [tissue] : undefined }));
  }

  onStainSelection(stain: string | undefined): void {
    this.store.dispatch(TagsActions.selectStains({ stains: stain ? [stain] : undefined }));
  }
}
