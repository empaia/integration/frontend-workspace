import { ChangeDetectionStrategy, Component, AfterViewInit, OnDestroy, ViewChild, HostListener } from '@angular/core';
import { MatSidenav } from '@angular/material/sidenav';
import { MenuEntity, SettingsMenuItem } from '@menu/models/menu.models';
import { SettingsMenuActions, SettingsMenuSelectors } from '@menu/store';
import { Store } from '@ngrx/store';
import { ResizeEvent } from 'angular-resizable-element';
import { Observable, Subject, takeUntil } from 'rxjs';

@Component({
  selector: 'app-settings-menu-container',
  templateUrl: './settings-menu-container.component.html',
  styleUrls: ['./settings-menu-container.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SettingsMenuContainerComponent implements AfterViewInit, OnDestroy {
  @ViewChild('drawer') private menuDrawer!: MatSidenav;
  private destroy$ = new Subject();

  public preprocessingMenu$: Observable<MenuEntity>;

  private initialMaxWidth = 100; // vw
  public initialMaxWidthStyle = {
    width: `${this.initialMaxWidth}vw`,
  };

  public sidenavWidth$: Observable<number>;

  constructor(private store: Store) {
    this.preprocessingMenu$ = this.store.select(SettingsMenuSelectors.selectPreprocessingMenu);
    this.sidenavWidth$ = this.store.select(SettingsMenuSelectors.selectSideNavSize);
  }

  ngAfterViewInit(): void {
    this.store
      .select(SettingsMenuSelectors.selectMinimizeAll)
      .pipe(takeUntil(this.destroy$))
      .subscribe(minimized => {
        if (this.menuDrawer && minimized !== undefined) {
          this.menuDrawer.toggle(!minimized);
        }
      });
  }

  ngOnDestroy(): void {
    this.destroy$.next(null);
  }

  public toggle(): void {
    this.menuDrawer.toggle();
    this.store.dispatch(SettingsMenuActions.toggleMenu({ minimizeAll: !this.menuDrawer.opened }));
  }

  public railMenuItemRoute(selected: boolean, value: SettingsMenuItem): void {
    if (this.menuDrawer && !this.menuDrawer.opened || this.menuDrawer && selected) {
      this.toggle();
    }
    this.store.dispatch(SettingsMenuActions.openMenuMidi({ menuItem: value }));
  }

  @HostListener('window:resize', ['$event'])
  public onResize(_event: ResizeEvent): void {
    // Dispatch resize action to inform the menu that the window viewport
    // was changed
    this.store.dispatch(SettingsMenuActions.resizeMenu());
  }
}
