import { LetDirective } from '@ngrx/component';
import { provideMockStore } from '@ngrx/store/testing';
import { MenuButtonComponent } from '@shared/components/menu-button/menu-button.component';
import { MockComponents } from 'ng-mocks';
import { MaterialModule } from '@material/material.module';
import { createComponentFactory, Spectator } from '@ngneat/spectator/jest';
import { SettingsMenuContainerComponent } from './settings-menu-container.component';
import { PreprocessingsLabelComponent } from '@preprocessings/components/preprocessings-label/preprocessings-label.component';
import { SettingsMenuSelectors } from '@menu/store';

describe('SettingsMenuContainerComponent', () => {
  let spectator: Spectator<SettingsMenuContainerComponent>;
  const createComponent = createComponentFactory({
    component: SettingsMenuContainerComponent,
    imports: [
      MaterialModule,
      LetDirective,
    ],
    declarations: [
      MockComponents(
        MenuButtonComponent,
        PreprocessingsLabelComponent,
      ),
    ],
    providers: [
      provideMockStore({
        selectors: [
          {
            selector: SettingsMenuSelectors.selectPreprocessingMenu,
            value: {}
          },
          {
            selector: SettingsMenuSelectors.selectSideNavSize,
            value: -1
          }
        ]
      })
    ]
  });

  beforeEach(() => spectator = createComponent());

  it('should create', () => {
    expect(spectator.component).toBeTruthy();
  });
});
