import { DiagnosticMenuContainerComponent } from './diagnostic-menu-container.component';
import { createComponentFactory, Spectator } from '@ngneat/spectator';
import { MaterialModule } from '@material/material.module';
import { MockComponents } from 'ng-mocks';
import { CasesContainerComponent } from '@cases/containers/cases-container/cases-container.component';
import { AppsContainerComponent } from '@apps/containers/apps-container/apps-container.component';
import { provideMockStore } from '@ngrx/store/testing';
import { DiagnosticMenuSelectors } from '@menu/store';
import { LetDirective, PushPipe } from '@ngrx/component';
import { MenuButtonComponent } from '@shared/components/menu-button/menu-button.component';
import { CasesSelectors } from '@cases/store';
import { ExaminationsSelectors } from '@examinations/store';
import { CasesLabelComponent } from '@cases/components/cases-label/cases-label.component';

describe('MenuComponent', () => {
  let spectator: Spectator<DiagnosticMenuContainerComponent>;
  const createComponent = createComponentFactory({
    component: DiagnosticMenuContainerComponent,
    imports: [
      MaterialModule,
      LetDirective,
      PushPipe,
    ],
    declarations: [
      MockComponents(
        CasesContainerComponent,
        AppsContainerComponent,
        MenuButtonComponent,
        CasesLabelComponent,
      ),
    ],
    providers: [
      provideMockStore({
        selectors: [
          {
            selector: DiagnosticMenuSelectors.selectCasesMenu,
            value: {}
          },
          {
            selector: DiagnosticMenuSelectors.selectAppsMenu,
            value: {}
          },
          {
            selector: DiagnosticMenuSelectors.selectMinimizeAll,
            value: false,
          },
          {
            selector: DiagnosticMenuSelectors.selectSideNavSize,
            value: -1,
          },
          {
            selector: CasesSelectors.selectSelectedCase,
            value: {}
          },
          {
            selector: CasesSelectors.selectCasesLoaded,
            value: true
          },
          {
            selector: ExaminationsSelectors.selectSelected,
            value: {}
          },
        ]
      })
    ]
  });

  beforeEach(() => spectator = createComponent());

  it('should create', () => {
    expect(spectator.component).toBeTruthy();
  });
});
