import { AfterViewInit, ChangeDetectionStrategy, Component, HostListener, OnDestroy, ViewChild } from '@angular/core';
import { MatSidenav } from '@angular/material/sidenav';
import { MenuEntity, DiagnosticMenuItem } from '@menu/models/menu.models';
import { Store } from '@ngrx/store';
import { Observable, Subject } from 'rxjs';
import { DiagnosticMenuActions, DiagnosticMenuSelectors } from '@menu/store';
import { Case, CasesSelectors } from '@cases/store';
import { Examination } from '@examinations/store/examinations/examinations.models';
import { ExaminationsSelectors } from '@examinations/store';
import { takeUntil } from 'rxjs/operators';
import { ResizeEvent } from 'angular-resizable-element';

@Component({
  selector: 'app-diagnostic-menu-container',
  templateUrl: './diagnostic-menu-container.component.html',
  styleUrls: ['./diagnostic-menu-container.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DiagnosticMenuContainerComponent implements AfterViewInit, OnDestroy {
  @ViewChild('drawer') private menuDrawer!: MatSidenav;
  private destroy$ = new Subject();

  public casesMenu$: Observable<MenuEntity>;
  public appsMenu$: Observable<MenuEntity>;

  public caseSelected$: Observable<Case | undefined>;
  public casesLoaded$: Observable<boolean>;
  public examinationSelected$: Observable<Examination | undefined>;

  private initialMaxWidth = 100; // vw
  public initialMaxWidthStyle = {
    width: `${this.initialMaxWidth}vw`,
  };

  public sidenavWidth$: Observable<number>;

  constructor(private store: Store) {
    this.casesMenu$ = this.store.select(DiagnosticMenuSelectors.selectCasesMenu);
    this.appsMenu$ = this.store.select(DiagnosticMenuSelectors.selectAppsMenu);
    this.sidenavWidth$ = this.store.select(DiagnosticMenuSelectors.selectSideNavSize);

    this.caseSelected$ = this.store.select(CasesSelectors.selectSelectedCase);
    this.casesLoaded$ = this.store.select(CasesSelectors.selectCasesLoaded);
    this.examinationSelected$ = this.store.select(ExaminationsSelectors.selectSelected);
  }

  ngAfterViewInit(): void {
    this.store
      .select(DiagnosticMenuSelectors.selectMinimizeAll)
      .pipe(takeUntil(this.destroy$))
      .subscribe(minimized => {
        if (this.menuDrawer && minimized !== undefined) {
          this.menuDrawer.toggle(!minimized);
        }
      });
  }

  ngOnDestroy(): void {
    this.destroy$.next(null);
  }

  public toggle(): void {
    this.menuDrawer.toggle();
    this.store.dispatch(DiagnosticMenuActions.toggleMenu({ minimizeAll: !this.menuDrawer.opened }));
  }

  public railMenuItemRoute(selected: boolean, value: DiagnosticMenuItem): void {
    if (this.menuDrawer && !this.menuDrawer.opened || this.menuDrawer && selected) {
      this.toggle();
    }
    if (value === 'Cases') {
      this.store.dispatch(DiagnosticMenuActions.openMenuMax({ menuItem: value }));
    } else {
      this.store.dispatch(DiagnosticMenuActions.openMenuMidi({ menuItem: value }));
    }
  }

  @HostListener('window:resize', ['$event'])
  public onResize(_event: ResizeEvent): void {
    // Dispatch resize action to inform the menu that the window viewport
    // was changed
    this.store.dispatch(DiagnosticMenuActions.resizeMenu());
  }
}

