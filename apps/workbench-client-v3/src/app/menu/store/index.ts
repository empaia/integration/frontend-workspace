import * as DiagnosticMenuActions from './diagnostic-menu/diagnostic-menu.actions';
import * as DiagnosticMenuFeature from './diagnostic-menu/diagnostic-menu.reducer';
import * as DiagnosticMenuSelectors from './diagnostic-menu/diagnostic-menu.selectors';
export * from './diagnostic-menu/diagnostic-menu.effects';

export { DiagnosticMenuActions, DiagnosticMenuFeature, DiagnosticMenuSelectors };

import * as SettingsMenuActions from './settings-menu/settings-menu.actions';
import * as SettingsMenuFeature from './settings-menu/settings-menu.reducer';
import * as SettingsMenuSelectors from './settings-menu/settings-menu.selectors';
export * from './settings-menu/settings-menu.effects';

export { SettingsMenuActions, SettingsMenuFeature, SettingsMenuSelectors };
