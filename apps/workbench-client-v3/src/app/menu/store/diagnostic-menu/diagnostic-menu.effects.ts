import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { map } from 'rxjs/operators';
import * as DiagnosticMenuActions from './diagnostic-menu.actions';

@Injectable()
export class DiagnosticMenuEffects {

  clickMenuButton$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(DiagnosticMenuActions.clickMenuButton),
      map(action => action.menuItem),
      map(menuItem =>
        menuItem === 'Cases'
          ? DiagnosticMenuActions.openMenuMax({ menuItem })
          : DiagnosticMenuActions.openMenuMidi({ menuItem })
      )
    );
  });

  constructor(
    private readonly actions$: Actions,
  ) {}
}
