import { createAction, props } from '@ngrx/store';
import { DiagnosticMenuItem } from '@menu/models/menu.models';

export const openMenuMidi = createAction(
  '[DiagnosticMenu] Open Menu Midi',
  props<{ menuItem: DiagnosticMenuItem }>()
);

export const openMenuMax = createAction(
  '[DiagnosticMenu] Open Menu Max',
  props<{ menuItem: DiagnosticMenuItem }>()
);

export const toggleMenu = createAction(
  '[DiagnosticMenu] Toggle Menu',
  props<{ minimizeAll: boolean }>()
);

export const resizeMenu = createAction(
  '[DiagnosticMenu] Resize Menu',
);

export const clickMenuButton = createAction(
  '[DiagnosticMenu] Click Menu Button',
  props<{ menuItem: DiagnosticMenuItem }>()
);
