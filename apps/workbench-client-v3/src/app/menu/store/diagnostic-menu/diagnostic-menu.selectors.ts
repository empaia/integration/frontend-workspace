import { MenuEntity, DiagnosticMenuItem } from '@menu/models/menu.models';
import { createSelector } from '@ngrx/store';
import {
  State as ModuleState,
  selectMenuFeatureState,
} from '../menu-feature.state';
import {
  State,
} from './diagnostic-menu.reducer';

// Lookup the 'Menu' feature state managed by NgRx
export const selectMenuState = createSelector(
  selectMenuFeatureState,
  (state: ModuleState) => state.diagnosticMenu
);

export const selectMenuEntity = (menu: DiagnosticMenuItem) => createSelector(
  selectMenuState,
  (state: State) => state.menu.find(m => m.id === menu) as MenuEntity,
);

export const selectSelectedMenuEntity = createSelector(
  selectMenuState,
  (state: State) => state.menu.find(m => m.selected) as MenuEntity,
);

export const selectCasesMenu = selectMenuEntity('Cases');
export const selectAppsMenu = selectMenuEntity('Apps');

export const selectMinimizeAll = createSelector(
  selectMenuState,
  (state: State) => state.minimizeAll
);

export const selectSideNavSize = createSelector(
  selectMenuState,
  (state: State) => {
    return state.minimizeAll ? 0 : state.sideNavSize;
  }
);
