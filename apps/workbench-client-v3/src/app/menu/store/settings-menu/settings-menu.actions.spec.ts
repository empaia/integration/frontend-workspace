import * as fromSettingsMenu from './settings-menu.actions';

describe('clickMenuButton', () => {
  it('should return an action', () => {
    expect(fromSettingsMenu.clickMenuButton({
      menuItem: 'Preprocessing'
    }).type).toBe('[SettingsMenu] Click Menu Button');
  });
});
