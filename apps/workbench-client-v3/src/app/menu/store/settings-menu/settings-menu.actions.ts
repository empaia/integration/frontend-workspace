import { SettingsMenuItem } from '@menu/models/menu.models';
import { createAction, props } from '@ngrx/store';

export const openMenuMidi = createAction(
  '[SettingsMenu] Open Menu Midi',
  props<{ menuItem: SettingsMenuItem }>()
);

export const openMenuMax = createAction(
  '[SettingsMenu] Open Menu Max',
  props<{ menuItem: SettingsMenuItem }>()
);

export const toggleMenu = createAction(
  '[SettingsMenu] Toggle Menu',
  props<{ minimizeAll: boolean }>()
);

export const resizeMenu = createAction(
  '[SettingsMenu] Resize Menu',
);

export const clickMenuButton = createAction(
  '[SettingsMenu] Click Menu Button',
  props<{ menuItem: SettingsMenuItem }>()
);
