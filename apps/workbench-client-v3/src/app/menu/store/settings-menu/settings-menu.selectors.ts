import { SettingsMenuItem, MenuEntity } from '@menu/models/menu.models';
import { createSelector } from '@ngrx/store';
import {
  State as ModuleState,
  selectMenuFeatureState,
} from '../menu-feature.state';

export const selectSettingsMenuState = createSelector(
  selectMenuFeatureState,
  (state: ModuleState) => state.settingsMenu
);

export const selectSettingsMenuEntity = (menuItem: SettingsMenuItem) => createSelector(
  selectSettingsMenuState,
  (state) => state.menu.find(m => m.id === menuItem) as MenuEntity
);

export const selectSelectedSettingsMenuEntity = createSelector(
  selectSettingsMenuState,
  (state) => state.menu.find(m => m.selected) as MenuEntity
);

export const selectPreprocessingMenu = selectSettingsMenuEntity('Preprocessing');

export const selectMinimizeAll = createSelector(
  selectSettingsMenuState,
  (state) => state.minimizeAll
);

export const selectSideNavSize = createSelector(
  selectSettingsMenuState,
  (state) => state.minimizeAll ? 0 : state.sideNavSize
);
