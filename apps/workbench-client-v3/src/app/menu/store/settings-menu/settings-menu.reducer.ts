import { immerOn } from 'ngrx-immer/store';
import { calculateMaxSize, convertPxToVw } from '@helper/dimensional-style-calculation';
import {
  MAX_SIZE,
  MenuEntity,
  MenuState,
  SettingsMenuItem,
  MENU_BUTTON_WIDTH,
  MIDI_SIZE,
  MINIMIZED_SIZE,
  MIN_SIZE,
  SETTINGS_MENU
} from '@menu/models/menu.models';
import { Action, createReducer } from '@ngrx/store';
import * as SettingsMenuActions from './settings-menu.actions';


export const SETTINGS_MENU_FEATURE_KEY = 'settingsMenu';

export interface State {
  menu: MenuEntity[];
  minimizeAll: boolean;
  sideNavSize: number;
}

export const initialState: State = {
  menu: Object.values(SETTINGS_MENU),
  minimizeAll: false,
  sideNavSize: MIDI_SIZE,
};

export const settingsMenuReducer = createReducer(
  initialState,
  immerOn(SettingsMenuActions.openMenuMidi, (state, { menuItem }) => {
    const index = getMenuItemIndex(menuItem, state.menu);
    selectAndOpenItem(index, MenuState.MIDI, state.menu);
    deselectOtherMenus(menuItem, state.menu);
    state.sideNavSize = Math.max(MIDI_SIZE, convertPxToVw(MIN_SIZE));
  }),
  immerOn(SettingsMenuActions.openMenuMax, (state, { menuItem }) => {
    const index = getMenuItemIndex(menuItem, state.menu);
    selectAndOpenItem(index, MenuState.MAX, state.menu);
    deselectOtherMenus(menuItem, state.menu);
    state.sideNavSize = calculateMaxSize(MAX_SIZE, [convertPxToVw(MENU_BUTTON_WIDTH)]);
  }),
  immerOn(SettingsMenuActions.toggleMenu, (state, { minimizeAll }) => {
    state.minimizeAll = minimizeAll;
  }),
  immerOn(SettingsMenuActions.resizeMenu, (state) => {
    const item = getSelectedMenuItem(state.menu);
    state.sideNavSize = getMenuSize(item);
  })
);

function getMenuSize(item: MenuEntity | undefined): number {
  if (item) {
    switch(item.contentState) {
      case MenuState.HIDDEN: return MINIMIZED_SIZE;
      case MenuState.MIDI: return Math.max(MIDI_SIZE, convertPxToVw(MIN_SIZE));
      case MenuState.MAX: return calculateMaxSize(MAX_SIZE, [convertPxToVw(MENU_BUTTON_WIDTH)]);
    }
  } else {
    return MINIMIZED_SIZE;
  }
}

function getMenuItemIndex(id: SettingsMenuItem, menu: Array<MenuEntity>): number {
  return menu.findIndex(i => i.id === id);
}

function getSelectedMenuItem(menu: Array<MenuEntity>): MenuEntity | undefined {
  return menu.find(item => item.selected);
}

function selectAndOpenItem(index: number, state: MenuState, menu: Array<MenuEntity>) {
  menu[index].selected = true;
  menu[index].labelState = state;
  menu[index].contentState = state;
}

function deselectOtherMenus(id: SettingsMenuItem, menu: Array<MenuEntity>) {
  const others = menu.filter(i => i.id !== id);
  for (const item of others) {
    item.selected = false;
    item.labelState = MenuState.MIDI;
    item.contentState = MenuState.HIDDEN;
  }
}

export function reducer(state: State | undefined, action: Action) {
  return settingsMenuReducer(state, action);
}
