import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { map } from 'rxjs/operators';
import * as SettingsMenuAction from './settings-menu.actions';



@Injectable()
export class SettingsMenuEffects {

  clickMenuButton$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(SettingsMenuAction.clickMenuButton),
      map(action => action.menuItem),
      map(menuItem => SettingsMenuAction.openMenuMidi({ menuItem }))
    );
  });

  constructor(private readonly actions$: Actions) {}
}
