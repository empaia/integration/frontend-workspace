import { Action, combineReducers, createFeatureSelector } from '@ngrx/store';
import * as fromDiagnosticMenu from '@menu/store/diagnostic-menu/diagnostic-menu.reducer';
import * as fromSettingsMenu from '@menu/store/settings-menu/settings-menu.reducer';

export const MENU_MODULE_FEATURE_KEY = 'menuModuleFeature';

export const selectMenuFeatureState = createFeatureSelector<State>(
  MENU_MODULE_FEATURE_KEY
);

export interface State {
  [fromDiagnosticMenu.DIAGNOSTIC_MENU_FEATURE_KEY]: fromDiagnosticMenu.State;
  [fromSettingsMenu.SETTINGS_MENU_FEATURE_KEY]: fromDiagnosticMenu.State;
}

export function reducers(state: State | undefined, action: Action) {
  return combineReducers({
    [fromDiagnosticMenu.DIAGNOSTIC_MENU_FEATURE_KEY]: fromDiagnosticMenu.reducer,
    [fromSettingsMenu.SETTINGS_MENU_FEATURE_KEY]: fromSettingsMenu.reducer,
  })(state, action);
}
