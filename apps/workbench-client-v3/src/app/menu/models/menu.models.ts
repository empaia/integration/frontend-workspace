/**
 * Interface for the 'Menu' data
 */

export type DiagnosticMenuItem =
 | 'Cases'
 | 'Apps'
 | 'Slides';

export type SettingsMenuItem =
 | 'Preprocessing';

export type MenuItem = DiagnosticMenuItem | SettingsMenuItem;

export enum MenuState {
  HIDDEN = 0,
  MIDI = 1,
  MAX = 2,
}

export const DiagnosticMenuIcons: Record<DiagnosticMenuItem, string> = {
  Cases: 'layers',
  Apps: 'settings_suggest',
  Slides: 'filter',
};

export const SettingsMenuIcons: Record<SettingsMenuItem, string> = {
  Preprocessing: 'history',
};

export interface MenuEntity {
  readonly id: MenuItem,
  selected: boolean,
  contentState: MenuState,
  labelState: MenuState,
  readonly icon: string,
}

// Initial Diagnostic Menu state definition
export const DIAGNOSTIC_MENU: Record<DiagnosticMenuItem, MenuEntity> = {
  Cases: {
    id: 'Cases',
    contentState: MenuState.HIDDEN,
    labelState: MenuState.HIDDEN,
    selected: true,
    icon: DiagnosticMenuIcons.Cases,
  },
  Apps: {
    id: 'Apps',
    contentState: MenuState.HIDDEN,
    labelState: MenuState.HIDDEN,
    selected: false,
    icon: DiagnosticMenuIcons.Apps,
  },
  Slides: {
    id: 'Slides',
    contentState: MenuState.HIDDEN,
    labelState: MenuState.HIDDEN,
    selected: false,
    icon: DiagnosticMenuIcons.Slides,
  },
};

// Initial Settings Menu state definition
export const SETTINGS_MENU: Record<SettingsMenuItem, MenuEntity> = {
  Preprocessing: {
    id: 'Preprocessing',
    contentState: MenuState.HIDDEN,
    labelState: MenuState.HIDDEN,
    selected: true,
    icon: SettingsMenuIcons.Preprocessing,
  }
};

export const MINIMIZED_SIZE = 0;
export const MIDI_SIZE = 20; // vw
export const MAX_SIZE = 100; // vw
export const MIN_SIZE = 295; // px
export const MENU_BUTTON_WIDTH = 64; // px
