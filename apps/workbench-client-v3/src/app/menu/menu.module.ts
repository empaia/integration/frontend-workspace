import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DiagnosticMenuContainerComponent } from './containers/diagnostic-menu-container/diagnostic-menu-container.component';
import { MaterialModule } from '@material/material.module';
import { StoreModule } from '@ngrx/store';
import { MENU_MODULE_FEATURE_KEY, reducers } from '@menu/store/menu-feature.state';
import { EffectsModule } from '@ngrx/effects';
import { DiagnosticMenuEffects, SettingsMenuEffects } from './store';
import { CasesModule } from '@cases/cases.module';
import { ExaminationsModule } from '@examinations/examinations.module';
import { AppsModule } from '@apps/apps.module';
import { LetDirective } from '@ngrx/component';
import { SharedModule } from '@shared/shared.module';
import { SettingsMenuContainerComponent } from './containers/settings-menu-container/settings-menu-container.component';
import { PreprocessingsModule } from '@preprocessings/preprocessings.module';

const COMPONENTS = [
  DiagnosticMenuContainerComponent,
  SettingsMenuContainerComponent,
];

@NgModule({
  declarations: COMPONENTS,
  imports: [
    CommonModule,
    MaterialModule,
    StoreModule.forFeature(
      MENU_MODULE_FEATURE_KEY,
      reducers,
    ),
    EffectsModule.forFeature([
      DiagnosticMenuEffects,
      SettingsMenuEffects,
    ]),
    CasesModule,
    ExaminationsModule,
    AppsModule,
    PreprocessingsModule,
    LetDirective,
    SharedModule,
  ],
  exports: COMPONENTS
})
export class MenuModule { }
