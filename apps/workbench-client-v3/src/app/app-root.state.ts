import { AppsFeature } from "@apps/store";
import { CasesFeature } from "@cases/store";
import { ExaminationsFeature } from "@examinations/store";
import { DiagnosticMenuFeature, SettingsMenuFeature } from "@menu/store";
import { RouterFeature } from "@router/store";

export interface RootState {
  [RouterFeature.ROUTER_FEATURE_KEY]: RouterFeature.MergedRouteReducerState;
  [DiagnosticMenuFeature.DIAGNOSTIC_MENU_FEATURE_KEY]: DiagnosticMenuFeature.State;
  [SettingsMenuFeature.SETTINGS_MENU_FEATURE_KEY]: SettingsMenuFeature.State;
  [CasesFeature.CASES_FEATURE_KEY]: CasesFeature.State;
  [ExaminationsFeature.EXAMINATIONS_FEATURE_KEY]: ExaminationsFeature.State;
  [AppsFeature.APPS_FEATURE_KEY]: AppsFeature.State;
}


