import { Injectable } from '@angular/core';
import { Actions, concatLatestFrom, createEffect, ofType } from '@ngrx/effects';
import * as TokenActions from './token.actions';
import * as ExaminationsSelectors from '@examinations/store/examinations/examinations.selectors';
import { map, retry } from 'rxjs/operators';
import { Token, TOKEN_TYPE } from 'vendor-app-integration';
import { Store } from '@ngrx/store';
import { fetch } from '@ngrx/router-store/data-persistence';
import { WbcV3ExaminationsService } from 'empaia-api-lib';
import { DEFAULT_TOKEN_RETRIES, TokenEntity } from '@token/store/token/token.models';
import { filterNullish } from '@helper/rxjs-operators';
import { State } from './token.reducer';

@Injectable()
export class TokenEffects {
  prepareSelectedAppToken$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(TokenActions.loadSelectedAppToken),
      concatLatestFrom(() => [
        this.store
          .select(ExaminationsSelectors.selectSelectedId)
          .pipe(filterNullish()),
      ]),
      map(([, examinationId]) => examinationId),
      map((examinationId) => TokenActions.loadAppToken({ examinationId }))
    );
  });

  loadToken$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(TokenActions.loadAppToken),
      fetch({
        run: (
          action: ReturnType<typeof TokenActions.loadAppToken>,
          _state: State
        ) => {
          return this.examinationsPanelService
            .examinationsExaminationIdScopePut({
              examination_id: action.examinationId,
            })
            .pipe(
              retry(DEFAULT_TOKEN_RETRIES),
              map((response) => response.access_token),
              map((accessToken) => {
                const token: Token = {
                  value: accessToken,
                  type: TOKEN_TYPE,
                };
                return token;
              }),
              map((token) => {
                const accessToken: TokenEntity = {
                  id: action.examinationId,
                  loaded: true,
                  token,
                };
                return accessToken;
              }),
              map((accessToken) =>
                TokenActions.loadTokenSuccess({ accessToken })
              )
            );
        },
        onError: (
          action: ReturnType<typeof TokenActions.loadAppToken>,
          error
        ) => {
          return TokenActions.loadTokenFailure({
            examinationId: action.examinationId,
            error,
          });
        },
      })
    );
  });

  constructor(
    private readonly actions$: Actions,
    private readonly store: Store,
    private readonly examinationsPanelService: WbcV3ExaminationsService
  ) {}
}
