import { Token } from 'vendor-app-integration';
import { HttpErrorResponse } from '@angular/common/http';

export interface TokenEntity {
  id: string;
  loaded: boolean;
  token?: Token;
  error?: HttpErrorResponse;
}

export const DEFAULT_TOKEN_RETRIES = 3;
