import * as fromToken from './token.actions';

describe('loadTokens', () => {
  it('should return an action', () => {
    expect(fromToken.loadSelectedAppToken().type).toBe('[Token] Load Selected App Token');
  });
});
