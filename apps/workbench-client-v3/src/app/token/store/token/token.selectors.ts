import { createSelector } from '@ngrx/store';
import {
  State as ModuleState,
  selectTokenFeatureState,
} from '../token-feature.state';
import { accessTokenAdapter } from '@token/store/token/token.reducer';

const {
  selectAll,
  selectEntities
} = accessTokenAdapter.getSelectors();

export const selectTokenState = createSelector(
  selectTokenFeatureState,
  (state: ModuleState) => state.token
);

export const selectAllTokens = createSelector(
  selectTokenState,
  selectAll
);

export const selectTokenEntities = createSelector(
  selectTokenState,
  selectEntities
);

export const selectToken = (appId: string) => createSelector(
  selectTokenEntities,
  (entities) => entities[appId]?.token
);

export const selectTokenLoaded = (appId: string) => createSelector(
  selectTokenEntities,
  (entities) => entities[appId]?.loaded
);

export const selectAllTokenLoaded = createSelector(
  selectAllTokens,
  (tokens) => tokens.every(token => token.loaded)
);

export const selectTokenError = (appId: string) => createSelector(
  selectTokenEntities,
  (entities) => entities[appId]?.error
);

export const selectAllTokenErrors = createSelector(
  selectAllTokens,
  (tokens) => tokens.map(token => token.error)
);
