import { Pipe, PipeTransform } from '@angular/core';
import { Job } from '@jobs/store';

@Pipe({
  name: 'jobToString'
})
export class JobToStringPipe implements PipeTransform {

  transform(job: Job): string {
    return `
    Id: ${job.id}
    Status: ${job.status}
    ${job.error_message ? 'Error: ' + job.error_message : ''}
    ${job.input_validation_status ? 'Input validation status: ' + job.input_validation_status : ''}
    ${job.input_validation_error_message ? 'Input validation error: ' + job.input_validation_error_message : ''}
    ${job.output_validation_status ? 'Output validation status: ' + job.output_validation_status: ''}
    ${job.output_validation_error_message ? 'Output validation error: ' + job.output_validation_error_message : ''}
    `;
  }

}
