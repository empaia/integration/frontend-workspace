import { Pipe, PipeTransform } from '@angular/core';
import { AnnotationEntity } from 'slide-viewer';

@Pipe({
  name: 'annotationToString'
})
export class AnnotationToStringPipe implements PipeTransform {

  transform(annotation: AnnotationEntity): string {
    return `
    Id: ${annotation.id}
    ${annotation.name ? 'Name: ' + annotation.name : ''}
    ${annotation.description ? 'Description: ' + annotation.description : ''}
    `;
  }

}
