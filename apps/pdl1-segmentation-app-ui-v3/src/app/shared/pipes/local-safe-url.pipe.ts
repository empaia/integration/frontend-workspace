import { SafeUrl, DomSanitizer } from '@angular/platform-browser';
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'localSafeUrl'
})
export class LocalSafeUrlPipe implements PipeTransform {

  constructor(private sanitizer: DomSanitizer) {}

  transform(localUrl: string): SafeUrl {
    return this.sanitizer.bypassSecurityTrustUrl(localUrl);
  }

}
