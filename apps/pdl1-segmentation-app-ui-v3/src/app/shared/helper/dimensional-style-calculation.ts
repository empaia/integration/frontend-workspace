function calculateMaxSize(maxSize: number, subtrahends: number[]): number {
  return maxSize - subtrahends.reduce((prev, curr) => prev + curr);
}

function convertVwToPx(vw: number): number {
  return vw * window.innerWidth / 100;
}

function convertPxToVw(px: number): number {
  return px * 100 / window.innerWidth;
}

export {
  calculateMaxSize,
  convertPxToVw,
  convertVwToPx,
};
