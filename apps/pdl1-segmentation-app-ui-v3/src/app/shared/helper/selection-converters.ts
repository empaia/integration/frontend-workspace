export interface Selector {
  id: string;
  checked: boolean;
}

export interface Item {
  id: string;
}

export function convertItemToSelector<T extends Item, K extends Selector>(item: T, checked: boolean): K {
  return {
    id: item.id,
    checked,
  } as K;
}

export function convertItemsToSelectors<T extends Item, K extends Selector>(items: T[], checked: boolean): K[] {
  return items.map(item => convertItemToSelector(item, checked));
}

export function convertIdToSelector<K extends Selector>(id: string, checked: boolean): K {
  return { id, checked } as K;
}

export function convertIdsToSelectors<K extends Selector>(ids: string[], checked: boolean): K[] {
  return ids.map(id => convertIdToSelector(id, checked));
}
