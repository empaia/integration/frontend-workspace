import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { JobStatus, JobValidationStatus } from '@jobs/store';

@Component({
  selector: 'app-job-status',
  templateUrl: './job-status.component.html',
  styleUrls: ['./job-status.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class JobStatusComponent {
  public readonly JOB_STATUS = JobStatus;
  public readonly JOB_VALIDATION_STATUS = JobValidationStatus;
  @Input() jobStatus!: JobStatus;
  @Input() jobInputStatus!: JobValidationStatus;
  @Input() jobOutputStatus!: JobValidationStatus;
  @Input() jobIndicatorClass!: string;
}
