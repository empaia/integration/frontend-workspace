import {
  Component,
  ChangeDetectionStrategy,
  OnChanges,
  SimpleChanges,
  Input,
  Output,
  EventEmitter
} from '@angular/core';

@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PaginationComponent implements OnChanges {
  @Input() maxCount!: number;
  @Input() limit!: number;
  @Input() position!: number;

  @Output() nextPage = new EventEmitter<number>();
  @Output() previousPage = new EventEmitter<number>();

  roundedLimit = 0;

  ngOnChanges(changes: SimpleChanges): void {
    if (changes['maxCount'] || changes['limit']) {
      this.roundedLimit = Math.ceil(this.maxCount / this.limit);
    }
  }

  onNextPage(): void {
    this.nextPage.emit(this.position + this.limit);
  }

  onPreviousPage(): void {
    this.previousPage.emit(this.position - this.limit);
  }
}
