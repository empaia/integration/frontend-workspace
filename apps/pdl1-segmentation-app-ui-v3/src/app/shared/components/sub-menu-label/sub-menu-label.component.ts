import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
  selector: 'app-sub-menu-label',
  templateUrl: './sub-menu-label.component.html',
  styleUrls: ['./sub-menu-label.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SubMenuLabelComponent {

}
