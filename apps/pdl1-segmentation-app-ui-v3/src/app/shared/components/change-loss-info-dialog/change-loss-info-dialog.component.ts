import { ChangeDetectionStrategy, Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-change-loss-info-dialog',
  templateUrl: './change-loss-info-dialog.component.html',
  styleUrls: ['./change-loss-info-dialog.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ChangeLossInfoDialogComponent {
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: boolean,
  ) { }
}
