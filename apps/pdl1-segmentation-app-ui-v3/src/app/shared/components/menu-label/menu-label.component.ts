import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

@Component({
  selector: 'app-menu-label',
  templateUrl: './menu-label.component.html',
  styleUrls: ['./menu-label.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MenuLabelComponent {
  @Input() selected!: boolean;
  @Input() loaded = true;
  @Input() enableMenuLabelTopShadow = false;

  getClass(): string {
    let cssClass = this.selected ? 'mat-primary-bg' : 'button-bg';
    if (this.enableMenuLabelTopShadow) {
      cssClass += ' menu-border-box-shadow';
    }
    return cssClass;
  }
}
