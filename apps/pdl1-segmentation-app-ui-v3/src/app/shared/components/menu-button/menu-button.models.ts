import { MenuType } from '@menu/models/menu.models';

export interface MenuButtonEventModel {
  mouse: MouseEvent;
  value: MenuType;
}
