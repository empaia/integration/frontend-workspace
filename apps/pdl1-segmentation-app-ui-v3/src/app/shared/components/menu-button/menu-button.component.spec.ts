import { MaterialModule } from '@material/material.module';
import { createComponentFactory, Spectator } from '@ngneat/spectator';
import { MenuButtonComponent } from './menu-button.component';

describe('MenuButtonComponent', () => {
  let spectator: Spectator<MenuButtonComponent>;
  const createComponent = createComponentFactory({
    component: MenuButtonComponent,
    imports: [
      MaterialModule,
    ]
  });

  beforeEach(() => spectator = createComponent());

  it('should be created', () => {
    expect(spectator.component).toBeTruthy();
  });
});
