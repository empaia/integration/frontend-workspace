import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { MenuEntity, MenuState } from '@menu/models/menu.models';
import { MenuSelectors } from '@menu/store';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-menu-item-layout-container',
  templateUrl: './menu-item-layout-container.component.html',
  styleUrls: ['./menu-item-layout-container.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MenuItemLayoutContainerComponent {
  readonly menuState = MenuState;
  @Input() menu!: MenuEntity;

  public showAll$!: Observable<boolean>;

  constructor(private store: Store) {
    this.showAll$ = this.store.select(MenuSelectors.selectMenuVisibilityState);
  }
}
