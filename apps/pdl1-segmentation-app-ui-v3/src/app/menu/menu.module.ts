import { SlidesModule } from '@slides/slides.module';
import { EffectsModule } from '@ngrx/effects';
import { MENU_MODULE_FEATURE_KEY, reducers } from '@menu/store/menu-feature.state';
import { StoreModule } from '@ngrx/store';
import { SharedModule } from './../shared/shared.module';
import { MaterialModule } from '@material/material.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MenuComponent } from './containers/menu/menu.component';
import { MenuEffects } from '@menu/store';
import { LetDirective, PushPipe } from '@ngrx/component';
import { ValidationModule } from '@validation/validation.module';



@NgModule({
  declarations: [
    MenuComponent
  ],
  imports: [
    CommonModule,
    MaterialModule,
    SharedModule,
    SlidesModule,
    ValidationModule,
    StoreModule.forFeature(
      MENU_MODULE_FEATURE_KEY,
      reducers
    ),
    EffectsModule.forFeature([
      MenuEffects
    ]),
    LetDirective,
    PushPipe,
  ],
  exports: [
    MenuComponent
  ]
})
export class MenuModule { }
