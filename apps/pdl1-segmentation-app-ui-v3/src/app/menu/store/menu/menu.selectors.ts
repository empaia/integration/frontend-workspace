import { createSelector } from '@ngrx/store';
import {
  State as ModuleState,
  selectMenuFeatureState
} from '@menu/store/menu-feature.state';
import { MenuEntity, MenuType } from '@menu/models/menu.models';

export const selectMenuState = createSelector(
  selectMenuFeatureState,
  (state: ModuleState) => state.menu
);

export const selectMenuEntity = (menu: MenuType) => createSelector(
  selectMenuState,
  // casting to MenuEntity here as we can be sure that we do not receive undefined
  (state) => state.menu.find(m => m.id === menu) as MenuEntity
);

export const selectAllMenuEntities = createSelector(
  selectMenuState,
  (state) => state.menu
);

export const selectMenuVisibilityState = createSelector(
  selectMenuState,
  (state) => state.showAll
);

export const selectMenuSize = createSelector(
  selectMenuState,
  (state) => state.showAll ? state.menuSize : 0
);

export const selectSlidesMenu = selectMenuEntity('slides');
export const selectValidationMenu = selectMenuEntity('validation');
