import { MAX_SIZE, MIDI_SIZE, MINIMIZED_SIZE } from '@menu/models/menu.models';
import { createReducer } from '@ngrx/store';
import { immerOn } from 'ngrx-immer/store';
import { MENU, MenuEntity, MenuState, MenuType } from '@menu/models/menu.models';
import * as MenuActions from './menu.actions';
import { calculateMaxSize, convertVwToPx } from '@shared/helper/dimensional-style-calculation';
import { MENU_BUTTON_WIDTH } from '@menu/models/menu.models';


export const MENU_FEATURE_KEY = 'menu';

export interface State {
  menu: MenuEntity[];
  showAll: boolean;
  menuSize: number;
}

export const initialState: State = {
  menu: Object.values(MENU),
  showAll: true,
  menuSize: MIDI_SIZE
};


export const reducer = createReducer(
  initialState,
  immerOn(MenuActions.openMenuMidi, (state, { id }) => {
    const index = getMenuItemIndex(id, state.menu);
    selectAndOpenItem(index, MenuState.MIDI, state.menu);
    deselectAndMinimizeOthers(id, state.menu);
    state.menuSize = MIDI_SIZE;
  }),
  immerOn(MenuActions.openMenuMax, (state, { id }) => {
    const index = getMenuItemIndex(id, state.menu);
    selectAndOpenItem(index, MenuState.MAX, state.menu);
    deselectAndMinimizeOthers(id, state.menu);
    state.menuSize = calculateMaxSize(convertVwToPx(MAX_SIZE), [MENU_BUTTON_WIDTH]);
  }),
  immerOn(MenuActions.showLabel, (state, { id }) => {
    const index = getMenuItemIndex(id, state.menu);
    state.menu[index].labelState = MenuState.MIDI;
  }),
  immerOn(MenuActions.toggleMenu, (state) => {
    state.showAll = !state.showAll;
  }),
  immerOn(MenuActions.resizeMenu, (state) => {
    const item = getSelectedMenuItem(state.menu);
    state.menuSize = getMenuSize(item);
  })
);

function getMenuSize(item: MenuEntity | undefined): number {
  if (item) {
    switch(item.contentState) {
      case MenuState.HIDDEN: return MINIMIZED_SIZE;
      case MenuState.MAX: return calculateMaxSize(convertVwToPx(MAX_SIZE), [MENU_BUTTON_WIDTH]);
      case MenuState.MIDI: return MIDI_SIZE;
    }
  } else {
    return MINIMIZED_SIZE;
  }
}

function getMenuItemIndex(id: MenuType, menu: MenuEntity[]): number {
  return menu.findIndex(i => i.id === id);
}

function getSelectedMenuItem(menu: MenuEntity[]): MenuEntity | undefined {
  return menu.find(item => item.selected);
}

function getMenuItemsAbove(id: MenuType, menu: MenuEntity[]): MenuEntity[] {
  const entity = getMenuItem(id, menu);
  const onSameSide = menu.filter(i => i.alignment === entity.alignment);
  return onSameSide.filter(i => i.order < entity.order);
}

function getMenuItem(id: MenuType, menu: MenuEntity[]): MenuEntity {
  const index = menu.findIndex(i => i.id === id);
  return menu[index];
}

function deselectAndMinimizeOthers(id: MenuType, menu: MenuEntity[]): void {
  deselectOtherMenus(id, menu);
  minimizeLabelsAbove(id, menu);
}

// hide all other menus in same column / on same side
function minimizeLabelsAbove(id: MenuType, menu: MenuEntity[]): void {
  const above = getMenuItemsAbove(id, menu);
  for (const item of above) {
    item.labelState = MenuState.MIDI;
    item.contentState = MenuState.HIDDEN;
  }
}

// deselect all other menus if menu (id) is selected
function deselectOtherMenus(id: MenuType, menu: MenuEntity[]): void {
  const clickedItem = getMenuItem(id, menu);
  const others = menu.filter(
    i => i.id !== id
      && i.alignment === clickedItem.alignment
  );
  for (const item of others) {
    item.selected = false;
    item.contentState = MenuState.HIDDEN;
  }
}

function selectAndOpenItem(index: number, state: MenuState, menu: MenuEntity[]): void {
  menu[index].selected = true;
  menu[index].labelState = state;
  menu[index].contentState = state;
}

