import { WorkspaceSizeService } from '@menu/services/workspace-size.service';
import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import * as MenuActions from './menu.actions';
import { SlidesActions } from '@slides/store/slides/slides.actions';
import { exhaustMap, map } from 'rxjs/operators';
import { MatDialog } from '@angular/material/dialog';
import { InfoDialogComponent } from '@shared/components/info-dialog/info-dialog.component';



@Injectable()
export class MenuEffects {

  // open slide menu when slides are loading
  openSlideMenu$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(SlidesActions.loadSlides),
      map(() => MenuActions.openMenuMax({ id: 'slides' }))
    );
  });

  setViewerToolbarPosition$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(MenuActions.changeMenuWorkspaceSize),
      map(action => action.domRect),
      map(domRect => this.workspaceSizeService.setSlideViewerToolbarPosition(domRect)),
    );
  }, { dispatch: false });

  setViewerOverviewPosition$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(MenuActions.changeMenuWorkspaceSize),
      map(action => action.domRect),
      map(domRect => this.workspaceSizeService.setSlideViewerOverviewPosition(domRect))
    );
  }, { dispatch: false });

  // open validation menu on slide selection
  openResultsOnSlideSelection$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(SlidesActions.selectSlideReady),
      map(() => MenuActions.openMenuMidi({ id: 'validation' }))
    );
  });

  openInfoDialog$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(MenuActions.openInfoDialog),
      exhaustMap(action =>
        this.dialog.open(InfoDialogComponent, {
          data: action.infoData,
          width: action.size,
        }).afterClosed()
      )
    );
  }, { dispatch: false });

  constructor(
    private readonly actions$: Actions,
    private readonly workspaceSizeService: WorkspaceSizeService,
    private readonly dialog: MatDialog,
  ) {}
}
