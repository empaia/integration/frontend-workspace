import { InfoDialogData } from "@shared/components/info-dialog/info-dialog.component";

export const INITIAL_MENU_PERIOD = 100;

export const SMALL_DIALOG_WIDTH = '400px';
export const LARGE_DIALOG_WIDTH = '800px';

export const INFO_DIALOG_DATA: InfoDialogData = {
  infoTopic: 'Information',
  infoText: 'Hier könnte Ihre Werbung stehen!'
};

export const SLIDES_DONE_DIALOG_DATA: InfoDialogData = {
  infoTopic: 'Congratulation!',
  infoText: 'All slides were successfully processed.'
};
