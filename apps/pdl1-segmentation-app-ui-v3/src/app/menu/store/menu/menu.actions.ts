import { createAction, props } from '@ngrx/store';
import { MenuType } from '@menu/models/menu.models';
import { InfoDialogData } from '@shared/components/info-dialog/info-dialog.component';

export const showLabel = createAction(
  '[APP/Menu] Show Label',
  props<{ id: MenuType }>()
);

export const openMenuMidi = createAction(
  '[APP/Menu] Open Menu Midi',
  props<{ id: MenuType }>()
);

export const openMenuMax = createAction(
  '[App/Menu] Open Menu Max',
  props<{ id: MenuType }>()
);

export const changeMenuWorkspaceSize = createAction(
  '[APP/Menu] Change Menu Workspace Size',
  props<{ domRect: DOMRect }>()
);

export const initWorkspaceSize = createAction(
  '[App/Menu] Init Workspace Size'
);

export const toggleMenu = createAction(
  '[APP/Menu] Toggle Menu',
);

export const resizeMenu = createAction(
  '[APP/Menu] Resize Menu',
);

export const openInfoDialog = createAction(
  '[APP/Menu] Open Info Dialog',
  props<{ infoData: InfoDialogData, size: string }>()
);
