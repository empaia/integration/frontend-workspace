/**
 * Interface of the menu data
 */
export type MenuType =
 | 'slides'
 | 'validation';

export enum MenuState {
  HIDDEN,
  MIDI,
  MAX,
}

export const MenuIcons: Record<MenuType, string> = {
  slides: 'filter',
  validation: 'fact_check',
};

export interface MenuEntity {
  readonly id: MenuType;
  selected: boolean;
  contentState: MenuState;
  labelState: MenuState;
  readonly alignment: 'start' | 'end'; // start = left, end = right
  readonly order: number, // sort buttons low to high (1 = top most button)
  readonly icon: string,
}

// Initial Menu state definition
export const MENU: Record<MenuType, MenuEntity> = {
  slides: {
    id: 'slides',
    contentState: MenuState.HIDDEN,
    labelState: MenuState.MIDI,
    selected: false,
    alignment: 'end',
    order: 1,
    icon: MenuIcons.slides,
  },
  validation: {
    id: 'validation',
    contentState: MenuState.HIDDEN,
    labelState: MenuState.MIDI,
    selected: false,
    alignment: 'end',
    order: 2,
    icon: MenuIcons.validation,
  }
};

export const MINIMIZED_SIZE = 0;
export const MIDI_SIZE = 401; // px
export const MAX_SIZE = 100; // vw
export const MIN_SIZE = 295; // px
export const MENU_BUTTON_WIDTH = 64; // px
