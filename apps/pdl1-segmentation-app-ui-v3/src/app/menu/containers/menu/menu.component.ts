import { MatSidenav } from '@angular/material/sidenav';
import { ChangeDetectionStrategy, Component, HostListener, ViewChild } from '@angular/core';
import { MenuEntity, MenuType } from '@menu/models/menu.models';
import { MenuActions, MenuSelectors } from '@menu/store';
import { Store } from '@ngrx/store';
import { MenuButtonEventModel } from '@shared/components/menu-button/menu-button.models';
import { SlideEntity, SlidesSelectors } from '@slides/store';
import { Observable } from 'rxjs';
import { PrimitivesSelectors } from '@primitives/store';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MenuComponent {
  @ViewChild('drawer') private menuDrawer!: MatSidenav;
  public slidesMenu$: Observable<MenuEntity>;
  public validationMenu$: Observable<MenuEntity>;
  public showMenu$: Observable<boolean>;
  public menuSize$: Observable<number>;

  public slideSelected$: Observable<SlideEntity | undefined>;
  public slides$: Observable<SlideEntity[]>;
  public slidesLoaded$: Observable<boolean>;
  public freezedSlides$: Observable<number>;

  constructor(private store: Store) {
    this.slidesMenu$ = this.store.select(MenuSelectors.selectSlidesMenu);
    this.validationMenu$ = this.store.select(MenuSelectors.selectValidationMenu);
    this.showMenu$ = this.store.select(MenuSelectors.selectMenuVisibilityState);
    this.menuSize$ = this.store.select(MenuSelectors.selectMenuSize);

    this.slideSelected$ = this.store.select(SlidesSelectors.selectSelectedSlide);
    this.slides$ = this.store.select(SlidesSelectors.selectAllSlides);
    this.slidesLoaded$ = this.store.select(SlidesSelectors.selectSlidesLoaded);
    this.freezedSlides$ = this.store.select(PrimitivesSelectors.selectNumberOfFreezedSlides);
  }

  public toggle(): void {
    this.menuDrawer.toggle();
    this.store.dispatch(MenuActions.toggleMenu());
  }

  public railMenuItemButton(selected: boolean, event: MenuButtonEventModel): void {
    if (this.menuDrawer && !this.menuDrawer.opened || this.menuDrawer && selected) {
      this.toggle();
    }
    this.dispatchOpenMenuItem(event.value);
  }

  public onLabelClicked(event: MouseEvent, id: MenuType): void {
    event.stopImmediatePropagation();
    this.dispatchOpenMenuItem(id);
  }

  @HostListener('window:resize', ['$event'])
  public onResize(): void {
    // Dispatch resize action to inform the menu that the window viewport
    // was changed
    this.store.dispatch(MenuActions.resizeMenu());
  }

  private dispatchOpenMenuItem(id: MenuType): void {
    if (id === 'slides') {
      this.store.dispatch(MenuActions.openMenuMax({ id }));
    } else {
      this.store.dispatch(MenuActions.openMenuMidi({ id }));
    }
  }
}
