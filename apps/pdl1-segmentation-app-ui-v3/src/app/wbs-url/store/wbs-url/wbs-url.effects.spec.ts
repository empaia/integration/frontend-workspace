import { provideMockActions } from '@ngrx/effects/testing';
import { WbsUrlService } from '@wbs-url/services/wbs-url.service';
import { Observable } from 'rxjs';
import { createServiceFactory, SpectatorService, mockProvider } from '@ngneat/spectator/jest';
import { WbsUrlEffects } from './wbs-url.effects';

describe('WbsUrlEffects', () => {
  let actions$: Observable<unknown>;
  let spectator: SpectatorService<WbsUrlEffects>;
  const createService = createServiceFactory({
    service: WbsUrlEffects,
    providers: [
      WbsUrlEffects,
      mockProvider(WbsUrlService),
      provideMockActions(() => actions$)
    ]
  });

  beforeEach(() => spectator = createService());

  it('should be created', () => {
    const effects = spectator.service;
    expect(effects).toBeTruthy();
  });
});
