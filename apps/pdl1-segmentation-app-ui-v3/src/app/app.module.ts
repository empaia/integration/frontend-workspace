import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { environment } from '../environments/environment';
import { AppComponent } from '@core/containers/app.component';
import { MetaReducer, StoreModule } from '@ngrx/store';
import { storeFreeze } from 'ngrx-store-freeze';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { EffectsModule } from '@ngrx/effects';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { WbsAppApiV3Module } from 'empaia-api-lib';
import { CoreModule } from '@core/core.module';
import { AuthenticationInterceptor } from '@core/services/interceptors/authentication.interceptor';
import { ScopeModule } from '@scope/scope.module';
import { SharedModule } from '@shared/shared.module';
import { TokenModule } from '@token/token.module';
import { WbsUrlModule } from '@wbs-url/wbs-url.module';
import { MenuModule } from '@menu/menu.module';
import { EadModule } from '@ead/ead.module';
import { JobsModule } from '@jobs/jobs.module';
import { ValidationModule } from '@validation/validation.module';
import { AnnotationsModule } from '@annotations/annotations.module';
import { PrimitivesModule } from '@primitives/primitives.module';
import { CollectionsModule } from '@collections/collections.module';
import { FullscreenOverlayContainer, OverlayContainer } from '@angular/cdk/overlay';
import { MAT_TOOLTIP_DEFAULT_OPTIONS } from '@angular/material/tooltip';
import { customTooltipDefaultOptions } from '@material/models/custom-options.models';
import { EmpaiaUiCommonsModule } from 'empaia-ui-commons';

// The app know by it's own the wbs api version to use
const API_VERSION = 'v3';

export const metaReducers: MetaReducer<object>[] = !environment.production
  ? [storeFreeze]
  : [];

@NgModule({
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    StoreModule.forRoot({},
      {
        metaReducers,
        runtimeChecks: {
          strictActionImmutability: true,
          strictStateImmutability: true,
          strictActionWithinNgZone: true
        }
      }
    ),
    EffectsModule.forRoot([]),
    !environment.production ? StoreDevtoolsModule.instrument({
      name: "Pdl1-Segmentation-App-Ui Store",
      connectInZone: true}) : [],
    AnnotationsModule,
    CollectionsModule,
    CoreModule,
    EadModule,
    JobsModule,
    MenuModule,
    PrimitivesModule,
    ScopeModule,
    SharedModule,
    TokenModule,
    ValidationModule,
    WbsAppApiV3Module,
    WbsUrlModule,
    EmpaiaUiCommonsModule
  ],
  providers: [
    { provide: 'API_VERSION', useValue: API_VERSION },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthenticationInterceptor,
      multi: true,
    },
    { provide: OverlayContainer, useClass: FullscreenOverlayContainer },
    { provide: MAT_TOOLTIP_DEFAULT_OPTIONS, useValue: customTooltipDefaultOptions }
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
