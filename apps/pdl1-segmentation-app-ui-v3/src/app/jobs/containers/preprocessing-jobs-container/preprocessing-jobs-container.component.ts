import { ChangeDetectionStrategy, Component } from '@angular/core';
import { Job, PreprocessingJobsActions, PreprocessingJobsSelectors } from '@jobs/store';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-preprocessing-jobs-container',
  templateUrl: './preprocessing-jobs-container.component.html',
  styleUrls: ['./preprocessing-jobs-container.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PreprocessingJobsContainerComponent {
  public preprocessingJobEntities$: Observable<Job[] | undefined>;
  public selectedPreprocessingJob$: Observable<string | undefined>;

  constructor(private store: Store) {
    this.preprocessingJobEntities$ = this.store.select(PreprocessingJobsSelectors.selectAllPreprocessingJobsOfCurrentSlide);
    this.selectedPreprocessingJob$ = this.store.select(PreprocessingJobsSelectors.selectSelectedPreprocessingJobId);
  }

  public onJobSelection(jobId: string): void {
    this.store.dispatch(PreprocessingJobsActions.selectPreprocessingJob({ jobId }));
  }
}
