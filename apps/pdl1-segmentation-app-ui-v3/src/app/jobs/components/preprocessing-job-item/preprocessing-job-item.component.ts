import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { Job } from '@jobs/store';

@Component({
  selector: 'app-preprocessing-job-item',
  templateUrl: './preprocessing-job-item.component.html',
  styleUrls: ['./preprocessing-job-item.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PreprocessingJobItemComponent {
  @Input() public job!: Job;
  @Input() public selected!: boolean;

  @Output() public jobSelected = new EventEmitter<string>();
}
