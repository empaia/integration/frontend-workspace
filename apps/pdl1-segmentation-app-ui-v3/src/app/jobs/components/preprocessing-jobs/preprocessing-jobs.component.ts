import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { MatRadioChange } from '@angular/material/radio';
import { Job } from '@jobs/store';

@Component({
  selector: 'app-preprocessing-jobs',
  templateUrl: './preprocessing-jobs.component.html',
  styleUrls: ['./preprocessing-jobs.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PreprocessingJobsComponent {
  @Input() public jobEntities!: Job[];
  @Input() public selectedJob!: string;

  @Output() public changeJobSelection = new EventEmitter<string>();

  public onRadioGroupChange(event: MatRadioChange): void {
    this.changeJobSelection.emit(event.value);
  }
}
