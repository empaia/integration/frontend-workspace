import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';
import { JOBS_MODULE_FEATURE_KEY, reducers } from './store/jobs-feature.state';
import { EffectsModule } from '@ngrx/effects';
import { JobsEffects, PostProcessingJobsEffects, PreprocessingJobsEffects } from './store';
import { PreprocessingJobsContainerComponent } from './containers/preprocessing-jobs-container/preprocessing-jobs-container.component';
import { SharedModule } from '@shared/shared.module';
import { MaterialModule } from '@material/material.module';
import { PreprocessingJobItemComponent } from './components/preprocessing-job-item/preprocessing-job-item.component';
import { PreprocessingJobsComponent } from './components/preprocessing-jobs/preprocessing-jobs.component';
import { PushPipe } from '@ngrx/component';
import { CommonUiModule } from 'empaia-ui-commons';



@NgModule({
  declarations: [
    PreprocessingJobsContainerComponent,
    PreprocessingJobItemComponent,
    PreprocessingJobsComponent
  ],
  imports: [
    CommonModule,
    CommonUiModule,
    MaterialModule,
    SharedModule,
    PushPipe,
    StoreModule.forFeature(
      JOBS_MODULE_FEATURE_KEY,
      reducers
    ),
    EffectsModule.forFeature([
      JobsEffects,
      PostProcessingJobsEffects,
      PreprocessingJobsEffects,
    ])
  ],
  exports: [
    PreprocessingJobsContainerComponent
  ]
})
export class JobsModule { }
