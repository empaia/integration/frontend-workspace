import { Action, combineReducers, createFeatureSelector } from '@ngrx/store';
import * as fromJobs from '@jobs/store/jobs/jobs.reducer';
import * as fromPostPrecessingJobs from '@jobs/store/post-processing-jobs/post-processing-jobs.reducer';
import * as fromPreprocessingJobs from '@jobs/store/preprocessing-jobs/preprocessing-jobs.reducer';

export const JOBS_MODULE_FEATURE_KEY = 'jobsModuleFeature';

export const selectJobsFeatureState = createFeatureSelector<State>(
  JOBS_MODULE_FEATURE_KEY
);

export interface State {
  [fromJobs.JOBS_FEATURE_KEY]: fromJobs.State;
  [fromPostPrecessingJobs.POST_PROCESSING_JOBS_FEATURE_KEY]: fromPostPrecessingJobs.State;
  [fromPreprocessingJobs.PREPROCESSING_JOBS_FEATURE_KEY]: fromPreprocessingJobs.State;
}

export function reducers(state: State | undefined, action: Action) {
  return combineReducers({
    [fromJobs.JOBS_FEATURE_KEY]: fromJobs.reducer,
    [fromPostPrecessingJobs.POST_PROCESSING_JOBS_FEATURE_KEY]: fromPostPrecessingJobs.reducer,
    [fromPreprocessingJobs.PREPROCESSING_JOBS_FEATURE_KEY]: fromPreprocessingJobs.reducer,
  })(state, action);
}
