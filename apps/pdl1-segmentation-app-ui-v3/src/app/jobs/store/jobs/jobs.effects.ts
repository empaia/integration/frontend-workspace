import { Injectable } from '@angular/core';
import { AppV3JobsService } from 'empaia-api-lib';
import { Actions, concatLatestFrom, createEffect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { JobsActions } from './jobs.actions';
import * as ScopeActions from '@scope/store/scope/scope.actions';
import * as TokenActions from '@token/store/token/token.actions';
import * as ScopeSelectors from '@scope/store/scope/scope.selectors';
import * as TokenSelectors from '@token/store/token/token.selectors';
import { fetch } from '@ngrx/router-store/data-persistence';
import { State } from './jobs.reducer';
import {
  distinctUntilChanged,
  filter,
  map,
  mergeMap,
  retryWhen,
  switchMap,
  takeUntil,
  timer,
} from 'rxjs';
import { filterNullish, retryOnAction } from '@shared/helper/rxjs-operators';
import {
  JOB_POLLING_PERIOD,
  Job,
  JobMode,
  SlideJobs,
  isJobTerminated,
  isJobValidationRunning,
  isPreprocessingJob,
} from './jobs.models';
import { PreprocessingJobsActions } from '../preprocessing-jobs/preprocessing-jobs.actions';
import { PostProcessingJobsActions } from '../post-processing-jobs/post-processing-jobs.actions';
import { requestNewToken } from 'vendor-app-communication-interface';
import { EadService, EmpaiaAppDescriptionV3 } from 'empaia-ui-commons';

@Injectable()
export class JobsEffects {
  loadJobs$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(JobsActions.loadJobs),
      fetch({
        run: (
          action: ReturnType<typeof JobsActions.loadJobs>,
          _state: State
        ) => {
          return this.jobsService
            .scopeIdJobsGet({
              scope_id: action.scopeId,
            })
            .pipe(
              retryWhen((errors) =>
                retryOnAction(
                  errors,
                  this.actions$,
                  TokenActions.setAccessToken,
                  requestNewToken
                )
              ),
              map((response) => response.items),
              map((jobs) => JobsActions.loadJobsSuccess({ jobs }))
            );
        },
        onError: (_action: ReturnType<typeof JobsActions.loadJobs>, error) => {
          return JobsActions.loadJobsFailure({ error });
        },
      })
    );
  });

  // filter and sort preprocessing, post processing and slide jobs map
  sortJobs$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(JobsActions.loadJobsSuccess),
      map((action) => action.jobs),
      concatLatestFrom(() =>
        this.store.select(ScopeSelectors.selectExtendedScope).pipe(
          filterNullish(),
          map((extended) => extended.ead as EmpaiaAppDescriptionV3)
        )
      ),
      map(([jobs, ead]) => {
        const wsiKey = this.eadService.getV3WsiInputKey(
          ead,
          'preprocessing'
        ) as string;
        const preprocessingJobs: Job[] = [];
        const postProcessingJobs: Job[] = [];
        const slideJobsMap = new Map<string, string[]>();

        jobs.forEach((job) => {
          switch (job.mode) {
            case JobMode.Postprocessing:
              postProcessingJobs.push(job);
              break;
            case JobMode.Preprocessing:
              preprocessingJobs.push(job);
              break;
          }

          const slideId = job.inputs[wsiKey];
          if (slideJobsMap.has(slideId)) {
            const jobIds = slideJobsMap.get(slideId) as string[];
            jobIds.push(job.id);
            slideJobsMap.set(slideId, jobIds);
          } else {
            slideJobsMap.set(slideId, [job.id]);
          }
        });

        const slideJobs: SlideJobs[] = Array.from(
          slideJobsMap,
          ([slideId, jobIds]) => ({ slideId, jobIds })
        );

        return {
          preprocessingJobs,
          postProcessingJobs,
          slideJobs,
        };
      }),
      mergeMap((jobs) => [
        JobsActions.setSlideJobs({ slideJobs: jobs.slideJobs }),
        PreprocessingJobsActions.setPreprocessingJobs({
          jobs: jobs.preprocessingJobs,
        }),
        PostProcessingJobsActions.setPostProcessingJobs({
          jobs: jobs.postProcessingJobs,
        }),
      ])
    );
  });

  // add post processing job to map after creation
  addPostProcessingJob$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(PostProcessingJobsActions.createPostProcessingJobSuccess),
      map((action) =>
        JobsActions.addJobToSlideJobs({
          slideId: action.slideId,
          jobId: action.job.id,
        })
      )
    );
  });

  jobsPolling$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(JobsActions.startJobsPolling),
      switchMap(() =>
        timer(0, JOB_POLLING_PERIOD).pipe(
          takeUntil(this.actions$.pipe(ofType(JobsActions.stopJobsPolling))),
          concatLatestFrom(() => [
            this.store
              .select(ScopeSelectors.selectScopeId)
              .pipe(filterNullish()),
          ]),
          map(([, scopeId]) => JobsActions.loadJobs({ scopeId }))
        )
      )
    );
  });

  stopJobsPolling$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(JobsActions.loadJobsSuccess),
      map((action) => action.jobs),
      // stop polling when every preprocessing job is done
      // ignore post processing job, these are never done
      filter(
        (jobs) =>
          jobs.filter(isPreprocessingJob).every(isJobTerminated) &&
          !jobs.filter(isPreprocessingJob).find(isJobValidationRunning)
      ),
      map(() => JobsActions.stopJobsPolling())
    );
  });

  startJobsPolling$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ScopeActions.setScope, TokenActions.setAccessToken),
      concatLatestFrom(() => [
        this.store.select(ScopeSelectors.selectScopeId).pipe(filterNullish()),
        this.store.select(TokenSelectors.selectAccessToken),
      ]),
      filter(([, _scopeId, token]) => !!token),
      distinctUntilChanged((prev, curr) => prev[1] === curr[1]),
      map(() => JobsActions.startJobsPolling())
    );
  });

  constructor(
    private readonly actions$: Actions,
    private readonly store: Store,
    private readonly jobsService: AppV3JobsService,
    private readonly eadService: EadService
  ) {}
}
