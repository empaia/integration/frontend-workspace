import { createSelector } from '@ngrx/store';
import { selectJobsFeatureState } from '../jobs-feature.state';
import { jobsAdapter, JOBS_FEATURE_KEY } from './jobs.reducer';
import { selectSelectedSlideId } from '@slides/store/slides/slides.selectors';

const {
  selectAll,
  selectEntities,
} = jobsAdapter.getSelectors();

export const selectJobsState = createSelector(
  selectJobsFeatureState,
  (state) => state[JOBS_FEATURE_KEY]
);

export const selectAllJobs = createSelector(
  selectJobsState,
  selectAll,
);

export const selectJobEntities = createSelector(
  selectJobsState,
  selectEntities,
);

export const selectJobsLoaded = createSelector(
  selectJobsState,
  (state) => state.loaded
);

export const selectJobsError = createSelector(
  selectJobsState,
  (state) => state.error
);

export const selectAllJobIdsOfCurrentSlide = createSelector(
  selectJobEntities,
  selectSelectedSlideId,
  (jobEntities, slideId) => slideId ? jobEntities[slideId]?.jobIds : undefined
);
