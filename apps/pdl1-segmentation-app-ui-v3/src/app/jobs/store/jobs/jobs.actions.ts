import { createActionGroup, emptyProps, props } from '@ngrx/store';
import { Job, SlideJobs } from './jobs.models';
import { HttpErrorResponse } from '@angular/common/http';

export const JobsActions = createActionGroup({
  source: 'Jobs',
  events: {
    'Load Jobs': props<{ scopeId: string }>(),
    'Load Jobs Success': props<{ jobs: Job[] }>(),
    'Load Jobs Failure': props<{ error: HttpErrorResponse }>(),
    'Set Slide Jobs': props<{ slideJobs: SlideJobs[] }>(),
    'Add Job To Slide Jobs': props<{ slideId: string, jobId: string }>(),
    'Clear Slide Jobs': emptyProps(),
    'Start Jobs Polling': emptyProps(),
    'Stop Jobs Polling': emptyProps(),
  }
});
