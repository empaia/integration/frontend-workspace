import {
  AppV3Job,
  AppV3JobMode,
  AppV3JobStatus,
  AppV3JobValidationStatus
} from 'empaia-api-lib';
export {
  AppV3JobMode as JobMode,
  AppV3JobStatus as JobStatus,
  AppV3JobValidationStatus as JobValidationStatus,
  AppV3JobCreatorType as JobCreatorType,
} from 'empaia-api-lib';

export type Job = AppV3Job;

export interface SlideJobs {
  slideId: string;
  jobIds: string[];
}

export const JOB_POLLING_PERIOD = 5000;

export function isJobRunning(job: Job): boolean {
  return job.status === AppV3JobStatus.Assembly
    || job.status === AppV3JobStatus.Ready
    || job.status === AppV3JobStatus.Scheduled
    || job.status === AppV3JobStatus.Running;
}

export function isJobValidationRunning(job: Job): boolean {
  return job.input_validation_status === AppV3JobValidationStatus.Running
  || job.output_validation_status === AppV3JobValidationStatus.Running;
}

export function isJobTerminated(job: Job): boolean {
  return (
    job.status === AppV3JobStatus.Completed
    || job.status === AppV3JobStatus.Error
    || job.status === AppV3JobStatus.Failed
    || job.status === AppV3JobStatus.Incomplete
    || job.status === AppV3JobStatus.None
    || job.status === AppV3JobStatus.Timeout
  );
}

export function isPreprocessingJob(job: Job): boolean {
  return job.mode === AppV3JobMode.Preprocessing;
}

export function isPostProcessingJob(job: Job): boolean {
  return job.mode === AppV3JobMode.Postprocessing;
}
