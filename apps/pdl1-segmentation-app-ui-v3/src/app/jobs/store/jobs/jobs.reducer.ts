import { EntityState, createEntityAdapter } from '@ngrx/entity';
import { createReducer, on } from '@ngrx/store';
import { SlideJobs } from './jobs.models';
import { HttpErrorResponse } from '@angular/common/http';
import { JobsActions } from './jobs.actions';


export const JOBS_FEATURE_KEY = 'jobs';

export interface State extends EntityState<SlideJobs> {
  loaded: boolean;
  error?: HttpErrorResponse | undefined;
}

export const jobsAdapter = createEntityAdapter<SlideJobs>({
  selectId: slideJob => slideJob.slideId
});

export const initialState: State = jobsAdapter.getInitialState({
  loaded: true,
  error: undefined,
});

export const reducer = createReducer(
  initialState,
  on(JobsActions.loadJobs, (state): State => ({
    ...state,
    loaded: false,
  })),
  on(JobsActions.loadJobsSuccess, (state): State => ({
    ...state,
    loaded: true,
  })),
  on(JobsActions.loadJobsFailure, (state, { error }): State => ({
    ...state,
    error,
  })),
  on(JobsActions.setSlideJobs, (state, { slideJobs }): State =>
    jobsAdapter.setAll(slideJobs, {
      ...state,
      loaded: true,
    })
  ),
  on(JobsActions.addJobToSlideJobs, (state, { slideId, jobId }): State =>
    jobsAdapter.updateOne({
      id: slideId,
      changes: {
        jobIds: state.entities[slideId]?.jobIds.concat(jobId)
      }
    }, {
      ...state
    })
  ),
  on(JobsActions.clearSlideJobs, (state): State =>
    jobsAdapter.removeAll({
      ...state,
      ...initialState,
    })
  ),
);
