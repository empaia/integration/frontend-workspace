import { HttpErrorResponse } from '@angular/common/http';
import { createActionGroup, emptyProps, props } from '@ngrx/store';
import { MissingJobIO } from './post-processing-jobs.models';
import { Job } from '../jobs/jobs.models';

export const PostProcessingJobsActions = createActionGroup({
  source: 'PostProcessingJobs',
  events: {
    'Create Post Processing Job': props<{
      scopeId: string,
      slideId: string,
      preprocessingCollectionId: string,
    }>(),
    'Create Post Processing Job Success': props<{
      job: Job,
      scopeId: string,
      slideId: string,
      preprocessingCollectionId: string,
    }>(),
    'Create Post Processing Job Failure': props<{ error: HttpErrorResponse }>(),
    'Set Id To Job Input': props<{
      scopeId: string,
      jobId: string,
      inputId: string,
      inputKey: string,
    }>(),
    'Set Id To Job Output': props<{
      scopeId: string,
      jobId: string,
      outputId: string,
      outputKey: string,
    }>(),
    'Set Job Input Or Output Success': props<{ job: Job }>(),
    'Set Job Input Or Output Failure': props<{ error: HttpErrorResponse }>(),
    'Remove Job Input Key': props<{
      scopeId: string,
      jobId: string,
      inputKey: string,
    }>(),
    'Remove Job Output Key': props<{
      scopeId: string,
      jobId: string,
      outputKey: string,
    }>(),
    'Remove Job Input Or Output Key Success': props<{ job: Job }>(),
    'Remove Job Input Or Output Key Failure': props<{ error: HttpErrorResponse }>(),
    'Update Job Input Key': props<{
      scopeId: string,
      jobId: string,
      inputKey: string,
      inputId: string,
    }>(),
    'Update Job Input Key Success': props<{ job: Job }>(),
    'Update Job Input Key Failure': props<{ error: HttpErrorResponse }>(),
    'Update Job Output Key': props<{
      scopeId: string,
      jobId: string,
      outputKey: string,
      outputId: string,
    }>(),
    'Update Job Output Key Success': props<{ job: Job }>(),
    'Update Job Output Key Failure': props<{ error: HttpErrorResponse }>(),
    'Set Post Processing Job': props<{ job: Job }>(),
    'Set Post Processing Jobs': props<{ jobs: Job[] }>(),
    'Clear Post Processing Jobs': emptyProps(),

    'Repair Missing Job IO': props<{
      job: Job,
      scopeId: string,
      slideId: string,
      preprocessingCollectionId: string,
      missingIo: MissingJobIO,
    }>(),
  }
});
