import {
  PostArrowCollection,
  PostBoolCollection,
  PostCirceCollection,
  PostClassCollection,
  PostFloatCollection,
  PostIdCollection,
  PostIntegerCollection,
  PostLineCollection,
  PostNestedCollection,
  PostPointCollection,
  PostPolygonCollection,
  PostRectangleCollection,
  PostSlideCollection,
  PostStringCollection
} from '@collections/store';
import { Job } from '../jobs/jobs.models';


export type PostCollection = PostPointCollection
| PostLineCollection
| PostArrowCollection
| PostCirceCollection
| PostRectangleCollection
| PostPolygonCollection
| PostClassCollection
| PostIntegerCollection
| PostFloatCollection
| PostBoolCollection
| PostStringCollection
| PostSlideCollection
| PostIdCollection
| PostNestedCollection;

export enum JobKeyType {
  INPUT = 'input',
  OUTPUT = 'output',
}

export enum MissingType {
  COLLECTION = 'collection',
  PRIMITIVE = 'primitive',
  WSI = 'wsi',
}

export const ioKeys = [
  'my_wsi',
  'preprocessing_regions',
  'handdrawn_regions',
  'selected_regions',
  'freeze'
] as const;

export type MissingKeys = typeof ioKeys[number];

export interface MissingJobIO {
  key: MissingKeys;
  type: MissingType;
  keyType: JobKeyType;
}

export function missMyWsi(postProcessingJob: Job): boolean {
  return !postProcessingJob.inputs['my_wsi'];
}

export function missPreprocessingRegionsCollection(postProcessingJob: Job): boolean {
  return !postProcessingJob.inputs['preprocessing_regions'];
}

export function missHandDrawnRegionsCollection(postProcessingJob: Job): boolean {
  return !postProcessingJob.inputs['handdrawn_regions'];
}

export function missSelectedRegionsCollection(postProcessingJob: Job): boolean {
  return !postProcessingJob.outputs['selected_regions'];
}

export function missFreezePrimitive(postProcessingJob: Job): boolean {
  return !postProcessingJob.outputs['freeze'];
}

export function hasMissingJobIOs(postProcessingJob: Job): boolean {
  return missMyWsi(postProcessingJob)
  || missPreprocessingRegionsCollection(postProcessingJob)
  || missHandDrawnRegionsCollection(postProcessingJob)
  || missSelectedRegionsCollection(postProcessingJob)
  || missFreezePrimitive(postProcessingJob);
}

export function checkForMissingJobIOs(postProcessingJob: Job): MissingJobIO[] {
  const missing: MissingJobIO[] = [];

  if (missMyWsi(postProcessingJob)) {
    missing.push({
      key: 'my_wsi',
      type: MissingType.WSI,
      keyType: JobKeyType.INPUT
    });
  }

  if (missPreprocessingRegionsCollection(postProcessingJob)) {
    missing.push({
      key: 'preprocessing_regions',
      type: MissingType.COLLECTION,
      keyType: JobKeyType.INPUT
    });
  }

  if (missHandDrawnRegionsCollection(postProcessingJob)) {
    missing.push({
      key: 'handdrawn_regions',
      type: MissingType.COLLECTION,
      keyType: JobKeyType.INPUT
    });
  }

  if (missSelectedRegionsCollection(postProcessingJob)) {
    missing.push({
      key: 'selected_regions',
      type: MissingType.COLLECTION,
      keyType: JobKeyType.OUTPUT
    });
  }

  if (missFreezePrimitive(postProcessingJob)) {
    missing.push({
      key: 'freeze',
      type: MissingType.PRIMITIVE,
      keyType: JobKeyType.OUTPUT
    });
  }

  return missing;
}
