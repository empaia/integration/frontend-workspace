import { createSelector } from '@ngrx/store';
import { selectJobsFeatureState } from '../jobs-feature.state';
import { postProcessingJobsAdapter, POST_PROCESSING_JOBS_FEATURE_KEY } from './post-processing-jobs.reducer';
import { selectAllJobIdsOfCurrentSlide } from '../jobs/jobs.selectors';
import { Job, isPostProcessingJob } from '../jobs/jobs.models';
import { selectCollectionEntities } from '@collections/store/collections/collections.selectors';

const {
  selectAll,
  selectEntities,
  selectIds,
} = postProcessingJobsAdapter.getSelectors();

export const selectPostProcessingJobsState = createSelector(
  selectJobsFeatureState,
  (state) => state[POST_PROCESSING_JOBS_FEATURE_KEY]
);

export const selectAllPostProcessingJobs = createSelector(
  selectPostProcessingJobsState,
  selectAll,
);

export const selectAllPostProcessingJobIds = createSelector(
  selectPostProcessingJobsState,
  (state) => selectIds(state) as string[]
);

export const selectPostProcessingJobEntities = createSelector(
  selectPostProcessingJobsState,
  selectEntities,
);

export const selectAllPostProcessingJobsOfCurrentSlide = createSelector(
  selectPostProcessingJobEntities,
  selectAllJobIdsOfCurrentSlide,
  (jobEntities, jobIds) => {
    if (!jobIds) { return undefined; }

    return jobIds.map(jobId => jobEntities[jobId]).filter(job => !!job && isPostProcessingJob(job)) as Job[];
  }
);

export const selectInputCollectionIdOfCurrentSlideJob = (key: string) => createSelector(
  selectAllPostProcessingJobsOfCurrentSlide,
  (jobs) => jobs ? jobs[0]?.inputs[key] : undefined
);

export const selectOutputCollectionIdOfCurrentSlideJob = (key: string) => createSelector(
  selectAllPostProcessingJobsOfCurrentSlide,
  (jobs) => jobs ? jobs[0]?.outputs[key] : undefined
);

export const selectInputCollectionOfCurrentSlideJob = (key: string) => createSelector(
  selectInputCollectionIdOfCurrentSlideJob(key),
  selectCollectionEntities,
  (collectionId, collectionEntities) => collectionId ? collectionEntities[collectionId] : undefined
);

export const selectOutputCollectionOfCurrentSlideJob = (key: string) => createSelector(
  selectOutputCollectionIdOfCurrentSlideJob(key),
  selectCollectionEntities,
  (collectionId, collectionEntities) => collectionId ? collectionEntities[collectionId] : undefined
);

export const selectInputCollectionIdOfPostProcessingJob = (jobId: string, key: string) => createSelector(
  selectPostProcessingJobEntities,
  (jobEntities) => jobEntities[jobId]?.inputs[key]
);

export const selectOutputCollectionIdOfPostProcessingJob = (jobId: string, key: string) => createSelector(
  selectPostProcessingJobEntities,
  (jobEntities) => jobEntities[jobId]?.outputs[key]
);

export const selectInputCollectionOfPostProcessingJob = (jobId: string, key: string) => createSelector(
  selectInputCollectionIdOfPostProcessingJob(jobId, key),
  selectCollectionEntities,
  (collectionId, collectionEntities) => collectionId ? collectionEntities[collectionId] : undefined
);

export const selectOutputCollectionOfPostProcessingJob = (jobId: string, key: string) => createSelector(
  selectOutputCollectionIdOfPostProcessingJob(jobId, key),
  selectCollectionEntities,
  (collectionId, collectionEntities) => collectionId ? collectionEntities[collectionId] : undefined
);

export const selectInputKeyOfAllJobs = (key: string) => createSelector(
  selectAllPostProcessingJobs,
  (jobs) => jobs.map(job => job.inputs[key]).filter(input => !!input)
);

export const selectOutputKeyOfAllJobs = (key: string) => createSelector(
  selectAllPostProcessingJobs,
  (jobs) => jobs.map(job => job.outputs[key]).filter(output => !!output)
);

export const selectPostProcessingJobsLoaded = createSelector(
  selectPostProcessingJobsState,
  (state) => state.loaded
);

export const selectPostProcessingJobsError = createSelector(
  selectPostProcessingJobsState,
  (state) => state.error
);
