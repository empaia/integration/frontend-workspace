import { Injectable } from '@angular/core';
import { AppV3DataService, AppV3JobsService } from 'empaia-api-lib';
import { Actions, concatLatestFrom, createEffect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { PostProcessingJobsActions } from './post-processing-jobs.actions';
import { PostBoolPrimitive, PrimitivesActions } from '@primitives/store';
import { PreprocessingJobsActions } from '@jobs/store/preprocessing-jobs/preprocessing-jobs.actions';
import * as TokenActions from '@token/store/token/token.actions';
import * as PreprocessingJobsSelectors from '@jobs/store/preprocessing-jobs/preprocessing-jobs.selectors';
import * as PostProcessingJobsSelectors from './post-processing-jobs.selectors';
import * as ScopeSelectors from '@scope/store/scope/scope.selectors';
import * as SlidesSelectors from '@slides/store/slides/slides.selectors';
import { pessimisticUpdate } from '@ngrx/router-store/data-persistence';
import { State } from './post-processing-jobs.reducer';
import {
  catchError,
  concatMap,
  filter,
  map,
  mergeMap,
  of,
  retry,
  retryWhen,
  switchMap,
} from 'rxjs';
import { filterNullish, retryOnAction } from '@shared/helper/rxjs-operators';
import { requestNewToken } from 'vendor-app-communication-interface';
import {
  JobKeyType,
  checkForMissingJobIOs,
  hasMissingJobIOs,
} from './post-processing-jobs.models';
import { CollectionsActions } from '@collections/store/collections/collections.actions';
import {
  CollectionItemType,
  DataCreatorType,
  PostPolygonCollection,
} from '@collections/store';
import { JobCreatorType, JobMode } from '../jobs/jobs.models';

@Injectable()
export class PostProcessingJobsEffects {
  // when a preprocessed job is selected (automatically happens after slide selection)
  // check if a post processing job for this slide is available
  // and if not create one
  checkForJobCreation$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(PreprocessingJobsActions.selectPreprocessingJobReady),
      map((action) => action.jobId),
      filterNullish(),
      concatLatestFrom(() => [
        this.store.select(
          PostProcessingJobsSelectors.selectAllPostProcessingJobsOfCurrentSlide
        ),
        this.store.select(ScopeSelectors.selectScopeId).pipe(filterNullish()),
        this.store
          .select(SlidesSelectors.selectSelectedSlideId)
          .pipe(filterNullish()),
        this.store.select(
          PreprocessingJobsSelectors.selectPreprocessingJobEntities
        ),
      ]),
      filter(([, jobs]) => !jobs?.length),
      map(([preprocessedJobId, _postJobs, scopeId, slideId, jobEntities]) =>
        PostProcessingJobsActions.createPostProcessingJob({
          scopeId,
          slideId,
          preprocessingCollectionId: jobEntities[preprocessedJobId]?.outputs[
            'preprocessing_regions'
          ] as string,
        })
      )
    );
  });

  // check if post processing job is missing one or more io's
  checkPostProcessingJobForMissingIos$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(
        PreprocessingJobsActions.selectPreprocessingJobReady,
        CollectionsActions.createCollectionFailure,
        PostProcessingJobsActions.setJobInputOrOutputFailure
      ),
      concatLatestFrom(() => [
        this.store
          .select(
            PostProcessingJobsSelectors.selectAllPostProcessingJobsOfCurrentSlide
          )
          .pipe(filterNullish()),
        this.store.select(ScopeSelectors.selectScopeId).pipe(filterNullish()),
        this.store
          .select(SlidesSelectors.selectSelectedSlideId)
          .pipe(filterNullish()),
        this.store.select(
          PreprocessingJobsSelectors.selectPreprocessingJobEntities
        ),
        this.store
          .select(PreprocessingJobsSelectors.selectSelectedPreprocessingJobId)
          .pipe(filterNullish()),
      ]),
      filter(([, jobs]) => !!jobs.length && !!hasMissingJobIOs(jobs[0])),
      mergeMap(
        ([, postJobs, scopeId, slideId, jobEntities, preprocessedJobId]) =>
          checkForMissingJobIOs(postJobs[0]).map((missingIo) =>
            PostProcessingJobsActions.repairMissingJobIO({
              scopeId,
              slideId,
              job: postJobs[0],
              preprocessingCollectionId: jobEntities[preprocessedJobId]
                ?.outputs['preprocessing_regions'] as string,
              missingIo,
            })
          )
      )
    );
  });

  repairPostProcessingJobIo$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(PostProcessingJobsActions.repairMissingJobIO),
      map((action) => {
        switch (action.missingIo.key) {
          case 'my_wsi':
            return PostProcessingJobsActions.setIdToJobInput({
              scopeId: action.scopeId,
              jobId: action.job.id,
              inputKey: action.missingIo.key,
              inputId: action.slideId,
            });
          case 'preprocessing_regions':
            return PostProcessingJobsActions.setIdToJobInput({
              scopeId: action.scopeId,
              jobId: action.job.id,
              inputKey: action.missingIo.key,
              inputId: action.preprocessingCollectionId,
            });
          // handdrawn_regions uses a fallthrough
          case 'handdrawn_regions':
          case 'selected_regions':
            return CollectionsActions.createCollection({
              scopeId: action.scopeId,
              jobId: action.job.id,
              key: action.missingIo.key,
              keyType: action.missingIo.keyType,
              postCollection: {
                type: 'collection',
                creator_id: action.scopeId,
                creator_type: DataCreatorType.Scope,
                item_type: CollectionItemType.Polygon,
              } as PostPolygonCollection,
            });
          case 'freeze':
            return PrimitivesActions.createPrimitive({
              scopeId: action.scopeId,
              jobId: action.job.id,
              postPrimitive: {
                name: 'freeze',
                creator_id: action.scopeId,
                creator_type: DataCreatorType.Scope,
                type: 'bool',
                value: false,
              } as PostBoolPrimitive,
            });
        }
      })
    );
  });

  createJob$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(PostProcessingJobsActions.createPostProcessingJob),
      pessimisticUpdate({
        run: (
          action: ReturnType<
            typeof PostProcessingJobsActions.createPostProcessingJob
          >,
          _state: State
        ) => {
          return this.jobsService
            .scopeIdJobsPost({
              scope_id: action.scopeId,
              body: {
                creator_id: action.scopeId,
                creator_type: JobCreatorType.Scope,
                mode: JobMode.Postprocessing,
                containerized: false,
              },
            })
            .pipe(
              retryWhen((errors) =>
                retryOnAction(
                  errors,
                  this.actions$,
                  TokenActions.setAccessToken,
                  requestNewToken
                )
              ),
              map((job) =>
                PostProcessingJobsActions.createPostProcessingJobSuccess({
                  job,
                  ...action,
                })
              )
            );
        },
        onError: (
          _action: ReturnType<
            typeof PostProcessingJobsActions.createPostProcessingJob
          >,
          error
        ) => {
          return PostProcessingJobsActions.createPostProcessingJobFailure({
            error,
          });
        },
      })
    );
  });

  setIdToJobInput$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(PostProcessingJobsActions.setIdToJobInput),
      pessimisticUpdate({
        run: (
          action: ReturnType<typeof PostProcessingJobsActions.setIdToJobInput>,
          _state: State
        ) => {
          return this.jobsService
            .scopeIdJobsJobIdInputsInputKeyPut({
              scope_id: action.scopeId,
              job_id: action.jobId,
              input_key: action.inputKey,
              body: {
                id: action.inputId,
              },
            })
            .pipe(
              retryWhen((errors) =>
                retryOnAction(
                  errors,
                  this.actions$,
                  TokenActions.setAccessToken,
                  requestNewToken
                )
              ),
              retry(3),
              map((job) =>
                PostProcessingJobsActions.setJobInputOrOutputSuccess({ job })
              )
            );
        },
        onError: (
          _action: ReturnType<typeof PostProcessingJobsActions.setIdToJobInput>,
          error
        ) => {
          return PostProcessingJobsActions.setJobInputOrOutputFailure({
            error,
          });
        },
      })
    );
  });

  setIdToJobOutput$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(PostProcessingJobsActions.setIdToJobOutput),
      pessimisticUpdate({
        run: (
          action: ReturnType<typeof PostProcessingJobsActions.setIdToJobOutput>,
          _state: State
        ) => {
          return this.jobsService
            .scopeIdJobsJobIdOutputsOutputKeyPut({
              scope_id: action.scopeId,
              job_id: action.jobId,
              output_key: action.outputKey,
              body: {
                id: action.outputId,
              },
            })
            .pipe(
              retryWhen((errors) =>
                retryOnAction(
                  errors,
                  this.actions$,
                  TokenActions.setAccessToken,
                  requestNewToken
                )
              ),
              retry(3),
              map((job) =>
                PostProcessingJobsActions.setJobInputOrOutputSuccess({ job })
              )
            );
        },
        onError: (
          _action: ReturnType<
            typeof PostProcessingJobsActions.setIdToJobOutput
          >,
          error
        ) => {
          return PostProcessingJobsActions.setJobInputOrOutputFailure({
            error,
          });
        },
      })
    );
  });

  removeJobInputKey$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(PostProcessingJobsActions.removeJobInputKey),
      pessimisticUpdate({
        run: (
          action: ReturnType<
            typeof PostProcessingJobsActions.removeJobInputKey
          >,
          _state: State
        ) => {
          return this.jobsService
            .scopeIdJobsJobIdInputsInputKeyDelete({
              scope_id: action.scopeId,
              job_id: action.jobId,
              input_key: action.inputKey,
            })
            .pipe(
              retryWhen((errors) =>
                retryOnAction(
                  errors,
                  this.actions$,
                  TokenActions.setAccessToken,
                  requestNewToken
                )
              ),
              map((job) =>
                PostProcessingJobsActions.removeJobInputOrOutputKeySuccess({
                  job,
                })
              )
            );
        },
        onError: (
          _action: ReturnType<
            typeof PostProcessingJobsActions.removeJobInputKey
          >,
          error
        ) => {
          return PostProcessingJobsActions.removeJobInputOrOutputKeyFailure({
            error,
          });
        },
      })
    );
  });

  removeJobOutputKey$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(PostProcessingJobsActions.removeJobOutputKey),
      pessimisticUpdate({
        run: (
          action: ReturnType<
            typeof PostProcessingJobsActions.removeJobOutputKey
          >,
          _state: State
        ) => {
          return this.jobsService
            .scopeIdJobsJobIdOutputsOutputKeyDelete({
              scope_id: action.scopeId,
              job_id: action.jobId,
              output_key: action.outputKey,
            })
            .pipe(
              retryWhen((errors) =>
                retryOnAction(
                  errors,
                  this.actions$,
                  TokenActions.setAccessToken,
                  requestNewToken
                )
              ),
              map((job) =>
                PostProcessingJobsActions.removeJobInputOrOutputKeySuccess({
                  job,
                })
              )
            );
        },
        onError: (
          _action: ReturnType<
            typeof PostProcessingJobsActions.removeJobOutputKey
          >,
          error
        ) => {
          return PostProcessingJobsActions.removeJobInputOrOutputKeyFailure({
            error,
          });
        },
      })
    );
  });

  updateJobInputKey$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(PostProcessingJobsActions.updateJobInputKey),
      concatMap((action) =>
        this.jobsService
          .scopeIdJobsJobIdInputsInputKeyDelete({
            scope_id: action.scopeId,
            job_id: action.jobId,
            input_key: action.inputKey,
          })
          .pipe(
            retryWhen((errors) =>
              retryOnAction(
                errors,
                this.actions$,
                TokenActions.setAccessToken,
                requestNewToken
              )
            ),
            switchMap(() =>
              this.jobsService
                .scopeIdJobsJobIdInputsInputKeyPut({
                  scope_id: action.scopeId,
                  job_id: action.jobId,
                  input_key: action.inputKey,
                  body: {
                    id: action.inputId,
                  },
                })
                .pipe(
                  retryWhen((errors) =>
                    retryOnAction(
                      errors,
                      this.actions$,
                      TokenActions.setAccessToken,
                      requestNewToken
                    )
                  ),
                  map((job) =>
                    PostProcessingJobsActions.updateJobInputKeySuccess({ job })
                  ),
                  catchError((error) =>
                    of(
                      PostProcessingJobsActions.updateJobInputKeyFailure({
                        error,
                      })
                    )
                  )
                )
            ),
            // ignore the error, update is more of a upsert
            catchError(() =>
              of(PostProcessingJobsActions.setIdToJobInput({ ...action }))
            )
          )
      )
    );
  });

  updateJobOutputKey$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(PostProcessingJobsActions.updateJobOutputKey),
      concatMap((action) =>
        this.jobsService
          .scopeIdJobsJobIdOutputsOutputKeyDelete({
            scope_id: action.scopeId,
            job_id: action.jobId,
            output_key: action.outputKey,
          })
          .pipe(
            retryWhen((errors) =>
              retryOnAction(
                errors,
                this.actions$,
                TokenActions.setAccessToken,
                requestNewToken
              )
            ),
            switchMap(() =>
              this.jobsService
                .scopeIdJobsJobIdOutputsOutputKeyPut({
                  scope_id: action.scopeId,
                  job_id: action.jobId,
                  output_key: action.outputKey,
                  body: {
                    id: action.outputId,
                  },
                })
                .pipe(
                  retryWhen((errors) =>
                    retryOnAction(
                      errors,
                      this.actions$,
                      TokenActions.setAccessToken,
                      requestNewToken
                    )
                  ),
                  map((job) =>
                    PostProcessingJobsActions.updateJobOutputKeySuccess({ job })
                  ),
                  catchError((error) =>
                    of(
                      PostProcessingJobsActions.updateJobOutputKeyFailure({
                        error,
                      })
                    )
                  )
                )
            ),
            // ignore the error, update is more of a upsert
            catchError(() =>
              of(PostProcessingJobsActions.setIdToJobOutput({ ...action }))
            )
          )
      )
    );
  });

  // create collections and primitive after job creation
  createInputAndOutputs$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(PostProcessingJobsActions.createPostProcessingJobSuccess),
      mergeMap((action) => [
        // set wsi as input
        PostProcessingJobsActions.setIdToJobInput({
          scopeId: action.scopeId,
          jobId: action.job.id,
          inputKey: 'my_wsi',
          inputId: action.slideId,
        }),
        // set preprocessing regions as input
        PostProcessingJobsActions.setIdToJobInput({
          scopeId: action.scopeId,
          jobId: action.job.id,
          inputKey: 'preprocessing_regions',
          inputId: action.preprocessingCollectionId,
        }),
        // create and set hand drawn regions collection
        CollectionsActions.createCollection({
          scopeId: action.scopeId,
          jobId: action.job.id,
          key: 'handdrawn_regions',
          keyType: JobKeyType.INPUT,
          postCollection: {
            type: 'collection',
            creator_id: action.scopeId,
            creator_type: DataCreatorType.Scope,
            item_type: CollectionItemType.Polygon,
          } as PostPolygonCollection,
        }),
        // create and set selected regions collection
        CollectionsActions.createCollection({
          scopeId: action.scopeId,
          jobId: action.job.id,
          key: 'selected_regions',
          keyType: JobKeyType.OUTPUT,
          postCollection: {
            type: 'collection',
            creator_id: action.scopeId,
            creator_type: DataCreatorType.Scope,
            item_type: CollectionItemType.Polygon,
          } as PostPolygonCollection,
        }),
        // create freeze primitive
        PrimitivesActions.createPrimitive({
          scopeId: action.scopeId,
          jobId: action.job.id,
          postPrimitive: {
            name: 'freeze',
            creator_id: action.scopeId,
            creator_type: DataCreatorType.Scope,
            type: 'bool',
            value: false,
          } as PostBoolPrimitive,
        }),
      ])
    );
  });

  // add create primitive to job output
  addPrimitiveToJobOutput$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(PrimitivesActions.createPrimitiveSuccess),
      concatLatestFrom(() =>
        this.store.select(ScopeSelectors.selectScopeId).pipe(filterNullish())
      ),
      map(([action, scopeId]) =>
        PostProcessingJobsActions.updateJobOutputKey({
          scopeId,
          jobId: action.jobId,
          outputKey: 'freeze',
          outputId: action.primitive.id as string,
        })
      )
    );
  });

  constructor(
    private readonly actions$: Actions,
    private readonly store: Store,
    private readonly jobsService: AppV3JobsService,
    private readonly dataService: AppV3DataService
  ) {}
}
