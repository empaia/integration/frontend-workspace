import { HttpErrorResponse } from '@angular/common/http';
import { EntityState, createEntityAdapter } from '@ngrx/entity';
import { createReducer, on } from '@ngrx/store';
import { PostProcessingJobsActions } from './post-processing-jobs.actions';
import { Job } from '../jobs/jobs.models';


export const POST_PROCESSING_JOBS_FEATURE_KEY = 'postProcessingJobs';

export interface State extends EntityState<Job> {
  loaded: boolean;
  error?: HttpErrorResponse | undefined;
}

export const postProcessingJobsAdapter = createEntityAdapter<Job>();

export const initialState: State = postProcessingJobsAdapter.getInitialState({
  loaded: true,
  error: undefined,
});

export const reducer = createReducer(
  initialState,
  on(PostProcessingJobsActions.setPostProcessingJob, (state, { job }): State =>
    postProcessingJobsAdapter.upsertOne(job, {
      ...state,
      loaded: true,
    })
  ),
  on(PostProcessingJobsActions.setPostProcessingJobs, (state, { jobs }): State =>
    postProcessingJobsAdapter.upsertMany(jobs, {
      ...state,
      loaded: true,
    })
  ),
  on(PostProcessingJobsActions.clearPostProcessingJobs, (state): State =>
    postProcessingJobsAdapter.removeAll({
      ...state,
      ...initialState,
    })
  ),
  on(PostProcessingJobsActions.createPostProcessingJob, (state): State => ({
    ...state,
    loaded: false,
  })),
  on(PostProcessingJobsActions.createPostProcessingJobSuccess, (state, { job }): State =>
    postProcessingJobsAdapter.addOne(job, {
      ...state,
      loaded: true,
    })
  ),
  on(PostProcessingJobsActions.createPostProcessingJobFailure, (state, { error }): State => ({
    ...state,
    loaded: true,
    error,
  })),
  on(PostProcessingJobsActions.setIdToJobInput, (state): State => ({
    ...state,
    loaded: false,
  })),
  on(PostProcessingJobsActions.setIdToJobOutput, (state): State => ({
    ...state,
    loaded: false,
  })),
  on(PostProcessingJobsActions.setJobInputOrOutputSuccess, (state, { job }): State =>
    postProcessingJobsAdapter.upsertOne(job, {
      ...state,
      loaded: true,
    })
  ),
  on(PostProcessingJobsActions.setJobInputOrOutputFailure, (state, { error }): State => ({
    ...state,
    loaded: true,
    error,
  })),
  on(PostProcessingJobsActions.removeJobInputKey, (state): State => ({
    ...state,
    loaded: false,
  })),
  on(PostProcessingJobsActions.removeJobOutputKey, (state): State => ({
    ...state,
    loaded: false,
  })),
  on(PostProcessingJobsActions.removeJobInputOrOutputKeySuccess, (state, { job }): State =>
    postProcessingJobsAdapter.upsertOne(job, {
      ...state,
      loaded: true,
    })
  ),
  on(PostProcessingJobsActions.removeJobInputOrOutputKeyFailure, (state, { error }): State => ({
    ...state,
    loaded: true,
    error,
  })),
  on(PostProcessingJobsActions.updateJobInputKey, (state): State => ({
    ...state,
    loaded: false,
  })),
  on(PostProcessingJobsActions.updateJobInputKeySuccess, (state, { job }): State =>
    postProcessingJobsAdapter.upsertOne(job, {
      ...state,
      loaded: true,
    })
  ),
  on(PostProcessingJobsActions.updateJobInputKeyFailure, (state, { error }): State => ({
    ...state,
    loaded: true,
    error,
  })),
  on(PostProcessingJobsActions.updateJobOutputKey, (state): State => ({
    ...state,
    loaded: false,
  })),
  on(PostProcessingJobsActions.updateJobOutputKeySuccess, (state, { job }): State =>
    postProcessingJobsAdapter.upsertOne(job, {
      ...state,
      loaded: true,
    })
  ),
  on(PostProcessingJobsActions.updateJobOutputKeyFailure, (state, { error }): State => ({
    ...state,
    loaded: true,
    error,
  }))
);
