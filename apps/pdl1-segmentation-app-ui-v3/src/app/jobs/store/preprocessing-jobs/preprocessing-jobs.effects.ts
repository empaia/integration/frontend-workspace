import { Injectable } from '@angular/core';
import { Actions, concatLatestFrom, createEffect, ofType } from '@ngrx/effects';
import { PreprocessingJobsActions } from './preprocessing-jobs.actions';
import { SlidesActions } from '@slides/store/slides/slides.actions';
import * as PreprocessingJobsSelectors from './preprocessing-jobs.selectors';
import * as PostProcessingJobsSelectors from '@jobs/store/post-processing-jobs/post-processing-jobs.selectors';
import * as ScopeSelectors from '@scope/store/scope/scope.selectors';
import { exhaustMap, map, of } from 'rxjs';
import { Store } from '@ngrx/store';
import { filterNullish } from '@shared/helper/rxjs-operators';
import { PostProcessingJobsActions } from '@jobs/store/post-processing-jobs/post-processing-jobs.actions';
import { MatDialog } from '@angular/material/dialog';
import { PrimitivesSelectors } from '@primitives/store';
import { ChangeLossInfoDialogComponent } from '@shared/components/change-loss-info-dialog/change-loss-info-dialog.component';
import { SMALL_DIALOG_WIDTH } from '@menu/store/menu/menu.models';



@Injectable()
export class PreprocessingJobsEffects {

  // when a slide selected, select the first
  // preprocessing job
  selectFirstJob$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(
        SlidesActions.selectSlideReady,
        PreprocessingJobsActions.setPreprocessingJobs
      ),
      concatLatestFrom(() => [
        this.store.select(PostProcessingJobsSelectors.selectInputCollectionIdOfCurrentSlideJob('preprocessing_regions')),
        this.store.select(PreprocessingJobsSelectors.selectAllPreprocessingJobsOfCurrentSlide).pipe(filterNullish())
      ]),
      map(([, postProcessingCollectionId, jobs]) =>
        jobs.find(job => job.outputs['preprocessing_regions'] === postProcessingCollectionId)?.id ?? jobs[0].id
      ),
      map(jobId => PreprocessingJobsActions.selectPreprocessingJob({ jobId }))
    );
  });

  // change job input of preprocessing regions collections
  // when another preprocessing job was selected
  changePreprocessingJobInput$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(PreprocessingJobsActions.selectPreprocessingJobReady),
      map(action => action.jobId),
      filterNullish(),
      concatLatestFrom(() => [
        this.store.select(ScopeSelectors.selectScopeId).pipe(filterNullish()),
        this.store.select(PreprocessingJobsSelectors.selectPreprocessingJobEntities),
        this.store.select(PostProcessingJobsSelectors.selectAllPostProcessingJobsOfCurrentSlide).pipe(
          filterNullish(),
          map(jobIds => jobIds[0]?.id),
          filterNullish(),
        )
      ]),
      map(([preprocessingJobId, scopeId, jobEntities, postProcessingJobId]) => PostProcessingJobsActions.updateJobInputKey({
        scopeId,
        jobId: postProcessingJobId,
        inputKey: 'preprocessing_regions',
        inputId: jobEntities[preprocessingJobId]?.outputs['preprocessing_regions'] as string
      }))
    );
  });

  // open change loss dialog when a user want to switch the preprocessing job
  // when not all changes are applied
  openDialogOnPreprocessingJobSelection$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(PreprocessingJobsActions.selectPreprocessingJob),
      map(action => action.jobId),
      concatLatestFrom(() => [
        this.store.select(PrimitivesSelectors.selectAreChangesToApply)
      ]),
      exhaustMap(([jobId, allApplied]) =>
        allApplied
          ? this.dialog
            .open(ChangeLossInfoDialogComponent, {
              data: true,
              width: SMALL_DIALOG_WIDTH
            })
            .afterClosed()
            .pipe(
              filterNullish(),
              map(() => PreprocessingJobsActions.selectPreprocessingJobReady({ jobId }))
            )
          : of(PreprocessingJobsActions.selectPreprocessingJobReady({ jobId }))
      )
    );
  });

  constructor(
    private readonly actions$: Actions,
    private readonly store: Store,
    private readonly dialog: MatDialog,
  ) {}
}
