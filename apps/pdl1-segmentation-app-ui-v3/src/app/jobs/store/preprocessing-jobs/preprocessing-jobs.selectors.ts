import { createSelector } from '@ngrx/store';
import { selectJobsFeatureState } from '../jobs-feature.state';
import { preprocessingJobsAdapter, PREPROCESSING_JOBS_FEATURE_KEY } from './preprocessing-jobs.reducer';
import { Job, isPreprocessingJob } from '../jobs/jobs.models';
import { selectAllJobIdsOfCurrentSlide } from '../jobs/jobs.selectors';
import { selectCollectionEntities } from '@collections/store/collections/collections.selectors';

const {
  selectAll,
  selectEntities,
} = preprocessingJobsAdapter.getSelectors();

export const selectPreprocessingJobsState = createSelector(
  selectJobsFeatureState,
  (state) => state[PREPROCESSING_JOBS_FEATURE_KEY]
);

export const selectAllPreprocessingJobs = createSelector(
  selectPreprocessingJobsState,
  selectAll,
);

export const selectPreprocessingJobEntities = createSelector(
  selectPreprocessingJobsState,
  selectEntities,
);

export const selectSelectedPreprocessingJobId = createSelector(
  selectPreprocessingJobsState,
  (state) => state.selected
);

export const selectSelectedPreprocessingJob = createSelector(
  selectPreprocessingJobEntities,
  selectSelectedPreprocessingJobId,
  (jobEntities, selectedId) => selectedId ? jobEntities[selectedId] : undefined
);

export const selectAllPreprocessingJobsOfCurrentSlide = createSelector(
  selectPreprocessingJobEntities,
  selectAllJobIdsOfCurrentSlide,
  (jobEntities, jobIds) => {
    if (!jobIds) { return undefined; }

    return jobIds.map(jobId => jobEntities[jobId]).filter(job => !!job && isPreprocessingJob(job)) as Job[];
  }
);

export const selectPreprocessingJobsLoaded = createSelector(
  selectPreprocessingJobsState,
  (state) => state.loaded
);

export const selectPreprocessingJobsError = createSelector(
  selectPreprocessingJobsState,
  (state) => state.error
);

export const selectOutputCollectionIdOfCurrentSlideJob = (key: string) => createSelector(
  selectPreprocessingJobEntities,
  selectSelectedPreprocessingJobId,
  (jobs, selectedJobId) => selectedJobId ? jobs[selectedJobId]?.outputs[key] : undefined
);

export const selectOutputCollectionOfCurrentSlideJob = (key: string) => createSelector(
  selectOutputCollectionIdOfCurrentSlideJob(key),
  selectCollectionEntities,
  (collectionId, collectionEntities) => collectionId ? collectionEntities[collectionId] : undefined
);

export const selectOutputCollectionIdOfPreprocessingJob = (jobId: string, key: string) => createSelector(
  selectPreprocessingJobEntities,
  (jobEntities) => jobEntities[jobId]?.outputs[key]
);

export const selectOutputCollectionOfPreprocessingJob = (jobId: string, key: string) => createSelector(
  selectOutputCollectionIdOfPreprocessingJob(jobId, key),
  selectCollectionEntities,
  (collectionId, collectionEntities) => collectionId ? collectionEntities[collectionId] : undefined
);
