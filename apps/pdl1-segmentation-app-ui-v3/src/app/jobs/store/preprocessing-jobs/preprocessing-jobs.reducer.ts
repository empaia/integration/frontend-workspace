import { HttpErrorResponse } from '@angular/common/http';
import { EntityState, createEntityAdapter } from '@ngrx/entity';
import { createReducer, on } from '@ngrx/store';
import { PreprocessingJobsActions } from './preprocessing-jobs.actions';
import { Job } from '../jobs/jobs.models';


export const PREPROCESSING_JOBS_FEATURE_KEY = 'preprocessingJobs';

export interface State extends EntityState<Job> {
  loaded: boolean;
  selected?: string | undefined;
  error?: HttpErrorResponse | undefined;
}

export const preprocessingJobsAdapter = createEntityAdapter<Job>();

export const initialState: State = preprocessingJobsAdapter.getInitialState({
  loaded: true,
  selected: undefined,
  error: undefined,
});

export const reducer = createReducer(
  initialState,
  on(PreprocessingJobsActions.setPreprocessingJob, (state, { job }): State =>
    preprocessingJobsAdapter.upsertOne(job, {
      ...state,
      loaded: true,
    }),
  ),
  on(PreprocessingJobsActions.setPreprocessingJobs, (state, { jobs }): State =>
    preprocessingJobsAdapter.upsertMany(jobs, {
      ...state,
      loaded: true,
    })
  ),
  on(PreprocessingJobsActions.clearPreprocessingJobs, (state): State =>
    preprocessingJobsAdapter.removeAll({
      ...state,
      ...initialState,
    })
  ),
  on(PreprocessingJobsActions.selectPreprocessingJobReady, (state, { jobId }): State => ({
    ...state,
    selected: jobId,
  })),
);
