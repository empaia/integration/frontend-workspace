import { createActionGroup, emptyProps, props } from '@ngrx/store';
import { Job } from '../jobs/jobs.models';

export const PreprocessingJobsActions = createActionGroup({
  source: 'PreprocessingJobs',
  events: {
    'Set Preprocessing Job': props<{ job: Job }>(),
    'Set Preprocessing Jobs': props<{ jobs: Job[] }>(),
    'Clear Preprocessing Jobs': emptyProps(),

    'Select Preprocessing Job': props<{ jobId?: string | undefined }>(),
    'Select Preprocessing Job Ready': props<{ jobId?: string | undefined }>(),
  }
});
