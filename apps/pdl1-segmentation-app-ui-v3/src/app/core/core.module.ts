import { MenuModule } from '@menu/menu.module';
import { MaterialModule } from '@material/material.module';
import { SlideViewerModule } from 'slide-viewer';
import { AppComponent } from './containers/app.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoadingIndicatorComponent } from './components/loading-indicator/loading-indicator.component';
import { PushPipe } from '@ngrx/component';
import { PersistenceOverlayComponent } from './components/persistence-overlay/persistence-overlay.component';



@NgModule({
  declarations: [
    AppComponent,
    LoadingIndicatorComponent,
    PersistenceOverlayComponent
  ],
  imports: [
    CommonModule,
    MaterialModule,
    PushPipe,
    MenuModule,
    SlideViewerModule,
  ]
})
export class CoreModule { }
