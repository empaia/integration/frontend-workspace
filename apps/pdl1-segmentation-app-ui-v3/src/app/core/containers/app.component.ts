import { Component, NgZone, OnInit } from '@angular/core';
import {
  AnnotationsColorMapsSelectors,
  AnnotationsUiActions,
  AnnotationsUiSelectors,
  AnnotationsViewerActions,
  AnnotationsViewerSelectors,
  HandDrawnAnnotationsActions,
  SelectedAnnotationsActions
} from '@annotations/store';
import { MenuActions, MenuSelectors } from '@menu/store';
import { Store } from '@ngrx/store';
import { PrimitivesSelectors } from '@primitives/store';
import { ScopeActions, ScopeSelectors } from '@scope/store';
import { SlidesSelectors } from '@slides/store';
import { TokenActions } from '@token/store';
import { WbsUrlActions } from '@wbs-url/store';
import { asyncScheduler } from 'rxjs';
import { observeOn } from 'rxjs/operators';
import { Annotation, AnnotationHighlightConfig, CurrentView, HighlightType, UiConfig } from 'slide-viewer';
import { addScopeListener, addTokenListener, addWbsUrlListener, Scope, Token, WbsUrl } from 'vendor-app-communication-interface';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  public slide$ = this.store.select(SlidesSelectors.selectSelectedSlideImageView);
  public selectedSlideId$ = this.store.select(SlidesSelectors.selectSelectedSlideId);
  public annotationIds$ = this.store.select(AnnotationsViewerSelectors.selectAllAnnotationViewerIds);
  public annotations$ = this.store.select(AnnotationsViewerSelectors.selectAllViewerAnnotations);
  public annotationLoaded$ = this.store.select(AnnotationsViewerSelectors.selectAnnotationsViewerLoaded).pipe(
    observeOn(asyncScheduler)
  );
  public centroids$ = this.store.select(AnnotationsViewerSelectors.selectAnnotationsViewerCentroids);
  public allowedInteractionTypes$ = this.store.select(AnnotationsUiSelectors.selectAllowedInteractionTypes);
  public selectedInteractionType$ = this.store.select(AnnotationsUiSelectors.selectInteractionType);
  public focusAnnotation$ = this.store.select(AnnotationsViewerSelectors.selectFocusedViewerAnnotationId);
  public removeAnnotations$ = this.store.select(AnnotationsViewerSelectors.selectRemovedAnnotationIds);
  public clearAnnotations$ = this.store.select(AnnotationsViewerSelectors.selectAnnotationsClearState);
  public hiddenAnnotationIds$ = this.store.select(AnnotationsViewerSelectors.selectHiddenAnnotationIds);
  public examinationState$ = this.store.select(ScopeSelectors.selectExaminationState);
  public menuSize$ = this.store.select(MenuSelectors.selectMenuSize);
  public classColorMapping$ = this.store.select(AnnotationsColorMapsSelectors.selectSelectedAnnotationClassColorMapping);

  // activates overlay while persisting changes
  public allPersisted$ = this.store.select(PrimitivesSelectors.selectAllChangesPersisted);

  public uiConfig: UiConfig = {
    showAnnotationBar: true,
    autoSetAnnotationTitle: true,
    renderHideNavigationButton: true,
  };

  public annotationHighlightConfig: AnnotationHighlightConfig = {
    highlightType: HighlightType.CLICK
  };

  public readonly CLUSTER_DISTANCE = 60;

  constructor(
    private store: Store,
    private ngZone: NgZone
  ) { }

  public ngOnInit(): void {
    addTokenListener((token: Token) => {
      this.ngZone.run(() => {
        this.store.dispatch(TokenActions.setAccessToken({ token }));
      });
    });

    addScopeListener((scope: Scope) => {
      this.ngZone.run(() => {
        this.store.dispatch(ScopeActions.setScope({ scopeId: scope.id }));
      });
    });

    addWbsUrlListener((wbsUrl: WbsUrl) => {
      this.ngZone.run(() => {
        this.store.dispatch(WbsUrlActions.setWbsUrl({ wbsUrl }));
      });
    });
  }

  public toggleMenuVisibility(): void {
    this.store.dispatch(MenuActions.toggleMenu());
  }

  public requestAnnotations(annotationIds: string[]): void {
    this.store.dispatch(AnnotationsViewerActions.loadAnnotations({ annotationIds }));
  }

  public onViewerMoveEnd(currentView: CurrentView): void {
    this.store.dispatch(AnnotationsUiActions.setViewport({ currentView }));
  }

  public onAnnotationCreated(annotation: Annotation): void {
    this.store.dispatch(HandDrawnAnnotationsActions.createAnnotation({ annotation }));
  }

  public onAnnotationClicked(ids: string[]): void {
    this.store.dispatch(SelectedAnnotationsActions.clickAnnotationId({ annotationId: ids[0] }));
  }
}
