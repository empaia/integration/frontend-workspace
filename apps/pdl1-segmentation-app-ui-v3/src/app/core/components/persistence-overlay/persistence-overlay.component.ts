import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
  selector: 'app-persistence-overlay',
  templateUrl: './persistence-overlay.component.html',
  styleUrls: ['./persistence-overlay.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PersistenceOverlayComponent {

}
