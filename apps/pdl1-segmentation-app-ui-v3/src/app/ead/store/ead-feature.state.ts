import { Action, combineReducers, createFeatureSelector } from '@ngrx/store';
import * as fromEad from '@ead/store/ead/ead.reducer';

export const EAD_MODULE_FEATURE_KEY = 'eadModuleFeature';

export const selectEadFeatureState = createFeatureSelector<State>(
  EAD_MODULE_FEATURE_KEY
);

export interface State {
  [fromEad.EAD_FEATURE_KEY]: fromEad.State;
}

export function reducers(state: State | undefined, action: Action) {
  return combineReducers({
    [fromEad.EAD_FEATURE_KEY]: fromEad.reducer,
  })(state, action);
}
