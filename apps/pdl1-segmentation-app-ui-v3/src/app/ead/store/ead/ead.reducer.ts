import { createReducer, on } from '@ngrx/store';
import { EadActions } from './ead.actions';
import { EmpaiaAppDescriptionV3Mode } from 'empaia-ui-commons';


export const EAD_FEATURE_KEY = 'ead';

export interface State {
  modes: EmpaiaAppDescriptionV3Mode[],
  active: EmpaiaAppDescriptionV3Mode,
}

export const initialState: State = {
  modes: [],
  active: 'standalone',
};

export const reducer = createReducer(
  initialState,
  on(EadActions.setV3Modes, (state, { modes }): State => ({
    ...state,
    modes,
  })),
  on(EadActions.setActiveV3Mode, (state, { active }): State => ({
    ...state,
    active,
  })),
);
