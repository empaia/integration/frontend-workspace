import { Injectable } from '@angular/core';
import { Actions } from '@ngrx/effects';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Store } from '@ngrx/store';
import { EadService } from 'empaia-ui-commons';



@Injectable()
export class EadEffects {

  constructor(
    private readonly actions$: Actions,
    private readonly eadService: EadService,
    private readonly snackBar: MatSnackBar,
    private readonly store: Store,
  ) {}
}
