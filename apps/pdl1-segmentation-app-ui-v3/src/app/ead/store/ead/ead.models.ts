import { EAD_COMPATIBILITY_ERROR } from '@menu/models/ui.models';

export const EadErrorMap = {
  '[EmpaiaAppDescription] Check Ead Input Compatibility Failure': EAD_COMPATIBILITY_ERROR,
};

export const INCOMPATIBLE_APP = { message: 'App is not compatible with rectangle, polygon or circle input in standalone mode!' };
