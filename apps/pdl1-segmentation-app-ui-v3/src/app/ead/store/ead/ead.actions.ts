import { createActionGroup, props } from '@ngrx/store';
import { EmpaiaAppDescriptionV3Mode } from 'empaia-ui-commons';

export const EadActions = createActionGroup({
  source: 'EmpaiaAppDescription',
  events: {
    'Set V3 Modes': props<{ modes: EmpaiaAppDescriptionV3Mode[] }>(),
    'Set Active V3 Mode': props<{ active: EmpaiaAppDescriptionV3Mode }>(),
    'Check Ead Input Compatibility Failure': props<{ error: object }>(),
  }
});
