import { createSelector } from '@ngrx/store';
import { selectEadFeatureState } from '../ead-feature.state';
import { EAD_FEATURE_KEY } from './ead.reducer';

export const selectEadState = createSelector(
  selectEadFeatureState,
  (state) => state[EAD_FEATURE_KEY]
);

export const selectEadModes = createSelector(
  selectEadState,
  (state) => state.modes
);

export const selectActiveMode = createSelector(
  selectEadState,
  (state) => state.active
);
