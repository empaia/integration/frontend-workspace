import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';
import { EAD_MODULE_FEATURE_KEY, reducers } from './store/ead-feature.state';
import { EffectsModule } from '@ngrx/effects';
import { EadEffects } from './store';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    StoreModule.forFeature(
      EAD_MODULE_FEATURE_KEY,
      reducers
    ),
    EffectsModule.forFeature([
      EadEffects
    ])
  ]
})
export class EadModule { }
