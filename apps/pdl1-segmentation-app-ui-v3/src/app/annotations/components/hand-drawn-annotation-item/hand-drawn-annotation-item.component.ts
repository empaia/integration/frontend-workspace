import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { MatCheckboxChange } from '@angular/material/checkbox';
import { AnnotationSelector } from '@annotations/store';
import { AnnotationEntity } from 'slide-viewer';

@Component({
  selector: 'app-hand-drawn-annotation-item',
  templateUrl: './hand-drawn-annotation-item.component.html',
  styleUrls: ['./hand-drawn-annotation-item.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class HandDrawnAnnotationItemComponent {
  @Input() public annotation!: AnnotationEntity;
  @Input() public selected!: boolean;
  @Input() public disabled!: boolean;

  @Output() public changeSelection = new EventEmitter<AnnotationSelector>();
  @Output() public zoomToAnnotation = new EventEmitter<AnnotationEntity>();
  @Output() public deleteAnnotation = new EventEmitter<string>();

  public onCheckboxChange(change: MatCheckboxChange): void {
    const annotationSelector: AnnotationSelector = {
      id: this.annotation.id,
      checked: change.checked,
    };
    this.changeSelection.emit(annotationSelector);
  }
}
