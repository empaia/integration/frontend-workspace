import { Component, EventEmitter, Input, Output } from '@angular/core';
import { AnnotationSelector } from '@annotations/store';
import { AnnotationIdObject } from '@annotations/store/selected-annotations/selected-annotations.models';
import { Dictionary } from '@ngrx/entity';
import { AnnotationEntity } from 'slide-viewer';

@Component({
  selector: 'app-preprocessing-annotation-list',
  templateUrl: './preprocessing-annotation-list.component.html',
  styleUrls: ['./preprocessing-annotation-list.component.scss']
})
export class PreprocessingAnnotationListComponent {
  @Input() public annotationEntities!: AnnotationEntity[];
  @Input() public annotationSelection!: Map<string, AnnotationIdObject>;
  @Input() public loaded!: boolean;
  @Input() public hiddenAnnotationSelection!: Dictionary<AnnotationSelector>;

  // inputs for superior checkbox
  @Input() public allSelected!: boolean;
  @Input() public someSelected!: boolean;
  @Input() public disabled!: boolean;

  @Output() public selectAllChanged = new EventEmitter<boolean>();
  @Output() public changeAnnotationSelection = new EventEmitter<AnnotationSelector>();
  @Output() public zoomToAnnotation = new EventEmitter<AnnotationEntity>();
  @Output() public changeHiddenAnnotationSelection = new EventEmitter<AnnotationSelector>();
}
