import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { AnnotationSelector } from '@annotations/store';
import { AnnotationIdObject } from '@annotations/store/selected-annotations/selected-annotations.models';
import { AnnotationEntity } from 'slide-viewer';

@Component({
  selector: 'app-hand-drawn-annotation-list',
  templateUrl: './hand-drawn-annotation-list.component.html',
  styleUrls: ['./hand-drawn-annotation-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class HandDrawnAnnotationListComponent {
  @Input() public annotationEntities!: AnnotationEntity[];
  @Input() public annotationSelection!: Map<string, AnnotationIdObject>;
  @Input() public loaded!: boolean;

  // inputs for superior checkbox
  @Input() public allSelected!: boolean;
  @Input() public someSelected!: boolean;
  @Input() public disabled!: boolean;

  @Output() public selectAllChanged = new EventEmitter<boolean>();
  @Output() public changeAnnotationSelection = new EventEmitter<AnnotationSelector>();
  @Output() public zoomToAnnotation = new EventEmitter<AnnotationEntity>();
  @Output() public deleteAnnotation = new EventEmitter<string>();
}
