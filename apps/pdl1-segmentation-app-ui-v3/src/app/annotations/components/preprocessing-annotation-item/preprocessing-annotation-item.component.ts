import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { MatCheckboxChange } from '@angular/material/checkbox';
import { AnnotationSelector } from '@annotations/store';
import { AnnotationEntity } from 'slide-viewer';

@Component({
  selector: 'app-preprocessing-annotation-item',
  templateUrl: './preprocessing-annotation-item.component.html',
  styleUrls: ['./preprocessing-annotation-item.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PreprocessingAnnotationItemComponent {
  @Input() public annotation!: AnnotationEntity;
  @Input() public selected!: boolean;
  @Input() public hidden!: boolean;
  @Input() public disabled!: boolean;

  @Output() public changeSelection = new EventEmitter<AnnotationSelector>();
  @Output() public zoomToAnnotation = new EventEmitter<AnnotationEntity>();
  @Output() public changeHiddenState = new EventEmitter<AnnotationSelector>();

  public onCheckboxChange(change: MatCheckboxChange): void {
    const annotationSelector: AnnotationSelector = {
      id: this.annotation.id,
      checked: change.checked,
    };
    this.changeSelection.emit(annotationSelector);
  }

  public onHideClicked(): void {
    const annotationSelector: AnnotationSelector = {
      id: this.annotation.id,
      checked: !this.hidden,
    };
    this.changeHiddenState.emit(annotationSelector);
  }
}
