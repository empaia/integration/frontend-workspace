import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';
import { ANNOTATIONS_MODULE_FEATURE_KEY, reducers } from './store/annotations-feature.state';
import { EffectsModule } from '@ngrx/effects';
import {
  AnnotationsColorMapsEffects,
  AnnotationsUiEffects,
  AnnotationsViewerEffects,
  HandDrawnAnnotationsEffects,
  PreprocessingAnnotationsEffects,
  SelectedAnnotationsEffects
} from './store';
import { PreprocessingAnnotationItemComponent } from './components/preprocessing-annotation-item/preprocessing-annotation-item.component';
import { MaterialModule } from '@material/material.module';
import { SharedModule } from '@shared/shared.module';
import { PreprocessingAnnotationListComponent } from './components/preprocessing-annotation-list/preprocessing-annotation-list.component';
import { PreprocessingAnnotationsContainerComponent } from './containers/preprocessing-annotations-container/preprocessing-annotations-container.component';
import { LetDirective, PushPipe } from '@ngrx/component';
import { HandDrawnAnnotationItemComponent } from './components/hand-drawn-annotation-item/hand-drawn-annotation-item.component';
import { HandDrawnAnnotationListComponent } from './components/hand-drawn-annotation-list/hand-drawn-annotation-list.component';
import { HandDrawnAnnotationsContainerComponent } from './containers/hand-drawn-annotations-container/hand-drawn-annotations-container.component';
import { CommonUiModule } from 'empaia-ui-commons';



@NgModule({
  declarations: [
    PreprocessingAnnotationItemComponent,
    PreprocessingAnnotationListComponent,
    PreprocessingAnnotationsContainerComponent,
    HandDrawnAnnotationItemComponent,
    HandDrawnAnnotationListComponent,
    HandDrawnAnnotationsContainerComponent,
  ],
  imports: [
    CommonModule,
    CommonUiModule,
    MaterialModule,
    SharedModule,
    LetDirective,
    PushPipe,
    StoreModule.forFeature(
      ANNOTATIONS_MODULE_FEATURE_KEY,
      reducers,
    ),
    EffectsModule.forFeature([
      AnnotationsUiEffects,
      AnnotationsViewerEffects,
      PreprocessingAnnotationsEffects,
      HandDrawnAnnotationsEffects,
      SelectedAnnotationsEffects,
      AnnotationsColorMapsEffects,
    ])
  ],
  exports: [
    PreprocessingAnnotationsContainerComponent,
    HandDrawnAnnotationsContainerComponent,
  ]
})
export class AnnotationsModule { }
