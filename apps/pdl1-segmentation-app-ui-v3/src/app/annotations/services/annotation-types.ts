import {
  ArrowAnnotation,
  CircleAnnotation,
  LineAnnotation,
  PointAnnotation,
  PolygonAnnotation,
  PostArrowAnnotation,
  PostCircleAnnotation,
  PostLineAnnotation,
  PostPointAnnotation,
  PostPolygonAnnotation,
  PostRectangleAnnotation,
  RectangleAnnotation } from '@annotations/store/annotations-viewer/annotations-viewer.models';
import {
  AnnotationCircle,
  AnnotationRectangle,
  AnnotationPolygon,
  AnnotationPoint
} from 'slide-viewer';

export type AnnotationApiOutput =
  | PointAnnotation
  | LineAnnotation
  | ArrowAnnotation
  | CircleAnnotation
  | RectangleAnnotation
  | PolygonAnnotation
  ;

export type AnnotationViewerInput =
  | AnnotationCircle
  | AnnotationRectangle
  | AnnotationPolygon
  | AnnotationPoint
  ;

export type AnnotationApiPostType =
  | PostArrowAnnotation
  | PostCircleAnnotation
  | PostLineAnnotation
  | PostRectangleAnnotation
  | PostPolygonAnnotation
  | PostPointAnnotation
  ;
