import { ChangeDetectionStrategy, Component } from '@angular/core';
import { AnnotationSelector, AnnotationsViewerActions, AnnotationsViewerSelectors, PreprocessingAnnotationsActions, PreprocessingAnnotationsSelectors, SelectedAnnotationsActions, SelectedAnnotationsSelectors } from '@annotations/store';
import { AnnotationIdObject } from '@annotations/store/selected-annotations/selected-annotations.models';
import { PostProcessingJobsSelectors } from '@jobs/store';
import { Dictionary } from '@ngrx/entity';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { AnnotationEntity } from 'slide-viewer';

@Component({
  selector: 'app-preprocessing-annotations-container',
  templateUrl: './preprocessing-annotations-container.component.html',
  styleUrls: ['./preprocessing-annotations-container.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PreprocessingAnnotationsContainerComponent {
  public preprocessingAnnotationEntities$: Observable<AnnotationEntity[]>;
  public preprocessingAnnotationSelection$: Observable<Map<string, AnnotationIdObject>>;
  public hiddenAnnotationSelection$: Observable<Dictionary<AnnotationSelector>>;
  public allSelected$: Observable<boolean>;
  public indeterminateSelection$: Observable<boolean>;
  public loaded$: Observable<boolean>;
  // used to check if selected collection is ready for user input
  public selectedRegionsCollection$: Observable<string | undefined>;

  constructor(private store: Store) {
    this.preprocessingAnnotationEntities$ = this.store.select(PreprocessingAnnotationsSelectors.selectAllPreprocessingAnnotations);
    this.preprocessingAnnotationSelection$ = this.store.select(SelectedAnnotationsSelectors.selectFilteredAnnotationIdEntities);
    this.hiddenAnnotationSelection$ = this.store.select(AnnotationsViewerSelectors.selectHiddenAnnotationEntities);
    this.allSelected$ = this.store.select(PreprocessingAnnotationsSelectors.selectAllSelectedState);
    this.indeterminateSelection$ = this.store.select(PreprocessingAnnotationsSelectors.selectIndeterminateAnnotationSelection);
    this.loaded$ = this.store.select(PreprocessingAnnotationsSelectors.selectPreprocessingAnnotationReadyToSelect);
    this.selectedRegionsCollection$ = this.store.select(PostProcessingJobsSelectors.selectOutputCollectionIdOfCurrentSlideJob('selected_regions'));
  }

  public onZoomToAnnotation(focus: AnnotationEntity): void {
    this.store.dispatch(AnnotationsViewerActions.zoomToAnnotation({ focus }));
  }

  public onAllAnnotationSelectChanged(allSelected: boolean): void {
    this.store.dispatch(PreprocessingAnnotationsActions.selectAllAnnotations({ allSelected }));
  }

  public onAnnotationSelectionChanged(annotationSelector: AnnotationSelector): void {
    if (annotationSelector.checked) {
      this.store.dispatch(SelectedAnnotationsActions.setSelectedAnnotationId({ annotationId: annotationSelector }));
    } else {
      this.store.dispatch(SelectedAnnotationsActions.removeAnnotationId({ annotationId: annotationSelector }));
    }
  }

  public onHiddenAnnotationSelectionChanged(annotationSelector: AnnotationSelector): void {
    this.store.dispatch(AnnotationsViewerActions.setAnnotationVisibility({ annotationSelector }));
  }
}
