import { ChangeDetectionStrategy, Component } from '@angular/core';
import { AnnotationSelector, AnnotationsViewerActions, HandDrawnAnnotationsActions, HandDrawnAnnotationsSelectors, SelectedAnnotationsActions, SelectedAnnotationsSelectors } from '@annotations/store';
import { AnnotationIdObject } from '@annotations/store/selected-annotations/selected-annotations.models';
import { PostProcessingJobsSelectors } from '@jobs/store';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { AnnotationEntity } from 'slide-viewer';

@Component({
  selector: 'app-hand-drawn-annotations-container',
  templateUrl: './hand-drawn-annotations-container.component.html',
  styleUrls: ['./hand-drawn-annotations-container.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class HandDrawnAnnotationsContainerComponent {
  public handDrawnAnnotationEntities$: Observable<AnnotationEntity[]>;
  public handDrawnAnnotationSelection$: Observable<Map<string, AnnotationIdObject>>;
  public allSelected$: Observable<boolean>;
  public indeterminateSelection$: Observable<boolean>;
  public loaded$: Observable<boolean>;
  // used to check if selected collection is ready for user input
  public selectedRegionsCollection$: Observable<string | undefined>;

  constructor(private store: Store) {
    this.handDrawnAnnotationEntities$ = this.store.select(HandDrawnAnnotationsSelectors.selectAllHandDrawnAnnotations);
    this.handDrawnAnnotationSelection$ = this.store.select(SelectedAnnotationsSelectors.selectFilteredAnnotationIdEntities);
    this.allSelected$ = this.store.select(HandDrawnAnnotationsSelectors.selectAllSelectedState);
    this.indeterminateSelection$ = this.store.select(HandDrawnAnnotationsSelectors.selectIndeterminateAnnotationSelection);
    this.loaded$ = this.store.select(SelectedAnnotationsSelectors.selectSelectedAnnotationsLoaded);
    this.selectedRegionsCollection$ = this.store.select(PostProcessingJobsSelectors.selectOutputCollectionIdOfCurrentSlideJob('selected_regions'));
  }

  public onZoomToAnnotation(focus: AnnotationEntity): void {
    this.store.dispatch(AnnotationsViewerActions.zoomToAnnotation({ focus }));
  }

  public onAllAnnotationSelectChanged(allSelected: boolean): void {
    this.store.dispatch(HandDrawnAnnotationsActions.selectAllAnnotations({ allSelected }));
  }

  public onAnnotationSelectionChanged(annotationSelector: AnnotationSelector): void {
    if (annotationSelector.checked) {
      this.store.dispatch(SelectedAnnotationsActions.setSelectedAnnotationId({ annotationId: annotationSelector }));
    } else {
      this.store.dispatch(SelectedAnnotationsActions.removeAnnotationId({ annotationId: annotationSelector }));
    }
  }

  public onDeleteAnnotation(annotationId: string): void {
    this.store.dispatch(HandDrawnAnnotationsActions.deleteAnnotation({ annotationId }));
  }
}
