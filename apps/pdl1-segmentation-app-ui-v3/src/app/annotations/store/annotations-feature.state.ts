import { Action, combineReducers, createFeatureSelector } from '@ngrx/store';
import * as fromAnnotationUi from '@annotations/store/annotations-ui/annotations-ui.reducer';
import * as fromAnnotationsViewer from '@annotations/store/annotations-viewer/annotations-viewer.reducer';
import * as fromPreprocessingAnnotations from '@annotations/store/preprocessing-annotations/preprocessing-annotations.reducer';
import * as fromHandDrawnAnnotations from '@annotations/store/hand-drawn-annotations/hand-drawn-annotations.reducer';
import * as fromSelectedAnnotations from '@annotations/store/selected-annotations/selected-annotations.reducer';
import * as fromAnnotationsColorMaps from '@annotations/store/annotations-color-maps/annotations-color-maps.reducer';

export const ANNOTATIONS_MODULE_FEATURE_KEY = 'annotationsModuleFeature';

export const selectAnnotationsFeatureState = createFeatureSelector<State>(
  ANNOTATIONS_MODULE_FEATURE_KEY
);

export interface State {
  [fromAnnotationUi.ANNOTATIONS_UI_FEATURE_KEY]: fromAnnotationUi.State;
  [fromAnnotationsViewer.ANNOTATIONS_VIEWER_FEATURE_KEY]: fromAnnotationsViewer.State;
  [fromPreprocessingAnnotations.PREPROCESSING_ANNOTATIONS_FEATURE_KEY]: fromPreprocessingAnnotations.State;
  [fromHandDrawnAnnotations.HAND_DRAWN_ANNOTATIONS_FEATURE_KEY]: fromHandDrawnAnnotations.State;
  [fromSelectedAnnotations.SELECTED_ANNOTATIONS_FEATURE_KEY]: fromSelectedAnnotations.State;
  [fromAnnotationsColorMaps.ANNOTATIONS_COLOR_MAPS_FEATURE_KEY]: fromAnnotationsColorMaps.State;
}

export function reducers(state: State | undefined, action: Action) {
  return combineReducers({
    [fromAnnotationUi.ANNOTATIONS_UI_FEATURE_KEY]: fromAnnotationUi.reducer,
    [fromAnnotationsViewer.ANNOTATIONS_VIEWER_FEATURE_KEY]: fromAnnotationsViewer.reducer,
    [fromPreprocessingAnnotations.PREPROCESSING_ANNOTATIONS_FEATURE_KEY]: fromPreprocessingAnnotations.reducer,
    [fromHandDrawnAnnotations.HAND_DRAWN_ANNOTATIONS_FEATURE_KEY]: fromHandDrawnAnnotations.reducer,
    [fromSelectedAnnotations.SELECTED_ANNOTATIONS_FEATURE_KEY]: fromSelectedAnnotations.reducer,
    [fromAnnotationsColorMaps.ANNOTATIONS_COLOR_MAPS_FEATURE_KEY]: fromAnnotationsColorMaps.reducer,
  })(state, action);
}
