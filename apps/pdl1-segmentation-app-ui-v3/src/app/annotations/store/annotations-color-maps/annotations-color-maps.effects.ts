import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { AnnotationsColorMapsService, EmpaiaAppDescriptionV3 } from 'empaia-ui-commons';
import * as ScopeActions from '@scope/store/scope/scope.actions';
import { AnnotationsColorMapsActions } from './annotations-color-maps.actions';
import { map } from 'rxjs/operators';
import { DISABLED_ANNOTATION_CLASS, NULL_ANNOTATION_CLASS } from '../annotations-viewer/annotations-viewer.models';



@Injectable()
export class AnnotationsColorMapsEffects {

  clearAnnotationRenderingHints$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ScopeActions.loadExtendedScopeSuccess),
      map(() => AnnotationsColorMapsActions.clearAnnotationColorMaps())
    );
  });

  setAnnotationRenderingHints$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ScopeActions.loadExtendedScopeSuccess),
      map(action => action.extendedScope.ead as EmpaiaAppDescriptionV3),
      map(ead =>
        this.acmService.getAllV3AnnotationRenderingHints(ead, [NULL_ANNOTATION_CLASS, DISABLED_ANNOTATION_CLASS])
      ),
      map(annotationRenderingHints => AnnotationsColorMapsActions.setAnnotationColorMaps({
        annotationRenderingHints
      }))
    );
  });

  // select first rendering as default after setting
  // the rendering hints
  selectFirstRenderingHint$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(AnnotationsColorMapsActions.setAnnotationColorMaps),
      map(action => action.annotationRenderingHints[0].id),
      map(selected => AnnotationsColorMapsActions.selectAnnotationColorMap({ selected }))
    );
  });

  constructor(
    private readonly actions$: Actions,
    private readonly acmService: AnnotationsColorMapsService,
  ) {}
}
