import { createActionGroup, props } from '@ngrx/store';
import { CurrentView as ApiCurrentView, ToolbarInteractionType } from 'slide-viewer';
import { CurrentView } from '@annotations/store/annotations-ui/annotations-ui.models';

export const AnnotationsUiActions = createActionGroup({
  source: 'AnnotationsUi',
  events: {
    'Set Viewport': props<{ currentView: ApiCurrentView }>(),
    'Set Viewport Ready': props<{ currentView: CurrentView }>(),
    'Set Interaction Type': props<{ interactionType: ToolbarInteractionType | undefined }>(),
    'Set Allowed Interaction Types': props<{ allowedDrawTypes: ToolbarInteractionType[] | undefined }>(),
  }
});
