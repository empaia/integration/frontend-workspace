import { createSelector } from '@ngrx/store';
import {
  selectAnnotationsFeatureState,
} from '../annotations-feature.state';
import { ANNOTATIONS_UI_FEATURE_KEY } from './annotations-ui.reducer';

export const selectAnnotationsUiState = createSelector(
  selectAnnotationsFeatureState,
  (state) => state[ANNOTATIONS_UI_FEATURE_KEY]
);

export const selectCurrentView = createSelector(
  selectAnnotationsUiState,
  (state) => state.currentView
);

export const selectInteractionType = createSelector(
  selectAnnotationsUiState,
  (state) => state.interactionType
);

export const selectAllowedInteractionTypes = createSelector(
  selectAnnotationsUiState,
  (state) => state.allowedDrawTypes
);
