import { Injectable } from '@angular/core';
import { ViewportConversionService } from '@annotations/services/viewport-conversion.service';
import { Actions, createEffect, ofType, concatLatestFrom } from '@ngrx/effects';
import { filter, map } from 'rxjs/operators';
import { AnnotationsUiActions } from './annotations-ui.actions';
import * as ScopeActions from '@scope/store/scope/scope.actions';
import * as ScopeSelectors from '@scope/store/scope/scope.selectors';
import { SlidesActions } from '@slides/store/slides/slides.actions';
import { AppV3ExaminationState } from 'empaia-api-lib';
import { DrawType, InteractionType } from 'slide-viewer';
import { Store } from '@ngrx/store';
import { filterNullish } from '@shared/helper/rxjs-operators';
import { EadActions } from '@ead/store/ead/ead.actions';
import { JobsActions, PostProcessingJobsActions, PostProcessingJobsSelectors } from '@jobs/store';
import { EadService, EmpaiaAppDescriptionV3 } from 'empaia-ui-commons';



@Injectable()
export class AnnotationsUiEffects {

  convertViewport$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(AnnotationsUiActions.setViewport),
      map(action => action.currentView),
      map(currentView => {
        return {
          viewport: this.viewportConverter.convertFromApi(currentView.extent),
          nppCurrent: currentView.nppCurrent,
          nppBase: currentView.nppBase,
        };
      }),
      map(currentView => AnnotationsUiActions.setViewportReady({ currentView }))
    );
  });

  // set allowed interaction types to move and polygon
  setAllowedInteractionTypes$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(SlidesActions.loadSlides),
      map(() => AnnotationsUiActions.setAllowedInteractionTypes({
        allowedDrawTypes: [InteractionType.Move, DrawType.PolygonAlternative]
      }))
    );
  });

  // set active interaction to move on examination close
  setInteractionToMoveOnClose$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ScopeActions.loadExtendedScopeSuccess),
      map(action => action.extendedScope.examination_state),
      filter(examinationState => examinationState === AppV3ExaminationState.Closed),
      map(() => AnnotationsUiActions.setInteractionType({ interactionType: InteractionType.Move }))
    );
  });

  // set interaction type move and compatible types when examination
  // is open
  setDrawTypesOnOpenExamination$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(EadActions.setActiveV3Mode),
      map(action => action.active),
      concatLatestFrom(() =>
        this.store.select(ScopeSelectors.selectExtendedScope).pipe(
          filterNullish(),
        )
      ),
      map(([mode, extendedScope]) => {
        if (extendedScope.examination_state === AppV3ExaminationState.Open) {
          const types = this.eadService.getV3AnnotationInputTypes(extendedScope.ead as EmpaiaAppDescriptionV3, mode);
          return AnnotationsUiActions.setAllowedInteractionTypes({
            allowedDrawTypes: [
              InteractionType.Move,
              // if polygon is an allowed input add alternative polygon as draw mode
              ...(types.includes('polygon') ? (types as DrawType[]).concat(DrawType.PolygonAlternative) : types as DrawType[])
            ]
          });
        } else {
          return AnnotationsUiActions.setAllowedInteractionTypes({
            allowedDrawTypes: [InteractionType.Move]
          });
        }
      })
    );
  });

  // deactivate annotation draw tools if app is not compatible
  disableDrawToolsOnIncompatibility$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(EadActions.checkEadInputCompatibilityFailure),
      map(() => AnnotationsUiActions.setAllowedInteractionTypes({
        allowedDrawTypes: [InteractionType.Move]
      }))
    );
  });

  // activate annotation draw tools when a post processing job is
  // available and has a selected_regions collection, if not only allow mode
  enableDrawToolsOnPostProcessingJob$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(
        PostProcessingJobsActions.createPostProcessingJobSuccess,
        PostProcessingJobsActions.setPostProcessingJob,
        PostProcessingJobsActions.setPostProcessingJobs,
        PostProcessingJobsActions.setJobInputOrOutputSuccess,
        SlidesActions.selectSlideReady,
        JobsActions.addJobToSlideJobs,
      ),
      concatLatestFrom(() =>
        this.store.select(PostProcessingJobsSelectors.selectOutputCollectionIdOfCurrentSlideJob('selected_regions')).pipe(
          filterNullish()
        )
      ),
      map(([, jobs]) => jobs),
      map(jobs => AnnotationsUiActions.setAllowedInteractionTypes({
        allowedDrawTypes: jobs && jobs.length ? [InteractionType.Move, DrawType.PolygonAlternative] : [InteractionType.Move]
      }))
    );
  });

  constructor(
    private readonly actions$: Actions,
    private readonly viewportConverter: ViewportConversionService,
    private readonly eadService: EadService,
    private readonly store: Store,
  ) {}
}
