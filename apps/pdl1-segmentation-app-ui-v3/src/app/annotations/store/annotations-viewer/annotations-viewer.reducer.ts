import { HttpErrorResponse } from '@angular/common/http';
import { EntityState, createEntityAdapter } from '@ngrx/entity';
import { createReducer, on } from '@ngrx/store';
import { AnnotationEntity } from 'slide-viewer';
import { AnnotationsViewerActions } from './annotations-viewer.actions';
import { AnnotationSelector } from '../annotations-ui/annotations-ui.models';


export const ANNOTATIONS_VIEWER_FEATURE_KEY = 'annotationsViewer';

export interface State extends EntityState<AnnotationEntity> {
  annotationIds: string[];
  centroids: number[][];
  remove: string[];
  hidden: EntityState<AnnotationSelector>;
  clear: boolean;
  loaded: boolean;
  focus?: AnnotationEntity | undefined;
  error?: HttpErrorResponse | undefined;
}

export const annotationsViewerAdapter = createEntityAdapter<AnnotationEntity>();
export const annotationsHiddenAdapter = createEntityAdapter<AnnotationSelector>();

export const initialState: State = annotationsViewerAdapter.getInitialState({
  annotationIds: [],
  centroids: [],
  remove: [],
  hidden: annotationsHiddenAdapter.getInitialState(),
  clear: false,
  loaded: true,
  focus: undefined,
  error: undefined,
});

export const reducer = createReducer(
  initialState,
  on(AnnotationsViewerActions.setAnnotationIds, (state, { annotationIds }): State => ({
    ...state,
    annotationIds,
  })),
  on(AnnotationsViewerActions.loadAnnotations, (state): State => ({
    ...state,
    loaded: false,
    clear: false,
  })),
  on(AnnotationsViewerActions.loadAnnotationsSuccess, (state, { annotations }): State =>
    annotationsViewerAdapter.upsertMany(annotations, {
      ...state,
      loaded: true,
      clear: false,
    })
  ),
  on(AnnotationsViewerActions.loadAnnotationsFailure, (state, { error }): State => ({
    ...state,
    loaded: true,
    clear: false,
    error,
  })),
  on(AnnotationsViewerActions.zoomToAnnotation, (state, { focus }): State => ({
    ...state,
    focus,
  })),
  on(AnnotationsViewerActions.addAnnotation, (state, { annotation }): State =>
    annotationsViewerAdapter.upsertOne(annotation, {
      ...state,
      clear: false,
      annotationIds: [...state.annotationIds, annotation.id]
    })
  ),
  on(AnnotationsViewerActions.removeAnnotations, (state, { annotationIds }): State =>
    annotationsViewerAdapter.removeMany(annotationIds, {
      ...state,
      clear: false,
      remove: annotationIds,
    })
  ),
  on(AnnotationsViewerActions.updateAnnotation, (state, { annotation }): State =>
    annotationsViewerAdapter.upsertOne(annotation, {
      ...state,
      clear: false,
    })
  ),
  on(AnnotationsViewerActions.updateAnnotations, (state, { annotations }): State =>
    annotationsViewerAdapter.upsertMany(annotations, {
      ...state,
      clear: false,
    })
  ),
  on(AnnotationsViewerActions.setAnnotationVisibility, (state, { annotationSelector }): State => ({
    ...state,
    hidden: annotationsHiddenAdapter.upsertOne(annotationSelector, {
      ...state.hidden
    })
  })),
  on(AnnotationsViewerActions.setAnnotationsVisibility, (state, { annotationSelectors }): State => ({
    ...state,
    hidden: annotationsHiddenAdapter.upsertMany(annotationSelectors, {
      ...state.hidden
    })
  })),
  on(AnnotationsViewerActions.clearAnnotations, (state): State =>
    annotationsViewerAdapter.removeAll({
      ...state,
      ...initialState,
      clear: true,
    })
  )
);
