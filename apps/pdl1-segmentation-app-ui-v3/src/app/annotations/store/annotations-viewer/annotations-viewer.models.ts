import {
  AppV3CircleAnnotation,
  AppV3RectangleAnnotation,
  AppV3PolygonAnnotation,
  AppV3PointAnnotation,
  AppV3LineAnnotation,
  AppV3ArrowAnnotation,
  AppV3PostCircleAnnotation,
  AppV3PostRectangleAnnotation,
  AppV3PostPolygonAnnotation,
  AppV3PostPointAnnotation,
  AppV3PostArrowAnnotation,
  AppV3PostLineAnnotation,
} from 'empaia-api-lib';
export {
  AppV3AnnotationType as AnnotationApiType,
  AppV3AnnotationReferenceType as AnnotationReferenceType
} from 'empaia-api-lib';

export type CircleAnnotation = AppV3CircleAnnotation;
export type RectangleAnnotation = AppV3RectangleAnnotation;
export type PolygonAnnotation = AppV3PolygonAnnotation;
export type PointAnnotation = AppV3PointAnnotation;
export type LineAnnotation = AppV3LineAnnotation;
export type ArrowAnnotation = AppV3ArrowAnnotation;
export type PostCircleAnnotation = AppV3PostCircleAnnotation;
export type PostRectangleAnnotation = AppV3PostRectangleAnnotation;
export type PostPolygonAnnotation = AppV3PostPolygonAnnotation;
export type PostPointAnnotation = AppV3PostPointAnnotation;
export type PostArrowAnnotation = AppV3PostArrowAnnotation;
export type PostLineAnnotation = AppV3PostLineAnnotation;

export const NPP_MIN_RANGE_FACTOR = 0.75;
export const NPP_MAX_RANGE_FACTOR = 1.5;

export const ANNOTATION_QUERY_LIMIT = 10000;

export const DISABLED_ANNOTATION_CLASS = 'org.empaia.v1.classes.disabled';
export const NULL_ANNOTATION_CLASS = 'null';
