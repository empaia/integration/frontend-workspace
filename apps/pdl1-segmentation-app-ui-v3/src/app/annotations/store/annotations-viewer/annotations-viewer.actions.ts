import { createActionGroup, emptyProps, props } from '@ngrx/store';
import { HttpErrorResponse } from '@angular/common/http';
import { AnnotationEntity } from 'slide-viewer';
import { AnnotationSelector } from '../annotations-ui/annotations-ui.models';

export const AnnotationsViewerActions = createActionGroup({
  source: 'AnnotationsViewer',
  events: {
    'Set Annotation Ids': props<{ annotationIds: string [] }>(),
    'Load Annotations': props<{ annotationIds: string[] }>(),
    'Load Annotations Ready': props<{
      withClasses: boolean,
      scopeId: string,
      annotationIds: string[],
      skip: number,
    }>(),
    'Load Annotations Success': props<{ annotations: AnnotationEntity[] }>(),
    'Load Annotations Failure': props<{ error: HttpErrorResponse }>(),
    'Zoom To Annotation': props<{ focus?: AnnotationEntity | undefined }>(),
    'Add Annotation': props<{ annotation: AnnotationEntity }>(),
    'Remove Annotations': props<{ annotationIds: string[] }>(),
    'Update Annotation': props<{ annotation: AnnotationEntity }>(),
    'Update Annotations': props<{ annotations: AnnotationEntity[] }>(),
    'Set Annotation Visibility': props<{ annotationSelector: AnnotationSelector }>(),
    'Set Annotations Visibility': props<{ annotationSelectors: AnnotationSelector[] }>(),
    'Clear Annotations': emptyProps(),
  }
});
