import { createSelector } from '@ngrx/store';
import { selectAnnotationsFeatureState } from '../annotations-feature.state';
import { annotationsViewerAdapter, annotationsHiddenAdapter, ANNOTATIONS_VIEWER_FEATURE_KEY } from './annotations-viewer.reducer';

const {
  selectAll,
  selectEntities,
} = annotationsViewerAdapter.getSelectors();

const hiddenSelectors = annotationsHiddenAdapter.getSelectors();

export const selectAnnotationsViewerState = createSelector(
  selectAnnotationsFeatureState,
  (state) => state[ANNOTATIONS_VIEWER_FEATURE_KEY]
);

export const selectAllAnnotationViewerIds = createSelector(
  selectAnnotationsViewerState,
  (state) => state.annotationIds
);

export const selectAnnotationsViewerCentroids = createSelector(
  selectAnnotationsViewerState,
  (state) => state.centroids
);

export const selectAllViewerAnnotations = createSelector(
  selectAnnotationsViewerState,
  selectAll,
);

export const selectAnnotationsViewerEntities = createSelector(
  selectAnnotationsViewerState,
  selectEntities,
);

export const selectAnnotationsViewerLoaded = createSelector(
  selectAnnotationsViewerState,
  (state) => state.loaded
);

export const selectFocusedViewerAnnotation = createSelector(
  selectAnnotationsViewerState,
  (state) => state.focus
);

export const selectFocusedViewerAnnotationId = createSelector(
  selectFocusedViewerAnnotation,
  (annotation) => annotation?.id
);

export const selectRemovedAnnotationIds = createSelector(
  selectAnnotationsViewerState,
  (state) => state.remove
);

export const selectHiddenAnnotationSelectors = createSelector(
  selectAnnotationsViewerState,
  (state) => hiddenSelectors.selectAll(state.hidden)
);

export const selectHiddenAnnotationEntities = createSelector(
  selectAnnotationsViewerState,
  (state) => hiddenSelectors.selectEntities(state.hidden)
);

export const selectHiddenAnnotationIds = createSelector(
  selectHiddenAnnotationSelectors,
  (hiddenAnnotationSelectors) => hiddenAnnotationSelectors.filter(a => a.checked).map(a => a.id)
);

export const selectAnnotationsClearState = createSelector(
  selectAnnotationsViewerState,
  (state) => state.clear
);

export const selectAnnotationsViewerError = createSelector(
  selectAnnotationsViewerState,
  (state) => state.error
);
