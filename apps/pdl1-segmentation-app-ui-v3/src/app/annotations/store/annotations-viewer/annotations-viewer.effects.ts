import { Injectable } from '@angular/core';
import { AppV3DataService } from 'empaia-api-lib';
import { Actions, concatLatestFrom, createEffect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { AnnotationsViewerActions } from './annotations-viewer.actions';
import { SlidesActions } from '@slides/store/slides/slides.actions';
import * as TokenActions from '@token/store/token/token.actions';
import { HandDrawnAnnotationsActions } from '../hand-drawn-annotations/hand-drawn-annotations.actions';
import { PreprocessingAnnotationsActions } from '../preprocessing-annotations/preprocessing-annotations.actions';
import * as AnnotationsViewerSelectors from './annotations-viewer.selectors';
import * as HandDrawnAnnotationsSelectors from '@annotations/store/hand-drawn-annotations/hand-drawn-annotations.selectors';
import * as PreprocessingAnnotationsSelectors from '@annotations/store/preprocessing-annotations/preprocessing-annotations.selectors';
import * as SelectedAnnotationsSelectors from '@annotations/store/selected-annotations/selected-annotations.selectors';
import * as ScopeSelectors from '@scope/store/scope/scope.selectors';
import { fetch } from '@ngrx/router-store/data-persistence';
import { State } from './annotations-viewer.reducer';
import { filter, map, retryWhen } from 'rxjs';
import { filterNullish, retryOnAction } from '@shared/helper/rxjs-operators';
import { requestNewToken } from 'vendor-app-communication-interface';
import {
  ANNOTATION_QUERY_LIMIT,
  DISABLED_ANNOTATION_CLASS,
  NULL_ANNOTATION_CLASS,
} from './annotations-viewer.models';
import { AnnotationConversionService } from '@annotations/services/annotation-conversion.service';
import { AnnotationEntity } from 'slide-viewer';
import { SelectedAnnotationsActions } from '../selected-annotations/selected-annotations.actions';

@Injectable()
export class AnnotationsViewerEffects {
  // clear viewer annotations
  clearAnnotations$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(SlidesActions.selectSlideReady, SlidesActions.clearSlides),
      map(() => AnnotationsViewerActions.clearAnnotations())
    );
  });

  // set annotation ids when preprocessed annotations or hand drawn
  // annotation where loaded
  setAnnotationIds$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(
        PreprocessingAnnotationsActions.loadPreprocessingAnnotationsSuccess,
        HandDrawnAnnotationsActions.loadAnnotationsSuccess,
        HandDrawnAnnotationsActions.deleteAnnotationSuccess,
        AnnotationsViewerActions.setAnnotationVisibility,
        AnnotationsViewerActions.setAnnotationsVisibility
      ),
      concatLatestFrom(() => [
        this.store.select(
          HandDrawnAnnotationsSelectors.selectAllHandDrawnAnnotationIds
        ),
        this.store.select(
          PreprocessingAnnotationsSelectors.selectAllPreprocessingAnnotationIds
        ),
        this.store.select(AnnotationsViewerSelectors.selectHiddenAnnotationIds),
      ]),
      map(([, handDrawnAnnotationIds, preprocessedAnnotationIds, hiddenIds]) =>
        preprocessedAnnotationIds
          .concat(handDrawnAnnotationIds)
          .filter((id) => !hiddenIds.includes(id))
      ),
      map((annotationIds) =>
        AnnotationsViewerActions.setAnnotationIds({ annotationIds })
      )
    );
  });

  prepareLoadAnnotation$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(AnnotationsViewerActions.loadAnnotations),
      map((action) => action.annotationIds),
      filter((annotationIds) => !!annotationIds.length),
      concatLatestFrom(() =>
        this.store.select(ScopeSelectors.selectScopeId).pipe(filterNullish())
      ),
      map(([annotationIds, scopeId]) =>
        AnnotationsViewerActions.loadAnnotationsReady({
          scopeId,
          annotationIds,
          withClasses: true,
          skip: 0,
        })
      )
    );
  });

  loadAnnotations$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(AnnotationsViewerActions.loadAnnotationsReady),
      fetch({
        id: (
          action: ReturnType<
            typeof AnnotationsViewerActions.loadAnnotationsReady
          >,
          _state: State
        ) => {
          return action.type;
        },
        run: (
          action: ReturnType<
            typeof AnnotationsViewerActions.loadAnnotationsReady
          >,
          _state: State
        ) => {
          return this.dataService
            .scopeIdAnnotationsQueryPut({
              scope_id: action.scopeId,
              with_classes: action.withClasses,
              skip: action.skip,
              limit: ANNOTATION_QUERY_LIMIT,
              body: {
                annotations: action.annotationIds,
              },
            })
            .pipe(
              retryWhen((errors) =>
                retryOnAction(
                  errors,
                  this.actions$,
                  TokenActions.setAccessToken,
                  requestNewToken
                )
              ),
              concatLatestFrom(() =>
                this.store.select(
                  SelectedAnnotationsSelectors.selectFilteredAnnotationIdEntities
                )
              ),
              map(([response, selectedIdEntities]) =>
                response.items.map((a) => {
                  const annotation = this.annotationConverter.fromApiType(
                    a
                  ) as AnnotationEntity;
                  annotation.classes = selectedIdEntities.get(annotation.id)
                    ? [{ value: NULL_ANNOTATION_CLASS }]
                    : [{ value: DISABLED_ANNOTATION_CLASS }];
                  return annotation;
                })
              ),
              map((annotations) =>
                AnnotationsViewerActions.loadAnnotationsSuccess({ annotations })
              )
            );
        },
        onError: (
          _action: ReturnType<
            typeof AnnotationsViewerActions.loadAnnotationsReady
          >,
          error
        ) => {
          return AnnotationsViewerActions.loadAnnotationsFailure({ error });
        },
      })
    );
  });

  // add hand drawn annotation after posting
  addHandDrawnAnnotationAfterCreation$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(HandDrawnAnnotationsActions.createAnnotationSuccess),
      map((action) => action.annotation),
      map((annotation) =>
        AnnotationsViewerActions.addAnnotation({ annotation })
      )
    );
  });

  // remove hand drawn annotation after user deletes it
  removeHandDrawnAnnotation$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(HandDrawnAnnotationsActions.deleteAnnotationSuccess),
      map((action) => [action.annotationId]),
      map((annotationIds) =>
        AnnotationsViewerActions.removeAnnotations({ annotationIds })
      )
    );
  });

  // update annotation when selected to empty array classes
  updateAnnotationOnSelect$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(SelectedAnnotationsActions.setSelectedAnnotationId),
      map((action) => action.annotationId.id),
      concatLatestFrom(() => [
        this.store.select(
          AnnotationsViewerSelectors.selectAnnotationsViewerEntities
        ),
      ]),
      map(([annotationId, annotationEntities]) => {
        const annotation = {
          ...annotationEntities[annotationId],
        } as AnnotationEntity;
        if (annotation) {
          annotation.classes = [{ value: NULL_ANNOTATION_CLASS }];
        }
        return annotation;
      }),
      filterNullish(),
      map((annotation) =>
        AnnotationsViewerActions.updateAnnotation({ annotation })
      )
    );
  });

  // update annotation when deselected to disabled class
  updateAnnotationOnDeselect$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(SelectedAnnotationsActions.removeAnnotationId),
      map((action) => action.annotationId.id),
      concatLatestFrom(() => [
        this.store.select(
          AnnotationsViewerSelectors.selectAnnotationsViewerEntities
        ),
      ]),
      map(([annotationId, annotationEntities]) => {
        const annotation = {
          ...annotationEntities[annotationId],
        } as AnnotationEntity;

        if (annotation) {
          annotation.classes = [{ value: DISABLED_ANNOTATION_CLASS }];
        }

        return annotation;
      }),
      filterNullish(),
      map((annotation) =>
        AnnotationsViewerActions.updateAnnotation({ annotation })
      )
    );
  });

  // update annotations when selected cache got cleared
  updateAnnotationsOnClearCache$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(SelectedAnnotationsActions.clearSelectedAnnotationIdsCache),
      concatLatestFrom(() => [
        this.store.select(
          AnnotationsViewerSelectors.selectAllViewerAnnotations
        ),
        this.store.select(
          SelectedAnnotationsSelectors.selectFilteredAnnotationIdEntities
        ),
      ]),
      map(([, annotations, selectedEntities]) =>
        annotations.map(
          (annotation) =>
            ({
              ...annotation,
              classes: selectedEntities.get(annotation.id)
                ? [{ value: NULL_ANNOTATION_CLASS }]
                : [{ value: DISABLED_ANNOTATION_CLASS }],
            } as AnnotationEntity)
        )
      ),
      filter((annotations) => !!annotations?.length),
      map((annotations) =>
        AnnotationsViewerActions.updateAnnotations({ annotations })
      )
    );
  });

  constructor(
    private readonly actions$: Actions,
    private readonly store: Store,
    private readonly dataService: AppV3DataService,
    private readonly annotationConverter: AnnotationConversionService
  ) {}
}
