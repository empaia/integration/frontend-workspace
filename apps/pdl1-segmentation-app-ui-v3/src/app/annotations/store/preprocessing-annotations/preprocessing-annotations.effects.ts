import { Injectable } from '@angular/core';
import { AnnotationConversionService } from '@annotations/services/annotation-conversion.service';
import { AppV3DataService } from 'empaia-api-lib';
import { Actions, concatLatestFrom, createEffect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { PreprocessingAnnotationsActions } from './preprocessing-annotations.actions';
import { PreprocessingJobsActions } from '@jobs/store';
import { SlidesActions } from '@slides/store/slides/slides.actions';
import * as PreprocessingJobsSelectors from '@jobs/store/preprocessing-jobs/preprocessing-jobs.selectors';
import * as PreprocessingAnnotationsSelectors from './preprocessing-annotations.selectors';
import * as ScopeSelectors from '@scope/store/scope/scope.selectors';
import { filter, map } from 'rxjs';
import { filterNullish } from '@shared/helper/rxjs-operators';
import { AnnotationEntity } from 'slide-viewer';
import { SelectedAnnotationsActions } from '../selected-annotations/selected-annotations.actions';
import { CollectionsActions } from '@collections/store/collections/collections.actions';
import { AnnotationApiOutput } from '@annotations/services/annotation-types';



@Injectable()
export class PreprocessingAnnotationsEffects {

  // clear annotations on slide selection
  clearAnnotationsOnSlideSelection$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(SlidesActions.selectSlideReady),
      map(() => PreprocessingAnnotationsActions.clearPreprocessingAnnotations())
    );
  });

  loadOnSlideSelection$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(
        SlidesActions.selectSlideReady,
        PreprocessingJobsActions.selectPreprocessingJobReady
      ),
      concatLatestFrom(() => [
        this.store.select(ScopeSelectors.selectScopeId).pipe(filterNullish()),
        this.store.select(PreprocessingJobsSelectors.selectOutputCollectionIdOfCurrentSlideJob('preprocessing_regions')).pipe(
          filterNullish()
        )
      ]),
      map(([, scopeId, collectionId]) => CollectionsActions.loadCollection({
        scopeId,
        collectionId,
      }))
    );
  });

  // load annotations when preprocessing job
  // was selected
  loadAnnotationsOnJobSelection$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(CollectionsActions.loadCollectionSuccess),
      map(action => action.collection.id),
      filterNullish(),
      concatLatestFrom(() => [
        this.store.select(PreprocessingJobsSelectors.selectOutputCollectionOfCurrentSlideJob('preprocessing_regions')).pipe(
          filterNullish()
        )
      ]),
      filter(([collectionId, preprocessingCollection]) => collectionId === preprocessingCollection.id),
      map(([, collection]) =>
        collection.items?.map(item =>
          this.annotationConversion.fromApiType(item as AnnotationApiOutput) as AnnotationEntity
        )
      ),
      filterNullish(),
      map(annotations => PreprocessingAnnotationsActions.loadPreprocessingAnnotationsSuccess({ annotations }))
    );
  });

  // update selected annotations on selected / deselect all
  selectAll$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(PreprocessingAnnotationsActions.selectAllAnnotations),
      map(action => action.allSelected),
      concatLatestFrom(() => [
        this.store.select(PreprocessingAnnotationsSelectors.selectAllPreprocessingAnnotations)
      ]),
      map(([allSelected, annotationIds]) => allSelected
        ? SelectedAnnotationsActions.setSelectedAnnotationIds({
          annotationIds
        })
        : SelectedAnnotationsActions.removeAnnotationIds({
          annotationIds
        })
      )
    );
  });

  constructor(
    private readonly actions$: Actions,
    private readonly store: Store,
    private readonly dataService: AppV3DataService,
    private readonly annotationConversion: AnnotationConversionService,
  ) {}
}
