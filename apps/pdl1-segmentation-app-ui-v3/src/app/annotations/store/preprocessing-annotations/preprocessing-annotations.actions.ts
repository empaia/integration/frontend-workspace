import { HttpErrorResponse } from '@angular/common/http';
import { createActionGroup, emptyProps, props } from '@ngrx/store';
import { AnnotationEntity } from 'slide-viewer';

export const PreprocessingAnnotationsActions = createActionGroup({
  source: 'Preprocessing Annotations Actions',
  events: {
    'Load Preprocessing Annotations Success': props<{ annotations: AnnotationEntity[] }>(),
    'Load Preprocessing Annotations Failure': props<{ error: HttpErrorResponse }>(),
    'Select All Annotations': props<{ allSelected: boolean }>(),
    'Highlight Preprocessing Annotation': props<{ highlighted?: string | undefined }>(),
    'Clear Preprocessing Annotations': emptyProps(),
  }
});
