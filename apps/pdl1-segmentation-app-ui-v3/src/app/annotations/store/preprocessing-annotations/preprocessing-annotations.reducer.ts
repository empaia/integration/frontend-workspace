import { HttpErrorResponse } from '@angular/common/http';
import { EntityState, createEntityAdapter } from '@ngrx/entity';
import { createReducer, on } from '@ngrx/store';
import { AnnotationEntity } from 'slide-viewer';
import { PreprocessingAnnotationsActions } from './preprocessing-annotations.actions';


export const PREPROCESSING_ANNOTATIONS_FEATURE_KEY = 'preprocessingAnnotations';

export interface State extends EntityState<AnnotationEntity> {
  loaded: boolean;
  highlighted?: string | undefined;
  error?: HttpErrorResponse | undefined;
}

export const preprocessingAnnotationsAdapter = createEntityAdapter<AnnotationEntity>();

export const initialState: State = preprocessingAnnotationsAdapter.getInitialState({
  loaded: true,
  highlighted: undefined,
  error: undefined,
});

export const reducer = createReducer(
  initialState,
  on(PreprocessingAnnotationsActions.loadPreprocessingAnnotationsSuccess, (state, { annotations }): State =>
    preprocessingAnnotationsAdapter.setAll(annotations, {
      ...state,
      loaded: true,
    })
  ),
  on(PreprocessingAnnotationsActions.loadPreprocessingAnnotationsFailure, (state, { error }): State => ({
    ...state,
    loaded: true,
    error,
  })),
  on(PreprocessingAnnotationsActions.highlightPreprocessingAnnotation, (state, { highlighted }): State => ({
    ...state,
    highlighted,
  })),
  on(PreprocessingAnnotationsActions.clearPreprocessingAnnotations, (state): State =>
    preprocessingAnnotationsAdapter.removeAll({
      ...state,
      ...initialState,
    })
  ),
);
