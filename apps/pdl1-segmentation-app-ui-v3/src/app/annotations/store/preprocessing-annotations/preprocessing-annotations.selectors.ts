import { createSelector } from '@ngrx/store';
import { selectAnnotationsFeatureState } from '../annotations-feature.state';
import {
  preprocessingAnnotationsAdapter,
  PREPROCESSING_ANNOTATIONS_FEATURE_KEY
} from './preprocessing-annotations.reducer';
import { selectFilteredAnnotationIdEntities, selectSelectedAnnotationsLoaded } from '../selected-annotations/selected-annotations.selectors';

const {
  selectAll,
  selectEntities,
  selectIds,
} = preprocessingAnnotationsAdapter.getSelectors();

export const selectPreprocessingAnnotationsState = createSelector(
  selectAnnotationsFeatureState,
  (state) => state[PREPROCESSING_ANNOTATIONS_FEATURE_KEY]
);

export const selectAllPreprocessingAnnotations = createSelector(
  selectPreprocessingAnnotationsState,
  selectAll,
);

export const selectPreprocessingAnnotationEntities = createSelector(
  selectPreprocessingAnnotationsState,
  selectEntities,
);

export const selectAllPreprocessingAnnotationIds = createSelector(
  selectPreprocessingAnnotationsState,
  (state) => selectIds(state) as string[]
);

export const selectHighlightedAnnotationId = createSelector(
  selectPreprocessingAnnotationsState,
  (state) => state.highlighted
);

export const selectHighlightedAnnotation = createSelector(
  selectPreprocessingAnnotationEntities,
  selectHighlightedAnnotationId,
  (entities, annotationId) => annotationId ? entities[annotationId] : undefined
);

export const selectAllSelectedState = createSelector(
  selectAllPreprocessingAnnotations,
  selectFilteredAnnotationIdEntities,
  (annotations, selectedEntities) => !!annotations?.length && annotations.every(annotation => !!selectedEntities.get(annotation.id))
);

export const selectIndeterminateAnnotationSelection = createSelector(
  selectAllSelectedState,
  selectAllPreprocessingAnnotations,
  selectFilteredAnnotationIdEntities,
  (allSelected, annotations, selectedEntities) => !allSelected && annotations.some(annotation => !!selectedEntities.get(annotation.id))
);

export const selectPreprocessingAnnotationsLoaded = createSelector(
  selectPreprocessingAnnotationsState,
  (state) => state.loaded
);

export const selectPreprocessingAnnotationsError = createSelector(
  selectPreprocessingAnnotationsState,
  (state) => state.error
);

export const selectPreprocessingAnnotationReadyToSelect = createSelector(
  selectPreprocessingAnnotationsLoaded,
  selectSelectedAnnotationsLoaded,
  (loaded, selectedLoaded) => loaded && selectedLoaded
);
