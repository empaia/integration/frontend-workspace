import { Injectable } from '@angular/core';
import { AppV3DataService } from 'empaia-api-lib';
import { Actions, concatLatestFrom, createEffect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { SelectedAnnotationsActions } from './selected-annotations.actions';
import { SlidesActions } from '@slides/store/slides/slides.actions';
import { CollectionsActions } from '@collections/store/collections/collections.actions';
import * as PostProcessingJobsSelectors from '@jobs/store/post-processing-jobs/post-processing-jobs.selectors';
import * as SelectedAnnotationsSelectors from './selected-annotations.selectors';
import * as HandDrawnAnnotationsSelectors from '@annotations/store/hand-drawn-annotations/hand-drawn-annotations.selectors';
import * as ScopeSelectors from '@scope/store/scope/scope.selectors';
import { filter, map, mergeMap } from 'rxjs';
import { filterNullish } from '@shared/helper/rxjs-operators';
import { PostProcessingJobsActions, PreprocessingJobsActions } from '@jobs/store';
import { AnnotationChange, AnnotationIdObject } from './selected-annotations.models';
import { IdObject } from '@collections/store';



@Injectable()
export class SelectedAnnotationsEffects {

  // clear annotations on slide selection
  clearAnnotationsOnSlideSelection$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(SlidesActions.selectSlideReady),
      map(() => SelectedAnnotationsActions.clearSelectedAnnotationIds())
    );
  });

  // load selected annotations on slide selection
  clearAndLoad$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(
        SlidesActions.selectSlideReady,
        PostProcessingJobsActions.setPostProcessingJobs
      ),
      concatLatestFrom(() => [
        this.store.select(ScopeSelectors.selectScopeId).pipe(filterNullish()),
        this.store.select(PostProcessingJobsSelectors.selectOutputCollectionIdOfCurrentSlideJob('selected_regions')).pipe(
          filterNullish()
        ),
      ]),
      map(([, scopeId, collectionId]) => CollectionsActions.loadCollection({
        scopeId,
        collectionId,
      }))
    );
  });

  loadSelectedAnnotations = createEffect(() => {
    return this.actions$.pipe(
      ofType(CollectionsActions.loadCollectionSuccess),
      map(action => action.collection.id),
      filterNullish(),
      concatLatestFrom(() => [
        this.store.select(PostProcessingJobsSelectors.selectOutputCollectionOfCurrentSlideJob('selected_regions')).pipe(filterNullish())
      ]),
      filter(([collectionId, selectedCollection]) => collectionId === selectedCollection.id),
      map(([, collection]) => collection.items),
      filterNullish(),
      map(items => items as IdObject[]),
      map(annotationIds => SelectedAnnotationsActions.loadSelectedAnnotationIdsSuccess({ annotationIds }))
    );
  });

  applyChanges$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(SelectedAnnotationsActions.applyChanges),
      concatLatestFrom(() => [
        this.store.select(SelectedAnnotationsSelectors.selectAllAnnotationIdCacheObjects)
      ]),
      map(([, annotationIds]) => annotationIds),
      filter(annotationIds => !!annotationIds?.length),
      mergeMap(annotationIds => {
        const addAnnotations: AnnotationIdObject[] = [];
        const removeAnnotations: AnnotationIdObject[] = [];

        annotationIds.forEach(annotationId => {
          switch (annotationId.change) {
            case AnnotationChange.ADD:
              addAnnotations.push(annotationId);
              break;
            case AnnotationChange.REMOVE:
              removeAnnotations.push(annotationId);
              break;
          }
        });

        return [
          addAnnotations.length
            ? SelectedAnnotationsActions.addAnnotationIds({ annotationIds: addAnnotations })
            : { type: 'noop' },
          removeAnnotations.length
            ? SelectedAnnotationsActions.deleteAnnotationIds({ annotationIds: removeAnnotations })
            : { type: 'noop' },
          addAnnotations.length || removeAnnotations.length
            ? SelectedAnnotationsActions.applyChangesReady()
            : { type: 'noop' }
        ];
      })
    );
  });

  addAnnotationIdToCollection$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(SelectedAnnotationsActions.addAnnotationId),
      map(action => ({ id: action.annotationId.id } as IdObject)),
      concatLatestFrom(() => [
        this.store.select(ScopeSelectors.selectScopeId).pipe(filterNullish()),
        this.store.select(PostProcessingJobsSelectors.selectOutputCollectionIdOfCurrentSlideJob('selected_regions')).pipe(
          filterNullish()
        )
      ]),
      map(([postIdObject, scopeId, collectionId]) => CollectionsActions.addItemToCollection({
        scopeId,
        collectionId,
        postIdObject,
      }))
    );
  });

  addAnnotationIdsToCollection$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(SelectedAnnotationsActions.addAnnotationIds),
      map(action =>
        action.annotationIds.map(annotationId => ({ id: annotationId.id } as IdObject))
      ),
      concatLatestFrom(() => [
        this.store.select(ScopeSelectors.selectScopeId).pipe(filterNullish()),
        this.store.select(PostProcessingJobsSelectors.selectOutputCollectionIdOfCurrentSlideJob('selected_regions')).pipe(
          filterNullish()
        )
      ]),
      map(([annotationIds, scopeId, collectionId]) => CollectionsActions.addItemsToCollection({
        scopeId,
        collectionId,
        postIdObjects: {
          items: annotationIds
        }
      }))
    );
  });

  removeAnnotationIdFromCollection$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(SelectedAnnotationsActions.deleteAnnotationId),
      map(action => action.annotationId.id),
      concatLatestFrom(() => [
        this.store.select(ScopeSelectors.selectScopeId).pipe(filterNullish()),
        this.store.select(PostProcessingJobsSelectors.selectOutputCollectionIdOfCurrentSlideJob('selected_regions')).pipe(
          filterNullish()
        )
      ]),
      map(([itemId, scopeId, collectionId]) => CollectionsActions.removeItemFromCollection({
        scopeId,
        collectionId,
        itemId,
      }))
    );
  });

  removeAnnotationIdsFromCollection$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(SelectedAnnotationsActions.deleteAnnotationIds),
      map(action => action.annotationIds.map(a => a.id)),
      concatLatestFrom(() => [
        this.store.select(ScopeSelectors.selectScopeId).pipe(filterNullish()),
        this.store.select(PostProcessingJobsSelectors.selectOutputCollectionIdOfCurrentSlideJob('selected_regions')).pipe(
          filterNullish()
        )
      ]),
      map(([itemIds, scopeId, collectionId]) => CollectionsActions.removeItemsFromCollection({
        scopeId,
        collectionId,
        itemIds,
      }))
    );
  });

  // update selected regions collection by adding item
  // on user interaction
  addAnnotationIdToCache$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(SelectedAnnotationsActions.setSelectedAnnotationId),
      map(action => action.annotationId),
      filter(annotationId => !!annotationId.id),
      concatLatestFrom(() => [
        this.store.select(SelectedAnnotationsSelectors.selectAnnotationIdEntities)
      ]),
      map(([annotationId, stateEntities]) =>
        stateEntities[annotationId.id]
          ? SelectedAnnotationsActions.removeAnnotationIdFromCache({
            annotationId: annotationId.id
          })
          : SelectedAnnotationsActions.changeAnnotationId({
            annotationId: { ...annotationId, change: AnnotationChange.ADD }
          })
      )
    );
  });

  addAnnotationIdsToCache$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(SelectedAnnotationsActions.setSelectedAnnotationIds),
      map(action => action.annotationIds),
      mergeMap(annotationIds =>
        annotationIds
          .filter(annotationId => !!annotationId.id)
          .map(annotationId => SelectedAnnotationsActions.setSelectedAnnotationId({
            annotationId
          }))
      )
    );
  });

  addRemoveAnnotationIdToCache$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(SelectedAnnotationsActions.removeAnnotationId),
      map(action => action.annotationId),
      filter(annotationId => !!annotationId.id),
      concatLatestFrom(() => [
        this.store.select(SelectedAnnotationsSelectors.selectAnnotationIdEntities)
      ]),
      map(([annotationId, stateEntities]) =>
        !stateEntities[annotationId.id]
          ? SelectedAnnotationsActions.removeAnnotationIdFromCache({
            annotationId: annotationId.id
          })
          : SelectedAnnotationsActions.changeAnnotationId({
            annotationId: { ...annotationId, change: AnnotationChange.REMOVE }
          })
      )
    );
  });

  addRemoveAnnotationIdsToCache$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(SelectedAnnotationsActions.removeAnnotationIds),
      map(action => action.annotationIds),
      mergeMap(annotationIds =>
        annotationIds
          .filter(annotationId => !!annotationId.id)
          .map(annotationId => SelectedAnnotationsActions.removeAnnotationId({
            annotationId
          }))
      )
    );
  });

  // viewer annotation was clicked, check if annotation needs to be selected
  // or deselected
  // only select or deselect annotations when selected_regions collections is
  // available
  clickAnnotationId$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(SelectedAnnotationsActions.clickAnnotationId),
      map(action => action.annotationId),
      filterNullish(),
      concatLatestFrom(() => [
        this.store.select(SelectedAnnotationsSelectors.selectFilteredAnnotationIdEntities),
      ]),
      map(([annotationId, selectedAnnotationEntities]) =>
        selectedAnnotationEntities.has(annotationId)
          ? SelectedAnnotationsActions.removeAnnotationId({ annotationId: { id: annotationId } })
          : SelectedAnnotationsActions.setSelectedAnnotationId({ annotationId: { id: annotationId }})
      ),
    );
  });

  // remove annotations from selected_regions collection when the preprocessing job of the same slide was switched
  removeAnnotationsOnJobSwitch$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(PreprocessingJobsActions.selectPreprocessingJobReady),
      map(action => action.jobId),
      filterNullish(),
      concatLatestFrom(() => [
        this.store.select(ScopeSelectors.selectScopeId).pipe(filterNullish()),
        this.store.select(SelectedAnnotationsSelectors.selectAllAnnotationIds),
        this.store.select(HandDrawnAnnotationsSelectors.selectAllHandDrawnAnnotationIds),
        this.store.select(PostProcessingJobsSelectors.selectOutputCollectionIdOfCurrentSlideJob('selected_regions')).pipe(filterNullish())
      ]),
      map(([, scopeId, selectAnnotationIds, handDrawnAnnotationIds, collectionId]) => ({
        scopeId,
        collectionId,
        removeAnnotationIds: selectAnnotationIds.filter(id => !handDrawnAnnotationIds.includes(id))
      })),
      mergeMap(({scopeId, collectionId, removeAnnotationIds}) => [
        // clear selected annotation cache if user switches preprocessing job
        SelectedAnnotationsActions.clearSelectedAnnotationIdsCache(),
        CollectionsActions.removeItemsFromCollection({
          scopeId,
          collectionId,
          itemIds: removeAnnotationIds
        })
      ])
    );
  });

  checkIfChangesArePersisted$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(SelectedAnnotationsActions.loadSelectedAnnotationIdsSuccess),
      concatLatestFrom(() => [
        this.store.select(SelectedAnnotationsSelectors.selectChangesPersisted),
        this.store.select(SelectedAnnotationsSelectors.selectAllAnnotationIdCacheObjects),
        this.store.select(SelectedAnnotationsSelectors.selectAnnotationIdEntities),
      ]),
      // only check if persistence is set to false,
      // after clicking apply changes
      filter(([, persisted]) => !persisted),
      map(([, _persisted, cacheAnnotationIds, stateEntities]) =>
        cacheAnnotationIds.every(annotationId => {
          switch(annotationId.change) {
            case AnnotationChange.ADD:
              return !!stateEntities[annotationId.id];
            case AnnotationChange.REMOVE:
              return !stateEntities[annotationId.id];
          }
        })
      ),
      // clear selected annotation cache when all changes are persisted to
      // the backend
      filter(correct => correct),
      map(() => SelectedAnnotationsActions.clearSelectedAnnotationIdsCache())
    );
  });

  constructor(
    private readonly actions$: Actions,
    private readonly store: Store,
    private readonly dataService: AppV3DataService,
  ) {}
}
