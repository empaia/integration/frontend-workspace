import { createSelector } from '@ngrx/store';
import { selectAnnotationsFeatureState } from '../annotations-feature.state';
import { annotationIdAdapter, annotationIdCacheAdapter, SELECTED_ANNOTATIONS_FEATURE_KEY } from './selected-annotations.reducer';
import { AnnotationChange, AnnotationIdObject } from './selected-annotations.models';

const {
  selectAll,
  selectEntities,
  selectIds,
} = annotationIdAdapter.getSelectors();

const annotationCache = annotationIdCacheAdapter.getSelectors();

export const selectSelectedAnnotationsState = createSelector(
  selectAnnotationsFeatureState,
  (state) => state[SELECTED_ANNOTATIONS_FEATURE_KEY]
);

export const selectAllAnnotationIdObjects = createSelector(
  selectSelectedAnnotationsState,
  selectAll,
);

export const selectAnnotationIdEntities = createSelector(
  selectSelectedAnnotationsState,
  selectEntities,
);

export const selectAllAnnotationIds = createSelector(
  selectSelectedAnnotationsState,
  (state) => selectIds(state) as string[]
);

export const selectSelectedAnnotationsLoaded = createSelector(
  selectSelectedAnnotationsState,
  (state) => state.loaded
);

export const selectAllAnnotationIdCacheObjects = createSelector(
  selectSelectedAnnotationsState,
  (state) => annotationCache.selectAll(state.annotationCache)
);

export const selectAnnotationIdCacheEntities = createSelector(
  selectSelectedAnnotationsState,
  (state) => annotationCache.selectEntities(state.annotationCache)
);

export const selectAllAnnotationCacheIds = createSelector(
  selectSelectedAnnotationsState,
  (state) => annotationCache.selectIds(state.annotationCache) as string[]
);

export const selectAnnotationCacheIdsSize = createSelector(
  selectAllAnnotationCacheIds,
  (annotationIds) => annotationIds.length
);

export const selectChangesPersisted = createSelector(
  selectSelectedAnnotationsState,
  (state) => state.changesApplied
);

export const selectAllFilteredAnnotationIdObjects = createSelector(
  selectAllAnnotationIdObjects,
  selectAllAnnotationIdCacheObjects,
  selectAnnotationIdCacheEntities,
  selectChangesPersisted,
  (stateIds, cacheIds, cacheState, persisted) =>
    persisted
      ? stateIds
      : stateIds.concat(cacheIds).filter(annotationId => {
        const entity = cacheState[annotationId.id];

        if (entity && entity.change === AnnotationChange.REMOVE) {
          return false;
        }

        return true;
      })
);

export const selectFilteredAnnotationIdEntities = createSelector(
  selectAllAnnotationIdObjects,
  selectAllAnnotationIdCacheObjects,
  selectAnnotationIdCacheEntities,
  (stateIds, cacheIds, cacheState) => {
    const annotationIds = stateIds.concat(cacheIds);
    const annotationIdMap = new Map<string, AnnotationIdObject>();

    for (const annotationId of annotationIds) {
      const entity = cacheState[annotationId.id];

      if (entity && entity.change === AnnotationChange.REMOVE) {
        continue;
      }

      annotationIdMap.set(annotationId.id, annotationId);
    }

    return annotationIdMap;
  }
);

export const selectAllFilteredAnnotationIds = createSelector(
  selectAllFilteredAnnotationIdObjects,
  (annotationIdObjects) => annotationIdObjects.map(annotationIdObject => annotationIdObject.id)
);
