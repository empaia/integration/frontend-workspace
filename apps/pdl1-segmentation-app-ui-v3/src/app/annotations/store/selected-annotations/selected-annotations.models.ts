export interface AnnotationIdObject {
  id: string;
}

export enum AnnotationChange {
  ADD = 'ADD',
  REMOVE = 'REMOVE',
}

export interface AnnotationCacheObject extends AnnotationIdObject {
  change: AnnotationChange;
}
