import { createActionGroup, emptyProps, props } from '@ngrx/store';
import { AnnotationCacheObject, AnnotationIdObject } from './selected-annotations.models';
import { HttpErrorResponse } from '@angular/common/http';

export const SelectedAnnotationsActions = createActionGroup({
  source: 'Selected Annotations',
  events: {
    'Load Selected Annotation Ids Success': props<{ annotationIds: AnnotationIdObject[] }>(),
    'Load Selected Annotation Ids Failure': props<{ error: HttpErrorResponse }>(),
    'Set Selected Annotation Id': props<{ annotationId: AnnotationIdObject }>(),
    'Set Selected Annotation Ids': props<{ annotationIds: AnnotationIdObject[] }>(),
    'Remove Annotation Id': props<{ annotationId: AnnotationIdObject }>(),
    'Remove Annotation Ids': props<{ annotationIds: AnnotationIdObject[] }>(),
    'Change Annotation Id': props<{ annotationId: AnnotationCacheObject }>(),
    'Change Annotation Ids': props<{ annotationIds: AnnotationCacheObject[] }>(),
    'Remove Annotation Id From Cache': props<{ annotationId: string }>(),
    'Remove Annotation Ids From Cache': props<{ annotationIds: string[] }>(),
    'Click Annotation Id': props<{ annotationId: string }>(),
    'Click Annotation Ids': props<{ annotationIds: string[] }>(),
    'Apply Changes': emptyProps(),
    'Apply Changes Ready': emptyProps(),
    'Add Annotation Id': props<{ annotationId: AnnotationIdObject }>(),
    'Add Annotation Ids': props<{ annotationIds: AnnotationIdObject[] }>(),
    'Delete Annotation Id': props<{ annotationId: AnnotationIdObject }>(),
    'Delete Annotation Ids': props<{ annotationIds: AnnotationIdObject[] }>(),
    'Clear Selected Annotation Ids': emptyProps(),
    'Clear Selected Annotation Ids Cache': emptyProps()
  }
});
