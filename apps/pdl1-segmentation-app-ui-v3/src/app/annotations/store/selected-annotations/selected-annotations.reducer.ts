import { EntityState, createEntityAdapter } from '@ngrx/entity';
import { createReducer, on } from '@ngrx/store';
import { HttpErrorResponse } from '@angular/common/http';
import { SelectedAnnotationsActions } from './selected-annotations.actions';
import { AnnotationCacheObject, AnnotationIdObject } from './selected-annotations.models';


export const SELECTED_ANNOTATIONS_FEATURE_KEY = 'selectedAnnotations';

export interface State extends EntityState<AnnotationIdObject> {
  annotationCache: EntityState<AnnotationCacheObject>;
  loaded: boolean;
  changesApplied: boolean;
  error?: HttpErrorResponse | undefined;
}

export const annotationIdAdapter = createEntityAdapter<AnnotationIdObject>();
export const annotationIdCacheAdapter = createEntityAdapter<AnnotationCacheObject>();

export const initialState: State = annotationIdAdapter.getInitialState({
  annotationCache: annotationIdCacheAdapter.getInitialState(),
  loaded: true,
  changesApplied: true,
  error: undefined,
});

export const reducer = createReducer(
  initialState,
  on(SelectedAnnotationsActions.loadSelectedAnnotationIdsSuccess, (state, { annotationIds }): State =>
    annotationIdAdapter.setAll(annotationIds, {
      ...state,
      loaded: true,
    })
  ),
  on(SelectedAnnotationsActions.loadSelectedAnnotationIdsFailure, (state, { error }): State => ({
    ...state,
    loaded: true,
    error,
  })),
  on(SelectedAnnotationsActions.changeAnnotationId, (state, { annotationId }): State => ({
    ...state,
    annotationCache: annotationIdCacheAdapter.upsertOne(annotationId, {
      ...state.annotationCache
    })
  })),
  on(SelectedAnnotationsActions.changeAnnotationIds, (state, { annotationIds }): State => ({
    ...state,
    annotationCache: annotationIdCacheAdapter.upsertMany(annotationIds, {
      ...state.annotationCache
    })
  })),
  on(SelectedAnnotationsActions.removeAnnotationIdFromCache, (state, { annotationId }): State => ({
    ...state,
    annotationCache: annotationIdCacheAdapter.removeOne(annotationId, {
      ...state.annotationCache
    })
  })),
  on(SelectedAnnotationsActions.removeAnnotationIdsFromCache, (state, { annotationIds }): State => ({
    ...state,
    annotationCache: annotationIdCacheAdapter.removeMany(annotationIds, {
      ...state.annotationCache
    })
  })),
  on(SelectedAnnotationsActions.applyChangesReady, (state): State => ({
    ...state,
    changesApplied: false,
  })),
  on(SelectedAnnotationsActions.clearSelectedAnnotationIds, (state): State =>
    annotationIdAdapter.removeAll({
      ...state,
      ...initialState,
    })
  ),
  on(SelectedAnnotationsActions.clearSelectedAnnotationIdsCache, (state): State => ({
    ...state,
    annotationCache: annotationIdCacheAdapter.removeAll({
      ...state.annotationCache
    }),
    changesApplied: true,
  }))
);
