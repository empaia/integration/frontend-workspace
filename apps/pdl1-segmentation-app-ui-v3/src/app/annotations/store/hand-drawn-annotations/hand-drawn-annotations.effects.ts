import { Injectable } from '@angular/core';
import { AnnotationConversionService } from '@annotations/services/annotation-conversion.service';
import { AppV3DataService } from 'empaia-api-lib';
import { Actions, concatLatestFrom, createEffect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { HandDrawnAnnotationsActions } from './hand-drawn-annotations.actions';
import * as ScopeActions from '@scope/store/scope/scope.actions';
import { SlidesActions } from '@slides/store/slides/slides.actions';
import * as TokenActions from '@token/store/token/token.actions';
import * as HandDrawnAnnotationsSelectors from './hand-drawn-annotations.selectors';
import * as PostProcessingJobsSelectors from '@jobs/store/post-processing-jobs/post-processing-jobs.selectors';
import * as ScopeSelectors from '@scope/store/scope/scope.selectors';
import * as SlidesSelectors from '@slides/store/slides/slides.selectors';
import { pessimisticUpdate } from '@ngrx/router-store/data-persistence';
import { State } from './hand-drawn-annotations.reducer';
import { catchError, filter, map, mergeMap, retryWhen } from 'rxjs';
import { AnnotationEntity } from 'slide-viewer';
import {
  dispatchActionOnErrorCode,
  filterNullish,
  retryOnAction,
} from '@shared/helper/rxjs-operators';
import { requestNewToken } from 'vendor-app-communication-interface';
import { AnnotationApiOutput } from '@annotations/services/annotation-types';
import { EXAMINATION_CLOSED_ERROR_STATUS_CODE } from '@menu/models/ui.models';
import { SelectedAnnotationsActions } from '../selected-annotations/selected-annotations.actions';
import { CollectionsActions } from '@collections/store/collections/collections.actions';
import { PostProcessingJobsActions } from '@jobs/store/post-processing-jobs/post-processing-jobs.actions';
import { IdObject } from '@collections/store';

@Injectable()
export class HandDrawnAnnotationsEffects {
  clearAnnotations$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(SlidesActions.selectSlideReady, SlidesActions.clearSlides),
      map(() => HandDrawnAnnotationsActions.clearHandDrawnAnnotations())
    );
  });

  loadOnSlideSelection$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(
        SlidesActions.selectSlideReady,
        PostProcessingJobsActions.setPostProcessingJobs
      ),
      concatLatestFrom(() => [
        this.store.select(ScopeSelectors.selectScopeId).pipe(filterNullish()),
        this.store
          .select(
            PostProcessingJobsSelectors.selectInputCollectionIdOfCurrentSlideJob(
              'handdrawn_regions'
            )
          )
          .pipe(filterNullish()),
      ]),
      map(([, scopeId, collectionId]) =>
        CollectionsActions.loadCollection({
          scopeId,
          collectionId,
        })
      )
    );
  });

  loadAnnotations$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(CollectionsActions.loadCollectionSuccess),
      map((action) => action.collection.id),
      filterNullish(),
      concatLatestFrom(() => [
        this.store
          .select(
            PostProcessingJobsSelectors.selectInputCollectionOfCurrentSlideJob(
              'handdrawn_regions'
            )
          )
          .pipe(filterNullish()),
      ]),
      filter(
        ([collectionId, handDrawnCollection]) =>
          collectionId === handDrawnCollection.id
      ),
      map(([, collection]) =>
        collection.items?.map(
          (item) =>
            this.annotationConverter.fromApiType(
              item as AnnotationApiOutput
            ) as AnnotationEntity
        )
      ),
      filterNullish(),
      map((annotations) =>
        HandDrawnAnnotationsActions.loadAnnotationsSuccess({ annotations })
      )
    );
  });

  prepareCreateAnnotation$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(HandDrawnAnnotationsActions.createAnnotation),
      map((action) => action.annotation),
      concatLatestFrom(() => [
        this.store.select(ScopeSelectors.selectScopeId).pipe(filterNullish()),
        this.store
          .select(SlidesSelectors.selectSelectedSlideId)
          .pipe(filterNullish()),
      ]),
      map(([annotation, scopeId, slideId]) =>
        HandDrawnAnnotationsActions.createAnnotationReady({
          annotation,
          scopeId,
          slideId,
        })
      )
    );
  });

  createAnnotation$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(HandDrawnAnnotationsActions.createAnnotationReady),
      pessimisticUpdate({
        run: (
          action: ReturnType<
            typeof HandDrawnAnnotationsActions.createAnnotationReady
          >,
          _state: State
        ) => {
          return this.dataService
            .scopeIdAnnotationsPost({
              scope_id: action.scopeId,
              body: this.annotationConverter.toApiPostType(
                action.annotation,
                action.slideId,
                action.scopeId
              ),
            })
            .pipe(
              retryWhen((errors) =>
                retryOnAction(
                  errors,
                  this.actions$,
                  TokenActions.setAccessToken,
                  requestNewToken
                )
              ),
              map(
                (response) =>
                  this.annotationConverter.fromApiType(
                    response as AnnotationApiOutput
                  ) as AnnotationEntity
              ),
              map((annotation) =>
                HandDrawnAnnotationsActions.createAnnotationSuccess({
                  annotation,
                })
              ),
              catchError((error) =>
                dispatchActionOnErrorCode(
                  error,
                  EXAMINATION_CLOSED_ERROR_STATUS_CODE,
                  ScopeActions.loadExtendedScope({ scopeId: action.scopeId })
                )
              )
            );
        },
        onError: (
          _action: ReturnType<
            typeof HandDrawnAnnotationsActions.createAnnotationReady
          >,
          error
        ) => {
          return HandDrawnAnnotationsActions.createAnnotationFailure({ error });
        },
      })
    );
  });

  prepareDeleteAnnotation$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(HandDrawnAnnotationsActions.deleteAnnotation),
      map((action) => action.annotationId),
      concatLatestFrom(() =>
        this.store.select(ScopeSelectors.selectScopeId).pipe(filterNullish())
      ),
      map(([annotationId, scopeId]) =>
        HandDrawnAnnotationsActions.deleteAnnotationReady({
          annotationId,
          scopeId,
        })
      )
    );
  });

  deleteAnnotation$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(HandDrawnAnnotationsActions.deleteAnnotationReady),
      pessimisticUpdate({
        run: (
          action: ReturnType<
            typeof HandDrawnAnnotationsActions.deleteAnnotationReady
          >,
          _state: State
        ) => {
          return this.dataService
            .scopeIdAnnotationsAnnotationIdDelete({
              scope_id: action.scopeId,
              annotation_id: action.annotationId,
            })
            .pipe(
              map((response) => response.id),
              map((annotationId) =>
                HandDrawnAnnotationsActions.deleteAnnotationSuccess({
                  annotationId,
                })
              )
            );
        },
        onError: (
          _action: ReturnType<
            typeof HandDrawnAnnotationsActions.deleteAnnotationReady
          >,
          error
        ) => {
          return HandDrawnAnnotationsActions.deleteAnnotationFailure({ error });
        },
      })
    );
  });

  selectAll$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(HandDrawnAnnotationsActions.selectAllAnnotations),
      map((action) => action.allSelected),
      concatLatestFrom(() => [
        this.store.select(
          HandDrawnAnnotationsSelectors.selectAllHandDrawnAnnotations
        ),
      ]),
      map(([allSelected, annotationIds]) =>
        allSelected
          ? SelectedAnnotationsActions.setSelectedAnnotationIds({
            annotationIds,
          })
          : SelectedAnnotationsActions.removeAnnotationIds({
            annotationIds,
          })
      )
    );
  });

  // add annotation to handdrawn_regions and selected_regions collections after creation
  addNewAnnotationToCollections$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(HandDrawnAnnotationsActions.createAnnotationSuccess),
      map((action) => ({ id: action.annotation.id } as IdObject)),
      concatLatestFrom(() => [
        this.store.select(ScopeSelectors.selectScopeId).pipe(filterNullish()),
        this.store
          .select(
            PostProcessingJobsSelectors.selectInputCollectionIdOfCurrentSlideJob(
              'handdrawn_regions'
            )
          )
          .pipe(filterNullish()),
      ]),
      mergeMap(([postIdObject, scopeId, handDrawnCollectionId]) => [
        // add new annotation to handdrawn_regions collection
        CollectionsActions.addItemToCollection({
          scopeId,
          collectionId: handDrawnCollectionId,
          postIdObject,
        }),
        SelectedAnnotationsActions.setSelectedAnnotationId({
          annotationId: postIdObject,
        }),
      ])
    );
  });

  // reload hand drawn and selected collection after annotation was deleted
  reloadCollectionsAfterDeletion$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(HandDrawnAnnotationsActions.deleteAnnotationSuccess),
      concatLatestFrom(() => [
        this.store.select(ScopeSelectors.selectScopeId).pipe(filterNullish()),
        this.store
          .select(
            PostProcessingJobsSelectors.selectInputCollectionIdOfCurrentSlideJob(
              'handdrawn_regions'
            )
          )
          .pipe(filterNullish()),
        this.store
          .select(
            PostProcessingJobsSelectors.selectOutputCollectionIdOfCurrentSlideJob(
              'selected_regions'
            )
          )
          .pipe(filterNullish()),
      ]),
      mergeMap(([, scopeId, handDrawnCollectionId, selectedCollectionId]) => [
        CollectionsActions.loadCollection({
          scopeId,
          collectionId: handDrawnCollectionId,
        }),
        CollectionsActions.loadCollection({
          scopeId,
          collectionId: selectedCollectionId,
        }),
      ])
    );
  });

  constructor(
    private readonly actions$: Actions,
    private readonly store: Store,
    private readonly dataService: AppV3DataService,
    private readonly annotationConverter: AnnotationConversionService
  ) {}
}
