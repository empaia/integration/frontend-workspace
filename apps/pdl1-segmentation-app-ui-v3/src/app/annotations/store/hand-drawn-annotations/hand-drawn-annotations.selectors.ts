import { createSelector } from '@ngrx/store';
import { selectAnnotationsFeatureState } from '../annotations-feature.state';
import {
  handDrawnAnnotationsAdapter,
  HAND_DRAWN_ANNOTATIONS_FEATURE_KEY
} from './hand-drawn-annotations.reducer';
import { selectFilteredAnnotationIdEntities } from '../selected-annotations/selected-annotations.selectors';

const {
  selectAll,
  selectEntities,
  selectIds,
} = handDrawnAnnotationsAdapter.getSelectors();

export const selectHandDrawnAnnotationsState = createSelector(
  selectAnnotationsFeatureState,
  (state) => state[HAND_DRAWN_ANNOTATIONS_FEATURE_KEY]
);

export const selectAllHandDrawnAnnotations = createSelector(
  selectHandDrawnAnnotationsState,
  selectAll,
);

export const selectHandDrawnAnnotationEntities = createSelector(
  selectHandDrawnAnnotationsState,
  selectEntities,
);

export const selectAllHandDrawnAnnotationIds = createSelector(
  selectHandDrawnAnnotationsState,
  (state) => selectIds(state) as string[]
);

export const selectHighlightedAnnotationId = createSelector(
  selectHandDrawnAnnotationsState,
  (state) => state.highlighted
);

export const selectHighlightedAnnotation = createSelector(
  selectHandDrawnAnnotationEntities,
  selectHighlightedAnnotationId,
  (entities, annotationId) => annotationId ? entities[annotationId] : undefined
);

export const selectAllSelectedState = createSelector(
  selectAllHandDrawnAnnotations,
  selectFilteredAnnotationIdEntities,
  (annotations, selectedEntities) => annotations.every(annotation => !!selectedEntities.get(annotation.id))
);

export const selectIndeterminateAnnotationSelection = createSelector(
  selectAllSelectedState,
  selectAllHandDrawnAnnotations,
  selectFilteredAnnotationIdEntities,
  (allSelected, annotations, selectedEntities) => !allSelected && annotations.some(annotation => !!selectedEntities.get(annotation.id))
);

export const selectHandDrawnAnnotationsLoaded = createSelector(
  selectHandDrawnAnnotationsState,
  (state) => state.loaded
);

export const selectHandDrawnAnnotationsError = createSelector(
  selectHandDrawnAnnotationsState,
  (state) => state.error
);
