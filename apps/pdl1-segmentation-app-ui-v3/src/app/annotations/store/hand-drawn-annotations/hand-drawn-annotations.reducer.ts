import { EntityState, createEntityAdapter } from '@ngrx/entity';
import { createReducer, on } from '@ngrx/store';
import { AnnotationEntity } from 'slide-viewer';
import { HttpErrorResponse } from '@angular/common/http';
import { HandDrawnAnnotationsActions } from './hand-drawn-annotations.actions';


export const HAND_DRAWN_ANNOTATIONS_FEATURE_KEY = 'handDrawnAnnotations';

export interface State extends EntityState<AnnotationEntity> {
  loaded: boolean;
  highlighted?: string | undefined;
  error?: HttpErrorResponse | undefined;
}

export const handDrawnAnnotationsAdapter = createEntityAdapter<AnnotationEntity>();
export const initialState: State = handDrawnAnnotationsAdapter.getInitialState({
  loaded: true,
  highlighted: undefined,
  error: undefined,
});

export const reducer = createReducer(
  initialState,
  on(HandDrawnAnnotationsActions.loadAnnotationsSuccess, (state, { annotations }): State =>
    handDrawnAnnotationsAdapter.setAll(annotations, {
      ...state,
      loaded: true,
    })
  ),
  on(HandDrawnAnnotationsActions.loadAnnotationsFailure, (state, { error }): State => ({
    ...state,
    loaded: true,
    error,
  })),
  on(HandDrawnAnnotationsActions.createAnnotation, (state): State => ({
    ...state,
    loaded: false,
  })),
  on(HandDrawnAnnotationsActions.createAnnotationSuccess, (state, { annotation }): State =>
    handDrawnAnnotationsAdapter.addOne(annotation, {
      ...state,
      loaded: true,
    })
  ),
  on(HandDrawnAnnotationsActions.createAnnotationFailure, (state, { error }): State => ({
    ...state,
    loaded: true,
    error,
  })),
  on(HandDrawnAnnotationsActions.deleteAnnotation, (state): State => ({
    ...state,
    loaded: false,
  })),
  on(HandDrawnAnnotationsActions.deleteAnnotationSuccess, (state, { annotationId }): State =>
    handDrawnAnnotationsAdapter.removeOne(annotationId, {
      ...state,
      loaded: true,
    })
  ),
  on(HandDrawnAnnotationsActions.clearHandDrawnAnnotations, (state): State =>
    handDrawnAnnotationsAdapter.removeAll({
      ...state,
      ...initialState,
    })
  ),
  on(HandDrawnAnnotationsActions.highlightHandDrawnAnnotation, (state, { highlighted }): State => ({
    ...state,
    highlighted,
  }))
);
