import { HttpErrorResponse } from '@angular/common/http';
import { createActionGroup, emptyProps, props } from '@ngrx/store';
import { Annotation, AnnotationEntity } from 'slide-viewer';

export const HandDrawnAnnotationsActions = createActionGroup({
  source: 'HandDrawnAnnotations',
  events: {
    'Load Annotations Success': props<{ annotations: AnnotationEntity[] }>(),
    'Load Annotations Failure': props<{ error: HttpErrorResponse }>(),
    'Create Annotation': props<{ annotation: Annotation }>(),
    'Create Annotation Ready': props<{
      annotation: Annotation,
      scopeId: string,
      slideId: string,
    }>(),
    'Create Annotation Success': props<{ annotation: AnnotationEntity }>(),
    'Create Annotation Failure': props<{ error: HttpErrorResponse }>(),
    'Delete Annotation': props<{ annotationId: string }>(),
    'Delete Annotation Ready': props<{ scopeId: string, annotationId: string }>(),
    'Delete Annotation Success': props<{ annotationId: string }>(),
    'Delete Annotation Failure': props<{ error: HttpErrorResponse }>(),
    'Select All Annotations': props<{ allSelected: boolean }>(),
    'Highlight Hand Drawn Annotation': props<{ highlighted?: string | undefined }>(),
    'Clear Hand Drawn Annotations': emptyProps(),
  }
});
