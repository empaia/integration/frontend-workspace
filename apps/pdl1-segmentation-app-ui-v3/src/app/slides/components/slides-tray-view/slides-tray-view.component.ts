import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { SlideEntity, SlideImage } from '@slides/store';
import { SelectionError } from '@menu/models/ui.models';
import { Dictionary } from '@ngrx/entity';
import { Primitive } from '@primitives/store';

@Component({
  selector: 'app-slides-tray-view',
  templateUrl: './slides-tray-view.component.html',
  styleUrls: ['./slides-tray-view.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SlidesTrayViewComponent {
  @Input() public slideEntities!: SlideEntity[];
  @Input() public selectedSlideId!: string;
  @Input() public slideImages!: Dictionary<SlideImage>;
  @Input() public selectionMissing!: SelectionError;
  @Input() public contentLoaded!: boolean;
  @Input() public slidesPrimitiveEntities!: Map<string, Primitive>;
  @Input() public slidesSkip!: number;
  @Input() public slidesLimit!: number;
  @Input() public maxSlidesCount!: number;
  @Input() public freezedSlideCount!: number;

  @Output() public slideSelected = new EventEmitter<string>();
  @Output() public loadNewSlidesPage = new EventEmitter<number>();
  @Output() public nextSlide = new EventEmitter<void>();

  public selectionListChanged(id: string): void {
    this.slideSelected.emit(id);
  }

  public trackBySlideId(slide: SlideEntity): string {
    return slide.id;
  }
}
