import { SlideTrayItemComponent } from './slide-tray-item.component';
import { createComponentFactory, Spectator } from '@ngneat/spectator';
import { MockComponents } from 'ng-mocks';
import { SlideImageItemComponent } from '@slides/components/slide-image-item/slide-image-item.component';
import { MaterialModule } from '@material/material.module';
import { DictParserPipe } from '@shared/pipes/dict-parser.pipe';

describe('SlideTrayItemComponent', () => {
  let spectator: Spectator<SlideTrayItemComponent>;
  const createComponent = createComponentFactory({
    component: SlideTrayItemComponent,
    imports: [
      MaterialModule,
    ],
    declarations: [
      DictParserPipe,
      MockComponents(
        SlideImageItemComponent,
      )
    ]
  });

  beforeEach(() => spectator = createComponent());

  it('should create', () => {
    expect(spectator.component).toBeTruthy();
  });
});
