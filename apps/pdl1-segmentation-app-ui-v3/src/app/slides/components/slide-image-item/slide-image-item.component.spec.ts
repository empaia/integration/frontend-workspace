import { Spectator, createComponentFactory } from '@ngneat/spectator/jest';
import { SlideImageItemComponent } from './slide-image-item.component';

describe('SlideImageItemComponent', () => {
  let spectator: Spectator<SlideImageItemComponent>;
  const createComponent = createComponentFactory({
    component: SlideImageItemComponent,
  });

  beforeEach(() => spectator = createComponent());

  it('should create', () => {
    expect(spectator.component).toBeTruthy();
  });
});
