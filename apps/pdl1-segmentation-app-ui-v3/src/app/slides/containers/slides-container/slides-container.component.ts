import { ChangeDetectionStrategy, Component } from '@angular/core';
import { MenuEntity, MenuState } from '@menu/models/menu.models';
import { SelectionError } from '@menu/models/ui.models';
import { MenuSelectors } from '@menu/store';
import { Dictionary } from '@ngrx/entity';
import { Store } from '@ngrx/store';
import { Primitive, PrimitivesSelectors } from '@primitives/store';
import { SLIDES_LIMIT, SlideEntity, SlideImage, SlidesActions, SlidesImagesSelectors, SlidesSelectors } from '@slides/store';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-slides-container',
  templateUrl: './slides-container.component.html',
  styleUrls: ['./slides-container.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SlidesContainerComponent {
  public menuState = MenuState;
  public slidesMenu$: Observable<MenuEntity>;
  public slide$: Observable<SlideEntity | undefined>;
  public slides$: Observable<SlideEntity[]>;
  public slideImages$: Observable<Dictionary<SlideImage>>;
  public freezedSlides$: Observable<number>;
  public loaded$: Observable<boolean>;
  public slidePrimitiveEntities$: Observable<Map<string, Primitive>>;
  public slidesSkip$: Observable<number>;
  public maxSlidesCount$: Observable<number>;
  public selectionError$: Observable<SelectionError | undefined>;

  public readonly SLIDES_BATCH_LIMIT = SLIDES_LIMIT;

  constructor(private store: Store) {
    this.slidesMenu$ = this.store.select(MenuSelectors.selectSlidesMenu);
    this.slide$ = this.store.select(SlidesSelectors.selectSelectedSlide);
    this.slides$ = this.store.select(SlidesSelectors.selectSlideBatch);
    this.slideImages$ = this.store.select(SlidesImagesSelectors.selectSlidesImagesEntities);
    this.freezedSlides$ = this.store.select(PrimitivesSelectors.selectNumberOfFreezedSlides);
    this.loaded$ = this.store.select(SlidesSelectors.selectSlidesLoaded);
    this.slidePrimitiveEntities$ = this.store.select(PrimitivesSelectors.selectSlidePrimitiveEntities);
    this.slidesSkip$ = this.store.select(SlidesSelectors.selectSlideSkip);
    this.maxSlidesCount$ = this.store.select(SlidesSelectors.selectMaxSlidesCount);
    this.selectionError$ = this.store.select(SlidesSelectors.selectSelectionError);
  }

  public selectSlide(id: string): void {
    this.store.dispatch(SlidesActions.selectSlide({ id }));
  }

  public onNextSlideClicked(): void {
    this.store.dispatch(SlidesActions.selectNextUnapprovedSlide());
  }

  public onLoadNewSlidesPage(skip: number): void {
    this.store.dispatch(SlidesActions.setSlidePage({ skip }));
  }
}
