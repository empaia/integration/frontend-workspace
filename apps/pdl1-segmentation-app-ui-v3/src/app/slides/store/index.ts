import { SlidesActions } from './slides/slides.actions';
import * as SlidesFeature from './slides/slides.reducer';
import * as SlidesSelectors from './slides/slides.selectors';
export * from './slides/slides.effects';
export * from './slides/slides.models';

export { SlidesActions, SlidesFeature, SlidesSelectors };


import * as SlidesImagesActions from './slides-images/slides-images.actions';
import * as SlidesImagesFeature from './slides-images/slides-images.reducer';
import * as SlidesImagesSelectors from './slides-images/slides-images.selectors';
export * from './slides-images/slides-images.effects';
export * from './slides-images/slides-images.models';

export { SlidesImagesActions, SlidesImagesFeature, SlidesImagesSelectors };
