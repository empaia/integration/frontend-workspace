import { Injectable } from '@angular/core';
import { AppV3SlidesService } from 'empaia-api-lib';
import { Actions, concatLatestFrom, createEffect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { fetch } from '@ngrx/router-store/data-persistence';
import { filterNullish, retryOnAction } from '@shared/helper/rxjs-operators';
import {
  debounceTime,
  filter,
  map,
  mergeMap,
  retryWhen,
  takeUntil,
} from 'rxjs/operators';
import {
  MAX_SLIDE_THUMBNAIL_HEIGHT,
  MAX_SLIDE_THUMBNAIL_WIDTH,
  SlideImage,
  SlideImageFormat,
  SlideImageStatus,
  SlidesImagesFeature,
  SLIDE_THUMBNAIL_QUALITY,
  SlidesSelectors,
  SLIDE_PAGINATION_DEBOUNCE_TIME,
  SLIDE_IMAGE_CONCURRENCY,
} from '..';
import * as SlidesImagesActions from './slides-images.actions';
import { SlidesActions } from '@slides/store/slides/slides.actions';
import * as TokenActions from '@token/store/token/token.actions';
import * as SlidesImagesSelectors from './slides-images.selectors';
import * as ScopeSelectors from '@scope/store/scope/scope.selectors';
import { requestNewToken } from 'vendor-app-communication-interface';
import { PostProcessingJobsActions } from '@jobs/store/post-processing-jobs/post-processing-jobs.actions';

@Injectable()
export class SlidesImagesEffects {
  // prepare clear slide image. Revoke all stored urls to set the
  // allocated memory free
  prepareClearImages$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(SlidesImagesActions.prepareClearImages),
      concatLatestFrom(() =>
        this.store.select(SlidesImagesSelectors.selectAllSlidesImages)
      ),
      map(([_action, images]) => {
        images.forEach((img) => {
          if (img.label) {
            URL.revokeObjectURL(img.label);
          }
          if (img.thumbnail) {
            URL.revokeObjectURL(img.thumbnail);
          }
        });
        return SlidesImagesActions.clearAllImages();
      })
    );
  });

  // start loading labels and thumbnails, if slides are loaded
  // or a new pagination page was selected
  startLoadingImages$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(
        // SlidesActions.loadSlidesSuccess,
        PostProcessingJobsActions.setPostProcessingJobs,
        SlidesActions.setSlidePage
      ),
      debounceTime(SLIDE_PAGINATION_DEBOUNCE_TIME),
      concatLatestFrom(() => [
        this.store
          .select(SlidesSelectors.selectSlideBatchIds)
          .pipe(filterNullish()),
        this.store.select(SlidesImagesSelectors.selectSlidesImagesEntities),
      ]),
      // don't refetch images, only load images that are not in memory
      // or doesn't have labels or thumbnails, due to the fact that
      // label and thumbnail request can be aborted
      map(([, slideIds, entities]) =>
        slideIds.filter(
          (slideId) =>
            !entities[slideId] ||
            entities[slideId]?.thumbnailStatus === SlideImageStatus.LOADING ||
            entities[slideId]?.labelStatus === SlideImageStatus.LOADING
        )
      ),
      filter((slideIds) => !!slideIds?.length),
      map((slideIds) =>
        SlidesImagesActions.loadManySlidesLabelsAndThumbnails({
          slideIds,
          maxWidth: MAX_SLIDE_THUMBNAIL_WIDTH,
          maxHeight: MAX_SLIDE_THUMBNAIL_HEIGHT,
          imageFormat: SlideImageFormat.JPEG,
          imageQuality: SLIDE_THUMBNAIL_QUALITY,
        })
      )
    );
  });

  loadManySlidesLabelsAndThumbnails$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(SlidesImagesActions.loadManySlidesLabelsAndThumbnails),
      concatLatestFrom(() => this.store.select(ScopeSelectors.selectScopeId)),
      mergeMap(([action, scopeId]) => [
        SlidesImagesActions.loadManySlidesLabels({
          ...action,
          scopeId: scopeId as string,
        }),
        SlidesImagesActions.loadManySlidesThumbnails({
          ...action,
          scopeId: scopeId as string,
        }),
      ])
    );
  });

  loadManySlidesLabels$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(SlidesImagesActions.loadManySlidesLabels),
      mergeMap(
        (action) =>
          action.slideIds
            .filter((slideId) => !!slideId)
            .map((slideId) =>
              SlidesImagesActions.loadSlideLabel({
                slideId,
                scopeId: action.scopeId,
                maxWidth: action.maxWidth,
                maxHeight: action.maxHeight,
                imageFormat: action.imageFormat,
                imageQuality: action.imageQuality,
              })
            ),
        SLIDE_IMAGE_CONCURRENCY
      )
    );
  });

  loadManySlidesThumbnails$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(SlidesImagesActions.loadManySlidesThumbnails),
      mergeMap(
        (action) =>
          action.slideIds
            .filter((slideId) => !!slideId)
            .map((slideId) =>
              SlidesImagesActions.loadSlideThumbnail({
                slideId,
                scopeId: action.scopeId,
                maxWidth: action.maxWidth,
                maxHeight: action.maxHeight,
                imageFormat: action.imageFormat,
                imageQuality: action.imageQuality,
              })
            ),
        SLIDE_IMAGE_CONCURRENCY
      )
    );
  });

  loadSlideLabel$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(SlidesImagesActions.loadSlideLabel),
      fetch({
        id: (
          action: ReturnType<typeof SlidesImagesActions.loadSlideLabel>,
          _state: SlidesImagesFeature.State
        ) => {
          return action.slideId + ' ' + action.type;
        },
        run: (
          action: ReturnType<typeof SlidesImagesActions.loadSlideLabel>,
          _state: SlidesImagesFeature.State
        ) => {
          return this.slidesService
            .scopeIdSlidesSlideIdLabelMaxSizeMaxXMaxYGet({
              scope_id: action.scopeId,
              slide_id: action.slideId,
              max_x: action.maxWidth,
              max_y: action.maxHeight,
              image_format: action.imageFormat,
              image_quality: action.imageQuality,
            })
            .pipe(
              takeUntil(this.actions$.pipe(ofType(SlidesActions.setSlidePage))),
              // retry to load the selected slide after the token is expired
              retryWhen((errors) =>
                retryOnAction(
                  errors,
                  this.actions$,
                  TokenActions.setAccessToken,
                  requestNewToken
                )
              ),
              map((img) => URL.createObjectURL(img)),
              map(
                (img) =>
                  ({
                    id: action.slideId,
                    labelStatus: SlideImageStatus.LOADED,
                    label: img,
                  } as SlideImage)
              ),
              map((slideImage) =>
                SlidesImagesActions.loadSlideLabelSuccess({ slideImage })
              )
            );
        },
        onError: (
          action: ReturnType<typeof SlidesImagesActions.loadSlideLabel>,
          error
        ) => {
          return SlidesImagesActions.loadSlideLabelFailure({
            slideId: action.slideId,
            error,
          });
        },
      })
    );
  });

  loadSlideThumbnail$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(SlidesImagesActions.loadSlideThumbnail),
      fetch({
        id: (
          action: ReturnType<typeof SlidesImagesActions.loadSlideThumbnail>,
          _state: SlidesImagesFeature.State
        ) => {
          return action.slideId + ' ' + action.type;
        },
        run: (
          action: ReturnType<typeof SlidesImagesActions.loadSlideThumbnail>,
          _state: SlidesImagesFeature.State
        ) => {
          return this.slidesService
            .scopeIdSlidesSlideIdThumbnailMaxSizeMaxXMaxYGet({
              scope_id: action.scopeId,
              slide_id: action.slideId,
              max_x: action.maxWidth,
              max_y: action.maxHeight,
              image_format: action.imageFormat,
              image_quality: action.imageQuality,
            })
            .pipe(
              takeUntil(this.actions$.pipe(ofType(SlidesActions.setSlidePage))),
              // retry to load the selected slide after the token is expired
              retryWhen((errors) =>
                retryOnAction(
                  errors,
                  this.actions$,
                  TokenActions.setAccessToken,
                  requestNewToken
                )
              ),
              map((img) => URL.createObjectURL(img)),
              map((img) => {
                const slideImage: SlideImage = {
                  id: action.slideId,
                  thumbnailStatus: SlideImageStatus.LOADED,
                  thumbnail: img,
                };
                return slideImage;
              }),
              map((slideImage) =>
                SlidesImagesActions.loadSlideThumbnailSuccess({ slideImage })
              )
            );
        },
        onError: (
          action: ReturnType<typeof SlidesImagesActions.loadSlideThumbnail>,
          error
        ) => {
          return SlidesImagesActions.loadSlideThumbnailFailure({
            slideId: action.slideId,
            error,
          });
        },
      })
    );
  });

  constructor(
    private readonly actions$: Actions,
    private readonly store: Store,
    private readonly slidesService: AppV3SlidesService
  ) {}
}
