import { HttpErrorResponse } from '@angular/common/http';
import { createActionGroup, emptyProps, props } from '@ngrx/store';
import { Slide } from 'slide-viewer';
import { SlideEntity } from './slides.models';

export const SlidesActions = createActionGroup({
  source: 'Slides',
  events: {
    'Load Slides': props<{ scopeId: string }>(),
    'Load Slides Success': props<{ slides: SlideEntity[] }>(),
    'Load Slides Failure': props<{ error: HttpErrorResponse }>(),
    'Load Slide Image Info Success': props<{ slideImage: Slide }>(),
    'Select Slide': props<{ id: string | undefined }>(),
    'Select Slide Ready': props<{ id: string | undefined }>(),
    'Select Next Unapproved Slide': emptyProps(),
    'Set Slide Page': props<{ skip: number }>(),
    'Clear Slides': emptyProps(),
  }
});
