import {
  AppV3Slide as SlideView,
  AppV3SlideInfo,
  AppV3SlideLevel,
  AppV3SlideColor,
  AppV3TagMapping,
} from 'empaia-api-lib';
import { Slide } from 'slide-viewer';

export type SlideInfo = AppV3SlideInfo;
export type SlideLevel = AppV3SlideLevel;
export type SlideColor = AppV3SlideColor;
export type TagMapping = AppV3TagMapping;

export interface SlideEntity {
  id: string;
  disabled: boolean;
  dataView: SlideView;
  imageView?: Slide;
}

export const SLIDES_LIMIT = 20;
