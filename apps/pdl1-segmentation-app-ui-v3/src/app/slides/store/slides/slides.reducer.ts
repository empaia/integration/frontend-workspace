import { HttpErrorResponse } from '@angular/common/http';
import { SlideEntity } from '@slides/store/slides/slides.models';
import { EntityState, createEntityAdapter } from '@ngrx/entity';
import { createReducer, on } from '@ngrx/store';
import { SlidesActions } from './slides.actions';


export const SLIDES_FEATURE_KEY = 'slides';

export interface State extends EntityState<SlideEntity> {
  loaded: boolean;
  skip: number;
  selected?: string;
  error?: HttpErrorResponse | null;
}

export const slidesAdapter = createEntityAdapter<SlideEntity>();

export const initialState: State = slidesAdapter.getInitialState({
  loaded: true,
  skip: 0,
  selected: undefined,
  error: undefined,
});

export const reducer = createReducer(
  initialState,
  on(SlidesActions.loadSlides, (state): State => ({
    ...state,
    loaded: false,
  })),
  on(SlidesActions.loadSlidesSuccess, (state, { slides }): State =>
    slidesAdapter.setAll(slides, {
      ...state,
      loaded: true,
      error: null,
    })
  ),
  on(SlidesActions.loadSlidesFailure, (state, { error }): State => ({
    ...state,
    loaded: true,
    error,
  })),
  on(SlidesActions.selectSlideReady, (state, { id }): State => ({
    ...state,
    selected: id,
  })),
  on(SlidesActions.loadSlideImageInfoSuccess, (state, { slideImage }): State =>
    slidesAdapter.updateOne({
      id: slideImage.slideId,
      changes: {
        imageView: slideImage
      }
    },
    {
      ...state,
      loaded: true
    })
  ),
  on(SlidesActions.setSlidePage, (state, { skip }): State => ({
    ...state,
    skip,
  })),
  on(SlidesActions.clearSlides, (state): State =>
    slidesAdapter.removeAll({
      ...state,
      ...initialState
    })
  )
);
