import { Injectable } from '@angular/core';
import { AppV3SlidesService } from 'empaia-api-lib';
import { Actions, concatLatestFrom, createEffect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { fetch } from '@ngrx/router-store/data-persistence';
import { filterNullish, retryOnAction } from '@shared/helper/rxjs-operators';
import { SlideConversionService } from '@slides/services/slide-conversion.service';
import {
  catchError,
  distinctUntilChanged,
  exhaustMap,
  filter,
  map,
  mergeMap,
  retryWhen,
} from 'rxjs/operators';
import { SLIDES_LIMIT, SlideEntity, SlidesFeature, SlidesSelectors } from '..';
import { SlidesActions } from './slides.actions';
import * as ScopeActions from '@scope/store/scope/scope.actions';
import * as TokenActions from '@token/store/token/token.actions';
import * as ScopeSelectors from '@scope/store/scope/scope.selectors';
import * as TokenSelectors from '@token/store/token/token.selectors';
import { requestNewToken } from 'vendor-app-communication-interface';
import { of } from 'rxjs';
import { PrimitivesSelectors } from '@primitives/store';
import { PostProcessingJobsActions } from '@jobs/store';
import { MenuActions } from '@menu/store';
import {
  LARGE_DIALOG_WIDTH,
  SLIDES_DONE_DIALOG_DATA,
  SMALL_DIALOG_WIDTH,
} from '@menu/store/menu/menu.models';
import { ChangeLossInfoDialogComponent } from '@shared/components/change-loss-info-dialog/change-loss-info-dialog.component';
import { MatDialog } from '@angular/material/dialog';

@Injectable()
export class SlidesEffects {
  // start loading slides after the scope id was set
  startLoadingSlides$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ScopeActions.setScope, TokenActions.setAccessToken),
      concatLatestFrom(() => [
        this.store.select(ScopeSelectors.selectScopeId).pipe(filterNullish()),
        this.store.select(TokenSelectors.selectAccessToken),
      ]),
      filter(([, _scopeId, token]) => !!token),
      distinctUntilChanged((prev, curr) => prev[1] === curr[1]),
      map(([, scopeId]) => scopeId),
      map((scopeId) => SlidesActions.loadSlides({ scopeId }))
    );
  });

  loadSlides$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(SlidesActions.loadSlides),
      fetch({
        run: (
          action: ReturnType<typeof SlidesActions.loadSlides>,
          _state: SlidesFeature.State
        ) => {
          return this.slideService
            .scopeIdSlidesGet({
              scope_id: action.scopeId,
            })
            .pipe(
              // retry to load the selected slide after the token is expired
              retryWhen((errors) =>
                retryOnAction(
                  errors,
                  this.actions$,
                  TokenActions.setAccessToken,
                  requestNewToken
                )
              ),
              map((slides) => slides.items),
              map((slideViews) =>
                slideViews.map(
                  (v) =>
                    ({
                      id: v.id,
                      dataView: v,
                      disabled: false,
                    } as SlideEntity)
                )
              ),
              map((slides) => SlidesActions.loadSlidesSuccess({ slides }))
            );
        },
        onError: (
          _action: ReturnType<typeof SlidesActions.loadSlides>,
          error
        ) => {
          return SlidesActions.loadSlidesFailure({ error });
        },
      })
    );
  });

  selectSlide$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(SlidesActions.selectSlideReady),
      map((action) => action.id),
      filterNullish(),
      concatLatestFrom(() => this.store.select(ScopeSelectors.selectScopeId)),
      mergeMap(([id, scopeId]) =>
        this.slideService
          .scopeIdSlidesSlideIdInfoGet({
            slide_id: id as string,
            scope_id: scopeId as string,
          })
          .pipe(
            map((slideInfo) => this.slideConverter.fromWbsApiType(slideInfo)),
            map((slideImage) =>
              SlidesActions.loadSlideImageInfoSuccess({ slideImage })
            ),
            // retry to load the selected slide after the token is expired
            retryWhen((errors) =>
              retryOnAction(
                errors,
                this.actions$,
                TokenActions.setAccessToken,
                requestNewToken
              )
            ),
            catchError((error) =>
              of(SlidesActions.loadSlidesFailure({ error }))
            )
          )
      )
    );
  });

  setSkipOnSelectSlide$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(SlidesActions.selectSlideReady),
      map((action) => action.id),
      filterNullish(),
      concatLatestFrom(() =>
        this.store.select(SlidesSelectors.selectSelectedSlideIndex)
      ),
      map(([, index]) => Math.floor(index / SLIDES_LIMIT) * SLIDES_LIMIT),
      map((skip) => SlidesActions.setSlidePage({ skip }))
    );
  });

  // select next slide which isn't freezed
  // if there is no unapproved slide or we reached
  // the end of the list, open slides tray view
  selectNextUnapprovedSlide$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(SlidesActions.selectNextUnapprovedSlide),
      concatLatestFrom(() =>
        this.store.select(PrimitivesSelectors.selectNextUnapprovedSlideId)
      ),
      map(([, slideId]) => slideId),
      map((id) =>
        id
          ? SlidesActions.selectSlide({ id })
          : MenuActions.openMenuMax({ id: 'slides' })
      )
    );
  });

  // open info dialog when all slides were processed by a user,
  // also when the user returns and all slides are already done
  openDialogOnAllSlidesDone$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(
        PostProcessingJobsActions.setPostProcessingJob,
        PostProcessingJobsActions.setPostProcessingJobs,
        SlidesActions.selectNextUnapprovedSlide
      ),
      concatLatestFrom(() =>
        this.store.select(PrimitivesSelectors.selectAllUnapprovedSlideIds)
      ),
      filter(([, slideIds]) => slideIds?.length <= 0),
      map(() =>
        MenuActions.openInfoDialog({
          infoData: SLIDES_DONE_DIALOG_DATA,
          size: LARGE_DIALOG_WIDTH,
        })
      )
    );
  });

  // open change loss dialog if user want to select a new
  // slide but forgot to save
  openDialogOnSlideSelection$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(SlidesActions.selectSlide),
      map((action) => action.id),
      concatLatestFrom(() => [
        this.store.select(PrimitivesSelectors.selectAreChangesToApply),
      ]),
      exhaustMap(([id, allApplied]) =>
        allApplied
          ? this.dialog
            .open(ChangeLossInfoDialogComponent, {
              width: SMALL_DIALOG_WIDTH,
            })
            .afterClosed()
            .pipe(
              filterNullish(),
              map(() => SlidesActions.selectSlideReady({ id }))
            )
          : of(SlidesActions.selectSlideReady({ id }))
      )
    );
  });

  constructor(
    private readonly actions$: Actions,
    private readonly slideService: AppV3SlidesService,
    private readonly slideConverter: SlideConversionService,
    private readonly store: Store,
    private readonly dialog: MatDialog
  ) {}
}
