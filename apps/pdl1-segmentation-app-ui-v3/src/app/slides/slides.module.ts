import { EffectsModule } from '@ngrx/effects';
import { SLIDES_MODULE_FEATURE_KEY, reducers } from '@slides/store/slides-feature.state';
import { StoreModule } from '@ngrx/store';
import { SharedModule } from './../shared/shared.module';
import { LetDirective, PushPipe } from '@ngrx/component';
import { MaterialModule } from '@material/material.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SlidesEffects, SlidesImagesEffects } from '@slides/store';
import { SlideImageItemComponent } from './components/slide-image-item/slide-image-item.component';
import { SlideItemComponent } from './components/slide-item/slide-item.component';
import { SlidesComponent } from './components/slides/slides.component';
import { SlidesLabelComponent } from './components/slides-label/slides-label.component';
import { SlidesContainerComponent } from './containers/slides-container/slides-container.component';
import { SlidesTrayViewComponent } from './components/slides-tray-view/slides-tray-view.component';
import { SlideTrayItemComponent } from './components/slide-tray-item/slide-tray-item.component';
import { CommonUiModule } from 'empaia-ui-commons';



@NgModule({
  declarations: [
    SlideImageItemComponent,
    SlideItemComponent,
    SlidesComponent,
    SlidesLabelComponent,
    SlidesContainerComponent,
    SlidesTrayViewComponent,
    SlideTrayItemComponent,
  ],
  imports: [
    CommonModule,
    CommonUiModule,
    MaterialModule,
    LetDirective,
    PushPipe,
    SharedModule,
    StoreModule.forFeature(
      SLIDES_MODULE_FEATURE_KEY,
      reducers
    ),
    EffectsModule.forFeature([
      SlidesEffects,
      SlidesImagesEffects,
    ])
  ],
  exports: [
    SlidesContainerComponent,
    SlidesLabelComponent,
  ]
})
export class SlidesModule { }
