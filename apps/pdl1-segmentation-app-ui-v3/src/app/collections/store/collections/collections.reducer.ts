import { HttpErrorResponse } from '@angular/common/http';
import { EntityState, createEntityAdapter } from '@ngrx/entity';
import { createReducer, on } from '@ngrx/store';
import { CollectionsActions } from './collections.actions';
import { Collection } from './collections.models';


export const COLLECTIONS_FEATURE_KEY = 'collections';

export interface State extends EntityState<Collection> {
  loaded: boolean;
  error?: HttpErrorResponse | undefined;
}

export const collectionsAdapter = createEntityAdapter<Collection>();

export const initialState: State = collectionsAdapter.getInitialState({
  loaded: true,
  error: undefined,
});

export const reducer = createReducer(
  initialState,
  on(CollectionsActions.loadCollections, (state): State => ({
    ...state,
    loaded: false,
  })),
  on(CollectionsActions.loadCollection, (state): State => ({
    ...state,
    loaded: false,
  })),
  on(CollectionsActions.loadCollectionSuccess, (state, { collection }): State =>
    collectionsAdapter.upsertOne(collection, {
      ...state,
      loaded: true,
    })
  ),
  on(CollectionsActions.loadCollectionFailure, (state, { error }): State => ({
    ...state,
    loaded: true,
    error,
  })),
  on(CollectionsActions.createCollection, (state): State => ({
    ...state,
    loaded: false,
  })),
  on(CollectionsActions.createCollectionSuccess, (state, { collection }): State =>
    collectionsAdapter.addOne(collection, {
      ...state,
      loaded: true,
    })
  ),
  on(CollectionsActions.createCollectionFailure, (state, { error }): State => ({
    ...state,
    loaded: true,
    error,
  })),
  on(CollectionsActions.addItemToCollection, (state): State => ({
    ...state,
    loaded: false,
  })),
  on(CollectionsActions.addItemsToCollection, (state): State => ({
    ...state,
    loaded: false,
  })),
  on(CollectionsActions.addItemOrItemsToCollectionFailure, (state, { error }): State => ({
    ...state,
    loaded: true,
    error,
  })),
  on(CollectionsActions.removeItemFromCollection, (state): State => ({
    ...state,
    loaded: false,
  })),
  on(CollectionsActions.removeItemsFromCollection, (state): State => ({
    ...state,
    loaded: false,
  })),
  on(CollectionsActions.removeItemFromCollectionFailure, (state, { error }): State => ({
    ...state,
    loaded: true,
    error,
  })),
  on(CollectionsActions.clearCollections, (state): State =>
    collectionsAdapter.removeAll({
      ...state,
      ...initialState,
    })
  ),
);
