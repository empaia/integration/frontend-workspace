import { createSelector } from '@ngrx/store';
import { selectCollectionsModuleState } from '../collections-feature.state';
import { collectionsAdapter, COLLECTIONS_FEATURE_KEY } from './collections.reducer';

const {
  selectIds,
  selectAll,
  selectEntities,
} = collectionsAdapter.getSelectors();

export const selectCollectionsState = createSelector(
  selectCollectionsModuleState,
  (state) => state[COLLECTIONS_FEATURE_KEY]
);

export const selectAllCollectionIds = createSelector(
  selectCollectionsState,
  (state) => selectIds(state) as string[]
);

export const selectAllCollections = createSelector(
  selectCollectionsState,
  selectAll,
);

export const selectCollectionEntities = createSelector(
  selectCollectionsState,
  selectEntities,
);
