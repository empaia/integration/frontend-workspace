import { HttpErrorResponse } from '@angular/common/http';
import { JobKeyType, PostCollection } from '@jobs/store/post-processing-jobs/post-processing-jobs.models';
import { createActionGroup, emptyProps, props } from '@ngrx/store';
import { Collection, IdObject, PostIdObjects } from './collections.models';

export const CollectionsActions = createActionGroup({
  source: 'Collections Actions',
  events: {
    'Load Collections': props<{
      scopeId: string,
      collectionIds: string[]
    }>(),
    'Load Collection': props<{
      scopeId: string,
      collectionId: string
    }>(),
    'Load Collection Success': props<{ collection: Collection }>(),
    'Load Collection Failure': props<{ error: HttpErrorResponse }>(),
    'Create Collection': props<{
      scopeId: string,
      jobId: string,
      key: string,
      keyType: JobKeyType,
      postCollection: PostCollection,
    }>(),
    'Create Collection Success': props<{
      collection: Collection,
      scopeId: string,
      jobId: string,
      key: string,
      keyType: JobKeyType,
    }>(),
    'Create Collection Failure': props<{ error: HttpErrorResponse }>(),
    'Add Item To Collection': props<{
      scopeId: string,
      collectionId: string,
      postIdObject: IdObject,
    }>(),
    'Add Items To Collection': props<{
      scopeId: string,
      collectionId: string,
      postIdObjects: PostIdObjects,
    }>(),
    'Add Item Or Items To Collection Failure': props<{ error: HttpErrorResponse }>(),
    'Remove Item From Collection': props<{
      scopeId: string,
      collectionId: string,
      itemId: string,
    }>(),
    'Remove Item From Collection Failure': props<{ error: HttpErrorResponse }>(),
    'Remove Items From Collection': props<{
      scopeId: string,
      collectionId: string,
      itemIds: string[],
    }>(),
    'Clear Collections': emptyProps(),
  }
});
