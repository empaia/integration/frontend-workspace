import {
  AppV3Collection,
  AppV3IdObject,
  AppV3PostIdObjects,
  AppV3PostPointCollection,
  AppV3PostLineCollection,
  AppV3PostArrowCollection,
  AppV3PostCirceCollection,
  AppV3PostRectangleCollection,
  AppV3PostPolygonCollection,
  AppV3PostClassCollection,
  AppV3PostIntegerCollection,
  AppV3PostFloatCollection,
  AppV3PostBoolCollection,
  AppV3PostStringCollection,
  AppV3PostSlideCollection,
  AppV3PostIdCollection,
  AppV3PostNestedCollection,
} from 'empaia-api-lib';
export {
  AppV3CollectionItemType as CollectionItemType,
  AppV3DataCreatorType as DataCreatorType,
} from 'empaia-api-lib';

export type Collection = AppV3Collection;
export type IdObject = AppV3IdObject;
export type PostIdObjects = AppV3PostIdObjects;
export type PostPointCollection = AppV3PostPointCollection;
export type PostLineCollection = AppV3PostLineCollection;
export type PostArrowCollection = AppV3PostArrowCollection;
export type PostCirceCollection = AppV3PostCirceCollection;
export type PostRectangleCollection = AppV3PostRectangleCollection;
export type PostPolygonCollection = AppV3PostPolygonCollection;
export type PostClassCollection = AppV3PostClassCollection;
export type PostIntegerCollection = AppV3PostIntegerCollection;
export type PostFloatCollection = AppV3PostFloatCollection;
export type PostBoolCollection = AppV3PostBoolCollection;
export type PostStringCollection = AppV3PostStringCollection;
export type PostSlideCollection = AppV3PostSlideCollection;
export type PostIdCollection = AppV3PostIdCollection;
export type PostNestedCollection = AppV3PostNestedCollection;
