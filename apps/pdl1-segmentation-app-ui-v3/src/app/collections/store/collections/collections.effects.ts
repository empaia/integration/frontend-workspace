import { Injectable } from '@angular/core';
import { AppV3DataService } from 'empaia-api-lib';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { CollectionsActions } from './collections.actions';
import * as TokenActions from '@token/store/token/token.actions';
import { fetch, pessimisticUpdate } from '@ngrx/router-store/data-persistence';
import { State } from './collections.reducer';
import { map, mergeMap, retryWhen } from 'rxjs/operators';
import { retryOnAction } from '@shared/helper/rxjs-operators';
import { requestNewToken } from 'vendor-app-communication-interface';
import { JobKeyType } from '@jobs/store/post-processing-jobs/post-processing-jobs.models';
import { PostProcessingJobsActions } from '@jobs/store/post-processing-jobs/post-processing-jobs.actions';
import { SlidesActions } from '@slides/store';

@Injectable()
export class CollectionsEffects {
  clearOnSlideSelection$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(SlidesActions.selectSlideReady),
      map(() => CollectionsActions.clearCollections())
    );
  });

  loadCollection$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(CollectionsActions.loadCollection),
      fetch({
        id: (
          action: ReturnType<typeof CollectionsActions.loadCollection>,
          _state: State
        ) => {
          return action.collectionId + ' ' + action.type;
        },
        run: (
          action: ReturnType<typeof CollectionsActions.loadCollection>,
          _state: State
        ) => {
          return this.dataService
            .scopeIdCollectionsCollectionIdGet({
              scope_id: action.scopeId,
              collection_id: action.collectionId,
            })
            .pipe(
              retryWhen((errors) =>
                retryOnAction(
                  errors,
                  this.actions$,
                  TokenActions.setAccessToken,
                  requestNewToken
                )
              ),
              map((collection) =>
                CollectionsActions.loadCollectionSuccess({ collection })
              )
            );
        },
        onError: (
          _action: ReturnType<typeof CollectionsActions.loadCollection>,
          error
        ) => {
          return CollectionsActions.loadCollectionFailure({ error });
        },
      })
    );
  });

  loadCollections$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(CollectionsActions.loadCollections),
      mergeMap((action) =>
        action.collectionIds.map((collectionId) =>
          CollectionsActions.loadCollection({
            ...action,
            collectionId,
          })
        )
      )
    );
  });

  createCollection$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(CollectionsActions.createCollection),
      pessimisticUpdate({
        run: (
          action: ReturnType<typeof CollectionsActions.createCollection>,
          _state: State
        ) => {
          return this.dataService
            .scopeIdCollectionsPost({
              scope_id: action.scopeId,
              body: action.postCollection,
            })
            .pipe(
              retryWhen((errors) =>
                retryOnAction(
                  errors,
                  this.actions$,
                  TokenActions.setAccessToken,
                  requestNewToken
                )
              ),
              map((collection) =>
                CollectionsActions.createCollectionSuccess({
                  collection,
                  ...action,
                })
              )
            );
        },
        onError: (
          _action: ReturnType<typeof CollectionsActions.createCollection>,
          error
        ) => {
          return CollectionsActions.createCollectionFailure({ error });
        },
      })
    );
  });

  // add collection id to job input or output after collection was created
  addCollectionToJob$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(CollectionsActions.createCollectionSuccess),
      map((action) => {
        switch (action.keyType) {
          case JobKeyType.INPUT:
            return PostProcessingJobsActions.setIdToJobInput({
              ...action,
              inputKey: action.key,
              inputId: action.collection.id as string,
            });
          case JobKeyType.OUTPUT:
            return PostProcessingJobsActions.setIdToJobOutput({
              ...action,
              outputKey: action.key,
              outputId: action.collection.id as string,
            });
        }
      })
    );
  });

  addItemToCollection$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(CollectionsActions.addItemToCollection),
      pessimisticUpdate({
        run: (
          action: ReturnType<typeof CollectionsActions.addItemToCollection>,
          _state: State
        ) => {
          return this.dataService
            .scopeIdCollectionsCollectionIdItemsPost({
              scope_id: action.scopeId,
              collection_id: action.collectionId,
              body: action.postIdObject,
            })
            .pipe(
              retryWhen((errors) =>
                retryOnAction(
                  errors,
                  this.actions$,
                  TokenActions.setAccessToken,
                  requestNewToken
                )
              ),
              map(() => CollectionsActions.loadCollection({ ...action }))
            );
        },
        onError: (
          _action: ReturnType<typeof CollectionsActions.addItemToCollection>,
          error
        ) => {
          return CollectionsActions.addItemOrItemsToCollectionFailure({
            error,
          });
        },
      })
    );
  });

  addItemsToCollection$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(CollectionsActions.addItemsToCollection),
      pessimisticUpdate({
        run: (
          action: ReturnType<typeof CollectionsActions.addItemsToCollection>,
          _state: State
        ) => {
          return this.dataService
            .scopeIdCollectionsCollectionIdItemsPost({
              scope_id: action.scopeId,
              collection_id: action.collectionId,
              body: action.postIdObjects,
            })
            .pipe(
              retryWhen((errors) =>
                retryOnAction(
                  errors,
                  this.actions$,
                  TokenActions.setAccessToken,
                  requestNewToken
                )
              ),
              map(() => CollectionsActions.loadCollection({ ...action }))
            );
        },
        onError: (
          _action: ReturnType<typeof CollectionsActions.addItemsToCollection>,
          error
        ) => {
          return CollectionsActions.addItemOrItemsToCollectionFailure({
            error,
          });
        },
      })
    );
  });

  removeItemFromCollection$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(CollectionsActions.removeItemFromCollection),
      pessimisticUpdate({
        run: (
          action: ReturnType<
            typeof CollectionsActions.removeItemFromCollection
          >,
          _state: State
        ) => {
          return this.dataService
            .scopeIdCollectionsCollectionIdItemsItemIdDelete({
              scope_id: action.scopeId,
              collection_id: action.collectionId,
              item_id: action.itemId,
            })
            .pipe(
              retryWhen((errors) =>
                retryOnAction(
                  errors,
                  this.actions$,
                  TokenActions.setAccessToken,
                  requestNewToken
                )
              ),
              map(() => CollectionsActions.loadCollection({ ...action }))
            );
        },
        onError: (
          _action: ReturnType<
            typeof CollectionsActions.removeItemFromCollection
          >,
          error
        ) => {
          return CollectionsActions.removeItemFromCollectionFailure({ error });
        },
      })
    );
  });

  removeItemsFromCollection$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(CollectionsActions.removeItemsFromCollection),
      mergeMap((action) =>
        action.itemIds.map((itemId) =>
          CollectionsActions.removeItemFromCollection({
            ...action,
            itemId,
          })
        )
      )
    );
  });

  constructor(
    private readonly actions$: Actions,
    private readonly store: Store,
    private readonly dataService: AppV3DataService
  ) {}
}
