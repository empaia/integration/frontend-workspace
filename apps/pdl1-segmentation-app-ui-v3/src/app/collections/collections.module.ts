import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';
import { COLLECTIONS_MODULE_FEATURE_KEY, reducers } from './store/collections-feature.state';
import { EffectsModule } from '@ngrx/effects';
import { CollectionsEffects } from './store';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    StoreModule.forFeature(
      COLLECTIONS_MODULE_FEATURE_KEY,
      reducers
    ),
    EffectsModule.forFeature([
      CollectionsEffects,
    ])
  ]
})
export class CollectionsModule { }
