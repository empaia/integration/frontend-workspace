import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

@Component({
  selector: 'app-validation-label',
  templateUrl: './validation-label.component.html',
  styleUrls: ['./validation-label.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ValidationLabelComponent {
  @Input() public selected!: boolean;
  @Input() public contentLoaded!: boolean;
}
