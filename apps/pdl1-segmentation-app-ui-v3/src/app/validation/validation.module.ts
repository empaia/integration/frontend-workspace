import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ValidationLabelComponent } from './components/validation-label/validation-label.component';
import { SharedModule } from '@shared/shared.module';
import { ValidationContainerComponent } from './containers/validation-container/validation-container.component';
import { LetDirective, PushPipe } from '@ngrx/component';
import { JobsModule } from '@jobs/jobs.module';
import { AnnotationsModule } from '@annotations/annotations.module';
import { PrimitivesModule } from '@primitives/primitives.module';



@NgModule({
  declarations: [
    ValidationLabelComponent,
    ValidationContainerComponent
  ],
  imports: [
    CommonModule,
    LetDirective,
    PushPipe,
    AnnotationsModule,
    JobsModule,
    PrimitivesModule,
    SharedModule,
  ],
  exports: [
    ValidationLabelComponent,
    ValidationContainerComponent,
  ]
})
export class ValidationModule { }
