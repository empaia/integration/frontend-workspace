import { ChangeDetectionStrategy, Component } from '@angular/core';
import { MenuEntity } from '@menu/models/menu.models';
import { MenuSelectors } from '@menu/store';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-validation-container',
  templateUrl: './validation-container.component.html',
  styleUrls: ['./validation-container.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ValidationContainerComponent {
  public validationMenu$: Observable<MenuEntity>;

  constructor(private store: Store) {
    this.validationMenu$ = this.store.select(MenuSelectors.selectValidationMenu);
  }
}
