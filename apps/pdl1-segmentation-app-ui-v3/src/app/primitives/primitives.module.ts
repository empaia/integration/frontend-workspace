import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';
import { PRIMITIVES_MODULE_FEATURE_KEY,reducers } from './store/primitives-feature.state';
import { EffectsModule } from '@ngrx/effects';
import { PrimitivesEffects } from './store';
import { MaterialModule } from '@material/material.module';
import { PrimitiveContainerComponent } from './containers/primitive-container/primitive-container.component';
import { SharedModule } from '@shared/shared.module';
import { LetDirective, PushPipe } from '@ngrx/component';
import { ApproveControlComponent } from './components/approve-control/approve-control.component';
import { SlideControlComponent } from './components/slide-control/slide-control.component';



@NgModule({
  declarations: [
    PrimitiveContainerComponent,
    ApproveControlComponent,
    SlideControlComponent
  ],
  imports: [
    CommonModule,
    MaterialModule,
    SharedModule,
    PushPipe,
    LetDirective,
    StoreModule.forFeature(
      PRIMITIVES_MODULE_FEATURE_KEY,
      reducers,
    ),
    EffectsModule.forFeature([
      PrimitivesEffects,
    ])
  ],
  exports: [
    PrimitiveContainerComponent,
  ]
})
export class PrimitivesModule { }
