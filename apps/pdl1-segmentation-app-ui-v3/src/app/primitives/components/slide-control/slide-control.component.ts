import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-slide-control',
  templateUrl: './slide-control.component.html',
  styleUrls: ['./slide-control.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SlideControlComponent {
  @Input() public disabled!: boolean;
  @Input() public freezedSlideCount!: number;
  @Input() public totalSlideCount!: number;
  @Input() public hasChanges!: boolean;

  @Output() public nextSlide = new EventEmitter<void>();
  @Output() public applyChanges = new EventEmitter<void>();
}
