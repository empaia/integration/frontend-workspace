import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { Primitive } from '@primitives/store';

@Component({
  selector: 'app-approve-control',
  templateUrl: './approve-control.component.html',
  styleUrls: ['./approve-control.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ApproveControlComponent {
  @Input() public freeze: Primitive | undefined;

  @Output() public freezeChanged = new EventEmitter<boolean>();
  @Output() public showInfo = new EventEmitter<void>();
}
