import { Action, combineReducers, createFeatureSelector } from '@ngrx/store';
import * as fromPrimitives from '@primitives/store/primitives/primitives.reducer';

export const PRIMITIVES_MODULE_FEATURE_KEY = 'primitiveModuleFeature';

export const selectPrimitiveFeatureState = createFeatureSelector<State>(
  PRIMITIVES_MODULE_FEATURE_KEY
);

export interface State {
  [fromPrimitives.PRIMITIVES_FEATURE_KEY]: fromPrimitives.State;
}

export function reducers(state: State | undefined, action: Action) {
  return combineReducers({
    [fromPrimitives.PRIMITIVES_FEATURE_KEY]: fromPrimitives.reducer,
  })(state, action);
}
