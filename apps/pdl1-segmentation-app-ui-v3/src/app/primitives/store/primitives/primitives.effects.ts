import { Injectable } from '@angular/core';
import { AppV3DataService } from 'empaia-api-lib';
import { Actions, concatLatestFrom, createEffect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { PrimitivesActions } from './primitives.actions';
import { SlidesActions } from '@slides/store/slides/slides.actions';
import * as TokenActions from '@token/store/token/token.actions';
import {
  PostProcessingJobsActions,
  PostProcessingJobsSelectors,
} from '@jobs/store';
import * as PrimitivesSelectors from './primitives.selectors';
import * as ScopeSelectors from '@scope/store/scope/scope.selectors';
import { fetch, pessimisticUpdate } from '@ngrx/router-store/data-persistence';
import { State } from './primitives.reducer';
import { filter, map, mergeMap, retryWhen } from 'rxjs';
import { Primitive, PrimitiveList, PrimitiveType } from './primitives.models';
import { filterNullish, retryOnAction } from '@shared/helper/rxjs-operators';
import { requestNewToken } from 'vendor-app-communication-interface';
import { SelectedAnnotationsActions } from '@annotations/store';
import { EadService } from 'empaia-ui-commons';

@Injectable()
export class PrimitivesEffects {
  // clear primitive cache on slide selection
  clearPrimitiveCache$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(SlidesActions.selectSlideReady),
      map(() => PrimitivesActions.clearPrimitiveCache())
    );
  });

  // load all primitives when slides starts to load
  loadPrimitivesOfJobs$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(PostProcessingJobsActions.setPostProcessingJobs),
      concatLatestFrom(() => [
        this.store.select(ScopeSelectors.selectScopeId).pipe(filterNullish()),
        this.store.select(
          PostProcessingJobsSelectors.selectOutputKeyOfAllJobs('freeze')
        ),
      ]),
      filter(([, _scopeId, primitiveIds]) => !!primitiveIds?.length),
      map(([, scopeId, primitiveIds]) =>
        PrimitivesActions.loadPrimitives({
          scopeId,
          primitiveIds,
        })
      )
    );
  });

  loadPrimitives$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(PrimitivesActions.loadPrimitives),
      fetch({
        run: (
          action: ReturnType<typeof PrimitivesActions.loadPrimitives>,
          _state: State
        ) => {
          return this.dataService
            .scopeIdPrimitivesQueryPut({
              scope_id: action.scopeId,
              body: {
                primitives: action.primitiveIds,
              },
            })
            .pipe(
              retryWhen((errors) =>
                retryOnAction(
                  errors,
                  this.actions$,
                  TokenActions.setAccessToken,
                  requestNewToken
                )
              ),
              map((response) => response.items),
              map((primitives) =>
                PrimitivesActions.loadPrimitivesSuccess({ primitives })
              )
            );
        },
        onError: (
          _action: ReturnType<typeof PrimitivesActions.loadPrimitives>,
          error
        ) => {
          return PrimitivesActions.loadPrimitivesFailure({ error });
        },
      })
    );
  });

  createOnePrimitive$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(PrimitivesActions.createPrimitive),
      pessimisticUpdate({
        run: (
          action: ReturnType<typeof PrimitivesActions.createPrimitive>,
          _state: State
        ) => {
          return this.dataService
            .scopeIdPrimitivesPost({
              scope_id: action.scopeId,
              body: action.postPrimitive,
            })
            .pipe(
              retryWhen((errors) =>
                retryOnAction(
                  errors,
                  this.actions$,
                  TokenActions.setAccessToken,
                  requestNewToken
                )
              ),
              map((primitive) => primitive as Primitive),
              map((primitive) =>
                PrimitivesActions.createPrimitiveSuccess({
                  primitive,
                  jobId: action.jobId,
                })
              )
            );
        },
        onError: (
          _action: ReturnType<typeof PrimitivesActions.createPrimitive>,
          error
        ) => {
          return PrimitivesActions.createPrimitiveFailure({ error });
        },
      })
    );
  });

  createPrimitives$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(PrimitivesActions.createPrimitives),
      pessimisticUpdate({
        run: (
          action: ReturnType<typeof PrimitivesActions.createPrimitives>,
          _state: State
        ) => {
          return this.dataService
            .scopeIdPrimitivesPost({
              scope_id: action.scopeId,
              body: action.postPrimitives,
            })
            .pipe(
              retryWhen((errors) =>
                retryOnAction(
                  errors,
                  this.actions$,
                  TokenActions.setAccessToken,
                  requestNewToken
                )
              ),
              map((response) => (response as PrimitiveList).items),
              map((primitives) =>
                PrimitivesActions.createPrimitivesSuccess({ primitives })
              )
            );
        },
        onError: (
          _action: ReturnType<typeof PrimitivesActions.createPrimitives>,
          error
        ) => {
          return PrimitivesActions.createPrimitivesFailure({ error });
        },
      })
    );
  });

  deletePrimitive$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(PrimitivesActions.deletePrimitive),
      pessimisticUpdate({
        run: (
          action: ReturnType<typeof PrimitivesActions.deletePrimitive>,
          _state: State
        ) => {
          return this.dataService
            .scopeIdPrimitivesPrimitiveIdDelete({
              scope_id: action.scopeId,
              primitive_id: action.primitiveId,
            })
            .pipe(
              retryWhen((errors) =>
                retryOnAction(
                  errors,
                  this.actions$,
                  TokenActions.setAccessToken,
                  requestNewToken
                )
              ),
              map((response) => response.id),
              map((primitiveId) =>
                PrimitivesActions.deletePrimitiveSuccess({ primitiveId })
              )
            );
        },
        onError: (
          _action: ReturnType<typeof PrimitivesActions.deletePrimitive>,
          error
        ) => {
          return PrimitivesActions.deletePrimitiveFailure({ error });
        },
      })
    );
  });

  prepareUpdatePrimitive$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(PrimitivesActions.updatePrimitive),
      map((action) => action.primitiveValue),
      concatLatestFrom(() => [
        this.store.select(ScopeSelectors.selectScopeId).pipe(filterNullish()),
        this.store
          .select(
            PostProcessingJobsSelectors.selectAllPostProcessingJobsOfCurrentSlide
          )
          .pipe(
            filterNullish(),
            map((jobs) => jobs[0]?.id),
            filterNullish()
          ),
        this.store
          .select(PrimitivesSelectors.selectPrimitiveOfCurrentJob)
          .pipe(filterNullish()),
      ]),
      map(([value, scopeId, jobId, primitive]) =>
        PrimitivesActions.updatePrimitiveReady({
          scopeId,
          jobId,
          primitive,
          value,
        })
      )
    );
  });

  updatePrimitive$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(PrimitivesActions.updatePrimitiveReady),
      mergeMap((action) => [
        PrimitivesActions.createPrimitive({
          ...action,
          postPrimitive: {
            name: action.primitive.name,
            creator_id: action.primitive.creator_id,
            creator_type: action.primitive.creator_type,
            type: PrimitiveType.Bool,
            value: action.value as boolean,
          },
        }),
        PrimitivesActions.deletePrimitive({
          ...action,
          primitiveId: action.primitive.id as string,
        }),
      ])
    );
  });

  setPrimitiveCache$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(PrimitivesActions.setPrimitiveCache),
      map((action) => action.primitiveValue),
      concatLatestFrom(() => [
        this.store
          .select(PrimitivesSelectors.selectPrimitiveOfCurrentJob)
          .pipe(filterNullish()),
      ]),
      map(([primitiveValue, statePrimitive]) =>
        // clear cache if new value is the same as the value in the
        // original primitive (no changes to apply)
        primitiveValue !== statePrimitive.value
          ? PrimitivesActions.setPrimitiveCacheReady({
            primitiveCache: {
              ...statePrimitive,
              value: primitiveValue,
            } as Primitive,
          })
          : PrimitivesActions.clearPrimitiveCache()
      )
    );
  });

  applyChanges$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(SelectedAnnotationsActions.applyChanges),
      concatLatestFrom(() => [
        this.store
          .select(PrimitivesSelectors.selectPrimitiveCache)
          .pipe(filterNullish()),
      ]),
      map(([, cachePrimitive]) =>
        PrimitivesActions.updatePrimitive({
          primitiveValue: cachePrimitive.value,
        })
      )
    );
  });

  checkIfChangesArePersisted$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(PostProcessingJobsActions.updateJobOutputKeySuccess),
      concatLatestFrom(() => [
        this.store
          .select(PrimitivesSelectors.selectPrimitiveOfCurrentJob)
          .pipe(filterNullish()),
        this.store
          .select(PrimitivesSelectors.selectPrimitiveCache)
          .pipe(filterNullish()),
      ]),
      // clear primitive cache when the value of the new state and the value
      // of the cache are the same
      filter(
        ([, statePrimitive, cachePrimitive]) =>
          statePrimitive.value === cachePrimitive.value
      ),
      map(() => PrimitivesActions.clearPrimitiveCache())
    );
  });

  constructor(
    private readonly actions$: Actions,
    private readonly store: Store,
    private readonly dataService: AppV3DataService,
    private readonly eadService: EadService
  ) {}
}
