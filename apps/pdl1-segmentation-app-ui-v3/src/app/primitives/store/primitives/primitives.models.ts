import {
  AppV3BoolPrimitive,
  AppV3FloatPrimitive,
  AppV3IntegerPrimitive,
  AppV3PostBoolPrimitive,
  AppV3PostFloatPrimitive,
  AppV3PostIntegerPrimitive,
  AppV3PostStringPrimitive,
  AppV3StringPrimitive,
  AppV3PostBoolPrimitives,
  AppV3PostFloatPrimitives,
  AppV3PostIntegerPrimitives,
  AppV3PostStringPrimitives,
  AppV3PrimitiveList,
} from 'empaia-api-lib';
export { AppV3PrimitiveType as PrimitiveType } from 'empaia-api-lib';

export type BoolPrimitive = AppV3BoolPrimitive;
export type FloatPrimitive = AppV3FloatPrimitive;
export type IntegerPrimitive = AppV3IntegerPrimitive;
export type PostBoolPrimitive = AppV3PostBoolPrimitive;
export type PostFloatPrimitive = AppV3PostFloatPrimitive;
export type PostIntegerPrimitive = AppV3PostIntegerPrimitive;
export type PostStringPrimitive = AppV3PostStringPrimitive;
export type StringPrimitive = AppV3StringPrimitive;
export type PrimitiveList = AppV3PrimitiveList;

export type Primitive = IntegerPrimitive | FloatPrimitive | BoolPrimitive | StringPrimitive;
export type PostPrimitive = PostIntegerPrimitive | PostFloatPrimitive | PostBoolPrimitive | PostStringPrimitive;
export type PostPrimitives = AppV3PostBoolPrimitives | AppV3PostFloatPrimitives | AppV3PostIntegerPrimitives | AppV3PostStringPrimitives;

export interface PrimitiveSelector {
  id: string,
  value: boolean | number | string;
}

export type PrimitiveValue = number | boolean | string;
