import { createActionGroup, emptyProps, props } from '@ngrx/store';
import { PostPrimitive, PostPrimitives, Primitive, PrimitiveValue } from './primitives.models';
import { HttpErrorResponse } from '@angular/common/http';

export const PrimitivesActions = createActionGroup({
  source: 'Primitives',
  events: {
    'Load Primitives': props<{
      scopeId: string,
      primitiveIds: string[],
    }>(),
    'Load Primitives Success': props<{ primitives: Primitive[] }>(),
    'Load Primitives Failure': props<{ error: HttpErrorResponse }>(),
    'Create Primitive': props<{
      scopeId: string,
      postPrimitive: PostPrimitive,
      jobId: string,
    }>(),
    'Create Primitive Success': props<{ primitive: Primitive, jobId: string }>(),
    'Create Primitive Failure': props<{ error: HttpErrorResponse }>(),
    'Create Primitives': props<{ scopeId: string, postPrimitives: PostPrimitives }>(),
    'Create Primitives Success': props<{ primitives: Primitive[] }>(),
    'Create Primitives Failure': props<{ error: HttpErrorResponse }>(),
    'Delete Primitive': props<{ scopeId: string, primitiveId: string }>(),
    'Delete Primitive Success': props<{ primitiveId: string }>(),
    'Delete Primitive Failure': props<{ error: HttpErrorResponse }>(),
    'Update Primitive': props<{ primitiveValue: PrimitiveValue }>(),
    'Update Primitive Ready': props<{       // Update is implemented as a combination of delete and create a new primitive
      scopeId: string,
      jobId: string,
      primitive: Primitive,
      value: number | boolean | string
    }>(),
    'Set Primitive Cache': props<{
      primitiveValue: PrimitiveValue,
    }>(),
    'Set Primitive Cache Ready': props<{
      primitiveCache: Primitive,
    }>(),
    'Clear Primitive Cache': emptyProps(),
    'Clear Primitives': emptyProps(),
  }
});
