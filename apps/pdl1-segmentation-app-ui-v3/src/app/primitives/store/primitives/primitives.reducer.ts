import { EntityState, createEntityAdapter } from '@ngrx/entity';
import { createReducer, on } from '@ngrx/store';
import { Primitive } from './primitives.models';
import { HttpErrorResponse } from '@angular/common/http';
import { PrimitivesActions } from './primitives.actions';


export const PRIMITIVES_FEATURE_KEY = 'primitives';

export interface State extends EntityState<Primitive> {
  loaded: boolean;
  changesApplied: boolean;
  error?: HttpErrorResponse | undefined;
  primitiveCache?: Primitive | undefined;
}

export const primitiveAdapter = createEntityAdapter<Primitive>();

export const initialState: State = primitiveAdapter.getInitialState({
  loaded: true,
  changesApplied: true,
  error: undefined,
  primitiveCache: undefined,
});

export const reducer = createReducer(
  initialState,
  on(PrimitivesActions.loadPrimitives, (state): State => ({
    ...state,
    loaded: false,
  })),
  on(PrimitivesActions.loadPrimitivesSuccess, (state, { primitives }): State =>
    primitiveAdapter.setAll(primitives, {
      ...state,
      loaded: true,
    })
  ),
  on(PrimitivesActions.loadPrimitivesFailure, (state, { error }): State => ({
    ...state,
    loaded: true,
    error,
  })),
  on(PrimitivesActions.createPrimitive, (state): State => ({
    ...state,
    loaded: false,
  })),
  on(PrimitivesActions.createPrimitiveSuccess, (state, { primitive }): State =>
    primitiveAdapter.addOne(primitive, {
      ...state,
      loaded: true,
    })
  ),
  on(PrimitivesActions.createPrimitiveFailure, (state, { error }): State => ({
    ...state,
    loaded: true,
    error,
  })),
  on(PrimitivesActions.createPrimitives, (state): State => ({
    ...state,
    loaded: false,
  })),
  on(PrimitivesActions.createPrimitivesSuccess, (state, { primitives }): State =>
    primitiveAdapter.addMany(primitives, {
      ...state,
      loaded: true,
    })
  ),
  on(PrimitivesActions.createPrimitivesFailure, (state, { error }): State => ({
    ...state,
    loaded: true,
    error,
  })),
  on(PrimitivesActions.clearPrimitives, (state): State =>
    primitiveAdapter.removeAll({
      ...state,
      ...initialState,
    })
  ),
  on(PrimitivesActions.deletePrimitive, (state): State => ({
    ...state,
    loaded: false,
  })),
  on(PrimitivesActions.deletePrimitiveSuccess, (state, { primitiveId }): State =>
    primitiveAdapter.removeOne(primitiveId, {
      ...state,
      loaded: true,
    })
  ),
  on(PrimitivesActions.deletePrimitiveFailure, (state, { error }): State => ({
    ...state,
    loaded: true,
    error,
  })),
  on(PrimitivesActions.updatePrimitive, (state): State => ({
    ...state,
    changesApplied: false,
  })),
  on(PrimitivesActions.setPrimitiveCacheReady, (state, { primitiveCache }): State => ({
    ...state,
    primitiveCache,
  })),
  on(PrimitivesActions.clearPrimitiveCache, (state): State => ({
    ...state,
    primitiveCache: undefined,
    changesApplied: true,
  })),
);
