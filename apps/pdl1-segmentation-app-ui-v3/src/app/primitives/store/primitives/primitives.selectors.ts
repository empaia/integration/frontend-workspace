import { createSelector } from '@ngrx/store';
import { selectPrimitiveFeatureState } from '../primitives-feature.state';
import { primitiveAdapter, PRIMITIVES_FEATURE_KEY } from './primitives.reducer';
import {
  selectAllPostProcessingJobs,
  selectOutputCollectionIdOfCurrentSlideJob,
  selectPostProcessingJobEntities,
  selectPostProcessingJobsLoaded
} from '@jobs/store/post-processing-jobs/post-processing-jobs.selectors';
import { selectAllJobs, selectJobEntities, selectJobsLoaded } from '@jobs/store/jobs/jobs.selectors';
import { Primitive } from './primitives.models';
import { selectAllSlideIds, selectAllSlides, selectSelectedSlideIndex, selectSlidesLoaded } from '@slides/store/slides/slides.selectors';
import { selectAnnotationsViewerLoaded } from '@annotations/store/annotations-viewer/annotations-viewer.selectors';
import { selectHandDrawnAnnotationsLoaded } from '@annotations/store/hand-drawn-annotations/hand-drawn-annotations.selectors';
import { selectPreprocessingAnnotationsLoaded } from '@annotations/store/preprocessing-annotations/preprocessing-annotations.selectors';
import {
  selectAnnotationCacheIdsSize,
  selectSelectedAnnotationsLoaded,
  selectChangesPersisted as selectSelectionChangesPersisted
} from '@annotations/store/selected-annotations/selected-annotations.selectors';
import { selectPreprocessingJobsLoaded } from '@jobs/store/preprocessing-jobs/preprocessing-jobs.selectors';
import { Job } from '@jobs/store';

const {
  selectAll,
  selectEntities,
} = primitiveAdapter.getSelectors();

export const selectPrimitiveState = createSelector(
  selectPrimitiveFeatureState,
  (state) => state[PRIMITIVES_FEATURE_KEY]
);

export const selectAllPrimitives = createSelector(
  selectPrimitiveState,
  selectAll,
);

export const selectPrimitiveEntities = createSelector(
  selectPrimitiveState,
  selectEntities,
);

export const selectPrimitiveOfCurrentJob = createSelector(
  selectPrimitiveEntities,
  selectOutputCollectionIdOfCurrentSlideJob('freeze'),
  (entities, primitiveId) => primitiveId ? entities[primitiveId] : undefined
);

export const selectPrimitiveCache = createSelector(
  selectPrimitiveState,
  (state) => state.primitiveCache
);

export const selectPrimitiveCacheId = createSelector(
  selectPrimitiveCache,
  (primitiveCache) => primitiveCache?.id
);

export const selectChangesPersisted = createSelector(
  selectPrimitiveState,
  (state) => state.changesApplied
);

export const selectFilteredPrimitiveOfCurrentJob = createSelector(
  selectPrimitiveOfCurrentJob,
  selectPrimitiveCache,
  (statePrimitive, cachePrimitive) =>
    cachePrimitive ? cachePrimitive : statePrimitive
);

export const selectAllChangesPersisted = createSelector(
  selectChangesPersisted,
  selectSelectionChangesPersisted,
  (primitiveChanges, annotationChanges) => primitiveChanges && annotationChanges
);

export const selectAreChangesToApply = createSelector(
  selectPrimitiveCache,
  selectAnnotationCacheIdsSize,
  (primitiveCache, annotationCacheSize) => !!primitiveCache || !!annotationCacheSize
);

export const selectFreezedSlideIds = createSelector(
  selectAllPostProcessingJobs,
  selectPrimitiveEntities,
  (jobs, primitiveEntities) => jobs.filter(job => !!primitiveEntities[job.outputs['freeze']]?.value).map(job => job.inputs['my_wsi'])
);

export const selectNumberOfFreezedSlides = createSelector(
  selectFreezedSlideIds,
  (freezedSlideIds) => freezedSlideIds?.length
);

export const selectSlidePrimitiveEntities = createSelector(
  selectPrimitiveEntities,
  selectPostProcessingJobEntities,
  selectAllJobs,
  (primitiveEntities, postProcessingEntities, slideJobs) => {
    const slidePrimitiveEntities = new Map<string, Primitive>();

    slideJobs.forEach(slideJob => {
      const jobs = slideJob.jobIds.map(jobId => postProcessingEntities[jobId]).filter(jobs => !!jobs) as Job[];
      const primitive = primitiveEntities[jobs?.[0]?.outputs['freeze']];
      if (primitive) {
        slidePrimitiveEntities.set(slideJob.slideId, primitive);
      }
    });

    return slidePrimitiveEntities;
  }
);

// select all slides that doesn't have a post processing
// job or where the freeze primitive is set to false
// NOTE: this selected must be here because when we would
// move it to the SlidesSelectors, it would result in
// initializing errors (runtime errors)
export const selectAllUnapprovedSlideIds = createSelector(
  selectAllSlideIds,
  selectJobEntities,
  selectPostProcessingJobEntities,
  selectAllPostProcessingJobs,
  selectPrimitiveEntities,
  (slideIds, slideJobEntities, postProcessingEntities, postProcessingJobs, primitiveEntities) => {
    const noPostProcessing = slideIds
      .map(slideId => slideJobEntities[slideId])
      .filter(slideJob => !!slideJob && !slideJob.jobIds.filter(jobId => postProcessingEntities[jobId]).length)
      .map(slideJob => slideJob?.slideId) as string[];
    const notFrozen = postProcessingJobs.filter(job => !primitiveEntities[job.outputs['freeze']]?.value).map(job => job.inputs['my_wsi']);
    return notFrozen.concat(noPostProcessing);
  }
);

// export const selectNextUnapprovedSlideId = createSelector(
//   selectAllUnapprovedSlideIds,
//   (slideIds) => slideIds.length ? slideIds[0] : undefined
// );

export const selectNextUnapprovedSlideId = createSelector(
  selectSelectedSlideIndex,
  selectAllSlides,
  selectAllUnapprovedSlideIds,
  (index, slides, unapprovedSlideIds) => {
    if (!unapprovedSlideIds.length) { return undefined; }

    for (let i = index + 1; i < slides.length; i++) {
      if (unapprovedSlideIds.includes(slides[i].id)) {
        return slides[i].id;
      }
    }

    return undefined;
  }
);

export const selectPrimitivesLoaded = createSelector(
  selectPrimitiveState,
  (state) => state.loaded
);

export const selectPrimitiveError = createSelector(
  selectPrimitiveState,
  (state) => state.error
);

// check if all request are currently finished
// and no state is loading
export const selectIsAllLoaded = createSelector(
  selectAnnotationsViewerLoaded,
  selectHandDrawnAnnotationsLoaded,
  selectPreprocessingAnnotationsLoaded,
  selectSelectedAnnotationsLoaded,
  selectJobsLoaded,
  selectPostProcessingJobsLoaded,
  selectPreprocessingJobsLoaded,
  selectPrimitivesLoaded,
  selectSlidesLoaded,
  (
    viewerLoaded,
    handAnnotationsLoaded,
    preAnnotationsLoaded,
    selectedAnnotationsLoaded,
    jobsLoaded,
    postJobsLoaded,
    preJobsLoaded,
    primitivesLoaded,
    slidesLoaded
  ) => viewerLoaded
  && handAnnotationsLoaded
  && preAnnotationsLoaded
  && selectedAnnotationsLoaded
  && jobsLoaded
  && postJobsLoaded
  && preJobsLoaded
  && primitivesLoaded
  && slidesLoaded
);
