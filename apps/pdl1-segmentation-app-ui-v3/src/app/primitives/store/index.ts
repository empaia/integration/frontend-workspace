import { PrimitivesActions } from './primitives/primitives.actions';
import * as PrimitivesFeature from './primitives/primitives.reducer';
import * as PrimitivesSelectors from './primitives/primitives.selectors';
export * from './primitives/primitives.effects';
export * from './primitives/primitives.models';

export {
  PrimitivesActions,
  PrimitivesFeature,
  PrimitivesSelectors,
};
