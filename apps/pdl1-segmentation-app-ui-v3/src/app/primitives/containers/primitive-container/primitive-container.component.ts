import { ChangeDetectionStrategy, Component } from '@angular/core';
import { SelectedAnnotationsActions } from '@annotations/store';
import { MenuActions } from '@menu/store';
import { INFO_DIALOG_DATA, SMALL_DIALOG_WIDTH } from '@menu/store/menu/menu.models';
import { Store } from '@ngrx/store';
import { Primitive, PrimitivesActions, PrimitivesSelectors } from '@primitives/store';
import { SlidesSelectors } from '@slides/store';
import { SlidesActions } from '@slides/store/slides/slides.actions';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-primitive-container',
  templateUrl: './primitive-container.component.html',
  styleUrls: ['./primitive-container.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PrimitiveContainerComponent {
  public freeze$: Observable<Primitive| undefined>;
  public freezedSlides$: Observable<number>;
  public maxSlidesCount$: Observable<number>;
  public allRequestsDone$: Observable<boolean>;
  public changesToApply$: Observable<boolean>;

  constructor(private store: Store) {
    this.freeze$ = this.store.select(PrimitivesSelectors.selectFilteredPrimitiveOfCurrentJob);
    this.freezedSlides$ = this.store.select(PrimitivesSelectors.selectNumberOfFreezedSlides);
    this.maxSlidesCount$ = this.store.select(SlidesSelectors.selectMaxSlidesCount);
    this.allRequestsDone$ = this.store.select(PrimitivesSelectors.selectIsAllLoaded);
    this.changesToApply$ = this.store.select(PrimitivesSelectors.selectAreChangesToApply);
  }

  public onFreezeChanged(primitiveValue: boolean): void {
    this.store.dispatch(PrimitivesActions.setPrimitiveCache({ primitiveValue }));
  }

  public onNextSlideClicked(): void {
    this.store.dispatch(SlidesActions.selectNextUnapprovedSlide());
  }

  public onInfoClicked(): void {
    this.store.dispatch(MenuActions.openInfoDialog({
      infoData: INFO_DIALOG_DATA,
      size: SMALL_DIALOG_WIDTH
    }));
  }

  public onApplyChanges(): void {
    this.store.dispatch(SelectedAnnotationsActions.applyChanges());
  }
}
