import { AppV3ExtendedScope } from 'empaia-api-lib';
import { EXTENDED_SCOPE_LOAD_ERROR } from '@menu/models/ui.models';

export type ExtendedScope = AppV3ExtendedScope;

export const ScopeErrorMap = {
  '[APP/Scope] Load Extended Scope Failure': EXTENDED_SCOPE_LOAD_ERROR,
};
