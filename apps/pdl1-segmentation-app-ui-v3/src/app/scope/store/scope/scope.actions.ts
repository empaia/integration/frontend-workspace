import { HttpErrorResponse } from '@angular/common/http';
import { createAction, props } from '@ngrx/store';
import { ExtendedScope } from './scope.models';

export const setScope = createAction(
  '[APP/Scope] Set Scope',
  props<{ scopeId: string }>()
);

export const loadExtendedScope = createAction(
  '[APP/Scope] Load Extended Scope',
  props<{ scopeId: string }>()
);

export const loadExtendedScopeSuccess = createAction(
  '[APP/Scope] Load Extended Scope Success',
  props<{ extendedScope: ExtendedScope }>()
);

export const loadExtendedScopeFailure = createAction(
  '[APP/Scope] Load Extended Scope Failure',
  props<{ error: HttpErrorResponse }>()
);
