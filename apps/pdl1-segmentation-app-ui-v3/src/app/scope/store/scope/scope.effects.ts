import { map, filter, distinctUntilChanged, exhaustMap } from 'rxjs/operators';
import { Store } from '@ngrx/store';
import { AppV3ScopeService } from 'empaia-api-lib';
import { fetch } from '@ngrx/router-store/data-persistence';
import { ScopeService } from '@scope/services/scope.service';
import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType, concatLatestFrom } from '@ngrx/effects';
import { ScopeFeature } from '..';
import * as ScopeActions from './scope.actions';
import * as ScopeSelectors from './scope.selectors';
import * as TokenActions from '@token/store/token/token.actions';
import * as TokenSelectors from '@token/store/token/token.selectors';
import { filterNullish } from '@shared/helper/rxjs-operators';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ErrorSnackbarComponent } from '@shared/components/error-snackbar/error-snackbar.component';
import { ScopeErrorMap } from './scope.models';
import { of } from 'rxjs';

@Injectable()
export class ScopeEffects {
  setScopeId$ = createEffect(
    () => {
      return this.actions$.pipe(
        ofType(ScopeActions.setScope),
        map((action) => action.scopeId),
        map((scopeId) => (this.scopeService.scopeId = scopeId))
      );
    },
    { dispatch: false }
  );

  startLoadingExtendedScope$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ScopeActions.setScope, TokenActions.setAccessToken),
      concatLatestFrom(() => [
        this.store.select(ScopeSelectors.selectScopeId).pipe(filterNullish()),
        this.store.select(TokenSelectors.selectAccessToken),
      ]),
      filter(([, _scopeId, token]) => !!token),
      distinctUntilChanged((prev, curr) => prev[1] === curr[1]),
      map(([, scopeId]) => scopeId),
      map((scopeId) => ScopeActions.loadExtendedScope({ scopeId }))
    );
  });

  loadExtendedScope$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ScopeActions.loadExtendedScope),
      fetch({
        run: (
          action: ReturnType<typeof ScopeActions.loadExtendedScope>,
          _state: ScopeFeature.State
        ) => {
          return this.scopePanelService
            .scopeIdGet({
              scope_id: action.scopeId,
            })
            .pipe(
              map((extendedScope) =>
                ScopeActions.loadExtendedScopeSuccess({ extendedScope })
              )
            );
        },
        onError: (
          _action: ReturnType<typeof ScopeActions.loadExtendedScope>,
          error
        ) => {
          return ScopeActions.loadExtendedScopeFailure({ error });
        },
      })
    );
  });

  showErrorMessage$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ScopeActions.loadExtendedScopeFailure),
      exhaustMap((action) => {
        this.snackBar.openFromComponent(ErrorSnackbarComponent, {
          data: { title: ScopeErrorMap[action.type], error: action.error },
        });
        return of({ type: 'noop' });
      })
    );
  });

  constructor(
    private readonly actions$: Actions,
    private readonly scopeService: ScopeService,
    private readonly scopePanelService: AppV3ScopeService,
    private readonly store: Store,
    private readonly snackBar: MatSnackBar
  ) {}
}
