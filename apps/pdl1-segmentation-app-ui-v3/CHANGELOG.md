# PDL1 Segmentation App Ui V3

# 1.4.4 (2024-07-16)

### Refactor

* upgraded to angular 17

# 1.4.2 (2024-01-11)

### Refactor

* updated to angular 16

# 1.4.1 (2024-01-05)

### Refactor

* changed annotation loading spinner position and appearance

# 1.4.0 (2023-12-22)

### Features

* added support for annotation rendering hints of ead

# 1.3.3 (2023-09-18)

### Refactor

* positioned tooltips above mouse cursor when ever possible

# 1.3.0 (2023-08-15)

### Feature

* uses cache for selected annotations and freeze primitive to increase performance

# 1.2.0 (2023-08-02)

### Refactor

* optimized slide pagination speed

# 1.1.0 (2023-06-12)

### Features

* preloads all fonts from the start

# 1.0.2 (2023-06-08)

### Refactor

* refactored tooltips

# 1.0.0 (2023-06-06)

### Fixes

* no broken post processing jobs state, due to check-up and repair effects and actions
* fast selection doesn't resolve in empty collections any more (fixed race conditions)
* first preprocessing job will always be auto selected (fixed race condition) 
* "next" button always jumps forward to the next unapproved slide and not back

### Features

* added frontend pagination for slide-tray-view in pdl1 and wbc 3
* added frontend pagination for slide-list in wbc3 and gau v3
* added vertex correction for out-of-bounds vertices for polygons
* preprocessed annotations can be hidden by the user
* "next" button is clickable when all requests are done (no need to approve first)
* refactored "next" button location in validation panel and slide-tray-view


# 0.0.1

* initial commit
