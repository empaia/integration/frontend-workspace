import { SpectatorService, createServiceFactory } from '@ngneat/spectator';
import { WbsUrlService } from './wbs-url.service';

describe('WbsUrlService', () => {
  let spectator: SpectatorService<WbsUrlService>;
  const createService = createServiceFactory({
    service: WbsUrlService,
    providers: [
      {
        provide: 'API_VERSION', useValue: ''
      }
    ]
  });

  beforeEach(() => spectator = createService());

  it('should be created', () => {
    expect(spectator.service).toBeTruthy();
  });
});
