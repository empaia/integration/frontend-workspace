import {
  AppV3ArrowAnnotation,
  AppV3CircleAnnotation,
  AppV3LineAnnotation,
  AppV3PointAnnotation,
  AppV3PolygonAnnotation,
  AppV3RectangleAnnotation,
  AppV3PostArrowAnnotation,
  AppV3PostCircleAnnotation,
  AppV3PostLineAnnotation,
  AppV3PostPointAnnotation,
  AppV3PostPolygonAnnotation,
  AppV3PostRectangleAnnotation,
} from 'empaia-api-lib';
export {
  AppV3AnnotationType as AnnotationApiType,
  AppV3AnnotationReferenceType as AnnotationReferenceType
} from 'empaia-api-lib';

export type ArrowAnnotation = AppV3ArrowAnnotation;
export type CircleAnnotation = AppV3CircleAnnotation;
export type LineAnnotation = AppV3LineAnnotation;
export type PointAnnotation = AppV3PointAnnotation;
export type PolygonAnnotation = AppV3PolygonAnnotation;
export type RectangleAnnotation = AppV3RectangleAnnotation;
export type PostArrowAnnotation = AppV3PostArrowAnnotation;
export type PostCircleAnnotation = AppV3PostCircleAnnotation;
export type PostLineAnnotation = AppV3PostLineAnnotation;
export type PostPointAnnotation = AppV3PostPointAnnotation;
export type PostPolygonAnnotation = AppV3PostPolygonAnnotation;
export type PostRectangleAnnotation = AppV3PostRectangleAnnotation;

export const INPUT_ANNOTATION_LIMIT = 10000;

export type ApiAnnotation = PointAnnotation | LineAnnotation | ArrowAnnotation | CircleAnnotation | RectangleAnnotation | PolygonAnnotation;
