import { Injectable } from '@angular/core';
import { Actions, concatLatestFrom, createEffect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { fetch } from '@ngrx/router-store/data-persistence';
import { AppV3DataService } from 'empaia-api-lib';
import { AnnotationConversionService } from '@annotations/services/annotation-conversion.service';
import { AnnotationsFeature } from '..';
import * as AnnotationsActions from '@annotations/store/annotations/annotations.actions';
import * as AnnotationsViewerActions from '@annotations/store/annotations-viewer/annotations-viewer.actions';
import * as SlidesActions from '@slides/store/slides/slides.actions';
import * as JobsActions from '@jobs/store/jobs/jobs.actions';
import * as SlidesSelectors from '@slides/store/slides/slides.selectors';
import * as ScopeSelectors from '@scope/store/scope/scope.selectors';
import * as TokenActions from '@token/store/token/token.actions';
import {
  distinctUntilChanged,
  filter,
  map,
  mergeMap,
  retryWhen,
} from 'rxjs/operators';
import { AnnotationEntity } from 'slide-viewer';
import { filterNullish, retryOnAction } from '@shared/helper/rxjs-operators';
import { INPUT_ANNOTATION_LIMIT } from '@annotations/store/annotations/annotations.models';
import { requestNewToken } from 'vendor-app-communication-interface';
import { compareJobStatus } from '@jobs/store/jobs/jobs.models';

@Injectable()
export class AnnotationsEffects {
  // clear annotations on slide selection
  clearAnnotationsOnSlideSelection$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(SlidesActions.selectSlide, SlidesActions.clearSlides),
      map(() => AnnotationsActions.clearAnnotations())
    );
  });

  // load annotations when job list was fetched
  loadAnnotationsAfterJobs$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(JobsActions.loadJobsSuccess),
      map((action) => action.jobs),
      distinctUntilChanged(compareJobStatus),
      concatLatestFrom(() => [
        this.store.select(ScopeSelectors.selectScopeId).pipe(filterNullish()),
        this.store
          .select(SlidesSelectors.selectSelectedSlideId)
          .pipe(filterNullish()),
      ]),
      filter(([jobs]) => !!jobs?.length),
      map(([jobs, scopeId, slideId]) => {
        const annotationIds = jobs
          .filter((job) => job.inputs['my_wsi'] === slideId)
          .map((job) => job.inputs['my_rectangle'])
          .filter((annotationId) => !!annotationId);

        return annotationIds?.length
          ? AnnotationsActions.loadInputAnnotations({
            scopeId,
            withClasses: true,
            skip: 0,
            limit: INPUT_ANNOTATION_LIMIT,
            annotationIds,
          })
          : { type: 'noop' };
      })
    );
  });

  // add annotation after creation
  addAnnotation$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(AnnotationsViewerActions.createAnnotationSuccess),
      map((action) => action.annotation),
      map((annotation) => AnnotationsActions.addAnnotation({ annotation }))
    );
  });

  loadAnnotations$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(AnnotationsActions.loadInputAnnotations),
      fetch({
        run: (
          action: ReturnType<typeof AnnotationsActions.loadInputAnnotations>,
          _state: AnnotationsFeature.State
        ) => {
          return this.dataService
            .scopeIdAnnotationsQueryPut({
              scope_id: action.scopeId,
              with_classes: action.withClasses,
              skip: action.skip,
              limit: action.limit,
              body: {
                annotations: action.annotationIds,
              },
            })
            .pipe(
              retryWhen((errors) =>
                retryOnAction(
                  errors,
                  this.actions$,
                  TokenActions.setAccessToken,
                  requestNewToken
                )
              ),
              map((response) => ({
                annotations: response.items.map(
                  (a) =>
                    this.annotationConversion.fromApiType(a) as AnnotationEntity
                ),
                itemCount: response.item_count,
              })),
              mergeMap((response) => [
                AnnotationsActions.loadInputAnnotationsSuccess({
                  annotations: response.annotations,
                }),
                response.itemCount > action.skip + action.limit
                  ? AnnotationsActions.loadInputAnnotations({
                    ...action,
                    skip: action.skip + action.limit,
                  })
                  : { type: 'noop' },
              ])
            );
        },
        onError: (
          _action: ReturnType<typeof AnnotationsActions.loadInputAnnotations>,
          error
        ) => {
          return AnnotationsActions.loadInputAnnotationsFailure({ error });
        },
      })
    );
  });

  constructor(
    private readonly actions$: Actions,
    private readonly store: Store,
    private readonly dataService: AppV3DataService,
    private readonly annotationConversion: AnnotationConversionService
  ) {}
}
