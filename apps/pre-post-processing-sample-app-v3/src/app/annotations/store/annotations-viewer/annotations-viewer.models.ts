export const NPP_MIN_RANGE_FACTOR = 0.75;
export const NPP_MAX_RANGE_FACTOR = 1.5;

export const ANNOTATION_QUERY_LIMIT = 10000;
