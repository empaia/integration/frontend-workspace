import { AppV3Viewport } from 'empaia-api-lib';

export type Viewport = AppV3Viewport;

export interface CurrentView {
  viewport: Viewport;
  nppCurrent: number;
  nppBase: number;
}
