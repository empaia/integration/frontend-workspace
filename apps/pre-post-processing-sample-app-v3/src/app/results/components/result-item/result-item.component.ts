import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  OnChanges,
  Output,
  SimpleChanges
} from '@angular/core';
import { FINISHED_JOB_STATES, ResultEntity } from '@results/store/results/results.models';
import { MatCheckboxChange } from '@angular/material/checkbox';
import { JobSelector } from '@jobs/store/jobs/jobs.models';
import { AnnotationEntity } from 'slide-viewer';


@Component({
  selector: 'app-result-item',
  templateUrl: './result-item.component.html',
  styleUrls: ['./result-item.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ResultItemComponent implements OnChanges {
  @Input() result!: ResultEntity;
  @Input() selected!: boolean;
  @Input() version!: string;

  @Output() changeSelection = new EventEmitter<JobSelector>();
  @Output() zoomToAnnotation = new EventEmitter<AnnotationEntity>();
  @Output() interruptJobExecution = new EventEmitter<string>();
  classVersion = 'v1';
  readonly FINISHED_JOB_STATES = FINISHED_JOB_STATES;

  ngOnChanges(changes: SimpleChanges) {
    if (changes.version) {
      this.classVersion = this.version === 'v2' ? this.version : 'v3.0';
    }
  }

  onCheckboxChange(change: MatCheckboxChange): void {
    const jobSelector: JobSelector = {
      id: this.result.job.id,
      checked: change.checked,
    };
    this.changeSelection.emit(jobSelector);
  }

  onJobStop(id: string): void {
    this.interruptJobExecution.emit(id);
  }
}
