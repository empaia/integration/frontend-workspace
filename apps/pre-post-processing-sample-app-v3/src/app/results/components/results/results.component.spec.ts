import { ResultsComponent } from './results.component';
import { createComponentFactory, Spectator } from '@ngneat/spectator';
import { MockComponents } from 'ng-mocks';
import { ResultItemComponent } from '@results/components/result-item/result-item.component';
import {
  MissingSelectionErrorComponent
} from '@shared/components/missing-selection-error/missing-selection-error.component';
import { MaterialModule } from '@material/material.module';

describe('ResultsComponent', () => {
  let spectator: Spectator<ResultsComponent>;
  const createComponent = createComponentFactory({
    component: ResultsComponent,
    imports: [
      MaterialModule,
    ],
    declarations: [
      MockComponents(
        ResultItemComponent,
        MissingSelectionErrorComponent,
      )
    ]
  });

  beforeEach(() => spectator = createComponent());

  it('should create', () => {
    expect(spectator.component).toBeTruthy();
  });
});
