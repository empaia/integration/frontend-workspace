import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { SelectionError } from '@menu/models/ui.model';
import { MenuType } from '@menu/models/menu.models';
import { ResultEntity } from '@results/store/results/results.models';
import { Dictionary } from '@ngrx/entity';
import { Job, JobMode, JobSelector } from '@jobs/store/jobs/jobs.models';
import { AnnotationEntity } from 'slide-viewer';
import { MatTabChangeEvent } from '@angular/material/tabs';

@Component({
  selector: 'app-results',
  templateUrl: './results.component.html',
  styleUrls: ['./results.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ResultsComponent {
  @Input() public standaloneResults!: ResultEntity[];
  @Input() public postprocessingResults!: ResultEntity[];
  @Input() public preprocessingJob!: Job;
  @Input() public jobsSelections!: Dictionary<JobSelector>;
  @Input() public version!: string;
  @Input() public selectionMissing!: SelectionError;

  public readonly STANDALONE_MODE = JobMode.Standalone;
  public readonly POSTPROCESSING_MODE = JobMode.Postprocessing;

  @Output() public zoomToAnnotation = new EventEmitter<AnnotationEntity>();
  @Output() public changeJobSelection = new EventEmitter<JobSelector>();
  @Output() public changeJobMode = new EventEmitter<JobMode>();
  @Output() public errorClicked = new EventEmitter<MenuType>();
  @Output() public interruptJobExecution = new EventEmitter<string>();

  public onTabChanged(event: MatTabChangeEvent): void {
    this.changeJobMode.emit(event.tab.ariaLabel as JobMode);
  }
}
