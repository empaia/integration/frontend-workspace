import { Injectable } from '@angular/core';
import { Actions, concatLatestFrom, createEffect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { fetch } from '@ngrx/router-store/data-persistence';
import { AppV3DataService } from 'empaia-api-lib';
import { ResultsFeature } from '..';
import * as ResultsActions from './results.actions';
import * as SlidesActions from '@slides/store/slides/slides.actions';
import * as AnnotationsActions from '@annotations/store/annotations/annotations.actions';
import * as ScopeSelectors from '@scope/store/scope/scope.selectors';
import * as AnnotationsSelectors from '@annotations/store/annotations/annotations.selectors';
import * as JobsSelectors from '@jobs/store/jobs/jobs.selectors';
import { filter, map } from 'rxjs/operators';
import { filterNullish } from '@shared/helper/rxjs-operators';
import { EadService } from 'empaia-ui-commons';

@Injectable()
export class ResultsEffects {
  // clear results on slide selection
  clearResultsOnSlideSelection$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(SlidesActions.selectSlide, SlidesActions.clearSlides),
      map(() => ResultsActions.clearResults())
    );
  });

  // load primitive results after input annotations were fetched
  loadPrimitives$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(AnnotationsActions.loadInputAnnotationsSuccess),
      concatLatestFrom(() => [
        this.store.select(ScopeSelectors.selectScopeId).pipe(filterNullish()),
        this.store
          .select(AnnotationsSelectors.selectAllAnnotationIds)
          .pipe(filterNullish()),
        this.store.select(JobsSelectors.selectAllJobIds),
      ]),
      filter(
        ([, _scopeId, annotationIds, jobIds]) =>
          !!jobIds.length && !!annotationIds.length
      ),
      map(([, scopeId, annotationIds, jobIds]) =>
        ResultsActions.loadResults({
          scopeId,
          annotationIds,
          jobIds,
        })
      )
    );
  });

  loadResults$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ResultsActions.loadResults),
      fetch({
        run: (
          action: ReturnType<typeof ResultsActions.loadResults>,
          _state: ResultsFeature.State
        ) => {
          return this.dataService
            .scopeIdPrimitivesQueryPut({
              scope_id: action.scopeId,
              body: {
                references: action.annotationIds,
                jobs: action.jobIds,
              },
            })
            .pipe(
              map((results) => results.items),
              map((results) => ResultsActions.loadResultsSuccess({ results }))
            );
        },
        onError: (
          _action: ReturnType<typeof ResultsActions.loadResults>,
          error
        ) => {
          return ResultsActions.loadResultsFailure({ error });
        },
      })
    );
  });

  constructor(
    private readonly actions$: Actions,
    private readonly store: Store,
    private readonly dataService: AppV3DataService,
    private readonly eadService: EadService
  ) {}
}
