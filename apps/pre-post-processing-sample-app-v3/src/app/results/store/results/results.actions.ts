import { createAction, props } from '@ngrx/store';
import { HttpError } from '@menu/models/ui.model';
import { PrimitiveEntity } from '@results/store/results/results.models';

export const loadResults = createAction(
  '[APP/Results] Load Results',
  props<{
    scopeId: string,
    annotationIds: string[],
    jobIds: string[],
  }>()
);

export const loadResultsSuccess = createAction(
  '[APP/Results] Load Results Success',
  props<{ results: PrimitiveEntity[] }>()
);

export const loadResultsFailure = createAction(
  '[APP/Results] Load Results Failure',
  props<{ error: HttpError }>()
);

export const clearResults = createAction(
  '[APP/Results] Clear Results',
);
