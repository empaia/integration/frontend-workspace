import { AnnotationEntity } from 'slide-viewer';
import {
  AppV3IntegerPrimitive,
  AppV3FloatPrimitive,
  AppV3BoolPrimitive,
  AppV3StringPrimitive,
} from 'empaia-api-lib';
import { Job, JobStatus } from '@jobs/store/jobs/jobs.models';

export type IntegerPrimitive = AppV3IntegerPrimitive;
export type FloatPrimitive = AppV3FloatPrimitive;
export type BoolPrimitive = AppV3BoolPrimitive;
export type StringPrimitive = AppV3StringPrimitive;

export type PrimitiveEntity = IntegerPrimitive | FloatPrimitive | BoolPrimitive | StringPrimitive;

export interface ResultEntity {
  job: Job;
  annotation: AnnotationEntity;
  primitives: PrimitiveEntity[];
}

export const FINISHED_JOB_STATES = [JobStatus.Completed, JobStatus.Error, JobStatus.Failed, JobStatus.Incomplete, JobStatus.Timeout];
