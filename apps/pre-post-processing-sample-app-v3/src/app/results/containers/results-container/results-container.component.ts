import { ChangeDetectionStrategy, Component, Inject } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { MenuEntity, MenuType } from '@menu/models/menu.models';
import { MenuActions, MenuSelectors } from '@menu/store';
import { ResultEntity } from '@results/store/results/results.models';
import { Job, JobMode, JobSelector } from '@jobs/store/jobs/jobs.models';
import { ResultsSelectors } from '@results/store';
import { JobsActions, JobsSelectors } from '@jobs/store';
import { Dictionary } from '@ngrx/entity';
import { AnnotationEntity } from 'slide-viewer';
import { AnnotationsViewerActions } from '@annotations/store';

@Component({
  selector: 'app-results-container',
  templateUrl: './results-container.component.html',
  styleUrls: ['./results-container.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ResultsContainerComponent {
  public resultsMenu$: Observable<MenuEntity>;
  public standaloneResults$: Observable<ResultEntity[]>;
  public postprocessingResults$: Observable<ResultEntity[]>;
  public preprocessingJob$: Observable<Job | undefined>;
  public resultsLoaded$: Observable<boolean>;
  public jobsSelections$: Observable<Dictionary<JobSelector>>;
  public allJobsSelected$: Observable<boolean>;

  constructor(
    private store: Store,
    @Inject('API_VERSION') public apiVersion: string,
  ) {
    this.resultsMenu$ = this.store.select(MenuSelectors.selectResultsMenu);
    this.standaloneResults$ = this.store.select(ResultsSelectors.selectAllStandaloneResults);
    this.postprocessingResults$ = this.store.select(ResultsSelectors.selectAllPostprocessingResults);
    this.preprocessingJob$ = this.store.select(JobsSelectors.selectLatestPreprocessingJob);
    this.resultsLoaded$ = this.store.select(ResultsSelectors.selectResultsLoaded);
    this.jobsSelections$ = this.store.select(JobsSelectors.selectJobSelectionEntities);
    this.allJobsSelected$ = this.store.select(JobsSelectors.selectAllSelectedState);
  }

  public onZoomToAnnotation(focus: AnnotationEntity): void {
    this.store.dispatch(AnnotationsViewerActions.zoomToAnnotation({ focus }));
  }

  public onJobSelectionChanged(jobSelection: JobSelector): void {
    this.store.dispatch(JobsActions.setJobSelection({ jobSelection }));
  }

  public onShowAll(show: boolean): void {
    if (show) {
      this.store.dispatch(JobsActions.showAllJobs());
    } else {
      this.store.dispatch(JobsActions.hideAllJobs());
    }
  }

  public onJobInterruption(jobId: string): void {
    this.store.dispatch(JobsActions.stopRunningJob({ jobId }));
  }

  public onJobModeChange(jobMode: JobMode): void {
    this.store.dispatch(JobsActions.setJobMode({ jobMode }));
  }

  public onClickError(menuType: MenuType): void {
    this.store.dispatch(MenuActions.openMenu({ id: menuType }));
  }
}
