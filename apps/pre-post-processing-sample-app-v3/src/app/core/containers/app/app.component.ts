import { Component, NgZone, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { SlidesSelectors } from '@slides/store';
import { MenuActions } from '@menu/store';
import { Annotation, AnnotationHighlightConfig, CurrentView, HighlightType, UiConfig } from 'slide-viewer';
import {
  AnnotationsViewerActions,
  AnnotationsViewerSelectors,
  AnnotationsUiActions,
  AnnotationsUiSelectors,
  AnnotationsColorMapsSelectors,
} from '@annotations/store';
import { observeOn } from 'rxjs/operators';
import { asyncScheduler } from 'rxjs';
import {
  Scope,
  Token,
  WbsUrl,
  addScopeListener,
  addTokenListener,
  addWbsUrlListener,
} from 'vendor-app-communication-interface';
import { ScopeActions, ScopeSelectors } from '@scope/store';
import { TokenActions } from '@token/store';
import { WbsUrlActions } from '@wbsUrl/store';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  public slide$ = this.store.select(SlidesSelectors.selectSelectedSlideImageView);
  public selectedSlideId$ = this.store.select(SlidesSelectors.selectSelectedSlideId);
  public annotationIds$ = this.store.select(AnnotationsViewerSelectors.selectAllAnnotationViewerIds);
  public annotations$ = this.store.select(AnnotationsViewerSelectors.selectAllViewerAnnotations);
  public annotationLoaded$ = this.store.select(AnnotationsViewerSelectors.selectAnnotationsViewerLoaded).pipe(
    observeOn(asyncScheduler)
  );
  public centroids$ = this.store.select(AnnotationsViewerSelectors.selectAnnotationsViewerCentroids);
  public allowedInteractionTypes$ = this.store.select(AnnotationsUiSelectors.selectAllowedInteractionTypes);
  public selectedInteractionType$ = this.store.select(AnnotationsUiSelectors.selectInteractionType);
  public focusAnnotation$ = this.store.select(AnnotationsViewerSelectors.selectFocusedViewerAnnotationId);
  public hiddenAnnotationIds$ = this.store.select(AnnotationsViewerSelectors.selectHiddenAnnotationViewerIds);
  public removeAnnotations$ = this.store.select(AnnotationsViewerSelectors.selectRemovedAnnotationViewerIds);
  public clearAnnotations$ = this.store.select(AnnotationsViewerSelectors.selectAnnotationViewerClearState);
  public examinationState$ = this.store.select(ScopeSelectors.selectExaminationState);
  public classColorMapping$ = this.store.select(AnnotationsColorMapsSelectors.selectSelectedAnnotationClassColorMapping);

  public uiConfig: UiConfig = {
    showAnnotationBar: true,
    autoSetAnnotationTitle: false,
    renderHideNavigationButton: true,
  };

  public annotationHighlightConfig: AnnotationHighlightConfig = {
    highlightType: HighlightType.CLICK
  };

  public readonly CLUSTER_DISTANCE = 60;

  constructor(
    private store: Store,
    private ngZone: NgZone
  ) { }

  ngOnInit(): void {
    addTokenListener((token: Token) => {
      this.ngZone.run(() => {
        this.store.dispatch(TokenActions.setAccessToken({ token }));
      });
    });

    addScopeListener((scope: Scope) => {
      this.ngZone.run(() => {
        this.store.dispatch(ScopeActions.setScope({ scopeId: scope.id }));
      });
    });

    addWbsUrlListener((wbsUrl: WbsUrl) => {
      this.ngZone.run(() => {
        this.store.dispatch(WbsUrlActions.setWbsUrl({ wbsUrl }));
      });
    });
  }

  toggleMenuVisibility(): void {
    this.store.dispatch(MenuActions.showAllMenusToggle());
  }

  requestAnnotations(annotationIds: string[]): void {
    this.store.dispatch(AnnotationsViewerActions.loadAnnotations({ annotationIds }));
  }

  onViewerMoveEnd(currentView: CurrentView): void {
    this.store.dispatch(AnnotationsUiActions.setViewport({ currentView }));
  }

  onAnnotationCreated(annotation: Annotation): void {
    this.store.dispatch(AnnotationsViewerActions.createAnnotation({ annotation }));
  }
}
