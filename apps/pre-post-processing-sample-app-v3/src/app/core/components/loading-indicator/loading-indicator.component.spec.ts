import { MaterialModule } from '@material/material.module';
import { Spectator, createComponentFactory } from '@ngneat/spectator/jest';

import { LoadingIndicatorComponent } from './loading-indicator.component';

describe('AnnotationLoadingIndicatorComponent', () => {
  let spectator: Spectator<LoadingIndicatorComponent>;
  const createComponent = createComponentFactory({
    component: LoadingIndicatorComponent,
    imports: [
      MaterialModule,
    ]
  });

  beforeEach(() => spectator = createComponent());

  it('should create the app', () => {
    expect(spectator.component).toBeDefined();
  });
});
