import { Component, Input } from '@angular/core';

type Position = 'before' | 'after';

@Component({
  selector: 'app-loading-indicator',
  templateUrl: './loading-indicator.component.html',
  styleUrls: ['./loading-indicator.component.scss']
})
export class LoadingIndicatorComponent {

  @Input() loaded!: boolean;
  @Input() message!: string;
  @Input() position: Position = 'after';

}
