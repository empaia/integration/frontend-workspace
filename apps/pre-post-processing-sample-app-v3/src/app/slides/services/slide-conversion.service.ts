import { Injectable } from '@angular/core';
import { Color, ImageInfo, Slide, TileResolver } from 'slide-viewer';
import { AccessTokenService } from '@token/services/access-token.service';
import { ScopeService } from '@scope/services/scope.service';
import { Actions } from '@ngrx/effects';
import { retryWhen } from 'rxjs/operators';
import { requestNewToken } from 'vendor-app-communication-interface';
import { retryOnAction } from '@shared/helper/rxjs-operators';
import { TokenActions } from '@token/store';
import { SlideColor, SlideInfo, SlideLevel } from '@slides/store/slides/slides.models';
import { WsiTileRequester } from 'empaia-ui-commons';
import { WbsUrlService } from '@wbsUrl/services/wbs-url.service';

@Injectable({
  providedIn: 'root'
})
export class SlideConversionService {
  // number of over zoom layers to be added
  private readonly overZoomLayerCount = 1;

  constructor(
    private accessTokenService: AccessTokenService,
    private scopeService: ScopeService,
    private wbsUrlService: WbsUrlService,
    private actions$: Actions
  ) { }

  public fromWbsApiType(wbsSlide: SlideInfo): Slide {
    const { resolutions, hasArtificialLevel } = this.calcResolutions(wbsSlide.levels);
    const imageInfo = this.createImageInfo(wbsSlide, resolutions, hasArtificialLevel);

    return {
      slideId: wbsSlide.id,
      resolver: new TileResolver(
        new WsiTileRequester(
          wbsSlide.id,
          `${this.wbsUrlService.wbsUrlComplete}/${this.scopeService.scopeId}`,
          [
            {
              key: this.accessTokenService.headerKey, value: this.accessTokenService.getAccessToken.bind(this.accessTokenService)
            }
          ],
          retryWhen(errors =>
            retryOnAction(errors, this.actions$, TokenActions.setAccessToken, requestNewToken)
          )
        ),
        wbsSlide.id,
        imageInfo,
        hasArtificialLevel,
      ),
      imageInfo,
    };
  }

  private createImageInfo(wbsSlide: SlideInfo, resolutions: number[], hasArtificialLevel: boolean): ImageInfo {
    const npp = this.calcNpp(wbsSlide);
    const tileSizes = this.calculateTileSizes(wbsSlide, hasArtificialLevel);

    return {
      width: wbsSlide.extent.x,
      height: wbsSlide.extent.y,
      numberOfLevels: wbsSlide.num_levels,
      tileSizes,
      npp,
      resolutions,
      numberOfOverZoomLevels: this.overZoomLayerCount,
      channels: wbsSlide.channels?.map(it => ({ color: this.convertColor(it.color), id: it.id, name: it.name })) ?? [],
      channel_depth: wbsSlide.channel_depth,
    };
  }

  private calcResolutions(wbsLevels: Array<SlideLevel>): { resolutions: Array<number>, hasArtificialLevel: boolean } {
    // add an artifical level here if our wsi has only levels with a downsample factor below 128
    let hasArtificialLevel = false;
    const resolutions = [];
    const sample_factors = [...wbsLevels.map(it => it.downsample_factor)];
    const maximum = Math.max(...sample_factors);
    const minimum = Math.min(...sample_factors);
    if (maximum < 128) {
      resolutions.push(128);
      hasArtificialLevel = true;
    }
    wbsLevels.reverse().forEach(it => resolutions.push(it.downsample_factor));
    // add an over zoom layer
    // all additional layer are dividing from the base layer (1)
    // with a base of 2
    for (let i = 0; i < this.overZoomLayerCount; i++) {
      resolutions.push(minimum / 2**(i+1));
    }
    return { resolutions, hasArtificialLevel };
  }

  private calculateTileSizes(wbsSlide: SlideInfo, hasArtificialLevel: boolean): number[][] {
    const tileSizes = new Array<number[]>(hasArtificialLevel ? wbsSlide.num_levels + 1 : wbsSlide.num_levels)
      .fill([wbsSlide.tile_extent.x, wbsSlide.tile_extent.y]);
    // the additinal layers don't have there own tiles, in order to use zoomed in tiles of the base layer
    // need bigger tiles so that the coordinates combinted with the zoom factor will call the right tiles
    // and draw a "bigger" version of the tile of the base layer
    for (let i = 0; i < this.overZoomLayerCount; i++) {
      tileSizes.push([wbsSlide.tile_extent.x * 2**(i+1), wbsSlide.tile_extent.y  * 2**(i+1)]);
    }
    return tileSizes;
  }

  private calcNpp(wbsSlide: SlideInfo): number {
    return wbsSlide.pixel_size_nm.x ? wbsSlide.pixel_size_nm.x : 0;
  }

  private convertColor(color: SlideColor): Color {
    return {
      r: color.r,
      g: color.g,
      b: color.b,
      a: color.a,
    };
  }
}
