import {
  AppV3Slide as SlideView,
  AppV3SlideInfo,
  AppV3SlideLevel,
  AppV3SlideColor,
} from 'empaia-api-lib';
import { Slide } from 'slide-viewer';

export type SlideInfo = AppV3SlideInfo;
export type SlideLevel = AppV3SlideLevel;
export type SlideColor = AppV3SlideColor;

export interface SlideEntity {
  id: string;
  disabled: boolean;
  dataView: SlideView;
  imageView?: Slide;
}
