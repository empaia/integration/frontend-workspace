import { createReducer, on } from '@ngrx/store';
import { createEntityAdapter, EntityAdapter, EntityState } from '@ngrx/entity';
import { SlideImage, SlideImageStatus } from '@slides/store/slides-images/slides-images.model';
import { HttpError } from '@menu/models/ui.model';
import * as SlidesImagesActions from './slides-images.actions';


export const SLIDES_IMAGES_FEATURE_KEY = 'slidesImages';

export interface State extends EntityState<SlideImage>{
  error?: HttpError | null;
}

export const slidesImagesAdapter: EntityAdapter<SlideImage> = createEntityAdapter<SlideImage>();

export const initialState: State = slidesImagesAdapter.getInitialState();

export const reducer = createReducer(
  initialState,
  on(SlidesImagesActions.loadSlideLabel, (state, { slideId }): State =>
    slidesImagesAdapter.upsertOne({ id: slideId, labelStatus: SlideImageStatus.LOADING }, {
      ...state
    })
  ),
  on(SlidesImagesActions.loadSlideLabelSuccess, (state, { slideImage }): State =>
    slidesImagesAdapter.upsertOne(slideImage, {
      ...state
    })
  ),
  on(SlidesImagesActions.loadSlideLabelFailure, (state, { slideId, error }): State =>
    slidesImagesAdapter.upsertOne({ id: slideId, labelStatus: SlideImageStatus.ERROR }, {
      ...state,
      error
    })
  ),
  on(SlidesImagesActions.loadSlideThumbnail, (state, { slideId }): State =>
    slidesImagesAdapter.upsertOne({ id: slideId, thumbnailStatus: SlideImageStatus.LOADING }, {
      ...state
    })
  ),
  on(SlidesImagesActions.loadSlideThumbnailSuccess, (state, { slideImage }): State =>
    slidesImagesAdapter.upsertOne(slideImage, {
      ...state
    })
  ),
  on(SlidesImagesActions.loadSlideThumbnailFailure, (state, { slideId, error }): State =>
    slidesImagesAdapter.upsertOne({ id: slideId, thumbnailStatus: SlideImageStatus.ERROR }, {
      ...state,
      error
    })
  ),
  on(SlidesImagesActions.clearAllImages, (state): State =>
    slidesImagesAdapter.removeAll({
      ...state
    })
  ),
);

