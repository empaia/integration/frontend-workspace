import {
  AppV3Job,
  AppV3JobStatus,
  AppV3FloatPrimitive,
  AppV3IdObject,
  AppV3PostClassCollection,
  AppV3PostFloatPrimitive,
  AppV3PostIdObjects,
  AppV3PostPolygonCollection,
} from 'empaia-api-lib';
export {
  AppV3JobStatus as JobStatus,
  AppV3JobMode as JobMode,
  AppV3JobCreatorType as JobCreatorType,
  AppV3CollectionItemType as CollectionItemType,
  AppV3DataCreatorType as DataCreatorType,
  AppV3PrimitiveReferenceType as PrimitiveReferenceType,
} from 'empaia-api-lib';

export type Job = AppV3Job;
export type FloatPrimitive = AppV3FloatPrimitive;
export type IdObject = AppV3IdObject;
export type PostClassCollection = AppV3PostClassCollection;
export type PostFloatPrimitive = AppV3PostFloatPrimitive;
export type PostIdObjects = AppV3PostIdObjects;
export type PostPolygonCollection = AppV3PostPolygonCollection;

export interface JobSelector {
  id: string;
  checked: boolean;
}

export const JOB_POLLING_PERIOD = 5000;

export function isJobRunning(job: Job): boolean {
  return job.status === AppV3JobStatus.Assembly
    || job.status === AppV3JobStatus.Ready
    || job.status === AppV3JobStatus.Scheduled
    || job.status === AppV3JobStatus.Running;
}

export function isJobFailed(job: Job): boolean {
  return job.status === AppV3JobStatus.Error
    || job.status === AppV3JobStatus.Failed
    || job.status === AppV3JobStatus.Incomplete
    || job.status === AppV3JobStatus.Timeout;
}

export const TUMOR_CLASS_NAME = 'org.empaia.vendor_name.tutorial_app_11.v3.0.classes.tumor';
export const NON_TUMOR_CLASS_NAME = 'org.empaia.vendor_name.tutorial_app_11.v3.0.classes.non_tumor';

export function compareJobStatus(prev: Job[], curr: Job[]): boolean {
  return prev.length === curr.length && prev.every((job, index) => job.status === curr[index].status);
}
