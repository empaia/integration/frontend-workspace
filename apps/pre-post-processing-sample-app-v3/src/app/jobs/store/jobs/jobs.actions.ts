import { AnnotationEntity } from 'slide-viewer';
import { createAction, props } from '@ngrx/store';
import { HttpError } from '@menu/models/ui.model';
import { Job, JobCreatorType, JobMode, JobSelector } from '@jobs/store/jobs/jobs.models';
import { EmpaiaAppDescriptionV3 } from '@core/models/ead.models';
import { ApiAnnotation } from '@annotations/store/annotations/annotations.models';

export const loadJobs = createAction(
  '[APP/Jobs] Load Jobs',
  props<{ scopeId: string }>()
);

export const loadJobsSuccess = createAction(
  '[APP/Jobs] Load Jobs Success',
  props<{ jobs: Job[] }>()
);

export const loadJobsFailure = createAction(
  '[APP/Jobs] Load Jobs Failure',
  props<{ error: HttpError }>()
);

export const startJobsPolling = createAction(
  '[APP/Jobs] Start Jobs Polling'
);

export const stopJobsPolling = createAction(
  '[APP/Jobs] Stop Jobs Polling'
);

export const createJob = createAction(
  '[APP/Jobs] Create Job',
  props<{
    scopeId: string,
    creatorId: string,
    creatorType: JobCreatorType,
    annotation: AnnotationEntity,
    jobMode: JobMode,
  }>()
);

export const setWsiJobInput = createAction(
  '[APP/Jobs] Set Wsi Job Input',
  props<{
    scopeId: string,
    jobId: string,
    annotationId: string,
  }>()
);

export const setWsiJobInputReady = createAction(
  '[APP/Jobs] Set Wsi Job Input Ready',
  props<{
    scopeId: string,
    jobId: string,
    slideInputKey: string,
    slideId: string,
    annotationInputKey: string,
    annotationId: string,
  }>()
);

export const setAnnotationJobInput = createAction(
  '[APP/Jobs] Set Annotation Job Input',
  props<{
    scopeId: string,
    jobId: string,
    annotationInputKey: string,
    annotationId: string,
  }>()
);

export const runJob = createAction(
  '[App/Jobs] Run Job',
  props<{
    scopeId: string,
    jobId: string,
    annotationId: string,
  }>()
);

export const setWsiPostprocessingJobInput = createAction(
  '[App/Jobs] Set Wsi Postprocessing Job Input',
  props<{
    scopeId: string,
    jobId: string,
    annotation: AnnotationEntity // ROI
  }>()
);

export const setWsiPostprocessingJobInputReady = createAction(
  '[App/Jobs] Set Wsi Postprocessing Job Input Ready',
  props<{
    scopeId: string,
    jobId: string,
    slideId: string,
    annotation: AnnotationEntity, // ROI
    ead: EmpaiaAppDescriptionV3
  }>()
);

export const setRoiPostprocessingJobInput = createAction(
  '[App/Jobs] Set Roi Postprocessing Job Input',
  props<{
    scopeId: string,
    jobId: string,
    slideId: string,
    annotation: AnnotationEntity, // ROI
    ead: EmpaiaAppDescriptionV3
  }>()
);

export const createCollection = createAction(
  '[App/Jobs] Create Collection',
  props<{
    scopeId: string,
    jobId: string,
    slideId: string,
    annotation: AnnotationEntity, // ROI
    ead: EmpaiaAppDescriptionV3,
    cellsId?: string | undefined,
    classesId?: string | undefined
  }>()
);

export const loadMyCells = createAction(
  '[App/Jobs] Load My Cells',
  props<{
    scopeId: string,
    jobId: string,
    slideId: string,
    annotation: AnnotationEntity, // ROI
    ead: EmpaiaAppDescriptionV3,
    cellsId: string,
    classesId: string,
  }>()
);

export const loadMyCellsReady = createAction(
  '[App/Jobs] Load My Cells Ready',
  props<{
    skip: number,
    limit: number,
    scopeId: string,
    jobId: string,
    slideId: string,
    annotation: AnnotationEntity, // ROI
    ead: EmpaiaAppDescriptionV3,
    preprocessingJobId: string,
    cellsId: string,
    classesId: string,
    cells?: ApiAnnotation[] | undefined
  }>()
);

export const postCellsToCollection = createAction(
  '[App/Jobs] Post Cells To Collection',
  props<{
    scopeId: string,
    jobId: string,
    skip: number,
    limit: number,
    ead: EmpaiaAppDescriptionV3,
    cells: ApiAnnotation[],
    cellsId: string,
    classesId: string,
    annotationId: string
  }>()
);

export const postClassesToCollection = createAction(
  '[App/Jobs] Post Classes To Collection',
  props<{
    scopeId: string,
    jobId: string,
    skip: number,
    limit: number,
    ead: EmpaiaAppDescriptionV3,
    cells: ApiAnnotation[],
    cellsId: string,
    classesId: string,
    annotationId: string
  }>()
);

export const postTumorRatio = createAction(
  '[App/Jobs] Post Tumor Ration',
  props<{
    scopeId: string,
    jobId: string,
    ead: EmpaiaAppDescriptionV3,
    cells: ApiAnnotation[],
    cellsId: string,
    classesId: string,
    annotationId: string
  }>()
);

export const setMyCellsPostprocessingJobInput = createAction(
  '[App/Jobs] Set My Cells Postprocessing Job Input',
  props<{
    scopeId: string,
    jobId: string,
    ead: EmpaiaAppDescriptionV3,
    cellsId: string, // id of collection of the cell annotations
    classesId: string,
    ratioId: string,
    annotationId: string
  }>()
);

export const setMyCellClassesPostprocessingJobInput = createAction(
  '[App/Jobs] Set My Cell Classes Postprocessing Job Input',
  props<{
    scopeId: string,
    jobId: string,
    ead: EmpaiaAppDescriptionV3,
    classesId: string, // id of collection of the classes
    ratioId: string,
    annotationId: string,
  }>()
);

export const setTumorRatioPostprocessingJobOutput = createAction(
  '[App/Jobs] Set Tumor Ratio Postprocessing Job Output',
  props<{
    scopeId: string,
    jobId: string,
    ead: EmpaiaAppDescriptionV3,
    ratioId: string,
    annotationId: string
  }>()
);

export const finalizeJob = createAction(
  '[App/Jobs] Finalize Job',
  props<{
    scopeId: string,
    jobId: string,
    annotationId: string
  }>()
);

export const createJobSuccess = createAction(
  '[APP/Jobs] Create Job Success',
  props<{ job: Job }>()
);

export const createJobFailure = createAction(
  '[APP/Jobs] Create Job Failure',
  props<{ error: HttpError }>()
);

export const setJobInputFailure = createAction(
  '[APP/Jobs] Set Job Input Failure',
  props<{
    error: HttpError,
    scopeId: string,
    jobId: string,
    annotationId: string,
  }>()
);

export const deleteJob = createAction(
  '[APP/Jobs] Delete Job',
  props<{ scopeId: string, jobId: string }>()
);

export const deleteJobSuccess = createAction(
  '[APP/Jobs] Delete Job Success',
  props<{ jobId: string }>()
);

export const deleteJobFailure = createAction(
  '[APP/Jobs] Delete Job Failure',
  props<{ error: HttpError }>()
);

export const setJobSelection = createAction(
  '[APP/Jobs] Set Job Selection',
  props<{ jobSelection: JobSelector }>()
);

export const setJobsSelections = createAction(
  '[APP/Jobs] Set Jobs Selections',
  props<{ jobsSelections: JobSelector[] }>()
);

export const showAllJobs = createAction(
  '[APP/Jobs] Show All Jobs',
);

export const hideAllJobs = createAction(
  '[APP/Jobs] Hide All Jobs',
);

export const clearJobs = createAction(
  '[APP/Jobs] Clear Jobs',
);

export const stopRunningJob = createAction(
  '[Jobs] Stop Running Job',
  props<{ jobId: string }>()
);

export const stopRunningJobReady = createAction(
  '[Jobs] Stop Running Job Ready',
  props<{ scopeId: string, jobId: string }>()
);

export const stopRunningJobSuccess = createAction(
  '[Jobs] Stop Running Job Success',
  props<{ done: boolean }>()
);

export const stopRunningJobFailure = createAction(
  '[Jobs] Stop Running Job Failure',
  props<{ error: HttpError }>()
);

export const stopLoading = createAction(
  '[Jobs] Stop Loading',
);

export const setJobMode = createAction(
  '[Jobs] Set Job Mode',
  props<{ jobMode: JobMode }>()
);
