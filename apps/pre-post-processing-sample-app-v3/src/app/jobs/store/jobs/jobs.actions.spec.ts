import * as fromJobs from './jobs.actions';

describe('JobsActions', () => {
  it('should return an action', () => {
    expect(fromJobs.loadJobs({
      scopeId: ''
    }).type).toBe('[APP/Jobs] Load Jobs');
  });
});
