import { Injectable } from '@angular/core';
import { Actions, concatLatestFrom, createEffect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { fetch, pessimisticUpdate } from '@ngrx/router-store/data-persistence';
import { JobsFeature } from '..';
import * as JobsActions from './jobs.actions';
import * as JobsSelectors from './jobs.selectors';
import * as SlidesActions from '@slides/store/slides/slides.actions';
import * as SlidesSelectors from '@slides/store/slides/slides.selectors';
import * as AnnotationsViewerActions from '@annotations/store/annotations-viewer/annotations-viewer.actions';
import * as ScopeActions from '@scope/store/scope/scope.actions';
import * as ScopeSelectors from '@scope/store/scope/scope.selectors';
import * as TokenActions from '@token/store/token/token.actions';
import {
  catchError,
  filter,
  map,
  retryWhen,
  switchMap,
  takeUntil,
} from 'rxjs/operators';
import { timer } from 'rxjs';
import {
  isJobRunning,
  JOB_POLLING_PERIOD,
  TUMOR_CLASS_NAME,
  NON_TUMOR_CLASS_NAME,
  JobCreatorType,
  JobMode,
  DataCreatorType,
  CollectionItemType,
  PostClassCollection,
  PostPolygonCollection,
  PostIdObjects,
  IdObject,
  PrimitiveReferenceType,
  PostFloatPrimitive,
  FloatPrimitive,
} from '@jobs/store/jobs/jobs.models';
import { AppV3DataService, AppV3JobsService } from 'empaia-api-lib';
import {
  dispatchActionOnErrorCode,
  filterNullish,
  retryOnAction,
} from '@shared/helper/rxjs-operators';
import { requestNewToken } from 'vendor-app-communication-interface';
import { EXAMINATION_CLOSED_ERROR_STATUS_CODE } from '@menu/models/ui.model';
import {
  EmpaiaAppDescriptionV1,
  EmpaiaAppDescriptionV3,
} from '@core/models/ead.models';
import { AnnotationRectangle } from 'slide-viewer';
import {
  ApiAnnotation,
  INPUT_ANNOTATION_LIMIT,
} from '@annotations/store/annotations/annotations.models';
import { EadService } from 'empaia-ui-commons';

@Injectable()
export class JobsEffects {
  // clear jobs on slide selection
  clearJobsOnSlideSelection$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(SlidesActions.selectSlide, SlidesActions.clearSlides),
      map(() => JobsActions.clearJobs())
    );
  });

  // create job when annotation was created
  prepareCreateJob$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(AnnotationsViewerActions.createAnnotationSuccess),
      map((action) => action.annotation),
      concatLatestFrom(() => [
        this.store.select(ScopeSelectors.selectScopeId).pipe(filterNullish()),
        this.store.select(JobsSelectors.selectCurrentJobMode),
      ]),
      map(([annotation, scopeId, jobMode]) =>
        JobsActions.createJob({
          scopeId,
          creatorId: scopeId,
          creatorType: JobCreatorType.Scope,
          annotation,
          jobMode,
        })
      )
    );
  });

  loadJobs$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(JobsActions.loadJobs),
      fetch({
        id: (
          action: ReturnType<typeof JobsActions.loadJobs>,
          _state: JobsFeature.State
        ) => {
          return action.type;
        },
        run: (
          action: ReturnType<typeof JobsActions.loadJobs>,
          _state: JobsFeature.State
        ) => {
          return this.jobsService
            .scopeIdJobsGet({
              scope_id: action.scopeId,
            })
            .pipe(
              // retry to load the selected slide after the token is expired
              retryWhen((errors) =>
                retryOnAction(
                  errors,
                  this.actions$,
                  TokenActions.setAccessToken,
                  requestNewToken
                )
              ),
              map((items) => items.items),
              map((jobs) => JobsActions.loadJobsSuccess({ jobs }))
            );
        },
        onError: (_action: ReturnType<typeof JobsActions.loadJobs>, error) => {
          return JobsActions.loadJobsFailure({ error });
        },
      })
    );
  });

  jobsPolling$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(JobsActions.startJobsPolling),
      switchMap(() =>
        timer(0, JOB_POLLING_PERIOD).pipe(
          takeUntil(this.actions$.pipe(ofType(JobsActions.stopJobsPolling))),
          concatLatestFrom(() =>
            this.store
              .select(ScopeSelectors.selectScopeId)
              .pipe(filterNullish())
          ),
          map(([, scopeId]) => scopeId),
          map((scopeId) => JobsActions.loadJobs({ scopeId }))
        )
      )
    );
  });

  stopJobsPolling$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(JobsActions.loadJobsSuccess),
      map((action) => action.jobs),
      filter((jobs) => !jobs.find(isJobRunning)),
      map(() => JobsActions.stopJobsPolling())
    );
  });

  startJobsPolling$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(
        JobsActions.setWsiJobInput,
        JobsActions.setWsiPostprocessingJobInput,
        SlidesActions.selectSlide
      ),
      map(() => JobsActions.startJobsPolling())
    );
  });

  createJob$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(JobsActions.createJob),
      pessimisticUpdate({
        run: (
          action: ReturnType<typeof JobsActions.createJob>,
          _state: JobsFeature.State
        ) => {
          return this.jobsService
            .scopeIdJobsPost({
              scope_id: action.scopeId,
              body: {
                creator_id: action.creatorId,
                creator_type: action.creatorType,
                mode: action.jobMode,
                containerized: !(action.jobMode === JobMode.Postprocessing),
              },
            })
            .pipe(
              // retry to load the selected slide after the token is expired
              retryWhen((errors) =>
                retryOnAction(
                  errors,
                  this.actions$,
                  TokenActions.setAccessToken,
                  requestNewToken
                )
              ),
              map((job) => job.id),
              map((jobId) =>
                action.jobMode === JobMode.Standalone
                  ? JobsActions.setWsiJobInput({
                    ...action,
                    jobId,
                    annotationId: action.annotation.id,
                  })
                  : JobsActions.setWsiPostprocessingJobInput({
                    ...action,
                    jobId,
                  })
              ),
              catchError((error) =>
                dispatchActionOnErrorCode(
                  error,
                  EXAMINATION_CLOSED_ERROR_STATUS_CODE,
                  ScopeActions.loadExtendedScope({ scopeId: action.scopeId })
                )
              )
            );
        },
        onError: (_action: ReturnType<typeof JobsActions.createJob>, error) => {
          return JobsActions.createJobFailure({ error });
        },
      })
    );
  });

  prepareWsiJobInput$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(JobsActions.setWsiJobInput),
      concatLatestFrom(() => [
        this.store
          .select(SlidesSelectors.selectSelectedSlideId)
          .pipe(filterNullish()),
        this.store
          .select(ScopeSelectors.selectExtendedScope)
          .pipe(filterNullish()),
      ]),
      map(([action, slideId, extended]) =>
        JobsActions.setWsiJobInputReady({
          ...action,
          slideId,
          slideInputKey: this.eadService.isV1Ead(extended.ead)
            ? this.eadService.getV1Inputs(
              extended.ead as EmpaiaAppDescriptionV1
            )[0]
            : this.eadService.getV3Inputs(
              extended.ead as EmpaiaAppDescriptionV3,
              'standalone'
            )[0],
          annotationInputKey: this.eadService.isV1Ead(extended.ead)
            ? this.eadService.getV1Inputs(
              extended.ead as EmpaiaAppDescriptionV1
            )[1]
            : this.eadService.getV3Inputs(
              extended.ead as EmpaiaAppDescriptionV3,
              'standalone'
            )[1],
        })
      )
    );
  });

  setWsiJobInput$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(JobsActions.setWsiJobInputReady),
      pessimisticUpdate({
        run: (
          action: ReturnType<typeof JobsActions.setWsiJobInputReady>,
          _state: JobsFeature.State
        ) => {
          return this.jobsService
            .scopeIdJobsJobIdInputsInputKeyPut({
              scope_id: action.scopeId,
              job_id: action.jobId,
              input_key: action.slideInputKey,
              body: {
                id: action.slideId,
              },
            })
            .pipe(
              // retry to load the selected slide after the token is expired
              retryWhen((errors) =>
                retryOnAction(
                  errors,
                  this.actions$,
                  TokenActions.setAccessToken,
                  requestNewToken
                )
              ),
              map(() => JobsActions.setAnnotationJobInput({ ...action })),
              catchError((error) =>
                dispatchActionOnErrorCode(
                  error,
                  EXAMINATION_CLOSED_ERROR_STATUS_CODE,
                  ScopeActions.loadExtendedScope({ scopeId: action.scopeId })
                )
              )
            );
        },
        onError: (
          action: ReturnType<typeof JobsActions.setWsiJobInputReady>,
          error
        ) => {
          // delete the current job if something goes wrong while setting the inputs
          return JobsActions.setJobInputFailure({ error, ...action });
          // return JobsActions.createJobFailure({ error });
        },
      })
    );
  });

  setAnnotationJobInput$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(JobsActions.setAnnotationJobInput),
      pessimisticUpdate({
        run: (
          action: ReturnType<typeof JobsActions.setAnnotationJobInput>,
          _state: JobsFeature.State
        ) => {
          return this.jobsService
            .scopeIdJobsJobIdInputsInputKeyPut({
              scope_id: action.scopeId,
              job_id: action.jobId,
              input_key: action.annotationInputKey,
              body: {
                id: action.annotationId,
              },
            })
            .pipe(
              // retry to load the selected slide after the token is expired
              retryWhen((errors) =>
                retryOnAction(
                  errors,
                  this.actions$,
                  TokenActions.setAccessToken,
                  requestNewToken
                )
              ),
              map(() => JobsActions.runJob({ ...action })),
              catchError((error) =>
                dispatchActionOnErrorCode(
                  error,
                  EXAMINATION_CLOSED_ERROR_STATUS_CODE,
                  ScopeActions.loadExtendedScope({ scopeId: action.scopeId })
                )
              )
            );
        },
        onError: (
          action: ReturnType<typeof JobsActions.setAnnotationJobInput>,
          error
        ) => {
          // delete the current job if something goes wrong while setting the inputs
          return JobsActions.setJobInputFailure({ error, ...action });
          // return JobsActions.createJobFailure({ error });
        },
      })
    );
  });

  runJob$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(JobsActions.runJob),
      pessimisticUpdate({
        run: (
          action: ReturnType<typeof JobsActions.runJob>,
          _state: JobsFeature.State
        ) => {
          return this.jobsService
            .scopeIdJobsJobIdRunPut({
              scope_id: action.scopeId,
              job_id: action.jobId,
            })
            .pipe(
              // retry to load the selected slide after the token is expired
              retryWhen((errors) =>
                retryOnAction(
                  errors,
                  this.actions$,
                  TokenActions.setAccessToken,
                  requestNewToken
                )
              ),
              map((job) => JobsActions.createJobSuccess({ job })),
              catchError((error) =>
                dispatchActionOnErrorCode(
                  error,
                  EXAMINATION_CLOSED_ERROR_STATUS_CODE,
                  ScopeActions.loadExtendedScope({ scopeId: action.scopeId })
                )
              )
            );
        },
        onError: (action: ReturnType<typeof JobsActions.runJob>, error) => {
          // delete the current job if something goes wrong while setting the inputs
          return JobsActions.setJobInputFailure({ error, ...action });
        },
      })
    );
  });

  setWsiPostprocessingJobInput$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(JobsActions.setWsiPostprocessingJobInput),
      concatLatestFrom(() => [
        this.store
          .select(SlidesSelectors.selectSelectedSlideId)
          .pipe(filterNullish()),
        this.store
          .select(ScopeSelectors.selectExtendedScope)
          .pipe(filterNullish()),
      ]),
      map(([action, slideId, extended]) =>
        JobsActions.setWsiPostprocessingJobInputReady({
          ...action,
          slideId,
          ead: extended.ead as EmpaiaAppDescriptionV3,
        })
      )
    );
  });

  setWsiPostprocessingJobInputReady$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(JobsActions.setWsiPostprocessingJobInputReady),
      pessimisticUpdate({
        run: (
          action: ReturnType<
            typeof JobsActions.setWsiPostprocessingJobInputReady
          >,
          _state: JobsFeature.State
        ) => {
          return this.jobsService
            .scopeIdJobsJobIdInputsInputKeyPut({
              scope_id: action.scopeId,
              job_id: action.jobId,
              input_key: this.eadService.getV3Inputs(
                action.ead,
                'postprocessing'
              )[0],
              body: {
                id: action.slideId,
              },
            })
            .pipe(
              // retry to load the selected slide after the token is expired
              retryWhen((errors) =>
                retryOnAction(
                  errors,
                  this.actions$,
                  TokenActions.setAccessToken,
                  requestNewToken
                )
              ),
              map(() => JobsActions.setRoiPostprocessingJobInput({ ...action }))
            );
        },
        onError: (
          action: ReturnType<
            typeof JobsActions.setWsiPostprocessingJobInputReady
          >,
          error
        ) => {
          return JobsActions.setJobInputFailure({
            error,
            ...action,
            annotationId: action.annotation.id,
          });
        },
      })
    );
  });

  setRoiPostprocessingJobInput$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(JobsActions.setRoiPostprocessingJobInput),
      pessimisticUpdate({
        run: (
          action: ReturnType<typeof JobsActions.setRoiPostprocessingJobInput>,
          _state: JobsFeature.State
        ) => {
          return this.jobsService
            .scopeIdJobsJobIdInputsInputKeyPut({
              scope_id: action.scopeId,
              job_id: action.jobId,
              input_key: this.eadService.getV3Inputs(
                action.ead,
                'postprocessing'
              )[1],
              body: {
                id: action.annotation.id,
              },
            })
            .pipe(
              // retry to load the selected slide after the token is expired
              retryWhen((errors) =>
                retryOnAction(
                  errors,
                  this.actions$,
                  TokenActions.setAccessToken,
                  requestNewToken
                )
              ),
              map(() => JobsActions.createCollection({ ...action }))
            );
        },
        onError: (
          action: ReturnType<typeof JobsActions.setRoiPostprocessingJobInput>,
          error
        ) => {
          return JobsActions.setJobInputFailure({
            error,
            ...action,
            annotationId: action.annotation.id,
          });
        },
      })
    );
  });

  createCollection$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(JobsActions.createCollection),
      pessimisticUpdate({
        run: (
          action: ReturnType<typeof JobsActions.createCollection>,
          _state: JobsFeature.State
        ) => {
          return this.dataService
            .scopeIdCollectionsPost({
              scope_id: action.scopeId,
              body: {
                type: 'collection',
                creator_id: action.scopeId,
                creator_type: DataCreatorType.Scope,
                item_type: action.cellsId
                  ? CollectionItemType.Class
                  : CollectionItemType.Polygon,
              } as PostClassCollection | PostPolygonCollection,
            })
            .pipe(
              // retry to load the selected slide after the token is expired
              retryWhen((errors) =>
                retryOnAction(
                  errors,
                  this.actions$,
                  TokenActions.setAccessToken,
                  requestNewToken
                )
              ),
              map((collection) =>
                action.cellsId
                  ? JobsActions.loadMyCells({
                    ...action,
                    cellsId: action.cellsId as string,
                    classesId: collection.id as string,
                  })
                  : JobsActions.createCollection({
                    ...action,
                    cellsId: collection.id as string,
                  })
              )
            );
        },
        onError: (
          action: ReturnType<typeof JobsActions.createCollection>,
          error
        ) => {
          return JobsActions.setJobInputFailure({
            error,
            ...action,
            annotationId: action.annotation.id,
          });
        },
      })
    );
  });

  prepareLoadMyCells$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(JobsActions.loadMyCells),
      concatLatestFrom(() =>
        this.store
          .select(JobsSelectors.selectLatestPreprocessingJob)
          .pipe(filterNullish())
      ),
      map(([action, preprocessingJob]) =>
        JobsActions.loadMyCellsReady({
          ...action,
          preprocessingJobId: preprocessingJob.id,
          skip: 0,
          limit: INPUT_ANNOTATION_LIMIT,
        })
      )
    );
  });

  loadMyCells$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(JobsActions.loadMyCellsReady),
      pessimisticUpdate({
        run: (
          action: ReturnType<typeof JobsActions.loadMyCellsReady>,
          _state: JobsFeature.State
        ) => {
          const roi = action.annotation as AnnotationRectangle;
          return this.dataService
            .scopeIdAnnotationsQueryPut({
              scope_id: action.scopeId,
              with_classes: true,
              skip: action.skip,
              limit: action.limit,
              body: {
                references: [action.slideId],
                jobs: [action.preprocessingJobId],
                class_values: [TUMOR_CLASS_NAME, NON_TUMOR_CLASS_NAME],
                viewport: {
                  x: roi.coordinates[0][0],
                  y: roi.coordinates[0][1],
                  width: roi.coordinates[1][0] - roi.coordinates[0][0],
                  height: roi.coordinates[2][1] - roi.coordinates[0][1],
                },
                npp_viewing: [
                  roi.nppViewing?.[0] as number,
                  roi.nppCreated as number,
                ],
              },
            })
            .pipe(
              // retry to load the selected slide after the token is expired
              retryWhen((errors) =>
                retryOnAction(
                  errors,
                  this.actions$,
                  TokenActions.setAccessToken,
                  requestNewToken
                )
              ),
              map((response) => {
                if (response.item_count <= 0) {
                  return JobsActions.postTumorRatio({
                    ...action,
                    annotationId: action.annotation.id,
                    cells: response.items,
                  });
                } else if (
                  (action?.cells?.length ?? 0) + response.items.length <
                  response.item_count
                ) {
                  return JobsActions.loadMyCellsReady({
                    ...action,
                    skip: action.skip + INPUT_ANNOTATION_LIMIT,
                    limit: INPUT_ANNOTATION_LIMIT,
                    cells: action.cells
                      ? action.cells.concat(...response.items)
                      : response.items,
                  });
                } else {
                  return JobsActions.postCellsToCollection({
                    ...action,
                    skip: 0,
                    limit: INPUT_ANNOTATION_LIMIT,
                    annotationId: action.annotation.id,
                    cells: action.cells
                      ? action.cells.concat(...response.items)
                      : response.items,
                  });
                }
              })
            );
        },
        onError: (
          action: ReturnType<typeof JobsActions.loadMyCellsReady>,
          error
        ) => {
          return JobsActions.setJobInputFailure({
            error,
            ...action,
            annotationId: action.annotation.id,
          });
        },
      })
    );
  });

  postCellsToCollection$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(JobsActions.postCellsToCollection),
      pessimisticUpdate({
        run: (
          action: ReturnType<typeof JobsActions.postCellsToCollection>,
          _state: JobsFeature.State
        ) => {
          return this.dataService
            .scopeIdCollectionsCollectionIdItemsPost({
              scope_id: action.scopeId,
              collection_id: action.cellsId,
              body: {
                items: action.cells
                  .slice(action.skip, action.limit)
                  .map((c) => ({ id: c.id as string } as IdObject)),
              } as PostIdObjects,
            })
            .pipe(
              // retry to load the selected slide after the token is expired
              retryWhen((errors) =>
                retryOnAction(
                  errors,
                  this.actions$,
                  TokenActions.setAccessToken,
                  requestNewToken
                )
              ),
              map(() =>
                action.skip + INPUT_ANNOTATION_LIMIT < action.cells.length
                  ? JobsActions.postCellsToCollection({
                    ...action,
                    skip: action.skip + INPUT_ANNOTATION_LIMIT,
                    limit: action.limit + INPUT_ANNOTATION_LIMIT,
                  })
                  : JobsActions.postClassesToCollection({
                    ...action,
                    skip: 0,
                    limit: INPUT_ANNOTATION_LIMIT,
                  })
              )
            );
        },
        onError: (
          action: ReturnType<typeof JobsActions.postCellsToCollection>,
          error
        ) => {
          return JobsActions.setJobInputFailure({ error, ...action });
        },
      })
    );
  });

  postClassesToCollection$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(JobsActions.postClassesToCollection),
      pessimisticUpdate({
        run: (
          action: ReturnType<typeof JobsActions.postClassesToCollection>,
          _state: JobsFeature.State
        ) => {
          return this.dataService
            .scopeIdCollectionsCollectionIdItemsPost({
              scope_id: action.scopeId,
              collection_id: action.classesId,
              body: {
                items: action.cells
                  .slice(action.skip, action.limit)
                  .map(
                    (c) => ({ id: c.classes?.[0]?.id as string } as IdObject)
                  ),
              } as PostIdObjects,
            })
            .pipe(
              // retry to load the selected slide after the token is expired
              retryWhen((errors) =>
                retryOnAction(
                  errors,
                  this.actions$,
                  TokenActions.setAccessToken,
                  requestNewToken
                )
              ),
              map(() =>
                action.skip + INPUT_ANNOTATION_LIMIT < action.cells.length
                  ? JobsActions.postClassesToCollection({
                    ...action,
                    skip: action.skip + INPUT_ANNOTATION_LIMIT,
                    limit: action.limit + INPUT_ANNOTATION_LIMIT,
                  })
                  : JobsActions.postTumorRatio({
                    ...action,
                  })
              )
            );
        },
        onError: (
          action: ReturnType<typeof JobsActions.postClassesToCollection>,
          error
        ) => {
          return JobsActions.setJobInputFailure({ error, ...action });
        },
      })
    );
  });

  postTumorRatio$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(JobsActions.postTumorRatio),
      pessimisticUpdate({
        run: (
          action: ReturnType<typeof JobsActions.postTumorRatio>,
          _state: JobsFeature.State
        ) => {
          const ratio = this.calculateTumorRatio(
            action.cells,
            TUMOR_CLASS_NAME
          );
          return this.dataService
            .scopeIdPrimitivesPost({
              scope_id: action.scopeId,
              body: {
                name: 'Tumor Ratio',
                type: 'float',
                value: ratio,
                creator_id: action.scopeId,
                creator_type: DataCreatorType.Scope,
                reference_id: action.annotationId,
                reference_type: PrimitiveReferenceType.Annotation,
              } as PostFloatPrimitive,
            })
            .pipe(
              // retry to load the selected slide after the token is expired
              retryWhen((errors) =>
                retryOnAction(
                  errors,
                  this.actions$,
                  TokenActions.setAccessToken,
                  requestNewToken
                )
              ),
              map((primitive) => primitive as FloatPrimitive),
              map((primitive) =>
                JobsActions.setMyCellsPostprocessingJobInput({
                  ...action,
                  ratioId: primitive.id as string,
                })
              )
            );
        },
        onError: (
          action: ReturnType<typeof JobsActions.postTumorRatio>,
          error
        ) => {
          return JobsActions.setJobInputFailure({ error, ...action });
        },
      })
    );
  });

  setMyCellsPostprocessingJobInput$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(JobsActions.setMyCellsPostprocessingJobInput),
      pessimisticUpdate({
        run: (
          action: ReturnType<
            typeof JobsActions.setMyCellsPostprocessingJobInput
          >,
          _state: JobsFeature.State
        ) => {
          return this.jobsService
            .scopeIdJobsJobIdInputsInputKeyPut({
              scope_id: action.scopeId,
              job_id: action.jobId,
              input_key: this.eadService.getV3Inputs(
                action.ead,
                'postprocessing'
              )[2],
              body: {
                id: action.cellsId,
              },
            })
            .pipe(
              // retry to load the selected slide after the token is expired
              retryWhen((errors) =>
                retryOnAction(
                  errors,
                  this.actions$,
                  TokenActions.setAccessToken,
                  requestNewToken
                )
              ),
              map(() =>
                JobsActions.setMyCellClassesPostprocessingJobInput({
                  ...action,
                })
              )
            );
        },
        onError: (
          action: ReturnType<
            typeof JobsActions.setMyCellsPostprocessingJobInput
          >,
          error
        ) => {
          return JobsActions.setJobInputFailure({ error, ...action });
        },
      })
    );
  });

  setMyCellClassesPostprocessingJobInput$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(JobsActions.setMyCellClassesPostprocessingJobInput),
      pessimisticUpdate({
        run: (
          action: ReturnType<
            typeof JobsActions.setMyCellClassesPostprocessingJobInput
          >,
          _state: JobsFeature.State
        ) => {
          return this.jobsService
            .scopeIdJobsJobIdInputsInputKeyPut({
              scope_id: action.scopeId,
              job_id: action.jobId,
              input_key: this.eadService.getV3Inputs(
                action.ead,
                'postprocessing'
              )[3],
              body: {
                id: action.classesId,
              },
            })
            .pipe(
              // retry to load the selected slide after the token is expired
              retryWhen((errors) =>
                retryOnAction(
                  errors,
                  this.actions$,
                  TokenActions.setAccessToken,
                  requestNewToken
                )
              ),
              map(() =>
                JobsActions.setTumorRatioPostprocessingJobOutput({
                  ...action,
                })
              )
            );
        },
        onError: (
          action: ReturnType<
            typeof JobsActions.setMyCellClassesPostprocessingJobInput
          >,
          error
        ) => {
          return JobsActions.setJobInputFailure({ error, ...action });
        },
      })
    );
  });

  setTumorRatioPostprocessingJobOutput$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(JobsActions.setTumorRatioPostprocessingJobOutput),
      pessimisticUpdate({
        run: (
          action: ReturnType<
            typeof JobsActions.setTumorRatioPostprocessingJobOutput
          >,
          _state: JobsFeature.State
        ) => {
          return this.jobsService
            .scopeIdJobsJobIdOutputsOutputKeyPut({
              scope_id: action.scopeId,
              job_id: action.jobId,
              output_key: this.eadService.getV3Outputs(
                action.ead,
                'postprocessing'
              )[0],
              body: {
                id: action.ratioId,
              },
            })
            .pipe(
              // retry to load the selected slide after the token is expired
              retryWhen((errors) =>
                retryOnAction(
                  errors,
                  this.actions$,
                  TokenActions.setAccessToken,
                  requestNewToken
                )
              ),
              map(() =>
                JobsActions.finalizeJob({
                  ...action,
                })
              )
            );
        },
        onError: (
          action: ReturnType<
            typeof JobsActions.setTumorRatioPostprocessingJobOutput
          >,
          error
        ) => {
          return JobsActions.setJobInputFailure({ error, ...action });
        },
      })
    );
  });

  finalizeJob$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(JobsActions.finalizeJob),
      pessimisticUpdate({
        run: (
          action: ReturnType<typeof JobsActions.finalizeJob>,
          _state: JobsFeature.State
        ) => {
          return this.jobsService
            .scopeIdJobsJobIdFinalizePut({
              scope_id: action.scopeId,
              job_id: action.jobId,
            })
            .pipe(
              // retry to load the selected slide after the token is expired
              retryWhen((errors) =>
                retryOnAction(
                  errors,
                  this.actions$,
                  TokenActions.setAccessToken,
                  requestNewToken
                )
              ),
              map((job) => JobsActions.createJobSuccess({ job })),
              catchError((error) =>
                dispatchActionOnErrorCode(
                  error,
                  EXAMINATION_CLOSED_ERROR_STATUS_CODE,
                  ScopeActions.loadExtendedScope({ scopeId: action.scopeId })
                )
              )
            );
        },
        onError: (
          action: ReturnType<typeof JobsActions.finalizeJob>,
          error
        ) => {
          return JobsActions.setJobInputFailure({ error, ...action });
        },
      })
    );
  });

  // delete job if anything goes wrong while setting the job inputs
  abortJob$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(JobsActions.setJobInputFailure),
      map((action) =>
        JobsActions.deleteJob({
          scopeId: action.scopeId,
          jobId: action.jobId,
        })
      )
    );
  });

  deleteJob$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(JobsActions.deleteJob),
      pessimisticUpdate({
        run: (
          action: ReturnType<typeof JobsActions.deleteJob>,
          _state: JobsFeature.State
        ) => {
          return this.jobsService
            .scopeIdJobsJobIdDelete({
              scope_id: action.scopeId,
              job_id: action.jobId,
            })
            .pipe(
              // retry to load the selected slide after the token is expired
              retryWhen((errors) =>
                retryOnAction(
                  errors,
                  this.actions$,
                  TokenActions.setAccessToken,
                  requestNewToken
                )
              ),
              map((job) => job.id),
              map((jobId) => JobsActions.deleteJobSuccess({ jobId }))
            );
        },
        onError: (_action: ReturnType<typeof JobsActions.deleteJob>, error) => {
          return JobsActions.deleteJobFailure({ error });
        },
      })
    );
  });

  prepareStopRunningJob$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(JobsActions.stopRunningJob),
      map((action) => action.jobId),
      concatLatestFrom(() =>
        this.store.select(ScopeSelectors.selectScopeId).pipe(filterNullish())
      ),
      map(([jobId, scopeId]) =>
        JobsActions.stopRunningJobReady({
          scopeId,
          jobId,
        })
      )
    );
  });

  stopRunningJob$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(JobsActions.stopRunningJobReady),
      pessimisticUpdate({
        run: (
          action: ReturnType<typeof JobsActions.stopRunningJobReady>,
          _state: JobsFeature.State
        ) => {
          return this.jobsService
            .scopeIdJobsJobIdStopPut({
              scope_id: action.scopeId,
              job_id: action.jobId,
            })
            .pipe(
              // retry to load the selected slide after the token is expired
              retryWhen((errors) =>
                retryOnAction(
                  errors,
                  this.actions$,
                  TokenActions.setAccessToken,
                  requestNewToken
                )
              ),
              map((done) => JobsActions.stopRunningJobSuccess({ done }))
            );
        },
        onError: (
          _action: ReturnType<typeof JobsActions.stopRunningJobReady>,
          error
        ) => {
          JobsActions.stopRunningJobFailure({ error });
          return null;
        },
      })
    );
  });

  // stop loading state on extended scope reload
  stopLoading$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ScopeActions.loadExtendedScope),
      map(() => JobsActions.stopLoading())
    );
  });

  constructor(
    private readonly actions$: Actions,
    private readonly store: Store,
    private readonly jobsService: AppV3JobsService,
    private readonly dataService: AppV3DataService,
    private readonly eadService: EadService
  ) {}

  private calculateTumorRatio(
    cells: ApiAnnotation[],
    classValue: string
  ): number {
    let tumor = 0.0;

    cells.forEach((cell) => {
      cell.classes?.forEach((c) => {
        if (c.value === classValue) {
          ++tumor;
        }
      });
    });

    return cells.length ? tumor / cells.length : tumor;
  }
}
