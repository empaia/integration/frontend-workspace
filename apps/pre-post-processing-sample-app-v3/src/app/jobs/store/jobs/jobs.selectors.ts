import { createSelector } from '@ngrx/store';
import {
  State as ModuleState,
  selectJobsFeatureState,
} from '../jobs-feature.state';
import { jobAdapter } from '@jobs/store/jobs/jobs.reducer';
import { jobSelectorAdapter } from '@jobs/store/jobs/jobs.reducer';
import { selectSelectedSlideId } from '@slides/store/slides/slides.selectors';
import { JobMode, isJobFailed } from './jobs.models';

const {
  selectIds,
  selectAll,
} = jobAdapter.getSelectors();

export const selectJobState = createSelector(
  selectJobsFeatureState,
  (state: ModuleState) => state.jobs
);

export const selectJobsLoaded = createSelector(
  selectJobState,
  (state) => state.loaded
);

export const selectAllJobs = createSelector(
  selectJobState,
  (state) => selectAll(state)
);

export const selectAllJobIds = createSelector(
  selectJobState,
  (state) => selectIds(state) as string[]
);

export const selectAllStandaloneJobs = createSelector(
  selectAllJobs,
  (jobs) => jobs.filter(job => job.mode === JobMode.Standalone)
);

export const selectAllPreprocessingJobs = createSelector(
  selectAllJobs,
  (jobs) => jobs.filter(job => job.mode === JobMode.Preprocessing)
);

export const selectLatestPreprocessingJob = createSelector(
  selectAllPreprocessingJobs,
  selectSelectedSlideId,
  (jobs, slideId) => jobs.find(job => !isJobFailed(job) && job.inputs['my_wsi'] === slideId)
);

export const selectAllPostprocessingJobs = createSelector(
  selectAllJobs,
  (jobs) => jobs.filter(job => job.mode === JobMode.Postprocessing)
);

export const selectAllSelectedState = createSelector(
  selectJobState,
  (state) => state.allSelected
);

export const selectAllJobsSelections = createSelector(
  selectJobState,
  (state) => jobSelectorAdapter.getSelectors().selectAll(state.selectedJobs)
);

export const selectJobSelectionEntities = createSelector(
  selectJobState,
  (state) => jobSelectorAdapter.getSelectors().selectEntities(state.selectedJobs)
);

export const selectAllCheckedJobIds = createSelector(
  selectAllJobsSelections,
  (jobsSelections) => jobsSelections.filter(j => !!j.checked).map(j => j.id)
);

export const selectAllUncheckedJobIds = createSelector(
  selectAllJobsSelections,
  (jobsSelections) => jobsSelections.filter(j => !j.checked).map(j => j.id)
);

export const selectCurrentJobMode = createSelector(
  selectJobState,
  (state) => state.jobMode
);
