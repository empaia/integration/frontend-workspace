import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import * as MenuActions from '@menu/store/menu/menu.actions';
import * as SlidesActions from '@slides/store/slides/slides.actions';
import { delay, filter, map, switchMap, takeUntil } from 'rxjs/operators';
import { filterNullish } from '@shared/helper/rxjs-operators';
import { WorkspaceSizeService } from '@menu/services/workspace-size.service';
import { timer } from 'rxjs';
import { INITIAL_MENU_PERIOD } from '@menu/store/menu/menu.models';


@Injectable()
export class MenuEffects {

  // open slide menu when slides are loading
  openSlideMenu$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(SlidesActions.loadSlides),
      map(() => MenuActions.openMenu({ id: 'slides' }))
    );
  });

  initWorkspaceSize = createEffect(() => {
    return this.actions$.pipe(
      ofType(MenuActions.initWorkspaceSize),
      switchMap(() =>
        timer(0, INITIAL_MENU_PERIOD).pipe(
          takeUntil(this.actions$.pipe(ofType(MenuActions.changeMenuWorkspaceSize))),
          delay(1), // await dom update, then get new workspace dimensions
          map(() => {
            const bar = document.querySelector('.svm-toolbar');
            const overview = document.querySelector('.svm-overview');
            const drag = document.querySelector('.svm-overview-drag-box');
            return {
              bar,
              overview,
              drag
            };
          }),
          filter(({ bar, overview, drag }) => !!bar && !!overview && !!drag),
          map(() => document.body.querySelector('.workspace')),
          filterNullish(),
          map(workspaceElement => workspaceElement.getBoundingClientRect()),
          map(domRect => MenuActions.changeMenuWorkspaceSize({ domRect }))
        )
      )
    );
  });

  menuChangeWorkspace$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(
        MenuActions.openMenu,
        MenuActions.showLabel,
        MenuActions.showAllMenusToggle,
      ),
      delay(1), // await dom update, then get new workspace dimensions
      map(() => {
        const bar = document.querySelector('.svm-toolbar');
        const overview = document.querySelector('.svm-overview');
        const drag = document.querySelector('.svm-overview-drag-box');
        return {
          bar,
          overview,
          drag
        };
      }),
      filter(({ bar, overview, drag }) => !!bar && !!overview && !!drag),
      map(() => document.body.querySelector('.workspace')),
      filterNullish(),
      map(workspaceElement => workspaceElement.getBoundingClientRect()),
      map(domRect => MenuActions.changeMenuWorkspaceSize({ domRect })),
    );
  });

  setViewerToolbarPosition$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(MenuActions.changeMenuWorkspaceSize),
      map(action => action.domRect),
      map(domRect => this.workspaceSizeService.setSlideViewerToolbarPosition(domRect)),
    );
  }, { dispatch: false });

  setViewerOverviewPosition$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(MenuActions.changeMenuWorkspaceSize),
      map(action => action.domRect),
      map(domRect => this.workspaceSizeService.setSlideViewerOverviewPosition(domRect))
    );
  }, { dispatch: false });

  // open results menu on slide selection
  openResultsOnSlideSelection$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(SlidesActions.selectSlide),
      map(() => MenuActions.openMenu({ id: 'results' }))
    );
  });

  constructor(
    private actions$: Actions,
    private workspaceSizeService: WorkspaceSizeService,
  ) {}

}
