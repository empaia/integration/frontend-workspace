import { AppV3ExtendedScope } from 'empaia-api-lib';

export type ExtendedScope = AppV3ExtendedScope;
