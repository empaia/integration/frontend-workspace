import { Injectable } from '@angular/core';
import { Actions, concatLatestFrom, createEffect, ofType } from '@ngrx/effects';
import { fetch } from '@ngrx/router-store/data-persistence';
import { ScopeService } from '@scope/services/scope.service';
import * as ScopeActions from './scope.actions';
import * as ScopeSelectors from './scope.selectors';
import * as TokenActions from '@token/store/token/token.actions';
import * as TokenSelectors from '@token/store/token/token.selectors';
import { distinctUntilChanged, filter, map } from 'rxjs/operators';
import { AppV3ScopeService } from 'empaia-api-lib';
import { Store } from '@ngrx/store';
import { filterNullish } from '@shared/helper/rxjs-operators';
import { State } from './scope.reducer';

@Injectable()
export class ScopeEffects {
  setScopeId$ = createEffect(
    () => {
      return this.actions$.pipe(
        ofType(ScopeActions.setScope),
        map((action) => action.scopeId),
        map((scopeId) => (this.scopeService.scopeId = scopeId))
      );
    },
    { dispatch: false }
  );

  startLoadingExtendedScope$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ScopeActions.setScope, TokenActions.setAccessToken),
      concatLatestFrom(() => [
        this.store.select(ScopeSelectors.selectScopeId).pipe(filterNullish()),
        this.store.select(TokenSelectors.selectAccessToken),
      ]),
      filter(([, _scopeId, token]) => !!token),
      distinctUntilChanged((prev, curr) => prev[1] === curr[1]),
      map(([, scopeId]) => scopeId),
      map((scopeId) => ScopeActions.loadExtendedScope({ scopeId }))
    );
  });

  loadExtendedScope$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ScopeActions.loadExtendedScope),
      fetch({
        run: (
          action: ReturnType<typeof ScopeActions.loadExtendedScope>,
          _state: State
        ) => {
          return this.scopePanelService
            .scopeIdGet({
              scope_id: action.scopeId,
            })
            .pipe(
              map((extendedScope) =>
                ScopeActions.loadExtendedScopeSuccess({ extendedScope })
              )
            );
        },
        onError: (
          _action: ReturnType<typeof ScopeActions.loadExtendedScope>,
          error
        ) => {
          return ScopeActions.loadExtendedScopeFailure({ error });
        },
      })
    );
  });

  constructor(
    private readonly actions$: Actions,
    private readonly scopeService: ScopeService,
    private readonly scopePanelService: AppV3ScopeService,
    private readonly store: Store
  ) {}
}
