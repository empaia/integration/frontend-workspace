import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';
import { SCOPE_MODULE_FEATURE_KEY, reducers } from '@scope/store/scope-feature.state';
import { EffectsModule } from '@ngrx/effects';
import { ScopeEffects } from '@scope/store';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    StoreModule.forFeature(
      SCOPE_MODULE_FEATURE_KEY,
      reducers
    ),
    EffectsModule.forFeature([
      ScopeEffects,
    ])
  ]
})
export class ScopeModule { }
