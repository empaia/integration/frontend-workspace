export const environment = {
  production: true,
  empaiaPortalUrl: window['env']['empaiaPortalUrl'],
  workbenchClientUrl: window['env']['workbenchClientUrl'],
  workbenchClientv2Url: window['env']['workbenchClientv2Url'],
  workbenchClientv3Url: window['env']['workbenchClientv3Url'],
  mdsUiUrl: window['env']['mdsUiUrl'],
  organizationName: window['env']['organizationName'],
  downTimeReport: window['env']['downTimeReport'],
};
