// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  empaiaPortalUrl: 'https://empaia-preprod.vitasystems.dev/',
  workbenchClientUrl: 'https://empaia-plat03.charite.de/testsuite-005/wbc/',
  workbenchClientv2Url: '', //'https://empaia-plat03.charite.de/testsuite-005/wbc2/',
  workbenchClientv3Url: '',
  mdsUiUrl: 'https://empaia-plat02.charite.de/testsuite-005/mds-ui/#/',
  organizationName: 'Charité - Universitätsmedizin Berlin',
  downTimeReport: ''
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
