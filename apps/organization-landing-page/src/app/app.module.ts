import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { RouterModule } from '@angular/router';
import { environment } from '../environments/environment';

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    RouterModule.forRoot([], {
      initialNavigation: 'enabledBlocking',
      useHash: true,
    }),
  ],
  providers: [
    { provide: 'EMPAIA_PORTAL_URL', useValue: environment.empaiaPortalUrl },
    {
      provide: 'WORKBENCH_CLIENT_URL',
      useValue: environment.workbenchClientUrl,
    },
    {
      provide: 'WORKBENCH_CLIENT_V2_URL',
      useValue: environment.workbenchClientv2Url,
    },
    {
      provide: 'WORKBENCH_CLIENT_V3_URL',
      useValue: environment.workbenchClientv3Url,
    },
    { provide: 'MDS_UI_URL', useValue: environment.mdsUiUrl },
    { provide: 'ORGANIZATION_NAME', useValue: environment.organizationName },
    { provide: 'DOWN_TIME_REPORT', useValue: environment.downTimeReport },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
