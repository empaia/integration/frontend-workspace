import { Component, Inject } from '@angular/core';

@Component({
  selector: 'empaia-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'organization-landing-page';

  constructor(
    @Inject('EMPAIA_PORTAL_URL') public portalUrl: string,
    @Inject('WORKBENCH_CLIENT_URL') public wbcUrl: string,
    @Inject('WORKBENCH_CLIENT_V2_URL') public wbc2Url: string,
    @Inject('WORKBENCH_CLIENT_V3_URL') public wbc3Url: string,
    @Inject('MDS_UI_URL') public mdsUiUrl: string,
    @Inject('ORGANIZATION_NAME') public organizationName: string,
    @Inject('DOWN_TIME_REPORT') public downTimeReport: string,
  ) { }
}
