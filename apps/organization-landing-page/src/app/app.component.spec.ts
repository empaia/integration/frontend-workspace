import { TestBed } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { RouterTestingModule } from '@angular/router/testing';

describe('AppComponent', () => {
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [RouterTestingModule],
      declarations: [AppComponent],
      providers: [
        { provide: 'EMPAIA_PORTAL_URL', useValue: 'test-url' },
        { provide: 'WORKBENCH_CLIENT_URL', useValue: 'test-url' },
        { provide: 'WORKBENCH_CLIENT_V2_URL', useValue: 'test-url' },
        { provide: 'WORKBENCH_CLIENT_V3_URL', useValue: 'test-url' },
        { provide: 'MDS_UI_URL', useValue: 'test-url' },
        { provide: 'ORGANIZATION_NAME', useValue: 'test-name' },
        { provide: 'DOWN_TIME_REPORT', useValue: 'test-text' },
      ]
    }).compileComponents();
  });

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app).toBeTruthy();
  });

  it(`should have as title 'organization-landing-page'`, () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app.title).toEqual('organization-landing-page');
  });

  it('should render title', () => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const compiled = fixture.nativeElement;
    expect(compiled.querySelector('h2').textContent).toContain(
      'Workspace'
    );
  });
});
