(function(window) {
    window['env'] = window['env'] || {};

    // Environment variables
    window['env']['production'] = true;
    window['env']['empaiaPortalUrl'] = '${OLP_EMPAIA_PORTAL_URL}';
    window['env']['workbenchClientUrl'] = '${OLP_WORKBENCH_CLIENT_URL}';
    window['env']['workbenchClientv2Url'] = '${OLP_WORKBENCH_CLIENT_V2_URL}';
    window['env']['workbenchClientv3Url'] = '${OLP_WORKBENCH_CLIENT_V3_URL}';
    window['env']['mdsUiUrl'] = '${OLP_MDS_UI_URL}';
    window['env']['organizationName'] = '${OLP_ORGANIZATION_NAME}';
    window['env']['downTimeReport'] = '${OLP_DOWN_TIME_REPORT}'
})(this);
