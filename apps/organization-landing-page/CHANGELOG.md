# Organization Landing Page

# 1.0.13 (2024-07-16)

### Refactor

* upgraded to angular 17

# 1.0.12 (2024-01-11)

### Refactor

* updated to angular 16

# 1.0.7 (2023-06-08)

### Refactor

* Replaced old logo for new one

# 1.0.0 (2023-03-20)

### Features

* updated to Angular 15

# 0.5.0 (2022-11-18)

### Feature

* Added button for Workbench Client 3.0

# 0.4.3 (2022-11-07)

### Refactor

* Removed Angular fxLayout

# 0.4.1 (2022-11-01)

### Refactor

* Removed unused cypress e2e tests

# 0.4.0 (2022-10-31)

### Features

* Added down time banner
* Down time text is store in environment variable
