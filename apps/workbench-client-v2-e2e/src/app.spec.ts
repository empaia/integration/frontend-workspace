import { expect, test, Page } from '@playwright/test';

test.describe.parallel('Workbench Client 2.0', () => {
  const TEST_CASE_2 = 'Test Case 02';
  const TEST_CASE_3 = 'Test Case 03';

  test.describe('Login', () => {
    test('should login', async ({ page }) => {
      await login(page);
    });
  });

  test.describe.parallel('Cases Panel', () => {
    test.beforeEach(async ({ page }) => {
      await login(page);
    });

    test('should contain 4 Test Cases', async ({ page }) => {
      await page.goto('http://localhost:4200/#/cases');
      await expect.soft(page.locator('app-case-item', { hasText: /.*Test Case/ })).toHaveCount(4);
    });

    test('should have 1 slide in Test Case 03', async ({ page }) => {
      await page.goto('http://localhost:4200/#/cases');
      await page.locator('app-case-item', { hasText: TEST_CASE_2 }).click();
      await expect.soft(page.locator('[data-cy=slides-tray-list]').locator('app-slide-tray-item')).toHaveCount(1);
    });
  });

  test.describe.serial('Examination tests', () => {
    test.beforeEach(async ({ page }) => {
      await login(page);
    });

    test('should create a new examination', async ({ page }) => {
      await page.goto('http://localhost:4200/#/cases');
      const caseItem = page.locator('app-case-item', { hasText: TEST_CASE_3 });
      const lockButton = caseItem.locator('[data-cy=case-lock-button]');
      await expect.soft(lockButton).toContainText(/lock$/);
      await lockButton.click();
      await page.locator('[data-cy=dialog-accept-button]').click();
      const createResponse = await page.waitForResponse(/\/examinations$/);
      expect.soft(createResponse.ok()).toBeTruthy();
      expect.soft(createResponse.request().method()).toMatch('POST');
    });

    test('should close an examnination via button click', async ({ page }) => {
      await page.goto('http://localhost:4200/#/cases');
      const caseItem = page.locator('app-case-item', { hasText: TEST_CASE_3 });
      const lockButton = caseItem.locator('[data-cy=case-lock-button]');

      await expect.soft(lockButton).toContainText(/lock_open$/);
      await lockButton.click();
      await page.locator('[data-cy=dialog-accept-button]').click();
      const closeRespone = await page.waitForResponse(/\/examinations\/[0-9a-fA-F]{8}\b-[0-9a-fA-F]{4}\b-[0-9a-fA-F]{4}\b-[0-9a-fA-F]{4}\b-[0-9a-fA-F]{12}\/close$/);
      expect.soft(closeRespone.ok()).toBeTruthy();
      expect.soft(closeRespone.request().method()).toMatch('PUT');
    });

    test('should show at least one examination', async ({ page }) => {
      await page.goto('http://localhost:4200/#/cases');
      const caseItem = page.locator('app-case-item', { hasText: TEST_CASE_3 });
      await caseItem.locator('app-expansion-button').first().locator('button').click();
      expect.soft(await caseItem.locator('app-case-examination-list').count()).toBeGreaterThanOrEqual(1);
    });
  });

  test.describe.serial('Apps Panel', () => {
    test.beforeEach(async ({ page }) => {
      await login(page);
      await page.locator('app-case-item', { hasText: TEST_CASE_2 }).click();
    });

    test('should add TA10v2 app to the list', async ({ page }) => {
      await page.locator('[data-cy=add-app-button]').click();
      await page.locator('[data-cy=new-app-cards-list]', { hasText: 'TA10v2' }).click();
      expect.soft(await page.locator('[data-cy=apps-list]').locator('app-app-item').count()).toBeGreaterThanOrEqual(1);
    });

    test('should start TA10v2 app', async ({ page }) => {
      await page.locator('[data-cy=apps-list]').locator('app-app-item', { hasText: 'TA10v2' }).locator('[data-cy=app-item]').click();
      await page.locator('[data-cy=start-app-button]').click();
      expect.soft(page.locator('[data-cy=vendor-app]')).toHaveClass('vendor-app-container active');
    });
  });
});

async function login(page: Page): Promise<void> {
  await page.goto('http://localhost:4200/#/cases');
  await page.getByRole('button', { name: 'Login' }).click();
  await page.getByLabel('Username or email').click();
  await page.getByLabel('Username or email').fill('pathologist');
  await page.getByLabel('Password').click();
  await page.getByLabel('Password').fill('secret');
  await page.getByRole('button', { name: 'Sign In' }).click();
  await expect(page).toHaveURL('http://localhost:4200/#/cases');
}
