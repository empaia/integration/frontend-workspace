import { IFrameComponent } from './i-frame.component';
import { createComponentFactory, Spectator } from '@ngneat/spectator';

describe('IFrameComponent', () => {
  let spectator: Spectator<IFrameComponent>;
  const createComponent = createComponentFactory({
    component: IFrameComponent,
  });

  beforeEach(() => spectator = createComponent());

  it('should create', () => {
    expect(spectator.component).toBeTruthy();
  });
});
