import { Component } from '@angular/core';

type Embed = 'iframe' | 'object' | 'video' | 'none';

@Component({
  selector: 'app-i-frame',
  templateUrl: './i-frame.component.html',
  styleUrls: ['./i-frame.component.scss']
})
export class IFrameComponent {
  public embed: Embed = 'none';
}
