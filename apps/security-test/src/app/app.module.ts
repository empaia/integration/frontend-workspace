import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { AttackComponent } from './attack/attack.component';
import { IFrameComponent } from './i-frame/i-frame.component';
import { AppRoutingModule } from './app-routing.module';
import { ForeignApiComponent } from './foreign-api/foreign-api.component';
import { HttpClientModule } from '@angular/common/http';
import { SunshinePipe } from './shared/pipes/sunshine.pipe';
import { HashLocationStrategy, LocationStrategy } from '@angular/common';

@NgModule({
  declarations: [AppComponent, AttackComponent, IFrameComponent, ForeignApiComponent, SunshinePipe],
  imports: [BrowserModule, AppRoutingModule, HttpClientModule],
  providers: [
    { provide: LocationStrategy, useClass: HashLocationStrategy }
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
