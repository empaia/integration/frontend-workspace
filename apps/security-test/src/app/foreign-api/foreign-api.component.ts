import { Component, OnInit } from '@angular/core';
import { WeatherService } from '../shared/services/weather.service';
import { Observable } from 'rxjs';
import { Icon, WeatherEntity } from '../shared/models/weather.models';

@Component({
  selector: 'app-foreign-api',
  templateUrl: './foreign-api.component.html',
  styleUrls: ['./foreign-api.component.scss']
})
export class ForeignApiComponent implements OnInit {
  public readonly ICONS = Icon;

  public weather$!: Observable<WeatherEntity>;
  public current!: Date;
  public timeStamps: number[] = [4, 8, 12, 16, 20];

  constructor(private weatherService: WeatherService) { }

  ngOnInit(): void {
    this.weather$ = this.weatherService.getWeather();
    this.current = new Date();
  }

}
