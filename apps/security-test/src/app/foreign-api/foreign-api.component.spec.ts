import { ForeignApiComponent } from './foreign-api.component';
import { createComponentFactory, Spectator } from '@ngneat/spectator';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('ForeignApiComponent', () => {
  let spectator: Spectator<ForeignApiComponent>;
  const createComponent = createComponentFactory({
    component: ForeignApiComponent,
    imports: [
      HttpClientTestingModule,
    ],
  });

  beforeEach(() => spectator = createComponent());

  it('should create', () => {
    expect(spectator.component).toBeTruthy();
  });
});
