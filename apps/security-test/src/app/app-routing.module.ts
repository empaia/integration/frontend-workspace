import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AttackComponent } from './attack/attack.component';
import { IFrameComponent } from './i-frame/i-frame.component';
import { ForeignApiComponent } from './foreign-api/foreign-api.component';

const routes: Routes = [
  { path: '', redirectTo: 'attack', pathMatch: 'full' },
  { path: 'attack', component: AttackComponent },
  { path: 'iframe', component: IFrameComponent },
  { path: 'foreign-api', component: ForeignApiComponent },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes),
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
