import { AttackComponent } from './attack.component';
import { createComponentFactory, Spectator } from '@ngneat/spectator';

describe('AttackComponent', () => {
  let spectator: Spectator<AttackComponent>;
  const createComponent = createComponentFactory({
    component: AttackComponent,
  });

  beforeEach(() => spectator = createComponent());

  it('should create', () => {
    expect(spectator.component).toBeTruthy();
  });
});
