/* eslint-disable @typescript-eslint/no-explicit-any */
import { Component } from '@angular/core';

@Component({
  selector: 'app-attack',
  templateUrl: './attack.component.html',
  styleUrls: ['./attack.component.scss']
})
export class AttackComponent {
  public secret: string | null = '';
  public error = '';
  public data: any[] = [];
  public cookies!: string;
  public frames: string | undefined = undefined;

  public resetError(): void {
    this.error = '';
  }

  public showAllFrames(): void {
    this.resetError();
    try {
      const frames = window.frames;
      console.log('Frames in App: ', frames);
      console.log('DOM in App: ', frames.document);
      console.log('Frames of Parent: ', window.parent?.frames);
      this.frames = JSON.stringify(window.top?.frames);
    } catch (e) {
      this.error = (e as Error).message;
    }
  }

  public setBackgroundColorOfWbc(): void {
    this.resetError();
    try {
      const parentNode = document.parentElement;
      console.log('Parent: ', parentNode);

      const parentWindow = window.top;

      if (parentWindow) {
        parentWindow.document.body.style.background = 'red';
      }
    } catch (e) {
      this.error = (e as Error).message;
    }
  }

  public setBackgroundColorOfApp(): void {
    this.resetError();
    try {
      const container = document.getElementById('attack-container');
      if (container) {
        container.style.background = 'red';
      }
    } catch (e) {
      this.error = (e as Error).message;
    }
  }

  public setBackgroundColorOfAllApps(): void {
    this.resetError();
    try {
      const frames = window.top?.frames;

      if (frames) {
        for (let i = 0; i < frames.length; i++) {
          const container = frames[i].document.body.querySelector('.attacker-container') as HTMLElement;
          if (container) {
            container.style.background = 'red';
          }
        }
      }
    } catch (e) {
      this.error = (e as Error).message;
    }
  }

  public showLocalStorage(): void {
    this.resetError();
    try {
      this.secret = localStorage.getItem('secret');
      console.log('WBC 2.0 Secret: ',this.secret);
    } catch (e) {
      this.error = (e as Error).message;
    }
  }

  public setLocalStorage(): void {
    this.resetError();
    try {
      localStorage.setItem('mySecret', 'this is the secret of the app');
      this.secret = localStorage.getItem('mySecret');
    } catch (e) {
      this.error = (e as Error).message;
    }
  }

  public openPopup(): void{
    this.resetError();
    try {
      const popup = window.open('about:blank', 'hello', 'width=200, height=200');
      console.log('My popup: ', popup);
    } catch (e) {
      this.error = (e as Error).message;
    }
  }

  public openTap(): void {
    this.resetError();
    try {
      window.open('https://www.google.com/', '_bank');
    } catch (e) {
      this.error = (e as Error).message;
    }
  }

  public getDataFromDataBase(): void {
    this.resetError();
    try {
      const request = window.indexedDB.open('WBC_DB');
      request.addEventListener('error', () => {
        this.error = 'IndexedDB error occurred on open';
      });
      request.addEventListener('success', () => {
        const db = request.result;
        const transaction = db.transaction(['customers']);
        const objectStore = transaction.objectStore('customers');
        const r = objectStore.getAll();
        r.addEventListener('error', () => {
          this.error = 'Could not read from database';
        });
        r.addEventListener('success', () => {
          this.data = r.result;
        });
      });
    } catch (e) {
      this.error = (e as Error).message;
    }
  }

  public createIndexedDb(): void {
    this.resetError();
    try {
      const data = [
        { id: 3, name: 'Bob', age: 35, email: 'bob@company.com' },
        { id: 4, name: 'Alice', age: 32, email: 'alice@company.com'}
      ];
      window.indexedDB.deleteDatabase('APP_DB');
      const request = window.indexedDB.open('APP_DB');
      request.addEventListener('error', () => {
        this.error = 'An error occurred on db opened';
      });
      request.addEventListener('upgradeneeded', () => {
        const db = request.result;
        const objectStore = db.createObjectStore('customers', { keyPath: 'id' });
        objectStore.createIndex('name', 'name', { unique: false });
        objectStore.createIndex('email', 'email',{ unique: false });
      });
      request.addEventListener('success', () => {
        const db = request.result;
        const transaction = db.transaction(['customers'], 'readwrite');
        transaction.addEventListener('error', () => {
          this.error = 'An error occurred on db transaction';
        });
        transaction.addEventListener('complete', () => {
          console.log('All stored');
        });

        const objectStore = transaction.objectStore('customers');
        for (const customer of data) {
          const r = objectStore.add(customer);
          r.onerror = () => this.error = 'Could not store data in db';
          r.onsuccess = () => console.log('Wrote data');
        }
      });
    } catch (e) {
      this.error = (e as Error).message;
    }
  }

  public readFromOwnDB(): void {
    this.resetError();
    try {
      const request = window.indexedDB.open('APP_DB');
      request.addEventListener('error', () => {
        this.error = 'An error occurred on db opened';
      });
      request.addEventListener('success', () => {
        const db = request.result;
        const transaction = db.transaction(['customers']);
        const objectStore = transaction.objectStore('customers');
        const r = objectStore.getAll();
        r.addEventListener('error', () => {
          this.error = 'Could not read from database';
        });
        r.addEventListener('success', () => {
          this.data = r.result;
        });
      });
    } catch (e) {
      this.error = (e as Error).message;
    }
  }

  public readCookies(): void {
    this.resetError();
    try {
      this.cookies = document.cookie;
    } catch (e) {
      this.error = (e as Error).message;
    }
  }

  public writeCookie(): void {
    this.resetError();
    try {
      document.cookie = 'username=Bob; path=/';
      this.readCookies();
    } catch (e) {
      this.error = (e as Error).message;
    }
  }

  public readSessionStorage(): void {
    const token = sessionStorage.getItem('refresh_token');
    console.log('Refresh token: ', token);
  }

  public writeSessionStorage(): void {
    sessionStorage.setItem('test', '123');
  }

  public clearSessionStorage(): void {
    sessionStorage.clear();
  }
}
