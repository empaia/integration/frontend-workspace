export interface WeatherEntity {
  weather: Weather[];
  sources: Source[];
}

interface Weather {
  timestamp: Date;
  source_id: number;
  cloud_cover?: number | undefined;
  dew_point?: number | undefined;
  precipitation?: number | undefined;
  pressure_msl?: number | undefined;
  relative_humidity?: number | undefined;
  sunshine?: number | undefined;
  temperature?: number | undefined;
  visibility?: number | undefined;
  wind_direction?: number | undefined;
  wind_speed?: number | undefined;
  wind_gust_direction?: number | undefined;
  wind_gust_speed?: number | undefined;
  condition?: Condition | undefined;
  icon?: Icon | undefined;
}

interface Source {
  id: number;
  dwd_station_id?: string | undefined;
  wmo_station_id?: string | undefined;
  station_name?: string | undefined;
  observation_type: ObservationType;
  first_record: Date;
  last_record: Date;
  lat: number;
  lon: number;
  height: number;
  distance: number;
}

export enum Condition {
  DRY = 'dry',
  FOG = 'fog',
  RAIN = 'rain',
  SLEET = 'sleet',
  SNOW = 'snow',
  HAIL = 'hail',
  THUNDERSTORM = 'thunderstorm'
}

export enum Icon {
  CLEAR_DAY = 'clear-day',
  CLEAR_NIGHT = 'clear-night',
  PARTLY_CLOUDY_DAY = 'partly-cloudy-day',
  PARTLY_CLOUDY_NIGHT = 'partly-cloudy-night',
  CLOUDY = 'cloudy',
  FOG = 'fog',
  WIND = 'wind',
  RAIN = 'rain',
  SLEET = 'sleet',
  SNOW = 'snow',
  HAIL = 'hail',
  THUNDERSTORM = 'thunderstorm'
}

export enum ObservationType {
  FORECAST = 'forecast',
  SYNOP = 'synop',
  CURRENT = 'current',
  HISTORICAL = 'historical'
}
