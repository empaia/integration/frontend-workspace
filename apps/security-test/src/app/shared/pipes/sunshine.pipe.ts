import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'sunshine'
})
export class SunshinePipe implements PipeTransform {

  transform(value: number | undefined): string {
    return value ? value < 59 ? value < 10 ? `00:0${value}` : `00:${value}` : '01:00' : '00:00';
  }

}
