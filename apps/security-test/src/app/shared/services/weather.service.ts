import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { WeatherEntity } from '../models/weather.models';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class WeatherService {
  private baseUrl = 'https://api.brightsky.dev/weather?lat=52.520008&lon=13.404954';

  constructor(private http: HttpClient) { }

  public getWeather(): Observable<WeatherEntity> {
    const now = new Date();
    const url = `${this.baseUrl}&date=${now.getFullYear()}-${now.getMonth() + 1}-${now.getDate()}`;
    return this.http.get(url).pipe(
      map(result => result as WeatherEntity)
    );
  }
}
