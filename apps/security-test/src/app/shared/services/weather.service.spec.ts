import { WeatherService } from './weather.service';
import { createServiceFactory, SpectatorService } from '@ngneat/spectator';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('WeatherService', () => {
  let spectator: SpectatorService<WeatherService>;
  const createService = createServiceFactory({
    service: WeatherService,
    imports: [
      HttpClientTestingModule
    ]
  });

  beforeEach(() => spectator = createService());

  it('should be created', () => {
    expect(spectator.service).toBeTruthy();
  });
});
