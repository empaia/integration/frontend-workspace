import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  // You can add this code to the wbc 2.0, to populate the localstorage, indexedDB and
  // cookies.
  /*
  localStorage.setItem('secret', 'this is the secret of the wbc 2.0');

    const data = [
      { id: 1, name: 'Bill', age: 35, email: 'bill@company.com' },
      { id: 2, name: 'Donna', age: 32, email: 'donna@company.com'}
    ];

    window.indexedDB.deleteDatabase('WBC_DB');
    const request = window.indexedDB.open('WBC_DB');
    request.addEventListener('error', event => {
      console.log('Not working: ', event);
    });
    request.addEventListener('upgradeneeded', () => {
      const db = request.result;
      const objectStore = db.createObjectStore('customers', { keyPath: 'id' });
      objectStore.createIndex('name', 'name', { unique: false });
      objectStore.createIndex('email', 'email',{ unique: false });
      console.log('Table created');
    });
    request.addEventListener('success', () => {
      console.log('DB was opened');
      const db = request.result;
      const transaction = db.transaction(['customers'], 'readwrite');
      transaction.addEventListener('error', event => {
        console.log('Error on transaction: ', event);
      });
      transaction.addEventListener('complete', () => {
        console.log('All stored');
      });

      const objectStore = transaction.objectStore('customers');
      for (const customer of data) {
        const r = objectStore.add(customer);
        r.onerror = event => console.log('An Error occurred: ', event);
        r.onsuccess = () => console.log('Wrote data');
      }
    });

    document.cookie = 'username=John Doe; path=/';
  */
}
