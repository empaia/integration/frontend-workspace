# Generic App Ui V3 Changelog

# 3.13.5 (2025-02-12)

### Fixes

* changed highlighting color for predefined color maps
* after zoom to annotation, annotation will still properly rendered

# 3.13.3 (2024-07-16)

### Refactor

* upgraded to angular 17

# 3.13.2 (2024-05-28)

### Refactor

* removed fetching of hidden annotations for slide-viewer

# 3.13.1 (2024-03-20)

### Refactor

* added job ids to filter in annotations query for the viewer

# 3.13.0 (2024-02-02)

### Features

* color maps settings of annotations and pixelmaps are saved in the backend

# 3.12.0 (2024-01-30)

### Features

* added color map inversion feature to annotation color maps
* inversion toggle button is deactivated on color map "Recommended"

# 3.11.0 (2024-01-29)

### Features

* added zoom to pixel map level functionality

# 3.10.3 (2024-01-26)

### Refactor

* changed panel division

# 3.10.2 (2024-01-11)

### Refactor

* updated to angular 16

# 3.10.1 (2024-01-05)

### Refactor

* changed annotation loading spinner position and appearance

# 3.10.0 (2024-01-03)

### Features

* use rendering hint of nominal pixelmap for color settings and item order

# 3.9.0 (2023-12-22)

### Features

* added support for annotation rendering hints of ead
* added pixelmaps support

# 3.8.9 (2023-12-06)

### Fixes

* run job button is immediately disabled after clicking

# 3.8.8 (2023-11-02)

### Fixes

* deactivate create job button in multi roi mode when examination is closed
* refactored error messages for annotations and jobs

# 3.8.7 (2023-10-18)

### Fixes

* added spinner when annotation was drawn in multi roi mode to prevent starting job to early

# 3.8.4 (2023-09-18)

### Refactor

* positioned tooltips above mouse cursor when ever possible

# 3.8.0 (2023-08-03)

### Features

* added multi user support

# 3.7.0 (2023-08-02)

### Refactor

* optimized slide pagination speed

# 3.6.1 (2023-07-17)

### Fixes

* expired token will not longer be shown as error message
* fix job polling if job completes before job output validation starts

# 3.6.0 (2023-06-30)

### Features

* primitives are sorted ascending via name
* classes and color classes are sorted ascending via id

# 3.5.0 (2023-06-21)

### Features

* multiple annotations can be created for one job

# 3.4.0 (2023-06-12)

### Features

* preloads all fonts from the start

# 3.3.2 (2023-06-08)

### Refactor

* refactored tooltips

# 3.3.0 (2023-06-06)

### Features

* added frontend pagination for slides in slides-list

# 3.2.3 (2023-04-21)

### Fixes

* primitives are only fetch with the selected jobs
* job status can reach the completed state
* enhanced job status is also available in standalone mode

### Features

* first job in preprocessing mode will be preselected
* jobs polling never stops in preprocessing mode

# 3.2.1 (2023-03-31)

### Fixes

* fixed job status indicator, showing complete status even if input/output validation is undefined/null

# 3.2.0 (2023-03-30)

### Features

* added an preprocessing mode
* preprocessing mode is active when an app supports it
* shows jobs instead of roi's in preprocessing mode
* job indicator was enhanced with input/output validation

# 3.1.1 (2023-03-24)

### Refactor

* use css for truncate text

# 3.1.0 (2023-03-24)

### Features

* added clipboard feature

# 3.0.0 (2023-03-20)

### Features

* updated to Angular 15

# 0.2.2 (2023-01-20)

### Fixes

* Slide-Viewer workspace adjusts for expanding or collapsing the side menu
* ROI's don't reappear after deselection
* Cluster can't be selected by mouse click
* Class color indicator have fixed width and height

# 0.2.0 (2022-12-22)

### Refactor

* Menu overhaul, added analysis menu with sub menus for classes, jobs/roi's and primitives

### Feature

* Highlight annotations in viewer by clicking annotations or selecting roi's in menu
* Annotations in menu are selectable
* Load an show primitives
* Allow input types of rectangle, circle and/or polygon when app is compatible
* Show classes with there colors


# 0.1.0 (2022-11-25)

### Feature

* The Generic App Ui checks if the app is compatible with rectangle as an input, if not shows an error message and it deactivates the annotation draw tools

# 0.0.1 (2022-11-24)

### Refactor

* created Generic App Ui V3 based on Generic App Ui V2
