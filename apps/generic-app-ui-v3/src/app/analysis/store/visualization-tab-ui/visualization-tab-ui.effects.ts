import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import * as AnnotationsActions from '@annotations/store/annotations/annotations.actions';
import { PixelMapsActions } from '@pixel-maps/store';
import { VisualizationTabUiActions } from './visualization-tab-ui.actions';
import { map } from 'rxjs/operators';
import { filterNullish } from '@shared/helper/rxjs-operators';


@Injectable()
export class VisualizationTabUiEffects {

  selectAnnotationsTab$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(AnnotationsActions.selectAnnotation),
      map(action => action.selected),
      filterNullish(),
      map(() => VisualizationTabUiActions.setTabIndex({ selectedTab: 0 }))
    );
  });

  selectPixelMapTab$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(PixelMapsActions.selectPixelMap),
      map(action => action.selected ? 1 : 0),
      map(selectedTab => VisualizationTabUiActions.setTabIndex({ selectedTab }))
    );
  });

  constructor(
    private readonly actions$: Actions
  ) {}
}
