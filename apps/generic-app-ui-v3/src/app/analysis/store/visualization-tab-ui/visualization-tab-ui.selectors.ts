import { createSelector } from '@ngrx/store';
import { selectAnalysisFeatureState } from '../analysis-feature.state';
import { VISUALIZATION_TAB_UI_FEATURE_KEY } from './visualization-tab-ui.reducer';

export const selectVisualizationTabUiState = createSelector(
  selectAnalysisFeatureState,
  (state) => state[VISUALIZATION_TAB_UI_FEATURE_KEY]
);

export const selectSelectedTabIndex = createSelector(
  selectVisualizationTabUiState,
  (state) => state.selectedTab
);
