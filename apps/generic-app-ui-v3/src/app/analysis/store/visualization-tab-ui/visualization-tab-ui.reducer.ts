import { createReducer, on } from '@ngrx/store';
import { VisualizationTabUiActions } from './visualization-tab-ui.actions';


export const VISUALIZATION_TAB_UI_FEATURE_KEY = 'visualizationTabUi';

export interface State {
  selectedTab: number;
}

export const initialState: State = {
  selectedTab: 0,
};

export const reducer = createReducer(
  initialState,
  on(VisualizationTabUiActions.setTabIndex, (state, { selectedTab }): State => ({
    ...state,
    selectedTab,
  }))
);
