import { createActionGroup, props } from '@ngrx/store';

export const VisualizationTabUiActions = createActionGroup({
  source: 'Visualization Tab Ui',
  events: {
    'Set Tab Index': props<{ selectedTab: number }>()
  }
});
