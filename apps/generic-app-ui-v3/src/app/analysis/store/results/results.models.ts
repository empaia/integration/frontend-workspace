import { AnnotationEntity } from 'slide-viewer';
import { SlideEntity } from '@slides/store';
import {
  AppV3BoolPrimitive,
  AppV3FloatPrimitive,
  AppV3IntegerPrimitive,
  AppV3StringPrimitive,
} from 'empaia-api-lib';
import { ErrorMap, PRIMITIVES_LOAD_ERROR } from '@menu/models/ui.models';

export type BoolPrimitive = AppV3BoolPrimitive;
export type FloatPrimitive = AppV3FloatPrimitive;
export type IntegerPrimitive = AppV3IntegerPrimitive;
export type StringPrimitive = AppV3StringPrimitive;

export type PrimitiveEntity = IntegerPrimitive | FloatPrimitive | BoolPrimitive | StringPrimitive;

export type ResultEntity = SlideEntity | AnnotationEntity;

export interface ReferenceEntity {
  id: string;
  type: 'wsi' | 'annotation';
}

export const RESULT_ERROR_MAP: ErrorMap = {
  '[App/Results] Load Primitives Failure': PRIMITIVES_LOAD_ERROR,
};

// we always wan't to use english for string comparison
// at primitive names
export const STRING_COMPARATOR_LOCAL = 'en';

export function comparePrimitiveNamesAsc(a: PrimitiveEntity, b: PrimitiveEntity): number {
  return a.name.trim().toLowerCase().localeCompare(b.name.trim().toLowerCase(), STRING_COMPARATOR_LOCAL);
}
