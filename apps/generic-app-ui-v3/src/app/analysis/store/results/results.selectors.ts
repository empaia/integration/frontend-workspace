import { createSelector } from '@ngrx/store';
import { selectSelectedSlide } from '@slides/store/slides/slides.selectors';
import {
  State as ModuleState,
  selectAnalysisFeatureState,
} from '../analysis-feature.state';
import { primitiveAdapter } from './results.reducer';
import { selectReferenceAnnotation } from '@annotations/store/annotations-ui/annotations-ui.selectors';

const {
  selectAll,
  selectEntities,
} = primitiveAdapter.getSelectors();


export const selectResultsState = createSelector(
  selectAnalysisFeatureState,
  (state: ModuleState) => state.results
);

export const selectAllResults = createSelector(
  selectResultsState,
  selectAll,
);

export const selectResultEntities = createSelector(
  selectResultsState,
  selectEntities,
);

export const selectResultsLoaded = createSelector(
  selectResultsState,
  (state) => state.loaded
);

export const selectResultsReference = createSelector(
  selectResultsState,
  (state) => state.reference
);

export const selectCurrentReferenceEntity = createSelector(
  selectResultsReference,
  selectSelectedSlide,
  selectReferenceAnnotation,
  (reference, slide, annotation) => reference ? reference.type === 'wsi' ? slide : annotation : undefined
);
