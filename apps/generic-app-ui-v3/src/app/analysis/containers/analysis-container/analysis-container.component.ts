import { Observable } from 'rxjs';
import { ChangeDetectionStrategy, Component } from '@angular/core';
import { MenuEntity } from '@menu/models/menu.models';
import { Store } from '@ngrx/store';
import { MenuSelectors } from '@menu/store';
import { EadSelectors } from '@ead/store';
import { EadRoiInputType, EmpaiaAppDescriptionV3Mode } from 'empaia-ui-commons';

@Component({
  selector: 'app-analysis-container',
  templateUrl: './analysis-container.component.html',
  styleUrls: ['./analysis-container.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AnalysisContainerComponent {
  public analysisMenu$: Observable<MenuEntity>;
  public activeEadMode$: Observable<EmpaiaAppDescriptionV3Mode>;
  public activeRoiMode$: Observable<EadRoiInputType>;

  constructor(private store: Store) {
    this.analysisMenu$ = this.store.select(MenuSelectors.selectAnalysisMenu);
    this.activeEadMode$ = this.store.select(EadSelectors.selectActiveMode);
    this.activeRoiMode$ = this.store.select(EadSelectors.selectActiveRoiMode);
  }
}
