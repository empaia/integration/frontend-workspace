import { ChangeDetectionStrategy, Component } from '@angular/core';
import { Job, JobSelector, JobsActions, JobsSelectors } from '@jobs/store';
import { Dictionary } from '@ngrx/entity';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-preprocessing-jobs-container',
  templateUrl: './preprocessing-jobs-container.component.html',
  styleUrls: ['./preprocessing-jobs-container.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PreprocessingJobsContainerComponent {
  public jobEntities$: Observable<Job[]>;
  public jobsSelections$: Observable<Dictionary<JobSelector>>;

  constructor(private store: Store) {
    this.jobEntities$ = this.store.select(JobsSelectors.selectAllJobs);
    this.jobsSelections$ = this.store.select(JobsSelectors.selectJobSelectionEntities);
  }

  public onJobSelection(jobSelection: JobSelector): void {
    this.store.dispatch(JobsActions.setUniqueJobSelection({ jobSelection }));
  }
}
