import { ChangeDetectionStrategy, Component } from '@angular/core';
import { CollectionsSelectors } from '@collections/store';
import { EadSelectors } from '@ead/store';
import { Job, JobSelector, JobsActions, JobsSelectors } from '@jobs/store';
import { Dictionary } from '@ngrx/entity';
import { Store } from '@ngrx/store';
import { ScopeSelectors } from '@scope/store';
import { EadRoiInputType } from 'empaia-ui-commons';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-standalone-jobs-container',
  templateUrl: './standalone-jobs-container.component.html',
  styleUrls: ['./standalone-jobs-container.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class StandaloneJobsContainerComponent {
  public jobEntities$: Observable<Job[]>;
  public jobsSelections$: Observable<Dictionary<JobSelector>>;
  public multiRoiMode$: Observable<EadRoiInputType>;
  public collectionsLoaded$: Observable<boolean>;
  public isExaminationClosed$: Observable<boolean>;

  constructor(private store: Store) {
    this.jobEntities$ = this.store.select(JobsSelectors.selectAllJobs);
    this.jobsSelections$ = this.store.select(JobsSelectors.selectJobSelectionEntities);
    this.multiRoiMode$ = this.store.select(EadSelectors.selectActiveRoiMode);
    this.collectionsLoaded$ = this.store.select(CollectionsSelectors.selectCollectionsLoaded);
    this.isExaminationClosed$ = this.store.select(ScopeSelectors.selectExaminationState);
  }

  public onJobSelection(jobSelection: JobSelector): void {
    this.store.dispatch(JobsActions.setUniqueJobSelection({ jobSelection }));
  }

  public onJobCreation(): void {
    this.store.dispatch(JobsActions.createJob());
  }

  public onJobStart(jobId: string): void {
    this.store.dispatch(JobsActions.runJob({ jobId }));
  }

  public onJobDeletion(jobId: string): void {
    this.store.dispatch(JobsActions.deleteJob({ jobId }));
  }

  public onStopRunningJob(jobId: string): void {
    this.store.dispatch(JobsActions.openStopJobDialog({ jobId }));
  }
}
