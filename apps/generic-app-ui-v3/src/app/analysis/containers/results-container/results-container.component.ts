import { AnnotationsActions } from '@annotations/store';
import { Observable } from 'rxjs';
import { ChangeDetectionStrategy, Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { PrimitiveEntity, ResultEntity, ResultsSelectors } from '@analysis/store';
import { PixelMap, PixelMapsActions, PixelMapsSelectors } from '@pixel-maps/store';

@Component({
  selector: 'app-results-container',
  templateUrl: './results-container.component.html',
  styleUrls: ['./results-container.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ResultsContainerComponent {
  public resultEntity$: Observable<ResultEntity | undefined>;
  public primitiveEntities$: Observable<PrimitiveEntity[]>;
  public primitivesLoaded$: Observable<boolean>;
  public pixelMaps$: Observable<PixelMap[]>;
  public pixelMapsLoaded$: Observable<boolean>;
  public selectedPixelMapId$: Observable<string | null | undefined>;

  constructor(private store: Store) {
    this.resultEntity$ = this.store.select(ResultsSelectors.selectCurrentReferenceEntity);
    this.primitiveEntities$ = this.store.select(ResultsSelectors.selectAllResults);
    this.primitivesLoaded$ = this.store.select(ResultsSelectors.selectResultsLoaded);
    this.pixelMaps$ = this.store.select(PixelMapsSelectors.selectAllPixelMaps);
    this.pixelMapsLoaded$ = this.store.select(PixelMapsSelectors.selectPixelMapsLoaded);
    this.selectedPixelMapId$ = this.store.select(PixelMapsSelectors.selectSelectedPixelMapId);
  }

  public onDeselectReference(): void {
    this.store.dispatch(AnnotationsActions.selectAnnotation({}));
  }

  public onPixelMapSelection(selected?: string | null): void {
    this.store.dispatch(PixelMapsActions.selectPixelMap({ selected }));
  }

  public onZoomToPixelMap(selected?: string | null): void {
    this.store.dispatch(PixelMapsActions.zoomToPixelMapLevel({ selected }));
  }
}
