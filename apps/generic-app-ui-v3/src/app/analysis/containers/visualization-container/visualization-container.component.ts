import { Dictionary } from '@ngrx/entity';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { ChangeDetectionStrategy, Component } from '@angular/core';
import { ClassesSelectors, ClassesActions } from '@classes/store';
import { CategoricalColorMap, ColorMapsActions, ColorMapsSelectors, PixelMap, PixelMapsSelectors, RenderingHintsSelectors, SequentialColorMap } from '@pixel-maps/store';
import { VisualizationTabUiActions, VisualizationTabUiSelectors } from '@analysis/store';
import { AnnotationsColorMapsActions, AnnotationsColorMapsSelectors } from '@annotations/store';
import { EadSelectors } from '@ead/store';
import { ClassColorMapping } from 'slide-viewer';
import { AnnotationColorMap, ClassEntity, ClassSelector, RenderingHint } from 'empaia-ui-commons';

@Component({
  selector: 'app-visualization-container',
  templateUrl: './visualization-container.component.html',
  styleUrls: ['./visualization-container.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class VisualizationContainerComponent {
  public classEntities$: Observable<ClassEntity[]>;
  public classesDict$: Observable<Dictionary<ClassEntity>>;
  public annotationColorMaps$: Observable<AnnotationColorMap[]>;
  public selectedAnnotationColorMapId$: Observable<string | undefined>;
  public selectedAnnotationColorMap$: Observable<AnnotationColorMap | undefined>;
  public annotationColorMapInversion$: Observable<boolean>;
  public annotationColorMapInversionDisableState$: Observable<boolean>;
  public classColorMapping$: Observable<ClassColorMapping | undefined>;
  public classSelections$: Observable<Dictionary<ClassSelector>>;
  public pixelMap$: Observable<PixelMap | null | undefined>;
  public colorMaps$: Observable<SequentialColorMap[] | CategoricalColorMap[] | undefined>;
  public selectedColorMap$: Observable<SequentialColorMap | CategoricalColorMap | undefined>;
  public colorMapInversion$: Observable<boolean>;
  public colorMapInversionDisableState$: Observable<boolean>;
  public selectedTabIndex$: Observable<number>;
  public hasEadAnnotationRenderingHint$: Observable<boolean>;
  public renderingHintEntities$: Observable<Dictionary<RenderingHint>>;
  public hasPixelMapsRenderingHint$: Observable<boolean>;

  constructor(private store: Store) {
    this.classEntities$ = this.store.select(ClassesSelectors.selectAllClasses);
    this.classesDict$ = this.store.select(ClassesSelectors.selectClassEntities);
    this.annotationColorMaps$ = this.store.select(AnnotationsColorMapsSelectors.selectAllAnnotationColorMaps);
    this.selectedAnnotationColorMapId$ = this.store.select(AnnotationsColorMapsSelectors.selectSelectedAnnotationColorMapId);
    this.selectedAnnotationColorMap$ = this.store.select(AnnotationsColorMapsSelectors.selectSelectedAnnotationColorMap);
    this.annotationColorMapInversion$ = this.store.select(AnnotationsColorMapsSelectors.selectInversionState);
    this.annotationColorMapInversionDisableState$ = this.store.select(AnnotationsColorMapsSelectors.selectInversionDeactivationState);
    this.classColorMapping$ = this.store.select(AnnotationsColorMapsSelectors.selectSelectedAnnotationClassColorMapping);
    this.classSelections$ = this.store.select(ClassesSelectors.selectClassSelectionEntity);
    this.pixelMap$ = this.store.select(PixelMapsSelectors.selectSelectedPixelMap);
    this.colorMaps$ = this.store.select(ColorMapsSelectors.selectAllColorMapsForPixelMapType);
    this.selectedColorMap$ = this.store.select(ColorMapsSelectors.selectSelectedColorMap);
    this.colorMapInversion$ = this.store.select(ColorMapsSelectors.selectInversionState);
    this.colorMapInversionDisableState$ = this.store.select(ColorMapsSelectors.selectInversionDeactivationState);
    this.selectedTabIndex$ = this.store.select(VisualizationTabUiSelectors.selectSelectedTabIndex);
    this.hasEadAnnotationRenderingHint$ = this.store.select(EadSelectors.selectHasEadAnnotationRenderingHint);
    this.renderingHintEntities$ = this.store.select(RenderingHintsSelectors.selectRenderingHintEntities);
    this.hasPixelMapsRenderingHint$ = this.store.select(RenderingHintsSelectors.selectHasPixelMapRenderingHint);
  }

  public onClassSelectionChanged(classSelection: ClassSelector): void {
    this.store.dispatch(ClassesActions.setClassSelection({ classSelection }));
  }

  public onColorMapSelectionChanged(selectedColorMap: SequentialColorMap | CategoricalColorMap | undefined): void {
    this.store.dispatch(ColorMapsActions.setColorMap({ selectedColorMap }));
  }

  public onColorMapInversionChanged(invert: boolean): void {
    this.store.dispatch(ColorMapsActions.setColorMapInversionOfSelectedPixelMap({ invert }));
  }

  public onSelectedTabIndexChanged(selectedTab: number): void {
    this.store.dispatch(VisualizationTabUiActions.setTabIndex({ selectedTab }));
  }

  public onAnnotationColorMapChanged(selected: string): void {
    this.store.dispatch(AnnotationsColorMapsActions.selectAnnotationColorMap({ selected }));
  }

  public onAnnotationColorMapInversionChanged(invert: boolean): void {
    this.store.dispatch(AnnotationsColorMapsActions.setColorMapInversion({
      invert
    }));
  }
}
