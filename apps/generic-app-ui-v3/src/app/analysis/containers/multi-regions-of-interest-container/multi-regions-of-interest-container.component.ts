import { ChangeDetectionStrategy, Component } from '@angular/core';
import { AnnotationCollectionEntity, AnnotationsActions, AnnotationsSelectors, AnnotationsViewerActions } from '@annotations/store';
import { JobsSelectors } from '@jobs/store';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-multi-regions-of-interest-container',
  templateUrl: './multi-regions-of-interest-container.component.html',
  styleUrls: ['./multi-regions-of-interest-container.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MultiRegionsOfInterestContainerComponent {
  public regionsOfInterest$: Observable<AnnotationCollectionEntity[]>;
  public selectedRoiId$: Observable<string | undefined>;
  public regionsOfInterestLoaded$: Observable<boolean>;

  constructor(private store: Store) {
    this.regionsOfInterest$ = this.store.select(JobsSelectors.selectAllJobCollectionsRois);
    this.selectedRoiId$ = this.store.select(AnnotationsSelectors.selectSelectedAnnotationId);
    this.regionsOfInterestLoaded$ = this.store.select(JobsSelectors.selectAllJobCollectionsRoisLoaded);
  }

  public onAnnotationSelection(selected: string): void {
    this.store.dispatch(AnnotationsActions.selectAnnotation({ selected }));
  }

  public onZoomToAnnotation(focus: AnnotationCollectionEntity): void {
    this.store.dispatch(AnnotationsViewerActions.zoomToAnnotation({ focus }));
  }
}
