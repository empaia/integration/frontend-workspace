import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { MaterialModule } from '@material/material.module';
import { SharedModule } from '@shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ResultItemComponent } from './components/result-item/result-item.component';
import { ResultsComponent } from './components/results/results.component';
import { ResultsLabelComponent } from './components/results-label/results-label.component';
import { ResultsContainerComponent } from './containers/results-container/results-container.component';
import { ANALYSIS_MODULE_FEATURE_KEY, reducers } from '@analysis/store/analysis-feature.state';
import { AnalysisContainerComponent } from './containers/analysis-container/analysis-container.component';
import { VisualizationContainerComponent } from './containers/visualization-container/visualization-container.component';
import { RegionsOfInterestContainerComponent } from './containers/regions-of-interest-container/regions-of-interest-container.component';
import { AnalysisLabelComponent } from './components/analysis-label/analysis-label.component';
import { ClassesComponent } from './components/classes/classes.component';
import { RegionsOfInterestComponent } from './components/regions-of-interest/regions-of-interest.component';
import { RegionOfInterestItemComponent } from './components/region-of-interest-item/region-of-interest-item.component';
import { ResultsEffects } from '@analysis/store/results/results.effects';
import { LetDirective } from '@ngrx/component';
import { JobsComponent } from './components/jobs/jobs.component';
import { JobItemComponent } from './components/job-item/job-item.component';
import { PushPipe } from '@ngrx/component';
import { MultiRegionsOfInterestContainerComponent } from './containers/multi-regions-of-interest-container/multi-regions-of-interest-container.component';
import { PreprocessingJobsContainerComponent } from './containers/preprocessing-jobs-container/preprocessing-jobs-container.component';
import { StandaloneJobsContainerComponent } from './containers/standalone-jobs-container/standalone-jobs-container.component';
import { AnnotationItemComponent } from './components/annotation-item/annotation-item.component';
import { AnnotationsComponent } from './components/annotations/annotations.component';
import { PixelMapElementClassItemComponent } from './components/pixel-map-element-class-item/pixel-map-element-class-item.component';
import { ElementClassMapItemComponent } from './components/element-class-map-item/element-class-map-item.component';
import { ElementClassMapsComponent } from './components/element-class-maps/element-class-maps.component';
import { PixelMapSettingsComponent } from './components/pixel-map-settings/pixel-map-settings.component';
import { ResultPixelMapItemComponent } from './components/result-pixel-map-item/result-pixel-map-item.component';
import { VisualizationTabUiEffects } from './store';
import { CommonUiModule } from 'empaia-ui-commons';



@NgModule({
  declarations: [
    ResultItemComponent,
    ResultsComponent,
    ResultsLabelComponent,
    ResultsContainerComponent,
    AnalysisContainerComponent,
    VisualizationContainerComponent,
    RegionsOfInterestContainerComponent,
    AnalysisLabelComponent,
    ClassesComponent,
    RegionsOfInterestComponent,
    RegionOfInterestItemComponent,
    JobsComponent,
    JobItemComponent,
    MultiRegionsOfInterestContainerComponent,
    PreprocessingJobsContainerComponent,
    StandaloneJobsContainerComponent,
    AnnotationItemComponent,
    AnnotationsComponent,
    PixelMapElementClassItemComponent,
    ElementClassMapItemComponent,
    ElementClassMapsComponent,
    PixelMapSettingsComponent,
    ResultPixelMapItemComponent,
  ],
  imports: [
    CommonModule,
    CommonUiModule,
    MaterialModule,
    SharedModule,
    LetDirective,
    PushPipe,
    StoreModule.forFeature(
      ANALYSIS_MODULE_FEATURE_KEY,
      reducers
    ),
    EffectsModule.forFeature([
      ResultsEffects,
      VisualizationTabUiEffects,
    ])
  ],
  exports: [
    AnalysisContainerComponent
  ]
})
export class AnalysisModule { }
