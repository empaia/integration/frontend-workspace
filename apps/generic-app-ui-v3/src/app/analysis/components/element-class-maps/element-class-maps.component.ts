import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { Dictionary } from '@ngrx/entity';
import { ElementClassMapping } from '@pixel-maps/store';
import { ClassEntity, RenderingHint } from 'empaia-ui-commons';

@Component({
  selector: 'app-element-class-maps',
  templateUrl: './element-class-maps.component.html',
  styleUrls: ['./element-class-maps.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ElementClassMapsComponent {
  @Input() public elementClassMapping!: ElementClassMapping[];
  @Input() public classEntities!: Dictionary<ClassEntity>;
  @Input() public renderingHintEntities!: Dictionary<RenderingHint>;
  @Input() public hasRenderingHint = false;

  public trackByElementClassMapping(_index: number, element: ElementClassMapping): number {
    return element.number_value;
  }
}
