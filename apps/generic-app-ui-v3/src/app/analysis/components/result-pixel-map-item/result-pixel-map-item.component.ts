import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { PixelMap } from '@pixel-maps/store';

@Component({
  selector: 'app-result-pixel-map-item',
  templateUrl: './result-pixel-map-item.component.html',
  styleUrls: ['./result-pixel-map-item.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ResultPixelMapItemComponent {
  @Input() public pixelMap!: PixelMap;
  @Input() public selected!: boolean;

  @Output() public pixelMapSelected = new EventEmitter<string | null | undefined>();
  @Output() public zoomToPixelMap = new EventEmitter<string | null | undefined>();

  public onPixelMapSelect(id: string | null | undefined): void {
    this.pixelMapSelected.emit(id);
  }

  public onZoomToPixelMap(id: string | null | undefined, event: MouseEvent): void {
    event.stopPropagation();
    this.zoomToPixelMap.emit(id);
  }
}
