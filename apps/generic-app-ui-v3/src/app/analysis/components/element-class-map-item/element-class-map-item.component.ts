import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { Dictionary } from '@ngrx/entity';
import { ElementClassMapping } from '@pixel-maps/store';
import { ClassEntity } from 'empaia-ui-commons';

@Component({
  selector: 'app-element-class-map-item',
  templateUrl: './element-class-map-item.component.html',
  styleUrls: ['./element-class-map-item.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ElementClassMapItemComponent {
  @Input() public elementClassMappingItem!: ElementClassMapping;
  @Input() public classEntities!: Dictionary<ClassEntity>;
}
