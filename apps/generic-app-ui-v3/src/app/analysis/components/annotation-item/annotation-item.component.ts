import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { AnnotationCollectionEntity } from '@annotations/store';

@Component({
  selector: 'app-annotation-item',
  templateUrl: './annotation-item.component.html',
  styleUrls: ['./annotation-item.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AnnotationItemComponent {
  @Input() public annotation!: AnnotationCollectionEntity;
  @Input() public selected!: boolean;

  @Output() public annotationSelected = new EventEmitter<string>();
  @Output() public zoomToAnnotation = new EventEmitter<AnnotationCollectionEntity>();
}
