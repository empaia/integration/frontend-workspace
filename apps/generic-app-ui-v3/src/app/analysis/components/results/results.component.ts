import { PrimitiveEntity } from '@analysis/store';
import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { PixelMap } from '@pixel-maps/store';

@Component({
  selector: 'app-results',
  templateUrl: './results.component.html',
  styleUrls: ['./results.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ResultsComponent {
  @Input() public primitiveEntities!: PrimitiveEntity[];
  @Input() public primitivesLoaded = true;
  @Input() public pixelMaps!: PixelMap[];
  @Input() public pixelMapsLoaded = true;
  @Input() public selectedPixelMapId!: string | undefined;

  @Output() public pixelMapSelected = new EventEmitter<string | null | undefined>();
  @Output() public zoomToPixelMap = new EventEmitter<string | null | undefined>();

  public trackByPrimitive(_index: number, primitive: PrimitiveEntity): string {
    return primitive.id as string;
  }

  public trackByPixelMap(_index: number, pixelMap: PixelMap): string {
    return pixelMap.id as string;
  }
}
