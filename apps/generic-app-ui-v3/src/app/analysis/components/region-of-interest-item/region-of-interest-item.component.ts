import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { MatCheckboxChange } from '@angular/material/checkbox';
import { JobRoiEntity, JobSelector } from '@jobs/store';
import { AnnotationEntity } from 'slide-viewer';

@Component({
  selector: 'app-region-of-interest-item',
  templateUrl: './region-of-interest-item.component.html',
  styleUrls: ['./region-of-interest-item.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RegionOfInterestItemComponent {
  @Input() public jobRoiEntity!: JobRoiEntity;
  @Input() public selected!: boolean;
  @Input() public selectedRoi!: boolean;

  @Output() public changeSelection = new EventEmitter<JobSelector>();
  @Output() public roiClicked = new EventEmitter<string>();
  @Output() public zoomToAnnotation = new EventEmitter<AnnotationEntity>();
  @Output() public stopRunningJob = new EventEmitter<string>();

  public onCheckboxChange(change: MatCheckboxChange): void {
    const jobSelector: JobSelector = {
      id: this.jobRoiEntity.job.id,
      checked: change.checked
    };
    this.changeSelection.emit(jobSelector);
  }

  public onRoiClicked(id: string): void {
    if (this.selected) {
      this.roiClicked.emit(id);
    }
  }
}
