import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { AnnotationCollectionEntity } from '@annotations/store';

@Component({
  selector: 'app-annotations',
  templateUrl: './annotations.component.html',
  styleUrls: ['./annotations.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AnnotationsComponent {
  @Input() public annotations!: AnnotationCollectionEntity[];
  @Input() public selectedAnnotation: string | undefined;
  @Input() public annotationsLoaded = true;

  @Output() public annotationSelected = new EventEmitter<string>();
  @Output() public zoomToAnnotation = new EventEmitter<AnnotationCollectionEntity>();

  public trackByAnnotation(_index: number, annotation: AnnotationCollectionEntity): string {
    return annotation.id;
  }
}
