import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { Job, JobSelector, JobStatus } from '@jobs/store';

@Component({
  selector: 'app-job-item',
  templateUrl: './job-item.component.html',
  styleUrls: ['./job-item.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class JobItemComponent {
  @Input() public job!: Job;
  @Input() public selected!: boolean;
  @Input() public multiRoiMode!: boolean;
  @Input() public collectionsLoaded = false;

  @Output() public jobSelected = new EventEmitter<JobSelector>();
  @Output() public startJob = new EventEmitter<string>();
  @Output() public deleteJob = new EventEmitter<string>();
  @Output() public stopRunningJob = new EventEmitter<string>();

  public readonly JOB_STATUS = JobStatus;
}
