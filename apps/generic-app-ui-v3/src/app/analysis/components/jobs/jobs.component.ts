import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { MatRadioChange } from '@angular/material/radio';
import { Job, JobSelector } from '@jobs/store';
import { Dictionary } from '@ngrx/entity';

@Component({
  selector: 'app-jobs',
  templateUrl: './jobs.component.html',
  styleUrls: ['./jobs.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class JobsComponent {
  @Input() public jobEntities!: Job[];
  @Input() public jobsSelections!: Dictionary<JobSelector>;
  @Input() public multiRoiMode!: boolean;
  @Input() public collectionsLoaded!: boolean;

  @Output() public changeJobSelection = new EventEmitter<JobSelector>();
  @Output() public startJob = new EventEmitter<string>();
  @Output() public deleteJob = new EventEmitter<string>();
  @Output() public stopRunningJob = new EventEmitter<string>();

  public trackByJobId(_index: number, job: Job): string {
    return job.id;
  }

  public onRadioGroupChange(event: MatRadioChange): void {
    const selected: JobSelector = {
      id: event.value,
      checked: true,
    };
    this.changeJobSelection.emit(selected);
  }
}
