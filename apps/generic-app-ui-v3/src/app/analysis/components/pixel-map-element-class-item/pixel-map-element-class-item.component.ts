import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { CategoricalColorMap, PixelMap, SequentialColorMap } from '@pixel-maps/store';
import { DEFAULT_CONNECTED_POSITIONS } from './pixel-map-element-class-item.models';
import { Dictionary } from '@ngrx/entity';
import { ClassEntity, RenderingHint } from 'empaia-ui-commons';

@Component({
  selector: 'app-pixel-map-element-class-item',
  templateUrl: './pixel-map-element-class-item.component.html',
  styleUrls: ['./pixel-map-element-class-item.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PixelMapElementClassItemComponent {
  @Input() public pixelMap!: PixelMap;
  @Input() public colorMaps: SequentialColorMap[] | CategoricalColorMap[] | undefined;
  @Input() public selectedColorMap: SequentialColorMap | CategoricalColorMap | undefined;
  @Input() public colorMapInversion!: boolean;
  @Input() public colorMapInversionDisableState!: boolean;
  @Input() public classEntities!: Dictionary<ClassEntity>;
  @Input() public renderingHintEntities!: Dictionary<RenderingHint>;
  @Input() public hasRenderingHint!: boolean;

  @Output() public colorMapSelectionChanged = new EventEmitter<SequentialColorMap | CategoricalColorMap | undefined>();
  @Output() public colorMapInversionChanged = new EventEmitter<boolean>();

  public readonly CONNECTED_POSITIONS = DEFAULT_CONNECTED_POSITIONS;
  public isSettingOpen = false;
}
