import { ConnectedPosition } from '@angular/cdk/overlay';

export const DEFAULT_CONNECTED_POSITIONS: ConnectedPosition[] = [
  {
    offsetX: 33,
    originX: 'center',
    originY: 'top',
    overlayX: 'end',
    overlayY: 'bottom'
  }
];
