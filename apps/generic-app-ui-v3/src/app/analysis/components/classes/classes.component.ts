import { Dictionary } from '@ngrx/entity';
import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { ClassColorMapping } from 'slide-viewer';
import { AnnotationColorMap, ClassEntity, ClassSelector } from 'empaia-ui-commons';

@Component({
  selector: 'app-classes',
  templateUrl: './classes.component.html',
  styleUrls: ['./classes.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ClassesComponent {
  @Input() public classEntities!: ClassEntity[];
  @Input() public classesDict!: Dictionary<ClassEntity>;
  @Input() public annotationColorMaps!: AnnotationColorMap[];
  @Input() public selectedAnnotationColorMapId?: string | undefined;
  @Input() public selectedAnnotationColorMap?: AnnotationColorMap;
  @Input() public classColorMapping: ClassColorMapping | undefined;
  @Input() public classSelections!: Dictionary<ClassSelector>;
  @Input() public hasEadRenderingHint = false;
  @Input() public annotationColorMapInversion!: boolean;
  @Input() public annotationColorMapInversionDisableState = false;

  @Output() public changeClassSelection = new EventEmitter<ClassSelector>();
  @Output() public changeAnnotationColorMap = new EventEmitter<string>();
  @Output() public changeAnnotationColorMapInversion = new EventEmitter<boolean>();

  public trackByClassEntity(_index: number, classEntity: ClassEntity): string {
    return classEntity.id;
  }
}
