import { MockComponents } from 'ng-mocks';
import { Spectator, createComponentFactory } from '@ngneat/spectator/jest';
import { RegionsOfInterestComponent } from './regions-of-interest.component';
import { RegionOfInterestItemComponent } from '../region-of-interest-item/region-of-interest-item.component';

describe('RegionsOfInterestComponent', () => {
  let spectator: Spectator<RegionsOfInterestComponent>;
  const createComponent = createComponentFactory({
    component: RegionsOfInterestComponent,
    declarations: [
      MockComponents(
        RegionOfInterestItemComponent
      )
    ]
  });

  beforeEach(() => spectator = createComponent());

  it('should create', () => {
    expect(spectator.component).toBeTruthy();
  });
});
