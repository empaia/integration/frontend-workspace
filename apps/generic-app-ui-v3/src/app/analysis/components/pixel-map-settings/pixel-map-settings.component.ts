import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { MatSelectChange } from '@angular/material/select';
import { MatSlideToggleChange } from '@angular/material/slide-toggle';
import { CategoricalColorMap, SequentialColorMap } from '@pixel-maps/store';

@Component({
  selector: 'app-pixel-map-settings',
  templateUrl: './pixel-map-settings.component.html',
  styleUrls: ['./pixel-map-settings.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PixelMapSettingsComponent {
  @Input() public colorMaps: SequentialColorMap[] | CategoricalColorMap[] | undefined;
  @Input() public selectedColorMap: SequentialColorMap | CategoricalColorMap | undefined;
  @Input() public colorMapInversion!: boolean;
  @Input() public colorMapInversionDisableState = false;

  @Output() public colorMapSelectionChanged = new EventEmitter<SequentialColorMap | CategoricalColorMap | undefined>();
  @Output() public colorMapInversionChanged = new EventEmitter<boolean>();

  public onColorMapSelectionChanged(event: MatSelectChange): void {
    const selectedColorMap = event.value;
    this.colorMapSelectionChanged.emit(selectedColorMap);
  }

  public onInversionChanged(event: MatSlideToggleChange): void {
    const invert = event.checked;
    this.colorMapInversionChanged.emit(invert);
  }
}
