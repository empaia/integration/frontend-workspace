import { Token } from 'vendor-app-communication-interface';
import { createAction, props } from '@ngrx/store';

export const setAccessToken = createAction(
  '[APP/Token] Set Access Token',
  props<{ token: Token }>()
);

export const requestNewToken = createAction(
  '[APP/Token] Request New Token',
);
