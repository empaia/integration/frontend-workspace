import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';
import { PIXEL_MAPS_MODULE_FEATURE_KEY, reducers } from './store/pixel-maps-feature.state';
import { EffectsModule } from '@ngrx/effects';
import { ColorMapsEffects, PixelMapsEffects, RenderingHintsEffects } from './store';
import { MaterialModule } from '@material/material.module';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    MaterialModule,
    StoreModule.forFeature(
      PIXEL_MAPS_MODULE_FEATURE_KEY,
      reducers
    ),
    EffectsModule.forFeature([
      ColorMapsEffects,
      PixelMapsEffects,
      RenderingHintsEffects,
    ])
  ]
})
export class PixelMapsModule { }
