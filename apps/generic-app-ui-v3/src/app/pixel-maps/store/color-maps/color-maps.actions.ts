import { createActionGroup, props } from '@ngrx/store';
import { CategoricalColorMap, SequentialColorMap } from './color-maps.models';
import { ColorMapFn } from '../pixel-maps/pixel-maps.models';
import { ElementColorMapping } from 'pixelmap-rendering-collection';

export const ColorMapsActions = createActionGroup({
  source: 'Color Maps Actions',
  events: {
    'Set Color Map': props<{
      selectedColorMap?: SequentialColorMap | CategoricalColorMap | undefined,
    }>(),
    'Set Sequential Color Map': props<{
      selectedSequential?: SequentialColorMap | undefined
    }>(),
    'Set Divergent Color Map': props<{
      selectedDivergent?: SequentialColorMap | undefined
    }>(),
    'Set Nominal Color Map': props<{
      selectedNominal?: CategoricalColorMap | undefined
    }>(),
    'Set All Selected Color Maps': props<{
      selectedSequential: SequentialColorMap | undefined,
      selectedDivergent: SequentialColorMap | undefined,
      selectedNominal: CategoricalColorMap | undefined,
      invert: boolean,
      inversionDisabled: boolean
    }>(),
    'Add Nominal Color Map': props<{
      nominalColorMap: CategoricalColorMap,
    }>(),
    'Set Sequential Or Divergent Color Map': props<{
      selected?: SequentialColorMap | undefined,
    }>(),
    'Set Color Map Of Selected Pixel Map': props<{
      colorMapFn?: ColorMapFn | ElementColorMapping | undefined,
    }>(),
    'Set Color Map Inversion Of Selected Pixel Map': props<{
      invert: boolean,
    }>(),
    'Set Color Map Inversion State': props<{
      inversionDisabled: boolean
    }>(),
  }
});
