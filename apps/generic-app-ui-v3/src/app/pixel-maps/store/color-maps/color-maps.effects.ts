/* eslint-disable @typescript-eslint/ban-ts-comment */
import { Injectable } from '@angular/core';
import { Actions, concatLatestFrom, createEffect, ofType } from '@ngrx/effects';
import { concatMap, filter, map } from 'rxjs';
import { ColorMapsActions } from './color-maps.actions';
import { PixelMapsActions } from '../pixel-maps/pixel-maps.actions';
import * as ColorMapsSelectors from './color-maps.selectors';
import * as PixelMapsSelectors from '@pixel-maps/store/pixel-maps/pixel-maps.selectors';
import * as PixelMapTileRequestersSelectors from '@pixel-maps/store/pixel-map-tile-requesters/pixel-map-tile-requesters.selectors';
import * as RenderingHintsSelectors from '../rendering-hints/rendering-hints.selectors';
import { Store } from '@ngrx/store';
import { filterNullish } from '@shared/helper/rxjs-operators';
import { PixelMapsV3ConversionService } from '@pixel-maps/services/pixel-maps-conversion.service';
import { CategoricalColorMap, SequentialColorMap, isSequentialColorMap } from './color-maps.models';
import { CanvasDrawCache, PixelMapCredentialManager } from 'pixelmap-rendering-collection';
import {
  RECOMMENDED_PIXEL_MAP_COLOR_MAP_ID,
  convertElementClassMappingToElementColorMapping,
  isDiscretePixelMap,
  isNominalPixelMap
} from 'empaia-ui-commons';
import { ContinuousPixelMap, DiscretePixelMap, NominalPixelMap } from '../pixel-maps/pixel-maps.models';
import { UserStorageActions } from '@storage/store';



@Injectable()
export class ColorMapsEffects {

  setColorMapFnToRequesters$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ColorMapsActions.setColorMapOfSelectedPixelMap),
      map(action => action.colorMapFn),
      concatLatestFrom(() => [
        this.store.select(PixelMapTileRequestersSelectors.selectPixelMapTileRequestersCount)
      ]),
      map(([colorMapFn, count]) => {
        const channels = [...Array(count).keys()];
        PixelMapCredentialManager.getPixelMapCredentialManager().colorMapFn = colorMapFn;
        CanvasDrawCache.getCanvasDrawCache().clearCache();
        return PixelMapsActions.updatePixelMapView({
          pixelMapUpdate: {
            channels,
          }
        });
      }),
    );
  });

  setInversionToRequesters$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ColorMapsActions.setColorMapInversionOfSelectedPixelMap),
      map(action => action.invert),
      concatLatestFrom(() => [
        this.store.select(PixelMapTileRequestersSelectors.selectPixelMapTileRequestersCount)
      ]),
      map(([invert, count]) => {
        const channels = [...Array(count).keys()];
        PixelMapCredentialManager.getPixelMapCredentialManager().inversion = invert;
        CanvasDrawCache.getCanvasDrawCache().clearCache();
        return PixelMapsActions.updatePixelMapView({
          pixelMapUpdate: {
            channels,
          }
        });
      })
    );
  });

  // recreate element color mapping and add color to element_class_mapping on inversion change
  recreateElementColorMapping$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(
        ColorMapsActions.setColorMapInversionOfSelectedPixelMap,
      ),
      concatLatestFrom(() => [
        this.store.select(ColorMapsSelectors.selectSelectedColorMap).pipe(filterNullish())
      ]),
      map(([, colorMap]) =>
        isSequentialColorMap(colorMap)
          ? ColorMapsActions.setSequentialOrDivergentColorMap({
            selected: colorMap
          })
          : ColorMapsActions.setNominalColorMap({
            selectedNominal: colorMap
          })
      )
    );
  });

  setColorMap$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ColorMapsActions.setColorMap),
      map(action => action.selectedColorMap),
      concatLatestFrom(() => [
        this.store.select(PixelMapsSelectors.selectSelectedPixelMap).pipe(filterNullish())
      ]),
      map(([colorMap, pixelMap]) =>
        pixelMap.type === 'nominal_pixelmap'
          ? ColorMapsActions.setNominalColorMap({
            selectedNominal: colorMap as CategoricalColorMap
          })
          : pixelMap.neutral_value === null || pixelMap.neutral_value === undefined
            ? ColorMapsActions.setSequentialColorMap({
              selectedSequential: colorMap as SequentialColorMap
            })
            : ColorMapsActions.setDivergentColorMap({
              selectedDivergent: colorMap as SequentialColorMap
            })
      )
    );
  });

  setSequentialColorMap$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ColorMapsActions.setSequentialColorMap),
      map(action => action.selectedSequential),
      map(selected => ColorMapsActions.setSequentialOrDivergentColorMap({ selected }))
    );
  });

  setDivergentColorMap$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ColorMapsActions.setDivergentColorMap),
      map(action => action.selectedDivergent),
      map(selected => ColorMapsActions.setSequentialOrDivergentColorMap({ selected }))
    );
  });

  setSequentialOrDivergentColorMap$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ColorMapsActions.setSequentialOrDivergentColorMap),
      map(action => action.selected),
      concatLatestFrom(() => [
        this.store.select(PixelMapsSelectors.selectSelectedPixelMap).pipe(filterNullish()),
        this.store.select(ColorMapsSelectors.selectInversionState),
      ]),
      map(([colorMap, pixelMap, invert]) => {
        return {
          pixelMap: {
            ...pixelMap,
            ...(isDiscretePixelMap(pixelMap)) && {
              element_class_mapping: this.converter.addColorToDiscreteElementClassMapping(
              // @ts-ignore
                pixelMap.min_value,
                pixelMap.max_value,
                pixelMap.element_class_mapping,
                colorMap?.colorMapFn,
                invert,
                pixelMap.neutral_value,
              )
            }
          } as ContinuousPixelMap | DiscretePixelMap,
          colorMap,
        };
      }),
      concatMap(({ pixelMap, colorMap }) => [
        ColorMapsActions.setColorMapOfSelectedPixelMap({
          colorMapFn: isDiscretePixelMap(pixelMap) && pixelMap.element_class_mapping
            ? convertElementClassMappingToElementColorMapping(pixelMap.element_class_mapping)
            : colorMap?.colorMapFn
        }),
        PixelMapsActions.addColorMapToElementClassMap({ pixelMap })
      ])
    );
  });

  setNominalColorMap$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ColorMapsActions.setNominalColorMap),
      map(action => action.selectedNominal),
      concatLatestFrom(() => [
        this.store.select(PixelMapsSelectors.selectSelectedPixelMap).pipe(filterNullish()),
        this.store.select(ColorMapsSelectors.selectInversionState),
        this.store.select(RenderingHintsSelectors.selectAllRenderingHints),
      ]),
      filter(([, pixelMap]) => isNominalPixelMap(pixelMap)),
      map(([nominalColorMap, pixelMap, invert, renderingHints]) => ({
        ...pixelMap,
        element_class_mapping: this.converter.addColorToNominalElementClassMapping(
          (pixelMap as NominalPixelMap).element_class_mapping,
          nominalColorMap?.colorMap,
          invert,
          // @ts-ignore
          pixelMap.neutral_value,
          renderingHints,
          nominalColorMap?.id === RECOMMENDED_PIXEL_MAP_COLOR_MAP_ID
        )
      } as NominalPixelMap)),
      concatMap(pixelMap => [
        ColorMapsActions.setColorMapOfSelectedPixelMap({
          colorMapFn: convertElementClassMappingToElementColorMapping(pixelMap.element_class_mapping)
        }),
        PixelMapsActions.addColorMapToElementClassMap({ pixelMap })
      ])
    );
  });

  // set inversion to false if color map selection changes
  resetColorMapInversion$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ColorMapsActions.setColorMap),
      map(() => ColorMapsActions.setColorMapInversionOfSelectedPixelMap({ invert: false }))
    );
  });

  // deactivate inversion if color map id matches recommended
  // color map id constance
  deactivateInversion$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ColorMapsActions.setColorMap),
      map(action => action.selectedColorMap),
      filterNullish(),
      map(colorMap => ColorMapsActions.setColorMapInversionState({
        inversionDisabled: colorMap.id === RECOMMENDED_PIXEL_MAP_COLOR_MAP_ID
      }))
    );
  });

  // set pixel map color maps settings after user storage
  // loaded successfully
  setSettings$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(UserStorageActions.loadUserStorageSuccess),
      map(action => action.userStorage.rendering.pixelmaps),
      concatLatestFrom(() => [
        this.store.select(ColorMapsSelectors.selectAllColorMaps)
      ]),
      map(([settings, colorMaps]) => ({
        selectedSequential: colorMaps.sequentialColorMaps.find(c => c.id === settings.selectedSequential),
        selectedDivergent: colorMaps.divergentColorMaps.find(c => c.id === settings.selectedDivergent),
        selectedNominal: colorMaps.nominalColorMaps.find(c => c.id === settings.selectedNominal),
        invert: settings.invert,
        inversionDisabled: settings.inversionDisabled
      })),
      map(settings => ColorMapsActions.setAllSelectedColorMaps({ ...settings }))
    );
  });

  constructor(
    private readonly actions$: Actions,
    private readonly store: Store,
    private readonly converter: PixelMapsV3ConversionService,
  ) {}
}
