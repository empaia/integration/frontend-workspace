import {
  ColorMapFn,
  Tableau9,
  Vibrant,
  interpolateBuRdAlpha,
  interpolateCividisAlpha,
  interpolatePlasmaAlpha,
  interpolatePuOrAlpha
} from 'pixelmap-rendering-collection';
import { interpolateCividis, interpolateMagma, interpolatePuOr, interpolateRdBu } from 'd3-scale-chromatic';

export interface SequentialColorMap {
  id: string;
  colorMapFn: ColorMapFn;
}

export interface CategoricalColorMap {
  id: string;
  colorMap: readonly string[];
}

export interface PixelMapColorMapsSettings {
  selectedSequential?: string;
  selectedDivergent?: string;
  selectedNominal?: string;
  invert: boolean;
  inversionDisabled: boolean;
}

export const DEFAULT_SEQUENTIAL_COLOR_MAPS: SequentialColorMap[] = [
  { id: 'Cividis', colorMapFn: interpolateCividis },
  { id: 'Magma', colorMapFn: interpolateMagma },
  { id: 'CividisAlpha', colorMapFn: interpolateCividisAlpha },
  { id: 'PlasmaAlpha', colorMapFn: interpolatePlasmaAlpha }
];

export const DEFAULT_DIVERGENT_COLOR_MAPS: SequentialColorMap[] = [
  { id: 'RdBu', colorMapFn: interpolateRdBu },
  { id: 'PuOr', colorMapFn: interpolatePuOr },
  { id: 'BuRdAlpha', colorMapFn: interpolateBuRdAlpha },
  { id: 'PuOrAlpha', colorMapFn: interpolatePuOrAlpha }
];

export const DEFAULT_NOMINAL_COLOR_MAPS: CategoricalColorMap[] = [
  { id: 'Vibrant', colorMap: Vibrant },
  { id: 'Tableau9', colorMap: Tableau9 }
];


export function isSequentialColorMap(colorMap: SequentialColorMap | CategoricalColorMap): colorMap is SequentialColorMap {
  return 'colorMapFn' in colorMap;
}

export function isCategoricalColorMap(colorMap: SequentialColorMap | CategoricalColorMap): colorMap is CategoricalColorMap {
  return 'colorMap' in colorMap;
}
