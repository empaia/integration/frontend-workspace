import { createSelector } from '@ngrx/store';
import { selectPixelMapsFeatureState } from '../pixel-maps-feature.state';
import { COLOR_MAPS_FEATURE_KEY } from './color-maps.reducer';
import { selectSelectedPixelMap } from '../pixel-maps/pixel-maps.selectors';
import { convertElementClassMappingToElementColorMapping, isDiscretePixelMap, isNominalPixelMap } from 'empaia-ui-commons';
import { PixelMapColorMapsSettings } from './color-maps.models';

export const selectColorMapsState = createSelector(
  selectPixelMapsFeatureState,
  (state) => state[COLOR_MAPS_FEATURE_KEY]
);

export const selectAllSequentialColorMaps = createSelector(
  selectColorMapsState,
  (state) => state.sequentialColorMaps
);

export const selectAllDivergeColorMaps = createSelector(
  selectColorMapsState,
  (state) => state.divergentColorMaps
);

export const selectAllNominalColorMaps = createSelector(
  selectColorMapsState,
  (state) => state.nominalColorMaps
);

export const selectAllColorMaps = createSelector(
  selectAllSequentialColorMaps,
  selectAllDivergeColorMaps,
  selectAllNominalColorMaps,
  (sequentialColorMaps, divergentColorMaps, nominalColorMaps) => ({
    sequentialColorMaps,
    divergentColorMaps,
    nominalColorMaps
  })
);

export const selectSelectedSequentialColorMap = createSelector(
  selectColorMapsState,
  (state) => state.selectedSequential
);

export const selectSelectedDivergentColorMap = createSelector(
  selectColorMapsState,
  (state) => state.selectedDivergent
);

export const selectSelectedNominalColorMap = createSelector(
  selectColorMapsState,
  (state) => state.selectedNominal
);

export const selectAllColorMapsForPixelMapType = createSelector(
  selectColorMapsState,
  selectSelectedPixelMap,
  (state, pixelMap) => {
    if (!pixelMap) {
      return undefined;
    }

    switch (pixelMap.type) {
      case 'continuous_pixelmap':
      case 'discrete_pixelmap':
        return pixelMap.neutral_value === null || pixelMap.neutral_value === undefined ? state.sequentialColorMaps : state.divergentColorMaps;
      case 'nominal_pixelmap':
        return state.nominalColorMaps;
    }
  }
);

export const selectSelectedColorMap = createSelector(
  selectColorMapsState,
  selectSelectedPixelMap,
  (state, pixelMap) => {
    if (!pixelMap) {
      return undefined;
    }

    switch (pixelMap.type) {
      case 'continuous_pixelmap':
      case 'discrete_pixelmap':
        return pixelMap.neutral_value === null || pixelMap.neutral_value === undefined ? state.selectedSequential : state.selectedDivergent;
      case 'nominal_pixelmap':
        return state.selectedNominal;
    }
  }
);

export const selectSelectedColorMapFn = createSelector(
  selectColorMapsState,
  selectSelectedPixelMap,
  (state, pixelMap) => {
    if (!pixelMap) {
      return undefined;
    }

    if ((isDiscretePixelMap(pixelMap) || isNominalPixelMap(pixelMap)) && pixelMap.element_class_mapping) {
      return convertElementClassMappingToElementColorMapping(pixelMap.element_class_mapping);
    } else if (pixelMap.neutral_value !== null || pixelMap.neutral_value !== undefined) {
      return state.selectedDivergent?.colorMapFn;
    } else {
      return state.selectedSequential?.colorMapFn;
    }
  }
);

export const selectInversionState = createSelector(
  selectColorMapsState,
  (state) => state.invert
);

export const selectInversionDeactivationState = createSelector(
  selectColorMapsState,
  (state) => state.inversionDisabled
);

export const selectPixelMapColorMapsSettings = createSelector(
  selectColorMapsState,
  (state) => ({
    selectedSequential: state.selectedSequential?.id,
    selectedDivergent: state.selectedDivergent?.id,
    selectedNominal: state.selectedNominal?.id,
    invert: state.invert,
    inversionDisabled: state.inversionDisabled
  } as PixelMapColorMapsSettings)
);
