import { createReducer, on } from '@ngrx/store';
import { CategoricalColorMap, DEFAULT_DIVERGENT_COLOR_MAPS, DEFAULT_NOMINAL_COLOR_MAPS, DEFAULT_SEQUENTIAL_COLOR_MAPS, SequentialColorMap } from './color-maps.models';
import { ColorMapsActions } from './color-maps.actions';


export const COLOR_MAPS_FEATURE_KEY = 'colorMaps';

export interface State {
  sequentialColorMaps: SequentialColorMap[];
  divergentColorMaps: SequentialColorMap[];
  nominalColorMaps: CategoricalColorMap[];
  selectedSequential?: SequentialColorMap | undefined;
  selectedDivergent?: SequentialColorMap | undefined;
  selectedNominal?: CategoricalColorMap | undefined;
  invert: boolean;
  inversionDisabled: boolean;
}

export const initialState: State = {
  sequentialColorMaps: DEFAULT_SEQUENTIAL_COLOR_MAPS,
  divergentColorMaps: DEFAULT_DIVERGENT_COLOR_MAPS,
  nominalColorMaps: DEFAULT_NOMINAL_COLOR_MAPS,
  selectedSequential: DEFAULT_SEQUENTIAL_COLOR_MAPS[0],
  selectedDivergent: DEFAULT_DIVERGENT_COLOR_MAPS[0],
  selectedNominal: DEFAULT_NOMINAL_COLOR_MAPS[0],
  invert: false,
  inversionDisabled: true,
};

export const reducer = createReducer(
  initialState,
  on(ColorMapsActions.setSequentialColorMap, (state, { selectedSequential }): State => ({
    ...state,
    selectedSequential,
  })),
  on(ColorMapsActions.setDivergentColorMap, (state, { selectedDivergent }): State => ({
    ...state,
    selectedDivergent,
  })),
  on(ColorMapsActions.setNominalColorMap, (state, { selectedNominal }): State => ({
    ...state,
    selectedNominal,
  })),
  on(ColorMapsActions.setAllSelectedColorMaps, (state, { selectedSequential, selectedDivergent, selectedNominal, invert, inversionDisabled }): State => ({
    ...state,
    selectedSequential,
    selectedDivergent,
    selectedNominal,
    invert,
    inversionDisabled
  })),
  on(ColorMapsActions.addNominalColorMap, (state, { nominalColorMap }): State => ({
    ...state,
    nominalColorMaps: [nominalColorMap].concat(state.nominalColorMaps),
    selectedNominal: nominalColorMap,
  })),
  on(ColorMapsActions.setColorMapInversionOfSelectedPixelMap, (state, { invert }): State => ({
    ...state,
    invert,
  })),
  on(ColorMapsActions.setColorMapInversionState, (state, { inversionDisabled }): State => ({
    ...state,
    inversionDisabled,
  })),
);
