import { createActionGroup, props, emptyProps } from '@ngrx/store';
import { PixelMap, PixelmapType } from './pixel-maps.models';
import { HttpErrorResponse } from '@angular/common/http';
import { PixelMapUpdate, ZoomToLevel } from 'slide-viewer';

export const PixelMapsActions = createActionGroup({
  source: 'PixelMaps Actions',
  events: {
    'Load Pixel Map': props<{
      scopeId: string,
      pixelMapId: string,
    }>(),
    'Load Pixel Map Success': props<{
      pixelMap: PixelMap,
    }>(),
    'Load Pixel Map Failure': props<{
      error: HttpErrorResponse,
    }>(),
    'Load Pixel Maps': props<{
      scopeId: string,
      referenceId: string,
      jobIds: string[],
      skip: number,
      limit: number,
      types?: PixelmapType[],
    }>(),
    'Load Pixel Maps Success': props<{
      pixelMaps: PixelMap[],
    }>(),
    'Load Pixel Maps Failure': props<{
      error: HttpErrorResponse,
    }>(),
    'Select Pixel Map': props<{
      selected?: string | null;
    }>(),
    'Select Pixel Map Ready': props<{
      selected?: PixelMap | null
    }>(),
    'Add Color Map To Element Class Map': props<{
      pixelMap: PixelMap,
    }>(),
    'Update Pixel Map View': props<{
      pixelMapUpdate: PixelMapUpdate
    }>(),
    'Clear Pixel Maps': emptyProps(),
    'Zoom To Pixel Map Level': props<{
      selected?: string | null
    }>(),
    'Zoom To Pixel Map Level Ready': props<{
      zoomToLevel: ZoomToLevel
    }>(),
  }
});
