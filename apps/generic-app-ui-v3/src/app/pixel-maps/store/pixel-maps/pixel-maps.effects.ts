/* eslint-disable @typescript-eslint/ban-ts-comment */
import { Injectable } from '@angular/core';
import { Actions, concatLatestFrom, createEffect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { AppV3DataService } from 'empaia-api-lib';
import { PixelMapsActions } from './pixel-maps.actions';
import * as TokenActions from '@token/store/token/token.actions';
import * as ResultsActions from '@analysis/store/results/results.actions';
import * as PixelMapsSelectors from './pixel-maps.selectors';
import * as JobsSelectors from '@jobs/store/jobs/jobs.selectors';
import * as ClassesSelectors from '@classes/store/classes/classes.selectors';
import { SlidesSelectors, SlidesActions } from '@slides/store';
import { ScopeSelectors } from '@scope/store';
import * as ColorMapsSelectors from '@pixel-maps/store/color-maps/color-maps.selectors';
import * as RenderingHintsSelectors from '@pixel-maps/store/rendering-hints/rendering-hints.selectors';
import { fetch } from '@ngrx/router-store/data-persistence';
import { State } from './pixel-maps.reducer';
import { concatMap, filter, map, retryWhen } from 'rxjs';
import { PixelMapsV3ConversionService } from '@pixel-maps/services/pixel-maps-conversion.service';
import { filterNullish, retryOnAction } from '@shared/helper/rxjs-operators';
import { requestNewToken } from 'vendor-app-communication-interface';
import { PIXEL_MAPS_QUERY_LIMIT, PixelMap } from './pixel-maps.models';
import {
  CategoricalColorMap,
  SequentialColorMap,
  isSequentialColorMap,
} from '../color-maps/color-maps.models';
import {
  CanvasDrawCache,
  PixelMapCredentialManager,
  PixelMapTileCache,
} from 'pixelmap-rendering-collection';
import { PixelMapTileRequestersActions } from '../pixel-map-tile-requesters/pixel-map-tile-requesters.actions';
import {
  RECOMMENDED_PIXEL_MAP_COLOR_MAP_ID,
  convertElementClassMappingToElementColorMapping,
  isDiscretePixelMap,
  isNominalPixelMap,
} from 'empaia-ui-commons';

@Injectable()
export class PixelMapsEffects {
  // clear pixel maps on slide selection
  clearPixelMaps$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(SlidesActions.selectSlide, SlidesActions.clearSlides),
      map(() => {
        PixelMapCredentialManager.getPixelMapCredentialManager().clear();
        PixelMapTileCache.getPixelMapTileCache().clearCache();
        CanvasDrawCache.getCanvasDrawCache().clearCache();
      }),
      map(() => PixelMapsActions.clearPixelMaps())
    );
  });

  loadPixelMap$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(PixelMapsActions.loadPixelMap),
      fetch({
        run: (
          action: ReturnType<typeof PixelMapsActions.loadPixelMap>,
          _state: State
        ) => {
          return this.dataService
            .scopeIdPixelmapsPixelmapIdGet({
              scope_id: action.scopeId,
              pixelmap_id: action.pixelMapId,
            })
            .pipe(
              // retry to load the selected slide after the token is expired
              retryWhen((errors) =>
                retryOnAction(
                  errors,
                  this.actions$,
                  TokenActions.setAccessToken,
                  requestNewToken
                )
              ),
              concatLatestFrom(() => [
                this.store.select(ClassesSelectors.selectClassEntities),
                this.store.select(RenderingHintsSelectors.selectAllRenderingHints),
              ]),
              map(
                ([item, classesDict,renderingHints]) =>
                  this.converter.convertApiPixelMapToAppPixelMap(
                    item,
                    classesDict,
                    renderingHints
                  ) as PixelMap
              ),
              map((pixelMap) =>
                PixelMapsActions.loadPixelMapSuccess({ pixelMap })
              )
            );
        },
        onError: (
          _action: ReturnType<typeof PixelMapsActions.loadPixelMap>,
          error
        ) => {
          return PixelMapsActions.loadPixelMapFailure({ error });
        },
      })
    );
  });

  loadPixelMaps$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(PixelMapsActions.loadPixelMaps),
      fetch({
        run: (
          action: ReturnType<typeof PixelMapsActions.loadPixelMaps>,
          _state: State
        ) => {
          return this.dataService
            .scopeIdPixelmapsQueryPut({
              scope_id: action.scopeId,
              skip: action.skip,
              limit: action.limit,
              body: {
                references: [action.referenceId],
                jobs: action.jobIds,
                types: action.types,
              },
            })
            .pipe(
              // retry to load the selected slide after the token is expired
              retryWhen((errors) =>
                retryOnAction(
                  errors,
                  this.actions$,
                  TokenActions.setAccessToken,
                  requestNewToken
                )
              ),
              concatLatestFrom(() => [
                this.store.select(ClassesSelectors.selectClassEntities),
                this.store.select(RenderingHintsSelectors.selectAllRenderingHints),
              ]),
              map(
                ([response, classesDict, renderingHints]) =>
                  response.items.map((item) =>
                    this.converter.convertApiPixelMapToAppPixelMap(
                      item,
                      classesDict,
                      renderingHints
                    )
                  ) as PixelMap[]
              ),
              map((pixelMaps) =>
                PixelMapsActions.loadPixelMapsSuccess({ pixelMaps })
              )
            );
        },
        onError: (
          _action: ReturnType<typeof PixelMapsActions.loadPixelMaps>,
          error
        ) => {
          return PixelMapsActions.loadPixelMapsFailure({ error });
        },
      })
    );
  });

  loadPixelMapsOnReferenceSelect$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ResultsActions.setPrimitivesReference),
      map((action) => action.reference),
      filterNullish(),
      map((reference) => reference.id),
      concatLatestFrom(() => [
        this.store.select(ScopeSelectors.selectScopeId).pipe(filterNullish()),
        this.store.select(JobsSelectors.selectAllCheckedJobIds),
      ]),
      filter(([_referenceId, _scopeId, jobIds]) => !!jobIds?.length),
      map(([referenceId, scopeId, jobIds]) =>
        PixelMapsActions.loadPixelMaps({
          scopeId,
          referenceId,
          jobIds,
          skip: 0,
          limit: PIXEL_MAPS_QUERY_LIMIT,
        })
      )
    );
  });

  // clear tile requesters on slide map selection
  // also clear tile and canvas cache
  clearTileRequesters$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(PixelMapsActions.selectPixelMap),
      map(() => {
        PixelMapTileCache.getPixelMapTileCache().clearCache();
        CanvasDrawCache.getCanvasDrawCache().clearCache();
      }),
      map(() => PixelMapTileRequestersActions.clearPixelMapTileRequesters())
    );
  });

  // save the pixel map object at selection in state
  selectPixelMapInstance$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(PixelMapsActions.selectPixelMap),
      map((action) => action.selected),
      concatLatestFrom(() => [
        this.store.select(PixelMapsSelectors.selectPixelMapEntities),
      ]),
      map(([selected, entities]) =>
        selected ? entities[selected] : undefined
      ),
      map((selected) => PixelMapsActions.selectPixelMapReady({ selected }))
    );
  });

  setTileRequesters$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(PixelMapsActions.selectPixelMapReady),
      map((action) => action.selected),
      filterNullish(),
      concatLatestFrom(() => [
        this.store
          .select(SlidesSelectors.selectSelectedSlide)
          .pipe(filterNullish()),
        this.store.select(ColorMapsSelectors.selectSelectedColorMap),
        this.store.select(ColorMapsSelectors.selectInversionState),
        this.store.select(RenderingHintsSelectors.selectAllRenderingHints),
      ]),
      map(([pixelMap, slide, colorMap, invert, renderingHints]) => ({
        slide,
        pixelMap: {
          ...pixelMap,
          ...(isDiscretePixelMap(pixelMap) && {
            element_class_mapping:
              this.converter.addColorToDiscreteElementClassMapping(
                // @ts-ignore
                pixelMap.min_value,
                pixelMap.max_value,
                pixelMap.element_class_mapping,
                (colorMap as SequentialColorMap)?.colorMapFn,
                invert,
                pixelMap.neutral_value
              ),
          }),
          ...(isNominalPixelMap(pixelMap) && {
            element_class_mapping:
              this.converter.addColorToNominalElementClassMapping(
                pixelMap.element_class_mapping,
                (colorMap as CategoricalColorMap)?.colorMap,
                invert,
                // @ts-ignore
                pixelMap.neutral_value,
                renderingHints,
                colorMap?.id === RECOMMENDED_PIXEL_MAP_COLOR_MAP_ID
              ),
          }),
        },
        colorMap,
        invert,
      })),
      map(({ slide, pixelMap, colorMap, invert }) => ({
        pixelMap,
        tileRequesters: this.converter.createPixelMapTileRequesters(
          pixelMap,
          slide,
          colorMap
            ? isSequentialColorMap(colorMap)
              ? colorMap.colorMapFn
              : convertElementClassMappingToElementColorMapping(
                pixelMap.element_class_mapping
              )
            : undefined,
          invert
        ),
      })),
      concatMap(({ pixelMap, tileRequesters }) => [
        PixelMapsActions.addColorMapToElementClassMap({
          pixelMap: pixelMap as PixelMap,
        }),
        PixelMapTileRequestersActions.setPixelMapTileRequesters({
          tileRequesters,
          tileSize: pixelMap.tilesize,
          channelClassMapping: pixelMap.channel_class_mapping,
        }),
      ])
    );
  });

  zoomToPixelMapLevel$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(PixelMapsActions.zoomToPixelMapLevel),
      map(action => action.selected),
      filterNullish(),
      concatLatestFrom(selected => [
        this.store.select(PixelMapsSelectors.selectPixelMapEntities).pipe(
          map(entities => entities[selected]),
          filterNullish()
        ),
        this.store.select(PixelMapsSelectors.selectSelectedPixelMap),
      ]),
      map(([, pixelMap, selected]) => ({
        pixelMap,
        selected: pixelMap === selected
      })),
      concatMap(zoomSelect => [
        !zoomSelect.selected
          ? PixelMapsActions.selectPixelMapReady({ selected: zoomSelect.pixelMap })
          : { type: 'noop' },
        PixelMapsActions.zoomToPixelMapLevelReady({
          zoomToLevel: { level : this.converter.calculateMaxLevel(zoomSelect.pixelMap.levels) }
        })
      ])
    );
  });

  constructor(
    private readonly actions$: Actions,
    private readonly store: Store,
    private readonly dataService: AppV3DataService,
    private readonly converter: PixelMapsV3ConversionService
  ) {}
}
