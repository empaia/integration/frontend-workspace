import { createSelector } from '@ngrx/store';
import { selectPixelMapsFeatureState } from '../pixel-maps-feature.state';
import { pixelMapsAdapter, PIXEL_MAPS_FEATURE_KEY } from './pixel-maps.reducer';

const {
  selectAll,
  selectEntities,
  selectIds,
} = pixelMapsAdapter.getSelectors();

export const selectPixelMapsState = createSelector(
  selectPixelMapsFeatureState,
  (state) => state[PIXEL_MAPS_FEATURE_KEY]
);

export const selectAllPixelMaps = createSelector(
  selectPixelMapsState,
  selectAll,
);

export const selectPixelMapEntities = createSelector(
  selectPixelMapsState,
  selectEntities,
);

export const selectAllPixelMapIds = createSelector(
  selectPixelMapsState,
  selectIds,
);

export const selectSelectedPixelMap = createSelector(
  selectPixelMapsState,
  (state) => state.selected
);

export const selectSelectedPixelMapId = createSelector(
  selectSelectedPixelMap,
  (pixelMap) => pixelMap?.id
);

export const selectPixelMapViewUpdate = createSelector(
  selectPixelMapsState,
  (state) => state.pixelMapUpdate
);

export const selectZoomToPixelMapLevel = createSelector(
  selectPixelMapsState,
  (state) => state.zoomToLevel
);

export const selectPixelMapsLoaded = createSelector(
  selectPixelMapsState,
  (state) => state.loaded
);

export const selectPixelMapsError = createSelector(
  selectPixelMapsState,
  (state) => state.error
);
