import { EntityState, createEntityAdapter } from '@ngrx/entity';
import { createReducer, on } from '@ngrx/store';
import { HttpErrorResponse } from '@angular/common/http';
import { PixelMapsActions } from './pixel-maps.actions';
import { PixelMapUpdate, ZoomToLevel } from 'slide-viewer';
import { PixelMap } from './pixel-maps.models';


export const PIXEL_MAPS_FEATURE_KEY = 'pixelMaps';

export interface State extends EntityState<PixelMap> {
  loaded: boolean;
  selected?: PixelMap | null | undefined;
  pixelMapUpdate?: PixelMapUpdate | undefined;
  zoomToLevel?: ZoomToLevel;
  error?: HttpErrorResponse | undefined;
}

export const pixelMapsAdapter = createEntityAdapter<PixelMap>();

export const initialState: State = pixelMapsAdapter.getInitialState({
  loaded: true,
  selected: undefined,
  zoomToLevel: undefined,
  error: undefined,
});

export const reducer = createReducer(
  initialState,
  on(PixelMapsActions.loadPixelMap, (state): State => ({
    ...state,
    loaded: false,
  })),
  on(PixelMapsActions.loadPixelMapSuccess, (state, { pixelMap }): State =>
    pixelMapsAdapter.setOne(pixelMap, {
      ...state,
      loaded: true,
    })
  ),
  on(PixelMapsActions.loadPixelMapFailure, (state, { error }): State => ({
    ...state,
    error,
    loaded: true,
  })),
  on(PixelMapsActions.loadPixelMaps, (state): State => ({
    ...state,
    loaded: false,
  })),
  on(PixelMapsActions.loadPixelMapsSuccess, (state, { pixelMaps }): State =>
    pixelMapsAdapter.setAll(pixelMaps, {
      ...state,
      loaded: true,
    })
  ),
  on(PixelMapsActions.loadPixelMapsFailure, (state, { error }): State => ({
    ...state,
    error,
    loaded: true,
  })),
  on(PixelMapsActions.selectPixelMapReady, (state, { selected }): State => ({
    ...state,
    selected,
  })),
  on(PixelMapsActions.addColorMapToElementClassMap, (state, { pixelMap }): State =>
    pixelMapsAdapter.upsertOne(pixelMap, {
      ...state,
      selected: pixelMap,
    })
  ),
  on(PixelMapsActions.updatePixelMapView, (state, { pixelMapUpdate }): State => ({
    ...state,
    pixelMapUpdate,
  })),
  on(PixelMapsActions.zoomToPixelMapLevelReady, (state, { zoomToLevel }): State => ({
    ...state,
    zoomToLevel,
  })),
  on(PixelMapsActions.clearPixelMaps, (state): State =>
    pixelMapsAdapter.removeAll({
      ...state,
      loaded: true,
      selected: undefined,
      error: undefined,
    })
  )
);
