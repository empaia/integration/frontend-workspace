import {
  AppV3ContinuousPixelmap,
  AppV3DiscretePixelmap,
  AppV3NominalPixelmap,
  AppV3NumberClassMapping,
} from 'empaia-api-lib';
import { PixelMapTileRequester } from 'empaia-ui-commons';
import { ChannelClassMapping } from 'slide-viewer';

export {
  AppV3ContinuousPixelmapElementType as ContinuousPixelmapElementType,
  AppV3DiscretePixelmapElementType as DiscretePixelmapElementType,
  AppV3NominalPixelmapElementType as NominalPixelmapElementType,
  AppV3PostContinuousPixelmap as PostContinuousPixelmap,
  AppV3PostContinuousPixelmaps as PostContinuousPixelmaps,
  AppV3PostDiscretePixelmap as PostDiscretePixelmap,
  AppV3PostDiscretePixelmaps as PostDiscretePixelmaps,
  AppV3PostNominalPixelmap as PostNominalPixelmap,
  AppV3PostNominalPixelmaps as PostNominalPixelmaps,
  AppV3PixelmapLevel as PixelmapLevel,
  AppV3PixelmapList as PixelmapList,
  AppV3PixelmapQuery as PixelmapQuery,
  AppV3PixelmapReferenceType as PixelmapReferenceType,
  AppV3PixelmapType as PixelmapType,
} from 'empaia-api-lib';

export const pixelMapType = [
  'continuous_pixelmap',
  'discrete_pixelmap',
  'nominal_pixelmap',
] as const;
export type PixelMapType = typeof pixelMapType[number];

export interface ElementClassMapping extends AppV3NumberClassMapping {
  color?: string;
}

export interface ContinuousPixelMap extends Omit<AppV3ContinuousPixelmap, 'max_value' | 'min_value' | 'neutral_value' | 'levels'> {
  channel_class_mapping?: ChannelClassMapping[];
  max_value: number | bigint;
  min_value: number | bigint;
  neutral_value?: number | bigint | null;
  levels: LevelDetailsMap;
  type: PixelMapType;
}

export interface DiscretePixelMap extends Omit<AppV3DiscretePixelmap, 'max_value' | 'min_value' | 'neutral_value' | 'levels'> {
  channel_class_mapping?: ChannelClassMapping[];
  max_value: number | bigint;
  min_value: number | bigint;
  neutral_value?: number | bigint | null;
  element_class_mapping?: ElementClassMapping[];
  levels: LevelDetailsMap;
  type: PixelMapType;
}

export interface NominalPixelMap extends Omit<AppV3NominalPixelmap, 'neutral_value' | 'levels'> {
  channel_class_mapping?: ChannelClassMapping[];
  neutral_value?: number | bigint | null;
  element_class_mapping: ElementClassMapping[];
  levels: LevelDetailsMap;
  type: PixelMapType;
}

export interface LevelDetails {
  position_min_x?: number | null;
  position_max_x?: number | null;
  position_min_y?: number | null;
  position_max_y?: number | null;
}

export interface LevelDetailsMap {
  [key: number]: LevelDetails;
}

export interface TileRequester {
  id: number;
  tileRequester: PixelMapTileRequester;
}

export type ColorMapFn = (t: number) => string;

export type PixelMap = ContinuousPixelMap | DiscretePixelMap | NominalPixelMap;

export const PIXEL_MAPS_QUERY_LIMIT = 10000;
