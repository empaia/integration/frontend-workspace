import { HttpErrorResponse } from '@angular/common/http';
import { createActionGroup, emptyProps, props } from '@ngrx/store';
import { PixelMapTileRequester } from 'empaia-ui-commons';
import { ChannelClassMapping } from 'slide-viewer';

export const PixelMapTileRequestersActions = createActionGroup({
  source: 'Pixel Map Tile Requesters',
  events: {
    'Set Pixel Map Tile Requester': props<{
      tileRequester: PixelMapTileRequester,
      tileSize: number,
      channelClassMapping?: ChannelClassMapping[];
    }>(),
    'Set Pixel Map Tile Requesters': props<{
      tileRequesters: PixelMapTileRequester[],
      tileSize: number,
      channelClassMapping?: ChannelClassMapping[];
    }>(),
    'Clear Pixel Map Tile Requesters': emptyProps(),
    'Load Pixel Map Tile Failure': props<{ error: HttpErrorResponse }>(),
  }
});
