import { HttpErrorResponse } from '@angular/common/http';
import { EntityState, createEntityAdapter } from '@ngrx/entity';
import { createReducer, on } from '@ngrx/store';
import { PixelMapTileRequestersActions } from './pixel-map-tile-requesters.actions';
import { ChannelClassMapping } from 'slide-viewer';
import { PixelMapTileRequester } from 'empaia-ui-commons';


export const PIXEL_MAP_TILE_REQUESTERS_FEATURE_KEY = 'pixelMapTileRequesters';

export interface State extends EntityState<PixelMapTileRequester> {
  tileSize: number;
  channelClassMapping?: ChannelClassMapping[] | undefined;
  error?: HttpErrorResponse | undefined;
}

export const pixelMapTileRequesterAdapter = createEntityAdapter<PixelMapTileRequester>({
  selectId: tileRequester => tileRequester.channel
});

export const initialState: State = pixelMapTileRequesterAdapter.getInitialState({
  tileSize: 0,
  channelClassMapping: undefined,
  error: undefined,
});

export const reducer = createReducer(
  initialState,
  on(PixelMapTileRequestersActions.setPixelMapTileRequester, (state, { tileRequester, tileSize, channelClassMapping }): State =>
    pixelMapTileRequesterAdapter.addOne(tileRequester, {
      ...state,
      tileSize,
      channelClassMapping,
    })
  ),
  on(PixelMapTileRequestersActions.setPixelMapTileRequesters, (state, { tileRequesters, tileSize, channelClassMapping }): State =>
    pixelMapTileRequesterAdapter.setAll(tileRequesters, {
      ...state,
      tileSize,
      channelClassMapping,
    })
  ),
  on(PixelMapTileRequestersActions.clearPixelMapTileRequesters, (state): State =>
    pixelMapTileRequesterAdapter.removeAll({
      ...state,
      ...initialState,
    })
  ),
  on(PixelMapTileRequestersActions.loadPixelMapTileFailure, (state, { error }): State => ({
    ...state,
    error,
  }))
);
