import { createSelector } from '@ngrx/store';
import { selectPixelMapsFeatureState } from '../pixel-maps-feature.state';
import { PIXEL_MAP_TILE_REQUESTERS_FEATURE_KEY, pixelMapTileRequesterAdapter } from './pixel-map-tile-requesters.reducer';
import { PixelMapInput } from 'slide-viewer';

const {
  selectAll,
  selectEntities,
} = pixelMapTileRequesterAdapter.getSelectors();

export const selectPixelMapTileRequestersState = createSelector(
  selectPixelMapsFeatureState,
  (state) => state[PIXEL_MAP_TILE_REQUESTERS_FEATURE_KEY]
);

export const selectAllPixelMapTileRequesters = createSelector(
  selectPixelMapTileRequestersState,
  selectAll,
);

export const selectPixelMapTileRequesterEntities = createSelector(
  selectPixelMapTileRequestersState,
  selectEntities,
);

export const selectPixelMapTileSize = createSelector(
  selectPixelMapTileRequestersState,
  (state) => state.tileSize
);

export const selectPixelMapChannelClassMapping = createSelector(
  selectPixelMapTileRequestersState,
  (state) => state.channelClassMapping
);

export const selectPixelMapInput = createSelector(
  selectPixelMapTileSize,
  selectAllPixelMapTileRequesters,
  selectPixelMapChannelClassMapping,
  (tileSize, tileRequesters, channelClassMapping) => tileSize || tileRequesters.length ? {
    tileSize,
    tileRequesters,
    channelClassMapping,
  } as PixelMapInput : undefined
);

export const selectPixelMapTileRequestersCount = createSelector(
  selectAllPixelMapTileRequesters,
  (tileRequesters) => tileRequesters.length
);
