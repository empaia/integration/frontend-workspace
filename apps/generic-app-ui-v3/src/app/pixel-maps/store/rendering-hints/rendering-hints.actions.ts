import { createActionGroup, props } from '@ngrx/store';
import { RenderingHint } from 'empaia-ui-commons';

export const RenderingHintsActions = createActionGroup({
  source: 'Pixel Maps Rendering Hints',
  events: {
    'Set Rendering Hints': props<{
      renderingHints?: RenderingHint[]
    }>(),
  }
});
