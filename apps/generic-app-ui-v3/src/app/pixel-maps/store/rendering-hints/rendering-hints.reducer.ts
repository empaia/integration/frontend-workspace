import { createReducer, on } from '@ngrx/store';
import { RenderingHintsActions } from './rendering-hints.actions';
import { EntityState, createEntityAdapter } from '@ngrx/entity';
import { RenderingHint } from 'empaia-ui-commons';

export const RENDERING_HINTS_FEATURE_KEY = 'renderingHints';

export type State = EntityState<RenderingHint>;

export const renderingHintsAdapter = createEntityAdapter<RenderingHint>({
  selectId: hint => hint.class_value
});

export const initialState: State = renderingHintsAdapter.getInitialState();

export const reducer = createReducer(
  initialState,
  on(RenderingHintsActions.setRenderingHints, (state, { renderingHints }): State =>
    renderingHints
      ? renderingHintsAdapter.setAll(renderingHints, {
        ...state
      })
      : renderingHintsAdapter.removeAll({
        ...state
      })
  )
);
