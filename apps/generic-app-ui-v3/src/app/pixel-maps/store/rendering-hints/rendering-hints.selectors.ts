import { createSelector } from '@ngrx/store';
import { RENDERING_HINTS_FEATURE_KEY, renderingHintsAdapter } from './rendering-hints.reducer';
import { selectPixelMapsFeatureState } from '../pixel-maps-feature.state';

const {
  selectAll,
  selectEntities,
  selectIds,
} = renderingHintsAdapter.getSelectors();

export const selectRenderingHintsState = createSelector(
  selectPixelMapsFeatureState,
  (state) => state[RENDERING_HINTS_FEATURE_KEY]
);

export const selectAllRenderingHints = createSelector(
  selectRenderingHintsState,
  selectAll,
);

export const selectRenderingHintEntities = createSelector(
  selectRenderingHintsState,
  selectEntities,
);

export const selectAllRenderingHintIds = createSelector(
  selectRenderingHintsState,
  (state) => selectIds(state) as string[]
);

export const selectHasPixelMapRenderingHint = createSelector(
  selectAllRenderingHints,
  (hints) => !!hints?.length
);
