import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import * as ScopeActions from '@scope/store/scope/scope.actions';
import { EmpaiaAppDescriptionV3, PixelMapsConversionService } from 'empaia-ui-commons';
import { map } from 'rxjs/operators';
import { RenderingHintsActions } from './rendering-hints.actions';
import { filterNullish } from '@shared/helper/rxjs-operators';
import { ColorMapsActions } from '../color-maps/color-maps.actions';

@Injectable()
export class RenderingHintsEffects {

  setRenderingHints$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ScopeActions.loadExtendedScopeSuccess),
      map(action => action.extendedScope.ead as EmpaiaAppDescriptionV3),
      map(ead => ead.rendering?.nominal_pixelmaps),
      map(renderingHints => RenderingHintsActions.setRenderingHints({ renderingHints }))
    );
  });

  // create a categorical color map from rendering hint
  addColorMap$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(RenderingHintsActions.setRenderingHints),
      map(action => action.renderingHints),
      filterNullish(),
      map(renderingHints => this.pixelMapConverter.convertRenderingHintsToCategoricalColorMap(renderingHints)),
      map(nominalColorMap => ColorMapsActions.addNominalColorMap({ nominalColorMap }))
    );
  });

  constructor(
    private readonly actions$: Actions,
    private readonly pixelMapConverter: PixelMapsConversionService,
  ) {}
}
