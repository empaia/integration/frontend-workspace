import { Action, combineReducers, createFeatureSelector } from '@ngrx/store';
import * as fromPixelMaps from './pixel-maps/pixel-maps.reducer';
import * as fromColorMaps from './color-maps/color-maps.reducer';
import * as fromTileRequesters from './pixel-map-tile-requesters/pixel-map-tile-requesters.reducer';
import * as fromRenderingHints from './rendering-hints/rendering-hints.reducer';

export const PIXEL_MAPS_MODULE_FEATURE_KEY = 'pixelMapsModuleFeature';

export const selectPixelMapsFeatureState = createFeatureSelector<State>(
  PIXEL_MAPS_MODULE_FEATURE_KEY
);

export interface State {
  [fromPixelMaps.PIXEL_MAPS_FEATURE_KEY]: fromPixelMaps.State;
  [fromColorMaps.COLOR_MAPS_FEATURE_KEY]: fromColorMaps.State;
  [fromTileRequesters.PIXEL_MAP_TILE_REQUESTERS_FEATURE_KEY]: fromTileRequesters.State;
  [fromRenderingHints.RENDERING_HINTS_FEATURE_KEY]: fromRenderingHints.State;
}

export function reducers(state: State | undefined, action: Action) {
  return combineReducers({
    [fromPixelMaps.PIXEL_MAPS_FEATURE_KEY]: fromPixelMaps.reducer,
    [fromColorMaps.COLOR_MAPS_FEATURE_KEY]: fromColorMaps.reducer,
    [fromTileRequesters.PIXEL_MAP_TILE_REQUESTERS_FEATURE_KEY]: fromTileRequesters.reducer,
    [fromRenderingHints.RENDERING_HINTS_FEATURE_KEY]: fromRenderingHints.reducer,
  })(state, action);
}
