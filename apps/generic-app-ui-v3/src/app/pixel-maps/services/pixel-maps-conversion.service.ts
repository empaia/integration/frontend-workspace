/* eslint-disable @typescript-eslint/no-explicit-any */
import { Injectable } from '@angular/core';
import { ElementColorMapping, PixelMapCredentialManager } from 'pixelmap-rendering-collection';
import { SlideEntity } from '@slides/store';
import { ImageInfo } from 'slide-viewer';
import { ScopeService } from '@scope/services/scope.service';
import { retryWhen } from 'rxjs';
import { retryOnAction } from '@shared/helper/rxjs-operators';
import { requestNewToken } from 'vendor-app-communication-interface';
import { Actions } from '@ngrx/effects';
import * as TokenActions from '@token/store/token/token.actions';
import { ColorMapFn, PixelMap, PixelMapTileRequester, PixelMapsConversionService, PixelMapsTileService } from 'empaia-ui-commons';
import { WbsUrlService } from '@wbs-url/services/wbs-url.service';
import { AccessTokenService } from '@token/services/access-token.service';

@Injectable({
  providedIn: 'root'
})
export class PixelMapsV3ConversionService extends PixelMapsConversionService {
  constructor(
    private scopeService: ScopeService,
    private wbsUrlService: WbsUrlService,
    private accessTokenService: AccessTokenService,
    private actions$: Actions,
  ) {
    super();
  }

  public createPixelMapTileRequester(
    pixelMap: PixelMap,
    slide: SlideEntity,
    channel: number,
  ): PixelMapTileRequester {
    return new PixelMapTileRequester(
      slide.imageView?.imageInfo as ImageInfo,
      pixelMap,
      new PixelMapsTileService(
        this.scopeService,
        [
          {
            key: this.accessTokenService.headerKey, value: this.accessTokenService.getAccessToken.bind(this.accessTokenService)
          }
        ],
        this.wbsUrlService.wbsUrlComplete,
        // retry to load the selected slide after the token is expired
        retryWhen(errors =>
          retryOnAction(errors, this.actions$, TokenActions.setAccessToken, requestNewToken)
        ),
      ),
      channel,
      slide.imageView?.resolver.hasArtificialLevel,
    );
  }

  public createPixelMapTileRequesters(
    pixelMap: PixelMap,
    slide: SlideEntity,
    colorMapFn?: ColorMapFn | ElementColorMapping,
    invert?: boolean,
  ): PixelMapTileRequester[] {
    const tileRequesters: PixelMapTileRequester[] = [];
    const pixelMapCredentialManager = PixelMapCredentialManager.getPixelMapCredentialManager();

    pixelMapCredentialManager.setAllCredentials(colorMapFn, invert);

    for (let i = 0; i < pixelMap.channel_count; i++) {
      const tileRequester = this.createPixelMapTileRequester(pixelMap, slide, i);
      tileRequesters.push(tileRequester);
    }

    return tileRequesters;
  }
}
