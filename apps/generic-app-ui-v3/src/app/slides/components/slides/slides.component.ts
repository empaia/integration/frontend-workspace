import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { SelectionError } from '@menu/models/ui.models';
import { SlideEntity, SlideImage } from '@slides/store';

@Component({
  selector: 'app-slides',
  templateUrl: './slides.component.html',
  styleUrls: ['./slides.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SlidesComponent {
  @Input() slideEntities!: SlideEntity[];
  @Input() slideImages!: SlideImage[];
  @Input() selectedSlideId!: string;
  @Input() slidesSkip!: number;
  @Input() slidesLimit!: number;
  @Input() maxSlidesCount!: number;

  @Output() slideSelected = new EventEmitter<string>();
  @Output() loadNewSlidesPage = new EventEmitter<number>();

  @Input() selectionMissing!: SelectionError;

  public getSlideImage(slideId: string): SlideImage | undefined {
    return this.slideImages.find(img => img.id === slideId);
  }
}
