import { ChangeDetectionStrategy, Component } from '@angular/core';
import { MenuEntity, MenuState } from '@menu/models/menu.models';
import { SelectionError } from '@menu/models/ui.models';
import { MenuSelectors } from '@menu/store';
import { Store } from '@ngrx/store';
import { SLIDES_LIMIT, SlideEntity, SlideImage, SlidesActions, SlidesImagesSelectors, SlidesSelectors } from '@slides/store';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-slides-container',
  templateUrl: './slides-container.component.html',
  styleUrls: ['./slides-container.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SlidesContainerComponent {
  public menuState = MenuState;
  public slidesMenu$: Observable<MenuEntity>;
  public slide$: Observable<SlideEntity | undefined>;
  public slides$: Observable<SlideEntity[]>;
  public slideImages$: Observable<SlideImage[]>;
  public slidesSkip$: Observable<number>;
  public maxSlidesCount$: Observable<number>;
  public loaded$: Observable<boolean>;
  public selectionError$: Observable<SelectionError | undefined>;

  public SLIDES_BATCH_LIMIT = SLIDES_LIMIT;

  constructor(private store: Store) {
    this.slidesMenu$ = this.store.select(MenuSelectors.selectSlidesMenu);
    this.slide$ = this.store.select(SlidesSelectors.selectSelectedSlide);
    this.slides$ = this.store.select(SlidesSelectors.selectSlideBatch);
    this.slideImages$ = this.store.select(SlidesImagesSelectors.selectAllSlidesImages);
    this.slidesSkip$ = this.store.select(SlidesSelectors.selectSlideSkip);
    this.maxSlidesCount$ = this.store.select(SlidesSelectors.selectMaxSlidesCount);
    this.loaded$ = this.store.select(SlidesSelectors.selectSlidesLoaded);
    this.selectionError$ = this.store.select(SlidesSelectors.selectSelectionError);
  }

  public selectSlide(id: string): void {
    this.store.dispatch(SlidesActions.selectSlide({ id }));
  }

  public loadNewSlidesPage(skip: number): void {
    this.store.dispatch(SlidesActions.setSlidePage({ skip }));
  }
}
