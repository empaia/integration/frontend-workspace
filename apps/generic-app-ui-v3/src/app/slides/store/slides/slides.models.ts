import {
  AppV3Slide as SlideView,
  AppV3SlideLevel,
  AppV3SlideInfo,
  AppV3SlideColor,
} from 'empaia-api-lib';
import { ErrorMap, SLIDES_LOAD_ERROR } from '@menu/models/ui.models';
import { Slide } from 'slide-viewer';

export type SlideLevel = AppV3SlideLevel;
export type SlideInfo = AppV3SlideInfo;
export type SlideColor = AppV3SlideColor;

export interface SlideEntity {
  id: string;
  disabled: boolean;
  dataView: SlideView;
  imageView?: Slide;
}

export const SLIDES_LIMIT = 20;

export const SLIDES_ERROR_MAP: ErrorMap = {
  '[App/Slides] Load Slides Failure': SLIDES_LOAD_ERROR,
};
