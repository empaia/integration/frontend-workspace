import { HttpErrorResponse } from '@angular/common/http';
import { createAction, props } from '@ngrx/store';
import { Slide } from 'slide-viewer';
import { SlideEntity } from './slides.models';

export const loadSlides = createAction(
  '[APP/Slides] Load Slides',
  props<{ scopeId: string }>()
);

export const loadSlidesSuccess = createAction(
  '[APP/Slides] Load Slides Success',
  props<{ slides: SlideEntity[] }>()
);

export const loadSlidesFailure = createAction(
  '[App/Slides] Load Slides Failure',
  props<{ error: HttpErrorResponse }>()
);

export const loadSlideImageInfoSuccess = createAction(
  '[APP/Slide] Load Slide Image Info Success',
  props<{ slideImage: Slide }>()
);

export const selectSlide = createAction(
  '[APP/Slides] Select Slide',
  props<{ id: string | undefined }>()
);

export const setSlidePage = createAction(
  '[APP/Slides] Set Slide Page',
  props<{ skip: number }>()
);

export const clearSlides = createAction(
  '[APP/Slides] Clear Slides'
);
