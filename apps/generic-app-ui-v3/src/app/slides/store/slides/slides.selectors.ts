import { createSelector } from '@ngrx/store';
import {
  State as ModuleState,
  selectSlideFeatureState,
} from '../slides-feature.state';
import { slidesAdapter } from './slides.reducer';
import { SLIDES_LIMIT } from './slides.models';

const {
  selectAll,
  selectEntities,
  selectIds,
} = slidesAdapter.getSelectors();

export const selectSlidesState = createSelector(
  selectSlideFeatureState,
  (state: ModuleState) => state.slides
);

export const selectAllSlides = createSelector(
  selectSlidesState,
  (state) => selectAll(state)
);

export const selectAllSlidesEntities = createSelector(
  selectSlidesState,
  (state) => selectEntities(state)
);

export const selectAllSlideIds = createSelector(
  selectSlidesState,
  (state) => selectIds(state) as string[]
);

export const selectSlideSkip = createSelector(
  selectSlidesState,
  (state) => state.skip
);

export const selectMaxSlidesCount = createSelector(
  selectAllSlides,
  (slides) => slides.length
);

export const selectSlideBatch = createSelector(
  selectAllSlides,
  selectSlideSkip,
  (slides, skip) => slides.slice(skip, SLIDES_LIMIT + skip)
);

export const selectSlideBatchIds = createSelector(
  selectAllSlideIds,
  selectSlideSkip,
  (slideIds, skip) => slideIds.slice(skip, SLIDES_LIMIT + skip)
);

export const selectSlidesLoaded = createSelector(
  selectSlidesState,
  (state) => state.loaded
);

export const selectSlidesError = createSelector(
  selectSlidesState,
  (state) => state.error
);

export const selectSelectedSlideId = createSelector(
  selectSlidesState,
  (state) => state.selected
);

export const selectSelectedSlide = createSelector(
  selectSelectedSlideId,
  selectAllSlidesEntities,
  (slideId, entities) => slideId ? entities[slideId] : undefined
);

export const selectSelectedSlideImageView = createSelector(
  selectSelectedSlide,
  (slide) => slide?.imageView
);

export const selectSelectionError = createSelector(
  selectSlidesState,
  // TODO: implement error
  (_state) => undefined
);
