import { MenuModule } from '@menu/menu.module';
import { MaterialModule } from '@material/material.module';
import { SlideViewerModule } from 'slide-viewer';
import { AppComponent } from './containers/app.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoadingIndicatorComponent } from './components/loading-indicator/loading-indicator.component';
import { PixelMapsModule } from '@pixel-maps/pixel-maps.module';



@NgModule({
  declarations: [
    AppComponent,
    LoadingIndicatorComponent
  ],
  imports: [
    CommonModule,
    MaterialModule,
    MenuModule,
    PixelMapsModule,
    SlideViewerModule,
  ]
})
export class CoreModule { }
