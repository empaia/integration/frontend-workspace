import { EffectsModule } from '@ngrx/effects';
import { WBS_URL_MODULE_FEATURE_KEY, reducers } from '@wbs-url/store/wbs-url-feature.state';
import { StoreModule } from '@ngrx/store';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WbsUrlEffects } from '@wbs-url//store';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    StoreModule.forFeature(
      WBS_URL_MODULE_FEATURE_KEY,
      reducers
    ),
    EffectsModule.forFeature([
      WbsUrlEffects
    ])
  ]
})
export class WbsUrlModule { }
