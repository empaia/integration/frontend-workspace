import { Inject, Injectable } from '@angular/core';
import { AppV3ApiConfiguration } from 'empaia-api-lib';

@Injectable({
  providedIn: 'root'
})
export class WbsUrlService {
  private _wbsUrl!: string;
  private _wbsUrlExtension: string;

  constructor(
    private apiConfig: AppV3ApiConfiguration,
    @Inject('API_VERSION') private apiVersion: string,
  ) {
    this._wbsUrlExtension = `/${this.apiVersion}/scopes`;
  }

  public get wbsUrl() {
    return this._wbsUrl;
  }

  public set wbsUrl(val: string) {
    this._wbsUrl = val;
    this.setRootUrl();
  }

  public get wbsUrlExtension() {
    return this._wbsUrlExtension;
  }

  public get wbsUrlComplete() {
    return this._wbsUrl + this._wbsUrlExtension;
  }

  private setRootUrl(): void {
    this.apiConfig.rootUrl = this.wbsUrlComplete;
  }
}
