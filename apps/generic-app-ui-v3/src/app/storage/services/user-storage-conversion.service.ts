import { Injectable } from '@angular/core';
import { UserStorage, UserStorageApi } from '../store/user-storage/user-storage.models';
import { AnnotationColorMapsSettings } from '@annotations/store/annotations-color-maps/annotations-color-maps.models';
import { PixelMapColorMapsSettings } from '@pixel-maps/store';

@Injectable({
  providedIn: 'root'
})
export class UserStorageConversionService {
  public convertUserStorageFromApi(userStorageApi: UserStorageApi): UserStorage {
    return {
      rendering: {
        annotations: {
          selected: userStorageApi.rendering_annotations_selected_color_map ?? undefined,
          invert: Boolean(userStorageApi.rendering_annotations_invert),
          inversionDisabled: Boolean(userStorageApi.rendering_annotations_inversion_disabled)
        },
        pixelmaps: {
          selectedSequential: userStorageApi.rendering_pixel_maps_selected_sequential ?? undefined,
          selectedDivergent: userStorageApi.rendering_pixel_maps_selected_divergent ?? undefined,
          selectedNominal: userStorageApi.rendering_pixel_maps_selected_nominal ?? undefined,
          invert: Boolean(userStorageApi.rendering_pixel_maps_invert),
          inversionDisabled: Boolean(userStorageApi.rendering_pixel_maps_inversion_disabled)
        }
      }
    };
  }

  public convertSettingsToApi(
    annotationSettings: AnnotationColorMapsSettings,
    pixelMapsSettings: PixelMapColorMapsSettings
  ): UserStorageApi {
    return {
      rendering_annotations_selected_color_map: annotationSettings.selected ?? '',
      rendering_annotations_invert: annotationSettings.invert,
      rendering_annotations_inversion_disabled: annotationSettings.inversionDisabled,
      rendering_pixel_maps_selected_sequential: pixelMapsSettings.selectedSequential ?? '',
      rendering_pixel_maps_selected_divergent: pixelMapsSettings.selectedDivergent ?? '',
      rendering_pixel_maps_selected_nominal: pixelMapsSettings.selectedNominal ?? '',
      rendering_pixel_maps_invert: pixelMapsSettings.invert,
      rendering_pixel_maps_inversion_disabled: pixelMapsSettings.inversionDisabled
    };
  }
}
