import { Action, combineReducers, createFeatureSelector } from '@ngrx/store';
import * as fromUserStorage from './user-storage/user-storage.reducer';

export const STORAGE_MODULE_FEATURE_KEY = 'storageModuleFeature';

export const selectStorageFeatureState = createFeatureSelector<State>(
  STORAGE_MODULE_FEATURE_KEY
);

export interface State {
  [fromUserStorage.USER_STORAGE_FEATURE_KEY]: fromUserStorage.State;
}

export function reducers(state: State | undefined, action: Action) {
  return combineReducers({
    [fromUserStorage.USER_STORAGE_FEATURE_KEY]: fromUserStorage.reducer,
  })(state, action);
}
