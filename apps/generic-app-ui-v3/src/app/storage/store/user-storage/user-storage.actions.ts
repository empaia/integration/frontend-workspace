import { createActionGroup, emptyProps, props } from '@ngrx/store';
import { UserStorage, UserStorageApi } from './user-storage.models';
import { HttpErrorResponse } from '@angular/common/http';

export const UserStorageActions = createActionGroup({
  source: 'UserStorage',
  events: {
    'Load User Storage': props<{ scopeId: string }>(),
    'Load User Storage Success': props<{ userStorage: UserStorage }>(),
    'Load User Storage Failure': props<{ error: HttpErrorResponse }>(),
    'Persist User Storage': props<{
      scopeId: string,
      userStorage: UserStorageApi,
    }>(),
    'Persist User Storage Success': props<{ userStorage: UserStorage }>(),
    'Persist User Storage Failure': props<{ error: HttpErrorResponse }>(),
    'Clear User Storage': emptyProps(),
  }
});
