import { Injectable } from '@angular/core';
import { Actions, concatLatestFrom, createEffect, ofType } from '@ngrx/effects';
import { AppV3StorageService } from 'empaia-api-lib';
import { UserStorageActions } from './user-storage.actions';
import { fetch, pessimisticUpdate } from '@ngrx/router-store/data-persistence';
import { HttpErrorResponse } from '@angular/common/http';
import { State } from './user-storage.reducer';
import { debounceTime, distinctUntilChanged, filter, map } from 'rxjs/operators';
import { UserStorageConversionService } from '../../services/user-storage-conversion.service';
import { USER_STORAGE_DEBOUNCE_TIME, UserStorageApi, hasUserStorageChanged, isUserStorageApi } from './user-storage.models';
import { Store } from '@ngrx/store';
import { filterNullish } from '@shared/helper/rxjs-operators';
import * as ClassesActions from '@classes/store/classes/classes.actions';
import * as ScopeSelectors from '@scope/store/scope/scope.selectors';
import { ColorMapsActions, ColorMapsSelectors } from '@pixel-maps/store';
import { AnnotationsColorMapsActions, AnnotationsColorMapsSelectors } from '@annotations/store';



@Injectable()
export class UserStorageEffects {

  loadUserStorage$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(UserStorageActions.loadUserStorage),
      fetch({
        run: (
          action: ReturnType<typeof UserStorageActions.loadUserStorage>,
          _state: State
        ) => {
          return this.storageService.scopeIdAppUiStorageUserGet({
            scope_id: action.scopeId
          }).pipe(
            map(response => response.content as UserStorageApi),
            filter(content => isUserStorageApi(content)),
            map(content => this.storageConverter.convertUserStorageFromApi(content)),
            map(userStorage => UserStorageActions.loadUserStorageSuccess({ userStorage }))
          );
        },
        onError: (_action: ReturnType<typeof UserStorageActions.loadUserStorage>, error: HttpErrorResponse) => {
          return UserStorageActions.loadUserStorageFailure({ error });
        }
      })
    );
  });

  persistUserStorage$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(UserStorageActions.persistUserStorage),
      pessimisticUpdate({
        run: (
          action: ReturnType<typeof UserStorageActions.persistUserStorage>,
          _state: State,
        ) => {
          return this.storageService.scopeIdAppUiStorageUserPut({
            scope_id: action.scopeId,
            body: {
              content: action.userStorage
            }
          }).pipe(
            map(response => response.content as UserStorageApi),
            map(content => this.storageConverter.convertUserStorageFromApi(content)),
            map(userStorage => UserStorageActions.persistUserStorageSuccess({ userStorage }))
          );
        },
        onError: (_action: ReturnType<typeof UserStorageActions.persistUserStorage>, error: HttpErrorResponse) => {
          return UserStorageActions.persistUserStorageFailure({ error });
        }
      })
    );
  });

  // load user storage after the class-namespaces are successfully loaded
  loadUserStorageInit$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ClassesActions.loadClassNamespacesSuccess),
      concatLatestFrom(() => [
        this.store.select(ScopeSelectors.selectScopeId).pipe(
          filterNullish()
        )
      ]),
      map(([, scopeId]) => scopeId),
      map(scopeId => UserStorageActions.loadUserStorage({ scopeId }))
    );
  });

  // persists user storage on annotations or pixelmaps
  // settings change
  persistOnSettingChanges$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(
        AnnotationsColorMapsActions.selectAnnotationColorMap,
        AnnotationsColorMapsActions.setColorMapInversion,
        AnnotationsColorMapsActions.setColorMapInversionState,
        ColorMapsActions.setSequentialColorMap,
        ColorMapsActions.setDivergentColorMap,
        ColorMapsActions.setNominalColorMap,
        ColorMapsActions.setColorMapInversionOfSelectedPixelMap,
        ColorMapsActions.setColorMapInversionState,
      ),
      // we wait a moment until the user is done changing settings
      // this way we reducing the number of requests
      debounceTime(USER_STORAGE_DEBOUNCE_TIME),
      concatLatestFrom(() => [
        this.store.select(ScopeSelectors.selectScopeId).pipe(filterNullish()),
        this.store.select(AnnotationsColorMapsSelectors.selectAnnotationColorMapsSettings),
        this.store.select(ColorMapsSelectors.selectPixelMapColorMapsSettings)
      ]),
      map(([, scopeId, annotationsSettings, pixelMapsSettings]) => ({
        scopeId,
        userStorage: this.storageConverter.convertSettingsToApi(annotationsSettings, pixelMapsSettings)
      })),
      // if the settings hasn't changed don't send a request for persistence
      distinctUntilChanged((prev, curr) => hasUserStorageChanged(prev.userStorage, curr.userStorage)),
      map(action => UserStorageActions.persistUserStorage({ ...action }))
    );
  });

  constructor(
    private readonly actions$: Actions,
    private readonly store: Store,
    private readonly storageService: AppV3StorageService,
    private readonly storageConverter: UserStorageConversionService,
  ) {}
}
