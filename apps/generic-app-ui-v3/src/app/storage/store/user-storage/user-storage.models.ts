export interface AnnotationColorMapSettings {
  selected?: string;
  invert: boolean;
  inversionDisabled: boolean;
}

export interface PixelMapColorMapSettings {
  selectedSequential?: string;
  selectedDivergent?: string;
  selectedNominal?: string;
  invert: boolean;
  inversionDisabled: boolean;
}

export interface RenderingSettings {
  annotations: AnnotationColorMapSettings,
  pixelmaps: PixelMapColorMapSettings;
}

export interface UserStorage {
  rendering: RenderingSettings;
}

export interface ContentApi {
  [key: string]: (string | number | boolean);
}

export interface UserStorageApi extends ContentApi {
  rendering_annotations_selected_color_map: string;
  rendering_annotations_invert: boolean;
  rendering_annotations_inversion_disabled: boolean;
  rendering_pixel_maps_selected_sequential: string;
  rendering_pixel_maps_selected_divergent: string;
  rendering_pixel_maps_selected_nominal: string;
  rendering_pixel_maps_invert: boolean;
  rendering_pixel_maps_inversion_disabled: boolean;
}

export function isUserStorageApi(obj: object): obj is UserStorageApi {
  return 'rendering_annotations_selected_color_map' in obj
  && 'rendering_annotations_invert' in obj
  && 'rendering_annotations_inversion_disabled' in obj
  && 'rendering_pixel_maps_selected_sequential' in obj
  && 'rendering_pixel_maps_selected_divergent' in obj
  && 'rendering_pixel_maps_selected_nominal' in obj
  && 'rendering_pixel_maps_invert' in obj
  && 'rendering_pixel_maps_inversion_disabled' in obj;
}

export function hasUserStorageChanged(a: UserStorageApi, b: UserStorageApi): boolean {
  return a.rendering_annotations_selected_color_map === b.rendering_annotations_selected_color_map
  && a.rendering_annotations_invert === b.rendering_annotations_invert
  && a.rendering_annotations_inversion_disabled === b.rendering_annotations_inversion_disabled
  && a.rendering_pixel_maps_selected_sequential === b.rendering_pixel_maps_selected_sequential
  && a.rendering_pixel_maps_selected_divergent === b.rendering_pixel_maps_selected_divergent
  && a.rendering_pixel_maps_selected_nominal === b.rendering_pixel_maps_selected_nominal
  && a.rendering_pixel_maps_invert === b.rendering_pixel_maps_invert
  && a.rendering_pixel_maps_inversion_disabled === b.rendering_pixel_maps_inversion_disabled;
}

export const USER_STORAGE_DEBOUNCE_TIME = 500;
