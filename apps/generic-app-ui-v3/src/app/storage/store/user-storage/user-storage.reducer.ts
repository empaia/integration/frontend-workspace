import { createReducer, on } from '@ngrx/store';
import { UserStorageActions } from './user-storage.actions';
import { UserStorage } from './user-storage.models';
import { HttpErrorResponse } from '@angular/common/http';

export const USER_STORAGE_FEATURE_KEY = 'userStorage';

export interface State {
  loaded: boolean;
  userStorage?: UserStorage | undefined;
  error?: HttpErrorResponse | undefined;
}

export const initialState: State = {
  loaded: true,
  userStorage: undefined,
  error: undefined,
};

export const reducer = createReducer(
  initialState,
  on(UserStorageActions.loadUserStorage, (state): State => ({
    ...state,
    loaded: false,
  })),
  on(UserStorageActions.loadUserStorageSuccess, (state, { userStorage }): State => ({
    ...state,
    userStorage,
    loaded: true,
  })),
  on(UserStorageActions.loadUserStorageFailure, (state, { error }): State => ({
    ...state,
    error,
    loaded: true,
  })),
  on(UserStorageActions.persistUserStorage, (state): State => ({
    ...state,
    loaded: false,
  })),
  on(UserStorageActions.persistUserStorageSuccess, (state, { userStorage }): State => ({
    ...state,
    userStorage,
    loaded: true,
  })),
  on(UserStorageActions.persistUserStorageFailure, (state, { error }): State => ({
    ...state,
    error,
    loaded: true,
  })),
);

