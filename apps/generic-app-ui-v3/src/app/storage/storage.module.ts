import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { UserStorageEffects } from './store';
import { STORAGE_MODULE_FEATURE_KEY, reducers } from './store/storage-feature.state';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    StoreModule.forFeature(
      STORAGE_MODULE_FEATURE_KEY,
      reducers
    ),
    EffectsModule.forFeature([UserStorageEffects]),
  ]
})
export class StorageModule { }
