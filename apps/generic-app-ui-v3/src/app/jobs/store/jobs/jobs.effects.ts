import { Injectable } from '@angular/core';
import { AppV3JobsService } from 'empaia-api-lib';
import { Actions, concatLatestFrom, createEffect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { fetch, pessimisticUpdate } from '@ngrx/router-store/data-persistence';
import { filterNullish, retryOnAction } from '@shared/helper/rxjs-operators';
import { forkJoin, of, timer } from 'rxjs';
import {
  exhaustMap,
  filter,
  first,
  map,
  mergeMap,
  retryWhen,
  switchMap,
  take,
  takeUntil,
} from 'rxjs/operators';
import { requestNewToken } from 'vendor-app-communication-interface';
import { JobsFeature, JOB_POLLING_PERIOD } from '..';
import * as JobsActions from './jobs.actions';
import * as CollectionsActions from '@collections/store/collections/collections.actions';
import * as ScopeActions from '@scope/store/scope/scope.actions';
import * as SlidesActions from '@slides/store/slides/slides.actions';
import * as TokenActions from '@token/store/token/token.actions';
import * as ScopeSelectors from '@scope/store/scope/scope.selectors';
import * as SlidesSelectors from '@slides/store/slides/slides.selectors';
import * as EadSelectors from '@ead/store/ead/ead.selectors';
import {
  DEFAULT_STOP_JOB_DIALOG_INPUT,
  DataCreatorType,
  JOBS_ERROR_MAP,
  JobCreatorType,
  JobKeyType,
  JobMode,
  JobSelector,
  JobStatus,
  isJobTerminated,
  isJobValidationTerminated,
} from './jobs.models';
import { MatDialog } from '@angular/material/dialog';
import { ActionDialogComponent } from '@shared/components/action-dialog/action-dialog.component';
import {
  FORBIDDEN_ERROR_STATUS_CODE,
  SMALL_DIALOG_WIDTH,
  VALIDATION_ERROR_STATUS_CODE,
} from '@menu/models/ui.models';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ErrorSnackbarComponent } from '@shared/components/error-snackbar/error-snackbar.component';
import {
  EadInputTypeAnnotation,
  EadService,
  EmpaiaAppDescriptionV3,
} from 'empaia-ui-commons';

@Injectable()
export class JobsEffects {
  // clear jobs on slide selection
  clearJobsOnSlideSelection$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(SlidesActions.selectSlide, SlidesActions.clearSlides),
      map(() => JobsActions.clearJobs())
    );
  });

  loadJobs$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(JobsActions.loadJobs),
      fetch({
        id: (
          action: ReturnType<typeof JobsActions.loadJobs>,
          _state: JobsFeature.State
        ) => {
          return action.type;
        },
        run: (
          action: ReturnType<typeof JobsActions.loadJobs>,
          _state: JobsFeature.State
        ) => {
          return this.jobsService
            .scopeIdJobsGet({
              scope_id: action.scopeId,
            })
            .pipe(
              // retry to load the selected slide after the token is expired
              retryWhen((errors) =>
                retryOnAction(
                  errors,
                  this.actions$,
                  TokenActions.setAccessToken,
                  requestNewToken
                )
              ),
              map((response) => {
                const wsiInputKey = this.eadService.getV3WsiInputKey(
                  action.ead,
                  action.activeMode
                ) as string;
                return response.items.filter(
                  (job) =>
                    job.inputs[wsiInputKey] === action.slideId &&
                    job.mode === action.activeMode.toUpperCase()
                );
              }),
              map((jobs) =>
                JobsActions.loadJobsSuccess({
                  jobs,
                  // selectAll: action.activeMode !== 'preprocessing'
                  selectAll:
                    action.activeMode !== 'preprocessing' &&
                    action.activeRoiMode !== 'multiple',
                })
              )
            );
        },
        onError: (_action: ReturnType<typeof JobsActions.loadJobs>, error) => {
          return JobsActions.loadJobsFailure({ error });
        },
      })
    );
  });

  jobsPolling$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(JobsActions.startJobsPolling),
      switchMap(() =>
        timer(0, JOB_POLLING_PERIOD).pipe(
          takeUntil(this.actions$.pipe(ofType(JobsActions.stopJobsPolling))),
          concatLatestFrom(() => [
            this.store
              .select(ScopeSelectors.selectScopeId)
              .pipe(filterNullish()),
            this.store
              .select(SlidesSelectors.selectSelectedSlideId)
              .pipe(filterNullish()),
            this.store.select(ScopeSelectors.selectExtendedScope).pipe(
              filterNullish(),
              map((extended) => extended.ead as EmpaiaAppDescriptionV3)
            ),
            this.store.select(EadSelectors.selectActiveMode),
            this.store.select(EadSelectors.selectActiveRoiMode),
          ]),
          map(([, scopeId, slideId, ead, activeMode, activeRoiMode]) =>
            JobsActions.loadJobs({
              scopeId,
              slideId,
              ead,
              activeMode,
              activeRoiMode,
            })
          )
        )
      )
    );
  });

  stopJobsPolling$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(JobsActions.loadJobsSuccess),
      map((action) => action.jobs),
      concatLatestFrom(() => this.store.select(EadSelectors.selectActiveMode)),
      filter(([jobs]) =>
        jobs.every(
          (job) =>
            isJobTerminated(job) &&
            (job.status === JobStatus.Assembly ||
              isJobValidationTerminated(job))
        )
      ),
      map(() => JobsActions.stopJobsPolling())
    );
  });

  startJobsPolling$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(
        // JobsActions.runJob,
        JobsActions.runJobSuccess,
        SlidesActions.selectSlide
      ),
      map(() => JobsActions.startJobsPolling())
    );
  });

  prepareCreateJob$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(JobsActions.createJob),
      concatLatestFrom(() => [
        this.store.select(ScopeSelectors.selectScopeId).pipe(filterNullish()),
        this.store.select(EadSelectors.selectActiveMode),
      ]),
      map(([, scopeId, mode]) =>
        JobsActions.createJobReady({
          scopeId,
          mode: mode.toUpperCase() as JobMode,
          containerized: mode !== 'postprocessing',
        })
      )
    );
  });

  createJob$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(JobsActions.createJobReady),
      pessimisticUpdate({
        run: (
          action: ReturnType<typeof JobsActions.createJobReady>,
          _state: JobsFeature.State
        ) => {
          return this.jobsService
            .scopeIdJobsPost({
              scope_id: action.scopeId,
              body: {
                creator_id: action.scopeId,
                creator_type: JobCreatorType.Scope,
                mode: action.mode,
                containerized: action.containerized,
              },
            })
            .pipe(
              retryWhen((errors) =>
                retryOnAction(
                  errors,
                  this.actions$,
                  TokenActions.setAccessToken,
                  requestNewToken
                )
              ),
              map((job) =>
                JobsActions.createJobSuccess({
                  job,
                  annotation: action.annotation,
                })
              )
            );
        },
        onError: (
          _action: ReturnType<typeof JobsActions.createJobReady>,
          error
        ) => {
          return JobsActions.createJobFailure({ error });
        },
      })
    );
  });

  setIdToJobInput$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(JobsActions.setIdToJobInput),
      pessimisticUpdate({
        run: (
          action: ReturnType<typeof JobsActions.setIdToJobInput>,
          _state: JobsFeature.State
        ) => {
          return this.jobsService
            .scopeIdJobsJobIdInputsInputKeyPut({
              scope_id: action.scopeId,
              job_id: action.jobId,
              input_key: action.inputKey,
              body: {
                id: action.inputId,
              },
            })
            .pipe(
              retryWhen((errors) =>
                retryOnAction(
                  errors,
                  this.actions$,
                  TokenActions.setAccessToken,
                  requestNewToken
                )
              ),
              map((job) => JobsActions.setJobInputOrOutputSuccess({ job }))
            );
        },
        onError: (
          _action: ReturnType<typeof JobsActions.setIdToJobInput>,
          error
        ) => {
          return JobsActions.setJobInputOrOutputFailure({ error });
        },
      })
    );
  });

  setIdToJobOutput$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(JobsActions.setIdToJobOutput),
      pessimisticUpdate({
        run: (
          action: ReturnType<typeof JobsActions.setIdToJobOutput>,
          _state: JobsFeature.State
        ) => {
          return this.jobsService
            .scopeIdJobsJobIdOutputsOutputKeyPut({
              scope_id: action.scopeId,
              job_id: action.jobId,
              output_key: action.outputKey,
              body: {
                id: action.outputId,
              },
            })
            .pipe(
              retryWhen((errors) =>
                retryOnAction(
                  errors,
                  this.actions$,
                  TokenActions.setAccessToken,
                  requestNewToken
                )
              ),
              map((job) => JobsActions.setJobInputOrOutputSuccess({ job }))
            );
        },
        onError: (
          _action: ReturnType<typeof JobsActions.setIdToJobOutput>,
          error
        ) => {
          return JobsActions.setJobInputOrOutputFailure({ error });
        },
      })
    );
  });

  removeJobInputKey$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(JobsActions.removeJobInputKey),
      pessimisticUpdate({
        run: (
          action: ReturnType<typeof JobsActions.removeJobInputKey>,
          _state: JobsFeature.State
        ) => {
          return this.jobsService
            .scopeIdJobsJobIdInputsInputKeyDelete({
              scope_id: action.scopeId,
              job_id: action.jobId,
              input_key: action.inputKey,
            })
            .pipe(
              retryWhen((errors) =>
                retryOnAction(
                  errors,
                  this.actions$,
                  TokenActions.setAccessToken,
                  requestNewToken
                )
              ),
              map((job) =>
                JobsActions.removeJobInputOrOutputKeySuccess({ job })
              )
            );
        },
        onError: (
          _action: ReturnType<typeof JobsActions.removeJobInputKey>,
          error
        ) => {
          return JobsActions.removeJobInputOrOutputKeyFailure({ error });
        },
      })
    );
  });

  removeJobOutputKey$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(JobsActions.removeJobOutputKey),
      pessimisticUpdate({
        run: (
          action: ReturnType<typeof JobsActions.removeJobOutputKey>,
          _state: JobsFeature.State
        ) => {
          return this.jobsService
            .scopeIdJobsJobIdOutputsOutputKeyDelete({
              scope_id: action.scopeId,
              job_id: action.jobId,
              output_key: action.outputKey,
            })
            .pipe(
              retryWhen((errors) =>
                retryOnAction(
                  errors,
                  this.actions$,
                  TokenActions.setAccessToken,
                  requestNewToken
                )
              ),
              map((job) =>
                JobsActions.removeJobInputOrOutputKeySuccess({ job })
              )
            );
        },
        onError: (
          _action: ReturnType<typeof JobsActions.removeJobOutputKey>,
          error
        ) => {
          return JobsActions.removeJobInputOrOutputKeyFailure({ error });
        },
      })
    );
  });

  prepareRunJob$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(JobsActions.runJob),
      map((action) => action.jobId),
      concatLatestFrom(() =>
        this.store.select(ScopeSelectors.selectScopeId).pipe(filterNullish())
      ),
      map(([jobId, scopeId]) =>
        JobsActions.runJobReady({
          scopeId,
          jobId,
        })
      )
    );
  });

  runJob$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(JobsActions.runJobReady),
      pessimisticUpdate({
        run: (
          action: ReturnType<typeof JobsActions.runJobReady>,
          _state: JobsFeature.State
        ) => {
          return this.jobsService
            .scopeIdJobsJobIdRunPut({
              scope_id: action.scopeId,
              job_id: action.jobId,
            })
            .pipe(
              retryWhen((errors) =>
                retryOnAction(
                  errors,
                  this.actions$,
                  TokenActions.setAccessToken,
                  requestNewToken
                )
              ),
              map((job) => JobsActions.runJobSuccess({ job }))
            );
        },
        onError: (
          _action: ReturnType<typeof JobsActions.runJobReady>,
          error
        ) => {
          return JobsActions.runJobFailure({ error });
        },
      })
    );
  });

  // select first job item in preprocessing mode or multi roi mode
  // only do this the first time after all jobs are loaded
  selectFirstJob$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(JobsActions.loadJobsSuccess),
      map((action) => action.jobs),
      first(),
      map((jobs) => jobs[0]?.id),
      filterNullish(),
      concatLatestFrom(() => [
        this.store.select(EadSelectors.selectActiveMode),
        this.store.select(EadSelectors.selectActiveRoiMode),
      ]),
      filter(
        ([, activeMode, activeRoiMode]) =>
          activeMode === 'preprocessing' || activeRoiMode === 'multiple'
      ),
      map(
        ([jobId]) =>
          ({
            id: jobId,
            checked: true,
          } as JobSelector)
      ),
      map((jobSelection) => JobsActions.setUniqueJobSelection({ jobSelection }))
    );
  });

  // select the new created job when in multi roi mode
  selectCreatedMultiRoiJob$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(JobsActions.createJobSuccess),
      map((action) => action.job.id),
      concatLatestFrom(() => [
        this.store.select(EadSelectors.selectActiveRoiMode),
      ]),
      filter(([, activeRoiMode]) => activeRoiMode === 'multiple'),
      map(([id]) => ({ id, checked: true } as JobSelector)),
      map((jobSelection) => JobsActions.setUniqueJobSelection({ jobSelection }))
    );
  });

  // start single roi job when all keys are set for the job
  startSingleRoiJob$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(JobsActions.createJobSuccess),
      concatLatestFrom(() => [
        this.store.select(EadSelectors.selectActiveRoiMode),
        this.store.select(ScopeSelectors.selectExtendedScope).pipe(
          filterNullish(),
          map((extended) => extended.ead as EmpaiaAppDescriptionV3)
        ),
        this.store.select(EadSelectors.selectActiveMode),
      ]),
      filter(
        ([action, activeRoiMode]) =>
          activeRoiMode === 'single' && !!action.annotation
      ),
      switchMap(([action, _activeRoiMode, ead, activeMode]) => {
        const wsiKey = this.eadService.getV3TypeInputKey('wsi', ead, activeMode)
          ?.inputKey as string;
        const annotationKey = this.eadService.getV3TypeInputKey(
          action.annotation?.annotationType as EadInputTypeAnnotation,
          ead,
          activeMode
        )?.inputKey as string;

        // create an observable which completes when the wsi
        // was set as an input to the job
        const wsi$ = this.actions$.pipe(
          ofType(JobsActions.setJobInputOrOutputSuccess),
          map((action) => action.job),
          filter((job) => !!job.inputs[wsiKey]),
          take(1)
        );
        // create an observable which completes when the annotation
        // was set as an input to the job
        const annotation$ = this.actions$.pipe(
          ofType(JobsActions.setJobInputOrOutputSuccess),
          map((action) => action.job),
          filter((job) => !!job.inputs[annotationKey]),
          take(1)
        );

        // Join both observables to create one observable which
        // emits when both (wsi and annotation) are completed
        // so that the job can be started
        return forkJoin([wsi$, annotation$]);
      }),
      map((jobs) => JobsActions.runJob({ jobId: jobs[0].id }))
    );
  });

  // set the job inputs for single roi mode
  // creates parallel requests for setting the inputs
  setInputsForSingleRoi$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(JobsActions.createJobSuccess),
      concatLatestFrom(() => [
        this.store.select(EadSelectors.selectActiveRoiMode),
        this.store.select(ScopeSelectors.selectScopeId).pipe(filterNullish()),
        this.store.select(ScopeSelectors.selectExtendedScope).pipe(
          filterNullish(),
          map((extended) => extended.ead as EmpaiaAppDescriptionV3)
        ),
        this.store.select(EadSelectors.selectActiveMode),
        this.store
          .select(SlidesSelectors.selectSelectedSlideId)
          .pipe(filterNullish()),
      ]),
      filter(
        ([action, activeRoiMode]) =>
          activeRoiMode === 'single' && !!action.annotation
      ),
      mergeMap(
        ([action, _activeRoiMode, scopeId, ead, activeMode, slideId]) => {
          const wsiKey = this.eadService.getV3TypeInputKey(
            'wsi',
            ead,
            activeMode
          )?.inputKey as string;
          const annotationKey = this.eadService.getV3TypeInputKey(
            action.annotation?.annotationType as EadInputTypeAnnotation,
            ead,
            activeMode
          )?.inputKey as string;

          return [
            JobsActions.setIdToJobInput({
              scopeId,
              jobId: action.job.id,
              inputKey: wsiKey,
              inputId: slideId,
            }),
            JobsActions.setIdToJobInput({
              scopeId,
              jobId: action.job.id,
              inputKey: annotationKey,
              inputId: action.annotation?.id as string,
            }),
          ];
        }
      )
    );
  });

  // set the job inputs for multi roi mode
  // set the wsi to job input and
  // create as many collections as they are
  // defined collections for annotations in
  // the Empaia App Description
  setInputsForMultiRoi$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(JobsActions.createJobSuccess),
      map((action) => action.job.id),
      concatLatestFrom(() => [
        this.store.select(EadSelectors.selectActiveRoiMode),
        this.store.select(ScopeSelectors.selectScopeId).pipe(filterNullish()),
        this.store.select(ScopeSelectors.selectExtendedScope).pipe(
          filterNullish(),
          map((extended) => extended.ead as EmpaiaAppDescriptionV3)
        ),
        this.store.select(EadSelectors.selectActiveMode),
        this.store
          .select(SlidesSelectors.selectSelectedSlideId)
          .pipe(filterNullish()),
      ]),
      filter(([, activeRoiMode]) => activeRoiMode === 'multiple'),
      mergeMap(([jobId, _activeRoiMode, scopeId, ead, activeMode, slideId]) => {
        const wsiKey = this.eadService.getV3TypeInputKey('wsi', ead, activeMode)
          ?.inputKey as string;
        const annotationTypes = this.eadService.getV3AnnotationInputTypes(
          ead,
          activeMode
        );
        const collectionKeys = this.eadService
          .getV3TypesInputKeysWithTypes(annotationTypes, ead, activeMode)
          .filter((input) => input.inCollection === 1);

        const collectionActions = collectionKeys.map((collectionKey) =>
          CollectionsActions.createCollection({
            scopeId,
            jobId,
            key: collectionKey.inputKey,
            keyType: JobKeyType.INPUT,
            postCollection: {
              type: 'collection',
              creator_id: scopeId,
              creator_type: DataCreatorType.Scope,
              item_type: collectionKey.type,
            },
          })
        );
        const jobInputAction = JobsActions.setIdToJobInput({
          scopeId,
          jobId,
          inputKey: wsiKey,
          inputId: slideId,
        });

        return [jobInputAction, ...collectionActions];
      })
    );
  });

  prepareJobDeletion$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(JobsActions.deleteJob),
      map((action) => action.jobId),
      concatLatestFrom(() =>
        this.store.select(ScopeSelectors.selectScopeId).pipe(filterNullish())
      ),
      map(([jobId, scopeId]) =>
        JobsActions.deleteJobReady({
          scopeId,
          jobId,
        })
      )
    );
  });

  deleteJob$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(JobsActions.deleteJobReady),
      pessimisticUpdate({
        run: (
          action: ReturnType<typeof JobsActions.deleteJobReady>,
          _state: JobsFeature.State
        ) => {
          return this.jobsService
            .scopeIdJobsJobIdDelete({
              scope_id: action.scopeId,
              job_id: action.jobId,
            })
            .pipe(
              // retry to load the selected slide after the token is expired
              retryWhen((errors) =>
                retryOnAction(
                  errors,
                  this.actions$,
                  TokenActions.setAccessToken,
                  requestNewToken
                )
              ),
              map((job) => job.id),
              map((jobId) => JobsActions.deleteJobSuccess({ jobId }))
            );
        },
        onError: (
          _action: ReturnType<typeof JobsActions.deleteJobReady>,
          error
        ) => {
          return JobsActions.deleteJobFailure({ error });
        },
      })
    );
  });

  prepareStopRunningJob$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(JobsActions.stopRunningJob),
      map((action) => action.jobId),
      concatLatestFrom(() =>
        this.store.select(ScopeSelectors.selectScopeId).pipe(filterNullish())
      ),
      map(([jobId, scopeId]) =>
        JobsActions.stopRunningJobReady({
          scopeId,
          jobId,
        })
      )
    );
  });

  stopRunningJob$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(JobsActions.stopRunningJobReady),
      pessimisticUpdate({
        run: (
          action: ReturnType<typeof JobsActions.stopRunningJobReady>,
          _state: JobsFeature.State
        ) => {
          return this.jobsService
            .scopeIdJobsJobIdStopPut({
              scope_id: action.scopeId,
              job_id: action.jobId,
            })
            .pipe(
              // retry to load the selected slide after the token is expired
              retryWhen((errors) =>
                retryOnAction(
                  errors,
                  this.actions$,
                  TokenActions.setAccessToken,
                  requestNewToken
                )
              ),
              map((done) => JobsActions.stopRunningJobSuccess({ done }))
            );
        },
        onError: (
          _action: ReturnType<typeof JobsActions.stopRunningJobReady>,
          error
        ) => {
          JobsActions.stopRunningJobFailure({ error });
          return null;
        },
      })
    );
  });

  // stop loading state on extended scope reload
  stopLoading$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ScopeActions.loadExtendedScope),
      map(() => JobsActions.stopLoading())
    );
  });

  openStopJobDialog$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(JobsActions.openStopJobDialog),
      map((action) => action.jobId),
      exhaustMap((jobId) =>
        this.dialog
          .open(ActionDialogComponent, {
            data: DEFAULT_STOP_JOB_DIALOG_INPUT,
            width: SMALL_DIALOG_WIDTH,
          })
          .afterClosed()
          .pipe(
            filterNullish(),
            map(() => JobsActions.stopRunningJob({ jobId }))
          )
      )
    );
  });

  showErrorMessage$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(
        JobsActions.runJobFailure,
        JobsActions.loadJobsFailure,
        JobsActions.createJobFailure,
        JobsActions.deleteJobFailure,
        JobsActions.stopRunningJobFailure,
        JobsActions.setJobInputOrOutputFailure,
        JobsActions.removeJobInputOrOutputKeyFailure
      ),
      filter(
        (action) =>
          !(
            action.error.status === VALIDATION_ERROR_STATUS_CODE ||
            action.error.status === FORBIDDEN_ERROR_STATUS_CODE
          )
      ),
      exhaustMap((action) => {
        this.snackBar.openFromComponent(ErrorSnackbarComponent, {
          data: { title: JOBS_ERROR_MAP[action.type], error: action.error },
        });
        return of({ type: 'noop' });
      })
    );
  });

  constructor(
    private readonly actions$: Actions,
    private readonly store: Store,
    private readonly jobsService: AppV3JobsService,
    private readonly eadService: EadService,
    private readonly dialog: MatDialog,
    private readonly snackBar: MatSnackBar
  ) {}
}
