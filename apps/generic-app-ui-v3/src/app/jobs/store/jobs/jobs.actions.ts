import { AnnotationEntity } from 'slide-viewer';
import { HttpErrorResponse } from '@angular/common/http';
import { createAction, props } from '@ngrx/store';
import { Job, JobMode, JobSelector } from './jobs.models';
import { EadRoiInputType, EmpaiaAppDescriptionV3, EmpaiaAppDescriptionV3Mode } from 'empaia-ui-commons';

export const loadJobs = createAction(
  '[APP/Jobs] Load Jobs',
  props<{
    scopeId: string,
    slideId: string,
    ead: EmpaiaAppDescriptionV3,
    activeMode: EmpaiaAppDescriptionV3Mode,
    activeRoiMode: EadRoiInputType,
  }>()
);

export const loadJobsSuccess = createAction(
  '[APP/Jobs] Load Jobs Success',
  props<{ jobs: Job[], selectAll: boolean }>()
);

export const loadJobsFailure = createAction(
  '[APP/Jobs] Load Jobs Failure',
  props<{ error: HttpErrorResponse }>()
);

export const startJobsPolling = createAction(
  '[APP/Jobs] Start Jobs Polling'
);

export const stopJobsPolling = createAction(
  '[APP/Jobs] Stop Jobs Polling'
);

export const createJob = createAction(
  '[APP/Jobs] Create Job'
);

export const createJobReady = createAction(
  '[APP/Jobs] Create Job Ready',
  props<{
    scopeId: string,
    mode: JobMode,
    containerized: boolean,
    annotation?: AnnotationEntity,
  }>(),
);

export const createJobSuccess = createAction(
  '[APP/Jobs] Create Job Success',
  props<{ job: Job, annotation?: AnnotationEntity }>(),
);

export const createJobFailure = createAction(
  '[APP/Jobs] Create Job Failure',
  props<{ error: HttpErrorResponse }>(),
);

export const setIdToJobInput = createAction(
  '[APP/Jobs] Set ID To Job Input',
  props<{
    scopeId: string,
    jobId: string,
    inputId: string,
    inputKey: string,
  }>(),
);

export const setIdToJobOutput = createAction(
  '[APP/Jobs] Set Id To Job Output',
  props<{
    scopeId: string,
    jobId: string,
    outputId: string,
    outputKey: string,
  }>(),
);

export const setJobInputOrOutputSuccess = createAction(
  '[APP/Jobs] Set Job Input Or Output Success',
  props<{ job: Job }>(),
);

export const setJobInputOrOutputFailure = createAction(
  '[APP/Jobs] Set Job Input Or Output Failure',
  props<{ error: HttpErrorResponse }>(),
);

export const removeJobInputKey = createAction(
  '[APP/Jobs] Remove Job Input Key',
  props<{
    scopeId: string,
    jobId: string,
    inputKey: string,
  }>(),
);

export const removeJobOutputKey = createAction(
  '[APP/Jobs] Remove Job Output Key',
  props<{
    scopeId: string,
    jobId: string,
    outputKey: string,
  }>(),
);

export const removeJobInputOrOutputKeySuccess = createAction(
  '[APP/Jobs] Remove Job Input Or Output Key Success',
  props<{ job: Job }>(),
);

export const removeJobInputOrOutputKeyFailure = createAction(
  '[APP/Jobs] Remove Job Input Or Output Key Failure',
  props<{ error: HttpErrorResponse }>(),
);

export const runJob = createAction(
  '[APPS/Jobs] Run Job',
  props<{ jobId: string }>()
);

export const runJobReady = createAction(
  '[APP/Jobs] Run Job Ready',
  props<{ scopeId: string, jobId: string }>(),
);

export const runJobSuccess = createAction(
  '[APP/Jobs] Run Job Success',
  props<{ job: Job }>(),
);

export const runJobFailure = createAction(
  '[APP/Jobs] Run Job Failure',
  props<{ error: HttpErrorResponse }>(),
);

export const deleteJob = createAction(
  '[APP/Jobs] Delete Job',
  props<{ jobId: string }>(),
);

export const deleteJobReady = createAction(
  '[APP/Jobs] Delete Job Ready',
  props<{ scopeId: string, jobId: string }>()
);

export const deleteJobSuccess = createAction(
  '[APP/Jobs] Delete Job Success',
  props<{ jobId: string }>()
);

export const deleteJobFailure = createAction(
  '[APP/Jobs] Delete Job Failure',
  props<{ error: HttpErrorResponse }>()
);

export const setJobSelection = createAction(
  '[APP/Jobs] Set Job Selection',
  props<{ jobSelection: JobSelector }>()
);

export const setJobsSelections = createAction(
  '[APP/Jobs] Set Jobs Selections',
  props<{ jobsSelections: JobSelector[] }>()
);

// select one job and deselect all other
export const setUniqueJobSelection = createAction(
  '[APP/Jobs] Set Unique Job Selection',
  props<{ jobSelection: JobSelector }>(),
);

export const showAllJobs = createAction(
  '[APP/Jobs] Show All Jobs',
);

export const hideAllJobs = createAction(
  '[APP/Jobs] Hide All Jobs',
);

export const clearJobs = createAction(
  '[APP/Jobs] Clear Jobs',
);

export const stopRunningJob = createAction(
  '[Jobs] Stop Running Job',
  props<{ jobId: string }>()
);

export const stopRunningJobReady = createAction(
  '[Jobs] Stop Running Job Ready',
  props<{ scopeId: string, jobId: string }>()
);

export const stopRunningJobSuccess = createAction(
  '[Jobs] Stop Running Job Success',
  props<{ done: boolean }>()
);

export const stopRunningJobFailure = createAction(
  '[Jobs] Stop Running Job Failure',
  props<{ error: HttpErrorResponse }>()
);

export const stopLoading = createAction(
  '[Jobs] Stop Loading',
);

export const openStopJobDialog = createAction(
  '[Jobs] Open Stop Job Dialog',
  props<{ jobId: string }>(),
);
