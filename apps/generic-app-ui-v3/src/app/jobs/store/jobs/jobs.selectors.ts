import { AnnotationEntity } from 'slide-viewer';
import { createSelector } from '@ngrx/store';
import {
  State as ModuleState,
  selectJobsFeatureState,
} from '../jobs-feature.state';
import { jobAdapter, jobSelectorAdapter } from './jobs.reducer';
import * as AnnotationsSelectors from '@annotations/store/annotations/annotations.selectors';
import { Job, JobRoiEntity } from './jobs.models';
import { selectCollectionEntities, selectCollectionsLoaded } from '@collections/store/collections/collections.selectors';
import { selectAnnotationEntities } from '@annotations/store/annotations/annotations.selectors';
import { AnnotationCollectionEntity } from '@annotations/store';

const {
  selectIds,
  selectAll,
  selectEntities,
} = jobAdapter.getSelectors();

export const selectJobState = createSelector(
  selectJobsFeatureState,
  (state: ModuleState) => state.jobs
);

export const selectJobsLoaded = createSelector(
  selectJobState,
  (state) => state.loaded
);

export const selectAllJobs = createSelector(
  selectJobState,
  selectAll,
);

export const selectJobEntities = createSelector(
  selectJobState,
  selectEntities,
);

export const selectAllJobIds = createSelector(
  selectJobState,
  (state) => selectIds(state) as string[]
);

export const selectAllSelectedState = createSelector(
  selectJobState,
  (state) => state.allSelected
);

export const selectAllJobsSelections = createSelector(
  selectJobState,
  (state) => jobSelectorAdapter.getSelectors().selectAll(state.selectedJobs)
);

export const selectJobSelectionEntities = createSelector(
  selectJobState,
  (state) => jobSelectorAdapter.getSelectors().selectEntities(state.selectedJobs)
);

export const selectAllCheckedJobIds = createSelector(
  selectAllJobsSelections,
  (jobsSelections) => jobsSelections.filter(j => !!j.checked).map(j => j.id)
);

export const selectAllCheckedJobs = createSelector(
  selectAllCheckedJobIds,
  selectJobEntities,
  (jobIds, entities) => jobIds.map(id => entities[id]).filter(job => !!job) as Job[]
);

export const selectAllUncheckedJobIds = createSelector(
  selectAllJobsSelections,
  (jobsSelections) => jobsSelections.filter(j => !j.checked).map(j => j.id)
);

export const selectAllUncheckedJobs = createSelector(
  selectAllUncheckedJobIds,
  selectJobEntities,
  (jobIds, entities) => jobIds.map(id => entities[id]).filter(job => !!job) as Job[]
);

export const selectAllJobsWithRois = createSelector(
  selectAllJobs,
  AnnotationsSelectors.selectAnnotationEntities,
  (jobs, annotationEntities) => {
    const jobRoiEntities: JobRoiEntity[] = [];

    jobs.forEach(job => {
      const inputs = Object.values(job.inputs);
      const annotations = inputs.map(i => annotationEntities[i]).filter(a => !!a) as AnnotationEntity[];
      const jobRoiEntity: JobRoiEntity = {
        job,
        roi: annotations[0],
      };
      if (jobRoiEntity.roi) {
        jobRoiEntities.push(jobRoiEntity);
      }
    });

    return jobRoiEntities;
  }
);

export const selectAllJobCollectionsRois = createSelector(
  selectAllCheckedJobs,
  selectCollectionEntities,
  selectAnnotationEntities,
  (jobs, collectionEntities, annotationEntities) =>
    jobs.map(job =>
      Object
        .values(job.inputs)
        .map(id => collectionEntities[id])
        .filter(c => !!c)
        .map(c => c?.item_ids
          ?.map(id => annotationEntities[id])
          .filter(a => !!a)
          .map(a => ({ ...a, collectionId: c.id } as AnnotationCollectionEntity)) as AnnotationCollectionEntity[]
        )
    ).flat(2)
);

export const selectAllJobCollectionsRoisIds = createSelector(
  selectAllJobCollectionsRois,
  (annotations) => annotations.map(a => a.id)
);

export const selectAllJobCollectionsRoisLoaded = createSelector(
  selectAllJobCollectionsRois,
  selectCollectionsLoaded,
  (annotations, collectionsLoaded) => !!annotations?.length || collectionsLoaded
);
