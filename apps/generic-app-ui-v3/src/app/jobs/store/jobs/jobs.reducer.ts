import { HttpErrorResponse } from '@angular/common/http';
import { EntityState, createEntityAdapter } from '@ngrx/entity';
import { createReducer, on } from '@ngrx/store';
import { Job, JobSelector, JobStatus, sortByCreationDate } from './jobs.models';
import * as JobsActions from './jobs.actions';
import { upsertManyWithoutUpdate } from '@shared/helper/ngrx-operators';
import { isNotNullish, isString } from '@shared/helper/array-operators';


export const JOBS_FEATURE_KEY = 'jobs';

export interface State extends EntityState<Job> {
  selectedJobs: EntityState<JobSelector>;
  allSelected: boolean;
  loaded: boolean;
  error?: HttpErrorResponse | null;
}

export const jobAdapter = createEntityAdapter<Job>({
  sortComparer: sortByCreationDate,
});
export const jobSelectorAdapter = createEntityAdapter<JobSelector>();

export const initialState: State = jobAdapter.getInitialState({
  selectedJobs: jobSelectorAdapter.getInitialState(),
  allSelected: true,
  loaded: true,
  error: null,
});

export const reducer = createReducer(
  initialState,
  on(JobsActions.loadJobs, (state): State => ({
    ...state,
    loaded: false,
  })),
  on(JobsActions.loadJobsSuccess, (state, { jobs, selectAll }): State =>
    jobAdapter.setAll(jobs, {
      ...state,
      loaded: true,
      selectedJobs: upsertManyWithoutUpdate(jobSelectorAdapter, convertJobsToJobsSelectors(jobs, selectAll), state.selectedJobs)
    })
  ),
  on(JobsActions.loadJobsFailure, (state, { error }): State => ({
    ...state,
    error,
  })),
  // on(JobsActions.createJob, (state): State => ({
  //   ...state,
  //   loaded: false,
  // })),
  // on(JobsActions.createJobSuccess, (state, { job }): State =>
  //   jobAdapter.upsertOne(job, {
  //     ...state,
  //     loaded: true,
  //     selectedJobs: jobSelectorAdapter.addOne(convertJobToJobSelector(job, true), {
  //       ...state.selectedJobs
  //     })
  //   })
  // ),
  // on(JobsActions.createJobFailure, (state, { error }): State => ({
  //   ...state,
  //   error,
  // })),
  // on(JobsActions.deleteJobSuccess, (state, { jobId }): State =>
  //   jobAdapter.removeOne(jobId, {
  //     ...state,
  //     selectedJobs: jobSelectorAdapter.removeOne(jobId, {
  //       ...state.selectedJobs
  //     })
  //   })
  // ),
  on(JobsActions.createJobReady, (state): State => ({
    ...state,
    loaded: false,
  })),
  on(JobsActions.createJobSuccess, (state, { job, annotation }): State =>
    jobAdapter.addOne(job, {
      ...state,
      loaded: true,
      selectedJobs: annotation
        ? jobSelectorAdapter.addOne(
          convertJobToJobSelector(job, true),
          {
            ...state.selectedJobs
          }
        )
        : state.selectedJobs
    })
  ),
  on(JobsActions.createJobFailure, (state, { error }): State => ({
    ...state,
    loaded: true,
    error,
  })),
  on(JobsActions.setIdToJobInput, (state): State => ({
    ...state,
    loaded: false,
  })),
  on(JobsActions.setIdToJobOutput, (state): State => ({
    ...state,
    loaded: false,
  })),
  on(JobsActions.setJobInputOrOutputSuccess, (state, { job }): State =>
    jobAdapter.upsertOne(job, {
      ...state,
      loaded: true,
    })
  ),
  on(JobsActions.setJobInputOrOutputFailure, (state, { error }): State => ({
    ...state,
    loaded: true,
    error,
  })),
  on(JobsActions.removeJobInputKey, (state): State => ({
    ...state,
    loaded: false,
  })),
  on(JobsActions.removeJobOutputKey, (state): State => ({
    ...state,
    loaded: false,
  })),
  on(JobsActions.removeJobInputOrOutputKeySuccess, (state, { job }): State =>
    jobAdapter.upsertOne(job, {
      ...state,
      loaded: true,
    })
  ),
  on(JobsActions.removeJobInputOrOutputKeyFailure, (state, { error }): State => ({
    ...state,
    loaded: true,
    error,
  })),
  on(JobsActions.runJob, (state, { jobId }): State =>
    jobAdapter.updateOne({
      id: jobId,
      changes: {
        status: JobStatus.Ready
      }
    }, {
      ...state
    })
  ),
  on(JobsActions.runJobReady, (state): State => ({
    ...state,
    loaded: false,
  })),
  on(JobsActions.runJobSuccess, (state, { job }): State =>
    jobAdapter.upsertOne(job, {
      ...state,
      loaded: true,
    })
  ),
  on(JobsActions.runJobFailure, (state, { error }): State => ({
    ...state,
    loaded: true,
    error,
  })),
  on(JobsActions.deleteJobReady, (state): State => ({
    ...state,
    loaded: false,
  })),
  on(JobsActions.deleteJobSuccess, (state, { jobId }): State =>
    jobAdapter.removeOne(jobId, {
      ...state,
      loaded: true,
      selectedJobs: jobSelectorAdapter.removeOne(jobId, {
        ...state.selectedJobs
      })
    })
  ),
  on(JobsActions.deleteJobFailure, (state, { error }): State => ({
    ...state,
    loaded: true,
    error,
  })),
  on(JobsActions.setJobSelection, (state, { jobSelection }): State => ({
    ...state,
    selectedJobs: jobSelectorAdapter.upsertOne(jobSelection, {
      ...state.selectedJobs
    })
  })),
  on(JobsActions.setUniqueJobSelection, (state, { jobSelection }): State => ({
    ...state,
    // only check the selected job and uncheck all others
    selectedJobs: jobSelectorAdapter.setAll(
      [...state.ids].filter(isNotNullish).filter(isString).map(id => id !== jobSelection.id ? convertIdToJobSelector(id, false) : jobSelection),
      {
        ...state.selectedJobs
      }
    )
  })),
  on(JobsActions.setJobsSelections, (state, { jobsSelections }): State => ({
    ...state,
    selectedJobs: jobSelectorAdapter.upsertMany(jobsSelections, {
      ...state.selectedJobs
    })
  })),
  on(JobsActions.showAllJobs, (state): State => ({
    ...state,
    allSelected: true,
    selectedJobs: jobSelectorAdapter.setAll(
      [...state.ids].filter(isNotNullish).filter(isString).map(id => convertIdToJobSelector(id, true)),
      {
        ...state.selectedJobs
      }
    )
  })),
  on(JobsActions.hideAllJobs, (state): State => ({
    ...state,
    allSelected: false,
    selectedJobs: jobSelectorAdapter.setAll(
      [...state.ids].filter(isNotNullish).filter(isString).map(id => convertIdToJobSelector(id, false)),
      {
        ...state.selectedJobs
      }
    )
  })),
  on(JobsActions.clearJobs, (state): State =>
    jobAdapter.removeAll({
      ...state,
      selectedJobs: jobSelectorAdapter.removeAll({
        ...state.selectedJobs
      }),
      allSelected: true,
      loaded: true,
      error: null,
    })
  ),
  on(JobsActions.stopRunningJob, (state): State => ({
    ...state,
    loaded: false,
  })),
  on(JobsActions.stopRunningJobSuccess, (state): State => ({
    ...state,
    loaded: true,
  })),
  on(JobsActions.stopRunningJobFailure, (state, { error }): State => ({
    ...state,
    error,
    loaded: true,
  })),
  on(JobsActions.stopLoading, (state): State => ({
    ...state,
    loaded: true,
  })),
);

function convertJobToJobSelector(job: Job, checked: boolean): JobSelector {
  return {
    id: job.id,
    checked,
  };
}

function convertJobsToJobsSelectors(jobs: Job[], checked: boolean): JobSelector[] {
  return jobs.map(job => convertJobToJobSelector(job, checked));
}

function convertIdToJobSelector(id: string, checked: boolean): JobSelector {
  return {
    id,
    checked,
  };
}
