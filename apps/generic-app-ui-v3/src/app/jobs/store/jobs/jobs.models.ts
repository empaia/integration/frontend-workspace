import { AnnotationEntity } from 'slide-viewer';
import { ActionDialogInput } from '@shared/components/action-dialog/action-dialog.models';
import { CREATE_JOB_ERROR, DELETE_JOB_ERROR, DELETE_JOB_INPUT_OUTPUT_ERROR, ErrorMap, JOBS_LOAD_ERROR, UPDATE_JOB_ERROR } from '@menu/models/ui.models';
import {
  AppV3JobStatus,
  AppV3JobValidationStatus,
  AppV3Job
} from 'empaia-api-lib';
export {
  AppV3JobStatus as JobStatus,
  AppV3JobValidationStatus as JobValidationStatus,
  AppV3JobMode as JobMode,
  AppV3JobCreatorType as JobCreatorType,
  AppV3DataCreatorType as DataCreatorType,
} from 'empaia-api-lib';

export type Job = AppV3Job;

export interface JobSelector {
  id: string;
  checked: boolean;
}

export interface JobRoiEntity {
  job: Job;
  roi: AnnotationEntity;
}

export enum JobKeyType {
  INPUT = 'input',
  OUTPUT = 'output',
}

export const JOB_POLLING_PERIOD = 5000;

export function isJobRunning(job: Job): boolean {
  // return job.status === JobStatus.Assembly
  return job.status === AppV3JobStatus.Ready
    || job.status === AppV3JobStatus.Scheduled
    || job.status === AppV3JobStatus.Running;
}

export function isJobTerminated(job: Job): boolean {
  return (
    job.status === AppV3JobStatus.Completed
    || job.status === AppV3JobStatus.Error
    || job.status === AppV3JobStatus.Failed
    || job.status === AppV3JobStatus.Incomplete
    || job.status === AppV3JobStatus.None
    || job.status === AppV3JobStatus.Timeout
    || job.status === AppV3JobStatus.Assembly
  );
}

export function isJobValidationRunning(job: Job): boolean {
  return !job.input_validation_status
    || !job.output_validation_status
    || job.input_validation_status === AppV3JobValidationStatus.None
    || job.input_validation_status === AppV3JobValidationStatus.Running
    || job.output_validation_status === AppV3JobValidationStatus.None
    || job.output_validation_status === AppV3JobValidationStatus.Running;
}

export function isJobValidationTerminated(job: Job): boolean {
  return !!job.input_validation_status
    && !!job.output_validation_status
    && (
      job.input_validation_status === AppV3JobValidationStatus.Completed
      || job.input_validation_status === AppV3JobValidationStatus.Error
    )
    && (
      job.output_validation_status === AppV3JobValidationStatus.Completed
      || job.output_validation_status === AppV3JobValidationStatus.Error
    );
}

export function sortByCreationDate(a: Job, b: Job): number {
  return b.created_at - a.created_at;
}

export const DEFAULT_STOP_JOB_DIALOG_INPUT: ActionDialogInput = {
  title: 'Stop Job',
  message: 'Do you want to stop the current job?',
};

export const JOBS_ERROR_MAP: ErrorMap = {
  '[APP/Jobs] Load Jobs Failure': JOBS_LOAD_ERROR,
  '[APP/Jobs] Create Job Failure': CREATE_JOB_ERROR,
  '[APP/Jobs] Set Job Input Or Output Failure': UPDATE_JOB_ERROR,
  '[APP/Jobs] Remove Job Input Or Output Key Failure': DELETE_JOB_INPUT_OUTPUT_ERROR,
  '[APP/Jobs] Run Job Failure': UPDATE_JOB_ERROR,
  '[APP/Jobs] Delete Job Failure': DELETE_JOB_ERROR,
  '[Jobs] Stop Running Job Failure': UPDATE_JOB_ERROR,
};

export function compareJobStatus(prev: AppV3Job[], curr: AppV3Job[]): boolean {
  return prev.length === curr.length && prev.every((job, index) => job.status === curr[index].status);
}
