import { Action, combineReducers, createFeatureSelector } from '@ngrx/store';
import * as fromJobs from '@jobs/store/jobs/jobs.reducer';

export const JOBS_MODULE_FEATURE_KEY = 'jobsModuleFeature';

export const selectJobsFeatureState = createFeatureSelector<State>(
  JOBS_MODULE_FEATURE_KEY
);

export interface State {
  [fromJobs.JOBS_FEATURE_KEY]: fromJobs.State;
}

export function reducers(state: State | undefined, action: Action) {
  return combineReducers({
    [fromJobs.JOBS_FEATURE_KEY]: fromJobs.reducer,
  })(state, action);
}
