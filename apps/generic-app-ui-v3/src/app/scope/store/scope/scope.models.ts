import { AppV3ExtendedScope } from 'empaia-api-lib';
import { EXTENDED_SCOPE_LOAD_ERROR, ErrorMap } from '@menu/models/ui.models';

export type ExtendedScope = AppV3ExtendedScope;

export const SCOPE_ERROR_MAP: ErrorMap = {
  '[APP/Scope] Load Extended Scope Failure': EXTENDED_SCOPE_LOAD_ERROR,
};
