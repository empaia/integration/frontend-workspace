import { Injectable } from '@angular/core';
import { ScopeServiceInterface } from 'empaia-ui-commons';

@Injectable({
  providedIn: 'root'
})
export class ScopeService implements ScopeServiceInterface {
  private _scopeId!: string;

  public get scopeId() {
    return this._scopeId;
  }

  public set scopeId(val: string) {
    this._scopeId = val;
  }
}
