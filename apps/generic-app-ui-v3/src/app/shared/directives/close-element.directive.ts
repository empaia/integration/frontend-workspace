import { Directive, EventEmitter, HostListener, Input, Output } from '@angular/core';

@Directive({
  selector: '[appCloseElement]'
})
export class CloseElementDirective {
  @Input() public open = false;
  @Input() public set relatedOrigin(val: HTMLElement | undefined) {
    if (val) {
      val.addEventListener('mouseenter', () => {
        this.isOutSide = false;
      });
      val.addEventListener('mouseleave', () => {
        this.isOutSide = true;
      });
    }
  }

  @Output() public closeElement = new EventEmitter<void>();

  private isOutSide = false;

  @HostListener('window:click')
  public onMouseClick(): void {
    if (this.open && this.isOutSide) {
      this.closeElement.emit();
    }
  }

  @HostListener('mouseenter')
  public onMouseEnter(): void {
    this.isOutSide = false;
  }

  @HostListener('mouseleave')
  public onMouseLeave(): void {
    this.isOutSide = true;
  }
}
