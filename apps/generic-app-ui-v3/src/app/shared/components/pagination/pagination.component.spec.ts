import { PaginationComponent } from './pagination.component';
import { createComponentFactory, Spectator } from '@ngneat/spectator';
import { MaterialModule } from '@material/material.module';

describe('PaginationComponent', () => {
  let spectator: Spectator<PaginationComponent>;
  const createComponent = createComponentFactory({
    component: PaginationComponent,
    imports: [
      MaterialModule,
    ]
  });

  beforeEach(() => spectator = createComponent());

  it('should create', () => {
    expect(spectator.component).toBeTruthy();
  });
});
