import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { JobStatus, JobValidationStatus } from '@jobs/store';

@Component({
  selector: 'app-job-status',
  templateUrl: './job-status.component.html',
  styleUrls: ['./job-status.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class JobStatusComponent {
  public readonly JOB_STATUS = JobStatus;
  public readonly JOB_VALIDATION_STATUS = JobValidationStatus;
  @Input() public jobStatus!: JobStatus;
  @Input() public jobInputStatus!: JobValidationStatus;
  @Input() public jobOutputStatus!: JobValidationStatus;
  @Input() public jobIndicatorClass!: string;

  @Output() public loadingSpinnerClicked = new EventEmitter<void>();
}
