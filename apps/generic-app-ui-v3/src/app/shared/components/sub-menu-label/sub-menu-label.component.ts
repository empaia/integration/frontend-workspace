import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

const justifyContent = [
  'flex-start',
  'flex-end',
  'start',
  'end',
  'left',
  'right',
  'center',
  'space-around',
  'space-between',
  'space-evenly'
] as const;

type JustifyContent = typeof justifyContent[number];

@Component({
  selector: 'app-sub-menu-label',
  templateUrl: './sub-menu-label.component.html',
  styleUrls: ['./sub-menu-label.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SubMenuLabelComponent {
  @Input() public justifyContent: JustifyContent = 'flex-start';
}
