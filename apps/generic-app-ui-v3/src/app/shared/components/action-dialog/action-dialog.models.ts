export interface ActionDialogInput {
  title?: string;
  message?: string;
  confirm?: string;
  cancel?: string;
}
