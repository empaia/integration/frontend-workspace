import { ChangeDetectionStrategy, Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ActionDialogInput } from './action-dialog.models';

@Component({
  selector: 'app-action-dialog',
  templateUrl: './action-dialog.component.html',
  styleUrls: ['./action-dialog.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ActionDialogComponent {
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: ActionDialogInput,
  ) {}
}
