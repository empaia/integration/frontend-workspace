import { MatSnackBarRef, MAT_SNACK_BAR_DATA } from '@angular/material/snack-bar';
import { MaterialModule } from '@material/material.module';
import { Spectator, createComponentFactory } from '@ngneat/spectator/jest';

import { ErrorSnackbarComponent } from './error-snackbar.component';

describe('ErrorSnackbarComponent', () => {
  let spectator: Spectator<ErrorSnackbarComponent>;
  const createComponent = createComponentFactory({
    component: ErrorSnackbarComponent,
    imports: [
      MaterialModule,
    ],
    providers: [
      {
        provide: MAT_SNACK_BAR_DATA,
        useValue: { data: {} }
      },
      {
        provide: MatSnackBarRef,
        useValue: {} //MatSnackBarRef<ErrorSnackbarComponent>
      }
    ]
  });

  it('should create', () => {
    spectator = createComponent();

    expect(spectator.component).toBeTruthy();
  });
});
