import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'dateCorrection'
})
export class DateCorrectionPipe implements PipeTransform {

  transform(value: number | string): number | string {
    if (typeof value === 'number') {
      return value * 1000;
    }
    else if (typeof value === 'string') {
      return value.concat('000');
    }
    else {
      throw Error('Not type for date correction');
    }
  }

}
