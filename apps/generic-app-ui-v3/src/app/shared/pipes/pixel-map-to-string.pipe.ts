import { Pipe, PipeTransform } from '@angular/core';
import { PixelMap } from '@pixel-maps/store';

@Pipe({
  name: 'pixelMapToString'
})
export class PixelMapToStringPipe implements PipeTransform {

  transform(pixelMap: PixelMap): string {
    return `
    Id: ${pixelMap.id}
    Name: ${pixelMap.name}
    `;
  }

}
