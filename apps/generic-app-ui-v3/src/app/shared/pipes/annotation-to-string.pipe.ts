import { Pipe, PipeTransform } from '@angular/core';
import { AnnotationCollectionEntity } from '@annotations/store';

@Pipe({
  name: 'annotationToString'
})
export class AnnotationToStringPipe implements PipeTransform {

  transform(annotation: AnnotationCollectionEntity): string {
    return `
    Id: ${annotation.id}
    ${annotation.name ? 'Name: ' + annotation.name : ''}
    ${annotation.description ? 'Description: ' + annotation.description : ''}
    ${annotation.collectionId ? 'Collection id: ' + annotation.collectionId : ''}
    `;
  }

}
