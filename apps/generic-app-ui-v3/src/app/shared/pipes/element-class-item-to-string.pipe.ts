import { Pipe, PipeTransform } from '@angular/core';
import { ElementClassMapping } from '@pixel-maps/store';
import { ClassEntity } from 'empaia-ui-commons';

@Pipe({
  name: 'elementClassItemToString'
})
export class ElementClassItemToStringPipe implements PipeTransform {

  transform(elementClassItem: ElementClassMapping, classEntity?: ClassEntity): string {
    return `
    Class Value: ${elementClassItem.class_value}
    Value: ${elementClassItem.number_value}
    Color: ${elementClassItem.color}
    ${classEntity ? 'Class Name: ' + classEntity.name : ''}
    ${classEntity?.description ? 'Description: ' + classEntity.description : ''}
    `;
  }

}
