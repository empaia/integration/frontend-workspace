import { ErrorSnackbarComponent } from './components/error-snackbar/error-snackbar.component';
import { MaterialModule } from '@material/material.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DictParserPipe } from './pipes/dict-parser.pipe';
import { LocalSafeUrlPipe } from './pipes/local-safe-url.pipe';
import { StringSlicerPipe } from './pipes/string-slicer.pipe';
import { CloseButtonComponent } from './components/close-button/close-button.component';
import { IconCheckboxComponent } from './components/icon-checkbox/icon-checkbox.component';
import { JobStatusComponent } from './components/job-status/job-status.component';
import { LoadingComponent } from './components/loading/loading.component';
import { MenuButtonComponent } from './components/menu-button/menu-button.component';
import { MenuLabelComponent } from './components/menu-label/menu-label.component';
import { MissingSelectionErrorComponent } from './components/missing-selection-error/missing-selection-error.component';
import { MenuItemLayoutContainerComponent } from './containers/menu-item-layout-container/menu-item-layout-container.component';
import { SubMenuLabelComponent } from './components/sub-menu-label/sub-menu-label.component';
import { DateCorrectionPipe } from './pipes/date-correction.pipe';
import { JobToStringPipe } from './pipes/job-to-string.pipe';
import { AnnotationToStringPipe } from './pipes/annotation-to-string.pipe';
import { PaginationComponent } from './components/pagination/pagination.component';
import { PrimitiveToStringPipe } from './pipes/primitive-to-string.pipe';
import { ActionDialogComponent } from './components/action-dialog/action-dialog.component';
import { CloseElementDirective } from './directives/close-element.directive';
import { PixelMapToStringPipe } from './pipes/pixel-map-to-string.pipe';
import { ElementClassItemToStringPipe } from './pipes/element-class-item-to-string.pipe';

const COMPONENTS = [
  CloseButtonComponent,
  IconCheckboxComponent,
  JobStatusComponent,
  LoadingComponent,
  MenuButtonComponent,
  MenuLabelComponent,
  MissingSelectionErrorComponent,
  MenuItemLayoutContainerComponent,
  ErrorSnackbarComponent,
  SubMenuLabelComponent,
  PaginationComponent,
  ActionDialogComponent,
];

const PIPES = [
  DictParserPipe,
  LocalSafeUrlPipe,
  StringSlicerPipe,
  DateCorrectionPipe,
  JobToStringPipe,
  AnnotationToStringPipe,
  PrimitiveToStringPipe,
  PixelMapToStringPipe,
  ElementClassItemToStringPipe,
];

const DIRECTIVES = [
  CloseElementDirective,
];

@NgModule({
  declarations: [
    COMPONENTS,
    PIPES,
    DIRECTIVES,
  ],
  imports: [
    CommonModule,
    MaterialModule,
  ],
  exports: [
    COMPONENTS,
    PIPES,
    DIRECTIVES,
  ]
})
export class SharedModule { }
