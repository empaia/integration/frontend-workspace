import { Injectable } from '@angular/core';
import { AppV3DataService } from 'empaia-api-lib';
import { Actions, concatLatestFrom, createEffect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { fetch } from '@ngrx/router-store/data-persistence';
import { filterNullish, retryOnAction } from '@shared/helper/rxjs-operators';
import { exhaustMap, map, retryWhen } from 'rxjs/operators';
import { CLASSES_ERROR_MAP, ClassesFeature } from '..';
import * as ClassesActions from './classes.actions';
import * as ScopeActions from '@scope/store/scope/scope.actions';
import * as TokenActions from '@token/store/token/token.actions';
import * as ScopeSelectors from '@scope/store/scope/scope.selectors';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ErrorSnackbarComponent } from '@shared/components/error-snackbar/error-snackbar.component';
import { of } from 'rxjs';
import { requestNewToken } from 'vendor-app-communication-interface';
import { ClassNameConversionServiceV3 } from 'empaia-ui-commons';

@Injectable()
export class ClassesEffects {
  prepareLoadClassNamespaces$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ScopeActions.loadExtendedScopeSuccess),
      concatLatestFrom(() =>
        this.store.select(ScopeSelectors.selectScopeId).pipe(filterNullish())
      ),
      map(([, scopeId]) => scopeId),
      map((scopeId) => ClassesActions.loadClassNamespaces({ scopeId }))
    );
  });

  loadClassNamespaces$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ClassesActions.loadClassNamespaces),
      fetch({
        run: (
          action: ReturnType<typeof ClassesActions.loadClassNamespaces>,
          _state: ClassesFeature.State
        ) => {
          return this.dataService
            .scopeIdClassNamespacesGet({
              scope_id: action.scopeId,
            })
            .pipe(
              // retry to load the selected slide after the token is expired
              retryWhen((errors) =>
                retryOnAction(
                  errors,
                  this.actions$,
                  TokenActions.setAccessToken,
                  requestNewToken
                )
              ),
              map((response) => this.classNameConverter.getClasses(response)),
              map((classes) =>
                ClassesActions.loadClassNamespacesSuccess({ classes })
              )
            );
        },
        onError: (
          _action: ReturnType<typeof ClassesActions.loadClassNamespaces>,
          error
        ) => {
          return ClassesActions.loadClassNamespacesFailure({ error });
        },
      })
    );
  });

  clearClassesEad$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ScopeActions.loadExtendedScope),
      map(() => ClassesActions.clearClassesEAD())
    );
  });

  showErrorMessage$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ClassesActions.loadClassNamespacesFailure),
      exhaustMap((action) => {
        this.snackBar.openFromComponent(ErrorSnackbarComponent, {
          data: { title: CLASSES_ERROR_MAP[action.type], error: action.error },
        });
        return of({ type: 'noop' });
      })
    );
  });

  constructor(
    private readonly actions$: Actions,
    private readonly dataService: AppV3DataService,
    private readonly classNameConverter: ClassNameConversionServiceV3,
    private readonly store: Store,
    private readonly snackBar: MatSnackBar
  ) {}
}
