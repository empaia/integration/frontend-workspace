import { CLASSES_LOAD_ERROR, ErrorMap } from '@menu/models/ui.models';
import { ClassEntity } from 'empaia-ui-commons';

export const CLASSES_ERROR_MAP: ErrorMap = {
  '[Classes] Load Class Namespaces Failure': CLASSES_LOAD_ERROR,
};

// we always wan't to use english for string comparison
export const STRING_COMPARATOR_LOCAL = 'en';

export function compareClassesAsc(a: ClassEntity, b: ClassEntity): number {
  return a.id.localeCompare(b.id, STRING_COMPARATOR_LOCAL);
}
