import { createSelector } from '@ngrx/store';
import {
  State as ModuleState,
  selectClassesFeatureState,
} from '../classes-feature.state';
import { classesAdapter, classSelectorAdapter } from './classes.reducer';

const {
  selectAll,
  selectEntities,
} = classesAdapter.getSelectors();

const classSelector = classSelectorAdapter.getSelectors();

export const selectClassesState = createSelector(
  selectClassesFeatureState,
  (state: ModuleState) => state.classes
);

export const selectAllClasses = createSelector(
  selectClassesState,
  selectAll,
);

export const selectClassEntities = createSelector(
  selectClassesState,
  selectEntities,
);

export const selectAllCheckedClasses = createSelector(
  selectClassesState,
  (state) => classSelector.selectAll(state.selectedClasses).filter(c => c.checked).map(c => c.id)
);

export const selectAllUncheckedClasses = createSelector(
  selectClassesState,
  (state) => classSelector.selectAll(state.selectedClasses).filter(c => !c.checked).map(c => c.id)
);

export const selectClassSelectionEntity = createSelector(
  selectClassesState,
  (state) => classSelectorAdapter.getSelectors().selectEntities(state.selectedClasses)
);
