import { CollectionsEffects } from '@collections/store/collections/collections.effects';
import { EffectsModule } from '@ngrx/effects';
import { COLLECTIONS_MODULE_FEATURE_KEY, reducers } from './store/collections-feature.state';
import { StoreModule } from '@ngrx/store';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    StoreModule.forFeature(
      COLLECTIONS_MODULE_FEATURE_KEY,
      reducers
    ),
    EffectsModule.forFeature([
      CollectionsEffects,
    ])
  ]
})
export class CollectionsModule { }
