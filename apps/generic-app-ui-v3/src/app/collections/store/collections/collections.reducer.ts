import { HttpErrorResponse } from '@angular/common/http';
import { EntityState, createEntityAdapter } from '@ngrx/entity';
import { createReducer, on } from '@ngrx/store';
import * as CollectionsActions from './collections.actions';
import { Collection } from './collections.models';


export const COLLECTIONS_FEATURE_STATE = 'collections';

export interface State extends EntityState<Collection> {
  loaded: boolean,
  error?: HttpErrorResponse | null;
}

export const collectionsAdapter = createEntityAdapter<Collection>();

export const initialState: State = collectionsAdapter.getInitialState({
  loaded: true,
  error: null,
});

export const reducer = createReducer(
  initialState,
  on(CollectionsActions.loadCollections, (state): State => ({
    ...state,
    loaded: false,
  })),
  on(CollectionsActions.loadCollectionsSuccess, (state, { collections }): State =>
    collectionsAdapter.setAll(collections, {
      ...state,
      loaded: true,
    })
  ),
  on(CollectionsActions.loadCollectionsFailure, (state, { error }): State => ({
    ...state,
    error,
  })),
  on(CollectionsActions.createCollection, (state): State => ({
    ...state,
    loaded: false,
  })),
  on(CollectionsActions.loadCollection, (state): State => ({
    ...state,
    loaded: false,
  })),
  on(CollectionsActions.loadCollectionSuccess, (state, { collection }): State =>
    collectionsAdapter.upsertOne(collection, {
      ...state,
      loaded: true,
    })
  ),
  on(CollectionsActions.loadCollectionFailure, (state, { error }): State => ({
    ...state,
    loaded: true,
    error,
  })),
  on(CollectionsActions.createCollectionSuccess, (state, { collection }): State =>
    collectionsAdapter.addOne(collection, {
      ...state,
      loaded: true,
    })
  ),
  on(CollectionsActions.createCollectionFailure, (state, { error }): State => ({
    ...state,
    loaded: true,
    error,
  })),
  on(CollectionsActions.addItemToCollection, (state): State => ({
    ...state,
    loaded: false,
  })),
  on(CollectionsActions.addItemsToCollection, (state): State => ({
    ...state,
    loaded: false,
  })),
  on(CollectionsActions.addItemOrItemsToCollectionFailure, (state, { error }): State => ({
    ...state,
    loaded: true,
    error,
  })),
  on(CollectionsActions.clearCollections, (state): State => ({
    ...state,
    ...initialState,
  })),
  on(CollectionsActions.setCollectionsLoadingState, (state, { loaded }): State => ({
    ...state,
    loaded,
  }))
);
