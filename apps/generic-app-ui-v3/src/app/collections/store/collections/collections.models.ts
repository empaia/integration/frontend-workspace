import {
  AppV3PostArrowCollection,
  AppV3PostBoolCollection,
  AppV3PostCirceCollection,
  AppV3PostClassCollection,
  AppV3PostFloatCollection,
  AppV3PostIdCollection,
  AppV3PostIntegerCollection,
  AppV3PostLineCollection,
  AppV3PostNestedCollection,
  AppV3PostPointCollection,
  AppV3PostPolygonCollection,
  AppV3PostRectangleCollection,
  AppV3PostSlideCollection,
  AppV3PostStringCollection,
  AppV3Collection,
  AppV3IdObject,
  AppV3PostIdObjects,
} from 'empaia-api-lib';
import {
  COLLECTIONS_LOAD_ERROR,
  COLLECTION_LOAD_ERROR,
  CREATE_COLLECTION_ERROR,
  DELETE_COLLECTION_ERROR,
  DELETE_COLLECTION_ITEM_ERROR,
  ErrorMap,
  UPDATE_COLLECTION_ERROR } from '@menu/models/ui.models';

export type PostArrowCollection = AppV3PostArrowCollection;
export type PostBoolCollection = AppV3PostBoolCollection;
export type PostCirceCollection = AppV3PostCirceCollection;
export type PostClassCollection = AppV3PostClassCollection;
export type PostFloatCollection = AppV3PostFloatCollection;
export type PostIdCollection = AppV3PostIdCollection;
export type PostIntegerCollection = AppV3PostIntegerCollection;
export type PostLineCollection = AppV3PostLineCollection;
export type PostNestedCollection = AppV3PostNestedCollection;
export type PostPointCollection = AppV3PostPointCollection;
export type PostPolygonCollection = AppV3PostPolygonCollection;
export type PostRectangleCollection = AppV3PostRectangleCollection;
export type PostSlideCollection = AppV3PostSlideCollection;
export type PostStringCollection = AppV3PostStringCollection;
export type PostIdObjects = AppV3PostIdObjects;

export type Collection = AppV3Collection;
export type IdObject = AppV3IdObject;

export type PostCollection = PostPointCollection
| PostLineCollection
| PostArrowCollection
| PostCirceCollection
| PostRectangleCollection
| PostPolygonCollection
| PostClassCollection
| PostIntegerCollection
| PostFloatCollection
| PostBoolCollection
| PostStringCollection
| PostSlideCollection
| PostIdCollection
| PostNestedCollection;

export const COLLECTIONS_ERROR_MAP: ErrorMap = {
  '[APP/Collections] Load Collections Failure': COLLECTIONS_LOAD_ERROR,
  '[APP/Collections] Load Collection Failure': COLLECTION_LOAD_ERROR,
  '[APP/Collections] Create Collection Failure': CREATE_COLLECTION_ERROR,
  '[APP/Collections] Add Item Or Items To Collection Failure': UPDATE_COLLECTION_ERROR,
  '[APP/Collections] Remove Item From Collection Failure': DELETE_COLLECTION_ITEM_ERROR,
  '[APP/Collections] Delete Collection Failure': DELETE_COLLECTION_ERROR,
};
