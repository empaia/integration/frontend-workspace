import { filterNullish, retryOnAction } from '@shared/helper/rxjs-operators';
import { Store } from '@ngrx/store';
import { fetch, pessimisticUpdate } from '@ngrx/router-store/data-persistence';
import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType, concatLatestFrom } from '@ngrx/effects';
import { CollectionsFeature } from '..';
import * as AnnotationsViewerActions from '@annotations/store/annotations-viewer/annotations-viewer.actions';
import * as CollectionsActions from './collections.actions';
import * as JobsActions from '@jobs/store/jobs/jobs.actions';
import * as SlidesActions from '@slides/store/slides/slides.actions';
import * as TokenActions from '@token/store/token/token.actions';
import * as ScopeSelectors from '@scope/store/scope/scope.selectors';
import { exhaustMap, filter, map, mergeMap, retryWhen } from 'rxjs/operators';
import { AppV3DataService } from 'empaia-api-lib';
import { requestNewToken } from 'vendor-app-communication-interface';
import { JobKeyType, JobsSelectors } from '@jobs/store';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ErrorSnackbarComponent } from '@shared/components/error-snackbar/error-snackbar.component';
import { COLLECTIONS_ERROR_MAP } from './collections.models';
import { of } from 'rxjs';
import { EadSelectors } from '@ead/store';
import { EadService, EmpaiaAppDescriptionV3 } from 'empaia-ui-commons';

@Injectable()
export class CollectionsEffects {
  // clear collections on slide selection
  clearAnnotationsOnSlideSelection$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(SlidesActions.selectSlide, SlidesActions.clearSlides),
      map(() => CollectionsActions.clearCollections())
    );
  });

  // Load every time the job list was loaded
  startLoadingCollections$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(JobsActions.setUniqueJobSelection),
      concatLatestFrom(() => [
        this.store.select(JobsSelectors.selectAllCheckedJobs),
        this.store.select(EadSelectors.selectActiveRoiMode),
        this.store.select(ScopeSelectors.selectScopeId).pipe(filterNullish()),
        this.store.select(ScopeSelectors.selectExtendedScope).pipe(
          filterNullish(),
          map((extended) => extended.ead as EmpaiaAppDescriptionV3)
        ),
        this.store.select(EadSelectors.selectActiveMode),
      ]),
      filter(
        ([, jobs, activeRoiMode]) =>
          !!jobs?.length && activeRoiMode === 'multiple'
      ),
      mergeMap(([, jobs, _activeRoiMode, scopeId, ead, mode]) => {
        const collectionKeys = this.eadService
          .getV3TypeInputKeys('collection', ead, mode)
          .map((input) => input.inputKey);
        const collectionIds = jobs
          .map((job) =>
            collectionKeys
              .map((key) => job.inputs[key])
              .filter((collectionId) => !!collectionId)
          )
          .flat();

        return collectionIds?.length
          ? collectionIds.map((collectionId) =>
            CollectionsActions.loadCollection({
              scopeId,
              collectionId,
            })
          )
          : [{ type: 'noop' }];
      })
    );
  });

  loadCollection$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(CollectionsActions.loadCollection),
      fetch({
        id: (
          action: ReturnType<typeof CollectionsActions.loadCollection>,
          _state: CollectionsFeature.State
        ) => {
          return action.collectionId + ' ' + action.type;
        },
        run: (
          action: ReturnType<typeof CollectionsActions.loadCollection>,
          _state: CollectionsFeature.State
        ) => {
          return this.dataService
            .scopeIdCollectionsCollectionIdGet({
              scope_id: action.scopeId,
              collection_id: action.collectionId,
              shallow: true,
              with_leaf_ids: true,
            })
            .pipe(
              retryWhen((errors) =>
                retryOnAction(
                  errors,
                  this.actions$,
                  TokenActions.setAccessToken,
                  requestNewToken
                )
              ),
              map((collection) =>
                CollectionsActions.loadCollectionSuccess({ collection })
              )
            );
        },
        onError: (
          _action: ReturnType<typeof CollectionsActions.loadCollection>,
          error
        ) => {
          return CollectionsActions.loadCollectionFailure({ error });
        },
      })
    );
  });

  createCollection$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(CollectionsActions.createCollection),
      pessimisticUpdate({
        run: (
          action: ReturnType<typeof CollectionsActions.createCollection>,
          _state: CollectionsFeature.State
        ) => {
          return this.dataService
            .scopeIdCollectionsPost({
              scope_id: action.scopeId,
              body: action.postCollection,
            })
            .pipe(
              retryWhen((errors) =>
                retryOnAction(
                  errors,
                  this.actions$,
                  TokenActions.setAccessToken,
                  requestNewToken
                )
              ),
              map((collection) =>
                CollectionsActions.createCollectionSuccess({
                  ...action,
                  collection,
                })
              )
            );
        },
        onError: (
          _action: ReturnType<typeof CollectionsActions.createCollection>,
          error
        ) => {
          return CollectionsActions.createCollectionFailure({ error });
        },
      })
    );
  });

  addItemToCollection$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(CollectionsActions.addItemToCollection),
      pessimisticUpdate({
        run: (
          action: ReturnType<typeof CollectionsActions.addItemToCollection>,
          _state: CollectionsFeature.State
        ) => {
          return this.dataService
            .scopeIdCollectionsCollectionIdItemsPost({
              scope_id: action.scopeId,
              collection_id: action.collectionId,
              body: action.postIdObject,
            })
            .pipe(
              retryWhen((errors) =>
                retryOnAction(
                  errors,
                  this.actions$,
                  TokenActions.setAccessToken,
                  requestNewToken
                )
              ),
              map(() => CollectionsActions.loadCollection({ ...action }))
            );
        },
        onError: (
          _action: ReturnType<typeof CollectionsActions.addItemToCollection>,
          error
        ) => {
          return CollectionsActions.addItemOrItemsToCollectionFailure({
            error,
          });
        },
      })
    );
  });

  addItemsToCollection$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(CollectionsActions.addItemsToCollection),
      pessimisticUpdate({
        run: (
          action: ReturnType<typeof CollectionsActions.addItemsToCollection>,
          _state: CollectionsFeature.State
        ) => {
          return this.dataService
            .scopeIdCollectionsCollectionIdItemsPost({
              scope_id: action.scopeId,
              collection_id: action.collectionId,
              body: action.postIdObjects,
            })
            .pipe(
              retryWhen((errors) =>
                retryOnAction(
                  errors,
                  this.actions$,
                  TokenActions.setAccessToken,
                  requestNewToken
                )
              ),
              map(() => CollectionsActions.loadCollection({ ...action }))
            );
        },
        onError: (
          _action: ReturnType<typeof CollectionsActions.addItemsToCollection>,
          error
        ) => {
          return CollectionsActions.addItemOrItemsToCollectionFailure({
            error,
          });
        },
      })
    );
  });

  removeItemFromCollection$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(CollectionsActions.removeItemFromCollection),
      pessimisticUpdate({
        run: (
          action: ReturnType<
            typeof CollectionsActions.removeItemFromCollection
          >,
          _state: CollectionsFeature.State
        ) => {
          return this.dataService
            .scopeIdCollectionsCollectionIdItemsItemIdDelete({
              scope_id: action.scopeId,
              collection_id: action.collectionId,
              item_id: action.itemId,
            })
            .pipe(
              retryWhen((errors) =>
                retryOnAction(
                  errors,
                  this.actions$,
                  TokenActions.setAccessToken,
                  requestNewToken
                )
              ),
              map(() => CollectionsActions.loadCollection({ ...action }))
            );
        },
        onError: (
          _action: ReturnType<
            typeof CollectionsActions.removeItemFromCollection
          >,
          error
        ) => {
          return CollectionsActions.removeItemFromCollectionFailure({ error });
        },
      })
    );
  });

  removeItemsFromCollection$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(CollectionsActions.removeItemsFromCollection),
      mergeMap((action) =>
        action.itemIds.map((itemId) =>
          CollectionsActions.removeItemFromCollection({
            ...action,
            itemId,
          })
        )
      )
    );
  });

  deleteCollection$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(CollectionsActions.deleteCollection),
      pessimisticUpdate({
        run: (
          action: ReturnType<typeof CollectionsActions.deleteCollection>,
          _state: CollectionsFeature.State
        ) => {
          return this.dataService
            .scopeIdCollectionsCollectionIdDelete({
              scope_id: action.scopeId,
              collection_id: action.collectionId,
            })
            .pipe(
              retryWhen((errors) =>
                retryOnAction(
                  errors,
                  this.actions$,
                  TokenActions.setAccessToken,
                  requestNewToken
                )
              ),
              map((response) => response.id),
              map((collectionId) =>
                CollectionsActions.deleteCollectionSuccess({ collectionId })
              )
            );
        },
        onError: (
          _action: ReturnType<typeof CollectionsActions.deleteCollection>,
          error
        ) => {
          return CollectionsActions.deleteCollectionFailure({ error });
        },
      })
    );
  });

  // add collection to job on creation success
  addCollectionToJob$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(CollectionsActions.createCollectionSuccess),
      map((action) => {
        switch (action.keyType) {
          case JobKeyType.INPUT:
            return JobsActions.setIdToJobInput({
              ...action,
              inputKey: action.key,
              inputId: action.collection.id as string,
            });
          case JobKeyType.OUTPUT:
            return JobsActions.setIdToJobOutput({
              ...action,
              outputKey: action.key,
              outputId: action.collection.id as string,
            });
        }
      })
    );
  });

  // set loaded state of collections to false in
  // multi roi mode when an annotation was drawn
  setLoadingStateAfterDrawing$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(AnnotationsViewerActions.createAnnotation),
      concatLatestFrom(() => [
        this.store.select(EadSelectors.selectActiveRoiMode),
      ]),
      filter(([, activeRoiMode]) => activeRoiMode === 'multiple'),
      map(() =>
        CollectionsActions.setCollectionsLoadingState({ loaded: false })
      )
    );
  });

  showErrorMessage$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(
        CollectionsActions.addItemOrItemsToCollectionFailure,
        CollectionsActions.createCollectionFailure,
        CollectionsActions.deleteCollectionFailure,
        CollectionsActions.loadCollectionsFailure,
        CollectionsActions.loadCollectionFailure,
        CollectionsActions.removeItemFromCollectionFailure
      ),
      exhaustMap((action) => {
        this.snackBar.openFromComponent(ErrorSnackbarComponent, {
          data: {
            title: COLLECTIONS_ERROR_MAP[action.type],
            error: action.error,
          },
        });
        return of({ type: 'noop' });
      })
    );
  });

  constructor(
    private readonly actions$: Actions,
    private readonly dataService: AppV3DataService,
    private readonly store: Store,
    private readonly snackBar: MatSnackBar,
    private readonly eadService: EadService
  ) {}
}
