import { HttpErrorResponse } from '@angular/common/http';
import { JobKeyType } from '@jobs/store';
import { createAction, props } from '@ngrx/store';
import { Collection, IdObject, PostCollection, PostIdObjects } from './collections.models';

export const loadCollections = createAction(
  '[APP/Collections] Load Collections',
  props<{
    scopeId: string,
    jobIds: string[],
  }>()
);

export const loadCollectionsSuccess = createAction(
  '[APP/Collections] Load Collections Success',
  props<{ collections: Collection[] }>()
);

export const loadCollectionsFailure = createAction(
  '[APP/Collections] Load Collections Failure',
  props<{ error: HttpErrorResponse }>()
);

export const loadCollection = createAction(
  '[APP/Collections] Load Collection',
  props<{
    scopeId: string,
    collectionId: string
  }>()
);

export const loadCollectionSuccess = createAction(
  '[APP/Collections] Load Collection Success',
  props<{ collection: Collection }>()
);

export const loadCollectionFailure = createAction(
  '[APP/Collections] Load Collection Failure',
  props<{ error: HttpErrorResponse }>()
);

export const createCollection = createAction(
  '[APP/Collections] Create Collection',
  props<{
    scopeId: string,
    jobId: string,
    key: string,
    keyType: JobKeyType,
    postCollection: PostCollection,
  }>(),
);

export const createCollectionSuccess = createAction(
  '[APP/Collections] Create Collection Success',
  props<{
    collection: Collection,
    scopeId: string,
    jobId: string,
    key: string,
    keyType: JobKeyType,
  }>(),
);

export const createCollectionFailure = createAction(
  '[APP/Collections] Create Collection Failure',
  props<{ error: HttpErrorResponse }>(),
);

export const addItemToCollection = createAction(
  '[APP/Collections] Add Item To Collection',
  props<{
    scopeId: string,
    collectionId: string,
    postIdObject: IdObject,
  }>(),
);

export const addItemsToCollection = createAction(
  '[APP/Collections] Add Items To Collection',
  props<{
    scopeId: string,
    collectionId: string,
    postIdObjects: PostIdObjects,
  }>(),
);

export const addItemOrItemsToCollectionFailure = createAction(
  '[APP/Collections] Add Item Or Items To Collection Failure',
  props<{ error: HttpErrorResponse }>(),
);

export const removeItemFromCollection = createAction(
  '[APP/Collections] Remove Item From Collection',
  props<{
    scopeId: string,
    collectionId: string,
    itemId: string,
  }>(),
);

export const removeItemFromCollectionFailure = createAction(
  '[APP/Collections] Remove Item From Collection Failure',
  props<{ error: HttpErrorResponse}>(),
);

export const removeItemsFromCollection = createAction(
  '[APP/Collections] Remove Items From Collection',
  props<{
    scopeId: string,
    collectionId: string,
    itemIds: string[],
  }>(),
);

export const deleteCollection = createAction(
  '[APP/Collections] Delete Collection',
  props<{ scopeId: string, collectionId: string }>(),
);

export const deleteCollectionSuccess = createAction(
  '[APP/Collections] Delete Collection Success',
  props<{ collectionId: string }>(),
);

export const deleteCollectionFailure = createAction(
  '[APP/Collections] Delete Collection Failure',
  props<{ error: HttpErrorResponse }>(),
);

export const clearCollections = createAction(
  '[APP/Collections] Clear Collections',
);

export const setCollectionsLoadingState = createAction(
  '[APP/Collections] Set Collections Loading State',
  props<{ loaded: boolean }>(),
);
