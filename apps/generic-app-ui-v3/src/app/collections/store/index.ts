import * as CollectionsActions from './collections/collections.actions';
import * as CollectionsFeature from './collections/collections.reducer';
import * as CollectionsSelectors from './collections/collections.selectors';
export * from './collections/collections.effects';

export { CollectionsActions, CollectionsFeature, CollectionsSelectors };
