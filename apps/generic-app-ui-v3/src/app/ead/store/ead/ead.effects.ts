import { Injectable } from '@angular/core';
import { Actions, concatLatestFrom, createEffect, ofType } from '@ngrx/effects';
import { EadActions } from './ead.actions';
import * as ScopeActions from '@scope/store/scope/scope.actions';
import * as ScopeSelectors from '@scope/store/scope/scope.selectors';
import { exhaustMap, filter, map, of } from 'rxjs';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ErrorSnackbarComponent } from '@shared/components/error-snackbar/error-snackbar.component';
import { EAD_ERROR_MAP, IncompatibleApp, getErrorMessagesOfIncompatibleApp } from './ead.models';
import { Store } from '@ngrx/store';
import { filterNullish } from '@shared/helper/rxjs-operators';
import { EadService, EmpaiaAppDescriptionV3, annotationTypes } from 'empaia-ui-commons';



@Injectable()
export class EadEffects {

  setEadModes$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ScopeActions.loadExtendedScopeSuccess),
      map(action => action.extendedScope.ead as EmpaiaAppDescriptionV3),
      map(ead => this.eadService.getV3Modes(ead)),
      map(modes => EadActions.setV3Modes({ modes }))
    );
  });

  selectActiveMode$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(EadActions.setV3Modes),
      map(action => action.modes),
      map(modes => modes.includes('preprocessing') ? 'preprocessing' : 'standalone'),
      map(active => EadActions.setActiveV3Mode({ active }))
    );
  });

  selectActiveRoiMode$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(EadActions.setActiveV3Mode),
      map(action => action.active),
      concatLatestFrom(() =>
        this.store.select(ScopeSelectors.selectExtendedScope).pipe(
          filterNullish(),
          map(extended => extended.ead as EmpaiaAppDescriptionV3)
        )
      ),
      map(([activeMode, ead]) => this.eadService.getV3InputMode(ead, activeMode)),
      map(activeRoiMode => EadActions.setActiveRoiMode({ activeRoiMode }))
    );
  });

  // check ead if app inputs are compatible with rectangle
  // also check if app has multi OR single mode (if not the app is not compatible)
  // check if app has nested input collections (it's not compatible)
  checkEadInputCompatibility$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(EadActions.setActiveV3Mode),
      map(action => action.active),
      // check only if the app has compatible input annotations
      // if the mode isn't preprocessing
      // because the user can not create preprocessing annotations
      // only postprocessing and standalone
      filter(active => active !== 'preprocessing'),
      concatLatestFrom(() =>
        this.store.select(ScopeSelectors.selectExtendedScope).pipe(
          filterNullish(),
          map(extended => extended.ead as EmpaiaAppDescriptionV3)
        )
      ),
      map(([mode, ead]) => {
        // has the app single annotation and annotations in collections input types mixed
        const singleAndMultiInput = this.eadService.hasV3MultiAndSingleInputs(ead, mode, [...annotationTypes]);
        // has the app compatible single input types
        const singleInput = this.eadService.hasAnyV3CompatibleInputType(ead, mode, [...annotationTypes]);
        // has the app compatible multi input types (annotations in collections)
        const multiInput = this.eadService.hasAnyV3CompatibleMultiInputType(ead, mode, [...annotationTypes]);
        // has the app nested collections for inputs
        const nestedInputCollections = this.eadService.hasV3NestedInputCollections(ead, mode, [...annotationTypes]);

        const inputKeyTypes = this.eadService.getV3TypesInputKeysWithTypes([...annotationTypes], ead, mode);
        // has the app multiple input annotations of the same type (single roi mode)
        const sameSingleInput = annotationTypes.filter(type =>
          inputKeyTypes
            .filter(input => input.inCollection === 0)
            .map(input => input.type)
            .filter(inputType => inputType === type).length > 1
        ).length > 0;
        // has the app multiple collections of the same input type (multi roi mode)
        const sameMultiInput = annotationTypes.filter(type =>
          inputKeyTypes
            .filter(input => input.inCollection === 1)
            .map(input => input.type)
            .filter(inputType => inputType === type).length > 1
        ).length > 0;

        return {
          singleAndMultiInput,
          noCompatibleInputs: !(singleInput || multiInput),
          nestedInputCollections,
          sameInputTypes: sameSingleInput || sameMultiInput
        } as IncompatibleApp;
      }),
      filter(incompatible => Object.values(incompatible).some(value => value)),
      map(incompatible => EadActions.checkEadInputCompatibilityFailure({ error: getErrorMessagesOfIncompatibleApp(incompatible) }))
    );
  });

  setEadAnnotationRenderingHint$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ScopeActions.loadExtendedScopeSuccess),
      map(action => action.extendedScope.ead as EmpaiaAppDescriptionV3),
      map(ead => this.eadService.getV3AnnotationRenderingHints(ead)),
      map(eadAnnotationRenderingHint => EadActions.setEadAnnotationRenderingHint({ eadAnnotationRenderingHint }))
    );
  });

  showErrorMessage$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(
        EadActions.checkEadInputCompatibilityFailure,
      ),
      exhaustMap(action => {
        this.snackBar.openFromComponent(ErrorSnackbarComponent, {
          data: { title: EAD_ERROR_MAP[action.type], error: action.error }
        });
        return of({ type: 'noop' });
      })
    );
  });

  constructor(
    private readonly actions$: Actions,
    private readonly eadService: EadService,
    private readonly snackBar: MatSnackBar,
    private readonly store: Store,
  ) {}
}
