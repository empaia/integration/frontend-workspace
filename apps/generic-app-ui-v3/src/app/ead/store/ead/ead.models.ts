import { EAD_COMPATIBILITY_ERROR, ErrorMap } from '@menu/models/ui.models';
import { annotationTypes } from 'empaia-ui-commons';

export const EAD_ERROR_MAP: ErrorMap = {
  '[EmpaiaAppDescription] Check Ead Input Compatibility Failure': EAD_COMPATIBILITY_ERROR,
};

export const INCOMPATIBLE_APP = { message: 'App is not compatible with rectangle, polygon or circle input in standalone mode!' };

export interface IncompatibleApp {
  singleAndMultiInput: boolean;
  noCompatibleInputs: boolean;
  nestedInputCollections: boolean;
  sameInputTypes: boolean;
}

export const INCOMPATIBLE_APP_ERROR_MAP: ErrorMap = {
  singleAndMultiInput: 'App has single annotation inputs as well as annotation collection inputs!',
  noCompatibleInputs: `App has none of the following compatible annotation types: ${annotationTypes.join(', ')}!`,
  nestedInputCollections: 'App has nested input collections!',
  sameInputTypes: 'App has multiple inputs of the same type!',
};

export function getErrorMessagesOfIncompatibleApp(incompatible: IncompatibleApp): object {
  return {
    message: Object
      .entries(incompatible)
      .filter(([_key, value]) => value)
      .map(([key]) => INCOMPATIBLE_APP_ERROR_MAP[key])
      .filter(error => !!error)
      .join(' ')
  };
}
