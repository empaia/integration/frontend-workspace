import { createReducer, on } from '@ngrx/store';
import { EadActions } from './ead.actions';
import { EadRoiInputType, EmpaiaAppDescriptionV3Mode, RenderingHint } from 'empaia-ui-commons';


export const EAD_FEATURE_KEY = 'ead';

export interface State {
  modes: EmpaiaAppDescriptionV3Mode[];
  active: EmpaiaAppDescriptionV3Mode;
  activeRoiMode: EadRoiInputType;
  eadAnnotationRenderingHint?: RenderingHint[] | undefined;
}

export const initialState: State = {
  modes: [],
  active: 'standalone',
  activeRoiMode: 'single',
  eadAnnotationRenderingHint: undefined,
};

export const reducer = createReducer(
  initialState,
  on(EadActions.setV3Modes, (state, { modes }): State => ({
    ...state,
    modes,
  })),
  on(EadActions.setActiveV3Mode, (state, { active }): State => ({
    ...state,
    active,
  })),
  on(EadActions.setActiveRoiMode, (state, { activeRoiMode }): State => ({
    ...state,
    activeRoiMode,
  })),
  on(EadActions.setEadAnnotationRenderingHint, (state, { eadAnnotationRenderingHint }): State => ({
    ...state,
    eadAnnotationRenderingHint,
  })),
);
