import { createActionGroup, props } from '@ngrx/store';
import { EadRoiInputType, EmpaiaAppDescriptionV3Mode, RenderingHint } from 'empaia-ui-commons';

export const EadActions = createActionGroup({
  source: 'EmpaiaAppDescription',
  events: {
    'Set V3 Modes': props<{ modes: EmpaiaAppDescriptionV3Mode[] }>(),
    'Set Active V3 Mode': props<{ active: EmpaiaAppDescriptionV3Mode }>(),
    'Set Active Roi Mode': props<{ activeRoiMode: EadRoiInputType }>(),
    'Set Ead Annotation Rendering Hint': props<{
      eadAnnotationRenderingHint?: RenderingHint[] | undefined
    }>(),
    'Check Ead Input Compatibility Failure': props<{ error: object }>(),
  }
});
