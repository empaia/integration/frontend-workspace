import { Injectable } from '@angular/core';
import { AnnotationConversionService } from '@annotations/services/annotation-conversion.service';
import { ViewportConversionService } from '@annotations/services/viewport-conversion.service';
import { AppV3DataService } from 'empaia-api-lib';
import { Actions, concatLatestFrom, createEffect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { fetch, pessimisticUpdate } from '@ngrx/router-store/data-persistence';
import {
  debounceTime,
  distinctUntilChanged,
  exhaustMap,
  filter,
  map,
} from 'rxjs/operators';
import {
  AnnotationsViewerFeature,
  ANNOTATION_QUERY_LIMIT,
  VIEWER_ANNOTATIONS_ERROR_MAP,
} from '..';
import * as AnnotationsViewerActions from './annotations-viewer.actions';
import * as AnnotationsUiActions from '@annotations/store/annotations-ui/annotations-ui.actions';
import * as CollectionsActions from '@collections/store/collections/collections.actions';
import * as JobsActions from '@jobs/store/jobs/jobs.actions';
import * as ScopeActions from '@scope/store/scope/scope.actions';
import * as SlidesActions from '@slides/store/slides/slides.actions';
import * as TokenActions from '@token/store/token/token.actions';
import * as AnnotationsUiSelectors from '@annotations/store/annotations-ui/annotations-ui.selectors';
import * as CollectionsSelectors from '@collections/store/collections/collections.selectors';
import * as JobsSelectors from '@jobs/store/jobs/jobs.selectors';
import * as ScopeSelectors from '@scope/store/scope/scope.selectors';
import * as SlidesSelectors from '@slides/store/slides/slides.selectors';
import * as ClassesActions from '@classes/store/classes/classes.actions';
import * as ClassesSelectors from '@classes/store/classes/classes.selectors';
import { filterNullish, retryOnAction } from '@shared/helper/rxjs-operators';
import { retryWhen } from 'rxjs/operators';
import { requestNewToken } from 'vendor-app-communication-interface';
import { AnnotationEntity } from 'slide-viewer';
import { AnnotationApiOutput } from '@annotations/services/annotation-types';
import { EadSelectors } from '@ead/store';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ErrorSnackbarComponent } from '@shared/components/error-snackbar/error-snackbar.component';
import { of } from 'rxjs';
import { JobMode, compareJobStatus } from '@jobs/store';
import {
  FORBIDDEN_ERROR_STATUS_CODE,
  VALIDATION_ERROR_STATUS_CODE,
} from '@menu/models/ui.models';

@Injectable()
export class AnnotationsViewerEffects {
  // clear annotations on slide selection
  clearAnnotationsOnSlideSelection$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(SlidesActions.selectSlide),
      map((action) => action.id),
      // we don't want to clear the annotations when a
      // user clicks on the same slide
      distinctUntilChanged(),
      map(() => AnnotationsViewerActions.clearAnnotations())
    );
  });

  clearAnnotationsOnSlideClearance$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(SlidesActions.clearSlides),
      map(() => AnnotationsViewerActions.clearAnnotations())
    );
  });

  // load annotation ids when the job list was loaded
  startLoadingAnnotationIds$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(JobsActions.loadJobsSuccess),
      map((action) => action.jobs),
      distinctUntilChanged(compareJobStatus),
      concatLatestFrom(() => [
        this.store.select(ScopeSelectors.selectScopeId).pipe(filterNullish()),
        this.store
          .select(SlidesSelectors.selectSelectedSlideId)
          .pipe(filterNullish()),
        this.store
          .select(AnnotationsUiSelectors.selectCurrentView)
          .pipe(filterNullish()),
        this.store
          .select(JobsSelectors.selectAllCheckedJobIds)
          .pipe(filterNullish()),
        this.store.select(ClassesSelectors.selectAllCheckedClasses),
      ]),
      map(([, scopeId, slideId, currentView, jobIds, classValues]) =>
        classValues.length > 0 && jobIds.length > 0
          ? AnnotationsViewerActions.loadAnnotationIds({
            scopeId,
            slideId,
            jobIds,
            currentView,
            classValues,
          })
          : AnnotationsViewerActions.clearAnnotations()
      )
    );
  });

  // load new annotation ids when the viewport has changed
  viewportChanged$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(AnnotationsUiActions.setViewportReady),
      map((action) => action.currentView),
      concatLatestFrom(() => [
        this.store.select(ScopeSelectors.selectScopeId).pipe(filterNullish()),
        this.store
          .select(SlidesSelectors.selectSelectedSlideId)
          .pipe(filterNullish()),
        this.store.select(JobsSelectors.selectAllCheckedJobIds),
        this.store.select(ClassesSelectors.selectAllCheckedClasses),
      ]),
      map(([currentView, scopeId, slideId, jobIds, classValues]) =>
        classValues.length > 0 && jobIds.length > 0
          ? AnnotationsViewerActions.loadAnnotationIds({
            scopeId,
            slideId,
            jobIds,
            currentView,
            classValues,
          })
          : AnnotationsViewerActions.clearAnnotations()
      )
    );
  });

  // remove focused annotation after the viewport changed
  removeFocusAnnotation$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(AnnotationsUiActions.setViewport),
      map(() => AnnotationsViewerActions.zoomToAnnotation({ focus: undefined }))
    );
  });

  // reload annotations when job/classes gets checked or unchecked
  reloadAnnotationIdsOnJobCheck$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(
        JobsActions.setJobSelection,
        JobsActions.setJobsSelections,
        JobsActions.setUniqueJobSelection,
        JobsActions.showAllJobs,
        ClassesActions.setClassSelection,
        ClassesActions.setClassesSelections
      ),
      debounceTime(1000),
      concatLatestFrom(() => [
        this.store.select(ScopeSelectors.selectScopeId).pipe(filterNullish()),
        this.store
          .select(SlidesSelectors.selectSelectedSlideId)
          .pipe(filterNullish()),
        this.store
          .select(AnnotationsUiSelectors.selectCurrentView)
          .pipe(filterNullish()),
        this.store.select(JobsSelectors.selectAllCheckedJobIds),
        this.store.select(ClassesSelectors.selectAllCheckedClasses),
      ]),
      map(([, scopeId, slideId, currentView, jobIds, selectedClasses]) =>
        selectedClasses.length > 0 && jobIds.length > 0
          ? AnnotationsViewerActions.loadAnnotationIds({
            scopeId,
            slideId,
            currentView,
            jobIds,
            classValues: selectedClasses,
          })
          : AnnotationsViewerActions.clearAnnotations()
      )
    );
  });

  loadAnnotationIds$ = createEffect(() => {
    return (
      this,
      this.actions$.pipe(
        ofType(AnnotationsViewerActions.loadAnnotationIds),
        fetch({
          id: (
            action: ReturnType<
              typeof AnnotationsViewerActions.loadAnnotationIds
            >,
            _state: AnnotationsViewerFeature.State
          ) => {
            return action.type;
          },
          run: (
            action: ReturnType<
              typeof AnnotationsViewerActions.loadAnnotationIds
            >,
            _state: AnnotationsViewerFeature.State
          ) => {
            return this.dataService
              .scopeIdAnnotationsQueryViewerPut({
                scope_id: action.scopeId,
                body: {
                  references: [action.slideId],
                  jobs: action.jobIds.length ? action.jobIds : undefined,
                  viewport: action.currentView.viewport,
                  npp_viewing: this.viewportConverter.calculateNppViewRange(
                    action.currentView.nppCurrent,
                    action.currentView.nppBase
                  ),
                  class_values: action.classValues,
                  types: action.annotationTypes?.map(
                    this.annotationConverter.convertAnnotationTypeToApi
                  ),
                },
              })
              .pipe(
                retryWhen((errors) =>
                  retryOnAction(
                    errors,
                    this.actions$,
                    TokenActions.setAccessToken,
                    requestNewToken
                  )
                ),
                concatLatestFrom(() =>
                  this.store.select(
                    JobsSelectors.selectAllJobCollectionsRoisIds
                  )
                ),
                map(([results, annotationIds]) =>
                  AnnotationsViewerActions.loadAnnotationIdsSuccess({
                    annotationIds: action.classValues?.includes('roi')
                      ? Array.from(new Set<string>(results.annotations.concat(annotationIds)))
                      : results.annotations,
                    centroids: results.low_npp_centroids,
                  })
                )
              );
          },
          onError: (
            _action: ReturnType<
              typeof AnnotationsViewerActions.loadAnnotationIds
            >,
            error
          ) => {
            return AnnotationsViewerActions.loadAnnotationIdsFailure({ error });
          },
        })
      )
    );
  });

  prepareLoadAnnotations$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(AnnotationsViewerActions.loadAnnotations),
      map((action) => action.annotationIds),
      concatLatestFrom(() => [
        this.store.select(ScopeSelectors.selectScopeId).pipe(filterNullish()),
        this.store.select(JobsSelectors.selectAllCheckedJobIds),
      ]),
      filter(([annotationIds, _scopeId]) => !!annotationIds?.length),
      map(([annotationIds, scopeId, jobIds]) =>
        AnnotationsViewerActions.loadAnnotationsReady({
          scopeId,
          annotationIds,
          withClasses: true,
          skip: 0,
          jobIds,
        })
      )
    );
  });

  loadAnnotations$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(AnnotationsViewerActions.loadAnnotationsReady),
      fetch({
        id: (
          action: ReturnType<
            typeof AnnotationsViewerActions.loadAnnotationsReady
          >,
          _state: AnnotationsViewerFeature.State
        ) => {
          return action.type;
        },
        run: (
          action: ReturnType<
            typeof AnnotationsViewerActions.loadAnnotationsReady
          >,
          _state: AnnotationsViewerFeature.State
        ) => {
          return this.dataService
            .scopeIdAnnotationsQueryPut({
              scope_id: action.scopeId,
              with_classes: action.withClasses,
              skip: action.skip,
              limit: ANNOTATION_QUERY_LIMIT,
              body: {
                annotations: action.annotationIds,
                jobs: action.jobIds.length ? action.jobIds : undefined,
              },
            })
            .pipe(
              retryWhen((errors) =>
                retryOnAction(
                  errors,
                  this.actions$,
                  TokenActions.setAccessToken,
                  requestNewToken
                )
              ),
              map((result) =>
                result.items.map(
                  (a) =>
                    this.annotationConverter.fromApiType(a) as AnnotationEntity
                )
              ),
              map((annotations) =>
                AnnotationsViewerActions.loadAnnotationsSuccess({ annotations })
              )
            );
        },
        onError: (
          _action: ReturnType<
            typeof AnnotationsViewerActions.loadAnnotationsReady
          >,
          error
        ) => {
          return AnnotationsViewerActions.loadAnnotationsFailure({ error });
        },
      })
    );
  });

  prepareCreateAnnotation$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(AnnotationsViewerActions.createAnnotation),
      map((action) => action.annotation),
      concatLatestFrom(() => [
        this.store.select(ScopeSelectors.selectScopeId).pipe(filterNullish()),
        this.store
          .select(SlidesSelectors.selectSelectedSlideId)
          .pipe(filterNullish()),
      ]),
      map(([annotation, scopeId, slideId]) =>
        AnnotationsViewerActions.createAnnotationReady({
          annotation,
          scopeId,
          slideId,
        })
      )
    );
  });

  createAnnotation$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(AnnotationsViewerActions.createAnnotationReady),
      pessimisticUpdate({
        run: (
          action: ReturnType<
            typeof AnnotationsViewerActions.createAnnotationReady
          >,
          _state: AnnotationsViewerFeature.State
        ) => {
          return this.dataService
            .scopeIdAnnotationsPost({
              scope_id: action.scopeId,
              is_roi: true,
              body: this.annotationConverter.toApiPostType(
                action.annotation,
                action.slideId,
                action.scopeId
              ),
            })
            .pipe(
              retryWhen((errors) =>
                retryOnAction(
                  errors,
                  this.actions$,
                  TokenActions.setAccessToken,
                  requestNewToken
                )
              ),
              map(
                (result) =>
                  this.annotationConverter.fromApiType(
                    result as AnnotationApiOutput
                  ) as AnnotationEntity
              ),
              map((annotation) =>
                AnnotationsViewerActions.createAnnotationSuccess({ annotation })
              )
            );
        },
        onError: (
          _action: ReturnType<
            typeof AnnotationsViewerActions.createAnnotationReady
          >,
          error
        ) => {
          return AnnotationsViewerActions.createAnnotationFailure({ error });
        },
      })
    );
  });

  // depending on roi mode:
  // create a job for single roi mode and add the annotation
  // for multi roi annotations add the annotation to the
  // correct collection (depends on the annotation type)
  addAnnotationToJobOrCollection$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(AnnotationsViewerActions.createAnnotationSuccess),
      map((action) => action.annotation),
      concatLatestFrom(() => [
        this.store.select(EadSelectors.selectActiveRoiMode),
        this.store.select(ScopeSelectors.selectScopeId).pipe(filterNullish()),
        this.store.select(EadSelectors.selectActiveMode),
        this.store.select(CollectionsSelectors.selectCollectionEntities),
        this.store
          .select(JobsSelectors.selectAllCheckedJobs)
          .pipe(map((jobs) => jobs[0])),
      ]),
      map(
        ([annotation, activeRoiMode, scopeId, activeMode, collections, job]) =>
          activeRoiMode === 'multiple'
            ? CollectionsActions.addItemToCollection({
              scopeId,
              collectionId: Object.values(job.inputs)
                .map((id) => collections[id])
                .filter(
                  (c) =>
                    !!c &&
                      c.item_type.toLowerCase() ===
                        annotation.annotationType.toLowerCase()
                )
                .map((c) => c?.id)[0] as string,
              postIdObject: {
                id: annotation.id,
              },
            })
            : JobsActions.createJobReady({
              scopeId,
              mode: activeMode.toUpperCase() as JobMode,
              containerized: activeMode !== 'postprocessing',
              annotation,
            })
      )
    );
  });

  deleteAnnotation$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(AnnotationsViewerActions.deleteAnnotation),
      pessimisticUpdate({
        run: (
          action: ReturnType<typeof AnnotationsViewerActions.deleteAnnotation>,
          _state: AnnotationsViewerFeature.State
        ) => {
          return this.dataService
            .scopeIdAnnotationsAnnotationIdDelete({
              scope_id: action.scopeId,
              annotation_id: action.annotationId,
            })
            .pipe(
              retryWhen((errors) =>
                retryOnAction(
                  errors,
                  this.actions$,
                  TokenActions.setAccessToken,
                  requestNewToken
                )
              ),
              map((result) => result.id),
              map((annotationId) =>
                AnnotationsViewerActions.deleteAnnotationSuccess({
                  annotationId,
                })
              )
            );
        },
        onError: (
          _action: ReturnType<typeof AnnotationsViewerActions.deleteAnnotation>,
          error
        ) => {
          return AnnotationsViewerActions.deleteAnnotationFailure({ error });
        },
      })
    );
  });

  // add focused annotation to slide-viewer cache
  addAnnotationOnFocus$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(AnnotationsViewerActions.zoomToAnnotation),
      map((action) => action.focus),
      filterNullish(),
      map((annotation) =>
        AnnotationsViewerActions.addAnnotation({ annotation })
      )
    );
  });

  // clear centroids on hide or result selection when no job is checked
  clearCentroids$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(
        JobsActions.setJobSelection,
        JobsActions.setJobsSelections,
        JobsActions.setUniqueJobSelection,
        JobsActions.hideAllJobs
      ),
      concatLatestFrom(() =>
        this.store.select(JobsSelectors.selectAllCheckedJobIds)
      ),
      filter(([, jobIds]) => !jobIds.length),
      map(() => AnnotationsViewerActions.clearCentroids())
    );
  });

  // stop loading state when loading extended scope
  stopLoading$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ScopeActions.loadExtendedScope),
      map(() => AnnotationsViewerActions.stopLoading())
    );
  });

  showErrorMessage$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(
        AnnotationsViewerActions.createAnnotationFailure,
        AnnotationsViewerActions.deleteAnnotationFailure,
        AnnotationsViewerActions.loadAnnotationIdsFailure,
        AnnotationsViewerActions.loadAnnotationsFailure
      ),
      filter(
        (action) =>
          !(
            action.error.status === VALIDATION_ERROR_STATUS_CODE ||
            action.error.status === FORBIDDEN_ERROR_STATUS_CODE
          )
      ),
      exhaustMap((action) => {
        this.snackBar.openFromComponent(ErrorSnackbarComponent, {
          data: {
            title: VIEWER_ANNOTATIONS_ERROR_MAP[action.type],
            error: action.error,
          },
        });
        return of({ type: 'noop' });
      })
    );
  });

  constructor(
    private readonly actions$: Actions,
    private readonly store: Store,
    private readonly dataService: AppV3DataService,
    private readonly annotationConverter: AnnotationConversionService,
    private readonly viewportConverter: ViewportConversionService,
    private readonly snackBar: MatSnackBar
  ) {}
}
