import { fetch } from '@ngrx/router-store/data-persistence';
import { Injectable } from '@angular/core';
import { ViewportConversionService } from '@annotations/services/viewport-conversion.service';
import { Actions, createEffect, ofType, concatLatestFrom } from '@ngrx/effects';
import { exhaustMap, filter, map, mergeMap, retryWhen } from 'rxjs/operators';
import * as AnnotationsUiActions from './annotations-ui.actions';
import * as AnnotationsUiFeature from './annotations-ui.reducer';
import * as JobsActions from '@jobs/store/jobs/jobs.actions';
import * as JobsSelectors from '@jobs/store/jobs/jobs.selectors';
import * as ResultsActions from '@analysis/store/results/results.actions';
import * as ScopeActions from '@scope/store/scope/scope.actions';
import * as ScopeSelectors from '@scope/store/scope/scope.selectors';
import * as SlidesActions from '@slides/store/slides/slides.actions';
import * as TokenActions from '@token/store/token/token.actions';
import { AnnotationEntity, DrawType, InteractionType } from 'slide-viewer';
import { AppV3DataService, AppV3ExaminationState } from 'empaia-api-lib';
import { AnnotationConversionService } from '@annotations/services/annotation-conversion.service';
import { Store } from '@ngrx/store';
import { filterNullish, retryOnAction } from '@shared/helper/rxjs-operators';
import { EadActions } from '@ead/store/ead/ead.actions';
import { EadSelectors } from '@ead/store';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ErrorSnackbarComponent } from '@shared/components/error-snackbar/error-snackbar.component';
import { ANNOTATION_UI_ERROR_MAP } from './annotations-ui.models';
import { of } from 'rxjs';
import { requestNewToken } from 'vendor-app-communication-interface';
import { JobStatus } from '@jobs/store';
import { EadService, EmpaiaAppDescriptionV3 } from 'empaia-ui-commons';

@Injectable()
export class AnnotationsUiEffects {
  convertViewport$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(AnnotationsUiActions.setViewport),
      map((action) => action.currentView),
      map((currentView) => {
        return {
          viewport: this.viewportConverter.convertFromApi(currentView.extent),
          nppCurrent: currentView.nppCurrent,
          nppBase: currentView.nppBase,
        };
      }),
      map((currentView) =>
        AnnotationsUiActions.setViewportReady({ currentView })
      )
    );
  });

  // set allowed interaction types to move and rectangle
  setAllowedInteractionTypes$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(SlidesActions.loadSlides),
      map(() =>
        AnnotationsUiActions.setAllowedInteractionTypes({
          allowedDrawTypes: [InteractionType.Move],
        })
      )
    );
  });

  // set active interaction to move on examination close
  setInteractionToMoveOnClose$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ScopeActions.loadExtendedScopeSuccess),
      map((action) => action.extendedScope.examination_state),
      filter(
        (examinationState) => examinationState === AppV3ExaminationState.Closed
      ),
      map(() =>
        AnnotationsUiActions.setInteractionType({
          interactionType: InteractionType.Move,
        })
      )
    );
  });

  // set interaction type move and compatible types when examination
  // is open
  setDrawTypesOnOpenExamination$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(EadActions.setActiveV3Mode),
      map((action) => action.active),
      concatLatestFrom(() =>
        this.store
          .select(ScopeSelectors.selectExtendedScope)
          .pipe(filterNullish())
      ),
      map(([mode, extendedScope]) => {
        if (extendedScope.examination_state === AppV3ExaminationState.Open) {
          const types = this.eadService.getV3AnnotationInputTypes(
            extendedScope.ead as EmpaiaAppDescriptionV3,
            mode
          );
          return AnnotationsUiActions.setAllowedInteractionTypes({
            allowedDrawTypes: [
              InteractionType.Move,
              // if polygon is an allowed input add alternative polygon as draw mode
              ...(types.includes('polygon')
                ? (types as DrawType[]).concat(DrawType.PolygonAlternative)
                : (types as DrawType[])),
            ],
          });
        } else {
          return AnnotationsUiActions.setAllowedInteractionTypes({
            allowedDrawTypes: [InteractionType.Move],
          });
        }
      })
    );
  });

  // deactivate annotation draw tools if app is not compatible
  disableDrawToolsOnIncompatibility$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(EadActions.checkEadInputCompatibilityFailure),
      map(() =>
        AnnotationsUiActions.setAllowedInteractionTypes({
          allowedDrawTypes: [InteractionType.Move],
        })
      )
    );
  });

  // deactivate annotation draw tools if multi roi mode was set
  // draw tools will later be activated when a user clicks on
  // a job
  disableDrawToolsOnSetMultiRoiMode$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(EadActions.setActiveRoiMode),
      map((action) => action.activeRoiMode),
      filter((activeRoiMode) => activeRoiMode === 'multiple'),
      map(() =>
        AnnotationsUiActions.setAllowedInteractionTypes({
          allowedDrawTypes: [InteractionType.Move],
        })
      )
    );
  });

  // deactivate draw tools if user starts/runs or
  // deletes a job in multi roi mode
  disableDrawToolsOnRunJob$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(JobsActions.runJob, JobsActions.deleteJob),
      concatLatestFrom(() =>
        this.store.select(EadSelectors.selectActiveRoiMode)
      ),
      filter(([, activeRoiMode]) => activeRoiMode === 'multiple'),
      mergeMap(() => [
        AnnotationsUiActions.setAllowedInteractionTypes({
          allowedDrawTypes: [InteractionType.Move],
        }),
        AnnotationsUiActions.setInteractionType({
          interactionType: InteractionType.Move,
        }),
      ])
    );
  });

  // activate draw tools if the job in unique job selection
  // is in status assembly
  activateDrawToolsOnAssemblyJob$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(JobsActions.setUniqueJobSelection),
      map((action) => action.jobSelection),
      filter((jobSelection) => jobSelection.checked),
      concatLatestFrom(() => [
        this.store.select(JobsSelectors.selectJobEntities),
        this.store.select(ScopeSelectors.selectExtendedScope).pipe(
          filterNullish(),
          map((extended) => extended.ead as EmpaiaAppDescriptionV3)
        ),
        this.store.select(EadSelectors.selectActiveMode),
      ]),
      map(([jobSelection, jobs, ead, mode]) => ({
        job: jobs[jobSelection.id],
        types: this.eadService.getV3AnnotationInputTypes(ead, mode),
      })),
      filter((jobTypes) => !!jobTypes.job),
      map((jobType) =>
        AnnotationsUiActions.setAllowedInteractionTypes({
          allowedDrawTypes:
            jobType.job?.status === JobStatus.Assembly
              ? [
                InteractionType.Move,
                // if polygon is an allowed input add alternative polygon as draw mode
                ...(jobType.types.includes('polygon')
                  ? (jobType.types as DrawType[]).concat(
                    DrawType.PolygonAlternative
                  )
                  : (jobType.types as DrawType[])),
              ]
              : [InteractionType.Move],
        })
      )
    );
  });

  // load annotation when the set reference is of
  // type annotation
  loadOnReferenceSet$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ResultsActions.setPrimitivesReference),
      map((action) => action.reference),
      concatLatestFrom(() =>
        this.store.select(ScopeSelectors.selectScopeId).pipe(filterNullish())
      ),
      map(([reference, scopeId]) =>
        reference?.type === 'annotation'
          ? AnnotationsUiActions.loadReferenceAnnotation({
            scopeId,
            annotationId: reference.id,
          })
          : AnnotationsUiActions.clearReferenceAnnotation()
      )
    );
  });

  loadReferenceAnnotation$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(AnnotationsUiActions.loadReferenceAnnotation),
      fetch({
        run: (
          action: ReturnType<
            typeof AnnotationsUiActions.loadReferenceAnnotation
          >,
          _state: AnnotationsUiFeature.State
        ) => {
          return this.dataService
            .scopeIdAnnotationsAnnotationIdGet({
              scope_id: action.scopeId,
              annotation_id: action.annotationId,
            })
            .pipe(
              // retry to load the selected slide after the token is expired
              retryWhen((errors) =>
                retryOnAction(
                  errors,
                  this.actions$,
                  TokenActions.setAccessToken,
                  requestNewToken
                )
              ),
              map(
                (annotation) =>
                  this.annotationConverter.fromApiType(
                    annotation
                  ) as AnnotationEntity
              ),
              map((referenceAnnotation) =>
                AnnotationsUiActions.loadReferenceAnnotationSuccess({
                  referenceAnnotation,
                })
              )
            );
        },
        onError: (
          _action: ReturnType<
            typeof AnnotationsUiActions.loadReferenceAnnotation
          >,
          error
        ) => {
          return AnnotationsUiActions.loadReferenceAnnotationFailure({ error });
        },
      })
    );
  });

  showErrorMessage$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(AnnotationsUiActions.loadReferenceAnnotationFailure),
      exhaustMap((action) => {
        this.snackBar.openFromComponent(ErrorSnackbarComponent, {
          data: {
            title: ANNOTATION_UI_ERROR_MAP[action.type],
            error: action.error,
          },
        });
        return of({ type: 'noop' });
      })
    );
  });

  constructor(
    private readonly actions$: Actions,
    private readonly viewportConverter: ViewportConversionService,
    private readonly eadService: EadService,
    private readonly dataService: AppV3DataService,
    private readonly annotationConverter: AnnotationConversionService,
    private readonly store: Store,
    private readonly snackBar: MatSnackBar
  ) {}
}
