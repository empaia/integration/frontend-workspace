import { AppV3Viewport } from 'empaia-api-lib';
import { ErrorMap, REFERENCE_ANNOTATION_LOAD_ERROR } from '@menu/models/ui.models';

export type Viewport = AppV3Viewport;

export interface CurrentView {
  viewport: Viewport;
  nppCurrent: number;
  nppBase: number;
}

export const ANNOTATION_UI_ERROR_MAP: ErrorMap = {
  '[APP/Annotation UI] Load Reference Annotation Failure': REFERENCE_ANNOTATION_LOAD_ERROR,
};
