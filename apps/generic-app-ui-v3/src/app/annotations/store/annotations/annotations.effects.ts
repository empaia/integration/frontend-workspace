import { Injectable } from '@angular/core';
import { AnnotationConversionService } from '@annotations/services/annotation-conversion.service';
import { AppV3DataService } from 'empaia-api-lib';
import { Actions, concatLatestFrom, createEffect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { fetch } from '@ngrx/router-store/data-persistence';
import { filterNullish, retryOnAction } from '@shared/helper/rxjs-operators';
import {
  distinctUntilChanged,
  exhaustMap,
  filter,
  map,
  mergeMap,
  retryWhen,
} from 'rxjs/operators';
import { AnnotationEntity } from 'slide-viewer';
import {
  AnnotationsFeature,
  INPUT_ANNOTATION_ERROR_MAP,
  INPUT_ANNOTATION_LIMIT,
} from '..';
import * as AnnotationsActions from './annotations.actions';
import * as AnnotationsViewerActions from '@annotations/store/annotations-viewer/annotations-viewer.actions';
import * as CollectionsActions from '@collections/store/collections/collections.actions';
import * as CollectionsSelectors from '@collections/store/collections/collections.selectors';
import * as JobsActions from '@jobs/store/jobs/jobs.actions';
import * as JobsSelectors from '@jobs/store/jobs/jobs.selectors';
import * as SlidesActions from '@slides/store/slides/slides.actions';
import * as TokenActions from '@token/store/token/token.actions';
import * as ScopeSelectors from '@scope/store/scope/scope.selectors';
import { requestNewToken } from 'vendor-app-communication-interface';
import { EadSelectors } from '@ead/store';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ErrorSnackbarComponent } from '@shared/components/error-snackbar/error-snackbar.component';
import { of } from 'rxjs';
import { compareJobStatus } from '@jobs/store';
import {
  EadService,
  EmpaiaAppDescriptionV3,
  annotationTypes,
} from 'empaia-ui-commons';

@Injectable()
export class AnnotationsEffects {
  // clear annotations on slide selection
  clearAnnotationsOnSlideSelection$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(SlidesActions.selectSlide),
      map((action) => action.id),
      // we don't want to clear the annotations when a
      // user clicks on the same slide
      distinctUntilChanged(),
      map(() => AnnotationsActions.clearAnnotations())
    );
  });

  clearAnnotationsOnSlideClearance$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(SlidesActions.clearSlides),
      map(() => AnnotationsActions.clearAnnotations())
    );
  });

  // load annotation when collections were fetched
  // only in active roi mode multiple
  loadAnnotationsAfterCollections$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(
        CollectionsActions.loadCollectionsSuccess,
        CollectionsActions.loadCollectionSuccess
      ),
      concatLatestFrom(() => [
        this.store.select(EadSelectors.selectActiveRoiMode),
        this.store.select(JobsSelectors.selectAllJobs),
        this.store.select(CollectionsSelectors.selectCollectionEntities),
        this.store.select(ScopeSelectors.selectScopeId).pipe(filterNullish()),
      ]),
      filter(
        ([, activeMode, jobs]) => !!jobs?.length && activeMode === 'multiple'
      ),
      map(([, _activeMode, jobs, collectionEntities, scopeId]) => {
        const annotationIds = jobs.map(job =>
          Object.values(job.inputs)
            .map(id => collectionEntities[id])
            .filter(c => !!c)
            .map(c => c?.item_ids)
            .filter(ids => !!ids)
        ).flat(2);

        return annotationIds.length
          ? AnnotationsActions.loadInputAnnotations({
            scopeId,
            withClasses: true,
            skip: 0,
            limit: INPUT_ANNOTATION_LIMIT,
            annotationIds,
          })
          : { type: 'noop' };
      })
    );
  });

  // load annotations when job list was fetched
  // and active roi mode is single
  loadAnnotationsAfterJobs$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(JobsActions.loadJobsSuccess),
      map((action) => action.jobs),
      distinctUntilChanged(compareJobStatus),
      concatLatestFrom(() => [
        this.store.select(ScopeSelectors.selectScopeId).pipe(filterNullish()),
        this.store.select(EadSelectors.selectActiveRoiMode),
        this.store.select(ScopeSelectors.selectExtendedScope).pipe(
          filterNullish(),
          map((extended) => extended.ead as EmpaiaAppDescriptionV3)
        ),
        this.store.select(EadSelectors.selectActiveMode),
      ]),
      filter(
        ([jobs, _scopeId, activeRoiMode]) =>
          !!jobs?.length && activeRoiMode === 'single'
      ),
      map(([jobs, scopeId, _activeRoiMode, ead, mode]) => {
        const annotationKeys = this.eadService
          .getV3TypesInputKeys([...annotationTypes], ead, mode)
          .map((input) => input.inputKey);
        const annotationIds = jobs
          .map((job) =>
            annotationKeys
              .map((key) => job.inputs[key])
              .filter((annotationId) => !!annotationId)
          )
          .flat();

        return annotationIds?.length
          ? AnnotationsActions.loadInputAnnotations({
            scopeId,
            withClasses: true,
            skip: 0,
            limit: INPUT_ANNOTATION_LIMIT,
            annotationIds,
          })
          : { type: 'noop' };
      })
    );
  });

  // add annotation after creation
  addAnnotation$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(AnnotationsViewerActions.createAnnotationSuccess),
      map((action) => action.annotation),
      map((annotation) => AnnotationsActions.addAnnotation({ annotation }))
    );
  });

  loadAnnotations$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(AnnotationsActions.loadInputAnnotations),
      fetch({
        run: (
          action: ReturnType<typeof AnnotationsActions.loadInputAnnotations>,
          _state: AnnotationsFeature.State
        ) => {
          return this.dataService
            .scopeIdAnnotationsQueryPut({
              scope_id: action.scopeId,
              with_classes: action.withClasses,
              skip: action.skip,
              limit: action.limit,
              body: {
                annotations: action.annotationIds,
              },
            })
            .pipe(
              retryWhen((errors) =>
                retryOnAction(
                  errors,
                  this.actions$,
                  TokenActions.setAccessToken,
                  requestNewToken
                )
              ),
              map((response) => ({
                annotations: response.items.map(
                  (a) =>
                    this.annotationConversion.fromApiType(a) as AnnotationEntity
                ),
                itemCount: response.item_count,
              })),
              mergeMap((response) => [
                AnnotationsActions.loadInputAnnotationsSuccess({
                  annotations: response.annotations,
                }),
                response.itemCount > action.skip + action.limit
                  ? AnnotationsActions.loadInputAnnotations({
                    ...action,
                    skip: action.skip + action.limit,
                  })
                  : { type: 'noop' },
              ])
            );
        },
        onError: (
          _action: ReturnType<typeof AnnotationsActions.loadInputAnnotations>,
          error
        ) => {
          return AnnotationsActions.loadInputAnnotationsFailure({ error });
        },
      })
    );
  });

  showErrorMessage$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(AnnotationsActions.loadInputAnnotationsFailure),
      exhaustMap((action) => {
        this.snackBar.openFromComponent(ErrorSnackbarComponent, {
          data: {
            title: INPUT_ANNOTATION_ERROR_MAP[action.type],
            error: action.error,
          },
        });
        return of({ type: 'noop' });
      })
    );
  });

  constructor(
    private readonly actions$: Actions,
    private readonly store: Store,
    private readonly dataService: AppV3DataService,
    private readonly annotationConversion: AnnotationConversionService,
    private readonly snackBar: MatSnackBar,
    private readonly eadService: EadService
  ) {}
}
