import { createSelector } from '@ngrx/store';
import {
  State as ModuleState,
  selectAnnotationsFeatureState,
} from '../annotations-feature.state';
import { annotationAdapter  } from './annotations.reducer';

const {
  selectAll,
  selectEntities,
  selectIds,
} = annotationAdapter.getSelectors();

export const selectAnnotationsState = createSelector(
  selectAnnotationsFeatureState,
  (state: ModuleState) => state.annotations
);

export const selectAllAnnotations = createSelector(
  selectAnnotationsState,
  selectAll,
);

export const selectAnnotationEntities = createSelector(
  selectAnnotationsState,
  selectEntities,
);

export const selectAllAnnotationIds = createSelector(
  selectAnnotationsState,
  (state) => selectIds(state) as string[]
);

export const selectAnnotationsLoaded = createSelector(
  selectAnnotationsState,
  (state) => state.loaded
);

export const selectAnnotationsError = createSelector(
  selectAnnotationsState,
  (state) => state.error
);

export const selectSelectedAnnotationId = createSelector(
  selectAnnotationsState,
  (state) => state.selected
);
