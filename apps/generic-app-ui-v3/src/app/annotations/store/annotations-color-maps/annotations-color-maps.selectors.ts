import { createSelector } from '@ngrx/store';
import { selectAnnotationsFeatureState } from '../annotations-feature.state';
import { ANNOTATIONS_COLOR_MAPS_FEATURE_KEY, annotationRenderingHintAdapter } from './annotations-color-maps.reducer';
import { convertAnnotationRenderingHintsToClassColorMapping } from 'empaia-ui-commons';
import { AnnotationColorMapsSettings } from './annotations-color-maps.models';

const {
  selectAll,
  selectEntities,
  selectIds,
} = annotationRenderingHintAdapter.getSelectors();

export const selectAnnotationsColorMapsState = createSelector(
  selectAnnotationsFeatureState,
  (state) => state[ANNOTATIONS_COLOR_MAPS_FEATURE_KEY]
);

export const selectAllAnnotationColorMaps = createSelector(
  selectAnnotationsColorMapsState,
  selectAll,
);

export const selectAnnotationColorMapEntities = createSelector(
  selectAnnotationsColorMapsState,
  selectEntities,
);

export const selectAllAnnotationColorMapIds = createSelector(
  selectAnnotationsColorMapsState,
  selectIds,
);

export const selectSelectedAnnotationColorMapId = createSelector(
  selectAnnotationsColorMapsState,
  (state) => state.selected
);

export const selectSelectedAnnotationColorMap = createSelector(
  selectSelectedAnnotationColorMapId,
  selectAnnotationColorMapEntities,
  (selected, entities) => selected ? entities[selected] : undefined
);

export const selectSelectedAnnotationClassColorMapping = createSelector(
  selectSelectedAnnotationColorMap,
  (selected) => selected ? convertAnnotationRenderingHintsToClassColorMapping(selected.renderingHints) : undefined
);

export const selectInversionState = createSelector(
  selectAnnotationsColorMapsState,
  (state) => state.invert
);

export const selectInversionDeactivationState = createSelector(
  selectAnnotationsColorMapsState,
  (state) => state.inversionDisabled
);

export const selectAnnotationColorMapsSettings = createSelector(
  selectAnnotationsColorMapsState,
  (state) => ({
    selected: state.selected,
    invert: state.invert,
    inversionDisabled: state.inversionDisabled
  } as AnnotationColorMapsSettings)
);
