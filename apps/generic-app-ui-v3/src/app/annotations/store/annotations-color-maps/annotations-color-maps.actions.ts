import { createActionGroup, emptyProps, props } from '@ngrx/store';
import { AnnotationColorMap } from 'empaia-ui-commons';

export const AnnotationsColorMapsActions = createActionGroup({
  source: 'Annotations Color Maps Actions',
  events: {
    'Set Annotation Color Maps': props<{
      annotationRenderingHints: AnnotationColorMap[],
    }>(),
    'Update Annotation Color Maps': props<{
      annotationRenderingHints: AnnotationColorMap[],
    }>(),
    'Select Annotation Color Map': props<{
      selected?: string | undefined,
    }>(),
    'Set Color Map Inversion': props<{
      invert: boolean
    }>(),
    'Set Color Map Inversion State': props<{
      inversionDisabled: boolean
    }>(),
    'Set Annotation Color Map Settings': props<{
      selected?: string,
      invert: boolean,
      inversionDisabled: boolean
    }>(),
    'Clear Annotation Color Maps': emptyProps()
  }
});
