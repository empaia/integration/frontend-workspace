import { Injectable } from '@angular/core';
import { Actions, concatLatestFrom, createEffect, ofType } from '@ngrx/effects';
import { AnnotationsColorMapsActions } from './annotations-color-maps.actions';
import * as ClassesActions from '@classes/store/classes/classes.actions';
import * as ClassesSelectors from '@classes/store/classes/classes.selectors';
import * as ScopeSelectors from '@scope/store/scope/scope.selectors';
import { map, mergeMap } from 'rxjs/operators';
import { Store } from '@ngrx/store';
import { filterNullish } from '@shared/helper/rxjs-operators';
import { compareFullClassesAsc } from './annotations-color-maps.models';
import { AnnotationsColorMapsService, EmpaiaAppDescriptionV3, RECOMMENDED_ANNOTATION_COLOR_MAP_ID } from 'empaia-ui-commons';
import { UserStorageActions } from '@storage/store';



@Injectable()
export class AnnotationsColorMapsEffects {

  clearAnnotationRenderingHints$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ClassesActions.clearClassesEAD),
      map(() => AnnotationsColorMapsActions.clearAnnotationColorMaps())
    );
  });

  // set annotation rendering hints after class namespace
  // was successfully loaded
  setAnnotationRenderingHints$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ClassesActions.loadClassNamespacesSuccess),
      map(action => action.classes.map(entity => entity.id).sort(compareFullClassesAsc)),
      concatLatestFrom(() => [
        this.store.select(ScopeSelectors.selectExtendedScope).pipe(
          filterNullish(),
          map(extended => extended.ead as EmpaiaAppDescriptionV3)
        )
      ]),
      map(([classes, ead]) => this.acmService.getAllV3AnnotationRenderingHints(ead, classes)),
      map(annotationRenderingHints => AnnotationsColorMapsActions.setAnnotationColorMaps({
        annotationRenderingHints
      }))
    );
  });

  // update annotation rendering hints when inversion state changes
  updateAnnotationRenderingHints$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(AnnotationsColorMapsActions.setColorMapInversion),
      map(action => action.invert),
      concatLatestFrom(() => [
        this.store.select(ScopeSelectors.selectExtendedScope).pipe(
          filterNullish(),
          map(extended => extended.ead as EmpaiaAppDescriptionV3)
        ),
        this.store.select(ClassesSelectors.selectAllClasses).pipe(
          map(classes => classes.map(entity => entity.id).sort(compareFullClassesAsc))
        )
      ]),
      map(([invert, ead, classes]) => this.acmService.getAllV3AnnotationRenderingHints(ead, classes, invert)),
      map(annotationRenderingHints => AnnotationsColorMapsActions.updateAnnotationColorMaps({
        annotationRenderingHints
      }))
    );
  });

  // select first rendering as default after setting
  // the rendering hints
  selectFirstRenderingHint$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(AnnotationsColorMapsActions.setAnnotationColorMaps),
      map(action => action.annotationRenderingHints[0].id),
      map(selected => AnnotationsColorMapsActions.selectAnnotationColorMap({ selected }))
    );
  });

  // set inversion to false if color map selection changes
  resetColorMapInversion$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(AnnotationsColorMapsActions.selectAnnotationColorMap),
      map(() => AnnotationsColorMapsActions.setColorMapInversion({
        invert: false
      }))
    );
  });

  // deactivate inversion if color map id matches recommended
  // color map id constance
  deactivateInversion$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(AnnotationsColorMapsActions.selectAnnotationColorMap),
      map(action => action.selected),
      filterNullish(),
      map(selected => AnnotationsColorMapsActions.setColorMapInversionState({
        inversionDisabled: selected === RECOMMENDED_ANNOTATION_COLOR_MAP_ID
      }))
    );
  });

  // set annotation color maps settings after user storage
  // loaded successfully
  setSettings$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(UserStorageActions.loadUserStorageSuccess),
      map(action => action.userStorage.rendering.annotations),
      mergeMap(annotationsSettings => [
        AnnotationsColorMapsActions.setAnnotationColorMapSettings({
          ...annotationsSettings
        }),
        // we have to call inversion action to update/recrate
        // the color map
        AnnotationsColorMapsActions.setColorMapInversion({
          invert: annotationsSettings.invert
        })
      ])
    );
  });

  constructor(
    private readonly actions$: Actions,
    private readonly store: Store,
    private readonly acmService: AnnotationsColorMapsService,
  ) {}
}
