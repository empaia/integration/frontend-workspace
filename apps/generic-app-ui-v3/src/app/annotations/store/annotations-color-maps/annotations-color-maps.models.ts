export interface AnnotationColorMapsSettings {
  selected?: string;
  invert: boolean;
  inversionDisabled: boolean;
}

// we always wan't to use english for string comparison
export const STRING_COMPARATOR_LOCAL = 'en';

export function compareFullClassesAsc(a: string, b: string): number {
  return a.localeCompare(b, STRING_COMPARATOR_LOCAL);
}
