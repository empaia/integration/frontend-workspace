/**
 * Interface of the menu data
 */
export type MenuType =
 | 'slides'
 | 'analysis';

export enum MenuState {
  HIDDEN,
  VISIBLE
}

export const MenuIcons: Record<MenuType, string> = {
  slides: 'filter',
  analysis: 'settings',
};

export interface MenuEntity {
  readonly id: MenuType;
  selected: boolean;
  contentState: MenuState;
  labelState: MenuState;
  readonly alignment: 'start' | 'end'; // start = left, end = right
  readonly order: number, // sort buttons low to high (1 = top most button)
  readonly icon: string,
}

// Initial Menu state definition
export const MENU: Record<MenuType, MenuEntity> = {
  slides: {
    id: 'slides',
    contentState: MenuState.HIDDEN,
    labelState: MenuState.VISIBLE,
    selected: false,
    alignment: 'end',
    order: 1,
    icon: MenuIcons.slides,
  },
  analysis: {
    id: 'analysis',
    contentState: MenuState.HIDDEN,
    labelState: MenuState.VISIBLE,
    selected: false,
    alignment: 'end',
    order: 2,
    icon: MenuIcons.analysis,
  }
};

export const MINIMIZED_SIZE = 0;
export const MIDI_SIZE = 401; // px
export const MAX_SIZE = 100; // vw
export const MIN_SIZE = 295; // px
export const MENU_BUTTON_WIDTH = 64; // px
