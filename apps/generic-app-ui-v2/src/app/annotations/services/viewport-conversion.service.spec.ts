import { TestBed } from '@angular/core/testing';

import { ViewportConversionService } from './viewport-conversion.service';

describe('ViewportConversionService', () => {
  let service: ViewportConversionService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ViewportConversionService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
