import { HttpErrorResponse } from '@angular/common/http';
import { createAction, props } from '@ngrx/store';
import { CurrentView } from '@annotations/store/annotations-ui/annotations-ui.models';
import { Annotation, AnnotationEntity, AnnotationType } from 'slide-viewer';

export const loadAnnotationIds = createAction(
  '[APP/Annotations-Viewer] Load Annotation Ids',
  props<{
    scopeId: string,
    slideId: string,
    currentView: CurrentView,
    jobIds: string[],
    classValues?: string[],
    annotationTypes?: AnnotationType[],
  }>()
);

export const loadAnnotationIdsSuccess = createAction(
  '[APP/Annotations-Viewer] Load Annotation Ids Success',
  props<{
    annotationIds: string[],
    centroids: number[][],
  }>()
);

export const loadAnnotationIdsFailure = createAction(
  '[APP/Annotations-Viewer] Load Annotation Ids Failure',
  props<{ error: HttpErrorResponse }>()
);

export const loadAnnotations = createAction(
  '[APP/Annotations-Viewer] Load Annotations',
  props<{ annotationIds: string[] }>()
);

export const loadAnnotationsReady = createAction(
  '[APP/Annotations-Viewer] Load Annotations Ready',
  props<{
    withClasses: boolean,
    scopeId: string,
    annotationIds: string[],
    jobIds?: string[],
    skip: number,
  }>()
);

export const loadAnnotationsSuccess = createAction(
  '[APP/Annotations-Viewer] Load Annotations Success',
  props<{ annotations: AnnotationEntity[] }>()
);

export const loadAnnotationsFailure = createAction(
  '[APP/Annotations-Viewer] Load Annotations Failure',
  props<{ error: HttpErrorResponse }>()
);

export const clearAnnotations = createAction(
  '[APP/Annotations-Viewer] Clear Annotations'
);

export const createAnnotation = createAction(
  '[APP/Annotations-Viewer] Create Annotation',
  props<{ annotation: Annotation }>()
);

export const createAnnotationReady = createAction(
  '[APP/Annotations-Viewer] Create Annotation Ready',
  props<{
    annotation: Annotation,
    scopeId: string,
    slideId: string,
    isRoi: boolean | undefined,
  }>()
);

export const createAnnotationSuccess = createAction(
  '[APP/Annotations-Viewer] Create Annotation Success',
  props<{ annotation: AnnotationEntity }>()
);

export const createAnnotationFailure = createAction(
  '[APP/Annotations-Viewer] Create Annotation Failure',
  props<{ error: HttpErrorResponse }>()
);

export const setAnnotationTypeFilter = createAction(
  '[APP/Annotations-Viewer] Set Annotation Type Filter',
  props<{ annotationTypes: AnnotationType[] | undefined }>()
);

export const deleteAnnotation = createAction(
  '[APP/Annotations-Viewer] Delete Annotation',
  props<{ scopeId: string, annotationId: string }>()
);

export const deleteAnnotationSuccess = createAction(
  '[APP/Annotations-Viewer] Delete Annotation Success',
  props<{ annotationId: string }>()
);

export const deleteAnnotationFailure = createAction(
  '[APP/Annotations-Viewer] Delete Annotation Failure',
  props<{ error: HttpErrorResponse }>()
);

export const zoomToAnnotation = createAction(
  '[APP/Annotations-Viewer] Zoom To Annotation',
  props<{ focus?: AnnotationEntity | undefined }>()
);

export const addAnnotation = createAction(
  '[APP/Annotations-Viewer] Add Annotation',
  props<{ annotation: AnnotationEntity }>()
);

export const loadHiddenAnnotationIds = createAction(
  '[APP/Annotations-Viewer] Load Hidden Annotation Ids',
  props<{
    scopeId: string,
    slideId: string,
    currentView: CurrentView,
    jobIds: string[],
    classValues?: string[],
    annotationTypes?: AnnotationType[],
  }>()
);

export const loadHiddenAnnotationIdsSuccess = createAction(
  '[APP/Annotations-Viewer] Load Hidden Annotation Ids Success',
  props<{ hidden: string[] }>()
);

export const loadHiddenAnnotationIdsFailure = createAction(
  '[APP/Annotations-Viewer] Load Hidden Annotation Ids Failure',
  props<{ error: HttpErrorResponse }>()
);

export const clearCentroids = createAction(
  '[APP/Annotations-Viewer] Clear Centroids',
);

export const stopLoading = createAction(
  '[APP/Annotations-Viewer] Stop Loading',
);
