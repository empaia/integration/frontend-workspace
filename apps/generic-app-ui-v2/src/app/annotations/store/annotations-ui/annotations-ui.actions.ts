import { createAction, props } from '@ngrx/store';
import { AnnotationEntity, CurrentView as ApiCurrentView, ToolbarInteractionType } from 'slide-viewer';
import { CurrentView } from '@annotations/store/annotations-ui/annotations-ui.models';
import { HttpErrorResponse } from '@angular/common/http';
import { RenderingHint } from 'empaia-ui-commons';

export const setViewport = createAction(
  '[APP/Annotation UI] Set Viewport',
  props<{ currentView: ApiCurrentView }>()
);

export const setViewportReady = createAction(
  '[APP/Annotation UI] Set Viewport Ready',
  props<{ currentView: CurrentView }>()
);

export const setInteractionType = createAction(
  '[APP/Annotation UI] Set Interaction Type',
  props<{ interactionType: ToolbarInteractionType | undefined }>()
);

export const setAllowedInteractionTypes = createAction(
  '[APP/Annotation UI] Set Allowed Interaction Types',
  props<{ allowedDrawTypes: ToolbarInteractionType[] | undefined }>()
);

export const loadReferenceAnnotation = createAction(
  '[APP/Annotation UI] Load Reference Annotation',
  props<{
    scopeId: string;
    annotationId: string;
  }>()
);

export const loadReferenceAnnotationSuccess = createAction(
  '[APP/Annotation UI] Load Reference Annotation Success',
  props<{ referenceAnnotation: AnnotationEntity }>()
);

export const loadReferenceAnnotationFailure = createAction(
  '[APP/Annotation UI] Load Reference Annotation Failure',
  props<{ error: HttpErrorResponse }>()
);

export const setEadAnnotationRenderingHint = createAction(
  '[APP/Annotation UI] Set Ead Annotation Rendering Hint',
  props<{ eadAnnotationRenderingHint?: RenderingHint[] | undefined }>(),
);

export const clearReferenceAnnotation = createAction(
  '[APP/Annotation UI] Clear Reference Annotation',
);
