import { HttpErrorResponse } from '@angular/common/http';
import { createAction, props } from '@ngrx/store';
import { AnnotationEntity } from 'slide-viewer';

export const loadInputAnnotations = createAction(
  '[APP/Annotations] Load Input Annotations',
  props<{
    scopeId: string,
    withClasses: boolean,
    skip: number,
    limit: number,
    slideId: string,
    jobIds: string[],
  }>()
);

export const loadInputAnnotationsSuccess = createAction(
  '[APP/Annotations] Load Input Annotations Success',
  props<{ annotations: AnnotationEntity[] }>()
);

export const loadInputAnnotationsFailure = createAction(
  '[APP/Annotations] Load Input Annotations Failure',
  props<{ error: HttpErrorResponse }>()
);

export const clearAnnotations = createAction(
  '[APP/Annotations] Clear Annotations',
);

export const selectAnnotation = createAction(
  '[APP/Annotations] Select Annotation',
  props<{ selected?: string }>()
);

export const addAnnotation = createAction(
  '[APP/Annotations] Add Annotation',
  props<{ annotation: AnnotationEntity }>()
);
