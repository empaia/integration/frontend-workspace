import { Injectable } from '@angular/core';
import { Actions, concatLatestFrom, createEffect, ofType } from '@ngrx/effects';
import { AnnotationsColorMapsActions } from './annotations-color-maps.actions';
import * as ClassesActions from '@classes/store/classes/classes.actions';
import * as ScopeSelectors from '@scope/store/scope/scope.selectors';
import { map } from 'rxjs/operators';
import { Store } from '@ngrx/store';
import { filterNullish } from '@shared/helper/rxjs-operators';
import { AnnotationsColorMapsService, EmpaiaAppDescriptionV3 } from 'empaia-ui-commons';
import { compareFullClassesAsc } from './annotations-color-maps.models';



@Injectable()
export class AnnotationsColorMapsEffects {

  clearAnnotationRenderingHints$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ClassesActions.clearClassesEAD),
      map(() => AnnotationsColorMapsActions.clearAnnotationColorMaps())
    );
  });

  // set annotation rendering hints after class namespace
  // was successfully loaded
  setAnnotationRenderingHints$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ClassesActions.loadClassNamespacesSuccess),
      map(action => action.classes.map(entity => entity.id).sort(compareFullClassesAsc)),
      concatLatestFrom(() => [
        this.store.select(ScopeSelectors.selectExtendedScope).pipe(
          filterNullish(),
          map(extended => extended.ead as EmpaiaAppDescriptionV3)
        )
      ]),
      map(([classes, ead]) => this.acmService.getAllV3AnnotationRenderingHints(ead, classes)),
      map(annotationRenderingHints => AnnotationsColorMapsActions.setAnnotationColorMaps({
        annotationRenderingHints
      }))
    );
  });

  // select first rendering as default after setting
  // the rendering hints
  selectFirstRenderingHint$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(AnnotationsColorMapsActions.setAnnotationColorMaps),
      map(action => action.annotationRenderingHints[0].id),
      map(selected => AnnotationsColorMapsActions.selectAnnotationColorMap({ selected }))
    );
  });

  constructor(
    private readonly actions$: Actions,
    private readonly store: Store,
    private readonly acmService: AnnotationsColorMapsService,
  ) {}
}
