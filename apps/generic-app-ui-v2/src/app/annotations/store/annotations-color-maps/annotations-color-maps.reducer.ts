import { createReducer, on } from '@ngrx/store';
import { AnnotationsColorMapsActions } from './annotations-color-maps.actions';
import { EntityState, createEntityAdapter } from '@ngrx/entity';
import { AnnotationColorMap } from 'empaia-ui-commons';


export const ANNOTATIONS_COLOR_MAPS_FEATURE_KEY = 'annotationsColorMaps';

export interface State extends EntityState<AnnotationColorMap> {
  selected?: string | undefined;
}

export const annotationRenderingHintAdapter = createEntityAdapter<AnnotationColorMap>();

export const initialState: State = annotationRenderingHintAdapter.getInitialState({
  selected: undefined,
});

export const reducer = createReducer(
  initialState,
  on(AnnotationsColorMapsActions.setAnnotationColorMaps, (state, { annotationRenderingHints }): State =>
    annotationRenderingHintAdapter.setAll(annotationRenderingHints, {
      ...state
    })
  ),
  on(AnnotationsColorMapsActions.selectAnnotationColorMap, (state, { selected }): State => ({
    ...state,
    selected,
  })),
  on(AnnotationsColorMapsActions.clearAnnotationColorMaps, (state): State =>
    annotationRenderingHintAdapter.removeAll({
      ...state,
      ...initialState,
    })
  )
);
