import { AnnotationEntity } from 'slide-viewer';
import { Dictionary } from '@ngrx/entity';
import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { JobRoiEntity, JobSelector } from '@jobs/store';

@Component({
  selector: 'app-regions-of-interest',
  templateUrl: './regions-of-interest.component.html',
  styleUrls: ['./regions-of-interest.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RegionsOfInterestComponent {
  @Input() public jobRoiEntities!: JobRoiEntity[];
  @Input() public jobsSelections!: Dictionary<JobSelector>;
  @Input() public selectedRoi!: string | undefined;

  @Output() public changeJobSelection = new EventEmitter<JobSelector>();
  @Output() public roiClicked = new EventEmitter<string>();
  @Output() public zoomToAnnotation = new EventEmitter<AnnotationEntity>();
}
