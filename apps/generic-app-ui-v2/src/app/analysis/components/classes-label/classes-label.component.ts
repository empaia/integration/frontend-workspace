import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
  selector: 'app-classes-label',
  templateUrl: './classes-label.component.html',
  styleUrls: ['./classes-label.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ClassesLabelComponent {}
