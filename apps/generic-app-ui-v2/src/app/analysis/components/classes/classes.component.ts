import { Dictionary } from '@ngrx/entity';
import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { ClassEntity, ClassSelector } from '@classes/store';
import { AnnotationColorMap, RenderingHint } from 'empaia-ui-commons';
import { ClassColorMapping } from 'slide-viewer';

@Component({
  selector: 'app-classes',
  templateUrl: './classes.component.html',
  styleUrls: ['./classes.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ClassesComponent {
  @Input() public classEntities!: ClassEntity[];
  @Input() public classesDict!: Dictionary<ClassEntity>;
  @Input() public annotationColorMaps!: AnnotationColorMap[];
  @Input() public selectedAnnotationColorMapId?: string | undefined;
  @Input() public selectedAnnotationColorMap?: AnnotationColorMap;
  @Input() public classColorMapping: ClassColorMapping | undefined;
  @Input() public classSelections!: Dictionary<ClassSelector>;
  @Input() public hasEadRenderingHint = false;

  @Output() public changeClassSelection = new EventEmitter<ClassSelector>();
  @Output() public changeAnnotationColorMap = new EventEmitter<string>();

  public trackByClassEntity(_index: number, classEntity: ClassEntity): string {
    return classEntity.id;
  }

  public trackByRenderingHint(_index: number, renderingHint: RenderingHint): string {
    return renderingHint.class_value;
  }
}
