import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
  selector: 'app-regions-of-interest-label',
  templateUrl: './regions-of-interest-label.component.html',
  styleUrls: ['./regions-of-interest-label.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RegionsOfInterestLabelComponent {}
