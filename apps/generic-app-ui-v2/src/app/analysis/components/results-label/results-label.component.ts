import { AnnotationEntity } from 'slide-viewer';
import { ResultEntity } from '@analysis/store';
import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output, OnChanges, SimpleChanges } from '@angular/core';
import { SlideEntity } from '@slides/store';
import { headlineInfoToString, isWsiInstance, SLIDE_NAME } from './results-label.models';

@Component({
  selector: 'app-results-label',
  templateUrl: './results-label.component.html',
  styleUrls: ['./results-label.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ResultsLabelComponent implements OnChanges {
  @Input() public resultEntity!: ResultEntity;

  @Output() public deselectReference = new EventEmitter<void>();

  public headlineName: string | undefined = '';
  public headlineTooltip = '';
  public readonly DEFAULT_RESULT = SLIDE_NAME;

  public ngOnChanges(changes: SimpleChanges): void {
    if (changes.resultEntity && this.resultEntity) {
      if (isWsiInstance(this.resultEntity)) {
        this.createWsiInfos(this.resultEntity);
      } else {
        this.createAnnotationInfos(this.resultEntity);
      }
    }
  }

  public onClearResult(event: MouseEvent): void {
    event.stopImmediatePropagation();
    this.deselectReference.emit();
  }

  private createWsiInfos(slide: SlideEntity): void {
    this.headlineName = SLIDE_NAME;
    this.headlineTooltip = headlineInfoToString({
      id: slide.id,
      type: 'wsi',
      name: slide.dataView.local_id,
    });
  }

  private createAnnotationInfos(annotation: AnnotationEntity): void {
    this.headlineName = annotation.name;
    this.headlineTooltip = headlineInfoToString({
      id: annotation.id,
      type: annotation.annotationType,
      name: annotation.name,
      description: annotation.description,
    });
  }
}
