import { PrimitiveEntity, ReferenceEntity } from './results.models';
import { HttpErrorResponse } from '@angular/common/http';
import { createAction, props } from '@ngrx/store';

export const loadPrimitives = createAction(
  '[App/Results] Load Primitives',
  props<{
    scopeId: string,
    referenceId: string,
    jobIds: string[]
  }>()
);

export const loadPrimitivesSuccess = createAction(
  '[App/Results] Load Primitives Success',
  props<{ primitives: PrimitiveEntity[] }>()
);

export const loadPrimitivesFailure = createAction(
  '[App/Results] Load Primitives Failure',
  props<{ error: HttpErrorResponse }>()
);

export const setPrimitivesReference = createAction(
  '[App/Results] Set Primitives Reference',
  props<{ reference?: ReferenceEntity }>()
);

export const clearPrimitives = createAction(
  '[App/Results] Clear Primitives',
);
