import { AnnotationEntity } from 'slide-viewer';
import { SlideEntity } from '@slides/store';
import {
  AppV2BoolPrimitive,
  AppV2FloatPrimitive,
  AppV2IntegerPrimitive,
  AppV2StringPrimitive,
} from 'empaia-api-lib';

export type BoolPrimitive = AppV2BoolPrimitive;
export type FloatPrimitive = AppV2FloatPrimitive;
export type IntegerPrimitive = AppV2IntegerPrimitive;
export type StringPrimitive = AppV2StringPrimitive;

export type PrimitiveEntity = IntegerPrimitive | FloatPrimitive | BoolPrimitive | StringPrimitive;

export type ResultEntity = SlideEntity | AnnotationEntity;

export interface ReferenceEntity {
  id: string;
  type: 'wsi' | 'annotation';
}

// we always wan't to use english for string comparison
// at primitive names
export const STRING_COMPARATOR_LOCAL = 'en';

export function comparePrimitiveNamesAsc(a: PrimitiveEntity, b: PrimitiveEntity): number {
  return a.name.trim().toLowerCase().localeCompare(b.name.trim().toLowerCase(), STRING_COMPARATOR_LOCAL);
}
