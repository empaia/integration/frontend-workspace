import { map, filter, retryWhen } from 'rxjs/operators';
import { pessimisticUpdate } from '@ngrx/router-store/data-persistence';
import { Store } from '@ngrx/store';
import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType, concatLatestFrom } from '@ngrx/effects';
import { AppV2DataPanelService } from 'empaia-api-lib';
import * as AnnotationsActions from '@annotations/store/annotations/annotations.actions';
import * as ResultsActions from './results.actions';
import * as ResultsFeature from './results.reducer';
import * as ResultsSelectors from './results.selectors';
import * as ScopeSelectors from '@scope/store/scope/scope.selectors';
import * as JobsActions from '@jobs/store/jobs/jobs.actions';
import * as JobsSelectors from '@jobs/store/jobs/jobs.selectors';
import * as SlidesActions from '@slides/store/slides/slides.actions';
import * as SlidesSelectors from '@slides/store/slides/slides.selectors';
import * as TokenActions from '@token/store/token/token.actions';
import { filterNullish, retryOnAction } from '@shared/helper/rxjs-operators';
import { requestNewToken } from 'vendor-app-communication-interface';
import { isJobRunning } from '@jobs/store/jobs/jobs.models';

@Injectable()
export class ResultsEffects {
  // clear primitives when selecting a slide
  clearOnSlideSelection$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(SlidesActions.selectSlide),
      map(() => ResultsActions.clearPrimitives())
    );
  });

  // select current slide as reference when a new
  // slide was selected (loaded)
  selectSlideOnLoad$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(SlidesActions.selectSlide),
      map((action) => action.id),
      filterNullish(),
      map((slideId) =>
        ResultsActions.setPrimitivesReference({
          reference: { id: slideId, type: 'wsi' },
        })
      )
    );
  });

  // clear primitive when deselecting reference
  clearOnDeselect$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ResultsActions.setPrimitivesReference),
      filter((action) => !action.reference),
      map(() => ResultsActions.clearPrimitives())
    );
  });

  // when deselecting a reference, select current slide
  // as default
  deselectReference$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ResultsActions.setPrimitivesReference),
      filter((action) => !action.reference),
      concatLatestFrom(() =>
        this.store
          .select(SlidesSelectors.selectSelectedSlideId)
          .pipe(filterNullish())
      ),
      map(([, slideId]) => slideId),
      map((slideId) =>
        ResultsActions.setPrimitivesReference({
          reference: { id: slideId, type: 'wsi' },
        })
      )
    );
  });

  // fetch primitives when jobs where successfully loaded
  fetchPrimitivesOnJobsLoaded$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(JobsActions.loadJobsSuccess),
      map((action) => action.jobs),
      filter((jobs) => !jobs.find(isJobRunning)),
      concatLatestFrom(() =>
        this.store
          .select(ResultsSelectors.selectResultsReference)
          .pipe(filterNullish())
      ),
      map(([, reference]) => reference),
      map((reference) => ResultsActions.setPrimitivesReference({ reference }))
    );
  });

  // set primitive reference when a roi was selected via viewer
  setReferenceOnRoiSelection$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(AnnotationsActions.selectAnnotation),
      map((action) => action.selected),
      map((roiId) =>
        ResultsActions.setPrimitivesReference({
          reference: roiId ? { id: roiId, type: 'annotation' } : undefined,
        })
      )
    );
  });

  setPrimitivesReference$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ResultsActions.setPrimitivesReference),
      map((action) => action.reference),
      filterNullish(),
      map((reference) => reference.id),
      concatLatestFrom(() => [
        this.store.select(ScopeSelectors.selectScopeId).pipe(filterNullish()),
        this.store.select(JobsSelectors.selectAllCheckedJobIds),
      ]),
      filter(([_referenceId, _scopeId, jobIds]) => !!jobIds?.length),
      map(([referenceId, scopeId, jobIds]) =>
        ResultsActions.loadPrimitives({
          referenceId,
          scopeId,
          jobIds,
        })
      )
    );
  });

  loadPrimitives$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ResultsActions.loadPrimitives),
      pessimisticUpdate({
        run: (
          action: ReturnType<typeof ResultsActions.loadPrimitives>,
          _state: ResultsFeature.State
        ) => {
          return this.dataPanelService
            .scopeIdPrimitivesQueryPut({
              scope_id: action.scopeId,
              body: {
                references: [action.referenceId],
                jobs: action.jobIds,
              },
            })
            .pipe(
              // retry to load the selected slide after the token is expired
              retryWhen((errors) =>
                retryOnAction(
                  errors,
                  this.actions$,
                  TokenActions.setAccessToken,
                  requestNewToken
                )
              ),
              map((repsonse) => repsonse.items),
              map((primitives) =>
                ResultsActions.loadPrimitivesSuccess({ primitives })
              )
            );
        },
        onError: (
          _action: ReturnType<typeof ResultsActions.loadPrimitives>,
          error
        ) => {
          return ResultsActions.loadPrimitivesFailure({ error });
        },
      })
    );
  });

  constructor(
    private readonly actions$: Actions,
    private readonly store: Store,
    private readonly dataPanelService: AppV2DataPanelService
  ) {}
}
