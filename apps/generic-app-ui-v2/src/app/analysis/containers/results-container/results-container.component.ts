import { AnnotationsActions } from '@annotations/store';
import { Observable } from 'rxjs';
import { ChangeDetectionStrategy, Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { PrimitiveEntity, ResultEntity, ResultsSelectors } from '@analysis/store';

@Component({
  selector: 'app-results-container',
  templateUrl: './results-container.component.html',
  styleUrls: ['./results-container.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ResultsContainerComponent {
  public resultEntity$: Observable<ResultEntity | undefined>;
  public primitiveEntities$: Observable<PrimitiveEntity[]>;
  public primitivesLoaded$: Observable<boolean>;

  constructor(private store: Store) {
    this.resultEntity$ = this.store.select(ResultsSelectors.selectCurrentReferenceEntity);
    this.primitiveEntities$ = this.store.select(ResultsSelectors.selectAllResults);
    this.primitivesLoaded$ = this.store.select(ResultsSelectors.selectResultsLoaded);
  }

  public onDeselectReference(): void {
    this.store.dispatch(AnnotationsActions.selectAnnotation({}));
  }
}
