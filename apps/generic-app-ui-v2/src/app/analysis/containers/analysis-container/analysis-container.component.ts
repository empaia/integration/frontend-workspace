import { Observable, of } from 'rxjs';
import { ChangeDetectionStrategy, Component } from '@angular/core';
import { MenuEntity } from '@menu/models/menu.models';
import { Store } from '@ngrx/store';
import { MenuSelectors } from '@menu/store';

@Component({
  selector: 'app-analysis-container',
  templateUrl: './analysis-container.component.html',
  styleUrls: ['./analysis-container.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AnalysisContainerComponent {
  public analysisMenu$: Observable<MenuEntity>;
  public analysisLoaded$ = of(true); // TODO: replace later for selector

  constructor(private store: Store) {
    this.analysisMenu$ = this.store.select(MenuSelectors.selectAnalysisMenu);
  }
}
