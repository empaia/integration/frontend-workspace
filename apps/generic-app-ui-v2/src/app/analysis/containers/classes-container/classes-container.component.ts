import { Dictionary } from '@ngrx/entity';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { ChangeDetectionStrategy, Component } from '@angular/core';
import { ClassEntity, ClassesSelectors, ClassSelector, ClassesActions } from '@classes/store';
import { AnnotationsColorMapsActions, AnnotationsColorMapsSelectors, AnnotationsUiSelectors } from '@annotations/store';
import { AnnotationColorMap } from 'empaia-ui-commons';
import { ClassColorMapping } from 'slide-viewer';

@Component({
  selector: 'app-classes-container',
  templateUrl: './classes-container.component.html',
  styleUrls: ['./classes-container.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ClassesContainerComponent {
  public classEntities$: Observable<ClassEntity[]>;
  public classesDict$: Observable<Dictionary<ClassEntity>>;
  public annotationColorMaps$: Observable<AnnotationColorMap[]>;
  public selectedAnnotationColorMapId$: Observable<string | undefined>;
  public selectedAnnotationColorMap$: Observable<AnnotationColorMap | undefined>;
  public classColorMapping$: Observable<ClassColorMapping | undefined>;
  public classSelections$: Observable<Dictionary<ClassSelector>>;
  public hasEadAnnotationRenderingHint$: Observable<boolean>;

  constructor(private store: Store) {
    this.classEntities$ = this.store.select(ClassesSelectors.selectAllClasses);
    this.classesDict$ = this.store.select(ClassesSelectors.selectClassEntities);
    this.classSelections$ = this.store.select(ClassesSelectors.selectClassSelectionEntity);
    this.annotationColorMaps$ = this.store.select(AnnotationsColorMapsSelectors.selectAllAnnotationColorMaps);
    this.selectedAnnotationColorMapId$ = this.store.select(AnnotationsColorMapsSelectors.selectSelectedAnnotationColorMapId);
    this.selectedAnnotationColorMap$ = this.store.select(AnnotationsColorMapsSelectors.selectSelectedAnnotationColorMap);
    this.classColorMapping$ = this.store.select(AnnotationsColorMapsSelectors.selectSelectedAnnotationClassColorMapping);
    this.hasEadAnnotationRenderingHint$ = this.store.select(AnnotationsUiSelectors.selectHasEadAnnotationRenderingHint);
  }

  public onClassSelectionChanged(classSelection: ClassSelector): void {
    this.store.dispatch(ClassesActions.setClassSelection({ classSelection }));
  }

  public onAnnotationColorMapChanged(selected: string): void {
    this.store.dispatch(AnnotationsColorMapsActions.selectAnnotationColorMap({ selected }));
  }
}
