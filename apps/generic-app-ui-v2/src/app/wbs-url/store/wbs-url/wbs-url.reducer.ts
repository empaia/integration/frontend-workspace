import { WbsUrl } from 'vendor-app-communication-interface';
import { createReducer, on } from '@ngrx/store';
import * as WbsUrlActions from './wbs-url.actions';


export const WBS_URL_FEATURE_KEY = 'wbsUrl';

export interface State {
  wbsUrl: WbsUrl | undefined;
}

export const initialState: State = {
  wbsUrl: undefined,
};

export const reducer = createReducer(
  initialState,
  on(WbsUrlActions.setWbsUrl, (state, { wbsUrl }): State => ({
    ...state,
    wbsUrl,
  }))
);
