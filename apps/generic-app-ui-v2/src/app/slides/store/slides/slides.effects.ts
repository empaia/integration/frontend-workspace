import { Injectable } from '@angular/core';
import { AppV2SlidesPanelService } from 'empaia-api-lib';
import { Actions, concatLatestFrom, createEffect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { fetch } from '@ngrx/router-store/data-persistence';
import { filterNullish, retryOnAction } from '@shared/helper/rxjs-operators';
import { SlideConversionService } from '@slides/services/slide-conversion.service';
import {
  catchError,
  distinctUntilChanged,
  filter,
  map,
  mergeMap,
  retryWhen,
} from 'rxjs/operators';
import { SlideEntity, SlidesFeature } from '..';
import * as SlidesActions from './slides.actions';
import * as ScopeActions from '@scope/store/scope/scope.actions';
import * as TokenActions from '@token/store/token/token.actions';
import * as ScopeSelectors from '@scope/store/scope/scope.selectors';
import * as TokenSelectors from '@token/store/token/token.selectors';
import { requestNewToken } from 'vendor-app-communication-interface';
import { of } from 'rxjs';

@Injectable()
export class SlidesEffects {
  // start loading slides after the scope id was set
  startLoadingSlides$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ScopeActions.setScope, TokenActions.setAccessToken),
      concatLatestFrom(() => [
        this.store.select(ScopeSelectors.selectScopeId).pipe(filterNullish()),
        this.store.select(TokenSelectors.selectAccessToken),
      ]),
      filter(([, _scopeId, token]) => !!token),
      distinctUntilChanged((prev, curr) => prev[1] === curr[1]),
      map(([, scopeId]) => scopeId),
      map((scopeId) => SlidesActions.loadSlides({ scopeId }))
    );
  });

  loadSlides$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(SlidesActions.loadSlides),
      fetch({
        run: (
          action: ReturnType<typeof SlidesActions.loadSlides>,
          _state: SlidesFeature.State
        ) => {
          return this.slidePanelService
            .scopeIdSlidesGet({
              scope_id: action.scopeId,
            })
            .pipe(
              // retry to load the selected slide after the token is expired
              retryWhen((errors) =>
                retryOnAction(
                  errors,
                  this.actions$,
                  TokenActions.setAccessToken,
                  requestNewToken
                )
              ),
              map((slides) => slides.items),
              map((slideViews) =>
                slideViews.map(
                  (v) =>
                    ({
                      id: v.id,
                      dataView: v,
                      disabled: false,
                    } as SlideEntity)
                )
              ),
              map((slides) => SlidesActions.loadSlidesSuccess({ slides }))
            );
        },
        onError: (
          _action: ReturnType<typeof SlidesActions.loadSlides>,
          error
        ) => {
          return SlidesActions.loadSlidesFailure({ error });
        },
      })
    );
  });

  selectSlide$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(SlidesActions.selectSlide),
      // map(action => action.id),
      filter((action) => !!action.id),
      map((action) => action.id),
      concatLatestFrom(() => this.store.select(ScopeSelectors.selectScopeId)),
      mergeMap(([id, scopeId]) =>
        this.slidePanelService
          .scopeIdSlidesSlideIdInfoGet({
            slide_id: id as string,
            scope_id: scopeId as string,
          })
          .pipe(
            map((slideInfo) => this.slideConverter.fromWbsApiType(slideInfo)),
            map((slideImage) =>
              SlidesActions.loadSlideImageInfoSuccess({ slideImage })
            ),
            // retry to load the selected slide after the token is expired
            retryWhen((errors) =>
              retryOnAction(
                errors,
                this.actions$,
                TokenActions.setAccessToken,
                requestNewToken
              )
            ),
            catchError((error) =>
              of(SlidesActions.loadSlidesFailure({ error }))
            )
          )
      )
    );
  });

  constructor(
    private readonly actions$: Actions,
    private readonly slidePanelService: AppV2SlidesPanelService,
    private readonly slideConverter: SlideConversionService,
    private readonly store: Store
  ) {}
}
