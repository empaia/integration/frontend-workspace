import {
  AppV2SlideInput as SlideView,
  AppV2SlideInfo,
  AppV2SlideLevel,
  AppV2SlideColor,
} from 'empaia-api-lib';
import { Slide } from 'slide-viewer';

export type SlideInfo = AppV2SlideInfo;
export type SlideLevel = AppV2SlideLevel;
export type SlideColor = AppV2SlideColor;

export interface SlideEntity {
  id: string;
  disabled: boolean;
  dataView: SlideView;
  imageView?: Slide;
}
