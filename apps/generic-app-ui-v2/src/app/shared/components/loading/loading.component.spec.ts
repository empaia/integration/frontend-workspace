import { MaterialModule } from '@material/material.module';
import { createComponentFactory, Spectator } from '@ngneat/spectator/jest';
import { LoadingComponent } from './loading.component';

describe('LoadingComponent', () => {
  let spectator: Spectator<LoadingComponent>;
  const createComponent = createComponentFactory({
    component: LoadingComponent,
    imports: [
      MaterialModule,
    ]
  });

  beforeEach(() => spectator = createComponent());

  it('should create', () => {
    expect(spectator.component).toBeTruthy();
  });
});
