import { LoadingComponent } from '@shared/components/loading/loading.component';
import { MockComponents } from 'ng-mocks';
import { createComponentFactory, Spectator } from '@ngneat/spectator';
import { MenuLabelComponent } from './menu-label.component';

describe('MenuLabelComponent', () => {
  let spectator: Spectator<MenuLabelComponent>;
  const createComponent = createComponentFactory({
    component: MenuLabelComponent,
    declarations: [
      MockComponents(
        LoadingComponent
      )
    ]
  });

  beforeEach(() => spectator = createComponent());

  it('should create', () => {
    expect(spectator.component).toBeTruthy();
  });
});
