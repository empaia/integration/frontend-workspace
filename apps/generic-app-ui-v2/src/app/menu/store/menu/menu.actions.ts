import { createAction, props } from '@ngrx/store';
import { MenuType } from '@menu/models/menu.models';

export const showLabel = createAction(
  '[APP/Menu] Show Label',
  props<{ id: MenuType }>()
);

export const openMenu = createAction(
  '[APP/Menu] Open Menu',
  props<{ id: MenuType }>()
);

export const changeMenuWorkspaceSize = createAction(
  '[APP/Menu] Change Menu Workspace Size',
  props<{ domRect: DOMRect }>()
);

export const initWorkspaceSize = createAction(
  '[App/Menu] Init Workspace Size'
);

export const toggleMenu = createAction(
  '[APP/Menu] Toggle Menu',
);
