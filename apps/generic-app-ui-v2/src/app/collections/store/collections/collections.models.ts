import { AppV2Collection } from 'empaia-api-lib';
export {
  AppV2CollectionItemType as CollectionItemType,
  AppV2DataCreatorType as DataCreatorType,
} from 'empaia-api-lib';

export type Collection = AppV2Collection;
