import { HttpErrorResponse } from '@angular/common/http';
import { createAction, props } from '@ngrx/store';
import { ClassEntity, ClassSelector } from './classes.models';

export const loadClassNamespaces = createAction(
  '[Classes] Load Class Namespaces Ready',
  props<{ scopeId: string }>()
);

export const loadClassNamespacesSuccess = createAction(
  '[Classes] Load Class Namespaces Success',
  props<{ classes: ClassEntity[] }>()
);

export const loadClassNamespacesFailure = createAction(
  '[Classes] Load Class Namespaces Failure',
  props<{ error: HttpErrorResponse }>()
);

export const setClassesEAD = createAction(
  '[Classes] Set Classes EAD',
  props<{ classes: ClassEntity[] }>()
);

export const clearClassesEAD = createAction(
  '[Classes] Clear Classes EAD',
);

export const setClassSelection = createAction(
  '[Classes] Set Class Selection',
  props<{ classSelection: ClassSelector }>()
);

export const setClassesSelections = createAction(
  '[Classes] Set Classes Selections',
  props<{ classesSelections: ClassSelector[] }>()
);
