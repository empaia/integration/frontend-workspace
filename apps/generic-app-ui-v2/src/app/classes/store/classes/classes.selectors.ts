import { createSelector } from '@ngrx/store';
import {
  State as ModuleState,
  selectClassesFeatureState,
} from '../classes-feature.state';
import { classesAdapter, classSelectorAdapter } from './classes.reducer';

const {
  selectAll,
  selectEntities,
  selectIds,
} = classesAdapter.getSelectors();

export const selectClassesState = createSelector(
  selectClassesFeatureState,
  (state: ModuleState) => state.classes
);

export const selectAllClasses = createSelector(
  selectClassesState,
  selectAll,
);

export const selectClassEntities = createSelector(
  selectClassesState,
  selectEntities,
);

export const selectClassIds = createSelector(
  selectClassesState,
  (state) => selectIds(state) as string[]
);

export const selectAllCheckedClasses = createSelector(
  selectClassesState,
  (state) => classSelectorAdapter.getSelectors().selectAll(state.selectedClasses).filter(c => c.checked).map(c => c.id)
);

export const selectClassSelectionEntity = createSelector(
  selectClassesState,
  (state) => classSelectorAdapter.getSelectors().selectEntities(state.selectedClasses)
);
