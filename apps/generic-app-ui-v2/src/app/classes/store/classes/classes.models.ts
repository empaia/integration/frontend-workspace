import { AppV2ClassesDict } from 'empaia-api-lib';

export type ClassesDict = AppV2ClassesDict;

export interface ClassEntity {
  id: string;
  name: string;
  description?: string;
}

export interface ClassSelector {
  id: string;
  checked: boolean;
}

export interface EmpaiaAppDescription {
  name: string;
  name_short: string;
  namespace: string;
  description: string;
  inputs: object;
  outputs: object;
  classes?: object;
}

export interface ClassDictionaries {
  [p: string]: ClassesDict;
}

export const ROI_CLASS = 'org.empaia.global.v1.classes.roi';

// we always wan't to use english for string comparison
export const STRING_COMPARATOR_LOCAL = 'en';

// null id will be placed at the end of the list
export function compareClassesAsc(a: ClassEntity, b: ClassEntity): number {
  return a.id
    ? b.id
      ? a.id.localeCompare(b.id, STRING_COMPARATOR_LOCAL)
      : -1
    : 1;
}
