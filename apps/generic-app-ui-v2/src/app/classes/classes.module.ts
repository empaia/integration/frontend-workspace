import { EffectsModule } from '@ngrx/effects';
import { CLASSES_MODULE_FEATURE_KEY, reducers } from '@classes/store/classes-feature.state';
import { StoreModule } from '@ngrx/store';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ClassesEffects } from '@classes/store/classes/classes.effects';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    StoreModule.forFeature(
      CLASSES_MODULE_FEATURE_KEY,
      reducers
    ),
    EffectsModule.forFeature([
      ClassesEffects
    ])
  ]
})
export class ClassesModule { }
