import { createSelector } from '@ngrx/store';
import {
  State as ModuleState,
  selectTokenFeatureState,
} from '../token-feature.state';

export const selectTokenState = createSelector(
  selectTokenFeatureState,
  (state: ModuleState) => state.token
);

export const selectAccessToken = createSelector(
  selectTokenState,
  (state) => state.token?.value
);
