import * as fromJobs from './jobs.actions';

describe('JobsActions', () => {
  it('should return an action', () => {
    expect(fromJobs.loadJobs({
      scopeId: '',
      slideId: '',
      ead: {
        $shema: '',
        name: 'Test',
        name_short: 'Test',
        namespace: '',
        description: '',
        inputs: {},
        outputs: {}
      }
    }).type).toBe('[APP/Jobs] Load Jobs');
  });
});
