import { Injectable } from '@angular/core';
import { AppV2DataPanelService, AppV2JobsPanelService } from 'empaia-api-lib';
import {
  EXAMINATION_CLOSED_ERROR_STATUS_CODE,
  FORBIDDEN_ERROR_STATUS_CODE,
  VALIDATION_ERROR_STATUS_CODE,
} from '@menu/models/ui.models';
import { Actions, concatLatestFrom, createEffect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { fetch, pessimisticUpdate } from '@ngrx/router-store/data-persistence';
import {
  dispatchActionOnErrorCode,
  filterNullish,
  retryOnAction,
} from '@shared/helper/rxjs-operators';
import { of, timer } from 'rxjs';
import {
  catchError,
  exhaustMap,
  filter,
  map,
  retryWhen,
  switchMap,
  takeUntil,
} from 'rxjs/operators';
import { requestNewToken } from 'vendor-app-communication-interface';
import {
  isJobRunning,
  JobsFeature,
  JOB_POLLING_PERIOD,
  JobCreatorType,
  JOBS_ERROR_MAP,
} from '..';
import * as JobsActions from './jobs.actions';
import * as AnnotationsViewerActions from '@annotations/store/annotations-viewer/annotations-viewer.actions';
import * as ScopeActions from '@scope/store/scope/scope.actions';
import * as SlidesActions from '@slides/store/slides/slides.actions';
import * as TokenActions from '@token/store/token/token.actions';
import * as ScopeSelectors from '@scope/store/scope/scope.selectors';
import * as SlidesSelectors from '@slides/store/slides/slides.selectors';
import { CollectionItemType, DataCreatorType } from '@collections/store';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ErrorSnackbarComponent } from '@shared/components/error-snackbar/error-snackbar.component';
import {
  EadInputTypeAnnotation,
  EadService,
  EmpaiaAppDescriptionV1,
  EmpaiaAppDescriptionV3,
  InputKey,
} from 'empaia-ui-commons';

@Injectable()
export class JobsEffects {
  // clear jobs on slide selection
  clearJobsOnSlideSelection$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(SlidesActions.selectSlide, SlidesActions.clearSlides),
      map(() => JobsActions.clearJobs())
    );
  });

  // create job when annotation was created
  prepareCreateJob$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(AnnotationsViewerActions.createAnnotationSuccess),
      map((action) => action.annotation),
      concatLatestFrom(() => [
        this.store.select(ScopeSelectors.selectScopeId).pipe(filterNullish()),
        this.store
          .select(ScopeSelectors.selectExtendedScope)
          .pipe(filterNullish()),
      ]),
      map(([annotation, scopeId, scopeExtended]) =>
        JobsActions.createJob({
          scopeId,
          appId: scopeExtended.app_id,
          creatorId: scopeId,
          creatorType: JobCreatorType.Scope,
          annotation,
        })
      )
    );
  });

  loadJobs$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(JobsActions.loadJobs),
      fetch({
        id: (
          action: ReturnType<typeof JobsActions.loadJobs>,
          _state: JobsFeature.State
        ) => {
          return action.type;
        },
        run: (
          action: ReturnType<typeof JobsActions.loadJobs>,
          _state: JobsFeature.State
        ) => {
          return this.jobsPanelService
            .scopeIdJobsGet({
              scope_id: action.scopeId,
            })
            .pipe(
              // retry to load the selected slide after the token is expired
              retryWhen((errors) =>
                retryOnAction(
                  errors,
                  this.actions$,
                  TokenActions.setAccessToken,
                  requestNewToken
                )
              ),
              map((items) => items.items),
              map((jobs) => {
                const wsiInputKey = this.eadService.getV1TypeInputKey(
                  'wsi',
                  action.ead
                )?.inputKey as string;
                return jobs.filter(
                  (job) => job.inputs[wsiInputKey] === action.slideId
                );
              }),
              map((jobs) => JobsActions.loadJobsSuccess({ jobs }))
            );
        },
        onError: (_action: ReturnType<typeof JobsActions.loadJobs>, error) => {
          return JobsActions.loadJobsFailure({ error });
        },
      })
    );
  });

  jobsPolling$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(JobsActions.startJobsPolling),
      switchMap(() =>
        timer(0, JOB_POLLING_PERIOD).pipe(
          takeUntil(this.actions$.pipe(ofType(JobsActions.stopJobsPolling))),
          concatLatestFrom(() => [
            this.store
              .select(ScopeSelectors.selectScopeId)
              .pipe(filterNullish()),
            this.store
              .select(SlidesSelectors.selectSelectedSlideId)
              .pipe(filterNullish()),
            this.store.select(ScopeSelectors.selectExtendedScope).pipe(
              filterNullish(),
              map((extended) => extended.ead as EmpaiaAppDescriptionV1)
            ),
          ]),
          map(([, scopeId, slideId, ead]) =>
            JobsActions.loadJobs({ scopeId, slideId, ead })
          )
        )
      )
    );
  });

  stopJobsPolling$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(JobsActions.loadJobsSuccess),
      map((action) => action.jobs),
      filter((jobs) => !jobs.find(isJobRunning)),
      map(() => JobsActions.stopJobsPolling())
    );
  });

  startJobsPolling$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(JobsActions.createJobSuccess, SlidesActions.selectSlide),
      map(() => JobsActions.startJobsPolling())
    );
  });

  createJob$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(JobsActions.createJob),
      pessimisticUpdate({
        run: (
          action: ReturnType<typeof JobsActions.createJob>,
          _state: JobsFeature.State
        ) => {
          return this.jobsPanelService
            .scopeIdJobsPost({
              scope_id: action.scopeId,
              body: {
                app_id: action.appId,
                creator_id: action.creatorId,
                creator_type: action.creatorType,
              },
            })
            .pipe(
              // retry to load the selected slide after the token is expired
              retryWhen((errors) =>
                retryOnAction(
                  errors,
                  this.actions$,
                  TokenActions.setAccessToken,
                  requestNewToken
                )
              ),
              map((job) => job.id),
              map((jobId) =>
                JobsActions.setWsiJobInput({
                  scopeId: action.scopeId,
                  jobId,
                  annotation: action.annotation,
                })
              ),
              catchError((error) =>
                dispatchActionOnErrorCode(
                  error,
                  EXAMINATION_CLOSED_ERROR_STATUS_CODE,
                  ScopeActions.loadExtendedScope({ scopeId: action.scopeId })
                )
              )
            );
        },
        onError: (_action: ReturnType<typeof JobsActions.createJob>, error) => {
          return JobsActions.createJobFailure({ error });
        },
      })
    );
  });

  prepareWsiJobInput$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(JobsActions.setWsiJobInput),
      concatLatestFrom(() => [
        this.store
          .select(SlidesSelectors.selectSelectedSlideId)
          .pipe(filterNullish()),
        this.store
          .select(ScopeSelectors.selectExtendedScope)
          .pipe(filterNullish()),
      ]),
      map(([action, slideId, extended]) =>
        JobsActions.setWsiJobInputReady({
          ...action,
          annotation: action.annotation,
          slideId,
          slideInputKey: this.eadService.isV1Ead(extended.ead)
            ? (this.eadService.getV1WsiInputKey(
              extended.ead as EmpaiaAppDescriptionV1
            ) as string)
            : (this.eadService.getV3WsiInputKey(
              extended.ead as EmpaiaAppDescriptionV3,
              'standalone'
            ) as string),
          annotationInputKey: this.eadService.isV1Ead(extended.ead)
            ? (this.eadService.getV1TypeInputKey(
              action.annotation.annotationType as EadInputTypeAnnotation,
              extended.ead as EmpaiaAppDescriptionV1
            ) as InputKey)
            : (this.eadService.getV3TypeInputKey(
              action.annotation.annotationType as EadInputTypeAnnotation,
              extended.ead as EmpaiaAppDescriptionV3,
              'standalone'
            ) as InputKey),
        })
      )
    );
  });

  setWsiJobInput$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(JobsActions.setWsiJobInputReady),
      pessimisticUpdate({
        run: (
          action: ReturnType<typeof JobsActions.setWsiJobInputReady>,
          _state: JobsFeature.State
        ) => {
          return this.jobsPanelService
            .scopeIdJobsJobIdInputsInputKeyPut({
              scope_id: action.scopeId,
              job_id: action.jobId,
              input_key: action.slideInputKey,
              body: {
                id: action.slideId,
              },
            })
            .pipe(
              // retry to load the selected slide after the token is expired
              retryWhen((errors) =>
                retryOnAction(
                  errors,
                  this.actions$,
                  TokenActions.setAccessToken,
                  requestNewToken
                )
              ),
              // map(() => JobsActions.setAnnotationJobInput({ ...action })),
              map(() =>
                action.annotationInputKey.inCollection > 0
                  ? JobsActions.setCollectionToAnnotation({
                    ...action,
                    inputId: action.annotation.id,
                    collectionType:
                        action.annotation.annotationType.toLowerCase() as CollectionItemType,
                  })
                  : JobsActions.setAnnotationJobInput({
                    ...action,
                    inputId: action.annotation.id,
                  })
              ),
              catchError((error) =>
                dispatchActionOnErrorCode(
                  error,
                  EXAMINATION_CLOSED_ERROR_STATUS_CODE,
                  ScopeActions.loadExtendedScope({ scopeId: action.scopeId })
                )
              )
            );
        },
        onError: (
          action: ReturnType<typeof JobsActions.setWsiJobInputReady>,
          error
        ) => {
          // delete the current job if something goes wrong while setting the inputs
          return JobsActions.setJobInputFailure({
            error,
            ...action,
            inputId: action.annotation.id,
          });
          // return JobsActions.createJobFailure({ error });
        },
      })
    );
  });

  prepareCollectionToAnnotation$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(JobsActions.setCollectionToAnnotation),
      pessimisticUpdate({
        run: (
          action: ReturnType<typeof JobsActions.setCollectionToAnnotation>,
          _state: JobsFeature.State
        ) => {
          return this.dataPanelService
            .scopeIdCollectionsPost({
              scope_id: action.scopeId,
              body: {
                item_type: action.collectionType,
                items: [{ id: action.inputId }],
                type: 'collection',
                creator_type: DataCreatorType.Scope,
                creator_id: action.scopeId,
              },
            })
            .pipe(
              map((collection) => {
                const remain = action.annotationInputKey.inCollection - 1;
                return remain > 0
                  ? JobsActions.setCollectionToAnnotation({
                    ...action,
                    inputId: collection?.id as string,
                    collectionType: CollectionItemType.Collection,
                    annotationInputKey: {
                      ...action.annotationInputKey,
                      inCollection: remain,
                    },
                  })
                  : JobsActions.setAnnotationJobInput({
                    ...action,
                    inputId: collection?.id as string,
                  });
              })
            );
        },
        onError: (
          action: ReturnType<typeof JobsActions.setCollectionToAnnotation>,
          error
        ) => {
          // delete the current job if something goes wrong while setting the inputs
          return JobsActions.setJobInputFailure({ error, ...action });
        },
      })
    );
  });

  setAnnotationJobInput$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(JobsActions.setAnnotationJobInput),
      pessimisticUpdate({
        run: (
          action: ReturnType<typeof JobsActions.setAnnotationJobInput>,
          _state: JobsFeature.State
        ) => {
          return this.jobsPanelService
            .scopeIdJobsJobIdInputsInputKeyPut({
              scope_id: action.scopeId,
              job_id: action.jobId,
              input_key: action.annotationInputKey.inputKey,
              body: {
                id: action.inputId,
              },
            })
            .pipe(
              // retry to load the selected slide after the token is expired
              retryWhen((errors) =>
                retryOnAction(
                  errors,
                  this.actions$,
                  TokenActions.setAccessToken,
                  requestNewToken
                )
              ),
              map(() => JobsActions.runJob({ ...action })),
              catchError((error) =>
                dispatchActionOnErrorCode(
                  error,
                  EXAMINATION_CLOSED_ERROR_STATUS_CODE,
                  ScopeActions.loadExtendedScope({ scopeId: action.scopeId })
                )
              )
            );
        },
        onError: (
          action: ReturnType<typeof JobsActions.setAnnotationJobInput>,
          error
        ) => {
          // delete the current job if something goes wrong while setting the inputs
          return JobsActions.setJobInputFailure({ error, ...action });
          // return JobsActions.createJobFailure({ error });
        },
      })
    );
  });

  runJob$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(JobsActions.runJob),
      pessimisticUpdate({
        run: (
          action: ReturnType<typeof JobsActions.runJob>,
          _state: JobsFeature.State
        ) => {
          return this.jobsPanelService
            .scopeIdJobsJobIdRunPut({
              scope_id: action.scopeId,
              job_id: action.jobId,
            })
            .pipe(
              // retry to load the selected slide after the token is expired
              retryWhen((errors) =>
                retryOnAction(
                  errors,
                  this.actions$,
                  TokenActions.setAccessToken,
                  requestNewToken
                )
              ),
              map((job) => JobsActions.createJobSuccess({ job })),
              catchError((error) =>
                dispatchActionOnErrorCode(
                  error,
                  EXAMINATION_CLOSED_ERROR_STATUS_CODE,
                  ScopeActions.loadExtendedScope({ scopeId: action.scopeId })
                )
              )
            );
        },
        onError: (action: ReturnType<typeof JobsActions.runJob>, error) => {
          // delete the current job if something goes wrong while setting the inputs
          return JobsActions.setJobInputFailure({ error, ...action });
        },
      })
    );
  });

  deleteJob$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(JobsActions.deleteJob),
      pessimisticUpdate({
        run: (
          action: ReturnType<typeof JobsActions.deleteJob>,
          _state: JobsFeature.State
        ) => {
          return this.jobsPanelService
            .scopeIdJobsJobIdDelete({
              scope_id: action.scopeId,
              job_id: action.jobId,
            })
            .pipe(
              // retry to load the selected slide after the token is expired
              retryWhen((errors) =>
                retryOnAction(
                  errors,
                  this.actions$,
                  TokenActions.setAccessToken,
                  requestNewToken
                )
              ),
              map((job) => job.id),
              map((jobId) => JobsActions.deleteJobSuccess({ jobId }))
            );
        },
        onError: (_action: ReturnType<typeof JobsActions.deleteJob>, error) => {
          return JobsActions.deleteJobFailure({ error });
        },
      })
    );
  });

  prepareStopRunningJob$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(JobsActions.stopRunningJob),
      map((action) => action.jobId),
      concatLatestFrom(() =>
        this.store.select(ScopeSelectors.selectScopeId).pipe(filterNullish())
      ),
      map(([jobId, scopeId]) =>
        JobsActions.stopRunningJobReady({
          scopeId,
          jobId,
        })
      )
    );
  });

  stopRunningJob$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(JobsActions.stopRunningJobReady),
      pessimisticUpdate({
        run: (
          action: ReturnType<typeof JobsActions.stopRunningJobReady>,
          _state: JobsFeature.State
        ) => {
          return this.jobsPanelService
            .scopeIdJobsJobIdStopPut({
              scope_id: action.scopeId,
              job_id: action.jobId,
            })
            .pipe(
              // retry to load the selected slide after the token is expired
              retryWhen((errors) =>
                retryOnAction(
                  errors,
                  this.actions$,
                  TokenActions.setAccessToken,
                  requestNewToken
                )
              ),
              map((done) => JobsActions.stopRunningJobSuccess({ done }))
            );
        },
        onError: (
          _action: ReturnType<typeof JobsActions.stopRunningJobReady>,
          error
        ) => {
          JobsActions.stopRunningJobFailure({ error });
          return null;
        },
      })
    );
  });

  // stop loading state on extended scope reload
  stopLoading$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ScopeActions.loadExtendedScope),
      map(() => JobsActions.stopLoading())
    );
  });

  showErrorMessage$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(
        JobsActions.loadJobsFailure,
        JobsActions.createJobFailure,
        JobsActions.deleteJobFailure,
        JobsActions.setJobInputFailure,
        JobsActions.stopRunningJobFailure
      ),
      filter(
        (action) =>
          !(
            action.error.status === VALIDATION_ERROR_STATUS_CODE ||
            action.error.status === FORBIDDEN_ERROR_STATUS_CODE
          )
      ),
      exhaustMap((action) => {
        this.snackBar.openFromComponent(ErrorSnackbarComponent, {
          data: { title: JOBS_ERROR_MAP[action.type], error: action.error },
        });
        return of({ type: 'noop' });
      })
    );
  });

  constructor(
    private readonly actions$: Actions,
    private readonly store: Store,
    private readonly jobsPanelService: AppV2JobsPanelService,
    private readonly dataPanelService: AppV2DataPanelService,
    private readonly eadService: EadService,
    private readonly snackBar: MatSnackBar
  ) {}
}
