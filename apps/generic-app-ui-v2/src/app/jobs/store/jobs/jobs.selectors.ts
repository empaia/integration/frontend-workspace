import { createSelector } from '@ngrx/store';
import {
  State as ModuleState,
  selectJobsFeatureState,
} from '../jobs-feature.state';
import { jobAdapter, jobSelectorAdapter } from './jobs.reducer';
import * as CollectionsSelectors from '@collections/store/collections/collections.selectors';
import * as AnnotationsSelectors from '@annotations/store/annotations/annotations.selectors';
import { JobRoiEntity } from './jobs.models';
import { AnnotationEntity } from 'slide-viewer';

const {
  selectIds,
  selectAll,
} = jobAdapter.getSelectors();

export const selectJobState = createSelector(
  selectJobsFeatureState,
  (state: ModuleState) => state.jobs
);

export const selectJobsLoaded = createSelector(
  selectJobState,
  (state) => state.loaded
);

export const selectAllJobs = createSelector(
  selectJobState,
  (state) => selectAll(state)
);

export const selectAllJobIds = createSelector(
  selectJobState,
  (state) => selectIds(state) as string[]
);

export const selectAllSelectedState = createSelector(
  selectJobState,
  (state) => state.allSelected
);

export const selectAllJobsSelections = createSelector(
  selectJobState,
  (state) => jobSelectorAdapter.getSelectors().selectAll(state.selectedJobs)
);

export const selectJobSelectionEntities = createSelector(
  selectJobState,
  (state) => jobSelectorAdapter.getSelectors().selectEntities(state.selectedJobs)
);

export const selectAllCheckedJobIds = createSelector(
  selectAllJobsSelections,
  (jobsSelections) => jobsSelections.filter(j => !!j.checked).map(j => j.id)
);

export const selectAllUncheckedJobIds = createSelector(
  selectAllJobsSelections,
  (jobsSelections) => jobsSelections.filter(j => !j.checked).map(j => j.id)
);

export const selectAllJobsWithRois = createSelector(
  selectAllJobs,
  CollectionsSelectors.selectCollectionEntities,
  AnnotationsSelectors.selectAnnotationEntities,
  (jobs, collectionEntities, annotationEntities) => {
    const jobRoiEntities: JobRoiEntity[] = [];

    jobs.forEach(job => {
      const inputs = Object.values(job.inputs);
      const collectionItemIds = inputs.map(i => collectionEntities[i]).filter(c => !!c).map(c => c?.items?.[0]?.id).filter(id => !!id) as string[];
      let annotations: AnnotationEntity[] = collectionItemIds.map(id => annotationEntities[id]).filter(a => !!a) as AnnotationEntity[];
      annotations = annotations.concat(inputs.map(i => annotationEntities[i]).filter(a => !!a) as AnnotationEntity[]).filter(a => !!a);
      const jobRoiEntity: JobRoiEntity = {
        job,
        roi: annotations[0]
      };
      if (jobRoiEntity.roi) {
        jobRoiEntities.push(jobRoiEntity);
      }
    });

    return jobRoiEntities;
  }
);
