import { map, filter, distinctUntilChanged, exhaustMap } from 'rxjs/operators';
import { Store } from '@ngrx/store';
import { AppV2ScopePanelService } from 'empaia-api-lib';
import { fetch } from '@ngrx/router-store/data-persistence';
import { ScopeService } from '@scope/services/scope.service';
import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType, concatLatestFrom } from '@ngrx/effects';
import { ScopeFeature } from '..';
import * as ScopeActions from './scope.actions';
import * as ScopeSelectors from './scope.selectors';
import * as TokenActions from '@token/store/token/token.actions';
import * as TokenSelectors from '@token/store/token/token.selectors';
import * as JobsActions from '@jobs/store/jobs/jobs.actions';
import * as AnnotationsViewerActions from '@annotations/store/annotations-viewer/annotations-viewer.actions';
import { filterNullish } from '@shared/helper/rxjs-operators';
import { INCOMPATIBLE_APP, ScopeErrorMap } from './scope.models';
import { ErrorSnackbarComponent } from '@shared/components/error-snackbar/error-snackbar.component';
import { of } from 'rxjs';
import { MatSnackBar } from '@angular/material/snack-bar';
import { EXAMINATION_CLOSED_ERROR_STATUS_CODE } from '@menu/models/ui.models';
import { EadService, EmpaiaAppDescriptionV1 } from 'empaia-ui-commons';

@Injectable()
export class ScopeEffects {
  setScopeId$ = createEffect(
    () => {
      return this.actions$.pipe(
        ofType(ScopeActions.setScope),
        map((action) => action.scopeId),
        map((scopeId) => (this.scopeService.scopeId = scopeId))
      );
    },
    { dispatch: false }
  );

  startLoadingExtendedScope$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ScopeActions.setScope, TokenActions.setAccessToken),
      concatLatestFrom(() => [
        this.store.select(ScopeSelectors.selectScopeId).pipe(filterNullish()),
        this.store.select(TokenSelectors.selectAccessToken),
      ]),
      filter(([, _scopeId, token]) => !!token),
      distinctUntilChanged((prev, curr) => prev[1] === curr[1]),
      map(([, scopeId]) => scopeId),
      map((scopeId) => ScopeActions.loadExtendedScope({ scopeId }))
    );
  });

  loadExtendedScope$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ScopeActions.loadExtendedScope),
      fetch({
        run: (
          action: ReturnType<typeof ScopeActions.loadExtendedScope>,
          _state: ScopeFeature.State
        ) => {
          return this.scopePanelService
            .scopeIdGet({
              scope_id: action.scopeId,
            })
            .pipe(
              map((extendedScope) =>
                ScopeActions.loadExtendedScopeSuccess({ extendedScope })
              )
            );
        },
        onError: (
          _action: ReturnType<typeof ScopeActions.loadExtendedScope>,
          error
        ) => {
          return ScopeActions.loadExtendedScopeFailure({ error });
        },
      })
    );
  });

  // check ead if app inputs are compatible with rectangle
  checkEadInputCompatibility$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ScopeActions.loadExtendedScopeSuccess),
      map((action) => action.extendedScope.ead as EmpaiaAppDescriptionV1),
      map((ead) =>
        this.eadService.hasAnyV1CompatibleInputType(ead, [
          'rectangle',
          'polygon',
          'circle',
        ])
      ),
      map((compatible) =>
        compatible
          ? ScopeActions.checkEadInputCompatibilitySuccess()
          : ScopeActions.checkEadInputCompatibilityFailure({
            error: INCOMPATIBLE_APP,
          })
      )
    );
  });

  // refetch extended scope on job creation failure
  // annotation failure when error code is 423
  refetchOnFailure$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(
        JobsActions.createJobFailure,
        AnnotationsViewerActions.createAnnotationFailure
      ),
      filter(
        (action) => action.error.status === EXAMINATION_CLOSED_ERROR_STATUS_CODE
      ),
      concatLatestFrom(() => [
        this.store.select(ScopeSelectors.selectScopeId).pipe(filterNullish()),
      ]),
      map(([, scopeId]) => scopeId),
      map((scopeId) => ScopeActions.loadExtendedScope({ scopeId }))
    );
  });

  showErrorMessage$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(
        ScopeActions.loadExtendedScopeFailure,
        ScopeActions.checkEadInputCompatibilityFailure
      ),
      exhaustMap((action) => {
        this.snackBar.openFromComponent(ErrorSnackbarComponent, {
          data: { title: ScopeErrorMap[action.type], error: action.error },
        });
        return of({ type: 'noop' });
      })
    );
  });

  constructor(
    private readonly actions$: Actions,
    private readonly scopeService: ScopeService,
    private readonly scopePanelService: AppV2ScopePanelService,
    private readonly store: Store,
    private readonly snackBar: MatSnackBar,
    private readonly eadService: EadService
  ) {}
}
