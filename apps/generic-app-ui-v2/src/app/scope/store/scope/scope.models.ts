import { AppV2ExtendedScope } from 'empaia-api-lib';
import { EAD_COMPATIBILITY_ERROR, EXTENDED_SCOPE_LOAD_ERROR } from '@menu/models/ui.models';

export type ExtendedScope = AppV2ExtendedScope;

export const ScopeErrorMap = {
  '[APP/Scope] Load Extended Scope Failure': EXTENDED_SCOPE_LOAD_ERROR,
  '[App/Scope] Check Ead Input Compatibility Failure': EAD_COMPATIBILITY_ERROR,
};

export const INCOMPATIBLE_APP = { message: 'App is not compatible with rectangle, polygon or circle input!' };
