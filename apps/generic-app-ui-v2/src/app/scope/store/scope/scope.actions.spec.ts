import * as fromScope from './scope.actions';

describe('loadScopes', () => {
  it('should return an action', () => {
    expect(fromScope.setScope({
      scopeId: 'test-scope-id'
    }).type).toBe('[APP/Scope] Set Scope');
  });
});
