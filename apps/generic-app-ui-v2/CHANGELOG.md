# Generic App Ui V2 Changelog

# 2.5.5 (2024-07-16)

### Refactor

* upgraded to angular 17

# 2.5.3 (2024-01-26)

### Refactor

* changed panel division

# 2.5.2 (2024-01-11)

### Refactor

* updated to angular 16

# 2.5.1 (2024-01-05)

### Refactor

* changed annotation loading spinner position and appearance

# 2.5.0 (2023-12-22)

### Features

* added support for annotation rendering hints of ead

# 2.4.8 (2023-11-02)

### Refactor

* refactored error messages for annotations and jobs

# 2.4.4 (2023-09-18)

### Refactor

* positioned tooltips above mouse cursor when ever possible

# 2.4.0 (2023-08-02)

### Refactor

* optimized slide label and thumbnail fetching

# 2.3.0 (2023-06-30)

### Features

* primitives are sorted ascending via name
* classes and color classes are sorted ascending via id

### Fixes

* use annotation type for collection type instead of fallback

# 2.2.0 (2023-06-12)

### Features

* preloads all fonts from the start

# 2.1.8 (2023-06-08)

### Refactor

* refactored tooltips

# 2.1.1 (2023-03-24)

### Refactor

* use css for truncate text

# 2.1.0 (2023-03-24)

### Features

* added clipboard feature

# 2.0.0 (2023-03-20)

### Features

* updated to Angular 15

# 0.1.0 (2023-03-07)

### Features

* back ported design and features from Generic App Ui V3
* added undefined (null) class to class list
* posted annotation is checked beforehand if class constraint for EMPAIA ROI class is set for input annotations

# 0.0.5 (2023-01-20)

### Fixes

* ROI's don't reappear after deselection

# 0.0.1 (2022-11-24)

### Refactor

* Changed the name of the app from Generic App Ui to Generic App Ui V2

# Previous Changelog entries before name change

# 0.1.1 (2022-11-22)

### Refactor

* Removed DataPersistence Object

### Fixes

* Environment variable will be read properly in production mode

# 0.1.0 (2022-11-18)

### Features

* Added V3 Routes and Ead V3 compatibility

# 0.0.6 (2022-11-07)

### Refactor

* Removed Angular fxLayout

# 0.0.4 (2022-11-01)

### Refactor

* Removed unused cypress e2e tests

# 0.0.3 (2022-10-13)

### Refactor

* Updated dependencies

# 0.0.2 (2022-10-10)

### Fixes

* prevent to stop normal mouse events after using alt + tab

# 0.0.1 (2022-09-26)

### Feature

* First iteration of the generic app ui, tested with sample-app and hsa lca app
