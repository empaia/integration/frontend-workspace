# Sample App V2 Changelog

# 2.3.4 (2024-07-16)

### Refactor

* upgraded to angular 17

# 2.3.2 (2024-01-11)

### Refactor

* updated to angular 16

# 2.3.1 (2024-01-05)

### Refactor

* changed annotation loading spinner position and appearance

# 2.3.0 (2023-12-22)

### Features

* added support for annotation rendering hints of ead

# 2.2.8 (2023-11-02)

### Refactor

* refactored error messages for annotations and jobs

# 2.2.4 (2023-09-18)

### Refactor

* positioned tooltips above mouse cursor when ever possible

# 2.2.0 (2023-08-02)

### Refactor

* optimized slide label and thumbnail fetching

# 2.1.0 (2023-06-12)

### Features

* preloads all fonts from the start

# 2.0.9 (2023-06-08)

### Refactor

* refactored tooltips

# 2.0.2 (2023-03-24)

### Refactor

* use css for truncate text

# 2.0.0 (2023-03-20)

### Features

* updated to Angular 15

# 0.0.5 (2023-01-20)

### Fixes

* ROI's don't reappear after deselection

# 0.0.1 (2022-11-24)

### Refactor

* Changed the name of the app from Sample App to Sample App V2

# Previous Changelog entries before name change

# 0.6.2 (2022-11-23)

### Fixes

* Environment variable will be read properly in production mode

# 0.6.1 (2022-11-22)

### Refactor

* Removed DataPersistence Object

# 0.6.0 (2022-11-15)

### Features

* Added api version v3 support via environment variable

# 0.5.2 (2022-11-07)

### Refactor

* Removed Angular fxLayout

# 0.5.0 (2022-11-01)

### Features

* Added playwright e2e tests

# 0.4.3 (2022-10-13)

### Refactor

* Updated dependencies

# 0.4.2 (2022-10-10)

### Fixes

* prevent to stop normal mouse events after using alt + tab

# 0.4.0 (2022-09-19)

### Features

* Added an extra over zooming layer to every wsi

# 0.3.0 (2022-08-03)

### Features

* Read only mode is active when examination is closed

# 0.2.11 (2022-07-12)

### Refactor

* Using updated Slide Viewer (2022-07-12)
* Optimized compiling configuration
