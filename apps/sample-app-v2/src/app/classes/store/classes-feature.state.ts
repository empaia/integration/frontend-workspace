import { Action, combineReducers, createFeatureSelector } from '@ngrx/store';
import * as fromClasses from '@classes/store/classes/classes.reducer';

export const CLASSES_MODULE_FEATURE_KEY = 'classesModuleFeature';

export const selectClassesFeatureState = createFeatureSelector<State>(
  CLASSES_MODULE_FEATURE_KEY
);

export interface State {
  [fromClasses.CLASSES_FEATURE_KEY]: fromClasses.State;
}

export function reducers(state: State | undefined, action: Action) {
  return combineReducers({
    [fromClasses.CLASSES_FEATURE_KEY]: fromClasses.reducer,
  })(state, action);
}
