import { createAction, props } from '@ngrx/store';
import { ClassEntity } from '@classes/store/classes/classes.models';
import { HttpError } from '@menu/models/ui.model';

export const loadClassNamespaces = createAction(
  '[Classes] Load Class Namespaces Ready',
  props<{ scopeId: string }>()
);

export const loadClassNamespacesSuccess = createAction(
  '[Classes] Load Class Namespaces Success',
  props<{ classes: ClassEntity[] }>()
);

export const loadClassNamespacesFailure = createAction(
  '[Classes] Load Class Namespaces Failure',
  props<{ error: HttpError }>()
);

export const setClassesEAD = createAction(
  '[Classes] Set Classes EAD',
  props<{ classes: ClassEntity[] }>()
);

export const clearClassesEAD = createAction(
  '[Classes] Clear Classes EAD',
);
