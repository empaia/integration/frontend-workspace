import * as fromToken from './token.actions';

describe('loadTokens', () => {
  it('should return an action', () => {
    expect(fromToken.setAccessToken({
      token: { value: '1234', type: 'token' }
    }).type).toBe('[APP/Token] Set Access Token');
  });
});
