import { Action, combineReducers, createFeatureSelector } from '@ngrx/store';
import * as fromToken from '@token/store/token/token.reducer';

export const TOKEN_MODULE_FEATURE_KEY = 'tokenModuleFeature';

export const selectTokenFeatureState = createFeatureSelector<State>(
  TOKEN_MODULE_FEATURE_KEY
);

export interface State {
  [fromToken.TOKEN_FEATURE_KEY]: fromToken.State;
}

export function reducers(state: State | undefined, action: Action) {
  return combineReducers({
    [fromToken.TOKEN_FEATURE_KEY]: fromToken.reducer,
  })(state, action);
}
