import { createReducer, on } from '@ngrx/store';
import { createEntityAdapter, EntityState } from '@ngrx/entity';
import { PrimitiveEntity } from '@results/store/results/results.models';
import { HttpError } from '@menu/models/ui.model';
import * as ResultsActions from './results.actions';


export const RESULTS_FEATURE_KEY = 'results';

export interface State extends EntityState<PrimitiveEntity> {
  loaded: boolean;
  error?: HttpError | undefined;
}

export const resultsAdapter = createEntityAdapter<PrimitiveEntity>();

export const initialState: State = resultsAdapter.getInitialState({
  loaded: true,
  error: undefined,
});

export const reducer = createReducer(
  initialState,
  on(ResultsActions.loadResults, (state): State => ({
    ...state,
    loaded: false,
  })),
  on(ResultsActions.loadResultsSuccess, (state, { results }): State =>
    resultsAdapter.setAll(results, {
      ...state,
      loaded: true,
    })
  ),
  on(ResultsActions.loadResultsFailure, (state, { error }): State => ({
    ...state,
    error,
    loaded: true,
  })),
  on(ResultsActions.clearResults, (state): State => ({
    ...state,
    ...initialState,
  })),
);
