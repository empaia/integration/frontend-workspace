import { AnnotationEntity } from 'slide-viewer';
import {
  AppV2IntegerPrimitive,
  AppV2FloatPrimitive,
  AppV2BoolPrimitive,
  AppV2StringPrimitive,
} from 'empaia-api-lib';
import { Job, JobStatus } from '@jobs/store/jobs/jobs.models';

export type IntegerPrimitive = AppV2IntegerPrimitive;
export type FloatPrimitive = AppV2FloatPrimitive;
export type BoolPrimitive = AppV2BoolPrimitive;
export type StringPrimitive = AppV2StringPrimitive;

export type PrimitiveEntity = IntegerPrimitive | FloatPrimitive | BoolPrimitive | StringPrimitive;

export interface ResultEntity {
  job: Job;
  annotation: AnnotationEntity;
  primitives: PrimitiveEntity[];
}

export const FINISHED_JOB_STATES = [JobStatus.Completed, JobStatus.Error, JobStatus.Failed, JobStatus.Incomplete, JobStatus.Timeout];
