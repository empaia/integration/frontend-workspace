import * as fromResults from './results.actions';

describe('loadResults', () => {
  it('should return an action', () => {
    expect(fromResults.loadResults({
      scopeId: 'SCOPE-ID',
      jobIds: ['JOB-ID'],
      annotationIds: ['ANNOTATION-ID']
    }).type).toBe('[APP/Results] Load Results');
  });
});
