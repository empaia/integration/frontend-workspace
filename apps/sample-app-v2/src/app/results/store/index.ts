import * as ResultsActions from './results/results.actions';
import * as ResultsFeature from './results/results.reducer';
import * as ResultsSelectors from './results/results.selectors';
export * from './results/results.effects';

export { ResultsActions, ResultsFeature, ResultsSelectors };
