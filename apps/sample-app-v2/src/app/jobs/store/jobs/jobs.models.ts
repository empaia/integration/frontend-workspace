import { CREATE_JOB_ERROR, DELETE_JOB_ERROR, ErrorMap, JOBS_LOAD_ERROR, UPDATE_JOB_ERROR } from '@menu/models/ui.model';
import {
  AppV2JobInput,
  AppV2JobStatus,
} from 'empaia-api-lib';
export {
  AppV2JobStatus as JobStatus,
  AppV2JobCreatorType as JobCreatorType,
} from 'empaia-api-lib';

export type Job = AppV2JobInput;

export interface JobSelector {
  id: string;
  checked: boolean;
}

export const JOB_POLLING_PERIOD = 5000;

export function isJobRunning(job: Job): boolean {
  return job.status === AppV2JobStatus.Assembly
    || job.status === AppV2JobStatus.Ready
    || job.status === AppV2JobStatus.Scheduled
    || job.status === AppV2JobStatus.Running;
}

export const JOBS_ERROR_MAP: ErrorMap = {
  '[APP/Jobs] Load Jobs Failure': JOBS_LOAD_ERROR,
  '[APP/Jobs] Create Job Failure': CREATE_JOB_ERROR,
  '[APP/Jobs] Set Job Input Failure': UPDATE_JOB_ERROR,
  '[APP/Jobs] Delete Job Failure': DELETE_JOB_ERROR,
  '[Jobs] Stop Running Job Failure': UPDATE_JOB_ERROR,
};
