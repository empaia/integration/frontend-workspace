import { Action, combineReducers, createFeatureSelector } from '@ngrx/store';
import * as fromWbsUrl from '@wbsUrl/store/wbs-url/wbs-url.reducer';

export const WBS_URL_MODULE_FEATURE_KEY = 'wbsUrlModuleFeature';

export const selectWbsUrlFeatureState = createFeatureSelector<State>(
  WBS_URL_MODULE_FEATURE_KEY
);

export interface State {
  [fromWbsUrl.WBS_URL_FEATURE_KEY]: fromWbsUrl.State;
}

export function reducers(state: State | undefined, action: Action) {
  return combineReducers({
    [fromWbsUrl.WBS_URL_FEATURE_KEY]: fromWbsUrl.reducer,
  })(state, action);
}
