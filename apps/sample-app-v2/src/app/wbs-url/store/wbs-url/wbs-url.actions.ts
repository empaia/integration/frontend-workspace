import { createAction, props } from '@ngrx/store';
import { WbsUrl } from 'vendor-app-communication-interface';

export const setWbsUrl = createAction(
  '[APP/WbsUrl] Set Wbs Url',
  props<{ wbsUrl: WbsUrl }>()
);
