import { JobStatusComponent } from './job-status.component';
import { createComponentFactory, Spectator } from '@ngneat/spectator';
import { MaterialModule } from '@material/material.module';

describe('JobStatusComponent', () => {
  let spectator: Spectator<JobStatusComponent>;
  const createComponent = createComponentFactory({
    component: JobStatusComponent,
    imports: [
      MaterialModule,
    ]
  });

  beforeEach(() => spectator = createComponent());

  it('should create', () => {
    expect(spectator.component).toBeTruthy();
  });
});
