import { Pipe, PipeTransform } from '@angular/core';
import { PrimitiveEntity } from '@results/store/results/results.models';

@Pipe({
  name: 'primitiveToString'
})
export class PrimitiveToStringPipe implements PipeTransform {

  transform(primitive: PrimitiveEntity): string {
    return `
    Id: ${primitive.id}
    Name: ${primitive.name}
    ${primitive.description ? 'Description: ' + primitive.description : ''}
    `;
  }

}
