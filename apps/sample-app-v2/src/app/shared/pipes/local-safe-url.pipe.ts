import { Pipe, PipeTransform } from '@angular/core';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';

@Pipe({
  name: 'localSafeUrl'
})
export class LocalSafeUrlPipe implements PipeTransform {

  constructor(private sanitizer: DomSanitizer) {}

  transform(localUrl: string): SafeUrl {
    return this.sanitizer.bypassSecurityTrustUrl(localUrl);
  }

}
