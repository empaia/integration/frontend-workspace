import { Pipe, PipeTransform } from '@angular/core';
import { Job } from '@jobs/store/jobs/jobs.models';

@Pipe({
  name: 'jobToString'
})
export class JobToStringPipe implements PipeTransform {

  transform(job: Job): string {
    return `
    Id: ${job.id}
    Status: ${job.status}
    ${job.error_message ? 'Error: ' + job.error_message : ''}
    `;
  }

}
