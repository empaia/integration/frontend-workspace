import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MenuItemLayoutContainerComponent } from './containers/menu-item-layout-container/menu-item-layout-container.component';
import { MenuLabelComponent } from './components/menu-label/menu-label.component';
import { LoadingComponent } from './components/loading/loading.component';
import { MaterialModule } from '@material/material.module';
import { MissingSelectionErrorComponent } from './components/missing-selection-error/missing-selection-error.component';
import { LocalSafeUrlPipe } from './pipes/local-safe-url.pipe';
import { DictParserPipe } from './pipes/dict-parser.pipe';
import { MenuButtonComponent } from './components/menu-button/menu-button.component';
import { JobStatusComponent } from './components/job-status/job-status.component';
import { IconCheckboxComponent } from './components/icon-checkbox/icon-checkbox.component';
import { CloseButtonComponent } from './components/close-button/close-button.component';
import { AnnotationToStringPipe } from './pipes/annotation-to-string.pipe';
import { JobToStringPipe } from './pipes/job-to-string.pipe';
import { PrimitiveToStringPipe } from './pipes/primitive-to-string.pipe';
import { ErrorSnackbarComponent } from './components/error-snackbar/error-snackbar.component';

const COMPONENTS = [
  MenuItemLayoutContainerComponent,
  MenuLabelComponent,
  LoadingComponent,
  MissingSelectionErrorComponent,
  MenuButtonComponent,
  JobStatusComponent,
  IconCheckboxComponent,
  CloseButtonComponent,
  AnnotationToStringPipe,
  JobToStringPipe,
  PrimitiveToStringPipe,
  ErrorSnackbarComponent,
];

const PIPES = [
  LocalSafeUrlPipe,
  DictParserPipe,
];

@NgModule({
  declarations: [
    COMPONENTS,
    PIPES,
  ],
  imports: [
    CommonModule,
    MaterialModule,
  ],
  exports: [
    COMPONENTS,
    PIPES,
  ]
})
export class SharedModule { }
