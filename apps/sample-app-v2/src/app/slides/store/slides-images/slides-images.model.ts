export enum SlideImageFormat {
  BMP = 'bmp',
  GIF = 'gif',
  JPEG = 'jpeg',
  JPG = 'jpg',
  PNG = 'png',
  TIFF = 'tiff'
}

export enum SlideImageStatus {
  LOADING = 'loading',
  LOADED = 'loaded',
  ERROR = 'error'
}

export interface SlideImage {
  id: string | number;
  labelStatus?: SlideImageStatus,
  thumbnailStatus?: SlideImageStatus,
  label?: string;
  thumbnail?: string;
}

export const MAX_SLIDE_LABEL_WIDTH = 200;
export const MAX_SLIDE_LABEL_HEIGHT = 200;
export const MAX_SLIDE_THUMBNAIL_WIDTH = 200;
export const MAX_SLIDE_THUMBNAIL_HEIGHT = 200;
export const SLIDE_THUMBNAIL_QUALITY = 80;

export const SLIDE_PAGINATION_DEBOUNCE_TIME = 300;
export const SLIDE_IMAGE_CONCURRENCY = 10;
