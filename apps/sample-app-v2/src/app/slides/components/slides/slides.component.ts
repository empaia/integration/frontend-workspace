import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { SlideEntity } from '@slides/store/slides/slides.models';
import { SlideImage } from '@slides/store/slides-images/slides-images.model';
import { SelectionError } from '@menu/models/ui.model';

@Component({
  selector: 'app-slides',
  templateUrl: './slides.component.html',
  styleUrls: ['./slides.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SlidesComponent {
  @Input() slideEntities!: SlideEntity[];
  @Input() slideImages!: SlideImage[];
  @Input() selectedSlideId!: string;

  @Output() slideSelected = new EventEmitter<string>();

  @Input() selectionMissing!: SelectionError;

  public getSlideImage(slideId: string): SlideImage | undefined {
    return this.slideImages.find(img => img.id === slideId);
  }
}
