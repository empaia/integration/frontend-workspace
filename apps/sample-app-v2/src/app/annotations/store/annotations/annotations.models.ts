import {
  AppV2ArrowAnnotation,
  AppV2CircleAnnotation,
  AppV2LineAnnotation,
  AppV2PointAnnotation,
  AppV2PolygonAnnotation,
  AppV2PostArrowAnnotation,
  AppV2PostCircleAnnotation,
  AppV2PostLineAnnotation,
  AppV2PostPointAnnotation,
  AppV2PostPolygonAnnotation,
  AppV2PostRectangleAnnotation,
  AppV2RectangleAnnotation
} from 'empaia-api-lib';
export {
  AppV2AnnotationType as AnnotationApiType,
  AppV2AnnotationReferenceType as AnnotationReferenceType,
} from 'empaia-api-lib';

export type ArrowAnnotation = AppV2ArrowAnnotation;
export type CircleAnnotation = AppV2CircleAnnotation;
export type LineAnnotation = AppV2LineAnnotation;
export type PointAnnotation = AppV2PointAnnotation;
export type PolygonAnnotation = AppV2PolygonAnnotation;
export type PostArrowAnnotation = AppV2PostArrowAnnotation;
export type PostCircleAnnotation = AppV2PostCircleAnnotation;
export type PostLineAnnotation = AppV2PostLineAnnotation;
export type PostPointAnnotation = AppV2PostPointAnnotation;
export type PostPolygonAnnotation = AppV2PostPolygonAnnotation;
export type PostRectangleAnnotation = AppV2PostRectangleAnnotation;
export type RectangleAnnotation = AppV2RectangleAnnotation;

export const INPUT_ANNOTATION_LIMIT = 10000;
