import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import * as AnnotationsUiActions from './annotations-ui.actions';
import * as SlidesActions from '@slides/store/slides/slides.actions';
import * as ScopeActions from '@scope/store/scope/scope.actions';
import { ViewportConversionService } from '@annotations/services/viewport-conversion.service';
import { filter, map } from 'rxjs/operators';
import { DrawType, InteractionType } from 'slide-viewer';
import { AppV2ExaminationState } from 'empaia-api-lib';


@Injectable()
export class AnnotationsUiEffects {

  convertViewport$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(AnnotationsUiActions.setViewport),
      map(action => action.currentView),
      map(currentView => {
        return {
          viewport: this.viewportConverter.convertFromApi(currentView.extent),
          nppCurrent: currentView.nppCurrent,
          nppBase: currentView.nppBase,
        };
      }),
      map(currentView => AnnotationsUiActions.setViewportReady({ currentView }))
    );
  });

  // set allowed interaction types to move and rectangle
  setAllowedInteractionTypes$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(SlidesActions.loadSlides),
      map(() => AnnotationsUiActions.setAllowedInteractionTypes({
        allowedDrawTypes: [InteractionType.Move]
      }))
    );
  });

  // set active interaction to move on examination close
  setInteractionToMoveOnClose$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ScopeActions.loadExtendedScopeSuccess),
      map(action => action.extendedScope.examination_state),
      filter(examinationState => examinationState === AppV2ExaminationState.Closed),
      map(() => AnnotationsUiActions.setInteractionType({ interactionType: InteractionType.Move }))
    );
  });

  // set interaction type move and rectangle when examination
  // is open
  setDrawTypesOnOpenExamination$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ScopeActions.loadExtendedScopeSuccess),
      map(action => action.extendedScope.examination_state),
      map(examinationState =>
        examinationState === AppV2ExaminationState.Open
          ? AnnotationsUiActions.setAllowedInteractionTypes({
            allowedDrawTypes: [InteractionType.Move, DrawType.Rectangle]
          })
          : AnnotationsUiActions.setAllowedInteractionTypes({
            allowedDrawTypes: [InteractionType.Move]
          })
      )
    );
  });

  constructor(
    private actions$: Actions,
    private viewportConverter: ViewportConversionService,
  ) {}

}
