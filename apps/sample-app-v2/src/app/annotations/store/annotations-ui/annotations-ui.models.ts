import { AppV2Viewport } from 'empaia-api-lib';

export type Viewport = AppV2Viewport;

export interface CurrentView {
  viewport: Viewport;
  nppCurrent: number;
  nppBase: number;
}
