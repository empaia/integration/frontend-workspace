import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [];

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forRoot(routes, {
      initialNavigation: 'disabled',
      useHash: true,
    })
  ],
  exports: [RouterModule]
})
export class AppRouterModule { }
