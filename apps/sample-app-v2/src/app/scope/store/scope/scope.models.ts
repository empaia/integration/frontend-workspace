import { AppV2ExtendedScope } from 'empaia-api-lib';

export type ExtendedScope = AppV2ExtendedScope;
