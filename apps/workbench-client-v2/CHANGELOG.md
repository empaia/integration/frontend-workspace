# Workbench Client 2.0

# 2.2.14 (2024-07-16)

### Refactor

* upgraded to angular 17

# 2.2.13 (2024-03-20)

### Fixes

* reopening the slide tray view displays properly

# 2.1.11 (2024-01-19)

### Refactor

* removed mock preview images from new apps panel

# 2.1.10 (2024-01-11)

### Refactor

* updated to angular 16

# 2.1.6 (2023-09-19)

### Refactor

* cut optional tailing slash in env urls

# 2.1.5 (2023-09-18)

### Refactor

* change api for optional user profile picture

# 2.1.4 (2023-09-18)

### Refactor

* positioned tooltips above mouse cursor when ever possible

# 2.1.0 (2023-08-02)

### Refactor

* optimized slide label and thumbnail fetching

# 2.0.16 (2023-07-21)

### Fixes

* apps open properly when apps cache discards apps

# 2.0.15 (2023-06-16)

### Refactor

* reverted back to the old loading bar, instead of the loading spinner in apps panel

### Fixes

* loading bar is now visible on selected panel labels

# 2.0.13 (2023-06-09)

### Fixes

* new apps panel handles empty apps lists

### Refactor

* loading new apps is triggered by selecting a case

# 2.0.12 (2023-06-08)

### Refactor

* replaced old logo for new one
* refactored tooltips

# 2.0.7 (2023-04-20)

### Fixes

* new apps panel list will not flash on reopen
* apps in app list will be highlighted when hovering over in new apps panel

# 2.0.5 (2023-03-31)

### Fixes

* use correct api version for tile url

# 2.0.3 (2023-03-24)

### Refactor

* use css for truncate text

# 2.0.1 (2023-03-21)

### Refactor

* slide panel reopens when an app was closed by the user

# 2.0.0 (2023-03-20)

### Feature

* updated to Angular 15

### Refactor

* changed partition for cases panel elements
* uses css in cases panel for truncate

### Fixes

* logos in apps- and new apps panel are properly displayed

# 0.12.2 (2023-02-24)

### Refactor

* added allow popups to iframe sandbox
* set concurrent vendor apps from 3 to 5
* apps list is scrollable

# 0.12.1 (2023-01-20)

### Fixes

* App descriptions expand and collapse correctly
* Side-Nav adjust to the menu correctly

# 0.12.0 (2023-01-06)

### Refactor

* The wbc will always add "allow-scripts" as default settings

# 0.11.6 (2022-12-06)

### Refactor

* Removed `has_frontend` filter in apps effect

# 0.11.3 (2022-11-22)

### Refactor

* Removed DataPersistence Object

# 0.11.0 (2022-11-07)

### Features

* User which is in the wrong organization can now logout

# 0.10.2 (2022-11-07)

### Refactor

* Removed Angular fxLayout

# 0.10.0 (2022-11-01)

### Features

* Added playwright e2e tests

# 0.9.4 (2022-10-27)

### Fixes

* Toggling the slide side navigation menu very fast can't trigger any lifecycle issues anymore

# 0.9.3 (2022-10-13)

### Refactor

* Updated dependencies

# 0.9.2 (2022-10-10)

### Fixes

* prevent to stop normal mouse events after using alt + tab

# 0.9.1 (2022-09-29)

### Refactor

* Change color of selected examination

# 0.9.0 (2022-09-28)

### Features

* Added closing dialog to examination (cases panel)

# 0.8.0 (2022-09-19)

### Features

* Added an extra over zooming layer to every wsi

# 0.7.1

* Updated OAuth client scopes

# 0.7.0 (2022-09-15)

### Refactor

* Examinations are now in the cases panel

### Feature

* Examinations are more abstracted and easier to use

# 0.6.4

* cases that are marked as deleted will no longer be shown in the cases panel

# 0.6.2 (2022-08-25)

### Refactor

* fetch and display before after images in new apps panel from backend
* show version of new apps

# 0.6.1 (2022-08-17)

### Refactor

* Inserted new Workbench Client Logo

# 0.6.0 (2022-08-16)

### Features

* Apps that are already added to the apps panel will be grayed out in new apps panel and with a tool tip note

# 0.5.0 (2022-08-15)

### Features

* Only apps with UI will be listed in the new apps panel

# 0.4.15 (2022-08-03)

### Fixes

* The use of a fallback vendor icon doesn't crash anymore

# 0.4.11 (2022-07-12)

### Refactor

* Using updated Slide Viewer (2022-07-12)
