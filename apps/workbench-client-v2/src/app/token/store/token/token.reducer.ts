import { createReducer, on } from '@ngrx/store';
import { HttpErrorResponse } from '@angular/common/http';
import * as TokenActions from './token.actions';
import { createEntityAdapter, EntityAdapter, EntityState } from '@ngrx/entity';
import { TokenEntity } from '@token/store/token/token.models';


export const TOKEN_FEATURE_KEY = 'token';

export interface State extends EntityState<TokenEntity> {
  error?: HttpErrorResponse;
}

export const accessTokenAdapter: EntityAdapter<TokenEntity> = createEntityAdapter<TokenEntity>({
  selectId: (accessToken) => accessToken.appId
});

export const initialState: State = accessTokenAdapter.getInitialState();


export const reducer = createReducer(
  initialState,
  on(TokenActions.loadTokenReady, (state, { appId }): State =>
    accessTokenAdapter.upsertOne({ appId, loaded: false }, {
      ...state
    })
  ),
  on(TokenActions.loadTokenSuccess, (state, { accessToken }): State =>
    accessTokenAdapter.upsertOne(accessToken, {
      ...state
    })
  ),
  on(TokenActions.loadTokenFailure, (state, { appId, error }): State =>
    accessTokenAdapter.upsertOne({ appId, loaded: true, error }, {
      ...state
    })
  ),
  on(TokenActions.deleteToken, (state, { appId }): State =>
    accessTokenAdapter.removeOne(appId, {
      ...state
    })
  ),
  on(TokenActions.clearTokens, (state): State =>
    accessTokenAdapter.removeAll({
      ...state,
      ...initialState,
    })
  ),
);

