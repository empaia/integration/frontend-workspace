import { createAction, props } from '@ngrx/store';
import { HttpErrorResponse } from '@angular/common/http';
import { TokenEntity } from '@token/store/token/token.models';

export const loadSelectedAppToken = createAction(
  '[Token] Load Selected App Token',
);

export const loadAppToken = createAction(
  '[Token] Load App Token',
  props<{ appId: string }>()
);

export const loadTokenReady = createAction(
  '[Token] Load Token Ready',
  props<{ examinationId: string, appId: string }>()
);

export const loadTokenSuccess = createAction(
  '[Token] Load Token Success',
  props<{ accessToken: TokenEntity }>()
);

export const loadTokenFailure = createAction(
  '[Token] Load Token Failure',
  props<{ appId: string, error: HttpErrorResponse }>()
);

export const deleteToken = createAction(
  '[Token] Delete Token',
  props<{ appId: string }>()
);

export const clearTokens = createAction(
  '[Token] Clear Tokens',
);
