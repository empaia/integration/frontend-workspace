import { Injectable } from '@angular/core';
import { Actions, concatLatestFrom, createEffect, ofType } from '@ngrx/effects';
import * as TokenActions from './token.actions';
import * as ExaminationsSelectors from '@examinations/store/examinations/examinations.selectors';
import * as AppsSelectors from '@apps/store/apps/apps.selectors';
import { map, retry } from 'rxjs/operators';
import { Token, TOKEN_TYPE } from 'vendor-app-integration';
import { Store } from '@ngrx/store';
import { fetch } from '@ngrx/router-store/data-persistence';
import { WbcV2ExaminationsPanelService } from 'empaia-api-lib';
import { DEFAULT_TOKEN_RETRIES, TokenEntity } from '@token/store/token/token.models';
import { State } from './token.reducer';

@Injectable()
export class TokenEffects {
  prepareSelectedAppToken$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(TokenActions.loadSelectedAppToken),
      concatLatestFrom(() => [
        this.store.select(ExaminationsSelectors.selectSelectedId),
        this.store.select(AppsSelectors.selectSelectedId),
      ]),
      map(([_action, examinationId, appId]) =>
        TokenActions.loadTokenReady({
          examinationId: examinationId as string,
          appId: appId as string,
        })
      )
    );
  });

  prepareAppToken$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(TokenActions.loadAppToken),
      map((action) => action.appId),
      concatLatestFrom(() =>
        this.store.select(ExaminationsSelectors.selectSelectedId)
      ),
      map(([appId, examinationId]) =>
        TokenActions.loadTokenReady({
          examinationId: examinationId as string,
          appId,
        })
      )
    );
  });

  loadToken$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(TokenActions.loadTokenReady),
      fetch({
        run: (
          action: ReturnType<typeof TokenActions.loadTokenReady>,
          _state: State
        ) => {
          return this.examinationsPanelService
            .examinationsExaminationIdAppsAppIdScopePut({
              examination_id: action.examinationId,
              app_id: action.appId,
            })
            .pipe(
              retry(DEFAULT_TOKEN_RETRIES),
              map((response) => response.access_token),
              map((accessToken) => {
                const token: Token = {
                  value: accessToken,
                  type: TOKEN_TYPE,
                };
                return token;
              }),
              map((token) => {
                const accessToken: TokenEntity = {
                  appId: action.appId,
                  loaded: true,
                  token,
                };
                return accessToken;
              }),
              map((accessToken) =>
                TokenActions.loadTokenSuccess({ accessToken })
              )
            );
        },
        onError: (
          action: ReturnType<typeof TokenActions.loadTokenReady>,
          error
        ) => {
          return TokenActions.loadTokenFailure({ appId: action.appId, error });
        },
      })
    );
  });

  constructor(
    private readonly actions$: Actions,
    private readonly store: Store,
    private readonly examinationsPanelService: WbcV2ExaminationsPanelService
  ) {}
}
