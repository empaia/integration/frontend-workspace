import { Injectable } from '@angular/core';
import { Actions, concatLatestFrom, createEffect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import * as ScopeActions from './scope.actions';
import * as AppsSelectors from '@apps/store/apps/apps.selectors';
import * as ExaminationsSelectors from '@examinations/store/examinations/examinations.selectors';
import { distinctUntilChanged, filter, map, retry } from 'rxjs/operators';
import { Scope, SCOPE_TYPE } from 'vendor-app-integration';
import { fetch } from '@ngrx/router-store/data-persistence';
import { WbcV2ExaminationsPanelService } from 'empaia-api-lib';
import { DEFAULT_SCOPE_RETRIES, ScopeEntity } from '@scope/store/scope/scope.models';
import { State } from './scope.reducer';

@Injectable()
export class ScopeEffects {
  prepareSelectedAppScope$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ScopeActions.loadSelectedAppScope),
      concatLatestFrom(() => [
        this.store.select(AppsSelectors.selectSelectedId),
        this.store.select(ExaminationsSelectors.selectSelectedId),
      ]),
      filter(([, appId, examId]) => !!appId && !!examId),
      distinctUntilChanged(
        (prev, curr) => prev[1] === curr[1] && prev[2] === curr[2]
      ),
      map(([, appId, examinationId]) =>
        ScopeActions.loadScopeReady({
          appId: appId as string,
          examinationId: examinationId as string,
        })
      )
    );
  });

  prepareAppScope$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ScopeActions.loadAppScope),
      map((action) => action.appId),
      concatLatestFrom(() =>
        this.store.select(ExaminationsSelectors.selectSelectedId)
      ),
      map(([appId, examinationId]) =>
        ScopeActions.loadScopeReady({
          examinationId: examinationId as string,
          appId,
        })
      )
    );
  });

  loadScope$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ScopeActions.loadScopeReady),
      fetch({
        run: (
          action: ReturnType<typeof ScopeActions.loadScopeReady>,
          _state: State
        ) => {
          return this.examinationsPanelService
            .examinationsExaminationIdAppsAppIdScopePut({
              examination_id: action.examinationId,
              app_id: action.appId,
            })
            .pipe(
              retry(DEFAULT_SCOPE_RETRIES),
              map((response) => response.scope_id),
              map((scopeId) => {
                const scope: Scope = {
                  id: scopeId,
                  type: SCOPE_TYPE,
                };
                return scope;
              }),
              map((scope) => {
                const scopeEntity: ScopeEntity = {
                  appId: action.appId,
                  loaded: true,
                  scope,
                };
                return scopeEntity;
              }),
              map((scope) => ScopeActions.loadScopeSuccess({ scope }))
            );
        },
        onError: (
          action: ReturnType<typeof ScopeActions.loadScopeReady>,
          error
        ) => {
          return ScopeActions.loadScopeFailure({ appId: action.appId, error });
        },
      })
    );
  });

  constructor(
    private readonly actions$: Actions,
    private readonly store: Store,
    private readonly examinationsPanelService: WbcV2ExaminationsPanelService
  ) {}
}
