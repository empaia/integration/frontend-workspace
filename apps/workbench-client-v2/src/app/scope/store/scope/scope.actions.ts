import { createAction, props } from '@ngrx/store';
import { HttpErrorResponse } from '@angular/common/http';
import { ScopeEntity } from '@scope/store/scope/scope.models';


export const loadSelectedAppScope = createAction(
  '[Scope] Load Selected App Scope',
);

export const loadAppScope = createAction(
  '[Scope] Load App Scope',
  props<{ appId: string }>()
);

export const loadScopeReady = createAction(
  '[Scope] Load Scope Ready',
  props<{ examinationId: string, appId: string }>()
);

export const loadScopeSuccess = createAction(
  '[Scope] Load Scope',
  props<{ scope: ScopeEntity }>()
);

export const loadScopeFailure = createAction(
  '[Scope] Load Scope Failure',
  props<{ appId: string, error: HttpErrorResponse }>()
);

export const deleteScope = createAction(
  '[Scope] Delete Scope',
  props<{ appId: string }>()
);

export const clearScopes = createAction(
  '[Scope] Clear Scopes',
);
