import { Scope } from 'vendor-app-integration';
import { HttpErrorResponse } from '@angular/common/http';

export interface ScopeEntity {
  appId: string;
  loaded: boolean;
  scope?: Scope;
  error?: HttpErrorResponse;
}

export const DEFAULT_SCOPE_RETRIES = 3;
