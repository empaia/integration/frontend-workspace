import { createSelector } from '@ngrx/store';
import {
  State as ModuleState,
  selectScopeFeatureState,
} from '../scope-feature.state';
import { scopeAdapter } from '@scope/store/scope/scope.reducer';

const {
  selectAll,
  selectEntities,
} = scopeAdapter.getSelectors();

export const selectScopeState = createSelector(
  selectScopeFeatureState,
  (state: ModuleState) => state.scope
);

export const selectAllScopes = createSelector(
  selectScopeState,
  selectAll
);

export const selectScopeEntities = createSelector(
  selectScopeState,
  selectEntities
);

export const selectScope = (appId: string) => createSelector(
  selectScopeEntities,
  (entities) => entities[appId]?.scope
);

export const selectScopeLoaded = (appId: string) => createSelector(
  selectScopeEntities,
  (entities) => entities[appId]?.loaded
);

export const selectAllScopesLoaded = createSelector(
  selectAllScopes,
  (scopes) => scopes.every(scope => scope.loaded)
);

export const selectScopeError = (appId: string) => createSelector(
  selectScopeEntities,
  (entities) => entities[appId]?.error
);

export const selectAllScopeErrors = createSelector(
  selectAllScopes,
  (scopes) => scopes.map(scope => scope.error)
);
