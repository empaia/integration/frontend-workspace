import { HashLocationStrategy, LocationStrategy } from '@angular/common';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { EffectsModule } from '@ngrx/effects';
import { MetaReducer, StoreModule } from '@ngrx/store';
import { storeFreeze } from 'ngrx-store-freeze';

import { environment } from '@env/environment';
import { AppComponent } from '@core/containers/app/app.component';
import { CoreModule } from '@core/core.module';
import { MaterialModule } from '@material/material.module';
import { NoAuthInterceptor } from '@core/services/interceptors/no-auth.interceptor';
import { WbsWbcApiV2Module } from 'empaia-api-lib';
import { AppRouterModule } from '@router/app-router.module';
import { ScopeModule } from '@scope/scope.module';
import { TokenModule } from '@token/token.module';
import { WbsUrlModule } from '@wbsUrl/wbs-url.module';
import { OAuthModule } from 'angular-oauth2-oidc';
import { EmpaiaAuthInterceptor } from '@core/services/interceptors/empaia-auth.interceptor';
import { MAT_TOOLTIP_DEFAULT_OPTIONS } from '@angular/material/tooltip';
import { customTooltipDefaultOptions } from '@material/models/custom-options.models';

// adding url extension to wbs url so the wbc and the loaded apps
// must add the version of the wbs its self
// they must decide by its self which version to use
export const WBS_API_EXTENSION = '/v2';

export const BLANK_PROFILE_IMAGE = './assets/empty-user-avatar.png';

const wbsServeApiUrl = environment.wbsServerApiUrl?.endsWith('/')
  ? environment.wbsServerApiUrl.slice(0, environment.wbsServerApiUrl.length - 1)
  : environment.wbsServerApiUrl;
const idpUrl = environment.idpUrl?.endsWith('/')
  ? environment.idpUrl.slice(0, environment.idpUrl.length - 1)
  : environment.idpUrl;
const idpUserUrl = environment.idpUserUrl?.endsWith('/')
  ? environment.idpUserUrl.slice(0, environment.idpUserUrl.length - 1)
  : environment.idpUserUrl;
const solutionStoreUrl = environment.solutionStoreUrl?.endsWith('/')
  ? environment.solutionStoreUrl.slice(0, environment.solutionStoreUrl.length - 1)
  : environment.solutionStoreUrl;
const aaaApiUrl = environment.aaaApiUrl?.endsWith('/')
  ? environment.aaaApiUrl.slice(0, environment.aaaApiUrl.length - 1)
  : environment.aaaApiUrl;

export const metaReducers: MetaReducer<object>[] = !environment.production
  ? [storeFreeze]
  : [];

@NgModule({
  declarations: [],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRouterModule,
    CoreModule,
    MaterialModule,
    ScopeModule,
    TokenModule,
    WbsUrlModule,
    HttpClientModule,
    StoreModule.forRoot({},
      {
        metaReducers,
        runtimeChecks: {
          strictActionImmutability: true,
          strictStateImmutability: true
        }
      }
    ),
    !environment.production ? StoreDevtoolsModule.instrument({
      name: 'Workbench Client v2 Store',
      connectInZone: true}) : [],
    EffectsModule.forRoot([]),
    WbsWbcApiV2Module.forRoot({ rootUrl: wbsServeApiUrl + WBS_API_EXTENSION }),
    OAuthModule.forRoot({
      resourceServer: {
        allowedUrls: [
          wbsServeApiUrl,
          idpUserUrl,
          aaaApiUrl
        ],
        sendAccessToken: true,
      }
    }),
  ],
  providers: [
    { provide: LocationStrategy, useClass: HashLocationStrategy },
    { provide: 'WBS_SERVER_API_URL', useValue: wbsServeApiUrl },
    { provide: 'WBS_USER_ID', useValue: environment.wbsUserId },
    { provide: 'AUTHENTICATION_ON', useValue: environment.authenticationOn === 'true' },
    { provide: 'IDP_URL', useValue: idpUrl },
    { provide: 'IDP_USER_URL', useValue: idpUserUrl },
    { provide: 'SOLUTION_STORE_URL', useValue: solutionStoreUrl },
    { provide: 'AAA_API_URL', useValue: aaaApiUrl },
    { provide: 'API_VERSION', useValue: WBS_API_EXTENSION },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: environment.authenticationOn === 'true' ? EmpaiaAuthInterceptor : NoAuthInterceptor,
      multi: true,
    },
    { provide: MAT_TOOLTIP_DEFAULT_OPTIONS, useValue: customTooltipDefaultOptions }
  ],
  bootstrap: [
    AppComponent
  ],
})
export class AppModule {}
