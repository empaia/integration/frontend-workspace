import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MenuComponent } from './containers/menu/menu.component';
import { MaterialModule } from '@material/material.module';
import { StoreModule } from '@ngrx/store';
import { MENU_MODULE_FEATURE_KEY, reducers } from '@menu/store/menu-feature.state';
import { EffectsModule } from '@ngrx/effects';
import { MenuEffects } from './store';
import { CasesModule } from '@cases/cases.module';
import { ExaminationsModule } from '@examinations/examinations.module';
import { AppsModule } from '@apps/apps.module';
import { LetDirective, PushPipe } from '@ngrx/component';
import { SharedModule } from '@shared/shared.module';

@NgModule({
  declarations: [
    MenuComponent,
  ],
  imports: [
    CommonModule,
    MaterialModule,
    StoreModule.forFeature(
      MENU_MODULE_FEATURE_KEY,
      reducers,
    ),
    EffectsModule.forFeature([
      MenuEffects,
    ]),
    CasesModule,
    ExaminationsModule,
    AppsModule,
    LetDirective, PushPipe,
    SharedModule,
  ],
  exports: [MenuComponent]
})
export class MenuModule { }
