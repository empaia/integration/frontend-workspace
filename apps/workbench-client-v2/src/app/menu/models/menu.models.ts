/**
 * Interface for the 'Menu' data
 */

export type MenuItem =
 | 'Cases'
 | 'Apps'
 | 'Slides';

export enum MenuState {
  HIDDEN = 0,
  MIDI = 1,
  MAX = 2,
}

export const MenuIcons: Record<MenuItem, string> = {
  Cases: 'layers',
  Apps: 'settings_suggest',
  Slides: 'filter',
};

export interface MenuEntity {
  readonly id: MenuItem,
  selected: boolean,
  contentState: MenuState,
  labelState: MenuState,
  readonly icon: string,
}

// Initial Menu state definition
export const MENU: Record<MenuItem, MenuEntity> = {
  Cases: {
    id: 'Cases',
    contentState: MenuState.HIDDEN,
    labelState: MenuState.HIDDEN,
    selected: true,
    icon: MenuIcons.Cases,
  },
  Apps: {
    id: 'Apps',
    contentState: MenuState.HIDDEN,
    labelState: MenuState.HIDDEN,
    selected: false,
    icon: MenuIcons.Apps,
  },
  Slides: {
    id: 'Slides',
    contentState: MenuState.HIDDEN,
    labelState: MenuState.HIDDEN,
    selected: false,
    icon: MenuIcons.Slides,
  },
};

export const MINIMIZED_SIZE = 0;
export const MIDI_SIZE = 20; // vw
export const MAX_SIZE = 100; // vw
export const MIN_SIZE = 295; // px
export const MENU_BUTTON_WIDTH = 64; // px
