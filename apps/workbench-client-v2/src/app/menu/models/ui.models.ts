import { HttpErrorResponse } from '@angular/common/http';

export type IdSelection = { id: string };

export type SelectionError =
  | 'NO_CASE_SELECTION'
  | 'NO_EXAMINATION_SELECTION'
  | 'NO_APP_SELECTION';

export type ErrorSnackbarData = { titel: string, error: HttpErrorResponse, };

export interface HttpResponseError extends HttpErrorResponse {
  error: {
    detail: string;
  }
}

const LOAD_ERROR = `Error fetching`;
export const CASES_LOAD_ERROR = `${LOAD_ERROR} Cases!`;
export const SLIDES_LOAD_ERROR = `${LOAD_ERROR} Slides!`;
export const EXAMINATIONS_LOAD_ERROR = `${LOAD_ERROR} Examinations!`;
export const APPS_LOAD_ERROR = `${LOAD_ERROR} Apps!`;
export const NEW_APPS_LOAD_ERROR = `${LOAD_ERROR} new Apps list!`;
export const SLIDE_LABEL_LOAD_ERROR = `${LOAD_ERROR} Slide label!`;
export const SLIDE_THUMBNAIL_LOAD_ERROR = `${LOAD_ERROR} Slide Thumbnail`;

export const CLOSE_EXAMINATION_ERROR = `Closing examination failed!`;
