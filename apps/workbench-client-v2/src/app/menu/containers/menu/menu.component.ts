import { AfterViewInit, ChangeDetectionStrategy, Component, HostListener, OnDestroy, ViewChild } from '@angular/core';
import { MatSidenav } from '@angular/material/sidenav';
import { MenuEntity, MenuItem } from '@menu/models/menu.models';
import { Store } from '@ngrx/store';
import { Observable, Subject } from 'rxjs';
import { MenuActions, MenuSelectors } from '@menu/store';
import { Case, CasesSelectors } from '@cases/store';
import { Examination, ExaminationsSelectors } from '@examinations/store';
import { takeUntil } from 'rxjs/operators';
import { ResizeEvent } from 'angular-resizable-element';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MenuComponent implements AfterViewInit, OnDestroy {
  @ViewChild('drawer') private menuDrawer!: MatSidenav;
  private destroy$ = new Subject();

  public casesMenu$: Observable<MenuEntity>;
  public appsMenu$: Observable<MenuEntity>;

  public caseSelected$: Observable<Case | undefined>;
  public casesLoaded$: Observable<boolean>;
  public examinationSelected$: Observable<Examination | undefined>;

  private initialMaxWidth = 100; // vw
  public initialMaxWidthStyle = {
    width: `${this.initialMaxWidth}vw`,
  };

  public sidenavWidth$: Observable<number>;

  constructor(private store: Store) {
    this.casesMenu$ = this.store.select(MenuSelectors.selectCasesMenu);
    this.appsMenu$ = this.store.select(MenuSelectors.selectAppsMenu);
    this.sidenavWidth$ = this.store.select(MenuSelectors.selectSideNavSize);

    this.caseSelected$ = this.store.select(CasesSelectors.selectSelectedCase);
    this.casesLoaded$ = this.store.select(CasesSelectors.selectCasesLoaded);
    this.examinationSelected$ = this.store.select(ExaminationsSelectors.selectSelected);
  }

  ngAfterViewInit(): void {
    this.store
      .select(MenuSelectors.selectMinimizeAll)
      .pipe(takeUntil(this.destroy$))
      .subscribe(minimized => {
        if (this.menuDrawer && minimized !== undefined) {
          this.menuDrawer.toggle(!minimized);
        }
      });
  }

  ngOnDestroy(): void {
    this.destroy$.next(null);
  }

  public toggle(): void {
    this.menuDrawer.toggle();
    this.store.dispatch(MenuActions.toggleMenu({ minimizeAll: !this.menuDrawer.opened }));
  }

  public railMenuItemRoute(selected: boolean, value: MenuItem): void {
    if (this.menuDrawer && !this.menuDrawer.opened || this.menuDrawer && selected) {
      this.toggle();
    }
    if (value === 'Cases') {
      this.store.dispatch(MenuActions.openMenuMax({ menuItem: value }));
    } else {
      this.store.dispatch(MenuActions.openMenuMidi({ menuItem: value }));
    }
  }

  @HostListener('window:resize', ['$event'])
  public onResize(_event: ResizeEvent): void {
    // Dispatch resize action to inform the menu that the window viewport
    // was changed
    this.store.dispatch(MenuActions.resizeMenu());
  }
}

