import { Action, createReducer } from '@ngrx/store';

import * as MenuActions from './menu.actions';
import {
  MENU,
  MenuEntity,
  MenuItem,
  MenuState,
  MIN_SIZE,
  MIDI_SIZE,
  MAX_SIZE,
  MINIMIZED_SIZE,
  MENU_BUTTON_WIDTH,
} from '@menu/models/menu.models';
import { immerOn } from 'ngrx-immer/store';
import {
  calculateMaxSize,
  convertPxToVw,
} from '@helper/dimensional-style-calculation';

export const MENU_FEATURE_KEY = 'menu';

export interface State {
  menu: Array<MenuEntity>;
  minimizeAll: boolean;
  slideNavSize: number,
}

export const initialState: State = {
  menu: Object.values(MENU),
  minimizeAll: false,
  slideNavSize: MIDI_SIZE,
};

const menuReducer = createReducer(
  initialState,
  immerOn(MenuActions.openMenuMidi, (state, { menuItem }) => {
    const index = getMenuItemIndex(menuItem, state.menu);
    selectAndOpenItem(index, MenuState.MIDI, state.menu);
    deselectOtherMenus(menuItem, state.menu);
    state.slideNavSize = Math.max(MIDI_SIZE, convertPxToVw(MIN_SIZE));
  }),
  immerOn(MenuActions.openMenuMax, (state, { menuItem }) => {
    const index = getMenuItemIndex(menuItem, state.menu);
    selectAndOpenItem(index, MenuState.MAX, state.menu);
    deselectOtherMenus(menuItem, state.menu);
    state.slideNavSize = calculateMaxSize(MAX_SIZE, [convertPxToVw(MENU_BUTTON_WIDTH)]);
  }),
  immerOn(MenuActions.toggleMenu, (state, { minimizeAll }) => {
    state.minimizeAll = minimizeAll;
  }),
  immerOn(MenuActions.resizeMenu, (state) => {
    const item = getSelectedMenuItem(state.menu);
    state.slideNavSize = getMenuSize(item);
  }),
);

function getMenuSize(item: MenuEntity | undefined): number {
  if (item) {
    switch(item.contentState) {
      case MenuState.HIDDEN: return MINIMIZED_SIZE;
      case MenuState.MIDI: return Math.max(MIDI_SIZE, convertPxToVw(MIN_SIZE));
      case MenuState.MAX: return calculateMaxSize(MAX_SIZE, [convertPxToVw(MENU_BUTTON_WIDTH)]);
    }
  } else {
    return MINIMIZED_SIZE;
  }
}

function getMenuItemIndex(id: MenuItem, menu: Array<MenuEntity>): number {
  return menu.findIndex(i => i.id === id);
}

function getSelectedMenuItem(menu: Array<MenuEntity>): MenuEntity | undefined {
  return menu.find(item => item.selected);
}

function selectAndOpenItem(index: number, state: MenuState, menu: Array<MenuEntity>) {
  menu[index].selected = true;
  menu[index].labelState = state;
  menu[index].contentState = state;
}

function deselectOtherMenus(id: MenuItem, menu: Array<MenuEntity>) {
  const others = menu.filter(i => i.id !== id);
  for (const item of others) {
    item.selected = false;
    item.labelState = MenuState.MIDI;
    item.contentState = MenuState.HIDDEN;
  }
}

export function reducer(state: State | undefined, action: Action) {
  return menuReducer(state, action);
}
