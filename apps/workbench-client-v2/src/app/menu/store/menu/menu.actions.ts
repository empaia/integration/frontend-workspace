import { createAction, props } from '@ngrx/store';
import { MenuItem } from '@menu/models/menu.models';

export const openMenuMidi = createAction(
  '[Menu] Open Menu Midi',
  props<{ menuItem: MenuItem }>()
);

export const openMenuMax = createAction(
  '[Menu] Open Menu Max',
  props<{ menuItem: MenuItem }>()
);

export const toggleMenu = createAction(
  '[Menu] Toggle Menu',
  props<{ minimizeAll: boolean }>()
);

export const resizeMenu = createAction(
  '[Menu] Resize Menu',
);

export const clickMenuButton = createAction(
  '[Menu] Click Menu Button',
  props<{ menuItem: MenuItem }>()
);
