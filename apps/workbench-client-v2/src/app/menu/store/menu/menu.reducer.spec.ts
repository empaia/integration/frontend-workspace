/* eslint-disable @typescript-eslint/no-explicit-any */
import { initialState, reducer } from './menu.reducer';

describe('Menu Reducer', () => {
  //beforeEach(() => {});

  describe('valid Menu actions', () => {
    it('loadMenuSuccess should return set the list of known Menu', () => {
      /*const menu = [
        createMenuEntity('PRODUCT-AAA'),
        createMenuEntity('PRODUCT-zzz'),
      ];
      const action = MenuActions.loadMenuSuccess({ menu });

      const result: State = reducer(initialState, action);

      expect(result.loaded).toBe(true);
      expect(result.ids.length).toBe(2);*/

      // todo
      expect(true).toBeTruthy();
    });
  });

  describe('unknown action', () => {
    it('should return the previous state', () => {
      const action = {} as any;

      const result = reducer(initialState, action);

      expect(result).toBe(initialState);
    });
  });
});
