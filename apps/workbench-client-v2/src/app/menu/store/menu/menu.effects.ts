import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { map } from 'rxjs/operators';
import * as MenuActions from './menu.actions';

@Injectable()
export class MenuEffects {

  clickMenuButton$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(MenuActions.clickMenuButton),
      map(action => action.menuItem),
      map(menuItem =>
        menuItem === 'Cases'
          ? MenuActions.openMenuMax({ menuItem })
          : MenuActions.openMenuMidi({ menuItem })
      )
    );
  });

  constructor(
    private actions$: Actions,
  ) {}
}
