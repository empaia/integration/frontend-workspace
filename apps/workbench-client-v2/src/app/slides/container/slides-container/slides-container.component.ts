import { ChangeDetectionStrategy, Component, ViewChild } from '@angular/core';
import { Observable } from 'rxjs';
import { MenuEntity, MenuState } from '@menu/models/menu.models';
import {
  SlideEntity,
  SlideImage,
  SlidesMenuSelectors,
  SlidesImagesSelectors,
  SlidesSelectors,
  SlidesMenuActions,
  SlidesActions,
} from '@slides/store';
import { Store } from '@ngrx/store';
import { IdSelection } from '@menu/models/ui.models';
import { UiConfig } from 'slide-viewer';
import { MenuButtonEventModel } from '@shared/components/menu-button/menu-button.models';
import { MatSidenav } from '@angular/material/sidenav';
import { Dictionary } from '@ngrx/entity';
import { WbcV2Slide } from 'empaia-api-lib';

@Component({
  selector: 'app-slides-container',
  templateUrl: './slides-container.component.html',
  styleUrls: ['./slides-container.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SlidesContainerComponent {
  @ViewChild('drawer') menuDrawer!: MatSidenav;
  public open$: Observable<boolean>;
  public slidesMenu$: Observable<MenuEntity>;
  public showMenu$: Observable<boolean>;
  public slides$: Observable<WbcV2Slide[]>;
  public slideId$: Observable<string | undefined>;
  public slide$: Observable<SlideEntity | undefined>;
  public loaded$: Observable<boolean>;
  public slideImages$: Observable<Dictionary<SlideImage>>;

  public mainMenuChange$: Observable<number>;
  public slideMenuChange$: Observable<number>;

  public readonly MENU_STATE = MenuState;

  public uiConfig: UiConfig = {
    showAnnotationBar: false,
    autoSetAnnotationTitle: false,
    renderHideNavigationButton: true
  };

  constructor(private store: Store) {
    this.open$ = this.store.select(SlidesMenuSelectors.selectSlidesMenuPanelOpen);
    this.slidesMenu$ = this.store.select(SlidesMenuSelectors.selectSlidesMenu);
    this.showMenu$ = this.store.select(SlidesMenuSelectors.selectSlidesMenuVisibilityState);
    this.slides$ = this.store.select(SlidesSelectors.selectAllSlidesDataView);
    this.slideId$ = this.store.select(SlidesSelectors.selectSelectedSlideId);
    this.slide$ = this.store.select(SlidesSelectors.selectSelectedSlide);
    this.loaded$ = this.store.select(SlidesSelectors.selectSlidesLoaded);
    this.slideImages$ = this.store.select(SlidesImagesSelectors.selectSlidesImagesEntities);
    this.mainMenuChange$ = this.store.select(SlidesMenuSelectors.selectMenuSize);
    this.slideMenuChange$ = this.store.select(SlidesMenuSelectors.selectSlideSideNavSize);
  }

  public toggleTrayView(event: MouseEvent): void {
    this.store.dispatch(SlidesMenuActions.openMenuMax());
    event.stopImmediatePropagation();
  }

  public selectSlide(selected: IdSelection): void {
    const slideId = selected.id;
    this.store.dispatch(SlidesActions.selectSlide({ slideId }));
  }

  public hideSlideMenu(_event: MenuButtonEventModel): void {
    if (this.menuDrawer) {
      this.toggle();
    }
  }

  public onDoubleClick(): void {
    this.store.dispatch(SlidesMenuActions.toggleAllMenus());
  }

  public toggle(): void {
    this.menuDrawer.toggle();
    this.store.dispatch(SlidesMenuActions.toggleMenu());
  }
}
