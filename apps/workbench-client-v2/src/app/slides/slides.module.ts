import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';
import { SLIDES_MODULE_FEATURE_KEY, reducers } from '@slides/store/slides-feature.state';
import { EffectsModule } from '@ngrx/effects';
import { SlidesEffects, SlidesImagesEffects, SlidesMenuEffects } from '@slides/store';
import { SlideTrayItemComponent } from './components/slide-tray-item/slide-tray-item.component';
import { MaterialModule } from '@material/material.module';
import { SharedModule } from '@shared/shared.module';
import { SlideImageItemComponent } from './components/slide-image-item/slide-image-item.component';
import { SlidesTrayViewComponent } from './components/slides-tray-view/slides-tray-view.component';
import { SlideItemComponent } from './components/slide-item/slide-item.component';
import { SlidesComponent } from './components/slides/slides.component';
import { SlidesLabelComponent } from './components/slides-label/slides-label.component';
import { SlidesContainerComponent } from './container/slides-container/slides-container.component';
import { SlideViewerModule } from 'slide-viewer';
import { LetDirective, PushPipe } from '@ngrx/component';

const COMPONENTS = [
  SlideTrayItemComponent,
  SlideImageItemComponent,
  SlidesTrayViewComponent,
  SlideItemComponent,
  SlidesComponent,
  SlidesLabelComponent,
  SlidesContainerComponent,
];

@NgModule({
  declarations: [
    ...COMPONENTS,
  ],
  imports: [
    CommonModule,
    MaterialModule,
    SharedModule,
    SlideViewerModule.forRoot({ logging: false }),
    StoreModule.forFeature(
      SLIDES_MODULE_FEATURE_KEY,
      reducers,
    ),
    EffectsModule.forFeature([
      SlidesImagesEffects,
      SlidesEffects,
      SlidesMenuEffects,
    ]),
    LetDirective, PushPipe,
  ],
  exports: [
    SlidesContainerComponent
  ]
})
export class SlidesModule { }
