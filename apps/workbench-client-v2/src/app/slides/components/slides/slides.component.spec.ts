import { SlidesComponent } from './slides.component';
import { createComponentFactory, Spectator } from '@ngneat/spectator';
import { MaterialModule } from '@material/material.module';
import { MockComponents } from 'ng-mocks';
import { SlideItemComponent } from '@slides/components/slide-item/slide-item.component';
import {
  MissingSelectionErrorComponent
} from '@shared/components/missing-selection-error/missing-selection-error.component';

describe('SlidesComponent', () => {
  let spectator: Spectator<SlidesComponent>;
  const createComponent = createComponentFactory({
    component: SlidesComponent,
    imports: [
      MaterialModule,
    ],
    declarations: [
      MockComponents(
        SlideItemComponent,
        MissingSelectionErrorComponent,
      )
    ]
  });

  beforeEach(() => spectator = createComponent({
    props: {
      slideEntities: []
    }
  }));

  it('should create', () => {
    expect(spectator.component).toBeTruthy();
  });
});
