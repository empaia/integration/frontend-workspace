import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { SlideImage } from '@slides/store';
import { IdSelection, SelectionError } from '@menu/models/ui.models';
import { MenuItem } from '@menu/models/menu.models';
import { MatSelectionListChange } from '@angular/material/list';
import { Dictionary } from '@ngrx/entity';
import { WbcV2Slide } from 'empaia-api-lib';

@Component({
  selector: 'app-slides',
  templateUrl: './slides.component.html',
  styleUrls: ['./slides.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SlidesComponent {
  @Input() public slideEntities!: WbcV2Slide[];
  @Input() public slideImages!: Dictionary<SlideImage>;
  @Input() public selectedSlideId!: string;
  @Input() public selectionMissing!: SelectionError;
  @Input() public contentLoaded!: boolean;

  @Output() public slideSelected = new EventEmitter<IdSelection>();
  @Output() public errorClicked = new EventEmitter<MenuItem>();

  public selectionListChanged(event: MatSelectionListChange): void {
    const id = event.source.selectedOptions.selected[0].value;
    this.slideSelected.emit({ id });
  }

  public onMissingSelectionErrorClick(menuItem: MenuItem): void {
    this.errorClicked.emit(menuItem);
  }
}
