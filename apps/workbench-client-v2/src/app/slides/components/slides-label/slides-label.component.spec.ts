import { SlidesLabelComponent } from './slides-label.component';
import { createComponentFactory, Spectator } from '@ngneat/spectator';
import { MaterialModule } from '@material/material.module';
import { MockComponents } from 'ng-mocks';
import { CopyToClipboardDirective } from '@shared/directives/copy-to-clipboard.directive';
import { MenuLabelComponent } from '@shared/components/menu-label/menu-label.component';

describe('SlidesLabelComponent', () => {
  let spectator: Spectator<SlidesLabelComponent>;
  const createComponent = createComponentFactory({
    component: SlidesLabelComponent,
    imports: [
      MaterialModule,
    ],
    declarations: [
      CopyToClipboardDirective,
      MockComponents(
        MenuLabelComponent,
      )
    ]
  });

  beforeEach(() => spectator = createComponent());

  it('should create', () => {
    expect(spectator.component).toBeTruthy();
  });
});
