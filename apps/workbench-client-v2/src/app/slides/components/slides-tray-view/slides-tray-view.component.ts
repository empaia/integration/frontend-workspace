import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { SlideImage } from '@slides/store';
import { IdSelection, SelectionError } from '@menu/models/ui.models';
import { Dictionary } from '@ngrx/entity';
import { WbcV2Slide } from 'empaia-api-lib';

@Component({
  selector: 'app-slides-tray-view',
  templateUrl: './slides-tray-view.component.html',
  styleUrls: ['./slides-tray-view.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SlidesTrayViewComponent {
  @Input() public slideEntities!: WbcV2Slide[];
  @Input() public selectedSlideId!: string;
  @Input() public slideImages!: Dictionary<SlideImage>;
  @Input() public selectionMissing!: SelectionError;
  @Input() public contentLoaded!: boolean;

  @Output() public slideSelected = new EventEmitter<IdSelection>();

  public selectionListChanged(id: string): void {
    this.slideSelected.emit({ id });
  }
}
