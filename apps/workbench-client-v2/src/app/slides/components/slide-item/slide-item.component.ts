import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { SlideImage } from '@slides/store';
import { IdSelection } from '@menu/models/ui.models';
import { WbcV2Slide } from 'empaia-api-lib';

@Component({
  selector: 'app-slide-item',
  templateUrl: './slide-item.component.html',
  styleUrls: ['./slide-item.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SlideItemComponent {
  @Input() public slide!: WbcV2Slide;
  @Input() public slideImage!: SlideImage;
  @Input() public selectedSlide!: string;

  @Output() public itemSelected = new EventEmitter<IdSelection>();

  public selected() {
    if (this.slide.deleted === null || !this.slide.deleted) {
      this.itemSelected.emit({ id: this.slide.id });
    }
  }
}
