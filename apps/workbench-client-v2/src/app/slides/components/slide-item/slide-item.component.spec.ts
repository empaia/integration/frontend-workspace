import { SlideItemComponent } from './slide-item.component';
import { createComponentFactory, Spectator } from '@ngneat/spectator';
import { CopyToClipboardDirective } from '@shared/directives/copy-to-clipboard.directive';
import { MockComponents } from 'ng-mocks';
import { SlideImageItemComponent } from '@slides/components/slide-image-item/slide-image-item.component';
import { DictParserPipe } from '@shared/pipes/dict-parser.pipe';
import { MaterialModule } from '@material/material.module';

describe('SlideItemComponent', () => {
  let spectator: Spectator<SlideItemComponent>;
  const createComponent = createComponentFactory({
    component: SlideItemComponent,
    imports: [
      MaterialModule,
    ],
    declarations: [
      DictParserPipe,
      CopyToClipboardDirective,
      MockComponents(
        SlideImageItemComponent,
      )
    ]
  });

  beforeEach(() => spectator = createComponent({
    props: {
      slide: {
        id: 'Slide-ID',
        case_id: 'Case-ID',
        created_at: 0,
        updated_at: 0,
        block: null,
        deleted: null,
        local_id: null,
        mds_url: null,
        stain: null,
        tissue: null,
      }
    }
  }));

  it('should create', () => {
    expect(spectator.component).toBeTruthy();
  });
});
