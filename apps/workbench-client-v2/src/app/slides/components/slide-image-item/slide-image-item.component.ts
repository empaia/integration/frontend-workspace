import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { SlideImage } from '@slides/store';

enum ImageType {
  LABEL = 'label',
  THUMBNAIL = 'thumbnail'
}

@Component({
  selector: 'app-slide-image-item',
  templateUrl: './slide-image-item.component.html',
  styleUrls: ['./slide-image-item.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SlideImageItemComponent {
  @Input() public imageType!: ImageType;
  @Input() public slideImage!: SlideImage;
  @Input() public fallbackImage!: string;

  public readonly IMAGE_TYPES = ImageType;
}
