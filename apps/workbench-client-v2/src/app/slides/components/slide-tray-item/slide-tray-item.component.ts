import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { SlideImage, TagMapping } from '@slides/store';

@Component({
  selector: 'app-slide-tray-item',
  templateUrl: './slide-tray-item.component.html',
  styleUrls: ['./slide-tray-item.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SlideTrayItemComponent {
  @Input() public isTrayLabel!: boolean;
  @Input() public deleted!: boolean;

  @Input() public slideId!: string;
  @Input() public slideImage!: SlideImage;

  @Input() public displayId: string | undefined;
  @Input() public block!: string;
  @Input() public stain!: TagMapping | string;
  @Input() public tissue!: TagMapping | string;
}
