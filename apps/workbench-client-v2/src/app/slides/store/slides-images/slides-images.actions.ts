import { createAction, props } from '@ngrx/store';
import { SlideImage, SlideImageFormat } from '@slides/store/slides-images/slides-images.models';
import { HttpErrorResponse } from '@angular/common/http';

export const loadSlideLabel = createAction(
  '[Slides-Images] Load Slide Label',
  props<{
    slideId: string,
    maxWidth: number,
    maxHeight: number,
    imageFormat?: SlideImageFormat,
    imageQuality?: number,
  }>()
);

export const loadSlideLabelSuccess = createAction(
  '[Slides-Images] Load Slide Label Success',
  props<{ slideImage: SlideImage }>()
);

export const loadSlideLabelFailure = createAction(
  '[Slides-Images] Load Slide Label Failure',
  props<{ slideId: string, error: HttpErrorResponse }>()
);

export const loadManySlidesLabels = createAction(
  '[Slides-Images] Load Many Slides Labels',
  props<{
    slideIds: string[],
    maxWidth: number,
    maxHeight: number,
    imageFormat?: SlideImageFormat,
    imageQuality?: number,
  }>()
);

export const loadSlideThumbnail = createAction(
  '[Slides-Images] Load Slide Thumbnail',
  props<{
    slideId: string,
    maxWidth: number,
    maxHeight: number,
    imageFormat?: SlideImageFormat,
    imageQuality?: number,
  }>()
);

export const loadSlideThumbnailSuccess = createAction(
  '[Slides-Images] Load Slide Thumbnail Success',
  props<{ slideImage: SlideImage }>()
);

export const loadSlideThumbnailFailure = createAction(
  '[Slides-Images] Load Slide Thumbnail Failure',
  props<{ slideId: string, error: HttpErrorResponse }>()
);

export const loadManySlidesThumbnails = createAction(
  '[Slides-Images] Load Many Slides Thumbnails',
  props<{
    slideIds: string[],
    maxWidth: number,
    maxHeight: number,
    imageFormat?: SlideImageFormat,
    imageQuality?: number,
  }>()
);

export const loadManySlidesLabelsAndThumbnails = createAction(
  '[Slides-Images] Load Many Slides Labels And Thumbnails',
  props<{
    slideIds: string[],
    maxWidth: number,
    maxHeight: number,
    imageFormat?: SlideImageFormat,
    imageQuality?: number,
  }>()
);

export const clearImages = createAction(
  '[Slides-Images] Clear Images',
);

export const clearImagesReady = createAction(
  '[Slides-Images] Clear Images Ready',
);
