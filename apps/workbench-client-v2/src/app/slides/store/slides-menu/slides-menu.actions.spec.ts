import * as fromSlideMenu from './slides-menu.actions';

describe('loadSlideMenus', () => {
  it('should return an action', () => {
    expect(fromSlideMenu.showLabel().type).toBe('[SlidesMenu] Show Label');
  });
});
