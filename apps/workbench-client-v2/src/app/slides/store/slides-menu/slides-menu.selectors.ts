import { selectSideNavSize } from '@menu/store/menu/menu.selectors';
import { createSelector } from '@ngrx/store';
import {
  State as ModuleState,
  selectSlidesFeatureState,
} from '../slides-feature.state';

export const selectSlidesMenuState = createSelector(
  selectSlidesFeatureState,
  (state: ModuleState) => state.slidesMenu
);

export const selectSlidesMenu = createSelector(
  selectSlidesMenuState,
  (state) => state.slideMenu
);

export const selectSlidesMenuVisibilityState = createSelector(
  selectSlidesMenuState,
  (state) => state.visible
);

export const selectSlidesMenuPanelOpen = createSelector(
  selectSlidesMenuState,
  (state) => state.panelOpened
);

export const selectSlideSideNavSize = createSelector(
  selectSlidesMenuState,
  (state) => state.visible ? state.slideMenuSize : 0
);

export const selectMenuSize = createSelector(
  selectSideNavSize,
  selectSlideSideNavSize,
  (main, slide) => main + slide
);
