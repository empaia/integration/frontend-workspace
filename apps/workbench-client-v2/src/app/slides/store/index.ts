import * as SlidesImagesActions from './slides-images/slides-images.actions';
import * as SlidesImagesFeature from './slides-images/slides-images.reducer';
import * as SlidesImagesSelectors from './slides-images/slides-images.selectors';
export * from './slides-images/slides-images.models';
export * from './slides-images/slides-images.effects';

export { SlidesImagesActions, SlidesImagesFeature, SlidesImagesSelectors };

import * as SlidesActions from './slides/slides.actions';
import * as SlidesFeature from './slides/slides.reducer';
import * as SlidesSelectors from './slides/slides.selectors';
export * from './slides/slides.models';
export * from './slides/slides.effects';

export { SlidesActions, SlidesFeature, SlidesSelectors };

import * as SlidesMenuActions from './slides-menu/slides-menu.actions';
import * as SlidesMenuFeature from './slides-menu/slides-menu.reducer';
import * as SlidesMenuSelectors from './slides-menu/slides-menu.selectors';
export * from './slides-menu/slides-menu.effects';

export { SlidesMenuActions, SlidesMenuFeature, SlidesMenuSelectors };
