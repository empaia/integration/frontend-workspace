import { createSelector } from '@ngrx/store';
import {
  State as ModuleState,
  selectSlidesFeatureState,
} from '../slides-feature.state';
import { slidesAdapter } from '@slides/store/slides/slides.reducer';

const {
  selectAll,
  selectEntities,
} = slidesAdapter.getSelectors();

export const selectSlidesState = createSelector(
  selectSlidesFeatureState,
  (state: ModuleState) => state.slides
);

export const selectSlidesLoaded = createSelector(
  selectSlidesState,
  (state) => state.loaded
);

export const selectSlidesError = createSelector(
  selectSlidesState,
  (state) => state.error
);

export const selectAllSlides = createSelector(
  selectSlidesState,
  selectAll
);

export const selectSlidesEntities = createSelector(
  selectSlidesState,
  (state) => selectEntities(state)
);

export const selectAllSlidesDataView = createSelector(
  selectAllSlides,
  (slides) => slides.map(slide => slide.dataView)
);

export const selectSelectedSlideId = createSelector(
  selectSlidesState,
  (state) => state.selected
);

export const selectSelectedSlide = createSelector(
  selectSlidesEntities,
  selectSelectedSlideId,
  (entities, slideId) => slideId ? entities[slideId] : undefined
);
