import {
  WbcV2Slide as SlideView,
  WbcV2SlideInfo,
  WbcV2SlideLevel,
  WbcV2SlideColor,
  WbcV2TagMapping,
  WbcV2SlideList
} from 'empaia-api-lib';
import { Slide } from 'slide-viewer';

export type SlideEntity = {
  id: string;
  dataView: SlideView;
  imageView?: Slide;
  disabled: boolean;
};

export const SLIDES_POLLING_PERIOD = 30000;

export type SlideInfo = WbcV2SlideInfo;
export type SlideLevel = WbcV2SlideLevel;
export type SlideColor = WbcV2SlideColor;
export type TagMapping = WbcV2TagMapping;
export type SlideList = WbcV2SlideList;
