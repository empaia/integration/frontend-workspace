import * as fromSlides from './slides.actions';

describe('loadSlides', () => {
  it('should return an action', () => {
    expect(fromSlides.loadSlides({
      caseId: 'CASE-ID'
    }).type).toBe('[Slides] Load Slides');
  });
});
