import { Action, combineReducers, createFeatureSelector } from '@ngrx/store';
import * as fromSlidesImages from '@slides/store/slides-images/slides-images.reducer';
import * as fromSlides from '@slides/store/slides/slides.reducer';
import * as fromSlideMenu from '@slides/store/slides-menu/slides-menu.reducer';

export const SLIDES_MODULE_FEATURE_KEY = 'slidesModuleFeature';

export const selectSlidesFeatureState = createFeatureSelector<State>(
  SLIDES_MODULE_FEATURE_KEY
);

export interface State {
  [fromSlidesImages.SLIDES_IMAGES_FEATURE_KEY]: fromSlidesImages.State;
  [fromSlides.SLIDES_FEATURE_KEY]: fromSlides.State;
  [fromSlideMenu.SLIDE_MENU_FEATURE_KEY]: fromSlideMenu.State;
}

export function reducers(state: State | undefined, action: Action) {
  return combineReducers({
    [fromSlidesImages.SLIDES_IMAGES_FEATURE_KEY]: fromSlidesImages.reducer,
    [fromSlides.SLIDES_FEATURE_KEY]: fromSlides.reducer,
    [fromSlideMenu.SLIDE_MENU_FEATURE_KEY]: fromSlideMenu.reducer,
  })(state, action);
}
