import { WbcV2Examination } from 'empaia-api-lib';

export interface CaseExaminationSelection {
  caseId: string;
  examinationId: string;
}

export type Examination = WbcV2Examination;
