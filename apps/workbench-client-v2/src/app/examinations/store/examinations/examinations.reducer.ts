import { createReducer, on, Action } from '@ngrx/store';
import * as ExaminationsActions from './examinations.actions';
import { HttpErrorResponse } from '@angular/common/http';

export const EXAMINATIONS_FEATURE_KEY = 'examinations';

export interface State {
  error?: HttpErrorResponse;
}

export const initialState: State = {
  error: undefined
};

const examinationsReducer = createReducer(
  initialState,
  on(ExaminationsActions.createExaminationFailure, (state, { error }): State => ({
    ...state,
    error
  })),
  on(ExaminationsActions.closeExaminationFailure, (state, { error }): State => ({
    ...state,
    error
  })),
);

export function reducer(state: State | undefined, action: Action) {
  return examinationsReducer(state, action);
}
