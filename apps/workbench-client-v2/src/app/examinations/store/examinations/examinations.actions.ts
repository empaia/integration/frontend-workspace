import { createAction, props } from '@ngrx/store';
import { CaseExaminationSelection } from '@examinations/store/examinations/examinations.models';
import { HttpErrorResponse } from '@angular/common/http';

export const userSelectedExamination = createAction(
  '[Examinations Page] User Selected Examination',
  props<{ caseExaminationSelection: CaseExaminationSelection }>()
);

export const navigationSelectExamination = createAction(
  '[Examinations Page] User Selected Examination',
  props<{ caseExaminationSelection: CaseExaminationSelection }>()
);

export const createExamination = createAction(
  '[Examinations] Create Examinations',
  props<{ caseId: string }>()
);

export const createExaminationSuccess = createAction(
  '[Examinations] Create Examinations Success',
);

export const createExaminationFailure = createAction(
  '[Examinations] Create Examinations Failure',
  props<{ error: HttpErrorResponse }>()
);

export const closeExamination = createAction(
  '[Examinations] Close Examination',
  props<{ caseExaminationSelection: CaseExaminationSelection }>()
);

export const closeExaminationFailure = createAction(
  '[Examinations] Close Examination Failure',
  props<{ error: HttpErrorResponse }>()
);
