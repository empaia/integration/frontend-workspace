import { createSelector } from '@ngrx/store';
import { RouterSelectors } from '@router/store';
import { selectCasesEntities, selectSelectedCaseId } from '@cases/store/cases/cases.selectors';

export const selectSelectedId = createSelector(
  RouterSelectors.selectExaminationIdParam,
  (id) => id
);

export const selectExaminationFromRouter = createSelector(
  selectCasesEntities,
  selectSelectedCaseId,
  selectSelectedId,
  (cases, caseId, examinationId) =>
    caseId && examinationId ? cases[caseId]?.examinations.find(e => e.id === examinationId) : undefined
);

export  const selectSelected = createSelector(
  selectExaminationFromRouter,
  (entity) => entity
);
