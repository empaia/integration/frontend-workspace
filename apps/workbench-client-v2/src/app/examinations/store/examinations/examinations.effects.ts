import { Injectable } from '@angular/core';
import { createEffect, Actions, ofType, concatLatestFrom } from '@ngrx/effects';
import { pessimisticUpdate } from '@ngrx/router-store/data-persistence';

import * as ExaminationsFeature from './examinations.reducer';
import * as ExaminationsActions from './examinations.actions';
import * as CasesActions from '@cases/store/cases/cases.actions';
import * as CasesSelectors from '@cases/store/cases/cases.selectors';
import {
  WbcV2CasesPanelService,
  WbcV2ExaminationsPanelService,
} from 'empaia-api-lib';
import { exhaustMap, map } from 'rxjs/operators';
import { Store } from '@ngrx/store';
import { Router } from '@angular/router';
import { ROUTE_CASES, ROUTE_EXAMINATIONS } from '@router/routes';
import { ErrorSnackbarComponent } from '@shared/components/error-snackbar/error-snackbar.component';
import {
  CLOSE_EXAMINATION_ERROR,
  EXAMINATIONS_LOAD_ERROR,
} from '@menu/models/ui.models';
import { of } from 'rxjs';
import { MatSnackBar } from '@angular/material/snack-bar';
import { CaseExaminationSelection } from '@examinations/store/examinations/examinations.models';
import { HttpErrorResponse } from '@angular/common/http';

@Injectable()
export class ExaminationsEffects {
  examinationSelectedUpdateRoute$ = createEffect(
    () => {
      return this.actions$.pipe(
        ofType(ExaminationsActions.userSelectedExamination),
        map((action) => action.caseExaminationSelection),
        map((caseExamination) =>
          this.router.navigate(
            [
              ROUTE_CASES,
              caseExamination.caseId,
              ROUTE_EXAMINATIONS,
              caseExamination.examinationId,
            ],
            { queryParamsHandling: 'merge' }
          )
        )
      );
    },
    { dispatch: false }
  );

  createExamination$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ExaminationsActions.createExamination),
      pessimisticUpdate({
        run: (
          action: ReturnType<typeof ExaminationsActions.createExamination>,
          _state: ExaminationsFeature.State
        ) => {
          return this.casesPanelService
            .casesCaseIdExaminationsPost({
              case_id: action.caseId,
            })
            .pipe(
              map(
                (result) =>
                  ({
                    caseId: action.caseId,
                    examinationId: result.id,
                  } as CaseExaminationSelection)
              ),
              map((caseExaminationSelection) =>
                ExaminationsActions.userSelectedExamination({
                  caseExaminationSelection,
                })
              )
            );
        },
        onError: (
          _action: ReturnType<typeof ExaminationsActions.createExamination>,
          error
        ) => {
          return ExaminationsActions.createExaminationFailure({ error });
        },
      })
    );
  });

  examinationCreateError$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ExaminationsActions.createExaminationFailure),
      map((action) => action.error),
      exhaustMap((error: HttpErrorResponse) => {
        // ... you can check the payload here to show different messages
        // like if error.statusCode === 501 etc.
        this.snackBar.openFromComponent(ErrorSnackbarComponent, {
          data: { titel: EXAMINATIONS_LOAD_ERROR, error },
        });
        // remap to noop Action if no state needs to be updated.
        // or for example on 401 Errors dispatch a re-login action etc.
        return of({ type: 'noop' });
      })
    );
  });

  closeExamination$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ExaminationsActions.closeExamination),
      pessimisticUpdate({
        run: (
          action: ReturnType<typeof ExaminationsActions.closeExamination>,
          _state: ExaminationsFeature.State
        ) => {
          return this.examinationsPanelService
            .examinationsExaminationIdClosePut({
              examination_id: action.caseExaminationSelection.examinationId,
            })
            .pipe(map(() => ExaminationsActions.createExaminationSuccess()));
        },
        onError: (
          _action: ReturnType<typeof ExaminationsActions.closeExamination>,
          error
        ) => {
          return ExaminationsActions.closeExaminationFailure({ error });
        },
      })
    );
  });

  closeExaminationSuccess$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ExaminationsActions.createExaminationSuccess),
      concatLatestFrom(() =>
        this.store.select(CasesSelectors.selectCaseListSkip)
      ),
      map(([, skip]) => skip),
      map((skip) => CasesActions.loadCasePage({ skip }))
    );
  });

  closeExaminationFailed = createEffect(() => {
    return this.actions$.pipe(
      ofType(ExaminationsActions.closeExaminationFailure),
      map((action) => action.error),
      exhaustMap((error) => {
        this.snackBar.openFromComponent(ErrorSnackbarComponent, {
          data: { titel: CLOSE_EXAMINATION_ERROR, error },
        });
        return of({ type: 'noop' });
      })
    );
  });

  constructor(
    private readonly actions$: Actions,
    private readonly casesPanelService: WbcV2CasesPanelService,
    private readonly examinationsPanelService: WbcV2ExaminationsPanelService,
    private readonly store: Store,
    private readonly router: Router,
    private readonly snackBar: MatSnackBar
  ) {}
}
