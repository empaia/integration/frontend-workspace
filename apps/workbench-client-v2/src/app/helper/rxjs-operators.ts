/* eslint-disable @typescript-eslint/no-explicit-any */
import { combineLatest, from, ObservableInput } from 'rxjs';
import { UnaryFunction, Observable, pipe, OperatorFunction } from 'rxjs';
import { filter, last, map, mergeMap, take, toArray } from 'rxjs/operators';


// filter nullish values and infer type
export function filterNullish<T>(): UnaryFunction<Observable<T | null | undefined>, Observable<T>> {
  return pipe(
    filter(x => x != null) as OperatorFunction<T | null | undefined, T>
  );
}

/**
 *  if you need some extra values from other observables
 * - preferably synchronous like ngrx state
 * source: https://gist.github.com/sod/a34fc7705b396f61d0ef00713b807209
 *
 * Example:
 *     this.action.pipe(
 *         ofType(myActionType),
 *         tap((myActionType) => {}),      // <-- only has the action value
 *         mergeTakeOne(this.store.select(myStoreValue),
 *             // andAnotherObservable$, andEvenMoreObservables$
 *         ),
 *         tap(([myActionType, myStoreValue]) => {}),  // <-- has both, the  action value and the value from the store observable
 *
 */
export function mergeTakeOne<SOURCE, MERGE extends ObservableInput<any>[]>(
  ...toBeMerged$: [...MERGE]
): OperatorFunction<SOURCE, [SOURCE, ...{ [P in keyof MERGE]: MERGE[P] extends ObservableInput<infer U> ? U : never }]> {
  return mergeMap((source) =>
    combineLatest(toBeMerged$).pipe(
      take(1),
      map((source2): any => [source, ...source2]),
    ),
  );
}

// forkJoin with concurrent from:
// https://stackoverflow.com/questions/54245998/rxjs-parallel-queue-with-concurrent-workers
export function forkJoinConcurrent<T>(observables: Observable<T>[], concurrent: number): Observable<T[]> {
  return from(observables).pipe(
    mergeMap((observable, index) => observable.pipe(
      last(),
      map(value => ({ index, value }))
    ),
    concurrent
    ),
    toArray(),
    map(pairs => pairs.sort((l, r) => l.index - r.index).map(pair => pair.value))
  );
}

export function compareDistinct<T>(prev: T[], curr:T[]): boolean {
  return prev.length === curr.length && prev.every((val, index) => val === curr[index]);
}
