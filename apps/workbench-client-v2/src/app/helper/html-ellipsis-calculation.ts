export function isEllipsisActive(element: HTMLElement): boolean {
  // the offset height is 1px smaller than the scrollHeight
  // so add one to offset height and check if the text
  // was truncate by css
  return element.offsetHeight + 1 < element.scrollHeight;
}
