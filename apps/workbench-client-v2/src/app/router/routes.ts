import {
  RP_APP_ID,
  RP_CASE_ID,
  RP_EXAMINATION_ID,
} from './router.params';
  
export const ROUTE_CASES = 'cases';
export const ROUTE_CASES_ID = '/:' + RP_CASE_ID;
export const ROUTE_TO_CASES = `${ROUTE_CASES}${ROUTE_CASES_ID}`;

export const ROUTE_EXAMINATIONS = 'examinations';
export const ROUTE_EXAMINATIONS_ID = '/:' + RP_EXAMINATION_ID;
export const ROUTE_TO_EXAMINATIONS = `${ROUTE_TO_CASES}/${ROUTE_EXAMINATIONS}${ROUTE_EXAMINATIONS_ID}`;
  
export const ROUTE_APPS = 'apps';
export const ROUTE_APPS_ID = '/:' + RP_APP_ID;
export const ROUTE_TO_APPS = `${ROUTE_TO_EXAMINATIONS}/${ROUTE_APPS}${ROUTE_APPS_ID}`;
