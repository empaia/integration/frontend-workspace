import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from '@core/containers/app/app.component';
import { StoreModule } from '@ngrx/store';
import {
  ROUTE_APPS,
  ROUTE_APPS_ID,
  ROUTE_CASES,
  ROUTE_CASES_ID,
  ROUTE_EXAMINATIONS,
  ROUTE_EXAMINATIONS_ID,
  ROUTE_TO_CASES,
  ROUTE_TO_EXAMINATIONS
} from './routes';
import { RouterEffects, RouterFeature, RouterSerializer } from './store';
import { EffectsModule } from '@ngrx/effects';
import { StoreRouterConnectingModule } from '@ngrx/router-store';
import { ROUTER_FEATURE_KEY } from './store/router.reducer';


const routes: Routes = [
  {
    path: ROUTE_CASES,
    component: AppComponent,
  },
  {
    path: ROUTE_CASES + ROUTE_CASES_ID,
    component: AppComponent,
  },
  {
    path: ROUTE_TO_CASES + '/' + ROUTE_EXAMINATIONS,
    component: AppComponent,
  },
  {
    path: ROUTE_TO_CASES + '/' + ROUTE_EXAMINATIONS + ROUTE_EXAMINATIONS_ID,
    component: AppComponent,
  },
  {
    path: ROUTE_TO_EXAMINATIONS + '/' + ROUTE_APPS,
    component: AppComponent
  },
  {
    path: ROUTE_TO_EXAMINATIONS + '/' + ROUTE_APPS + ROUTE_APPS_ID,
    component: AppComponent
  },
  // Fallback route when no other route matches, to redirect to ROUTE_CASES
  // is use when a user is logged in and uses the base url without the hashtag (#)
  {
    path: '**',
    redirectTo: ROUTE_CASES,
    pathMatch: 'full',
  },
];

@NgModule({
  declarations: [],
  imports: [
    StoreModule.forFeature(
      RouterFeature.ROUTER_FEATURE_KEY,
      RouterFeature.reducer,
    ),
    EffectsModule.forFeature([RouterEffects]),
    StoreRouterConnectingModule.forRoot({
      serializer: RouterSerializer.MergedRouterStateSerializer,
      stateKey: ROUTER_FEATURE_KEY,
    }),
    RouterModule.forRoot(routes,  {
      initialNavigation: 'disabled',
      useHash: true,
      onSameUrlNavigation: 'reload'
    })
  ],
  exports: [RouterModule]
})
export class AppRouterModule { }
