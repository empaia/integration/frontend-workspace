import { Injectable } from '@angular/core';
import { filterNullish } from '@helper/rxjs-operators';
import * as AppsActions from '@apps/store/apps/apps.actions';
import * as MenuActions from '@menu/store/menu/menu.actions';
import * as CasesActions from '@cases/store/cases/cases.actions';
import { createEffect, Actions, ofType, concatLatestFrom } from '@ngrx/effects';
import { routerNavigatedAction } from '@ngrx/router-store';
import { Store } from '@ngrx/store';
import { distinctUntilChanged, filter, map } from 'rxjs/operators';
import { selectAppIdParam, selectCaseIdParam, selectExaminationIdParam } from './router.selectors';

@Injectable()
export class RouterEffects {

  autoOpenCasesMenu$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(routerNavigatedAction),
      concatLatestFrom(() => [
        this.store.select(selectCaseIdParam),
        this.store.select(selectExaminationIdParam)
      ]),
      map(([, caseId, examinationId]) => caseId && examinationId
        ? MenuActions.openMenuMidi({ menuItem: 'Apps' })
        : MenuActions.openMenuMax({ menuItem: 'Cases' })
      )
    );
  });

  autoOpenAppsMenu$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(routerNavigatedAction),
      concatLatestFrom(() => this.store.select(selectAppIdParam)),
      filter(([, appId]) => !!appId),
      map(() => MenuActions.openMenuMidi({ menuItem: 'Apps' }))
    );
  });

  dispatchSelectCaseAction$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(routerNavigatedAction),
      concatLatestFrom(() => this.store.select(selectCaseIdParam)),
      map(([_, id]) => id),
      distinctUntilChanged(),
      filterNullish(),
      map(id => CasesActions.navigationSelectCase({ id })),
    );
  });

  dispatchSelectAppAction$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(routerNavigatedAction),
      concatLatestFrom(() => this.store.select(selectAppIdParam)),
      map(([, appId]) => appId),
      distinctUntilChanged(),
      filterNullish(),
      map(appId => AppsActions.navigationSelectApp({ appId }))
    );
  });


  constructor(
    private actions$: Actions,
    private store: Store,
  ) {}
}
