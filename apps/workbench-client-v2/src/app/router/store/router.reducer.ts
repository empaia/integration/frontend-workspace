import { Action } from '@ngrx/store';
import { Data, Params } from '@angular/router';
import { routerReducer, RouterReducerState } from '@ngrx/router-store';

export const ROUTER_FEATURE_KEY = 'router';

export interface MergedRoute {
  url: string;
  queryParams: Params;
  params: Params;
  data: Data;
}

export type MergedRouteReducerState = RouterReducerState<MergedRoute>;

export const initialState: MergedRoute = {
  url: '',
  queryParams: {},
  params: {},
  data: {},
};

export function reducer(
  state: RouterReducerState<MergedRoute> | undefined,
  action: Action
) { return routerReducer(state, action);  }
