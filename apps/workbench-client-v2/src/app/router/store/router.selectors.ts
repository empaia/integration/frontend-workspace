import { createFeatureSelector, createSelector } from '@ngrx/store';
import { RP_APP_ID, RP_CASE_ID, RP_EXAMINATION_ID, RP_SLIDE_ID } from '@router/router.params';
import {
  MergedRouteReducerState,
  ROUTER_FEATURE_KEY,
} from './router.reducer';


export const selectRouter = createFeatureSelector<MergedRouteReducerState>(ROUTER_FEATURE_KEY);

export const selectMergedRoute = createSelector(
  selectRouter,
  (routerReducerState) => routerReducerState.state
);

export const selectRouteParams = createSelector(
  selectRouter,
  (routerReducerState) => (routerReducerState && routerReducerState.state && routerReducerState.state.params) || {},
);


export const selectRouterParam = (paramName: string) =>
  createSelector(selectRouteParams, (params) => params[paramName]);

const selectRouterStringParam = (paramName: string) => createSelector(
  selectRouterParam(paramName),
  //(p) => p as string
  (param) => {
    if (typeof param === 'string') { return param as string; }
    else { return undefined; }
  }
);

export const selectCaseIdParam =
  selectRouterStringParam(RP_CASE_ID);

export const selectExaminationIdParam =
  selectRouterStringParam(RP_EXAMINATION_ID);

export const selectAppIdParam =
  selectRouterStringParam(RP_APP_ID);

/**
 * Query Params selectors (optional params)
 * used for: slides
 */
export const selectRouteQueryParams = createSelector(
  selectRouter,
  (routerReducerState) => (routerReducerState && routerReducerState.state && routerReducerState.state.queryParams) || {},
);

export const selectRouterQueryParam = (paramName: string) =>
  createSelector(selectRouteQueryParams, (params) => params[paramName]);

const selectRouterStringQueryParam = (paramName: string) => createSelector(
  selectRouterQueryParam(paramName),
  (param) => {
    if (typeof param === 'string') { return param as string; }
    else { return undefined; }
  }
);

export const selectSlideIdParam =
  selectRouterStringQueryParam(RP_SLIDE_ID);
