import { TestBed } from '@angular/core/testing';

import { Observable } from 'rxjs';

import { provideMockActions } from '@ngrx/effects/testing';
import { provideMockStore } from '@ngrx/store/testing';

import { RouterEffects } from './router.effects';

describe('RouterEffects', () => {
  let actions: Observable<unknown>;
  let effects: RouterEffects;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        RouterEffects,
        provideMockActions(() => actions),
        provideMockStore(),
      ],
    });

    effects = TestBed.inject(RouterEffects);
  });

  it('should be created', () => {
    expect(effects).toBeTruthy();
  });

  describe('init$', () => {
    it('todo: should work', () => {
      expect(true).toBeTruthy();
      /*actions = hot('-a-|', { a: RouterActions.init() });

      const expected = hot('-a-|', {
        a: RouterActions.loadRouterSuccess({ router: [] }),
      });

      expect(effects.init$).toBeObservable(expected);*/
    });
  });
});
