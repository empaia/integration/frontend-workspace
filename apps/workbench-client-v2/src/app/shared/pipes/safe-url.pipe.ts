import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Pipe, PipeTransform } from '@angular/core';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';
import { Observable, of, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';

/**
 * source for this solution:
 * https://stackoverflow.com/a/50463201
 */

@Pipe({
  name: 'safeUrl'
})
export class SafeUrlPipe implements PipeTransform {

  constructor(private http: HttpClient, private sanitizer: DomSanitizer) { }

  transform(url: string): Observable<SafeUrl | undefined> {
    return this.http.get(url, { responseType: 'blob' }).pipe(
      catchError((error: HttpErrorResponse) => {
        if (error.status === 404) {
          // image is not available
          return of(undefined);
        } else {
          // actual error occured
          return throwError(error);
        }
      }),
      map(blob => {
        return (blob === undefined) ? blob : this.sanitizer.bypassSecurityTrustUrl(URL.createObjectURL(blob));
      })
    );
  }
}
