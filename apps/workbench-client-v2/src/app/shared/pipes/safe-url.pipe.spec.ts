import { createPipeFactory, SpectatorPipe } from '@ngneat/spectator/jest';

import { SafeUrlPipe } from './safe-url.pipe';

describe('SafeUrlPipe ', () => {
  let spectator: SpectatorPipe<SafeUrlPipe>;
  const createPipe = createPipeFactory(SafeUrlPipe);

  it('should create pipe for testing', () => {
    spectator = createPipe('testing');
    expect(spectator.element).toBeTruthy();
  });
});
