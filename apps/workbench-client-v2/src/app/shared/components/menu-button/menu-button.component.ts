import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { MenuItem } from '@menu/models/menu.models';
import { MenuButtonEventModel } from '@shared/components/menu-button/menu-button.models';

@Component({
  selector: 'app-menu-button',
  templateUrl: './menu-button.component.html',
  styleUrls: ['./menu-button.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MenuButtonComponent {
  @Input() public buttonValue!: MenuItem;
  @Input() public selected = false;
  @Input() public enableTopBorderBoxShadow = false;

  @Output() public buttonClicked = new EventEmitter<MenuButtonEventModel>();

  onButtonClick(event: MouseEvent): void {
    this.buttonClicked.emit({ mouse: event, value: this.buttonValue });
  }

  getCssClass(): string {
    let cssClass = this.selected ? 'mat-primary' : 'mat-button-bg';
    if (this.enableTopBorderBoxShadow) {
      cssClass += ' menu-border-box-shadow';
    }
    return cssClass;
  }
}
