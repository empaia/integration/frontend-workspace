import { MenuButtonComponent } from './menu-button.component';
import { createComponentFactory, Spectator } from '@ngneat/spectator';
import { MaterialModule } from '@material/material.module';

describe('MenuButtonComponent', () => {
  let spectator:Spectator<MenuButtonComponent>;
  const createComponent = createComponentFactory({
    component: MenuButtonComponent,
    imports: [
      MaterialModule,
    ]
  });

  beforeEach(() => spectator = createComponent());

  it('should create', () => {
    expect(spectator.component).toBeTruthy();
  });
});
