import { MenuItem } from '@menu/models/menu.models';

export interface MenuButtonEventModel {
  mouse: MouseEvent;
  value: MenuItem;
}
