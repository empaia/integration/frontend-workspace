import { MaterialModule } from '@material/material.module';
import { Spectator, createComponentFactory } from '@ngneat/spectator/jest';

import { MissingSelectionErrorComponent } from './missing-selection-error.component';

describe('MissingSelectionErrorComponent', () => {
  let spectator: Spectator<MissingSelectionErrorComponent>;

  const createComponent = createComponentFactory({
    component: MissingSelectionErrorComponent,
    imports: [
      MaterialModule
    ],
    declarations: [
      // MockComponent(NoCaseSelectedErrorComponent)
    ]
  });

  it('should create', () => {
    spectator = createComponent();

    expect(spectator.component).toBeTruthy();
  });

  it.todo('provide real tests for MissingSelectionErrorComponent');
});
