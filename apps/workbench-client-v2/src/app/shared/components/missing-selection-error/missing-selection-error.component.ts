import {
  Component,
  ChangeDetectionStrategy,
  Input,
  EventEmitter,
  Output
} from '@angular/core';
import { MenuItem } from '@menu/models/menu.models';
import { SelectionError } from '@menu/models/ui.models';

@Component({
  selector: 'app-missing-selection-error',
  templateUrl: './missing-selection-error.component.html',
  styleUrls: ['./missing-selection-error.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MissingSelectionErrorComponent {

  @Input() public missingSelectionType!: SelectionError;
  @Output() public errorClicked = new EventEmitter<MenuItem>();


  errorLabelClick(menuType: MenuItem) {
    this.errorClicked.emit(menuType);
  }

}
