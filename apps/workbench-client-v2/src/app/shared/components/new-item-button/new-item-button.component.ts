import { Component, ChangeDetectionStrategy, EventEmitter, Output, Input } from '@angular/core';

@Component({
  selector: 'app-new-item-button',
  templateUrl: './new-item-button.component.html',
  styleUrls: ['./new-item-button.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class NewItemButtonComponent {
  @Input() public disabled!: boolean;
  @Output() public clicked = new EventEmitter<MouseEvent>();
}
