import { ExpansionButtonComponent } from './expansion-button.component';
import { createComponentFactory, Spectator } from '@ngneat/spectator';
import { MaterialModule } from '@material/material.module';

describe('ExpansionButtonComponent', () => {
  let spectator: Spectator<ExpansionButtonComponent>;
  const createComponent = createComponentFactory({
    component: ExpansionButtonComponent,
    imports: [
      MaterialModule,
    ]
  });

  beforeEach(() => spectator = createComponent());

  it('should create', () => {
    expect(spectator.component).toBeTruthy();
  });
});
