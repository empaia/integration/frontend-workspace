import { TabItemComponent } from './tab-item.component';
import { createComponentFactory, Spectator } from '@ngneat/spectator';

describe('TabItemComponent', () => {
  let spectator: Spectator<TabItemComponent>;
  const createComponent = createComponentFactory({
    component: TabItemComponent,
  });

  beforeEach(() => spectator = createComponent());

  it('should create', () => {
    expect(spectator.component).toBeTruthy();
  });
});
