import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-close-button',
  templateUrl: './close-button.component.html',
  styleUrls: ['./close-button.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CloseButtonComponent {
  @Input() public disabled!: boolean;

  @Output() public pushedClose = new EventEmitter<void>();

  public onClose(event: MouseEvent): void {
    this.pushedClose.emit();
    event.stopImmediatePropagation();
  }
}
