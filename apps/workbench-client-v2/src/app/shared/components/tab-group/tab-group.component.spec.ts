import { TabGroupComponent } from './tab-group.component';
import { createComponentFactory, Spectator } from '@ngneat/spectator';

describe('TabGroupComponent', () => {
  let spectator: Spectator<TabGroupComponent>;
  const createComponent = createComponentFactory({
    component: TabGroupComponent,
  });

  beforeEach(() => spectator = createComponent());

  it('should create', () => {
    expect(spectator.component).toBeTruthy();
  });
});
