import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

@Component({
  selector: 'app-menu-label',
  templateUrl: './menu-label.component.html',
  styleUrls: ['./menu-label.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MenuLabelComponent {
  @Input() public selected!: boolean;
  @Input() public loaded = true;
  @Input() public enableMenuLabelTopShadow = false;

  getCssClass(): string {
    let cssClass = this.selected ? 'mat-primary-bg' : 'mat-button-bg';
    if (this.enableMenuLabelTopShadow) {
      cssClass += ' menu-border-box-shadow';
    }
    return cssClass;
  }
}
