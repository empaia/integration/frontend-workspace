import { MenuLoadingComponent } from './menu-loading.component';
import { createComponentFactory, Spectator } from '@ngneat/spectator';
import { MaterialModule } from '@material/material.module';

describe('MenuLoadingComponent', () => {
  let spectator: Spectator<MenuLoadingComponent>;
  const createComponent = createComponentFactory({
    component: MenuLoadingComponent,
    imports: [
      MaterialModule,
    ]
  });

  beforeEach(() => spectator = createComponent());

  it('should create', () => {
    expect(spectator.component).toBeTruthy();
  });
});
