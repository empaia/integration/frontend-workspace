import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

@Component({
  selector: 'app-menu-loading',
  templateUrl: './menu-loading.component.html',
  styleUrls: ['./menu-loading.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MenuLoadingComponent {
  @Input() public invertColor = false;
}
