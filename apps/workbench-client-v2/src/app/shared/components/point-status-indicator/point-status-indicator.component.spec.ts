import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PointStatusIndicatorComponent } from './point-status-indicator.component';

describe('PointStatusIndicatorComponent', () => {
  let component: PointStatusIndicatorComponent;
  let fixture: ComponentFixture<PointStatusIndicatorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PointStatusIndicatorComponent ]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PointStatusIndicatorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
