import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { MenuEntity, MenuItem, MenuState } from '@menu/models/menu.models';

@Component({
  selector: 'app-menu-item-layout',
  templateUrl: './menu-item-layout.component.html',
  styleUrls: ['./menu-item-layout.component.scss'],
  changeDetection: ChangeDetectionStrategy.Default
})
export class MenuItemLayoutComponent {
  readonly MENU_STATE = MenuState;
  @Input() public menu!: MenuEntity;
  @Output() public labelClicked = new EventEmitter<MenuItem>();

  getCssClass(labelState: MenuState): string {
    switch (labelState) {
      case MenuState.MAX:
        return 'menu-full-width';
      case MenuState.MIDI:
        return 'menu-midi-width';
      default:
        return '';
    }
  }

  labelClick(): void {
    this.labelClicked.emit(this.menu.id);
  }

}
