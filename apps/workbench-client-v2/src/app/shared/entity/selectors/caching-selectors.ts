import { EntityCachingSelectors, EntityCachingState } from '@shared/entity/models/caching.models';

export class CachingSelectors<T, V extends EntityCachingState<T>> implements EntityCachingSelectors<T, V> {
  selectAll(state: V): T[] {
    const values: T[] = [];
    for (const value of state.entityCache.values()) {
      values.push(value);
    }
    return values;
  }

  selectIds(state: V): string[] {
    const ids: string[] = [];
    for (const id of state.entityCache.keys()) {
      ids.push(id);
    }
    return ids;
  }

  selectTotal(state: V): number {
    return state.entityCache.size;
  }
}
