import { Directive, EventEmitter, HostListener, Input, Output } from "@angular/core";
import { MatSnackBar } from "@angular/material/snack-bar";
import { logger } from "@tools/logger";

@Directive({ selector: '[appCopyToClipboard]' })
export class CopyToClipboardDirective {

  public key: string | undefined;

  @Input('appCopyToClipboard')
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  public content!: any;

  @Output()
  public copied: EventEmitter<string> = new EventEmitter<string>();

  constructor(private _snackBar: MatSnackBar) {

  }

  @HostListener('click', ['$event'])
  public onClick($event: MouseEvent): void {
    if(this.key && this.content) {
      $event.preventDefault();

      if(this.key === 'i') {
        // user requests id of selected item
        const id = this.content['id'] ?? (this.content['item'].id ?? undefined);
        if (id) {
          navigator.clipboard.writeText(id).then().catch(e => logger.error(e));
          this.notify('ID');
        }
      } else if (this.key === 'j') {
        // user requests selected item as json string
        navigator.clipboard.writeText(
          JSON.stringify(this.content, null, 2)
        ).then().catch(e => logger.error(e));
        this.notify('JSON-Object');
      }

      $event.stopPropagation();
      $event.stopImmediatePropagation();
    }
  }

  private notify(text: string): void {
    this._snackBar.open(`${text} was copied to clipboard`, 'Ok', {
      duration: 500
    });
  }

  @HostListener('document:keydown', ['$event'])
  public keyDown($event: KeyboardEvent) {
    // filter alt key because user could press alt + tab
    // in this case alt would be stored in 'this.key'
    // and only be removed when another key was pressed
    // because the browser is not focused anymore keyup
    // of alt will never be registrated
    this.key = $event.key !== 'Alt' ? $event.key : undefined;
  }

  @HostListener('document:keyup', ['$event'])
  public keyUp(_$event: KeyboardEvent) {
    this.key = undefined;
  }

}
