import { AppMappingService } from './app-mapping.service';
import { createServiceFactory, SpectatorService } from '@ngneat/spectator';

describe('AppMappingService', () => {
  let spectator: SpectatorService<AppMappingService>;
  const createService = createServiceFactory({
    service: AppMappingService,
  });

  beforeEach(() => spectator = createService());

  it('should be created', () => {
    expect(spectator.service).toBeTruthy();
  });
});
