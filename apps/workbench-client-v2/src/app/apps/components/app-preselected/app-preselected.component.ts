import { Component, ChangeDetectionStrategy, Input, Output, EventEmitter } from '@angular/core';
import { App } from '@apps/store';
import { IdSelection } from '@menu/models/ui.models';

@Component({
  selector: 'app-app-preselected',
  templateUrl: './app-preselected.component.html',
  styleUrls: ['./app-preselected.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AppPreselectedComponent {
  @Input() appItem!: App;

  @Output() appSelected = new EventEmitter<IdSelection>();
  @Output() cancelSelected = new EventEmitter<void>();

  select(): void {
    this.appSelected.emit({ id: this.appItem.id });
  }
}
