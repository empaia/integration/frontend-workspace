import { AppPreselectedComponent } from './app-preselected.component';
import { createComponentFactory, Spectator } from '@ngneat/spectator';
import { MaterialModule } from '@material/material.module';
import { MockComponents } from 'ng-mocks';
import { AppHeaderComponent } from '@apps/components/app-header/app-header.component';

describe('AppPreselectedComponent', () => {
  let spectator: Spectator<AppPreselectedComponent>;
  const createComponent = createComponentFactory({
    component: AppPreselectedComponent,
    imports: [
      MaterialModule,
    ],
    declarations: [
      MockComponents(
        AppHeaderComponent,
      )
    ]
  });

  beforeEach(() => spectator = createComponent());

  it('should create', () => {
    expect(spectator.component).toBeTruthy();
  });
});
