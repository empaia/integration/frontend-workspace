import { Component, ChangeDetectionStrategy, Input, Output, EventEmitter } from '@angular/core';
import { NewAppEntity } from '@apps/store/new-apps/new-apps.models';


@Component({
  selector: 'app-new-app-panel',
  templateUrl: './new-app-panel.component.html',
  styleUrls: ['./new-app-panel.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class NewAppPanelComponent {
  @Input() public apps!: NewAppEntity[];
  @Input() public appsLoaded = true;
  @Input() public solutionStoreUrl!: string;

  @Output() public addAppToExamination = new EventEmitter<NewAppEntity>();
  @Output() public closePanel = new EventEmitter<MouseEvent>();
  @Output() public hoverOverApp = new EventEmitter<string | undefined>();

  public openSolutionStore(): void {
    window.open(this.solutionStoreUrl, '_blank');
  }

  public appsTrackBy(_index: number, app: NewAppEntity): string {
    return app.id;
  }
}
