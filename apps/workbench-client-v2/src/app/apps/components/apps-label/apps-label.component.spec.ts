import { AppsLabelComponent } from './apps-label.component';
import { createComponentFactory, Spectator } from '@ngneat/spectator';
import { MaterialModule } from '@material/material.module';
import { MockComponents } from 'ng-mocks';
import { MenuLabelComponent } from '@shared/components/menu-label/menu-label.component';
import { NewItemLabelButtonComponent } from '@shared/components/new-item-label-button/new-item-label-button.component';

describe('AppsLabelComponent', () => {
  let spectator: Spectator<AppsLabelComponent>;
  const createComponent = createComponentFactory({
    component: AppsLabelComponent,
    imports: [
      MaterialModule,
    ],
    declarations: [
      MockComponents(
        MenuLabelComponent,
        NewItemLabelButtonComponent,
      )
    ]
  });

  beforeEach(() => spectator = createComponent());

  it('should create', () => {
    expect(spectator.component).toBeTruthy();
  });
});
