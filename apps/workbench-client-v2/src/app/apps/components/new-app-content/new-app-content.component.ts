import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { NewAppEntity } from '@apps/store/new-apps/new-apps.models';

@Component({
  selector: 'app-new-app-content',
  templateUrl: './new-app-content.component.html',
  styleUrls: ['./new-app-content.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class NewAppContentComponent {
  @Input() public app!: NewAppEntity;
}
