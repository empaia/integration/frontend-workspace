import { NewAppContentComponent } from './new-app-content.component';
import { createComponentFactory, Spectator } from '@ngneat/spectator';

describe('NewAppContentComponent', () => {
  let spectator: Spectator<NewAppContentComponent>;
  const createComponent = createComponentFactory({
    component: NewAppContentComponent,
  });

  beforeEach(() => spectator = createComponent({
    props: {
      app: {
        app_version: 'v1',
        id: '123',
        name_short: 'Test_Name',
        store_description: 'Test description',
        store_icon_url: 'Image',
        store_url: 'Store',
        vendor_name: 'Test_Vendor',
        has_frontend: false,
        research_only: false,
        store_docs_url: null,
        store_preview_after_url: null,
        store_preview_before_url: null,
      }
    }
  }));

  it('should create', () => {
    expect(spectator.component).toBeTruthy();
  });
});
