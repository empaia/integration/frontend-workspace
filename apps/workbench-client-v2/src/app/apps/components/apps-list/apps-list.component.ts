import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { IdSelection, SelectionError } from '@menu/models/ui.models';
import { MenuItem } from '@menu/models/menu.models';
import { VendorAppEntity } from '@apps/store/vendor-app-surfaces/vendor-app-surfaces.models';
import { Dictionary } from '@ngrx/entity';
import { App, AppExpansion } from '@apps/store';

@Component({
  selector: 'app-apps-list',
  templateUrl: './apps-list.component.html',
  styleUrls: ['./apps-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AppsListComponent {

  @Input() public appList!: App[];
  @Input() public selectedApp!: string;
  @Input() public preselectedApp!: string;
  @Input() public selectionMissing!: SelectionError;
  @Input() public disableNewButton!: boolean;
  @Input() public selectedExamination: string | undefined;
  @Input() public appExpansionDictionary!: Dictionary<AppExpansion>;

  @Input() public runningApps!: ReadonlyMap<string, VendorAppEntity>;
  @Input() public activeApp!: string;

  @Input() public hoveredNewApp: string | undefined;


  @Output() public appPreselected = new EventEmitter<IdSelection>();
  @Output() public cancelSelected = new EventEmitter<void>();
  @Output() public appSelected = new EventEmitter<IdSelection>();
  @Output() public addNewApp = new EventEmitter<MouseEvent>();
  @Output() public closeApp = new EventEmitter<IdSelection>();
  @Output() public errorClicked = new EventEmitter<MenuItem>();
  @Output() public expandApp = new EventEmitter<string>();
  @Output() public collapseApp = new EventEmitter<string>();

  public trackByAppId(_index: number, app: App): string {
    return app.id;
  }
}
