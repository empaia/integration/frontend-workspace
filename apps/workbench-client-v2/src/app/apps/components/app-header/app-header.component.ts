import { Component, ChangeDetectionStrategy, Input} from '@angular/core';
import { App } from '@apps/store';

@Component({
  selector: 'app-app-header',
  templateUrl: './app-header.component.html',
  styleUrls: ['./app-header.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AppHeaderComponent {
  @Input() public appItem!: App;
}
