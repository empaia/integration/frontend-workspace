import { ExpansionButtonComponent } from '@shared/components/expansion-button/expansion-button.component';
import { MaterialModule } from '@material/material.module';
import { Spectator, createComponentFactory } from '@ngneat/spectator/jest';
import { ExpansionItemComponent } from '@shared/components/expansion-item/expansion-item.component';
import { MockComponents } from 'ng-mocks';

import { AppHeaderComponent } from './app-header.component';
import { App } from '@apps/store';

describe('AppHeaderComponent', () => {
  let spectator: Spectator<AppHeaderComponent>;

  const createAppsEntity = (id: string, appVersion = '') =>
    ({
      id,
      app_version: appVersion || `version-${id}`,
      store_description: 'desc',
      store_docs_url: 'docs',
      store_icon_url: 'icon',
      store_url: 'store_url',
    } as App);

  const createComponent = createComponentFactory({
    component: AppHeaderComponent,

    declarations: [
      MockComponents(
        ExpansionItemComponent,
        ExpansionButtonComponent
      ),
    ],
    imports: [
      MaterialModule,
    ],
  });

  it('should create', () => {
    spectator = createComponent(
      {
        props: {
          appItem: createAppsEntity('foo')
        }
      }
    );

    expect(spectator.component).toBeTruthy();
  });

  it.todo('provide real tests for AppHeaderComponent');
});
