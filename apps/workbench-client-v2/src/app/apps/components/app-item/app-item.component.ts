import { AfterViewInit, ChangeDetectionStrategy, ChangeDetectorRef, Component, ElementRef, EventEmitter, Input, Output, ViewChild } from '@angular/core';
import { ProgressBarMode } from '@angular/material/progress-bar';
import { App } from '@apps/store';
import { isEllipsisActive } from '@helper/html-ellipsis-calculation';
import { IdSelection } from '@menu/models/ui.models';

@Component({
  selector: 'app-app-item',
  templateUrl: './app-item.component.html',
  styleUrls: ['./app-item.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AppItemComponent implements AfterViewInit {
  @ViewChild('appDescriptionSpan') private appDescription!: ElementRef;

  @Input() public appItem!: App;
  @Input() public selectedApp!: string;
  @Input() public expanded!: boolean;

  @Input() public appActive = false;
  @Input() public appRunning = false;

  @Input() public hoveredNewApp!: boolean;

  @Output() public itemSelected = new EventEmitter<IdSelection>();
  @Output() public closeApp = new EventEmitter<IdSelection>();
  @Output() public expandApp = new EventEmitter<string>();
  @Output() public collapseApp = new EventEmitter<string>();

  public disabled = false;

  constructor(private changeRef: ChangeDetectorRef) {}

  private isDisabled(): boolean {
    return this.appDescription ? !isEllipsisActive(this.appDescription.nativeElement) : true;
  }

  public ngAfterViewInit(): void {
    this.disabled = this.isDisabled();
    this.changeRef.detectChanges();
  }

  public getProgressMode(): ProgressBarMode {
    const fin = this.appItem?.jobs_count_finished;
    const count = this.appItem?.jobs_count;
    if (fin && count) {
      return (fin < count) ? 'indeterminate' : 'determinate';
    }
    return 'determinate';
  }

  public select() {
    this.itemSelected.emit({ id: this.appItem.id });
  }

  public goToLink(url: string | undefined, clickEvent: Event){
    if(url) {
      window.open(url, "_blank");
    }
    clickEvent.stopPropagation();
  }

  public toggleExpansion(expanded: boolean): void {
    if (expanded) {
      this.expandApp.emit(this.appItem.id);
    } else {
      this.collapseApp.emit(this.appItem.id);
    }
  }

  public validateStringLength(s: string | undefined, length: number): boolean {
    if (s) {
      return s.length <= length;
    } else {
      return true;
    }
  }

  public onClose(id: string): void {
    this.closeApp.emit({ id });
  }
}
