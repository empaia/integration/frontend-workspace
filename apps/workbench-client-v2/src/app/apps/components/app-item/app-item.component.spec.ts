import { MaterialModule } from '@material/material.module';
import { createComponentFactory, Spectator } from '@ngneat/spectator';
import { ExpansionButtonComponent } from '@shared/components/expansion-button/expansion-button.component';
import { ExpansionItemComponent } from '@shared/components/expansion-item/expansion-item.component';
import { CopyToClipboardDirective } from '@shared/directives/copy-to-clipboard.directive';
import { MockComponents } from 'ng-mocks';
import { AppHeaderComponent } from '../app-header/app-header.component';

import { AppItemComponent } from './app-item.component';
import { CloseButtonComponent } from '@shared/components/close-button/close-button.component';
import { App } from '@apps/store';

describe('AppItemComponent', () => {
  let spectator: Spectator<AppItemComponent>;

  const createAppsEntity = (id: string, appVersion = '') =>
    ({
      id,
      app_version: appVersion || `version-${id}`,
      store_description: 'desc',
      store_docs_url: 'docs',
      store_icon_url: 'icon',
      store_url: 'store_url',
    } as App);

  const createComponent = createComponentFactory({
    component: AppItemComponent,
    imports: [
      MaterialModule,
    ],
    declarations: [
      MockComponents(
        ExpansionItemComponent,
        ExpansionButtonComponent,
        AppHeaderComponent,
        CloseButtonComponent,
      ),
      CopyToClipboardDirective,
    ],
  });

  it('should create', () => {
    spectator = createComponent(
      {
        props: {
          appItem: createAppsEntity('foo')
        }
      }
    );

    expect(spectator.component).toBeTruthy();
  });

  it.todo('provide real tests for AppItemComponent');
});
