import { NewAppCardComponent } from './new-app-card.component';
import { createComponentFactory, Spectator } from '@ngneat/spectator';
import { MockComponents, MockDirectives } from 'ng-mocks';
import { NewAppContentComponent } from '@apps/components/new-app-content/new-app-content.component';
import { NewAppDetailsComponent } from '@apps/components/new-app-details/new-app-details.component';
import { TabGroupComponent } from '@shared/components/tab-group/tab-group.component';
import { TabItemComponent } from '@shared/components/tab-item/tab-item.component';
import { MaterialModule } from '@material/material.module';
import { CopyToClipboardDirective } from '@shared/directives/copy-to-clipboard.directive';

describe('NewAppCardComponent', () => {
  let spectator: Spectator<NewAppCardComponent>;
  const createComponent = createComponentFactory({
    component: NewAppCardComponent,
    imports: [
      MaterialModule
    ],
    declarations: [
      MockComponents(
        NewAppContentComponent,
        NewAppDetailsComponent,
        TabGroupComponent,
        TabItemComponent,
      ),
      MockDirectives(
        CopyToClipboardDirective
      )
    ]
  });

  beforeEach(() => spectator = createComponent({
    props: {
      app: {
        app_version: 'v1',
        id: '123',
        name_short: 'Test_Name',
        store_description: 'Test description',
        store_icon_url: 'Image',
        store_url: 'Store',
        vendor_name: 'Test_Vendor',
        has_frontend: false,
        research_only: false,
        store_docs_url: null,
        store_preview_after_url: null,
        store_preview_before_url: null,
      }
    }
  }));

  it('should create', () => {
    expect(spectator.component).toBeTruthy();
  });
});
