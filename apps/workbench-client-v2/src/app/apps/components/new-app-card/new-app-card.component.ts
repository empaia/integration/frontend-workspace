import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { NewAppEntity } from '@apps/store/new-apps/new-apps.models';

@Component({
  selector: 'app-new-app-card',
  templateUrl: './new-app-card.component.html',
  styleUrls: ['./new-app-card.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class NewAppCardComponent {
  @Input() public app!: NewAppEntity;

  @Output() public addAppToExamination = new EventEmitter<NewAppEntity>();
  @Output() public hoverOverApp = new EventEmitter<string | undefined>();

  public onAppCardClicked(): void {
    if (!this.app.addedByUser) {
      this.addAppToExamination.emit(this.app);
    }
  }
}
