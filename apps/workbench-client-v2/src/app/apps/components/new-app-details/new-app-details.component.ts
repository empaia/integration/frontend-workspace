import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { NewAppEntity } from '@apps/store/new-apps/new-apps.models';

@Component({
  selector: 'app-new-app-details',
  templateUrl: './new-app-details.component.html',
  styleUrls: ['./new-app-details.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class NewAppDetailsComponent {
  @Input() public app!: NewAppEntity;

  goToLink(event: MouseEvent, url: string): void {
    window.open(url, '_blank');
    event.stopImmediatePropagation();
  }
}
