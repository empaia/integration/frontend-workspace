import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  Output,
} from '@angular/core';
import { AppEntity } from '@apps/store';
import { Scope, Token, WbsUrl } from 'vendor-app-integration';
import { ScopeEntity } from '@scope/store';
import { TokenEntity } from '@token/store';
import { Dictionary } from '@ngrx/entity';

@Component({
  selector: 'app-vendor-app-surface-list',
  templateUrl: './vendor-app-surface-list.component.html',
  styleUrls: ['./vendor-app-surface-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class VendorAppSurfaceListComponent {
  @Input() apps!: AppEntity[];
  @Input() focusAppId!: string;
  @Input() scopes!: Dictionary<ScopeEntity>;
  @Input() tokens!: Dictionary<TokenEntity>;
  @Input() wbsUrl!: WbsUrl;
  @Input() removedAppIds!: string[];

  @Output() receiveScopeReady = new EventEmitter<string>();
  @Output() receiveTokenReady = new EventEmitter<string>();
  @Output() receiveTokenRequest = new EventEmitter<string>();
  @Output() receiveWbsUrlReady = new EventEmitter<string>();

  getScope(app: AppEntity): Scope | undefined {
    return this.scopes[app.id]?.scope;
  }

  getToken(app: AppEntity): Token | undefined {
    return this.tokens[app.id]?.token;
  }
}
