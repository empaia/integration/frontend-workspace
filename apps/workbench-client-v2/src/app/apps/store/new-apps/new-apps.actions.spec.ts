import * as fromNewApps from './new-apps.actions';

describe('loadNewApps', () => {
  it('should return an action', () => {
    expect(fromNewApps.loadNewApps({
      caseId: 'CASE-ID'
    }).type).toBe('[NewApps] Load NewApps');
  });
});
