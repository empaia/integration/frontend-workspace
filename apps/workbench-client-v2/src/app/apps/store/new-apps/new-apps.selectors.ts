import { createSelector } from '@ngrx/store';
import {
  selectAppsFeatureState,
  State as ModuleState
} from '../apps-feature.state';
import { appsAdapter } from './new-apps.reducer';
import { selectAppsEntities } from '@apps/store/apps/apps.selectors';

const {
  selectAll,
} = appsAdapter.getSelectors();

// select feature sub state - rootState.appsFeature.newApps
export const selectNewAppsState = createSelector(
  selectAppsFeatureState,
  (state: ModuleState) => state.newApps
);

export const selectNewAppsPanelOpen = createSelector(
  selectNewAppsState,
  (state) => state.panelOpened
);

export const selectAllNewApps = createSelector(
  selectNewAppsState,
  (state) => selectAll(state)
);

export const selectHoverApp = createSelector(
  selectNewAppsState,
  (state) => state.hover
);

export const selectAllNewAppsWithUi = createSelector(
  selectAllNewApps,
  selectAppsEntities,
  (apps, appEntities) =>
    apps
      .map(app => ({ ...app, addedByUser: !!appEntities[app.id] }))
);

export const selectNewAppsLoaded = createSelector(
  selectNewAppsState,
  (state) => state.loaded
);
