import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { filter, map, switchMap } from 'rxjs/operators';
import { of } from 'rxjs';

import * as NewAppsActions from './new-apps.actions';
import * as AppsActions from '@apps/store/apps/apps.actions';
import * as MenuActions from '@menu/store/menu/menu.actions';
import * as CasesActions from '@cases/store/cases/cases.actions';
import { WbcV2CasesPanelService } from 'empaia-api-lib';
import { fetch } from '@ngrx/router-store/data-persistence';
import { Store } from '@ngrx/store';
import { ErrorSnackbarComponent } from '@shared/components/error-snackbar/error-snackbar.component';
import { MatSnackBar } from '@angular/material/snack-bar';
import { NEW_APPS_LOAD_ERROR } from '@menu/models/ui.models';
import { filterNullish } from '@helper/rxjs-operators';
import { HttpErrorResponse } from '@angular/common/http';
import { State } from './new-apps.reducer';
import { AppEntity } from '../apps/apps.models';

@Injectable()
export class NewAppsEffects {
  loadNewAppsOnCaseSelection$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(CasesActions.userSelectedCase, CasesActions.navigationSelectCase),
      map((action) => action.id),
      filterNullish(),
      map((caseId) => NewAppsActions.loadNewApps({ caseId }))
    );
  });

  loadNewApps$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(NewAppsActions.loadNewApps),
      fetch({
        run: (
          action: ReturnType<typeof NewAppsActions.loadNewApps>,
          _state: State
        ) => {
          return this.casesPanelService
            .casesCaseIdAppsGet({
              case_id: action.caseId,
            })
            .pipe(
              map((results) => results.items),
              // Augment with mockup images and data
              map((apps) => {
                const newApps: AppEntity[] = [];
                apps.forEach((app) => {
                  newApps.push({
                    ...app,
                    tissue: 'Breast',
                    stain: 'H&E',
                    indication: 'Breast cancer (NST/IDC)',
                    analysis: 'Detection, Segmentation',
                  });
                });
                return newApps;
              }),
              map((apps) => NewAppsActions.loadNewAppsSuccess({ apps }))
            );
        },
        onError: (
          _action: ReturnType<typeof NewAppsActions.loadNewApps>,
          error
        ) => {
          return NewAppsActions.loadNewAppsFailure({ error });
        },
      })
    );
  });

  newAppsLoadFailure$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(NewAppsActions.loadNewAppsFailure),
      map((action) => action.error),
      switchMap((error: HttpErrorResponse) => {
        this.snackBar.openFromComponent(ErrorSnackbarComponent, {
          data: { titel: NEW_APPS_LOAD_ERROR, error },
        });
        return of({ type: 'noop' });
      })
    );
  });

  // close new app panel when a menu button was clicked or an app was selected
  closeNewAppsPanel$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(
        AppsActions.userSelectedApp,
        MenuActions.openMenuMidi,
        MenuActions.openMenuMax
      ),
      filter(
        (action) =>
          !(
            action.type === '[Menu] Open Menu Midi' &&
            action.menuItem === 'Apps'
          )
      ),
      map(() => NewAppsActions.closeNewAppsPanel())
    );
  });

  constructor(
    private readonly actions$: Actions,
    private readonly casesPanelService: WbcV2CasesPanelService,
    private readonly store: Store,
    private readonly snackBar: MatSnackBar
  ) {}
}
