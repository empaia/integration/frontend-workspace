import { HttpErrorResponse } from '@angular/common/http';
import { createAction, props } from '@ngrx/store';
import { NewAppEntity } from '@apps/store/new-apps/new-apps.models';

export const loadNewApps = createAction(
  '[NewApps] Load NewApps',
  props<{ caseId: string }>()
);

export const loadNewAppsSuccess = createAction(
  '[NewApps] Load NewApps Success',
  props<{ apps: NewAppEntity[] }>()
);

export const loadNewAppsFailure = createAction(
  '[NewApps] Load NewApps Failure',
  props<{ error: HttpErrorResponse }>()
);

export const clearNewApps = createAction(
  '[NewApps] Clear'
);

export const openNewAppsPanel = createAction(
  '[NewApps] Open new Apps Panel',
);

export const closeNewAppsPanel = createAction(
  '[NewApps] Close new Apps Panel',
);

export const hoverOverApp = createAction(
  '[NewApps] Hover Over App',
  props<{ appId?: string | undefined }>()
);
