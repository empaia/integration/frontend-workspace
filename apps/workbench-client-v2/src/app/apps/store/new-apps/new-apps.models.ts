import { AppEntity } from '@apps/store/apps/apps.models';

export interface NewAppEntity extends AppEntity {
  addedByUser?: boolean;
}
