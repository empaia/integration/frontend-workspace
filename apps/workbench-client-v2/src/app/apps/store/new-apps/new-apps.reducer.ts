import { HttpErrorResponse } from '@angular/common/http';
import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';
import { Action, createReducer, on } from '@ngrx/store';
import * as NewAppsActions from './new-apps.actions';
import { NewAppEntity } from '@apps/store/new-apps/new-apps.models';

export const NEW_APPS_FEATURE_KEY = 'newApps';


export interface State extends EntityState<NewAppEntity> {
  loaded: boolean; // has the Apps list been loaded
  panelOpened: boolean;
  hover?: string | undefined;
  error?: HttpErrorResponse | null; // last known error (if any)
}

export const appsAdapter: EntityAdapter<NewAppEntity> = createEntityAdapter<NewAppEntity>();

export const initialState: State = appsAdapter.getInitialState({
  // set initial required properties
  loaded: true,
  panelOpened: false,
  hover: undefined,
});


export const newAppsReducer = createReducer(
  initialState,
  on(NewAppsActions.loadNewApps, (state): State => ({
    ...state,
    loaded: false,
    error: null,
  })),
  on(NewAppsActions.loadNewAppsSuccess, (state, { apps }): State =>
    appsAdapter.setAll(apps, {
      ...state,
      loaded: true
    })
  ),
  on(NewAppsActions.loadNewAppsFailure, (state, { error }): State => ({
    ...state,
    error,
    loaded: true
  })),
  on(NewAppsActions.clearNewApps, (state): State =>
    appsAdapter.removeAll({
      ...state,
    })
  ),
  on(NewAppsActions.openNewAppsPanel, (state): State => ({
    ...state,
    panelOpened: true,
  })),
  on(NewAppsActions.closeNewAppsPanel, (state): State => ({
    ...state,
    panelOpened: false,
  })),
  on(NewAppsActions.hoverOverApp, (state, { appId }): State => ({
    ...state,
    hover: appId,
  }))
);


export function reducer(state: State | undefined, action: Action) {
  return newAppsReducer(state, action);
}
