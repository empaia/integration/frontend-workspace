import { App } from "../apps/apps.models";

export interface VendorAppEntity extends App {
  appUrl?: string;
  iframe?: string;
}

export const DEFAULT_IFRAME_CONFIG = 'allow-scripts allow-popups';
export const DEFAULT_CONCURRENT_APPS = 5;
