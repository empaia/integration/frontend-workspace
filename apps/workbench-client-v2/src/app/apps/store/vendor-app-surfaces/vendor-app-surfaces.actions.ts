import { createAction, props } from '@ngrx/store';
import { VendorAppEntity } from '@apps/store/vendor-app-surfaces/vendor-app-surfaces.models';
import { HttpErrorResponse } from '@angular/common/http';

export const addActiveApp = createAction(
  '[VendorAppSurfaces] Add Active App',
  props<{ app: VendorAppEntity }>()
);

export const setActiveAppFocus = createAction(
  '[VendorAppSurfaces] Set Active App Focus',
  props<{ focus: string | undefined }>()
);

export const discardApps = createAction(
  '[VendorAppSurfaces] Discard Apps',
  props<{ appIds: string[] }>()
);

export const clearApps = createAction(
  '[VendorAppSurfaces] Clear Apps',
);

export const loadAppToken = createAction(
  '[VendorAppSurfaces] Load App Token',
  props<{ examinationId: string, app: VendorAppEntity }>()
);

export const loadAppTokenSuccess = createAction(
  '[VendorAppSurfaces] Load App Token Success',
  props<{ app: VendorAppEntity }>()
);

export const loadAppTokenFailure = createAction(
  '[VendorAppSurfaces] Load App Token Failure',
  props<{ error: HttpErrorResponse }>()
);
