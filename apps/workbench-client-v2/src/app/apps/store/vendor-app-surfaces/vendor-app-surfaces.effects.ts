import { Inject, Injectable } from '@angular/core';
import { Actions, concatLatestFrom, createEffect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { AppMappingService } from '@apps/services/app-mapping.service';
import * as VendorAppSurfacesActions from './vendor-app-surfaces.actions';
import * as VendorAppSurfacesSelectors from './vendor-app-surfaces.selectors';
import * as AppsActions from '@apps/store/apps/apps.actions';
import * as AppsSelectors from '@apps/store/apps/apps.selectors';
import * as CasesActions from '@cases/store/cases/cases.actions';
import * as ExaminationsSelectors from '@examinations/store/examinations/examinations.selectors';
import { filter, map } from 'rxjs/operators';
import { AppEntity } from '@apps/store';
import { filterNullish } from '@helper/rxjs-operators';
import { fetch } from '@ngrx/router-store/data-persistence';
import { WbcV2ExaminationsPanelService } from 'empaia-api-lib';
import { DEFAULT_IFRAME_CONFIG } from './vendor-app-surfaces.models';
import { State } from './vendor-app-surfaces.reducer';

@Injectable()
export class VendorAppSurfacesEffects {
  // clear apps when a new case or examination was selected
  clearOnCaseExaminationSelection$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(CasesActions.userSelectedCase, CasesActions.navigationSelectCase),
      map(() => VendorAppSurfacesActions.clearApps())
    );
  });

  // add an app when user selects an app in the menu
  addApp$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(AppsActions.userSelectedApp, AppsActions.loadAppsSuccess),
      concatLatestFrom(() =>
        this.store.select(AppsSelectors.selectSelected).pipe(filterNullish())
      ),
      map(([, app]) => app as AppEntity),
      map((app) => VendorAppSurfacesActions.addActiveApp({ app }))
    );
  });

  addActiveApp$ = createEffect(
    () => {
      return this.actions$.pipe(
        ofType(VendorAppSurfacesActions.addActiveApp),
        map((action) => action.app),
        map((app) => this.appIdSet.add(app.id))
      );
    },
    { dispatch: false }
  );

  checkCleanUpCache$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(VendorAppSurfacesActions.addActiveApp),
      concatLatestFrom(() => [
        this.store.select(VendorAppSurfacesSelectors.selectCurrentAppCacheSize),
        this.store.select(VendorAppSurfacesSelectors.selectMaxAppCacheSize),
      ]),
      filter(([, cacheSize, maxSize]) => cacheSize > maxSize),
      map(([, cacheSize, maxSize]) =>
        this.appIdSet.discard(cacheSize - maxSize)
      ),
      map((appIds) => VendorAppSurfacesActions.discardApps({ appIds }))
    );
  });

  clearApps$ = createEffect(
    () => {
      return this.actions$.pipe(
        ofType(VendorAppSurfacesActions.clearApps),
        map(() => this.appIdSet.clear())
      );
    },
    { dispatch: false }
  );

  addTokenUrlToApp$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(VendorAppSurfacesActions.addActiveApp),
      map((action) => action.app),
      concatLatestFrom(() => [
        this.store.select(VendorAppSurfacesSelectors.selectVendorAppMap),
        this.store
          .select(ExaminationsSelectors.selectSelectedId)
          .pipe(filterNullish()),
      ]),
      filter(
        ([app, appMap]) =>
          app.has_frontend &&
          !!appMap.get(app.id) &&
          !appMap.get(app.id)?.appUrl
      ),
      map(([app, _appMap, examinationId]) =>
        VendorAppSurfacesActions.loadAppToken({
          app,
          examinationId,
        })
      )
    );
  });

  loadAppToken$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(VendorAppSurfacesActions.loadAppToken),
      fetch({
        run: (
          action: ReturnType<typeof VendorAppSurfacesActions.loadAppToken>,
          _state: State
        ) => {
          return this.examinationsPanelService
            .examinationsExaminationIdAppsAppIdFrontendTokenGet({
              examination_id: action.examinationId,
              app_id: action.app.id,
            })
            .pipe(
              map(
                (token) =>
                  `${this.wbsServerUrl}/v2/frontends/${token.access_token}/`
              ),
              map((appUrl) => ({
                ...action.app,
                appUrl,
                iframe: DEFAULT_IFRAME_CONFIG,
              })),
              map((app) =>
                VendorAppSurfacesActions.loadAppTokenSuccess({ app })
              )
            );
        },
        onError: (
          _action: ReturnType<typeof VendorAppSurfacesActions.loadAppToken>,
          error
        ) => {
          return VendorAppSurfacesActions.loadAppTokenFailure({ error });
        },
      })
    );
  });

  // discard app on app close
  appClose$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(AppsActions.closeApp),
      map((action) => action.appId),
      map((appId) => VendorAppSurfacesActions.discardApps({ appIds: [appId] }))
    );
  });

  // set focus to last app pushed into cache after app was discarded
  // or set focus to undefined when list is empty
  unfocusedOnDiscard$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(VendorAppSurfacesActions.discardApps),
      map((action) => action.appIds),
      concatLatestFrom(() => [
        this.store
          .select(VendorAppSurfacesSelectors.selectFocusedAppId)
          .pipe(filterNullish()),
        this.store.select(VendorAppSurfacesSelectors.selectAllActiveAppIds),
      ]),
      filter(
        ([discardedIds, focusId]) => !!discardedIds.find((id) => id === focusId)
      ),
      map(([, _, activeAppIds]) =>
        VendorAppSurfacesActions.setActiveAppFocus({
          focus: activeAppIds?.length
            ? activeAppIds[activeAppIds.length - 1]
            : undefined,
        })
      )
    );
  });

  constructor(
    private actions$: Actions,
    private store: Store,
    private appIdSet: AppMappingService,
    private examinationsPanelService: WbcV2ExaminationsPanelService,
    @Inject('WBS_SERVER_API_URL') private wbsServerUrl: string
  ) {}
}
