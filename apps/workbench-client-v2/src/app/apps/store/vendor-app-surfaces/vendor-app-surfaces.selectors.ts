import { createSelector } from '@ngrx/store';
import {
  State as ModuleState,
  selectAppsFeatureState,
} from '../apps-feature.state';
import { vendorAppAdapter } from './vendor-app-surfaces.reducer';
import { VendorAppEntity } from '@apps/store/vendor-app-surfaces/vendor-app-surfaces.models';

const {
  selectAll,
  selectTotal,
  selectIds
} = vendorAppAdapter.getSelectors();

export const selectVendorAppSurfacesState = createSelector(
  selectAppsFeatureState,
  (state: ModuleState) => state.vendorAppSurfaces
);

export const selectAllActiveApps = createSelector(
  selectVendorAppSurfacesState,
  selectAll
);

export const selectActiveAppsEntities = createSelector(
  selectVendorAppSurfacesState,
  (state) => state.entityCache as ReadonlyMap<string, VendorAppEntity>
);

export const selectAllActiveAppIds = createSelector(
  selectVendorAppSurfacesState,
  selectIds,
);

export const selectFocusedAppId = createSelector(
  selectVendorAppSurfacesState,
  (state) => state.focus
);

export const selectFocusApp = createSelector(
  selectVendorAppSurfacesState,
  selectFocusedAppId,
  (state, focus) => focus ? state.entityCache.get(focus) : undefined
);

export const selectCurrentAppCacheSize = createSelector(
  selectVendorAppSurfacesState,
  selectTotal
);

export const selectMaxAppCacheSize = createSelector(
  selectVendorAppSurfacesState,
  (state) => state.cachedSizeConstraint
);

export const selectVendorAppMap = createSelector(
  selectVendorAppSurfacesState,
  (state) => state.entityCache
);

export const selectRemovedAppIds = createSelector(
  selectVendorAppSurfacesState,
  (state) => state.removed
);
