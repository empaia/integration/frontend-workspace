import * as fromVendorAppSurfaces from './vendor-app-surfaces.actions';

describe('loadVendorAppSurfaces', () => {
  it('should return an action', () => {
    expect(fromVendorAppSurfaces.addActiveApp({
      app: {
        id: 'APP-ID',
        app_version: 'v1',
        name_short: 'test-app',
        store_description: 'test app',
        store_icon_url: '',
        store_url: '',
        vendor_name: 'Test Vendor',
        has_frontend: false,
        research_only: false,
        store_docs_url: null,
        store_preview_after_url: null,
        store_preview_before_url: null,
      }
    }).type).toBe('[VendorAppSurfaces] Add Active App');
  });
});
