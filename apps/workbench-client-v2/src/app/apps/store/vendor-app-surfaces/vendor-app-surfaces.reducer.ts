import { createReducer, on } from '@ngrx/store';
import { createEntityCachingAdapter, EntityCachingAdapter, EntityCachingState } from '@shared/entity';
import * as VendorAppSurfacesActions from './vendor-app-surfaces.actions';
import { DEFAULT_CONCURRENT_APPS, VendorAppEntity } from '@apps/store/vendor-app-surfaces/vendor-app-surfaces.models';


export const VENDOR_APP_SURFACES_FEATURE_KEY = 'vendorAppSurfaces';

export interface State extends EntityCachingState<VendorAppEntity> {
  focus?: string;
  removed?: string[];
}

export const vendorAppAdapter: EntityCachingAdapter<VendorAppEntity> = createEntityCachingAdapter<VendorAppEntity>(
  (app) => app.id
);

export const initialState: State = vendorAppAdapter.getInitialState({
  focus: undefined,
}, DEFAULT_CONCURRENT_APPS);


export const reducer = createReducer(
  initialState,
  on(VendorAppSurfacesActions.addActiveApp, (state, { app }): State =>
    vendorAppAdapter.addOne(app, {
      ...state,
      focus: app.id,
    }),
  ),
  on(VendorAppSurfacesActions.setActiveAppFocus, (state, { focus }): State => ({
    ...state,
    focus,
  })),
  on(VendorAppSurfacesActions.discardApps, (state, { appIds }): State =>
    vendorAppAdapter.removeMany(appIds, {
      ...state,
      removed: appIds,
    }),
  ),
  on(VendorAppSurfacesActions.clearApps, (state): State =>
    vendorAppAdapter.removeAll({
      ...state,
      ...initialState,
      removed: [...state.entityCache.keys()]
    })
  ),
  on(VendorAppSurfacesActions.loadAppTokenSuccess, (state, { app }): State =>
    vendorAppAdapter.updateOne(app, {
      ...state
    })
  ),
);

