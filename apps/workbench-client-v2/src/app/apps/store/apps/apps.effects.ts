import { Injectable } from '@angular/core';
import { createEffect, Actions, ofType, concatLatestFrom } from '@ngrx/effects';
import { fetch } from '@ngrx/router-store/data-persistence';
import * as AppsFeature from './apps.reducer';
import * as AppsActions from './apps.actions';
import * as AppsSelectors from './apps.selectors';
import * as CasesActions from '@cases/store/cases/cases.actions';
import * as ExaminationsActions from '@examinations/store/examinations/examinations.actions';
import * as RouterSelectors from '@router/store/router.selectors';
import * as ExaminationsSelectors from '@examinations/store/examinations/examinations.selectors';
import * as MenuActions from '@menu/store/menu/menu.actions';
import * as VendorAppSurfacesActions from '@apps/store/vendor-app-surfaces/vendor-app-surfaces.actions';
import * as VendorAppSurfacesSelectors from '@apps/store/vendor-app-surfaces/vendor-app-surfaces.selectors';
import { WbcV2ExaminationsPanelService } from 'empaia-api-lib';
import {
  catchError,
  concatMap,
  distinctUntilChanged,
  filter,
  map,
  switchMap,
  takeUntil,
  tap,
} from 'rxjs/operators';
import { filterNullish, mergeTakeOne } from '@helper/rxjs-operators';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { ROUTE_APPS, ROUTE_CASES, ROUTE_EXAMINATIONS } from '@router/routes';
import { of, timer } from 'rxjs';
import { APPS_POLLING_PERIOD } from '@apps/store';

@Injectable()
export class AppsEffects {
  clearApps$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(CasesActions.userSelectedCase, CasesActions.navigationSelectCase),
      map(() => AppsActions.clearApps())
    );
  });

  userSelectedExamination$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(
        ExaminationsActions.userSelectedExamination,
        ExaminationsActions.navigationSelectExamination
      ),
      map((action) => action.caseExaminationSelection.examinationId),
      filter((examId) => !!examId),
      map((examId) => AppsActions.loadApps({ examinationId: examId }))
    );
  });

  load$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(AppsActions.loadApps),
      fetch({
        run: (
          action: ReturnType<typeof AppsActions.loadApps>,
          _state: AppsFeature.State
        ) => {
          return this.examinationsPanelService
            .examinationsExaminationIdAppsGet({
              examination_id: action.examinationId,
            })
            .pipe(
              map((apps) => apps.items),
              map((apps) => AppsActions.loadAppsSuccess({ apps }))
            );
        },
        onError: (_action: ReturnType<typeof AppsActions.loadApps>, error) => {
          return AppsActions.loadAppsFailure({ error });
        },
      })
    );
  });

  routerSelectedApp$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(AppsActions.navigationSelectApp),
      map((action) => action.appId),
      filter((appId) => !!appId),
      map((appId) => AppsActions.userSelectedApp({ appId: appId }))
    );
  });

  appSelectedUpdateRoute$ = createEffect(
    () => {
      return this.actions$.pipe(
        ofType(AppsActions.userSelectedApp),
        map((action) => action.appId),
        mergeTakeOne(
          this.store.select(RouterSelectors.selectCaseIdParam),
          this.store.select(RouterSelectors.selectExaminationIdParam)
        ),
        tap(([appId, caseId, examinationId]) =>
          this.router.navigate(
            [
              ROUTE_CASES,
              caseId,
              ROUTE_EXAMINATIONS,
              examinationId,
              ROUTE_APPS,
              appId,
            ],
            { queryParamsHandling: 'merge' }
          )
        )
      );
    },
    { dispatch: false }
  );

  // select preselected app if already running
  changeApp$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(AppsActions.preSelectApp),
      map((action) => action.appId),
      filterNullish(),
      concatLatestFrom(() =>
        this.store.select(VendorAppSurfacesSelectors.selectActiveAppsEntities)
      ),
      filter(([appId, appEntities]) => !!appEntities.get(appId)),
      map(([appId]) => AppsActions.userSelectedApp({ appId }))
    );
  });

  // unselect preselected app when an app was selected
  discardPreSelectedApp$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(AppsActions.navigationSelectApp, AppsActions.userSelectedApp),
      map(() => AppsActions.preSelectApp({ appId: undefined }))
    );
  });

  addAppToExamination$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(AppsActions.addAppFromPanel),
      map((action) => action.app),
      concatLatestFrom(() =>
        this.store.select(ExaminationsSelectors.selectSelectedId)
      ),
      filter(([_app, ex_id]) => !!ex_id),
      concatMap(([app, ex_id]) => {
        return this.examinationsPanelService
          .examinationsExaminationIdAppsAppIdPut({
            examination_id: ex_id as string,
            app_id: app.id,
          })
          .pipe(
            map((app) => app.app_id),
            map((appId) => AppsActions.addAppFromPanelSuccess({ appId })),
            catchError((error) =>
              of(AppsActions.addAppFromPanelFailure({ error }))
            )
          );
      })
    );
  });

  appsPolling$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(AppsActions.startPollingApps),
      switchMap(() =>
        timer(0, APPS_POLLING_PERIOD).pipe(
          takeUntil(this.actions$.pipe(ofType(AppsActions.stopPollingApps))),
          concatLatestFrom(() =>
            this.store.select(ExaminationsSelectors.selectSelectedId)
          ),
          map(([, examinationId]) => examinationId),
          filterNullish(),
          map((examinationId) => AppsActions.loadApps({ examinationId }))
        )
      )
    );
  });

  // start polling when apps menu is open and stop polling
  // when apps meni was closed
  appsPollingMenuManagement$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(
        MenuActions.openMenuMax,
        MenuActions.openMenuMidi,
        MenuActions.clickMenuButton
      ),
      map((action) => action.menuItem),
      distinctUntilChanged(),
      map((menuItem) =>
        menuItem === 'Apps'
          ? AppsActions.startPollingApps()
          : AppsActions.stopPollingApps()
      )
    );
  });

  // remove app id from router when the current selection app was closed
  removeAppIdOnClose$ = createEffect(
    () => {
      return this.actions$.pipe(
        ofType(AppsActions.closeApp),
        map((action) => action.appId),
        concatLatestFrom(() => [
          this.store
            .select(AppsSelectors.selectSelectedId)
            .pipe(filterNullish()),
          this.store.select(RouterSelectors.selectCaseIdParam),
          this.store.select(RouterSelectors.selectExaminationIdParam),
        ]),
        filter(([closeId, appId]) => closeId === appId),
        map(([_closeId, _appId, caseId, examId]) =>
          this.router.navigate(
            [ROUTE_CASES, caseId, ROUTE_EXAMINATIONS, examId],
            { queryParamsHandling: 'merge' }
          )
        )
      );
    },
    { dispatch: false }
  );

  updateAppIdOnDiscard$ = createEffect(
    () => {
      return this.actions$.pipe(
        ofType(VendorAppSurfacesActions.discardApps),
        map((action) => action.appIds),
        concatLatestFrom(() => [
          this.store.select(VendorAppSurfacesSelectors.selectAllActiveAppIds),
          this.store
            .select(VendorAppSurfacesSelectors.selectFocusedAppId)
            .pipe(filterNullish()),
          this.store.select(RouterSelectors.selectCaseIdParam),
          this.store.select(RouterSelectors.selectExaminationIdParam),
        ]),
        filter(
          ([discardedIds, activeAppIds, focusId]) =>
            !!activeAppIds?.length &&
            !!discardedIds.find((id) => id === focusId)
        ),
        map(([, activeAppIds, _focusId, caseId, examId]) =>
          this.router.navigate(
            [
              ROUTE_CASES,
              caseId,
              ROUTE_EXAMINATIONS,
              examId,
              ROUTE_APPS,
              activeAppIds[activeAppIds.length - 1],
            ],
            { queryParamsHandling: 'merge' }
          )
        )
      );
    },
    { dispatch: false }
  );

  // clear expanded apps when user left apps panel
  clearExpandedApps$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(MenuActions.openMenuMidi, MenuActions.clickMenuButton),
      map((action) => action.menuItem),
      filter((id) => id !== 'Apps'),
      map(() => AppsActions.collapseAllApps())
    );
  });

  constructor(
    private readonly actions$: Actions,
    private readonly examinationsPanelService: WbcV2ExaminationsPanelService,
    private readonly store: Store,
    private readonly router: Router
  ) {}
}
