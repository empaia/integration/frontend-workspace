import * as AppsActions from './apps.actions';
import { App } from './apps.models';
import { State, initialState, reducer } from './apps.reducer';

describe('Apps Reducer', () => {
  const createAppsEntity = (id: string, name = '') =>
    ({
      app_version: '0',
      id: id,
      name_short: name,
      store_description: 'desc',
      store_icon_url: '',
      store_url: '',
      vendor_name: '',
    } as App);

  describe('valid Apps actions', () => {
    it('loadAppsSuccess should return set the list of known Apps', () => {
      const apps = [
        createAppsEntity('PRODUCT-AAA'),
        createAppsEntity('PRODUCT-zzz'),
      ];
      const action = AppsActions.loadAppsSuccess({ apps });

      const result: State = reducer(initialState, action);

      expect(result.loaded).toBe(true);
      expect(result.ids.length).toBe(2);
    });
  });

  describe('unknown action', () => {
    it('should return the previous state', () => {
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      const action = {} as any;

      const result = reducer(initialState, action);

      expect(result).toBe(initialState);
    });
  });
});
