import { createSelector } from '@ngrx/store';
import {
  State as ModuleState,
  selectAppsFeatureState,
} from '../apps-feature.state';
import {
  State,
  appsAdapter,
  appsExpansionAdapter,
} from './apps.reducer';
import * as RouterSelectors from '@router/store/router.selectors';
import { SelectionError } from '@menu/models/ui.models';

// Lookup the 'Apps' feature state managed by NgRx
export const selectAppsState = createSelector(
  selectAppsFeatureState,
  (state: ModuleState) => state.apps
);

const { selectAll, selectEntities } = appsAdapter.getSelectors();

export const selectAppsLoaded = createSelector(
  selectAppsState,
  (state: State) => state.loaded
);

export const selectAppsError = createSelector(
  selectAppsState,
  (state: State) => state.error
);

export const selectAllApps = createSelector(
  selectAppsState,
  (state: State) => selectAll(state)
);

export const selectAppsEntities = createSelector(
  selectAppsState, (state: State) => selectEntities(state)
);

export const selectSelectedId = createSelector(
  RouterSelectors.selectAppIdParam,
  (id) => id
);

export const selectSelected = createSelector(
  selectAppsEntities,
  selectSelectedId,
  (entities, selectedId) => selectedId && entities[selectedId]
);

export const selectPreSelected = createSelector(
  selectAppsState,
  (state) => state.preselected
);

export const selectExpandedAppEntities = createSelector(
  selectAppsState,
  (state) => appsExpansionAdapter.getSelectors().selectEntities(state.expansion)
);

export const selectSelectionError = createSelector(
  RouterSelectors.selectCaseIdParam,
  RouterSelectors.selectExaminationIdParam,
  (caseId: string | undefined, examId: string | undefined): SelectionError | undefined => {
    if (!caseId) { return  'NO_CASE_SELECTION'; }
    if (!examId) { return 'NO_EXAMINATION_SELECTION'; }
    return undefined;
  }
);
