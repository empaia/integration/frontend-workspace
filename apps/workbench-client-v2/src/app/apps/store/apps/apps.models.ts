import { WbcV2AppInput } from 'empaia-api-lib';

export type App = WbcV2AppInput;

export interface AppEntity extends App {
  tissue?: string;
  stain?: string;
  indication?: string;
  analysis?: string;
}

export interface AppExpansion {
  id: string;
}

export const APPS_POLLING_PERIOD = 30000;

export const MAX_APP_DESCRIPTION = 100;
