import { createAction, props } from '@ngrx/store';
import { App, AppEntity } from '@apps/store/apps/apps.models';
import { HttpErrorResponse } from '@angular/common/http';

export const loadApps = createAction(
  '[Apps Page] Load Apps',
  props<{ examinationId: string }>()
);

export const loadAppsSuccess = createAction(
  '[Apps/API] Load Apps Success',
  props<{ apps: AppEntity[] }>()
);

export const loadAppsFailure = createAction(
  '[Apps/API] Load Apps Failure',
  props<{ error: HttpErrorResponse }>()
);

export const preSelectApp = createAction(
  '[Apps] Pre Select App',
  props<{ appId : string | undefined }>()
);

export const userSelectedApp = createAction(
  '[Apps] User Selected App',
  props<{ appId: string }>()
);

export const navigationSelectApp = createAction(
  '[Apps/API] Navigation Select App',
  props<{ appId: string }>()
);

export const clearApps = createAction(
  '[Apps] Clear Apps',
);

export const addAppFromPanel = createAction(
  '[Apps] Add App',
  props<{ app: App }>()
);

export const addAppFromPanelSuccess = createAction(
  '[Apps] Add App Success',
  props<{ appId: string }>()
);

export const addAppFromPanelFailure = createAction(
  '[Apps] Add App Failure',
  props<{ error: HttpErrorResponse }>()
);

export const startPollingApps = createAction(
  '[Apps] Start Polling Apps',
);

export const stopPollingApps = createAction(
  '[Apps] Stop Polling Apps',
);

export const closeApp = createAction(
  '[Apps] Close App',
  props<{ appId: string }>()
);

export const expandApp = createAction(
  '[Apps] Expand App',
  props<{ id: string }>()
);

export const collapseApp = createAction(
  '[Apps] Collapse App',
  props<{ id: string }>()
);

export const collapseAllApps = createAction(
  '[Apps] Collapse All Apps',
);
