import { appsAdapter, initialState } from './apps.reducer';
import * as AppsSelectors from './apps.selectors';
import { App, AppEntity } from '@apps/store/apps/apps.models';

describe('Apps Selectors', () => {
  const ERROR_MSG = 'No Error Available';
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  const getAppsId = (a: AppEntity) => a.id;
  const createAppsEntity = (id: string, name = '') =>
    ({
      app_version: '0',
      id: id,
      name_short: name,
      store_description: 'desc',
      store_icon_url: '',
      store_url: '',
      vendor_name: '',
    } as App);

  // eslint-disable-next-line @typescript-eslint/ban-types
  let state: object;

  beforeEach(() => {
    state = {
      appsModuleFeature: {
        apps: appsAdapter.setAll(
          [
            createAppsEntity('PRODUCT-AAA'),
            createAppsEntity('PRODUCT-BBB'),
            createAppsEntity('PRODUCT-CCC'),
          ],
          {
            ...initialState,
            selectedId: 'PRODUCT-BBB',
            error: ERROR_MSG,
            loaded: true,
          }
        ),
      }
    };
  });

  describe('Apps Selectors', () => {
    it('getAllApps() should return the list of Apps', () => {
      const results = AppsSelectors.selectAllApps(state);
      const selId = getAppsId(results[1]);

      expect(results.length).toBe(3);
      expect(selId).toBe('PRODUCT-BBB');
    });

    // TODO: refactor test with mocked out router
    /* it('getSelected() should return the selected Entity', () => {
      const result = AppsSelectors.selectSelected(state);
      const selId = getAppsId(result as AppEntity);

      expect(selId).toBe('PRODUCT-BBB');
    }); */

    it("getAppsLoaded() should return the current 'loaded' status", () => {
      const result = AppsSelectors.selectAppsLoaded(state);

      expect(result).toBe(true);
    });

    it("getAppsError() should return the current 'error' state", () => {
      const result = AppsSelectors.selectAppsError(state);

      expect(result).toBe(ERROR_MSG);
    });
  });
});
