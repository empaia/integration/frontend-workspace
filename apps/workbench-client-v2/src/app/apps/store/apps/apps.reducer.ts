import { createReducer, on, Action } from '@ngrx/store';
import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';

import * as AppsActions from './apps.actions';
import { HttpErrorResponse } from '@angular/common/http';
import { AppEntity, AppExpansion } from '@apps/store/apps/apps.models';

export const APPS_FEATURE_KEY = 'apps';

export interface State extends EntityState<AppEntity> {
  loaded: boolean; // has the Apps list been loaded
  preselected: string | undefined;
  expansion: EntityState<AppExpansion>;
  error?: HttpErrorResponse; // last known error (if any)
}

export const appsAdapter: EntityAdapter<AppEntity> = createEntityAdapter<AppEntity>();
export const appsExpansionAdapter: EntityAdapter<AppExpansion> = createEntityAdapter<AppExpansion>();

export const initialState: State = appsAdapter.getInitialState({
  // set initial required properties
  loaded: true,
  preselected: undefined,
  expansion: appsExpansionAdapter.getInitialState(),
});

const appsReducer = createReducer(
  initialState,
  on(AppsActions.loadApps, (state): State => ({
    ...state,
    loaded: false,
    error: undefined
  })),
  on(AppsActions.loadAppsSuccess, (state, { apps }) =>
    appsAdapter.setAll(apps, { ...state, loaded: true })
  ),
  on(AppsActions.loadAppsFailure, (state, { error }): State => ({
    ...state,
    loaded: true,
    error
  })),
  on(AppsActions.clearApps, (state) =>
    appsAdapter.removeAll(state)
  ),
  on(AppsActions.addAppFromPanel, (state, { app }): State =>
    appsAdapter.addOne(app, {
      ...state,
    })
  ),
  on(AppsActions.preSelectApp, (state, { appId }): State => ({
    ...state,
    preselected: appId,
  })),
  on(AppsActions.expandApp, (state, { id }): State => ({
    ...state,
    expansion: appsExpansionAdapter.upsertOne({ id }, {
      ...state.expansion
    })
  })),
  on(AppsActions.collapseApp, (state, { id }): State => ({
    ...state,
    expansion: appsExpansionAdapter.removeOne(id, {
      ...state.expansion
    })
  })),
  on(AppsActions.collapseAllApps, (state): State => ({
    ...state,
    expansion: appsExpansionAdapter.removeAll({
      ...state.expansion
    })
  }))
);

export function reducer(state: State | undefined, action: Action) {
  return appsReducer(state, action);
}
