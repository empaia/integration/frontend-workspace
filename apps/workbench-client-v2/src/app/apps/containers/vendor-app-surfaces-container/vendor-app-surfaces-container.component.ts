import { ChangeDetectionStrategy, Component } from '@angular/core';
import { Observable } from 'rxjs';
import { AppEntity, VendorAppSurfacesSelectors } from '@apps/store';
import { Store } from '@ngrx/store';
import { ScopeActions, ScopeEntity, ScopeSelectors } from '@scope/store';
import { Dictionary } from '@ngrx/entity';
import { TokenActions, TokenEntity, TokenSelectors } from '@token/store';
import { WbsUrl } from 'vendor-app-integration';
import { WbsUrlActions, WbsUrlSelectors } from '@wbsUrl/store';

@Component({
  selector: 'app-vendor-app-surfaces-container',
  templateUrl: './vendor-app-surfaces-container.component.html',
  styleUrls: ['./vendor-app-surfaces-container.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class VendorAppSurfacesContainerComponent {
  public activeApps$: Observable<AppEntity[]>;
  public focusedAppId$: Observable<string | undefined>;
  public scopes$: Observable<Dictionary<ScopeEntity>>;
  public tokens$: Observable<Dictionary<TokenEntity>>;
  public wbsUrl$: Observable<WbsUrl | undefined>;
  public removedAppIds$: Observable<string[] | undefined>;

  constructor(private store: Store) {
    this.activeApps$ = this.store.select(
      VendorAppSurfacesSelectors.selectAllActiveApps
    );
    this.focusedAppId$ = this.store.select(
      VendorAppSurfacesSelectors.selectFocusedAppId
    );
    this.scopes$ = this.store.select(ScopeSelectors.selectScopeEntities);
    this.tokens$ = this.store.select(TokenSelectors.selectTokenEntities);
    this.wbsUrl$ = this.store.select(WbsUrlSelectors.selectWbsUrl);
    this.removedAppIds$ = this.store.select(
      VendorAppSurfacesSelectors.selectRemovedAppIds
    );
  }

  onScopeReady(appId: string): void {
    this.store.dispatch(ScopeActions.loadAppScope({ appId }));
  }

  onTokenRequest(appId: string): void {
    this.store.dispatch(TokenActions.loadAppToken({ appId }));
  }

  onWbsUrlReady(): void {
    this.store.dispatch(WbsUrlActions.loadWbsUrl());
  }
}
