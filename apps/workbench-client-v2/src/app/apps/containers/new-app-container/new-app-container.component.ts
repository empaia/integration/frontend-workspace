import { ChangeDetectionStrategy, Component, Inject } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppEntity, AppsActions, NewAppsActions, NewAppsSelectors } from '@apps/store';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-new-app-container',
  templateUrl: './new-app-container.component.html',
  styleUrls: ['./new-app-container.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class NewAppContainerComponent {
  public open$: Observable<boolean>;
  public apps$: Observable<AppEntity[]>;
  public appsLoaded$: Observable<boolean>;

  constructor(
    private store: Store,
    @Inject('SOLUTION_STORE_URL') public solutionStoreUrl: string,
  ) {
    this.open$ = this.store.select(NewAppsSelectors.selectNewAppsPanelOpen);
    this.apps$ = this.store.select(NewAppsSelectors.selectAllNewAppsWithUi);
    this.appsLoaded$ = this.store.select(NewAppsSelectors.selectNewAppsLoaded);
  }

  public addApp(app: AppEntity): void {
    this.store.dispatch(AppsActions.addAppFromPanel({ app }));
  }

  public closePanel(): void {
    this.store.dispatch(NewAppsActions.closeNewAppsPanel());
  }

  public hoverOverApp(appId?: string | undefined): void {
    this.store.dispatch(NewAppsActions.hoverOverApp({ appId }));
  }
}
