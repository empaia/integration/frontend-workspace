import { NewAppContainerComponent } from './new-app-container.component';
import { createComponentFactory, Spectator } from '@ngneat/spectator';
import { MockComponents } from 'ng-mocks';
import { NewAppPanelComponent } from '@apps/components/new-app-panel/new-app-panel.component';
import { provideMockStore } from '@ngrx/store/testing';
import { NewAppsSelectors } from '@apps/store';

describe('NewAppContainerComponent', () => {
  let spectator: Spectator<NewAppContainerComponent>;
  const createComponent = createComponentFactory({
    component: NewAppContainerComponent,
    declarations: [
      MockComponents(
        NewAppPanelComponent
      ),
    ],
    providers: [
      provideMockStore({
        selectors: [
          {
            selector: NewAppsSelectors.selectNewAppsPanelOpen,
            value: true,
          },
          {
            selector: NewAppsSelectors.selectAllNewApps,
            value: []
          }
        ]
      }),
      { provide: 'SOLUTION_STORE_URL', useValue: 'Solution_Store_Url' }
    ]
  });

  beforeEach(() => spectator = createComponent());

  it('should create', () => {
    expect(spectator.component).toBeTruthy();
  });
});
