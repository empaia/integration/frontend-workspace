import { ChangeDetectionStrategy, Component } from '@angular/core';
import {
  App,
  AppEntity, AppExpansion,
  AppsActions,
  AppsSelectors,
  NewAppsActions,
  NewAppsSelectors,
  VendorAppSurfacesSelectors
} from '@apps/store';
import { Examination, ExaminationsSelectors } from '@examinations/store';
import { IdSelection, SelectionError } from '@menu/models/ui.models';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { MenuEntity, MenuItem } from '@menu/models/menu.models';
import { MenuActions, MenuSelectors } from '@menu/store';
import { VendorAppEntity } from '@apps/store/vendor-app-surfaces/vendor-app-surfaces.models';
import { Dictionary } from '@ngrx/entity';

@Component({
  selector: 'app-apps-container',
  templateUrl: './apps-container.component.html',
  styleUrls: ['./apps-container.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AppsContainerComponent {

  public apps$: Observable<App[]>;
  public selectedAppId$: Observable<string | undefined>;
  public selectedExaminationId$: Observable<string | undefined>;
  public selectedApp$:Observable<AppEntity | undefined | string>;
  public selectedExamination$: Observable<Examination | undefined>;
  public preselectedApp$: Observable<string | undefined>;
  public appsMenu$: Observable<MenuEntity>;
  public loaded$: Observable<boolean>;
  public appsExpansionDictionary$: Observable<Dictionary<AppExpansion>>;
  public hoverNewApp$: Observable<string | undefined>;

  public runningApps$: Observable<ReadonlyMap<string, VendorAppEntity>>;
  public activeApp$: Observable<string | undefined>;

  public selectionError$: Observable<SelectionError | undefined>;

  constructor(private readonly store: Store) {
    this.apps$ = this.store.select(AppsSelectors.selectAllApps);
    this.selectedAppId$ = this.store.select(AppsSelectors.selectSelectedId);
    this.selectedExaminationId$ = this.store.select(ExaminationsSelectors.selectSelectedId);
    this.selectionError$ = this.store.select(AppsSelectors.selectSelectionError);
    this.selectedApp$ = this.store.select(AppsSelectors.selectSelected);
    this.selectedExamination$ = this.store.select(ExaminationsSelectors.selectSelected);
    this.appsMenu$ = this.store.select(MenuSelectors.selectAppsMenu);
    this.loaded$ = this.store.select(AppsSelectors.selectAppsLoaded);
    this.runningApps$ = this.store.select(VendorAppSurfacesSelectors.selectActiveAppsEntities);
    this.activeApp$ = this.store.select(VendorAppSurfacesSelectors.selectFocusedAppId);
    this.preselectedApp$ = this.store.select(AppsSelectors.selectPreSelected);
    this.appsExpansionDictionary$ = this.store.select(AppsSelectors.selectExpandedAppEntities);
    this.hoverNewApp$ = this.store.select(NewAppsSelectors.selectHoverApp);
  }

  public selectApp(selected: IdSelection): void {
    this.store.dispatch(AppsActions.userSelectedApp({ appId: selected.id }));
  }

  public preselectApp(preselected: IdSelection | undefined): void {
    this.store.dispatch(AppsActions.preSelectApp({ appId: preselected ? preselected.id : undefined }));
  }

  public addNewApp($event: MouseEvent) {
    this.store.dispatch(NewAppsActions.openNewAppsPanel());
    $event.stopImmediatePropagation();
  }

  public clickMenuButton(menuItem: MenuItem): void {
    this.store.dispatch(MenuActions.clickMenuButton({ menuItem }));
  }

  public onAppClose(item: IdSelection): void {
    this.store.dispatch(AppsActions.closeApp({ appId: item.id }));
  }

  public onExpandApp(id: string): void {
    this.store.dispatch(AppsActions.expandApp({ id }));
  }

  public onCollapseApp(id: string): void {
    this.store.dispatch(AppsActions.collapseApp({ id }));
  }
}
