import { MaterialModule } from '@material/material.module';
import { createComponentFactory, Spectator } from '@ngneat/spectator';
import { StoreModule } from '@ngrx/store';
import { provideMockStore } from '@ngrx/store/testing';
import { CopyToClipboardDirective } from '@shared/directives/copy-to-clipboard.directive';
import { MockComponents } from 'ng-mocks';

import { AppsContainerComponent } from './apps-container.component';
import { AppsListComponent } from '@apps/components/apps-list/apps-list.component';
import { AppsSelectors } from '@apps/store';
import { ExaminationsSelectors } from '@examinations/store';
import { MenuSelectors } from '@menu/store';
import { MenuItemLayoutComponent } from '@shared/containers/menu-item-layout/menu-item-layout.component';
import { AppsLabelComponent } from '@apps/components/apps-label/apps-label.component';

describe('AppsContainerComponent', () => {
  let spectator: Spectator<AppsContainerComponent>;

  const createComponent = createComponentFactory({
    component: AppsContainerComponent,
    imports: [
      StoreModule.forRoot({}),
      MaterialModule,
    ],
    declarations: [
      MockComponents(
        AppsListComponent,
        MenuItemLayoutComponent,
        AppsLabelComponent,
      ),
      CopyToClipboardDirective,
    ],
    providers: [provideMockStore({
      selectors: [
        {
          selector: AppsSelectors.selectAllApps,
          value: []
        },
        {
          selector: AppsSelectors.selectSelectedId,
          value: ''
        },
        {
          selector: ExaminationsSelectors.selectSelectedId,
          value: ''
        },
        {
          selector: AppsSelectors.selectSelectionError,
          value: ''
        },
        {
          selector: AppsSelectors.selectSelected,
          value: {}
        },
        {
          selector: ExaminationsSelectors.selectSelected,
          value: {}
        },
        {
          selector: MenuSelectors.selectAppsMenu,
          value: {}
        },
        {
          selector: AppsSelectors.selectAppsLoaded,
          value: true
        }
      ]
    })],
  });

  it('should create', () => {
    spectator = createComponent();
    expect(spectator.component).toBeTruthy();
  });
});
