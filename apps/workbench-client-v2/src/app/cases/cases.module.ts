import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CasesContainerComponent } from './containers/cases-container/cases-container.component';
import { SharedModule } from '@shared/shared.module';
import { MaterialModule } from '@material/material.module';
import { StoreModule } from '@ngrx/store';
import { CasesEffects } from './store';
import { EffectsModule } from '@ngrx/effects';
import { CasesListMaxComponent } from './components/cases-list-max/cases-list-max.component';
import { CASES_MODULE_FEATURE_KEY, reducers } from '@cases/store/cases-feature.state';
import { CasesLabelComponent } from './components/cases-label/cases-label.component';
import { CaseItemComponent } from './components/case-item/case-item.component';
import { DictParserPipe } from '@shared/pipes/dict-parser.pipe';
import { CaseExaminationListComponent } from './components/case-examination-list/case-examination-list.component';
import { CaseExaminationItemComponent } from './components/case-examination-item/case-examination-item.component';

const COMPONENTS = [
  CasesContainerComponent,
  CasesListMaxComponent,
  CasesLabelComponent,
  CaseItemComponent,
  CaseExaminationListComponent,
  CaseExaminationItemComponent,
];

@NgModule({
  declarations: COMPONENTS,
  imports: [
    CommonModule,
    SharedModule,
    MaterialModule,
    StoreModule.forFeature(
      CASES_MODULE_FEATURE_KEY,
      reducers,
    ),
    EffectsModule.forFeature([CasesEffects]),
  ],
  providers: [
    DictParserPipe,
  ],
  exports: [
    CasesContainerComponent,
    CasesLabelComponent,
  ],
})
export class CasesModule { }
