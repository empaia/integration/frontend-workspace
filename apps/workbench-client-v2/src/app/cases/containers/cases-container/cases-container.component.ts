import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';
import { CasesSelectors, CasesActions, CaseExpansion, CASE_LIST_LIMIT, Case } from '@cases/store';
import { MenuEntity, MenuItem, MenuState } from '@menu/models/menu.models';
import { IdSelection } from '@menu/models/ui.models';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { MenuActions, MenuSelectors } from '@menu/store';
import { Dictionary } from '@ngrx/entity';
import { CaseExaminationSelection } from '@examinations/store/examinations/examinations.models';
import { ExaminationsActions, ExaminationsSelectors } from '@examinations/store';

@Component({
  selector: 'app-cases-container',
  templateUrl: './cases-container.component.html',
  styleUrls: ['./cases-container.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CasesContainerComponent implements OnInit {

  @Input() menuState!: MenuState;

  public cases$: Observable<Case[]>;
  public selectedCaseId$: Observable<string | undefined>;
  public selectedExaminationId$: Observable<string | undefined>;
  public casesExpansionDictionary$: Observable<Dictionary<CaseExpansion>>;
  public casesMenu$: Observable<MenuEntity>;
  public caseSelected$: Observable<Case | undefined>;
  public loaded$: Observable<boolean>;
  public casesCount$: Observable<number>;
  public casesSkip$: Observable<number>;
  public readonly CASE_LIST_LIMIT = CASE_LIST_LIMIT;

  constructor(private readonly store: Store) {
    this.cases$ = this.store.select(CasesSelectors.selectAllCases);
    this.selectedCaseId$ = this.store.select(CasesSelectors.selectSelectedCaseId);
    this.selectedExaminationId$ = this.store.select(ExaminationsSelectors.selectSelectedId);
    this.casesExpansionDictionary$ = this.store.select(CasesSelectors.selectExpandedCaseEntities);
    this.casesMenu$ = this.store.select(MenuSelectors.selectCasesMenu);
    this.caseSelected$ = this.store.select(CasesSelectors.selectSelectedCase);
    this.loaded$ = this.store.select(CasesSelectors.selectCasesLoaded);
    this.casesCount$ = this.store.select(CasesSelectors.selectCasesCount);
    this.casesSkip$ = this.store.select(CasesSelectors.selectCaseListSkip);
  }

  ngOnInit(): void {
    this.store.dispatch(CasesActions.loadCasePage({ skip: 0 }));
  }

  public onExpandCase(id: string): void {
    this.store.dispatch(CasesActions.expandCase({ id }));
  }

  public onCollapseCase(id: string): void {
    this.store.dispatch(CasesActions.collapseCase({ id }));
  }

  public onCaseLabelClick(menuItem: MenuItem): void {
    this.store.dispatch(MenuActions.clickMenuButton({ menuItem }));
  }

  public onLoadCasePage(skip: number): void {
    this.store.dispatch(CasesActions.loadCasePage({ skip }));
  }

  public onSelectCaseExamination(caseExaminationSelection: CaseExaminationSelection): void {
    this.store.dispatch(ExaminationsActions.userSelectedExamination({ caseExaminationSelection }));
  }

  public onCreateExamination(idSelection: IdSelection): void {
    this.store.dispatch(ExaminationsActions.createExamination({ caseId: idSelection.id }));
  }

  public onCloseExamination(caseExaminationSelection: CaseExaminationSelection): void {
    this.store.dispatch(ExaminationsActions.closeExamination({ caseExaminationSelection }));
  }
}
