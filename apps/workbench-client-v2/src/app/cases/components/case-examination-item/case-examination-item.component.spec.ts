import { CaseExaminationItemComponent } from './case-examination-item.component';
import { createComponentFactory, Spectator } from '@ngneat/spectator';
import { MockPipes } from 'ng-mocks';
import { CustomDatePipe } from '@shared/pipes/custom-date.pipe';
import { WbcV2ExaminationState } from 'empaia-api-lib';

describe('CaseExaminationItemComponent', () => {
  let spectator: Spectator<CaseExaminationItemComponent>;
  const createComponent = createComponentFactory({
    component: CaseExaminationItemComponent,
    declarations: [
      MockPipes(
        CustomDatePipe
      )
    ]
  });

  beforeEach(() => spectator = createComponent({
    props: {
      examination: {
        created_at: 1661262822262,
        apps: [],
        case_id: '',
        creator_id: '',
        creator_type: 'user',
        id: '',
        jobs: [],
        jobs_count: 0,
        jobs_count_finished: 0,
        updated_at: 1661262822262,
        state: WbcV2ExaminationState.Closed
      },
    }
  }));

  it('should create', () => {
    expect(spectator.component).toBeTruthy();
  });
});
