import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { Examination } from '@examinations/store';
import { WbcV2ExaminationState } from 'empaia-api-lib';

@Component({
  selector: 'app-case-examination-item',
  templateUrl: './case-examination-item.component.html',
  styleUrls: ['./case-examination-item.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CaseExaminationItemComponent {
  @Input() public examination!: Examination;
  @Input() public selected!: boolean;

  @Output() public examinationClicked = new EventEmitter<string>();

  public readonly EXAMINATION_STATE = WbcV2ExaminationState;
}
