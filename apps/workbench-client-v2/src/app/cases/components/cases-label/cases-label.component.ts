import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { Case } from '@cases/store';
import { MenuState } from '@menu/models/menu.models';

@Component({
  selector: 'app-cases-label',
  templateUrl: './cases-label.component.html',
  styleUrls: ['./cases-label.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CasesLabelComponent {
  @Input() public case!: Case | undefined;
  @Input() public selected!: boolean;
  @Input() public contentLoaded!: boolean;
  @Input() public labelState!: MenuState;
  public MENU_STATE = MenuState;
}
