import { CasesListMaxComponent } from './cases-list-max.component';
import { createComponentFactory, Spectator } from '@ngneat/spectator';
import { MaterialModule } from '@material/material.module';
import { SharedModule } from '@shared/shared.module';

describe('CasesListMaxComponent', () => {
  let spectator: Spectator<CasesListMaxComponent>;
  const createComponent = createComponentFactory({
    component: CasesListMaxComponent,
    imports: [
      MaterialModule,
      SharedModule,
    ]
  });

  beforeEach(() => spectator = createComponent());

  it('should be created', () => {
    expect(spectator.component).toBeTruthy();
  });
});
