export const OPEN_EXAMINATION_HEADLINE = 'Open new examination';
export const OPEN_EXAMINATION_CONTENT_TEXT = `
A new examination of the selected case will be opened. New examinations allow diagnostic results to be created.
Previously closed examinations are still available for selection and existing results can be viewed but not created
or altered. A list of available examinations is accessible when expanding the case (arrow symbol in case list).
`;
export const OPEN_EXAMINATION_ACCEPT_BUTTON_TEXT = 'open';
export const OPEN_EXAMINATION_CANCEL_BUTTON_TEXT = 'cancel';

export const CLOSE_EXAMINATION_HEADLINE = 'Close examination';
export const CLOSE_EXAMINATION_CONTENT_TEXT = `
The open examination of the selected case will be closed, such that existing diagnostic results can still be viewed
but not created or altered. Closed examinations are available for selection and it is possible to open a new examination
to create further diagnostic results for this case. A list of available examinations is accessible when expanding
the case (arrow symbol in case list).
`;
export const CLOSE_EXAMINATION_ACCEPT_BUTTON = 'close';
export const CLOSE_EXAMINATION_CANCEL_BUTTON = 'cancel';
