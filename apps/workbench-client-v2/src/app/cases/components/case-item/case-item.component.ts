import { OPEN_EXAMINATION_HEADLINE, OPEN_EXAMINATION_CONTENT_TEXT, OPEN_EXAMINATION_ACCEPT_BUTTON_TEXT, OPEN_EXAMINATION_CANCEL_BUTTON_TEXT, CLOSE_EXAMINATION_HEADLINE, CLOSE_EXAMINATION_CONTENT_TEXT, CLOSE_EXAMINATION_ACCEPT_BUTTON, CLOSE_EXAMINATION_CANCEL_BUTTON } from './case-item.models';
import { ConfirmationDialogData } from '@shared/components/confirmation-dialog/confirmation-dialog.models';
import { ConfirmationDialogComponent } from '@shared/components/confirmation-dialog/confirmation-dialog.component';
/* eslint-disable @typescript-eslint/no-non-null-assertion */
import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnChanges,
  Output,
  SimpleChanges,
  ViewChild
} from '@angular/core';
import { IdSelection } from '@menu/models/ui.models';
import { CaseExaminationSelection } from '@examinations/store/examinations/examinations.models';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { take } from 'rxjs/operators';
import { isEllipsisActive } from '@helper/html-ellipsis-calculation';
import { Case } from '@cases/store';
import { WbcV2ExaminationState } from 'empaia-api-lib';

@Component({
  selector: 'app-case-item',
  templateUrl: './case-item.component.html',
  styleUrls: ['./case-item.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CaseItemComponent implements OnChanges, AfterViewInit {
  @ViewChild('caseId') private caseIdSpan!: ElementRef;
  @ViewChild('caseTissue') private caseTissueSpan!: ElementRef;
  @ViewChild('caseDescription') private caseDescription!: ElementRef;

  public readonly EXAMINATION_STATE = WbcV2ExaminationState;
  public disabled = false;
  public firstExaminationOpen = false;

  @Input() public caseItem!: Case;
  @Input() public selected!: boolean;
  @Input() public caseExtended!: boolean;
  @Input() public selectedExaminationId: string | undefined;

  @Output() public selectCaseExamination = new EventEmitter<CaseExaminationSelection>();
  @Output() public createExamination = new EventEmitter<IdSelection>();
  @Output() public closeExamination = new EventEmitter<CaseExaminationSelection>();
  @Output() public expandCase = new EventEmitter<string>();
  @Output() public collapseCase = new EventEmitter<string>();

  constructor(
    private dialog: MatDialog,
    private changeRef: ChangeDetectorRef,
  ) { }

  public ngOnChanges(changes: SimpleChanges): void {
    if (changes.caseItem || changes.caseExaminations) {
      this.disabled = this.isDisabled();
      this.firstExaminationOpen = this.isFirstExaminationOpen();
    }
  }

  public ngAfterViewInit(): void {
    this.disabled = this.isDisabled();
    // Trigger change detection to inform the view and it's
    // children to update (is especially needed for the expansion button)
    this.changeRef.detectChanges();
  }

  private isDisabled(): boolean {
    return (this.caseIdSpan && this.caseTissueSpan && this.caseDescription)
      ? !(
        isEllipsisActive(this.caseIdSpan.nativeElement)
        || isEllipsisActive(this.caseTissueSpan.nativeElement)
        || isEllipsisActive(this.caseDescription.nativeElement)
        || !!this.caseItem?.examinations?.length
      )
      : !this.caseItem?.examinations?.length;
  }

  public onExpandCaseClick(expanded: boolean, id: string): void {
    if (expanded) {
      this.expandCase.emit(id);
    } else {
      this.collapseCase.emit(id);
    }
  }

  public onCaseClicked(): void {
    if (this.caseItem?.examinations?.length) {
      const caseExaminationSelection = this.createCaseExaminationSelection(this.caseItem.examinations[0].id);
      this.selectCaseExamination.emit(caseExaminationSelection);
    } else {
      this.createExamination.emit({ id: this.caseItem.id });
    }
  }

  public onCaseExaminationClicked(examinationId: string): void {
    const caseExaminationSelection = this.createCaseExaminationSelection(examinationId);
    this.selectCaseExamination.emit(caseExaminationSelection);
  }

  public onLockClicked(event: MouseEvent): void {
    event.stopImmediatePropagation();

    let dialogRef: MatDialogRef<ConfirmationDialogComponent>;
    let data: ConfirmationDialogData;
    const width = '500px';

    if (this.isFirstExaminationOpen()) {
      data = {
        headlineText: CLOSE_EXAMINATION_HEADLINE,
        contentText: CLOSE_EXAMINATION_CONTENT_TEXT,
        acceptButtonText: CLOSE_EXAMINATION_ACCEPT_BUTTON,
        cancelButtonText: CLOSE_EXAMINATION_CANCEL_BUTTON
      };
      dialogRef = this.dialog.open(ConfirmationDialogComponent, {
        width,
        data
      });
      dialogRef
        .afterClosed()
        .pipe(take(1))
        .subscribe(result => {
          if (result) {
            const caseExaminationSelection = this.createCaseExaminationSelection(this.caseItem.examinations![0].id);
            this.closeExamination.emit(caseExaminationSelection);
          }
        });
    } else {
      data = {
        headlineText: OPEN_EXAMINATION_HEADLINE,
        contentText: OPEN_EXAMINATION_CONTENT_TEXT,
        acceptButtonText: OPEN_EXAMINATION_ACCEPT_BUTTON_TEXT,
        cancelButtonText: OPEN_EXAMINATION_CANCEL_BUTTON_TEXT
      };
      dialogRef = this.dialog.open(ConfirmationDialogComponent, {
        width,
        data
      });
      dialogRef
        .afterClosed()
        .pipe(take(1))
        .subscribe(result => result ? this.createExamination.emit({ id: this.caseItem.id }) : undefined);
    }
  }

  private createCaseExaminationSelection(examinationId: string): CaseExaminationSelection {
    return {
      caseId: this.caseItem.id,
      examinationId
    };
  }

  private isFirstExaminationOpen(): boolean {
    return !!this.caseItem?.examinations?.length
      && this.caseItem?.examinations[0].state === WbcV2ExaminationState.Open;
  }
}
