import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { Examination } from '@examinations/store';

@Component({
  selector: 'app-case-examination-list',
  templateUrl: './case-examination-list.component.html',
  styleUrls: ['./case-examination-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CaseExaminationListComponent {
  @Input() public examinations!: Examination[];
  @Input() public selectedExaminationId: string | undefined;

  @Output() public examinationClicked = new EventEmitter<string>();
}
