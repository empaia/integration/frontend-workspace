import { CaseExaminationListComponent } from './case-examination-list.component';
import { createComponentFactory, Spectator } from '@ngneat/spectator';
import { MaterialModule } from '@material/material.module';
import { MockComponents } from 'ng-mocks';
import { CaseExaminationItemComponent } from '@cases/components/case-examination-item/case-examination-item.component';

describe('CaseExaminationListComponent', () => {
  let spectator: Spectator<CaseExaminationListComponent>;
  const createComponent = createComponentFactory({
    component: CaseExaminationListComponent,
    imports: [
      MaterialModule
    ],
    declarations: [
      MockComponents(
        CaseExaminationItemComponent
      )
    ]
  });

  beforeEach(() => spectator = createComponent());

  it('should create', () => {
    expect(spectator.component).toBeTruthy();
  });
});
