import { Action, combineReducers, createFeatureSelector } from '@ngrx/store';
import * as fromCases from '@cases/store/cases/cases.reducer';

export const CASES_MODULE_FEATURE_KEY = 'casesModuleFeature';

export const selectCasesFeatureState = createFeatureSelector<State>(
  CASES_MODULE_FEATURE_KEY
);

export interface State {
  [fromCases.CASES_FEATURE_KEY]: fromCases.State;
}

export function reducers(state: State | undefined, action: Action) {
  return combineReducers({
    [fromCases.CASES_FEATURE_KEY]: fromCases.reducer,
  })(state, action);
}
