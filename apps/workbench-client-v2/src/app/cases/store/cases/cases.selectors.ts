import { createSelector } from '@ngrx/store';
import { RouterSelectors } from '@router/store';
import {
  State as ModuleState,
  selectCasesFeatureState,
} from '../cases-feature.state';
import {
  State,
  casesAdapter,
  casesExpansionAdapter,
} from './cases.reducer';

// Lookup the 'Cases' feature state managed by NgRx
export const selectCasesState = createSelector(
  selectCasesFeatureState,
  (state: ModuleState) => state.cases
);

const { selectAll, selectEntities } = casesAdapter.getSelectors();

export const selectCasesLoaded = createSelector(
  selectCasesState,
  (state: State) => state.loaded
);

export const selectCasesError = createSelector(
  selectCasesState,
  (state: State) => state.error
);

export const selectAllCases = createSelector(
  selectCasesState,
  selectAll,
);

export const selectCasesEntities = createSelector(
  selectCasesState,
  selectEntities,
);

export const selectSelectedCaseId = createSelector(
  RouterSelectors.selectCaseIdParam,
  (id) => id
);

export const selectCaseFromRouter = createSelector(
  selectCasesEntities,
  selectSelectedCaseId,
  (entities, selectedId) => selectedId ? entities[selectedId] : undefined
);

export const selectSelectedCase = createSelector(
  selectCaseFromRouter,
  (caseEntity) => caseEntity
);


export const selectExpandedCaseEntities = createSelector(
  selectCasesState,
  (state: State) => casesExpansionAdapter.getSelectors().selectEntities(state.expansion)
);

export const selectCasesCount = createSelector(
  selectCasesState,
  (state) => state.casesCount
);

export const selectCaseListSkip = createSelector(
  selectCasesState,
  (state) => state.skip
);
