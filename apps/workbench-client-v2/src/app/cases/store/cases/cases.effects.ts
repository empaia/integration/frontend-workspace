import { Inject, Injectable } from '@angular/core';
import { createEffect, Actions, ofType, concatLatestFrom } from '@ngrx/effects';
import { fetch } from '@ngrx/router-store/data-persistence';
import { WbcV2CasesPanelService } from 'empaia-api-lib';
import {
  distinctUntilChanged,
  filter,
  map,
  switchMap,
  takeUntil,
} from 'rxjs/operators';

import * as CasesActions from './cases.actions';
import * as CasesSelectors from './cases.selectors';
import * as MenuActions from '@menu/store/menu/menu.actions';
import { CASE_LIST_LIMIT, CASE_POLLING_PERIOD } from '@cases/store';
import { CasesFeature } from '../index';
import { Router } from '@angular/router';
import { ROUTE_CASES } from '@router/routes';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ErrorSnackbarComponent } from '@shared/components/error-snackbar/error-snackbar.component';
import { of, timer } from 'rxjs';
import { CASES_LOAD_ERROR, HttpResponseError } from '@menu/models/ui.models';
import { routerNavigatedAction } from '@ngrx/router-store';
import { Store } from '@ngrx/store';
import { filterNullish } from '@helper/rxjs-operators';
import { AuthService } from '@core/services/auth.service';

@Injectable()
export class CasesEffects {
  loadCases$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(CasesActions.loadCasePage),
      fetch({
        id: (
          action: ReturnType<typeof CasesActions.loadCasePage>,
          _state: CasesFeature.State
        ) => {
          return action.type;
        },
        run: (
          action: ReturnType<typeof CasesActions.loadCasePage>,
          _state: CasesFeature.State
        ) => {
          return this.casesPanelService
            .casesGet({
              skip: action.skip,
              limit: CASE_LIST_LIMIT,
            })
            .pipe(
              map((response) => {
                return {
                  cases: response.items.filter(
                    (c) => c.deleted === null || !c.deleted
                  ),
                  casesCount: response.item_count,
                };
              }),
              map((data) => CasesActions.loadCasePageSuccess({ ...data }))
            );
        },
        onError: (
          _action: ReturnType<typeof CasesActions.loadCasePage>,
          error
        ) => {
          return CasesActions.loadCasePageFailure({ error });
        },
      })
    );
  });

  caseLoadError$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(CasesActions.loadCasePageFailure),
      map((action) => action.error),
      switchMap((error: HttpResponseError) => {
        // ... you can check the payload here to show different messages
        // like if error.statusCode === 501 etc.
        this.snackBar.openFromComponent(ErrorSnackbarComponent, {
          data: { titel: CASES_LOAD_ERROR, error },
        });
        // remap to noop Action if no state needs to be updated.
        // or for example on 401 Errors dispatch a re-login action etc.
        return of({ type: 'noop' });
      })
    );
  });

  // load cases in case the user refreshes the menu
  // or the user routes via an uri with parameters
  routingLoadCases$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(routerNavigatedAction),
      concatLatestFrom(() => [
        this.store.select(CasesSelectors.selectSelectedCaseId),
        this.store.select(CasesSelectors.selectCaseListSkip),
      ]),
      distinctUntilChanged((prev, curr) => prev[1] === curr[1]),
      filterNullish(),
      map(([_, _id, skip]) => CasesActions.loadCasePage({ skip }))
    );
  });

  caseSelectionRouting$ = createEffect(
    () => {
      return this.actions$.pipe(
        ofType(CasesActions.userSelectedCase),
        map((action) => action.id),
        filter((id) => !!id),
        map((id) => this.router.navigate([ROUTE_CASES, id]))
      );
    },
    { dispatch: false }
  );

  casesPolling$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(CasesActions.startPollingCases),
      switchMap(() =>
        timer(0, CASE_POLLING_PERIOD).pipe(
          takeUntil(this.actions$.pipe(ofType(CasesActions.stopPollingCases))),
          concatLatestFrom(() =>
            this.store.select(CasesSelectors.selectCaseListSkip)
          ),
          filter(() => (this.authOn ? this.authService.isLoggedIn() : true)),
          map(([_, skip]) => skip),
          map((skip) => CasesActions.loadCasePage({ skip }))
        )
      )
    );
  });

  casePollingMenuManagement$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(
        MenuActions.openMenuMidi,
        MenuActions.openMenuMax,
        MenuActions.clickMenuButton
      ),
      map((action) => action.menuItem),
      distinctUntilChanged(),
      map((id) =>
        id === 'Cases'
          ? CasesActions.startPollingCases()
          : CasesActions.stopPollingCases()
      )
    );
  });

  // clear expanded cases when user left the cases panel
  clearExpandedCases$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(
        MenuActions.openMenuMidi,
        MenuActions.openMenuMax,
        MenuActions.clickMenuButton
      ),
      map((action) => action.menuItem),
      filter((id) => id !== 'Cases'),
      map(() => CasesActions.collapseAllCases())
    );
  });

  constructor(
    private readonly actions$: Actions,
    private readonly casesPanelService: WbcV2CasesPanelService,
    private readonly router: Router,
    private readonly snackBar: MatSnackBar,
    private readonly store: Store,
    private readonly authService: AuthService,
    @Inject('AUTHENTICATION_ON') private readonly authOn: boolean
  ) {}
}
