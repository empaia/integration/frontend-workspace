import { TestBed } from '@angular/core/testing';

import { Observable } from 'rxjs';

import { provideMockActions } from '@ngrx/effects/testing';
import { provideMockStore } from '@ngrx/store/testing';

import { CasesEffects } from './cases.effects';
import * as CasesActions from './cases.actions';
import { HttpClient, HttpHandler } from '@angular/common/http';
import { hot } from 'jasmine-marbles';

describe('CasesEffects', () => {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  let actions: Observable<any>;
  let effects: CasesEffects;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        CasesEffects,
        HttpClient,
        HttpHandler,
        provideMockActions(() => actions),
        provideMockStore(),
      ],
    });

    effects = TestBed.inject(CasesEffects);
  });

  describe('init$', () => {
    it('should work', () => {
      actions = hot('-a-|', { a: CasesActions.loadCasePage({ skip: 0 }) });

      const expected = hot('-a-|', {
        a: CasesActions.loadCasePageSuccess({ cases: [], casesCount: 0 }),
      });

      expect(effects.loadCases$).toBeObservable(expected);
    });
  });
});
