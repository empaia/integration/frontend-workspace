import { createReducer, on, Action } from '@ngrx/store';
import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';

import * as CasesActions from './cases.actions';
import { Case, CaseExpansion } from '@cases/store';
import { HttpResponseError } from '@menu/models/ui.models';

export const CASES_FEATURE_KEY = 'cases';

export interface State extends EntityState<Case> {
  expansion: EntityState<CaseExpansion>;
  casesCount: number;
  skip: number;
  loaded: boolean; // has the Cases list been loaded
  error?: HttpResponseError; // last known error (if any)
}

export const casesAdapter: EntityAdapter<Case> = createEntityAdapter<Case>();
export const casesExpansionAdapter: EntityAdapter<CaseExpansion> = createEntityAdapter<CaseExpansion>();

export const initialState: State = casesAdapter.getInitialState({
  // set initial required properties
  expansion: casesExpansionAdapter.getInitialState(),
  casesCount: 0,
  skip: 0,
  loaded: true,
});

const casesReducer = createReducer(
  initialState,
  on(CasesActions.loadCasePage, (state, { skip }): State => ({
    ...state,
    loaded: false,
    skip,
  })),
  on(CasesActions.loadCasePageSuccess, (state, { cases, casesCount }): State =>
    casesAdapter.setAll(cases, {
      ...state,
      casesCount,
      loaded: true,
    })
  ),
  on(CasesActions.loadCasePageFailure, (state, { error }): State => ({
    ...state,
    loaded: true,
    error,
  })),
  on(CasesActions.expandCase, (state, { id }): State => ({
    ...state,
    expansion: casesExpansionAdapter.upsertOne({ id }, {
      ...state.expansion,
    })
  })),
  on(CasesActions.collapseCase, (state, { id }): State => ({
    ...state,
    expansion: casesExpansionAdapter.removeOne(id, {
      ...state.expansion,
    })
  })),
  on(CasesActions.collapseAllCases, (state): State => ({
    ...state,
    expansion: casesExpansionAdapter.removeAll({
      ...state.expansion,
    })
  }))
);

export function reducer(state: State | undefined, action: Action) {
  return casesReducer(state, action);
}
