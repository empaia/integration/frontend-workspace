import * as CasesActions from './cases.actions';
import { Case } from './cases.models';
import { State, initialState, reducer } from './cases.reducer';
import { Action } from '@ngrx/store';

describe('Cases Reducer', () => {
  const createCasesEntity = (id: string, name = '') =>
    ({
      id,
      name: name || `name-${id}`,
      blocks: [''],
      created_at: 0,
      creator_id: '',
      creator_type: 'user',
      slides_count: 0,
      updated_at: 0,
      stains:  {},
      tissues: {},
      examinations: [],
      deleted: null,
      description: null,
      local_id: null,
      mds_url: null,
    } as Case);

  //beforeEach(() => {});

  describe('valid Cases actions', () => {
    it('loadCasesSuccess should return set the list of known Cases', () => {
      const cases = [
        createCasesEntity('PRODUCT-AAA'),
        createCasesEntity('PRODUCT-zzz'),
      ];
      const action = CasesActions.loadCasePageSuccess({ cases, casesCount: cases.length });

      const result: State = reducer(initialState, action);

      expect(result.loaded).toBe(true);
      expect(result.ids.length).toBe(2);
    });
  });

  describe('unknown action', () => {
    it('should return the previous state', () => {
      const action = {} as Action;

      const result = reducer(initialState, action);

      expect(result).toBe(initialState);
    });
  });
});
