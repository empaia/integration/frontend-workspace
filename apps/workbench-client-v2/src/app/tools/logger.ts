/* eslint-disable no-console */
/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable @typescript-eslint/member-ordering */
/* eslint-disable @typescript-eslint/ban-types */
/* eslint-disable @typescript-eslint/no-inferrable-types */
import { environment } from '@env/environment';

export enum LOG_LEVEL {
  NONE = 0,
  ASSERT = 1,
  ERROR = 2,
  WARN = 3,
  INFO = 4,
  DEBUG = 5,
  VERBOSE = 6
}

abstract class Logger {
  log: any;
  info: any;
  warn: any;
  error: any;
  table: any;
}

class LoggerService implements Logger {
  log: any;
  info: any;
  warn: any;
  error: any;
  table: any;
  /* eslint-disable @typescript-eslint/no-empty-function */
  invokeConsoleMethod(type: string, args?: any): void { }
}

const noop = (): any => undefined;

class ConsoleLoggerService implements Logger {
  constructor(logLevel: LOG_LEVEL) {
    this.setLevel(logLevel);
    if (logLevel <= LOG_LEVEL.NONE) {
      this.disableLogging();
    }
  }

  private loggingLevel: number = 0;

  invokeConsoleMethod(type: string, args?: any): void {
    const conKey: keyof Console = type as keyof Console;
    const logFn: Function = console[conKey] || console.log || noop;
    const enhancedArgs = args.map((arg: any) => JSON.stringify(arg, null, 2));
    logFn.apply(console, [enhancedArgs]);
  }

  private disableLogging(): void {
    window['console']['log'] = function() {};
    window['console']['warn'] = function() {};
    window['console']['error'] = function() {};
    window['console']['info'] = function() {};
    window['console']['table'] = function() {};
    window['console']['assert'] = function() {};
  }

  public setLevel(level: number) {
    this.loggingLevel = level;
  }

  get log(): any {
    return this.loggingLevel >= LOG_LEVEL.VERBOSE
      ? console.log.bind(console)
      : noop;
  }

  get info(): any {
    return this.loggingLevel >= LOG_LEVEL.INFO
      ? console.info.bind(console)
      : noop;
  }

  get warn(): any {
    return this.loggingLevel >= LOG_LEVEL.WARN
      ? console.warn.bind(console)
      : noop;
  }

  get error(): any {
    return this.loggingLevel >= LOG_LEVEL.ERROR
      ? console.error.bind(console)
      : noop;
  }

  get table(): any {
    return this.loggingLevel >= LOG_LEVEL.VERBOSE
      ? console.table.bind(console)
      : noop;
  }

  get assert(): any {
    return this.loggingLevel >= LOG_LEVEL.ASSERT
      ? console.assert.bind(console)
      : noop;
  }
}

export const logger = new ConsoleLoggerService(environment.logging);
