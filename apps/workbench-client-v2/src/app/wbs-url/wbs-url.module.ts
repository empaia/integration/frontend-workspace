import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';
import { WBS_URL_MODULE_FEATURE_KEY, reducers } from '@wbsUrl/store/wbs-url-feature.state';
import { EffectsModule } from '@ngrx/effects';
import { WbsUrlEffects } from '@wbsUrl/store';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    StoreModule.forFeature(
      WBS_URL_MODULE_FEATURE_KEY,
      reducers
    ),
    EffectsModule.forFeature([
      WbsUrlEffects
    ])
  ]
})
export class WbsUrlModule { }
