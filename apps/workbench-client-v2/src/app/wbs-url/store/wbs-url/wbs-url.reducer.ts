import { createReducer, on } from '@ngrx/store';
import { WbsUrl } from 'vendor-app-integration';
import * as WbsUrlActions from './wbs-url.actions';

export const WBS_URL_FEATURE_KEY = 'wbsUrl';

export interface State {
  wbsUrl?: WbsUrl;
}

export const initialState: State = {
  wbsUrl: undefined,
};

export const reducer = createReducer(
  initialState,
  on(
    WbsUrlActions.loadWbsUrlSuccess,
    (state, { wbsUrl }): State => ({
      ...state,
      wbsUrl,
    })
  )
);
