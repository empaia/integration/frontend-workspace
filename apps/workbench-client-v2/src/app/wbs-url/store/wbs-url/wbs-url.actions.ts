import { createAction, props } from '@ngrx/store';
import { WbsUrl } from 'vendor-app-integration';

export const loadWbsUrl = createAction('[WbsUrl] Load Wbs Url');

export const loadWbsUrlSuccess = createAction(
  '[WbsUrl] Load Wbs Url Success',
  props<{ wbsUrl: WbsUrl }>()
);
