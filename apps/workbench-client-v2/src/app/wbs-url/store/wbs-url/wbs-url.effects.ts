import { Inject, Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import * as WbsUrlActions from './wbs-url.actions';
import { map } from 'rxjs/operators';
import { WBS_URL_TYPE, WbsUrl } from 'vendor-app-integration';

@Injectable()
export class WbsUrlEffects {
  loadWbsUrl$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(WbsUrlActions.loadWbsUrl),
      map(() => {
        const wbsUrl: WbsUrl = {
          url: this.wbsServerApiUrl,
          type: WBS_URL_TYPE,
        };
        return wbsUrl;
      }),
      map((wbsUrl) => WbsUrlActions.loadWbsUrlSuccess({ wbsUrl }))
    );
  });

  constructor(
    private actions$: Actions,
    @Inject('WBS_SERVER_API_URL') private wbsServerApiUrl: string
  ) {}
}
