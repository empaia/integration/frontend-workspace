import { AuthConfig } from 'angular-oauth2-oidc';
import { environment } from '@env/environment';

export const authCodeFlowConfig: AuthConfig = {
  issuer: environment.idpUrl,
  requireHttps: environment.idpHttpsNotRequired !== 'true',
  redirectUri: window.location.href.split('#')[0].split('?')[0],
  clientId: environment.clientId,
  responseType: 'code',
  scope: 'openid offline_access',
  showDebugInformation: !(environment.production),
  clearHashAfterLogin: true,
  disableAtHashCheck: false,
  timeoutFactor: 0.75,
};
