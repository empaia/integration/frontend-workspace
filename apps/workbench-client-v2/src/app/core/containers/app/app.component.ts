import { UserData } from '@core/store/authentication/authentication.model';
import { ChangeDetectionStrategy, Component, Inject } from '@angular/core';
import { Store } from '@ngrx/store';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from '@core/services/auth.service';
import { AuthenticationActions, AuthenticationSelectors } from '@core/store';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AppComponent {
  title = 'workbench-client-v2';

  public isExpanded = false;
  public userData$: Observable<UserData | undefined> | undefined;
  public userProfilePicture$: Observable<string | undefined> | undefined;
  public loggedIn$ = this.store.select(AuthenticationSelectors.selectIsAuthenticated);
  public isRedirecting$ = this.store.select(AuthenticationSelectors.selectIsRedirecting);
  public error$ = this.store.select(AuthenticationSelectors.selectError);

  constructor(
    private readonly store: Store,
    private readonly router: Router,
    private readonly authService: AuthService,
    @Inject('AUTHENTICATION_ON') private readonly authOn: boolean,
    @Inject('WBS_USER_ID') private readonly wbsUserId: string,
  ) {
    if(!this.authOn) {
      this.store.dispatch(AuthenticationActions.setUserId({ userId: wbsUserId }));
      // we need to navigate manually since initial navigation is switched off
      const urlFragment = location.href.split('#');
      this.router.navigateByUrl(urlFragment.length > 1 ? urlFragment[1] : '/');
    } else {
      this.authService.tryLogin()
        .then(() => {
          this.authService.redirect();
        })
        .then(() => {
          this.userData$ = this.store.select(AuthenticationSelectors.selectUserData);
          this.userProfilePicture$ = this.store.select(AuthenticationSelectors.selectUserProfilePicture);
          this.store.dispatch(AuthenticationActions.requestUserProfilePicture());
        });
    }
  }

  public logout(): void {
    this.store.dispatch(AuthenticationActions.logoutUser());
  }

  public login(): void {
    this.store.dispatch(AuthenticationActions.loginUser());
  }
}

