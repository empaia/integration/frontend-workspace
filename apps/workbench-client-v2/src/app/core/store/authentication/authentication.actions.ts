import { createAction, props } from '@ngrx/store';
import { UserData, HttpError } from '@core/store/authentication/authentication.model';

export const setIsAuthenticated = createAction(
  '[Authentication] Set Is Authenticated',
);

export const setIsNotAuthenticated = createAction(
  '[Authentication] Set Is Not Authenticated',
  props<{ error?: HttpError }>()
);

export const setUserId = createAction(
  '[Authentication] Set User Id',
  props<{ userId: string | undefined }>()
);

export const setUserData = createAction(
  '[Authentication] Set User Data',
  props<{ userData: UserData | undefined }>()
);

export const loginUser = createAction(
  '[Authentication] Login User',
);

export const logoutUser = createAction(
  '[Authentication] Logout User',
);

export const setIsRedirecting = createAction(
  '[Authentication] Set Is Redirecting',
  props<{ isRedirecting: boolean }>()
);

export const requestUserProfilePicture = createAction(
  '[Authentication] Request User Profile Picture',
);

export const requestUserProfilePictureSuccess = createAction(
  '[Authentication] Request User Profile Picture Success',
  props<{ userProfilePicture: string | undefined }>()
);

export const requestUserProfilePictureFailure = createAction(
  '[Authentication] Request User Profile Picture Failure',
  props<{ error: HttpError }>()
);

export const setHttpError = createAction(
  '[Authentication] Set Http Error',
  props<{ error: HttpError }>()
);
