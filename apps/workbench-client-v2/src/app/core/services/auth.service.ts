/* eslint-disable @typescript-eslint/no-explicit-any,no-useless-escape */
import { Inject, Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { Store } from '@ngrx/store';
import { OAuthService } from 'angular-oauth2-oidc';
import { Router } from '@angular/router';
import { authCodeFlowConfig } from '@core/auth.config';
import { takeUntil } from 'rxjs/operators';
import { AuthenticationActions } from '@core/store';
import { logger } from '@tools/logger';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private destroy$ = new Subject();

  constructor(
    private readonly store: Store,
    private readonly oAuthService: OAuthService,
    private readonly router: Router,
    @Inject('AUTHENTICATION_ON') private readonly authOn: boolean,
  ) {
    if (!this.authOn) { return; }
    this.oAuthService.configure(authCodeFlowConfig);
    this.oAuthService.skipSubjectCheck = true;
    this.oAuthService.oidc = true;
    this.oAuthService.setupAutomaticSilentRefresh();
    this.oAuthService.loadDiscoveryDocument();

    this.router.events
      .pipe(takeUntil(this.destroy$))
      .subscribe(() => {
        if(location.href.includes('?state=')) {
          const baseUrl = location.href.split('?state=')[0];
          const newUri = location.href.split('#')[1];
          location.href = baseUrl + '#' + newUri;
        }
      });
  }

  public initialLogin(): void {
    this.setRedirectUri();
    sessionStorage.setItem('isRedirecting', 'true');
    this.oAuthService.initLoginFlow();
  }

  public async tryLogin(): Promise<void> {
    this.setRedirectUri();

    if (sessionStorage.getItem('isRedirecting') !== null || this.isLoggedIn()) {
      this.store.dispatch(AuthenticationActions.setIsRedirecting({ isRedirecting: true }));
      sessionStorage.removeItem('isRedirecting');
    }

    if(this.isLoggedIn()) {
      await this.oAuthService.loadDiscoveryDocument()
        .then(async () => await this.loadUserData())
        .then(() => this.store.dispatch(AuthenticationActions.setIsAuthenticated()))
        .catch(error => {
          this.store.dispatch(AuthenticationActions.setIsNotAuthenticated({ error }));
          logger.error('[AuthService] an error occurred: ', error);
        });
    } else {
      await this.oAuthService.loadDiscoveryDocument()
        .then(() => this.oAuthService.tryLogin())
        .then(async () => await this.loadUserData())
        .then(() => this.store.dispatch(AuthenticationActions.setIsAuthenticated()))
        .catch((error) => {
          this.store.dispatch(AuthenticationActions.setIsNotAuthenticated({ error }));
          logger.error('[AuthService] an error occurred: ', error);
        });
    }
  }

  public setRedirectUri(): void {
    if(!location.href.includes('?state=')) {
      sessionStorage.setItem('redirectUri', location.href.split('#')[1]);
    }
  }

  public isLoggedIn(): boolean {
    return this.oAuthService.hasValidAccessToken();
  }

  public redirect(): void {
    const uri = sessionStorage.getItem('redirectUri');
    this.router.navigateByUrl(uri !== null ? uri : '/');
    this.store.dispatch(AuthenticationActions.setIsRedirecting({ isRedirecting: false }));
  }

  public getUriFromSessionStorage(): string {
    const redirectUri = sessionStorage.getItem('redirectUri');
    return (redirectUri === null || redirectUri === 'undefined') ? '/' : this.clearUri(redirectUri);
  }

  public clearUri(uri: string): string {
    return uri
      .replace(/[&\?]code=[^&\$]*/, '')
      .replace(/[&\?]scope=[^&\$]*/, '')
      .replace(/[&\?]state=[^&\$]*/, '')
      .replace(/[&\?]session_state=[^&\$]*/, '');
  }

  public logout(): void {
    sessionStorage.removeItem('isRedirecting');
    sessionStorage.removeItem('redirectUri');
    this.oAuthService.logOut();
    this.router.navigate(['/']);
    this.destroy$.next(null);
  }

  public isTokenValid(): boolean {
    const expTime = this.oAuthService.getAccessTokenExpiration();
    const now = Date.now();
    return expTime > now;
  }

  public async loadUserData(): Promise<void> {
    try {
      await this.oAuthService.loadUserProfile()
        .then(() => this.store.dispatch(AuthenticationActions.setIsRedirecting({ isRedirecting: true })))
        .then(() => {
          const userData = {
            userId: this.getUserId(),
            userName: this.getFullName(),
          };
          this.store.dispatch(AuthenticationActions.setUserData({ userData }));
        })
        .catch((error) => {
          logger.error('[AuthService] Failed to retrieve userinfo. ', error);
        });
    } catch (e: unknown) {
      this.store.dispatch(AuthenticationActions.setIsNotAuthenticated({ error: undefined }));
      logger.error('[AuthService] Error: ', (e as Error).message);
    }
  }

  public getUserId(): string | undefined {
    const claims: any = this.oAuthService.getIdentityClaims();
    return (claims && claims.sub) ? claims.sub : undefined;
  }

  public getFullName(): string | undefined {
    const claims: any = this.oAuthService.getIdentityClaims();
    return (claims && claims.name) ? claims.name : undefined;
  }
}
