import { UserData } from '@core/store/authentication/authentication.model';
import { ChangeDetectionStrategy, Component, EventEmitter, Inject, Input, Output } from '@angular/core';
import { BLANK_PROFILE_IMAGE } from 'src/app/app.module';

@Component({
  selector: 'app-top-bar',
  templateUrl: './top-bar.component.html',
  styleUrls: ['./top-bar.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TopBarComponent {
  @Input() public userProfilePictureUrl: string | undefined;
  @Input() public userData: UserData | undefined;

  @Output() public toggleHideAllMenus = new EventEmitter<void>();
  @Output() public logout = new EventEmitter<void>();
  @Output() public login = new EventEmitter<void>();

  public withAuthentication = false;
  public debugMode = false;

  public readonly FALLBACK_PROFILE = BLANK_PROFILE_IMAGE;

  constructor(
    @Inject('AUTHENTICATION_ON') private readonly authOn: boolean
  ) {
    this.withAuthentication = this.authOn;
    this.debugMode = !(this.authOn);
  }

  public onLogout(): void {
    this.logout.emit();
  }

  public onLogin(): void {
    this.login.emit();
  }
}
