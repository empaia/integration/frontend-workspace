import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { HttpError } from '@core/store/authentication/authentication.model';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LoginComponent {
  @Input() public inProgress!: boolean;
  @Input() public error!: HttpError;
  @Output() public login = new EventEmitter<void>();

  public onLogin(): void {
    this.login.emit();
  }
}
