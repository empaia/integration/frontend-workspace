(function(window) {
    window['env'] = window['env'] || {};

    // Environment variables
    window['env']['production'] = true;
    window['env']['authenticationOn'] = '${WBC_AUTHENTICATION_ON}';
    window['env']['wbsServerApiUrl'] = '${WBC_WBS_SERVER_API_URL}';
    window['env']['wbsUserId'] = '${WBC_WBS_USER_ID}';
    window['env']['idpUrl'] = '${WBC_IDP_URL}';
    window['env']['idpUserUrl'] ='${WBC_IDP_USER_URL}';
    window['env']['solutionStoreUrl'] = '${WBC_MARKETPLACE_URL}';
    window['env']['clientId'] = '${WBC_CLIENT_ID}';
    window['env']['logging'] = '${WBC_LOGGING}';
    window['env']['idpHttpsNotRequired'] = '${WBC_IDP_HTTPS_NOT_REQUIRED}';
    window['env']['organizationName'] = '${WBC_ORGANIZATION_NAME}';
    window['env']['aaaApiUrl'] = '${WBC_AAA_API_URL}';
})(this);
