import { expect, FrameLocator, Page, test, Response } from '@playwright/test';

test.describe.serial('Sample App Ui', () => {

  /* test.beforeAll(async ({ browser }) => {
    const context = await browser.newContext();
    const page = await context.newPage();
    await login(page);
    await page.locator('app-case-item', { hasText: 'Test Case 01' }).click();
    await page.close();
    await context.close();
  }); */

  test.describe('Slides Panel', () => {

    test.beforeEach(async ({ page }) => {
      await login(page);
      await openSampleApp(page);
      await page.locator('[data-cy=apps-menu-button]').click();
    });

    test('should have at least one slide in the list', async ({ page }) => {
      expect.soft(await page.frameLocator('iframe').locator('app-slide-item').count()).toBeGreaterThanOrEqual(1);
    });

    test('should load first slide', async ({ page }) => {
      const iframe = page.frameLocator('iframe');
      await iframe.locator('app-slide-item').first().click();
      const tileResponse = await page.waitForResponse(/.*scopes\/[0-9a-fA-F]{8}\b-[0-9a-fA-F]{4}\b-[0-9a-fA-F]{4}\b-[0-9a-fA-F]{4}\b-[0-9a-fA-F]{12}\/slides\/[0-9a-fA-F]{8}\b-[0-9a-fA-F]{4}\b-[0-9a-fA-F]{4}\b-[0-9a-fA-F]{4}\b-[0-9a-fA-F]{12}\/tile\/level/);
      expect.soft(tileResponse.ok()).toBeTruthy();
      expect.soft(tileResponse.request().method()).toMatch('GET');
      await expect.soft(iframe.locator('svm-viewer')).toBeVisible();
      await expect.soft(iframe.locator('[data-cy=slides-label-id]')).toContainText(/Aperio_CMU-1.svs_/);
    });
  });

  test.describe.serial('Results Panel', () => {
    test.beforeEach(async ({ page }) => {
      await login(page);
      await openSampleApp(page);
      await page.locator('[data-cy=apps-menu-button]').click();
      await getIframe(page).locator('app-slide-item').first().click();
    });

    test('should draw roi and create job', async ({ page }) => {
      const iframe = getIframe(page);
      await iframe.locator('[data-cy=annotation-rectangle-button]').click();
      const canvas = iframe.locator('canvas >> nth=0');
      await canvas.hover();
      await page.mouse.move(300, 300);
      await page.mouse.down();
      await page.mouse.move(350, 300);
      await page.mouse.move(350, 350);
      await page.mouse.up();
      await canvas.click({ force: true });
      const annotationTitleInput = iframe.locator('[data-cy=annotation-creation-title]');
      await annotationTitleInput.click();
      await annotationTitleInput.fill('PLAYWRIGHT TEST');
      await iframe.locator('[data-cy=annotation-save-button]').click();
      await waitForResponse(page, '/annotations?is_roi=true', 201, 'POST');
      await page.waitForResponse(async resp => resp.url().includes('/jobs') && resp.status() === 200 && resp.request().method() === 'GET' && (await resp.json())['items'][0]['status'] === 'COMPLETED');
    });

    test('should have a positive result', async ({ page }) => {
      if (!process.env.RUNNER_DOCKER_TAG) {
        await waitForResponse(page, '/jobs', 200, 'GET');
        await waitForResponse(page, '/primitives/query', 200, 'PUT');
      }
      const iframe = getIframe(page);
      const result = iframe.locator('[data-cy=results-list]').locator('app-result-item').first();
      await expect.soft(result.locator('[data-cy=result-name]')).toContainText('PLAYWRIGHT TEST');
      await expect.soft(result.locator('app-job-status').locator('mat-icon')).toHaveText('done');
    });

    test('should have result annotations', async ({ page }) => {
      if (!process.env.RUNNER_DOCKER_TAG) {
        await waitForResponse(page, '/jobs', 200, 'GET');
        await waitForResponse(page, '/primitives/query', 200, 'PUT');
      }
      const iframe = getIframe(page);
      await iframe.locator('[data-cy=results-list]').locator('app-result-item').first().locator('[data-cy=zoom-to-button]').click();
      await iframe.locator('[data-cy=slides-menu-button]').click();
      await page.waitForTimeout(20000);
      await page.waitForSelector('mat-progress-bar', { state: 'detached' });
      expect.soft(await page.screenshot()).toMatchSnapshot(['sample-app', 'roi-results.png'], { maxDiffPixelRatio: 0.3 });
    });
  });

  test.afterAll(async ({ browser }) => {
    const context = await browser.newContext();
    const page = await context.newPage();
    await login(page);
    await page.locator('app-case-item', { hasText: 'Test Case 01' }).click();
    await page.locator('[data-cy=apps-list]')
      .locator('app-app-item', { hasText: 'TA10v3' })
      .first()
      .locator('[data-cy=app-item]')
      .locator('[data-cy=examination-lock-button]')
      .click();
    await page.locator('[data-cy=dialog-accept-button]').click();
    await waitForResponse(page, '/close', 200, 'PUT');
    await page.close();
    await context.close();
  });
});

function getIframe(page: Page): FrameLocator {
  return page.frameLocator('iframe');
}

async function login(page: Page): Promise<void> {
  await page.goto('http://localhost:4200/#/cases');
  await page.getByRole('button', { name: 'Login' }).click();
  await page.getByLabel('Username or email').click();
  await page.getByLabel('Username or email').fill('pathologist');
  await page.getByLabel('Password').click();
  await page.getByLabel('Password').fill('secret');
  await page.getByRole('button', { name: 'Sign In' }).click();
  await expect(page).toHaveURL('http://localhost:4200/#/cases');
}

async function openSampleApp(page: Page): Promise<void> {
  await page.locator('app-case-item', { hasText: 'Test Case 01' }).click();
  await page.locator('[data-cy=add-app-button]').click();
  await page.locator('[data-cy=new-app-cards-list]', { hasText: 'TA10v3' }).click();
  await page.locator('[data-cy=apps-list]').locator('app-app-item', { hasText: 'TA10v3' }).first().locator('[data-cy=app-item]').click();
  await page.locator('[data-cy=start-app-button]').click();
  await expect(page).toHaveURL(/.*cases\/[0-9a-fA-F]{8}\b-[0-9a-fA-F]{4}\b-[0-9a-fA-F]{4}\b-[0-9a-fA-F]{4}\b-[0-9a-fA-F]{12}\/examinations\/[0-9a-fA-F]{8}\b-[0-9a-fA-F]{4}\b-[0-9a-fA-F]{4}\b-[0-9a-fA-F]{4}\b-[0-9a-fA-F]{12}$/);
  await waitForResponse(page, '/frontends/', 200, 'GET');
  await page.waitForTimeout(10000);
}

async function waitForResponse(page: Page, url: string, status: number, method: string): Promise<Response> {
  return page.waitForResponse(resp => resp.url().includes(url) && resp.status() === status && resp.request().method() === method);
}
