import { type PlaywrightTestConfig } from '@playwright/test';

const TIMEOUT = 300000;

const config: PlaywrightTestConfig = {
  outputDir: '../../dist/apps/sample-app-v3-e2e',
  timeout: TIMEOUT,
  expect: {
    timeout: TIMEOUT
  },

  retries: 5,

  use: {
    headless: true,
    viewport: { width: 1280, height: 720 },
    ignoreHTTPSErrors: true,
    video: 'on',
    screenshot: 'only-on-failure'
  },

  reporter: [
    ['list'],
    ['json', { outputFile: 'report/test-results.json' }],
    ['html', { open: 'never', outputFolder: 'report' }]
  ]
};

export default config;
