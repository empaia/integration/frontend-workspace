import { AfterViewInit, ChangeDetectionStrategy, Component } from '@angular/core';
import { Observable } from 'rxjs';
import { MenuEntity } from '@menu/models/menu.models';
import { Store } from '@ngrx/store';
import { MenuActions, MenuSelectors } from '@menu/store';
import { MenuButtonEventModel } from '@shared/components/menu-button/menu-button.models';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MenuComponent implements AfterViewInit {
  public slidesMenu$: Observable<MenuEntity>;
  public resultsMenu$: Observable<MenuEntity>;

  constructor(private store: Store) {
    this.slidesMenu$ = this.store.select(MenuSelectors.selectSlidesMenu);
    this.resultsMenu$ = this.store.select(MenuSelectors.selectResultsMenu);
  }

  ngAfterViewInit(): void {
    this.store.dispatch(MenuActions.initWorkspaceSize());
  }

  onMenuButtonClick(_event: MenuButtonEventModel): void {
    this.store.dispatch(MenuActions.showAllMenusToggle());
  }
}
