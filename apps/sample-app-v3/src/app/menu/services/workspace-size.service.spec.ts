import { TestBed } from '@angular/core/testing';

import { WorkspaceSizeService } from './workspace-size.service';

describe('WorkspaceSizeService', () => {
  let service: WorkspaceSizeService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(WorkspaceSizeService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
