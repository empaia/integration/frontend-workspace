import { createSelector } from '@ngrx/store';
import { selectScopeFeatureState, State as ModuleState, } from '../scope-feature.state';
import { AppV3ExaminationState } from 'empaia-api-lib';

export const selectScopeState = createSelector(
  selectScopeFeatureState,
  (state: ModuleState) => state.scope
);

export const selectScopeId = createSelector(
  selectScopeState,
  (state) => state.scopeId
);

export const selectExtendedScope = createSelector(
  selectScopeState,
  (state) => state.extendedScope
);

export const selectExaminationState = createSelector(
  selectExtendedScope,
  (scope) => scope?.examination_state === AppV3ExaminationState.Closed
);
