import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ScopeService {
  private _scopeId!: string;

  public get scopeId() {
    return this._scopeId;
  }

  public set scopeId(val: string) {
    this._scopeId = val;
  }
}
