import { createActionGroup, emptyProps, props } from '@ngrx/store';
import { AnnotationColorMap } from 'empaia-ui-commons';

export const AnnotationsColorMapsActions = createActionGroup({
  source: 'Annotations Color Maps Actions',
  events: {
    'Set Annotation Color Maps': props<{
      annotationRenderingHints: AnnotationColorMap[],
    }>(),
    'Select Annotation Color Map': props<{
      selected?: string | undefined,
    }>(),
    'Clear Annotation Color Maps': emptyProps()
  }
});
