import {
  AppV3CircleAnnotation,
  AppV3RectangleAnnotation,
  AppV3PolygonAnnotation,
  AppV3PointAnnotation,
  AppV3LineAnnotation,
  AppV3ArrowAnnotation,
  AppV3PostCircleAnnotation,
  AppV3PostRectangleAnnotation,
  AppV3PostPolygonAnnotation,
  AppV3PostPointAnnotation,
  AppV3PostArrowAnnotation,
  AppV3PostLineAnnotation,
} from 'empaia-api-lib';
export {
  AppV3AnnotationType as AnnotationApiType,
  AppV3AnnotationReferenceType as AnnotationReferenceType
} from 'empaia-api-lib';

export type CircleAnnotation = AppV3CircleAnnotation;
export type RectangleAnnotation = AppV3RectangleAnnotation;
export type PolygonAnnotation = AppV3PolygonAnnotation;
export type PointAnnotation = AppV3PointAnnotation;
export type LineAnnotation = AppV3LineAnnotation;
export type ArrowAnnotation = AppV3ArrowAnnotation;
export type PostCircleAnnotation = AppV3PostCircleAnnotation;
export type PostRectangleAnnotation = AppV3PostRectangleAnnotation;
export type PostPolygonAnnotation = AppV3PostPolygonAnnotation;
export type PostPointAnnotation = AppV3PostPointAnnotation;
export type PostArrowAnnotation = AppV3PostArrowAnnotation;
export type PostLineAnnotation = AppV3PostLineAnnotation;

export const INPUT_ANNOTATION_LIMIT = 10000;
