import { ANNOTATIONS_LOAD_ERROR, CREATE_ANNOTATION_ERROR, DELETE_ANNOTATION_ERROR, ErrorMap } from '@menu/models/ui.model';

export const NPP_MIN_RANGE_FACTOR = 0.75;
export const NPP_MAX_RANGE_FACTOR = 1.5;

export const ANNOTATION_QUERY_LIMIT = 10000;

export const VIEWER_ANNOTATIONS_ERROR_MAP: ErrorMap = {
  '[APP/Annotations-Viewer] Load Annotation Ids Failure': ANNOTATIONS_LOAD_ERROR,
  '[APP/Annotations-Viewer] Load Annotations Failure': ANNOTATIONS_LOAD_ERROR,
  '[APP/Annotations-Viewer] Create Annotation Failure': CREATE_ANNOTATION_ERROR,
  '[APP/Annotations-Viewer] Delete Annotation Failure': DELETE_ANNOTATION_ERROR,
};
