import { Injectable } from '@angular/core';
import {
  Annotation,
  AnnotationCircle,
  AnnotationPoint,
  AnnotationPolygon,
  AnnotationRectangle,
  AnnotationReferenceType,
  AnnotationType,
  Class,
  DataCreatorType,
} from 'slide-viewer';

import { AnnotationApiOutput, AnnotationApiPostType, } from '@annotations/services/annotation-types';
import {
  AnnotationApiType,
  CircleAnnotation,
  PointAnnotation,
  PolygonAnnotation,
  PostCircleAnnotation,
  PostPointAnnotation,
  PostPolygonAnnotation,
  PostRectangleAnnotation,
  RectangleAnnotation
} from '@annotations/store/annotations/annotations.models';


@Injectable({
  providedIn: 'root'
})
export class AnnotationConversionService {

  /**
   * from Viewer to Api type
   */
  public toApiType(annotation: Annotation): AnnotationApiOutput {
    switch (annotation.annotationType) {
      case AnnotationType.CIRCLE: {
        return this.toApiCircle(annotation as AnnotationCircle);
      }
      case AnnotationType.RECTANGLE: {
        return this.toApiRectangle(annotation as AnnotationRectangle);
      }
      case AnnotationType.POLYGON: {
        return this.toApiPolygon(annotation as AnnotationPolygon);
      }
      case AnnotationType.POINT: {
        return this.toApiPoint(annotation as AnnotationPoint);
      }
      default: {
        throw new Error(
          `Unknown Annotation received from Slide Viewer ${annotation?.annotationType}`
        );
      }
    }
  }

  /**
   * from Viewer to Api POST type
   */
  public toApiPostType(annotation: Annotation, slideId: string, scopeId: string): AnnotationApiPostType {
    switch (annotation.annotationType) {
      case AnnotationType.CIRCLE: {
        return this.toApiCirclePost(
          annotation as AnnotationCircle,
          slideId,
          scopeId,
        );
      }
      case AnnotationType.RECTANGLE: {
        return this.toApiRectanglePost(
          annotation as AnnotationRectangle,
          slideId,
          scopeId,
        );
      }
      case AnnotationType.POLYGON: {
        return this.toApiPolygonPost(
          annotation as AnnotationPolygon,
          slideId,
          scopeId,
        );
      }
      case AnnotationType.POINT: {
        return this.toApiPointPost(
          annotation as AnnotationPoint,
          slideId,
          scopeId,
        );
      }
      default: {
        throw new Error(
          `Unknown Annotation received from Slide Viewer ${annotation?.annotationType}`
        );
      }
    }
  }

  /**
   * from  Api to viewer types
   */
  public fromApiType(annotation: AnnotationApiOutput): Annotation {
    switch (annotation.type) {
      case AnnotationType.CIRCLE: {
        return this.fromApiToCircle(annotation as CircleAnnotation);
      }
      case AnnotationType.RECTANGLE: {
        return this.fromApiToRectangle(annotation as RectangleAnnotation);
      }
      case AnnotationType.POLYGON: {
        return this.fromApiToPolygon(annotation as PolygonAnnotation);
      }
      case AnnotationType.POINT: {
        return this.fromApiToPoint(annotation as PointAnnotation);
      }
      default: {
        throw new Error(
          `Unknown Annotation received from API ${annotation?.type}`
        );
      }
    }
  }

  public convertAnnotationTypeToApi(type: AnnotationType): AnnotationApiType {
    switch (type) {
      case AnnotationType.ARROW:
        return AnnotationApiType.Arrow;
      case AnnotationType.CIRCLE:
        return AnnotationApiType.Circle;
      case AnnotationType.LINE:
        return AnnotationApiType.Line;
      case AnnotationType.POINT:
        return AnnotationApiType.Point;
      case AnnotationType.POLYGON:
        return AnnotationApiType.Polygon;
      case AnnotationType.RECTANGLE:
        return AnnotationApiType.Rectangle;
    }
  }

  public convertAnnotationTypeFromApi(type: AnnotationApiType): AnnotationType {
    switch (type) {
      case AnnotationApiType.Arrow:
        return AnnotationType.ARROW;
      case AnnotationApiType.Circle:
        return AnnotationType.CIRCLE;
      case AnnotationApiType.Line:
        return AnnotationType.LINE;
      case AnnotationApiType.Point:
        return AnnotationType.POINT;
      case AnnotationApiType.Polygon:
        return AnnotationType.POLYGON;
      case AnnotationApiType.Rectangle:
        return AnnotationType.RECTANGLE;
    }
  }

  /**
   * from Viewer to Api
   */
  private basePropsToApi(
    annotation: Annotation
  ): Partial<AnnotationApiOutput> {
    return {
      name: annotation.name,
      // server rejects empty strings - either pass undefined or string with length > 0
      description:
        annotation.description && annotation.description.length > 0
          ? annotation.description
          : undefined,
      creator_type: DataCreatorType.User,
      reference_type: AnnotationReferenceType.Wsi,
      npp_created: annotation.nppCreated,
      npp_viewing: annotation.nppViewing,
      centroid: annotation.centroid,
      reference_id: annotation.referenceId ?? 'fake_ref_id',
      creator_id: annotation.creatorId ?? 'fake_user_id',
    };
  }

  /**
   * from Viewer to Api
   */
  private toApiCircle(a: AnnotationCircle): CircleAnnotation {
    return {
      ...this.basePropsToApi(a),
      type: AnnotationType.CIRCLE,
      center: a.center,
      radius: a.radius,
    } as CircleAnnotation;
  }

  private toApiRectangle(a: AnnotationRectangle): RectangleAnnotation {
    return {
      ...this.basePropsToApi(a),
      type: AnnotationType.RECTANGLE,
      upper_left: a.coordinates[0],
      width: a.coordinates[1][0] - a.coordinates[0][0],
      height: a.coordinates[3][1] - a.coordinates[0][1],
    } as RectangleAnnotation;
  }

  private toApiPolygon(a: AnnotationRectangle): PolygonAnnotation {
    return {
      ...this.basePropsToApi(a),
      type: AnnotationType.POLYGON,
      coordinates: a.coordinates,
    } as PolygonAnnotation;
  }

  private toApiPoint(a: AnnotationPoint): PointAnnotation {
    return {
      ...this.basePropsToApi(a),
      type: AnnotationType.POINT,
      coordinates: a.coordinates,
    } as PointAnnotation;
  }

  /**
   * from Viewer to Api Post types
   */
  private basePropsToApiPost(
    annotation: Annotation,
    slideId: string,
    scopeId: string
  ): Partial<AnnotationApiPostType> {
    return {
      name: annotation.name,
      description:
        annotation.description && annotation.description.length > 0
          ? annotation.description
          : undefined,
      npp_created: annotation.nppCreated,
      npp_viewing: annotation.nppViewing,
      centroid: annotation.centroid,
      reference_id: slideId,
      reference_type: AnnotationReferenceType.Wsi,
      creator_id: scopeId,
      creator_type: DataCreatorType.Scope,
    };
  }

  private toApiCirclePost(a: AnnotationCircle, slideId: string, scopeId: string): PostCircleAnnotation {
    const circle = {
      ...this.basePropsToApiPost(a, slideId, scopeId),
      type: AnnotationType.CIRCLE,
      center: a.center,
      radius: a.radius,
    };

    if (this.isPostCircle(circle)) {
      return circle;
    } else {
      throw new Error('Incomplete Post Annotation type Circle');
    }
  }


  private toApiRectanglePost(a: AnnotationRectangle, slideId: string, scopeId: string): PostRectangleAnnotation {
    const rect = {
      ...this.basePropsToApiPost(a, slideId, scopeId),
      type: AnnotationType.RECTANGLE,
      upper_left: a.coordinates[0],
      width: a.coordinates[1][0] - a.coordinates[0][0],
      height: a.coordinates[3][1] - a.coordinates[0][1],
    };

    if (this.isPostRectangle(rect)) {
      return rect;
    } else {
      throw new Error('Incomplete Post Annotation type Rectangle');
    }
  }

  private toApiPolygonPost(a: AnnotationPolygon, slideId: string, scopeId: string): PostPolygonAnnotation {
    const poly = {
      ...this.basePropsToApiPost(a, slideId, scopeId),
      type: AnnotationType.POLYGON,
      coordinates: a.coordinates,
    };

    if (this.isPostPolygon(poly)) {
      return poly;
    } else {
      throw new Error('Incomplete Post Annotation type Polygon');
    }
  }

  private toApiPointPost(a: AnnotationPoint, slideId: string, scopeId: string): PostPointAnnotation {
    const point = {
      ...this.basePropsToApiPost(a, slideId, scopeId),
      type: AnnotationType.POINT,
      coordinates: a.coordinates,
    };

    if (this.isPostPoint(point)) {
      return point;
    } else {
      throw new Error('Incomplete Post Annotation type Point');
    }
  }

  /**
   * Type checks
   */
  private isPostCircle = (annotation: unknown): annotation is PostCircleAnnotation => {
    const post = annotation as PostCircleAnnotation;
    return post.center !== undefined || post.name !== undefined;
  };

  private isPostRectangle = (annotation: unknown): annotation is PostRectangleAnnotation => {
    const post = annotation as PostRectangleAnnotation;
    return post.npp_created !== undefined || post.name !== undefined;
  };

  private isPostPolygon = (annotation: unknown): annotation is PostPolygonAnnotation => {
    const post = annotation as PostPolygonAnnotation;
    return post.npp_created !== undefined || post.name !== undefined;
  };

  private isPostPoint = (annotation: unknown): annotation is PostPointAnnotation => {
    const post = annotation as PostPointAnnotation;
    return post.npp_created !== undefined || post.name !== undefined;
  };


  /**
   * from Api to Viewer
   */
  private basePropsFromApi(
    annotation: AnnotationApiOutput
  ): Partial<Annotation> {
    return {
      id: annotation.id,
      name: annotation.name,
      // timestamp is in milliseconds
      createdAt: new Date(annotation.created_at ?? 0),
      updatedAt: new Date(annotation.updated_at ?? 0),
      description: annotation.description,
      creatorId: annotation.creator_id,
      referenceId: annotation.reference_id,
      referenceType: annotation.reference_type,
      nppCreated: annotation.npp_created,
      nppViewing: annotation.npp_viewing,
      // annotationType: has to be set in concrete types !!
      // classIds: annotation.classes?.map((c) => c.id as string),
      centroid: annotation.centroid,
      classes: this.classFromApi(annotation),
      isLocked: annotation.is_locked,
    };
  }

  private classFromApi(annotation: AnnotationApiOutput): Array<Class> | undefined {
    // add 'NO_CLASS_VALUE' constant to every annotation that has no class
    if (!annotation.classes || (annotation.classes && annotation.classes?.length <= 0)) {
      // return [{ value: 'unclassified' }];
      return annotation.classes as Class[];
    } else {
      return annotation.classes?.map((c) => ({
        ...c,
        creator_type: this.creatorTypeFromApi(c.creator_type)
      }));
    }
  }

  private creatorTypeFromApi(t: DataCreatorType): DataCreatorType {
    switch (t) {
      case DataCreatorType.Job:
        return DataCreatorType.Job;
      case DataCreatorType.User:
        return DataCreatorType.User;
      case DataCreatorType.Scope:
        return DataCreatorType.Scope;
    }
  }

  private fromApiToCircle(a: CircleAnnotation): AnnotationCircle {
    return {
      ...this.basePropsFromApi(a),
      annotationType: AnnotationType.CIRCLE,
      radius: a.radius,
      center: a.center,
      nppCreated: a.npp_created,
    };
  }

  private fromApiToRectangle(
    a: RectangleAnnotation
  ): AnnotationRectangle {
    const coords: number[][] = new Array(5)
      .fill(0)
      .map(() => new Array(2).fill(0));
    // restore point list
    coords[0][0] = a.upper_left[0];
    coords[0][1] = a.upper_left[1];
    coords[1][0] = a.upper_left[0] + a.width;
    coords[1][1] = a.upper_left[1];
    coords[2][0] = a.upper_left[0] + a.width;
    coords[2][1] = a.upper_left[1] + a.height;
    coords[3][0] = a.upper_left[0];
    coords[3][1] = a.upper_left[1] + a.height;
    // last point is same as first
    coords[4][0] = a.upper_left[0];
    coords[4][1] = a.upper_left[1];

    return {
      ...this.basePropsFromApi(a),
      annotationType: AnnotationType.RECTANGLE,
      coordinates: coords,
      nppCreated: a.npp_created,
    };
  }

  private fromApiToPolygon(a: PolygonAnnotation): AnnotationPolygon {
    return {
      ...this.basePropsFromApi(a),
      annotationType: AnnotationType.POLYGON,
      coordinates: a.coordinates,
      nppCreated: a.npp_created,
    };
  }

  private fromApiToPoint(a: PointAnnotation): AnnotationPoint {
    return {
      ...this.basePropsFromApi(a),
      annotationType: AnnotationType.POINT,
      coordinates: a.coordinates,
      nppCreated: a.npp_created,
    };
  }
}
