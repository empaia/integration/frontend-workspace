import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  OnChanges,
  Output,
  SimpleChanges
} from '@angular/core';
import { FINISHED_JOB_STATES, ResultEntity } from '@results/store/results/results.models';
import { MatCheckboxChange } from '@angular/material/checkbox';
import { JobSelector } from '@jobs/store/jobs/jobs.models';
import { AnnotationEntity, ClassColorMapping } from 'slide-viewer';


@Component({
  selector: 'app-result-item',
  templateUrl: './result-item.component.html',
  styleUrls: ['./result-item.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ResultItemComponent implements OnChanges {
  @Input() public result!: ResultEntity;
  @Input() public selected!: boolean;
  @Input() public version!: string;
  @Input() public classColorMapping?: ClassColorMapping;

  @Output() public changeSelection = new EventEmitter<JobSelector>();
  @Output() public zoomToAnnotation = new EventEmitter<AnnotationEntity>();
  @Output() public interruptJobExecution = new EventEmitter<string>();

  jobInfo = '';
  classVersion = 'v1';
  readonly FINISHED_JOB_STATES = FINISHED_JOB_STATES;

  ngOnChanges(changes: SimpleChanges) {
    if (changes.result) {
      this.jobInfo = `Job-ID: ${this.result.job.id}`;
      this.jobInfo += this.result.job.error_message ? `\nError: ${this.result.job.error_message}` : '';
    }
    if (changes.version) {
      this.classVersion = this.version === 'v2' ? this.version : 'v3.0';
    }
  }

  onCheckboxChange(change: MatCheckboxChange): void {
    const jobSelector: JobSelector = {
      id: this.result.job.id,
      checked: change.checked,
    };
    this.changeSelection.emit(jobSelector);
  }

  onJobStop(id: string): void {
    this.interruptJobExecution.emit(id);
  }
}
