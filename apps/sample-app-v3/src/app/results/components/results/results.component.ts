import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { SelectionError } from '@menu/models/ui.model';
import { MenuType } from '@menu/models/menu.models';
import { ResultEntity } from '@results/store/results/results.models';
import { Dictionary } from '@ngrx/entity';
import { JobSelector } from '@jobs/store/jobs/jobs.models';
import { AnnotationEntity, ClassColorMapping } from 'slide-viewer';

@Component({
  selector: 'app-results',
  templateUrl: './results.component.html',
  styleUrls: ['./results.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ResultsComponent {
  @Input() public results!: ResultEntity[];
  @Input() public jobsSelections!: Dictionary<JobSelector>;
  @Input() public classColorMapping?: ClassColorMapping;
  @Input() public version!: string;
  @Input() public selectionMissing!: SelectionError;

  @Output() public zoomToAnnotation = new EventEmitter<AnnotationEntity>();
  @Output() public changeJobSelection = new EventEmitter<JobSelector>();
  @Output() public errorClicked = new EventEmitter<MenuType>();
  @Output() public interruptJobExecution = new EventEmitter<string>();
}
