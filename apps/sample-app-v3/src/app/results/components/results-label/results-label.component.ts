import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-results-label',
  templateUrl: './results-label.component.html',
  styleUrls: ['./results-label.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ResultsLabelComponent {
  @Input() resultCount!: string;
  @Input() selected!: boolean;
  @Input() contentLoaded!: boolean;

  @Input() resultsSelected!: boolean;

  @Output() resultsSelectionChanged = new EventEmitter<boolean>();
}
