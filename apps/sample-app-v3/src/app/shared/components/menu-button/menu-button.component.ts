import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { MenuType } from '@menu/models/menu.models';
import { MenuButtonEventModel } from '@shared/components/menu-button/menu-button.models';

@Component({
  selector: 'app-menu-button',
  templateUrl: './menu-button.component.html',
  styleUrls: ['./menu-button.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MenuButtonComponent {
  @Input() public buttonValue!: MenuType;
  @Input() public selected = false;
  @Input() public enableTopBroderBoxShadow = true;

  @Output() public buttonClicked = new EventEmitter<MenuButtonEventModel>();

  onButtonClick($event: MouseEvent): void {
    this.buttonClicked.emit({ mouse: $event, value: this.buttonValue });
  }

  getClass(): string {
    let cssClass = this.selected ? 'mat-primary' : 'button-bg';
    if (this.enableTopBroderBoxShadow) {
      cssClass += ' menu-border-box-shadow';
    }
    return cssClass;
  }
}
