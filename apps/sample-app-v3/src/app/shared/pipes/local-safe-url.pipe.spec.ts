import { LocalSafeUrlPipe } from './local-safe-url.pipe';
import { createPipeFactory, SpectatorPipe } from '@ngneat/spectator/jest';

describe('LocalSafeUrlPipe', () => {
  let spectator: SpectatorPipe<LocalSafeUrlPipe>;
  const createPipe = createPipeFactory(LocalSafeUrlPipe);

  beforeEach(() => spectator = createPipe());

  it('should create a pipe for testing', () => {
    expect(spectator.element).toBeTruthy();
  });
});
