import { AppV3ClassesDict } from 'empaia-api-lib';

export type ClassesDict = AppV3ClassesDict;

export interface ClassEntity {
  id: string;
}

export interface EmpaiaAppDescription {
  name: string;
  name_short: string;
  namespace: string;
  description: string;
  inputs: object;
  outputs: object;
  classes?: object;
}

export interface ClassDictionaries {
  [p: string]: ClassesDict;
}
