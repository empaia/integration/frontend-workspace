import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';

import { WbsAppApiV3Module } from 'empaia-api-lib';

import { AppComponent } from '@core/containers/app/app.component';
import { AppRouterModule } from '@router/app-router.module';
import { AnnotationsModule } from '@annotations/annotations.module';
import { CoreModule } from '@core/core.module';
import { JobsModule } from '@jobs/jobs.module';
import { MaterialModule } from '@material/material.module';
import { MenuModule } from '@menu/menu.module';
import { SharedModule } from '@shared/shared.module';
import { MetaReducer, StoreModule } from '@ngrx/store';
import { environment } from '../environments/environment';
import { storeFreeze } from 'ngrx-store-freeze';
import { EffectsModule } from '@ngrx/effects';
import { AuthenticationInterceptor } from '@core/services/interceptors/authentication.interceptor';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { ScopeModule } from '@scope/scope.module';
import { TokenModule } from '@token/token.module';
import { WbsUrlModule } from '@wbsUrl/wbs-url.module';
import { ClassesModule } from '@classes/classes.module';
import { MAT_TOOLTIP_DEFAULT_OPTIONS } from '@angular/material/tooltip';
import { customTooltipDefaultOptions } from '@material/models/custom-options.models';
import { EmpaiaUiCommonsModule } from 'empaia-ui-commons';

// The app now by it's own with wbs api version to use
const API_VERSION = 'v3';

export const metaReducers: MetaReducer<object>[] = !environment.production
  ? [storeFreeze]
  : [];

@NgModule({
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    StoreModule.forRoot({},
      {
        metaReducers,
        runtimeChecks: {
          strictActionImmutability: true,
          strictStateImmutability: true,
          strictActionWithinNgZone: true
        }
      }
    ),
    EffectsModule.forRoot([]),
    !environment.production ? StoreDevtoolsModule.instrument({
      name: 'Sample-App Store',
      connectInZone: true}) : [],
    WbsAppApiV3Module,
    AnnotationsModule,
    AppRouterModule,
    ClassesModule,
    CoreModule,
    JobsModule,
    MaterialModule,
    MenuModule,
    SharedModule,
    ScopeModule,
    TokenModule,
    WbsUrlModule,
    EmpaiaUiCommonsModule,
  ],
  providers: [
    { provide: 'API_VERSION', useValue: API_VERSION },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthenticationInterceptor,
      multi: true,
    },
    { provide: MAT_TOOLTIP_DEFAULT_OPTIONS, useValue: customTooltipDefaultOptions }
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
