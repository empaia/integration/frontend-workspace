import { CREATE_JOB_ERROR, DELETE_JOB_ERROR, ErrorMap, JOBS_LOAD_ERROR, UPDATE_JOB_ERROR } from '@menu/models/ui.model';
import { AppV3Job, AppV3JobStatus } from 'empaia-api-lib';
export {
  AppV3JobCreatorType as JobCreatorType,
  AppV3JobStatus as JobStatus,
} from 'empaia-api-lib';

export type Job = AppV3Job;

export interface JobSelector {
  id: string;
  checked: boolean;
}

export const JOB_POLLING_PERIOD = 5000;

export function isJobRunning(job: Job): boolean {
  return job.status === AppV3JobStatus.Assembly
    || job.status === AppV3JobStatus.Ready
    || job.status === AppV3JobStatus.Scheduled
    || job.status === AppV3JobStatus.Running;
}

export function compareJobStatus(prev: Job[], curr: Job[]): boolean {
  return prev.length === curr.length && prev.every((job, index) => job.status === curr[index].status);
}

export const JOBS_ERROR_MAP: ErrorMap = {
  '[APP/Jobs] Load Jobs Failure': JOBS_LOAD_ERROR,
  '[APP/Jobs] Create Job Failure': CREATE_JOB_ERROR,
  '[APP/Jobs] Set Job Input Failure': UPDATE_JOB_ERROR,
  '[APP/Jobs] Run Job Failure': UPDATE_JOB_ERROR,
  '[APP/Jobs] Delete Job Failure': DELETE_JOB_ERROR,
  '[Jobs] Stop Running Job Failure': UPDATE_JOB_ERROR,
};
