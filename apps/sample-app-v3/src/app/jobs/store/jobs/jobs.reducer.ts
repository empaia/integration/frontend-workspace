import { createReducer, on } from '@ngrx/store';
import { createEntityAdapter, EntityState } from '@ngrx/entity';
import { HttpError } from '@menu/models/ui.model';
import * as JobsActions from './jobs.actions';
import { Job, JobSelector } from '@jobs/store/jobs/jobs.models';
import { upsertManyWithoutUpdate } from '@shared/helper/ngrx-operators';
import { isNotNullish, isString } from '@shared/helper/array-operators';


export const JOBS_FEATURE_KEY = 'jobs';

export interface State extends EntityState<Job> {
  selectedJobs: EntityState<JobSelector>;
  allSelected: boolean;
  loaded: boolean;
  error?: HttpError | null;
}

export const jobAdapter = createEntityAdapter<Job>();
export const jobSelectorAdapter = createEntityAdapter<JobSelector>();

export const initialState: State = jobAdapter.getInitialState({
  selectedJobs: jobSelectorAdapter.getInitialState(),
  allSelected: true,
  loaded: true,
  error: null,
});


export const reducer = createReducer(
  initialState,
  on(JobsActions.loadJobs, (state): State => ({
    ...state,
    loaded: false,
  })),
  on(JobsActions.loadJobsSuccess, (state, { jobs }): State =>
    jobAdapter.setAll(jobs, {
      ...state,
      loaded: true,
      selectedJobs: upsertManyWithoutUpdate(jobSelectorAdapter, convertJobsToJobsSelectors(jobs, true), state.selectedJobs)
    })
  ),
  on(JobsActions.loadJobsFailure, (state, { error }): State => ({
    ...state,
    error,
  })),
  on(JobsActions.createJob, (state): State => ({
    ...state,
    loaded: false,
  })),
  on(JobsActions.createJobSuccess, (state, { job }): State =>
    jobAdapter.upsertOne(job, {
      ...state,
      loaded: true,
      selectedJobs: jobSelectorAdapter.addOne(convertJobToJobSelector(job, true), {
        ...state.selectedJobs
      })
    })
  ),
  on(JobsActions.createJobFailure, (state, { error }): State => ({
    ...state,
    error,
  })),
  on(JobsActions.deleteJobSuccess, (state, { jobId }): State =>
    jobAdapter.removeOne(jobId, {
      ...state,
      selectedJobs: jobSelectorAdapter.removeOne(jobId, {
        ...state.selectedJobs
      })
    })
  ),
  on(JobsActions.deleteJobFailure, (state, { error }): State => ({
    ...state,
    error,
  })),
  on(JobsActions.setJobSelection, (state, { jobSelection }): State => ({
    ...state,
    selectedJobs: jobSelectorAdapter.upsertOne(jobSelection, {
      ...state.selectedJobs
    })
  })),
  on(JobsActions.setJobsSelections, (state, { jobsSelections }): State => ({
    ...state,
    selectedJobs: jobSelectorAdapter.upsertMany(jobsSelections, {
      ...state.selectedJobs
    })
  })),
  on(JobsActions.showAllJobs, (state): State => ({
    ...state,
    allSelected: true,
    selectedJobs: jobSelectorAdapter.setAll(
      [...state.ids].filter(isNotNullish).filter(isString).map(id => convertIdToJobSelector(id, true)),
      {
        ...state.selectedJobs
      }
    )
  })),
  on(JobsActions.hideAllJobs, (state): State => ({
    ...state,
    allSelected: false,
    selectedJobs: jobSelectorAdapter.setAll(
      [...state.ids].filter(isNotNullish).filter(isString).map(id => convertIdToJobSelector(id, false)),
      {
        ...state.selectedJobs
      }
    )
  })),
  on(JobsActions.clearJobs, (state): State =>
    jobAdapter.removeAll({
      ...state,
      selectedJobs: jobSelectorAdapter.removeAll({
        ...state.selectedJobs
      }),
      allSelected: true,
      loaded: true,
      error: null,
    })
  ),
  on(JobsActions.stopRunningJob, (state): State => ({
    ...state,
    loaded: false,
  })),
  on(JobsActions.stopRunningJobSuccess, (state): State => ({
    ...state,
    loaded: true,
  })),
  on(JobsActions.stopRunningJobFailure, (state, { error }): State => ({
    ...state,
    error,
    loaded: true,
  })),
  on(JobsActions.stopLoading, (state): State => ({
    ...state,
    loaded: true,
  }))
);

const convertJobToJobSelector = (job: Job, checked: boolean): JobSelector => {
  return {
    id: job.id,
    checked,
  };
};

const convertJobsToJobsSelectors = (jobs: Job[], checked: boolean): JobSelector[] => {
  return jobs.map(job => convertJobToJobSelector(job, checked));
};

const convertIdToJobSelector = (id: string, checked: boolean): JobSelector => {
  return {
    id,
    checked,
  };
};
