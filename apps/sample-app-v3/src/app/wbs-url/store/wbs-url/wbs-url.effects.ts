import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { WbsUrlService } from '@wbsUrl/services/wbs-url.service';
import * as WbsUrlActions from './wbs-url.actions';
import { map } from 'rxjs/operators';



@Injectable()
export class WbsUrlEffects {

  setWbsUrl$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(WbsUrlActions.setWbsUrl),
      map(action => action.wbsUrl),
      map(wbsUrl => this.wbsUrlService.wbsUrl = wbsUrl.url)
    );
  }, { dispatch: false });

  constructor(
    private readonly actions$: Actions,
    private readonly wbsUrlService: WbsUrlService,
  ) {}

}
