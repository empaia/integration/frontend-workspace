# Sample App V3 Changelog

# 3.4.3 (2024-07-16)

### Refactor

* upgraded to angular 17

# 3.4.2 (2024-01-11)

### Refactor

* updated to angular 16

# 3.4.1 (2024-01-05)

### Refactor

* changed annotation loading spinner position and appearance

# 3.4.0 (2023-12-22)

### Features

* added support for annotation rendering hints of ead

# 3.3.8 (2023-11-02)

### Refactor

* refactored error messages for annotations and jobs

# 3.3.4 (2023-09-18)

### Refactor

* positioned tooltips above mouse cursor when ever possible

# 3.3.0 (2023-08-03)

### Features

* added multi user support

# 3.2.0 (2023-08-02)

### Refactor

* optimized slide label and thumbnail fetching

# 3.1.0 (2023-06-12)

### Features

* preloads all fonts from the start

# 3.0.9 (2023-06-08)

### Refactor

* refactored tooltips

# 3.0.2 (2023-03-24)

### Refactor

* use css for truncate text

# 3.0.0 (2023-03-20)

### Features

* updated to Angular 15

# 0.0.5 (2023-01-20)

### Fixes

* ROI's don't reappear after deselection

# 0.0.1 (2022-11-24)

### Refactor

* created Sample App V3 based on Sample App V2
