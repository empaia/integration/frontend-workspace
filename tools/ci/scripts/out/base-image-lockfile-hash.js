"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const get_lockfile_hash_1 = require("./get-lockfile-hash");
(async () => {
    const hash = (0, get_lockfile_hash_1.getLockfileHash)();
    process.stdout.write(hash);
})();
