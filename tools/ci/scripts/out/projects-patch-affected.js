"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const cp = require("child_process");
const path = require("path");
const fs = require("fs");
const semver = require("semver");
const os = require("os");
const load_dot_env_1 = require("./load-dot-env");
const check_version_1 = require("./check-version");
const getDirectories = (srcPath) => fs.readdirSync(srcPath).filter(file => fs.statSync(path.join(srcPath, file)).isDirectory());
(async () => {
    // console.log('starting');
    const [, , ...args] = process.argv;
    // only json file arg is required!
    // other args only used for debugging on local machine
    if (!args || !args[0]) {
        console.error(`
      ERROR expected argument:
      check
      ---or---
      write
    `);
        return;
    }
    if (!process.env.GITLAB_CI) {
        await (0, load_dot_env_1.loadDotEnv)();
    }
    const mode = args[0];
    let affectedArray = [];
    // console.log(affected);
    if (mode === 'patch-apps') {
        const rootDir = process.cwd();
        const appsDir = rootDir + '/apps';
        affectedArray = getDirectories(appsDir).filter(p => !p.endsWith('-e2e'));
    }
    else {
        // get affected projects names
        const affected = cp.execSync(`nx print-affected base=origin/master --select=projects`).toString();
        // format to array and remove e2e test projects
        affectedArray = affected.trim().split(', ').filter(e => !e.endsWith('-e2e'));
    }
    const affectedProjects = readAffectedProjects(affectedArray);
    // console.log(affectedArray);
    // patch version / package file version field
    for (const project of affectedProjects) {
        // 'security-test' is not versioned
        if (project.name === 'security-test')
            continue;
        let fileContent;
        let newFile;
        try {
            console.log(project.path);
            fileContent = await fs.promises.readFile(`${project.path}`, 'utf-8');
        }
        catch (error) {
            console.error(`ERROR: reading file at ${project.path}`);
            continue;
        }
        const vJson = JSON.parse(fileContent);
        const version = vJson.version;
        // console.log(version);
        const semV = semver.coerce(version);
        if (semV) {
            const newV = semver.inc(semV, 'patch');
            if (newV == null) {
                throw new Error("newV is null");
            }
            vJson.version = newV;
            newFile = JSON.stringify(vJson, null, '  ').concat(os.EOL);
            const tagInfo = await getTagInfoFromApi(project);
            if (tagInfo) {
                let infoString = versionBumpWarn(tagInfo, newV);
                console.log(`${project.type} | ${project.name}:`);
                console.log(`    | this ${version} - next ${newV}`);
                console.log(`    | ${tagInfo} ${infoString}`);
                console.log(os.EOL);
                if (mode === 'write' || mode === 'patch-apps') {
                    try {
                        // const testPath = path.parse(app.path).dir + '/version-test.json';
                        await fs.promises.writeFile(project.path, newFile, 'utf-8');
                    }
                    catch (error) {
                        console.error(`ERROR: writing file for ${project.name}`);
                    }
                }
            }
        }
    }
})();
function readAffectedProjects(affectedArray) {
    // get project root
    // const rootDir = path.join(__dirname, '../../..');
    const rootDir = process.cwd();
    console.log(`project root: \n`, rootDir);
    const appsDir = rootDir + '/apps';
    const libsDir = rootDir + '/libs';
    // get all folder in sub dirs - used to determine project type (app or lib)
    const appsDirs = getDirectories(appsDir);
    const libsDirs = getDirectories(libsDir);
    // console.log(appsDirs);
    // filter only affected by project type
    const affectedApps = appsDirs.filter(v => affectedArray.includes(v));
    const affectedLibs = libsDirs.filter(v => affectedArray.includes(v));
    console.log(`affected apps: \n`, affectedApps);
    console.log(`affected libs: \n`, affectedLibs);
    // create array with interface containing type and path
    const affectedProjects = [];
    affectedApps.map(appName => affectedProjects.push({
        name: appName,
        path: appsDir + '/' + appName + '/version.json',
        type: 'app',
    }));
    affectedLibs.map(libName => affectedProjects.push({
        name: libName,
        path: libsDir + '/' + libName + '/package.json',
        type: 'lib',
    }));
    affectedProjects.forEach(i => i.path = i.path.split('\\').join('/'));
    return affectedProjects;
}
function versionBumpWarn(taggedVersion, currentVersion) {
    // const tagVS = tagInfo.slice(tagInfo.search(' last tag '), tagInfo.length - 1);
    const tagVersion = semver.coerce(taggedVersion);
    // console.log('tagVersion', tagVersion?.version);
    const thisVersion = semver.parse(currentVersion);
    // console.log('thisVersion', thisVersion?.version);
    let infoString = '';
    if (thisVersion && tagVersion) {
        if (tagVersion?.patch + 1 === thisVersion?.patch) {
            infoString = '✅';
        }
        if (thisVersion?.patch !== tagVersion?.patch + 1) {
            infoString = `❌ ${os.EOL}    | --> Wanted ${tagVersion.major}.${tagVersion.minor}.${tagVersion.patch + 1} ?`;
        }
    }
    return infoString;
}
async function getTagInfoFromApi(project) {
    // request api
    const instanceUrl = process.env.CI_SERVER_URL;
    const projectId = process.env.CI_PROJECT_ID;
    const proxy = process.env.HTTP_PROXY;
    const projectName = project.name;
    // const scriptCall = `node ./tools/ci/scripts/out/check-version.js`
    // const pathParam = project.type === 'app' ? `./apps/${project.name}/version.json` : `./libs/${project.name}/package.json`;
    // const scriptExec = `${scriptCall} ${pathParam} ${projectId} ${baseUrl}`
    // const versionResp = cp.execSync(scriptExec).toString();
    // const tagInfo = versionResp.slice(versionResp.search('this '), versionResp.length).trim();
    // // console.info(tagInfo);
    if (instanceUrl && projectId && projectName) {
        const response = await (0, check_version_1.getTagVersion)({
            projectName,
            projectId,
            instanceUrl,
            proxy
        });
        const tagVersion = (0, check_version_1.parseTags)(response, projectName);
        return tagVersion;
    }
    return undefined;
}
