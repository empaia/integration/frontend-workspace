import { getLockfileHash } from './get-lockfile-hash';


(async () => {
  const hash = getLockfileHash();
  process.stdout.write(hash);
})();

