import { readdirSync, statSync } from 'fs';
import { join } from 'path';
import { loadDotEnv } from './load-dot-env';

/**
 * get all files in directory
 * @param dirPath directory to check
 * @param arrayOfFiles existing array with files - used for recursive call
 * @returns array of files in directory and all subdirs
 */


const getAllFiles = (dirPath: string, arrayOfFiles: string[]): string[] => {
  const files = readdirSync(dirPath);

  arrayOfFiles = arrayOfFiles || [];

  files.forEach(function (file) {
    if (statSync(dirPath + "/" + file).isDirectory()) {
      arrayOfFiles = getAllFiles(dirPath + "/" + file, arrayOfFiles);
    } else {
      arrayOfFiles.push(join(dirPath, "/", file));
    }
  })

  return arrayOfFiles;
}

const getDockerfiles = (rootDir: string): string[] => {

  const dockerDir = rootDir + '/tools/docker';
  // get all Dockerfile
  const files = getAllFiles(dockerDir, []).filter(is => is.search('Dockerfile') > -1);
  return files;
}

const addRootCiFile = (rootDir: string, files: string[]): string[] => {
  // add '/.gitlab-ci.yml to files
  const entries = [rootDir + '/.gitlab-ci.yml', ...files];
  return entries;
}

const getBaseImageFiles = (): string[] => {
  const rootDir = process.cwd();
  const files = getDockerfiles(rootDir);
  const allFiles = addRootCiFile(rootDir, files);
  return allFiles;
}

// construct image name from env vars
const getBaseImageName = async (): Promise<string> => {
  if (!process.env.GITLAB_CI) {
    await loadDotEnv();
  }
  // CI_REGISTRY = registry.gitlab.com
  // CI_PROJECT_PATH = empaia/integration/ci-examples-frontend
  const baseImageName = `${process.env.CI_REGISTRY}/${process.env.CI_PROJECT_PATH}/ci-base-image`;
  return baseImageName;
}

export { getBaseImageFiles, getBaseImageName };