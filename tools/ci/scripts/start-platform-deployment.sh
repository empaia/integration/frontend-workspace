#!/bin/bash

if [ $# -eq 4 ]; then
  git clone --recurse-submodules https://gitlab.com/empaia/integration/platform-deployment.git
  cd platform-deployment
  python3 -m venv .venv
  source .venv/bin/activate
  export PYTHONPATH=$(pwd)
  poetry install
  cd deployments/auth/
  cp sample.env .env
  docker compose down -v --remove-orphans
  docker compose pull
  docker compose up -d --remove-orphans
  python3 ../../scripts/wait_for_services.py docker-compose.yml -v --all-services --ignore data-management-client workbench-client
  python3 -m platform_tests.global_services.data_generators.generate_apps --apps-file ../../../test_resources/$4
  python3 -m platform_tests.$1.data_generators.medical_data_service.generate_cases --cases-file ../../../test_resources/$2 --wsis-file ../../../test_resources/$3
  exit 0
else
  echo "wrong number of arguments"
  exit 1
fi
