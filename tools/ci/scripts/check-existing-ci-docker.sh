#!/bin/bash
if [[ "$(docker manifest inspect "$1" 2> /dev/null)" == "" ]]; then
  echo "$1 does not exist"
  exit 1
else
  exit 0
fi