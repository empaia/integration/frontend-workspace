# CI Script files 

TODO: document scripts!

compile folder with 
`npm run tsc --  -p ./tools/ci/scripts/tsconfig.tools.json`


watch & compile folder
`npm run tsc --  --watch -p ./tools/ci/scripts/tsconfig.tools.json`


run single output scripts from out folder with:
`node <./path/scriptname>`

 or watchmode: 
`nodemon <./path/scriptname>`


run ts directly with 
`ts-node <./path/scriptname>` 
or watchmode with 
`nodemon <./path/scriptname>`


CI pipeline should only use compiled js to avoid unnecessary dependency to ts-node.


# gitlab

frontend-workspace ProjectID: 28967218
ci-base-image RepositoryID: 3036445

get images tag 
http GET https://gitlab.com/api/v4/projects/28967218/registry/repositories/3026209/tags?per_page=100

get image tags other possibility: 

http GET "https://gitlab.com/api/v4/registry/repositories/3036445?tags=true&per_page=100" 

https://gitlab.com/empaia/integration/frontend-workspace/container_registry/3036445
