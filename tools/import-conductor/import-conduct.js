// running https://github.com/kreuzerk/import-conductor as script

const child_process = require('child_process');

console.log('RUNNING IMPORT CONDUCT SCRIPT')

var currentPath = process.cwd();
console.log(currentPath);

const basePath = 'node node_modules/import-conductor/index.js';
const conductPathArg = '-s apps/workbench-client/src/app/'
const modules = [
  'core',
  'shared',
  'router',
  'material',
  'menu',
  'cases',
  'slides',
  'annotations',
  'examinations',
  'apps',
  'jobs',
  'results',
];

const conductGlob =  '/**/*.ts'

const userLibPrefixes = '-p @core @shared @menu @tools @api @material @router @auth @helper @env @cases @slides @results @annotations @examinations @jobs @apps'

for (const module of modules) {
  child_process.execSync(
    `${basePath} ${conductPathArg}${module}${conductGlob} ${userLibPrefixes}`,{stdio:[0,1,2]});  
}
