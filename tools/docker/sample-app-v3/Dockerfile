FROM registry.gitlab.com/empaia/integration/frontend-workspace/ci-base-image:f95d1cc65069f1421bfbc7695988ff4b20ab6050c03ca2f041b05ca1be8e84c0 AS build

WORKDIR /usr/angular-build
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
COPY package*.json ./
COPY decorate-angular-cli.js .

# symlink from base image node_modules to project folder
RUN ln -s ${NPM_MODULES_FOLDER} ./node_modules

# build in production mode
COPY nx.json nx.json
COPY tsconfig.base.json tsconfig.base.json
COPY tools/ tools/
COPY libs/ libs/
COPY apps/ apps/
RUN npm run nx build sample-app-v3

# inject preload for fonts
RUN npm run preload-fonts:sa-v3

# run stage
FROM nginx:alpine AS run

# Remove default nginx index page
RUN rm -rf /usr/share/nginx/html/*

# copy configuration
COPY tools/docker/nginx.conf /etc/nginx/nginx.conf
COPY tools/docker/run.sh run.sh

# copy compiled angular sources from build stage
COPY --from=build /usr/angular-build/dist/apps/sample-app-v3 /usr/share/nginx/html

ENTRYPOINT ["sh", "run.sh"]
