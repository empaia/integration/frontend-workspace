# Vendor App Communication Interface

# 0.1.33 (2024-07-16)

### Refactor

* upgraded to nx 18

# 0.1.32 (2024-01-11)

### Refactor

* updated to nx 16

# 0.1.16 (2022-11-07)

### Refactor

* Removed Angular fxLayout

# 0.1.13 (2022-10-13)

### Refactor

* Updated dependencies

# 0.1.11

### Refactor

* updated to nx version 14
