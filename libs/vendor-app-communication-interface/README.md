# vendor-app-communication-interface

The Vendor App Communication Interface (VACI) is a library that provides a set of functions to let your AppUI communicate with the [The Workbench Client 2.0](https://developer.empaia.org/app_developer_docs/v2/#/tutorial_frontend/initialization). 

For further reference see the [Empaia Developer Docs](https://developer.empaia.org).
## Usage

### TypeScript

```typescript
import { 
  addScopeListener,
  addTokenListener,
  addWbsUrlListener,
  requestNewToken,
  Scope,
  Token,
  WbsUrl
} from 'vendor-app-communication-interface';

function mySampleFunction(): void {
  // give the scope listener a callback function with
  // a scope object as parameter
  addScopeListener((scope: Scope) => {
    const scopeId = scope.id;
    // do some thing with the received scope id
  });

  // give the token listener a callback function with
  // a token object as parameter
  addTokenListener((token: Token) => {
    const tokenValue = token.value;
    // the value of token contains the access token
    // use it in all further request headers
  });

  // give the wbsUrl listener a callback function with
  // a wbsUrl object as parameter
  addWbsUrlListener((wbsUrl: WbsUrl) => {
    const baseUrl = wbsUrl.url;
    // the url property contains the base url for all backend routes
    // concatenate it with a concrete path, e.g: 
    // baseUrl + '/v2/scopes/${scopeId}/slides // to get all slides
  });
}

function onTokenExpiration(): void {
  // call this function every time your access token has expired
  // after this you will receive a new token
  requestNewToken();
}
```

### JavaScript

```js
function mySampleFunction() {
  VendorAppCommunicationInterface.addScopeListener(function(scope) {
    // the scope object contains the attribute id which is the scopeId
    const scopeId = scope.id;
    // store the scope id in your app
  });

  VendorAppCommunicationInterface.addTokenListener(function(token) {
    // the token object contains the attribute value which is the access token
    const accessToken = token.value;
    // store the access token in your app
  });

  VendorAppCommunicationInterface.addWbsUrlListener(function(wbsUrl) {
    // the wbsUrl object contains the attribute url which is the base url to the Workbench Service 2.0
    const url = wbsUrl.url;
    // store the Workbench Service 2.0 URL in your app for the base URL for further api calls
  });
}
