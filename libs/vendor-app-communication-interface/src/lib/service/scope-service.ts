import { Scope, SCOPE_READY_TYPE, ScopeReady } from '../models/scope';
import { scopeListener } from '../listener/scope-listener';

function addScopeListener(callback: (data: Scope) => void): number {
  const index = scopeListener.setCallback(callback);

  if (scopeListener.getCallbackCount() <= 1) {
    sendScope();
  }

  return index;
}

function sendScope(): void {
  if (window.top) {
    const origin = scopeListener.origin ? scopeListener.origin : '*';
    const request: ScopeReady = {
      type: SCOPE_READY_TYPE
    };
    // use '*' because the origin is not known yet
    // this function is just called once
    window.top.postMessage(request, origin);
  } else {
    console.error('App is not embedded');
  }
}

function removeScopeListener(index: number): boolean {
  return scopeListener.removeCallbackByIndex(index);
}

export {
  addScopeListener,
  removeScopeListener,
};
