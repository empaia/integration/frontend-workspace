export class EventEmitter<T> {
  private lastData?: T;

  private eventCallbacks: Array<(data: T) => void | undefined> = [];

  public on(listener: (data: T) => void): number {
    if (this.lastData) {
      listener(this.lastData);
    }
    this.eventCallbacks.push(listener);
    return this.eventCallbacks.indexOf(listener);
  }

  public removeListener(listener: (data: T) => void): void {
    this.eventCallbacks = this.eventCallbacks.filter(l => l !== listener);
  }

  public removeListenerOnIndex(index: number): boolean {
    if (index > 0 && index < this.eventCallbacks.length) {
      this.eventCallbacks.splice(index, 1);
      return true;
    } else {
      return false;
    }
  }

  public emit(data: T): void {
    this.lastData = data;
    this.eventCallbacks.forEach(callback => callback(data));
  }

  public getCallbackCount(): number {
    return this.eventCallbacks.length;
  }
}
