/* eslint-disable @typescript-eslint/no-explicit-any */
import { WbsUrl, WBS_URL_TYPE } from '../models/wbs-url';
import { Listener } from './listener';

export class WbsUrlListener extends Listener<WbsUrl> {
  constructor() {
    super();
  }

  protected isTypeOf(data: any): data is WbsUrl {
    return 'type' in data && data['type'] === WBS_URL_TYPE;
  }
}

export const wbsUrlListener = new WbsUrlListener();
