/* eslint-disable @typescript-eslint/no-explicit-any */
import { Token, TOKEN_TYPE } from '../models/token';
import { Listener } from './listener';

export class TokenListener extends Listener<Token> {
  constructor() {
    super();
  }

  protected isTypeOf(data: any): data is Token {
    return 'type' in data && data['type'] === TOKEN_TYPE;
  }
}

export const tokenListener = new TokenListener();
