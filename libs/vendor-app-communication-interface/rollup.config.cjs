const terser = require('@rollup/plugin-terser');

// convert cjs to umd https://github.com/nrwl/nx/issues/10797
module.exports = (config) => {
  // convert cjs build to UMD
  for (const output of config.output) {
    if (output.format.includes('cjs')) {
      output.entryFileNames = output.entryFileNames.replace('cjs', 'umd');
      output.format = 'umd';
      config.external = [];
    }
  }

  return {
    ...config,
    plugins: [...config.plugins, terser()],
  };
}
