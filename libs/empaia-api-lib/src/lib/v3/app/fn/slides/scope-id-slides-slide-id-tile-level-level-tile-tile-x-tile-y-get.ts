/* tslint:disable */
/* eslint-disable */
import { HttpClient, HttpContext, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { StrictHttpResponse } from '../../strict-http-response';
import { RequestBuilder } from '../../request-builder';


export interface ScopeIdSlidesSlideIdTileLevelLevelTileTileXTileYGet$Params {
  scope_id: string;

/**
 * Slide ID
 */
  slide_id: string;

/**
 * Zoom level
 */
  level: number;

/**
 * Tile number horizontally
 */
  tile_x: number;

/**
 * Tile number vertically
 */
  tile_y: number;

/**
 * Image format (e.g. bmp, gif, jpeg, png, tiff). For raw image data choose tiff.
 */
  image_format?: string;

/**
 * Image quality (Only for specific formats. For Jpeg files compression is always lossy.         For tiff files 'deflate' compression is used by default. Set to 0 to compress lossy with 'jpeg')
 */
  image_quality?: number;

/**
 * List of requested image channels. By default all channels are returned.
 */
  image_channels?: Array<number>;

/**
 * Padding color as 24-bit hex-string
 */
  padding_color?: string;

/**
 * Z coordinate / stack
 */
  z?: number;
}

export function scopeIdSlidesSlideIdTileLevelLevelTileTileXTileYGet(http: HttpClient, rootUrl: string, params: ScopeIdSlidesSlideIdTileLevelLevelTileTileXTileYGet$Params, context?: HttpContext): Observable<StrictHttpResponse<Blob>> {
  const rb = new RequestBuilder(rootUrl, scopeIdSlidesSlideIdTileLevelLevelTileTileXTileYGet.PATH, 'get');
  if (params) {
    rb.path('scope_id', params.scope_id, {});
    rb.path('slide_id', params.slide_id, {});
    rb.path('level', params.level, {});
    rb.path('tile_x', params.tile_x, {});
    rb.path('tile_y', params.tile_y, {});
    rb.query('image_format', params.image_format, {});
    rb.query('image_quality', params.image_quality, {});
    rb.query('image_channels', params.image_channels, {});
    rb.query('padding_color', params.padding_color, {});
    rb.query('z', params.z, {});
  }

  return http.request(
    rb.build({ responseType: 'blob', accept: 'image/*', context })
  ).pipe(
    filter((r: any): r is HttpResponse<any> => r instanceof HttpResponse),
    map((r: HttpResponse<any>) => {
      return r as StrictHttpResponse<Blob>;
    })
  );
}

scopeIdSlidesSlideIdTileLevelLevelTileTileXTileYGet.PATH = '/{scope_id}/slides/{slide_id}/tile/level/{level}/tile/{tile_x}/{tile_y}';
