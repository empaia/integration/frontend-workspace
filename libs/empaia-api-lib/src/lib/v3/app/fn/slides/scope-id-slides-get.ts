/* tslint:disable */
/* eslint-disable */
import { HttpClient, HttpContext, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { StrictHttpResponse } from '../../strict-http-response';
import { RequestBuilder } from '../../request-builder';

import { SlideList } from '../../models/slide-list';

export interface ScopeIdSlidesGet$Params {
  scope_id: string;
}

export function scopeIdSlidesGet(http: HttpClient, rootUrl: string, params: ScopeIdSlidesGet$Params, context?: HttpContext): Observable<StrictHttpResponse<SlideList>> {
  const rb = new RequestBuilder(rootUrl, scopeIdSlidesGet.PATH, 'get');
  if (params) {
    rb.path('scope_id', params.scope_id, {});
  }

  return http.request(
    rb.build({ responseType: 'json', accept: 'application/json', context })
  ).pipe(
    filter((r: any): r is HttpResponse<any> => r instanceof HttpResponse),
    map((r: HttpResponse<any>) => {
      return r as StrictHttpResponse<SlideList>;
    })
  );
}

scopeIdSlidesGet.PATH = '/{scope_id}/slides';
