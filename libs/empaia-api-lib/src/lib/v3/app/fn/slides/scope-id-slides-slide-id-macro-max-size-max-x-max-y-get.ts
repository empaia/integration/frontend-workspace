/* tslint:disable */
/* eslint-disable */
import { HttpClient, HttpContext, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { StrictHttpResponse } from '../../strict-http-response';
import { RequestBuilder } from '../../request-builder';


export interface ScopeIdSlidesSlideIdMacroMaxSizeMaxXMaxYGet$Params {
  scope_id: string;

/**
 * Slide ID
 */
  slide_id: string;

/**
 * Maximum width of image
 */
  max_x: number;

/**
 * Maximum height of image
 */
  max_y: number;

/**
 * Image format (e.g. bmp, gif, jpeg, png, tiff). For raw image data choose tiff.
 */
  image_format?: string;

/**
 * Image quality (Only for specific formats. For Jpeg files compression is always lossy.         For tiff files 'deflate' compression is used by default. Set to 0 to compress lossy with 'jpeg')
 */
  image_quality?: number;
}

export function scopeIdSlidesSlideIdMacroMaxSizeMaxXMaxYGet(http: HttpClient, rootUrl: string, params: ScopeIdSlidesSlideIdMacroMaxSizeMaxXMaxYGet$Params, context?: HttpContext): Observable<StrictHttpResponse<Blob>> {
  const rb = new RequestBuilder(rootUrl, scopeIdSlidesSlideIdMacroMaxSizeMaxXMaxYGet.PATH, 'get');
  if (params) {
    rb.path('scope_id', params.scope_id, {});
    rb.path('slide_id', params.slide_id, {});
    rb.path('max_x', params.max_x, {});
    rb.path('max_y', params.max_y, {});
    rb.query('image_format', params.image_format, {});
    rb.query('image_quality', params.image_quality, {});
  }

  return http.request(
    rb.build({ responseType: 'blob', accept: 'image/*', context })
  ).pipe(
    filter((r: any): r is HttpResponse<any> => r instanceof HttpResponse),
    map((r: HttpResponse<any>) => {
      return r as StrictHttpResponse<Blob>;
    })
  );
}

scopeIdSlidesSlideIdMacroMaxSizeMaxXMaxYGet.PATH = '/{scope_id}/slides/{slide_id}/macro/max_size/{max_x}/{max_y}';
