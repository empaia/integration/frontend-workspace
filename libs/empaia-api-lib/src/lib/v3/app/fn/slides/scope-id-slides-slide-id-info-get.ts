/* tslint:disable */
/* eslint-disable */
import { HttpClient, HttpContext, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { StrictHttpResponse } from '../../strict-http-response';
import { RequestBuilder } from '../../request-builder';

import { SlideInfo } from '../../models/slide-info';

export interface ScopeIdSlidesSlideIdInfoGet$Params {
  scope_id: string;

/**
 * Slide ID
 */
  slide_id: string;
}

export function scopeIdSlidesSlideIdInfoGet(http: HttpClient, rootUrl: string, params: ScopeIdSlidesSlideIdInfoGet$Params, context?: HttpContext): Observable<StrictHttpResponse<SlideInfo>> {
  const rb = new RequestBuilder(rootUrl, scopeIdSlidesSlideIdInfoGet.PATH, 'get');
  if (params) {
    rb.path('scope_id', params.scope_id, {});
    rb.path('slide_id', params.slide_id, {});
  }

  return http.request(
    rb.build({ responseType: 'json', accept: 'application/json', context })
  ).pipe(
    filter((r: any): r is HttpResponse<any> => r instanceof HttpResponse),
    map((r: HttpResponse<any>) => {
      return r as StrictHttpResponse<SlideInfo>;
    })
  );
}

scopeIdSlidesSlideIdInfoGet.PATH = '/{scope_id}/slides/{slide_id}/info';
