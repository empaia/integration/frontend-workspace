/* tslint:disable */
/* eslint-disable */
import { HttpClient, HttpContext, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { StrictHttpResponse } from '../../strict-http-response';
import { RequestBuilder } from '../../request-builder';


export interface ScopeIdSlidesSlideIdRegionLevelLevelStartStartXStartYSizeSizeXSizeYGet$Params {
  scope_id: string;

/**
 * Slide ID
 */
  slide_id: string;

/**
 * Zoom level
 */
  level: number;

/**
 * Upper left x coordinate
 */
  start_x: number;

/**
 * Upper left y coordinate
 */
  start_y: number;

/**
 * Width
 */
  size_x: number;

/**
 * Height
 */
  size_y: number;

/**
 * Image format (e.g. bmp, gif, jpeg, png, tiff). For raw image data choose tiff.
 */
  image_format?: string;

/**
 * Image quality (Only for specific formats. For Jpeg files compression is always lossy.         For tiff files 'deflate' compression is used by default. Set to 0 to compress lossy with 'jpeg')
 */
  image_quality?: number;

/**
 * List of requested image channels. By default all channels are returned.
 */
  image_channels?: Array<number>;

/**
 * Z coordinate / stack
 */
  z?: number;
}

export function scopeIdSlidesSlideIdRegionLevelLevelStartStartXStartYSizeSizeXSizeYGet(http: HttpClient, rootUrl: string, params: ScopeIdSlidesSlideIdRegionLevelLevelStartStartXStartYSizeSizeXSizeYGet$Params, context?: HttpContext): Observable<StrictHttpResponse<Blob>> {
  const rb = new RequestBuilder(rootUrl, scopeIdSlidesSlideIdRegionLevelLevelStartStartXStartYSizeSizeXSizeYGet.PATH, 'get');
  if (params) {
    rb.path('scope_id', params.scope_id, {});
    rb.path('slide_id', params.slide_id, {});
    rb.path('level', params.level, {});
    rb.path('start_x', params.start_x, {});
    rb.path('start_y', params.start_y, {});
    rb.path('size_x', params.size_x, {});
    rb.path('size_y', params.size_y, {});
    rb.query('image_format', params.image_format, {});
    rb.query('image_quality', params.image_quality, {});
    rb.query('image_channels', params.image_channels, {});
    rb.query('z', params.z, {});
  }

  return http.request(
    rb.build({ responseType: 'blob', accept: 'image/*', context })
  ).pipe(
    filter((r: any): r is HttpResponse<any> => r instanceof HttpResponse),
    map((r: HttpResponse<any>) => {
      return r as StrictHttpResponse<Blob>;
    })
  );
}

scopeIdSlidesSlideIdRegionLevelLevelStartStartXStartYSizeSizeXSizeYGet.PATH = '/{scope_id}/slides/{slide_id}/region/level/{level}/start/{start_x}/{start_y}/size/{size_x}/{size_y}';
