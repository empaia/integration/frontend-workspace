/* tslint:disable */
/* eslint-disable */
import { HttpClient, HttpContext, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { StrictHttpResponse } from '../../strict-http-response';
import { RequestBuilder } from '../../request-builder';

import { ExtendedScope } from '../../models/extended-scope';

export interface ScopeIdGet$Params {
  scope_id: string;
}

export function scopeIdGet(http: HttpClient, rootUrl: string, params: ScopeIdGet$Params, context?: HttpContext): Observable<StrictHttpResponse<ExtendedScope>> {
  const rb = new RequestBuilder(rootUrl, scopeIdGet.PATH, 'get');
  if (params) {
    rb.path('scope_id', params.scope_id, {});
  }

  return http.request(
    rb.build({ responseType: 'json', accept: 'application/json', context })
  ).pipe(
    filter((r: any): r is HttpResponse<any> => r instanceof HttpResponse),
    map((r: HttpResponse<any>) => {
      return r as StrictHttpResponse<ExtendedScope>;
    })
  );
}

scopeIdGet.PATH = '/{scope_id}';
