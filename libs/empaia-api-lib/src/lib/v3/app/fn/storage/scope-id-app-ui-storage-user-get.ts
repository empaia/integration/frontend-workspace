/* tslint:disable */
/* eslint-disable */
import { HttpClient, HttpContext, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { StrictHttpResponse } from '../../strict-http-response';
import { RequestBuilder } from '../../request-builder';

import { AppUiStorage } from '../../models/app-ui-storage';

export interface ScopeIdAppUiStorageUserGet$Params {
  scope_id: string;
}

export function scopeIdAppUiStorageUserGet(http: HttpClient, rootUrl: string, params: ScopeIdAppUiStorageUserGet$Params, context?: HttpContext): Observable<StrictHttpResponse<AppUiStorage>> {
  const rb = new RequestBuilder(rootUrl, scopeIdAppUiStorageUserGet.PATH, 'get');
  if (params) {
    rb.path('scope_id', params.scope_id, {});
  }

  return http.request(
    rb.build({ responseType: 'json', accept: 'application/json', context })
  ).pipe(
    filter((r: any): r is HttpResponse<any> => r instanceof HttpResponse),
    map((r: HttpResponse<any>) => {
      return r as StrictHttpResponse<AppUiStorage>;
    })
  );
}

scopeIdAppUiStorageUserGet.PATH = '/{scope_id}/app-ui-storage/user';
