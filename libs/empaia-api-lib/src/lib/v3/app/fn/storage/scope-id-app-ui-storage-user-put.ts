/* tslint:disable */
/* eslint-disable */
import { HttpClient, HttpContext, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { StrictHttpResponse } from '../../strict-http-response';
import { RequestBuilder } from '../../request-builder';

import { AppUiStorage } from '../../models/app-ui-storage';

export interface ScopeIdAppUiStorageUserPut$Params {
  scope_id: string;
      body: AppUiStorage
}

export function scopeIdAppUiStorageUserPut(http: HttpClient, rootUrl: string, params: ScopeIdAppUiStorageUserPut$Params, context?: HttpContext): Observable<StrictHttpResponse<AppUiStorage>> {
  const rb = new RequestBuilder(rootUrl, scopeIdAppUiStorageUserPut.PATH, 'put');
  if (params) {
    rb.path('scope_id', params.scope_id, {});
    rb.body(params.body, 'application/json');
  }

  return http.request(
    rb.build({ responseType: 'json', accept: 'application/json', context })
  ).pipe(
    filter((r: any): r is HttpResponse<any> => r instanceof HttpResponse),
    map((r: HttpResponse<any>) => {
      return r as StrictHttpResponse<AppUiStorage>;
    })
  );
}

scopeIdAppUiStorageUserPut.PATH = '/{scope_id}/app-ui-storage/user';
