/* tslint:disable */
/* eslint-disable */
import { HttpClient, HttpContext, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { StrictHttpResponse } from '../../strict-http-response';
import { RequestBuilder } from '../../request-builder';

import { Job } from '../../models/job';

export interface ScopeIdJobsJobIdRunPut$Params {
  scope_id: string;

/**
 * Id of job that should be executed
 */
  job_id: string;
}

export function scopeIdJobsJobIdRunPut(http: HttpClient, rootUrl: string, params: ScopeIdJobsJobIdRunPut$Params, context?: HttpContext): Observable<StrictHttpResponse<Job>> {
  const rb = new RequestBuilder(rootUrl, scopeIdJobsJobIdRunPut.PATH, 'put');
  if (params) {
    rb.path('scope_id', params.scope_id, {});
    rb.path('job_id', params.job_id, {});
  }

  return http.request(
    rb.build({ responseType: 'json', accept: 'application/json', context })
  ).pipe(
    filter((r: any): r is HttpResponse<any> => r instanceof HttpResponse),
    map((r: HttpResponse<any>) => {
      return r as StrictHttpResponse<Job>;
    })
  );
}

scopeIdJobsJobIdRunPut.PATH = '/{scope_id}/jobs/{job_id}/run';
