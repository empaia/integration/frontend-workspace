/* tslint:disable */
/* eslint-disable */
import { HttpClient, HttpContext, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { StrictHttpResponse } from '../../strict-http-response';
import { RequestBuilder } from '../../request-builder';

import { IdObject } from '../../models/id-object';
import { Job } from '../../models/job';

export interface ScopeIdJobsJobIdOutputsOutputKeyPut$Params {
  scope_id: string;

/**
 * Id of job that should be updated
 */
  job_id: string;

/**
 * Identifier for the output to update
 */
  output_key: string;
      body: IdObject
}

export function scopeIdJobsJobIdOutputsOutputKeyPut(http: HttpClient, rootUrl: string, params: ScopeIdJobsJobIdOutputsOutputKeyPut$Params, context?: HttpContext): Observable<StrictHttpResponse<Job>> {
  const rb = new RequestBuilder(rootUrl, scopeIdJobsJobIdOutputsOutputKeyPut.PATH, 'put');
  if (params) {
    rb.path('scope_id', params.scope_id, {});
    rb.path('job_id', params.job_id, {});
    rb.path('output_key', params.output_key, {});
    rb.body(params.body, 'application/json');
  }

  return http.request(
    rb.build({ responseType: 'json', accept: 'application/json', context })
  ).pipe(
    filter((r: any): r is HttpResponse<any> => r instanceof HttpResponse),
    map((r: HttpResponse<any>) => {
      return r as StrictHttpResponse<Job>;
    })
  );
}

scopeIdJobsJobIdOutputsOutputKeyPut.PATH = '/{scope_id}/jobs/{job_id}/outputs/{output_key}';
