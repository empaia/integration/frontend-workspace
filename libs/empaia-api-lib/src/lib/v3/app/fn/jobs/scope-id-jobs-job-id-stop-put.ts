/* tslint:disable */
/* eslint-disable */
import { HttpClient, HttpContext, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { StrictHttpResponse } from '../../strict-http-response';
import { RequestBuilder } from '../../request-builder';


export interface ScopeIdJobsJobIdStopPut$Params {
  scope_id: string;

/**
 * Id of job that should be executed
 */
  job_id: string;
}

export function scopeIdJobsJobIdStopPut(http: HttpClient, rootUrl: string, params: ScopeIdJobsJobIdStopPut$Params, context?: HttpContext): Observable<StrictHttpResponse<boolean>> {
  const rb = new RequestBuilder(rootUrl, scopeIdJobsJobIdStopPut.PATH, 'put');
  if (params) {
    rb.path('scope_id', params.scope_id, {});
    rb.path('job_id', params.job_id, {});
  }

  return http.request(
    rb.build({ responseType: 'json', accept: 'application/json', context })
  ).pipe(
    filter((r: any): r is HttpResponse<any> => r instanceof HttpResponse),
    map((r: HttpResponse<any>) => {
      return (r as HttpResponse<any>).clone({ body: String((r as HttpResponse<any>).body) === 'true' }) as StrictHttpResponse<boolean>;
    })
  );
}

scopeIdJobsJobIdStopPut.PATH = '/{scope_id}/jobs/{job_id}/stop';
