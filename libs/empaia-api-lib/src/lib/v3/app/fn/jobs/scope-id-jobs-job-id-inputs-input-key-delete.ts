/* tslint:disable */
/* eslint-disable */
import { HttpClient, HttpContext, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { StrictHttpResponse } from '../../strict-http-response';
import { RequestBuilder } from '../../request-builder';

import { Job } from '../../models/job';

export interface ScopeIdJobsJobIdInputsInputKeyDelete$Params {
  scope_id: string;

/**
 * Id of job that should be updated
 */
  job_id: string;

/**
 * Identifier for the input to remove
 */
  input_key: string;
}

export function scopeIdJobsJobIdInputsInputKeyDelete(http: HttpClient, rootUrl: string, params: ScopeIdJobsJobIdInputsInputKeyDelete$Params, context?: HttpContext): Observable<StrictHttpResponse<Job>> {
  const rb = new RequestBuilder(rootUrl, scopeIdJobsJobIdInputsInputKeyDelete.PATH, 'delete');
  if (params) {
    rb.path('scope_id', params.scope_id, {});
    rb.path('job_id', params.job_id, {});
    rb.path('input_key', params.input_key, {});
  }

  return http.request(
    rb.build({ responseType: 'json', accept: 'application/json', context })
  ).pipe(
    filter((r: any): r is HttpResponse<any> => r instanceof HttpResponse),
    map((r: HttpResponse<any>) => {
      return r as StrictHttpResponse<Job>;
    })
  );
}

scopeIdJobsJobIdInputsInputKeyDelete.PATH = '/{scope_id}/jobs/{job_id}/inputs/{input_key}';
