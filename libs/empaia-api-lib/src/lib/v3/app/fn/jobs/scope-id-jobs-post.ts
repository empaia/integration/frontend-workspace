/* tslint:disable */
/* eslint-disable */
import { HttpClient, HttpContext, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { StrictHttpResponse } from '../../strict-http-response';
import { RequestBuilder } from '../../request-builder';

import { Job } from '../../models/job';
import { PostJob } from '../../models/post-job';

export interface ScopeIdJobsPost$Params {
  scope_id: string;
      body: PostJob
}

export function scopeIdJobsPost(http: HttpClient, rootUrl: string, params: ScopeIdJobsPost$Params, context?: HttpContext): Observable<StrictHttpResponse<Job>> {
  const rb = new RequestBuilder(rootUrl, scopeIdJobsPost.PATH, 'post');
  if (params) {
    rb.path('scope_id', params.scope_id, {});
    rb.body(params.body, 'application/json');
  }

  return http.request(
    rb.build({ responseType: 'json', accept: 'application/json', context })
  ).pipe(
    filter((r: any): r is HttpResponse<any> => r instanceof HttpResponse),
    map((r: HttpResponse<any>) => {
      return r as StrictHttpResponse<Job>;
    })
  );
}

scopeIdJobsPost.PATH = '/{scope_id}/jobs';
