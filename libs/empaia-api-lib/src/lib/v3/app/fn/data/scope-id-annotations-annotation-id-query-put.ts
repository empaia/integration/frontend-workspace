/* tslint:disable */
/* eslint-disable */
import { HttpClient, HttpContext, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { StrictHttpResponse } from '../../strict-http-response';
import { RequestBuilder } from '../../request-builder';

import { AnnotationQuery } from '../../models/annotation-query';
import { AnnotationQueryPosition } from '../../models/annotation-query-position';

export interface ScopeIdAnnotationsAnnotationIdQueryPut$Params {

/**
 * Annotation ID
 */
  annotation_id: string;
  scope_id: string;
      body: AnnotationQuery
}

export function scopeIdAnnotationsAnnotationIdQueryPut(http: HttpClient, rootUrl: string, params: ScopeIdAnnotationsAnnotationIdQueryPut$Params, context?: HttpContext): Observable<StrictHttpResponse<AnnotationQueryPosition>> {
  const rb = new RequestBuilder(rootUrl, scopeIdAnnotationsAnnotationIdQueryPut.PATH, 'put');
  if (params) {
    rb.path('annotation_id', params.annotation_id, {});
    rb.path('scope_id', params.scope_id, {});
    rb.body(params.body, 'application/json');
  }

  return http.request(
    rb.build({ responseType: 'json', accept: 'application/json', context })
  ).pipe(
    filter((r: any): r is HttpResponse<any> => r instanceof HttpResponse),
    map((r: HttpResponse<any>) => {
      return r as StrictHttpResponse<AnnotationQueryPosition>;
    })
  );
}

scopeIdAnnotationsAnnotationIdQueryPut.PATH = '/{scope_id}/annotations/{annotation_id}/query';
