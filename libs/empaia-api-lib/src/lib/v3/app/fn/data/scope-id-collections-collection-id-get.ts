/* tslint:disable */
/* eslint-disable */
import { HttpClient, HttpContext, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { StrictHttpResponse } from '../../strict-http-response';
import { RequestBuilder } from '../../request-builder';

import { Collection } from '../../models/collection';

export interface ScopeIdCollectionsCollectionIdGet$Params {

/**
 * Collection ID
 */
  collection_id: string;
  scope_id: string;
  shallow?: boolean;
  with_leaf_ids?: boolean;
}

export function scopeIdCollectionsCollectionIdGet(http: HttpClient, rootUrl: string, params: ScopeIdCollectionsCollectionIdGet$Params, context?: HttpContext): Observable<StrictHttpResponse<Collection>> {
  const rb = new RequestBuilder(rootUrl, scopeIdCollectionsCollectionIdGet.PATH, 'get');
  if (params) {
    rb.path('collection_id', params.collection_id, {});
    rb.path('scope_id', params.scope_id, {});
    rb.query('shallow', params.shallow, {});
    rb.query('with_leaf_ids', params.with_leaf_ids, {});
  }

  return http.request(
    rb.build({ responseType: 'json', accept: 'application/json', context })
  ).pipe(
    filter((r: any): r is HttpResponse<any> => r instanceof HttpResponse),
    map((r: HttpResponse<any>) => {
      return r as StrictHttpResponse<Collection>;
    })
  );
}

scopeIdCollectionsCollectionIdGet.PATH = '/{scope_id}/collections/{collection_id}';
