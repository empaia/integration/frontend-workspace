/* tslint:disable */
/* eslint-disable */
import { HttpClient, HttpContext, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { StrictHttpResponse } from '../../strict-http-response';
import { RequestBuilder } from '../../request-builder';

import { AnnotationViewerList } from '../../models/annotation-viewer-list';
import { AnnotationViewerQuery } from '../../models/annotation-viewer-query';

export interface ScopeIdAnnotationsQueryViewerPut$Params {
  scope_id: string;
      body: AnnotationViewerQuery
}

export function scopeIdAnnotationsQueryViewerPut(http: HttpClient, rootUrl: string, params: ScopeIdAnnotationsQueryViewerPut$Params, context?: HttpContext): Observable<StrictHttpResponse<AnnotationViewerList>> {
  const rb = new RequestBuilder(rootUrl, scopeIdAnnotationsQueryViewerPut.PATH, 'put');
  if (params) {
    rb.path('scope_id', params.scope_id, {});
    rb.body(params.body, 'application/json');
  }

  return http.request(
    rb.build({ responseType: 'json', accept: 'application/json', context })
  ).pipe(
    filter((r: any): r is HttpResponse<any> => r instanceof HttpResponse),
    map((r: HttpResponse<any>) => {
      return r as StrictHttpResponse<AnnotationViewerList>;
    })
  );
}

scopeIdAnnotationsQueryViewerPut.PATH = '/{scope_id}/annotations/query/viewer';
