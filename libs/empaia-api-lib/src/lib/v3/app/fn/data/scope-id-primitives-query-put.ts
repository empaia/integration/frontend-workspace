/* tslint:disable */
/* eslint-disable */
import { HttpClient, HttpContext, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { StrictHttpResponse } from '../../strict-http-response';
import { RequestBuilder } from '../../request-builder';

import { PrimitiveList } from '../../models/primitive-list';
import { PrimitiveQuery } from '../../models/primitive-query';

export interface ScopeIdPrimitivesQueryPut$Params {
  scope_id: string;
  skip?: number;
  limit?: number;
      body: PrimitiveQuery
}

export function scopeIdPrimitivesQueryPut(http: HttpClient, rootUrl: string, params: ScopeIdPrimitivesQueryPut$Params, context?: HttpContext): Observable<StrictHttpResponse<PrimitiveList>> {
  const rb = new RequestBuilder(rootUrl, scopeIdPrimitivesQueryPut.PATH, 'put');
  if (params) {
    rb.path('scope_id', params.scope_id, {});
    rb.query('skip', params.skip, {});
    rb.query('limit', params.limit, {});
    rb.body(params.body, 'application/json');
  }

  return http.request(
    rb.build({ responseType: 'json', accept: 'application/json', context })
  ).pipe(
    filter((r: any): r is HttpResponse<any> => r instanceof HttpResponse),
    map((r: HttpResponse<any>) => {
      return r as StrictHttpResponse<PrimitiveList>;
    })
  );
}

scopeIdPrimitivesQueryPut.PATH = '/{scope_id}/primitives/query';
