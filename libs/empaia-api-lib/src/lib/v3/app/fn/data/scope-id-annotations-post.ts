/* tslint:disable */
/* eslint-disable */
import { HttpClient, HttpContext, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { StrictHttpResponse } from '../../strict-http-response';
import { RequestBuilder } from '../../request-builder';

import { AnnotationListResponse } from '../../models/annotation-list-response';
import { ArrowAnnotation } from '../../models/arrow-annotation';
import { CircleAnnotation } from '../../models/circle-annotation';
import { LineAnnotation } from '../../models/line-annotation';
import { PointAnnotation } from '../../models/point-annotation';
import { PolygonAnnotation } from '../../models/polygon-annotation';
import { PostArrowAnnotation } from '../../models/post-arrow-annotation';
import { PostArrowAnnotations } from '../../models/post-arrow-annotations';
import { PostCircleAnnotation } from '../../models/post-circle-annotation';
import { PostCircleAnnotations } from '../../models/post-circle-annotations';
import { PostLineAnnotation } from '../../models/post-line-annotation';
import { PostLineAnnotations } from '../../models/post-line-annotations';
import { PostPointAnnotation } from '../../models/post-point-annotation';
import { PostPointAnnotations } from '../../models/post-point-annotations';
import { PostPolygonAnnotation } from '../../models/post-polygon-annotation';
import { PostPolygonAnnotations } from '../../models/post-polygon-annotations';
import { PostRectangleAnnotation } from '../../models/post-rectangle-annotation';
import { PostRectangleAnnotations } from '../../models/post-rectangle-annotations';
import { RectangleAnnotation } from '../../models/rectangle-annotation';

export interface ScopeIdAnnotationsPost$Params {
  scope_id: string;
  is_roi?: boolean;
  external_ids?: boolean;
      body: (PostPointAnnotations | PostLineAnnotations | PostArrowAnnotations | PostCircleAnnotations | PostRectangleAnnotations | PostPolygonAnnotations | PostPointAnnotation | PostLineAnnotation | PostArrowAnnotation | PostCircleAnnotation | PostRectangleAnnotation | PostPolygonAnnotation)
}

export function scopeIdAnnotationsPost(http: HttpClient, rootUrl: string, params: ScopeIdAnnotationsPost$Params, context?: HttpContext): Observable<StrictHttpResponse<(PointAnnotation | LineAnnotation | ArrowAnnotation | CircleAnnotation | RectangleAnnotation | PolygonAnnotation | AnnotationListResponse)>> {
  const rb = new RequestBuilder(rootUrl, scopeIdAnnotationsPost.PATH, 'post');
  if (params) {
    rb.path('scope_id', params.scope_id, {});
    rb.query('is_roi', params.is_roi, {});
    rb.query('external_ids', params.external_ids, {});
    rb.body(params.body, 'application/json');
  }

  return http.request(
    rb.build({ responseType: 'json', accept: 'application/json', context })
  ).pipe(
    filter((r: any): r is HttpResponse<any> => r instanceof HttpResponse),
    map((r: HttpResponse<any>) => {
      return r as StrictHttpResponse<(PointAnnotation | LineAnnotation | ArrowAnnotation | CircleAnnotation | RectangleAnnotation | PolygonAnnotation | AnnotationListResponse)>;
    })
  );
}

scopeIdAnnotationsPost.PATH = '/{scope_id}/annotations';
