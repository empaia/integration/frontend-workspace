/* tslint:disable */
/* eslint-disable */
import { HttpClient, HttpContext, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { StrictHttpResponse } from '../../strict-http-response';
import { RequestBuilder } from '../../request-builder';

import { ClassesDict } from '../../models/classes-dict';

export interface ScopeIdClassNamespacesGet$Params {
  scope_id: string;
}

export function scopeIdClassNamespacesGet(http: HttpClient, rootUrl: string, params: ScopeIdClassNamespacesGet$Params, context?: HttpContext): Observable<StrictHttpResponse<{
[key: string]: ClassesDict;
}>> {
  const rb = new RequestBuilder(rootUrl, scopeIdClassNamespacesGet.PATH, 'get');
  if (params) {
    rb.path('scope_id', params.scope_id, {});
  }

  return http.request(
    rb.build({ responseType: 'json', accept: 'application/json', context })
  ).pipe(
    filter((r: any): r is HttpResponse<any> => r instanceof HttpResponse),
    map((r: HttpResponse<any>) => {
      return r as StrictHttpResponse<{
      [key: string]: ClassesDict;
      }>;
    })
  );
}

scopeIdClassNamespacesGet.PATH = '/{scope_id}/class-namespaces';
