/* tslint:disable */
/* eslint-disable */
import { HttpClient, HttpContext, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { StrictHttpResponse } from '../../strict-http-response';
import { RequestBuilder } from '../../request-builder';

import { ItemQuery } from '../../models/item-query';
import { UniqueReferences } from '../../models/unique-references';

export interface ScopeIdCollectionsCollectionIdItemsQueryUniqueReferencesPut$Params {

/**
 * Collection ID
 */
  collection_id: string;
  scope_id: string;
      body: ItemQuery
}

export function scopeIdCollectionsCollectionIdItemsQueryUniqueReferencesPut(http: HttpClient, rootUrl: string, params: ScopeIdCollectionsCollectionIdItemsQueryUniqueReferencesPut$Params, context?: HttpContext): Observable<StrictHttpResponse<UniqueReferences>> {
  const rb = new RequestBuilder(rootUrl, scopeIdCollectionsCollectionIdItemsQueryUniqueReferencesPut.PATH, 'put');
  if (params) {
    rb.path('collection_id', params.collection_id, {});
    rb.path('scope_id', params.scope_id, {});
    rb.body(params.body, 'application/json');
  }

  return http.request(
    rb.build({ responseType: 'json', accept: 'application/json', context })
  ).pipe(
    filter((r: any): r is HttpResponse<any> => r instanceof HttpResponse),
    map((r: HttpResponse<any>) => {
      return r as StrictHttpResponse<UniqueReferences>;
    })
  );
}

scopeIdCollectionsCollectionIdItemsQueryUniqueReferencesPut.PATH = '/{scope_id}/collections/{collection_id}/items/query/unique-references';
