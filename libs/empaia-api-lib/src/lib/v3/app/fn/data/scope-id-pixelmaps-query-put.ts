/* tslint:disable */
/* eslint-disable */
import { HttpClient, HttpContext, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { StrictHttpResponse } from '../../strict-http-response';
import { RequestBuilder } from '../../request-builder';

import { PixelmapList } from '../../models/pixelmap-list';
import { PixelmapQuery } from '../../models/pixelmap-query';

export interface ScopeIdPixelmapsQueryPut$Params {
  scope_id: string;
  skip?: number;
  limit?: number;
      body: PixelmapQuery
}

export function scopeIdPixelmapsQueryPut(http: HttpClient, rootUrl: string, params: ScopeIdPixelmapsQueryPut$Params, context?: HttpContext): Observable<StrictHttpResponse<PixelmapList>> {
  const rb = new RequestBuilder(rootUrl, scopeIdPixelmapsQueryPut.PATH, 'put');
  if (params) {
    rb.path('scope_id', params.scope_id, {});
    rb.query('skip', params.skip, {});
    rb.query('limit', params.limit, {});
    rb.body(params.body, 'application/json');
  }

  return http.request(
    rb.build({ responseType: 'json', accept: 'application/json', context })
  ).pipe(
    filter((r: any): r is HttpResponse<any> => r instanceof HttpResponse),
    map((r: HttpResponse<any>) => {
      return r as StrictHttpResponse<PixelmapList>;
    })
  );
}

scopeIdPixelmapsQueryPut.PATH = '/{scope_id}/pixelmaps/query';
