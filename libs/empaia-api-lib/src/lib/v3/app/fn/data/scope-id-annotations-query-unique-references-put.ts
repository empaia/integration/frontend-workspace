/* tslint:disable */
/* eslint-disable */
import { HttpClient, HttpContext, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { StrictHttpResponse } from '../../strict-http-response';
import { RequestBuilder } from '../../request-builder';

import { AnnotationQuery } from '../../models/annotation-query';
import { UniqueReferences } from '../../models/unique-references';

export interface ScopeIdAnnotationsQueryUniqueReferencesPut$Params {
  scope_id: string;
      body: AnnotationQuery
}

export function scopeIdAnnotationsQueryUniqueReferencesPut(http: HttpClient, rootUrl: string, params: ScopeIdAnnotationsQueryUniqueReferencesPut$Params, context?: HttpContext): Observable<StrictHttpResponse<UniqueReferences>> {
  const rb = new RequestBuilder(rootUrl, scopeIdAnnotationsQueryUniqueReferencesPut.PATH, 'put');
  if (params) {
    rb.path('scope_id', params.scope_id, {});
    rb.body(params.body, 'application/json');
  }

  return http.request(
    rb.build({ responseType: 'json', accept: 'application/json', context })
  ).pipe(
    filter((r: any): r is HttpResponse<any> => r instanceof HttpResponse),
    map((r: HttpResponse<any>) => {
      return r as StrictHttpResponse<UniqueReferences>;
    })
  );
}

scopeIdAnnotationsQueryUniqueReferencesPut.PATH = '/{scope_id}/annotations/query/unique-references';
