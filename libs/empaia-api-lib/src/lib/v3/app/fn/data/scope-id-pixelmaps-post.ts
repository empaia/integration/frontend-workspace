/* tslint:disable */
/* eslint-disable */
import { HttpClient, HttpContext, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { StrictHttpResponse } from '../../strict-http-response';
import { RequestBuilder } from '../../request-builder';

import { ContinuousPixelmap } from '../../models/continuous-pixelmap';
import { DiscretePixelmap } from '../../models/discrete-pixelmap';
import { NominalPixelmap } from '../../models/nominal-pixelmap';
import { PixelmapList } from '../../models/pixelmap-list';
import { PostContinuousPixelmap } from '../../models/post-continuous-pixelmap';
import { PostContinuousPixelmaps } from '../../models/post-continuous-pixelmaps';
import { PostDiscretePixelmap } from '../../models/post-discrete-pixelmap';
import { PostDiscretePixelmaps } from '../../models/post-discrete-pixelmaps';
import { PostNominalPixelmap } from '../../models/post-nominal-pixelmap';
import { PostNominalPixelmaps } from '../../models/post-nominal-pixelmaps';

export interface ScopeIdPixelmapsPost$Params {
  scope_id: string;
      body: (PostContinuousPixelmaps | PostDiscretePixelmaps | PostNominalPixelmaps | PostContinuousPixelmap | PostDiscretePixelmap | PostNominalPixelmap)
}

export function scopeIdPixelmapsPost(http: HttpClient, rootUrl: string, params: ScopeIdPixelmapsPost$Params, context?: HttpContext): Observable<StrictHttpResponse<(ContinuousPixelmap | DiscretePixelmap | NominalPixelmap | PixelmapList)>> {
  const rb = new RequestBuilder(rootUrl, scopeIdPixelmapsPost.PATH, 'post');
  if (params) {
    rb.path('scope_id', params.scope_id, {});
    rb.body(params.body, 'application/json');
  }

  return http.request(
    rb.build({ responseType: 'json', accept: 'application/json', context })
  ).pipe(
    filter((r: any): r is HttpResponse<any> => r instanceof HttpResponse),
    map((r: HttpResponse<any>) => {
      return r as StrictHttpResponse<(ContinuousPixelmap | DiscretePixelmap | NominalPixelmap | PixelmapList)>;
    })
  );
}

scopeIdPixelmapsPost.PATH = '/{scope_id}/pixelmaps';
