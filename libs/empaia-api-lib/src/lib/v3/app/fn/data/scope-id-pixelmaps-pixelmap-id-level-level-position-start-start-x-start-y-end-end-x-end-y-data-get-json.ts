/* tslint:disable */
/* eslint-disable */
import { HttpClient, HttpContext, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { StrictHttpResponse } from '../../strict-http-response';
import { RequestBuilder } from '../../request-builder';


export interface ScopeIdPixelmapsPixelmapIdLevelLevelPositionStartStartXStartYEndEndXEndYDataGet$Json$Params {
  scope_id: string;

/**
 * Pixelmap ID
 */
  pixelmap_id: string;

/**
 * Pyramid level of region
 */
  level: number;

/**
 * Start position in x dimension
 */
  start_x: number;

/**
 * Start position in y dimension
 */
  start_y: number;

/**
 * End position in x dimension
 */
  end_x: number;

/**
 * End position in y dimension
 */
  end_y: number;
}

export function scopeIdPixelmapsPixelmapIdLevelLevelPositionStartStartXStartYEndEndXEndYDataGet$Json(http: HttpClient, rootUrl: string, params: ScopeIdPixelmapsPixelmapIdLevelLevelPositionStartStartXStartYEndEndXEndYDataGet$Json$Params, context?: HttpContext): Observable<StrictHttpResponse<any>> {
  const rb = new RequestBuilder(rootUrl, scopeIdPixelmapsPixelmapIdLevelLevelPositionStartStartXStartYEndEndXEndYDataGet$Json.PATH, 'get');
  if (params) {
    rb.path('scope_id', params.scope_id, {});
    rb.path('pixelmap_id', params.pixelmap_id, {});
    rb.path('level', params.level, {});
    rb.path('start_x', params.start_x, {});
    rb.path('start_y', params.start_y, {});
    rb.path('end_x', params.end_x, {});
    rb.path('end_y', params.end_y, {});
  }

  return http.request(
    rb.build({ responseType: 'json', accept: 'application/json', context })
  ).pipe(
    filter((r: any): r is HttpResponse<any> => r instanceof HttpResponse),
    map((r: HttpResponse<any>) => {
      return r as StrictHttpResponse<any>;
    })
  );
}

scopeIdPixelmapsPixelmapIdLevelLevelPositionStartStartXStartYEndEndXEndYDataGet$Json.PATH = '/{scope_id}/pixelmaps/{pixelmap_id}/level/{level}/position/start/{start_x}/{start_y}/end/{end_x}/{end_y}/data';
