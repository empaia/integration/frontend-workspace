/* tslint:disable */
/* eslint-disable */
import { HttpClient, HttpContext, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { StrictHttpResponse } from '../../strict-http-response';
import { RequestBuilder } from '../../request-builder';

import { BoolPrimitive } from '../../models/bool-primitive';
import { FloatPrimitive } from '../../models/float-primitive';
import { IntegerPrimitive } from '../../models/integer-primitive';
import { PostBoolPrimitive } from '../../models/post-bool-primitive';
import { PostBoolPrimitives } from '../../models/post-bool-primitives';
import { PostFloatPrimitive } from '../../models/post-float-primitive';
import { PostFloatPrimitives } from '../../models/post-float-primitives';
import { PostIntegerPrimitive } from '../../models/post-integer-primitive';
import { PostIntegerPrimitives } from '../../models/post-integer-primitives';
import { PostStringPrimitive } from '../../models/post-string-primitive';
import { PostStringPrimitives } from '../../models/post-string-primitives';
import { PrimitiveList } from '../../models/primitive-list';
import { StringPrimitive } from '../../models/string-primitive';

export interface ScopeIdPrimitivesPost$Params {
  scope_id: string;
      body: (PostIntegerPrimitives | PostFloatPrimitives | PostBoolPrimitives | PostStringPrimitives | PostIntegerPrimitive | PostFloatPrimitive | PostBoolPrimitive | PostStringPrimitive)
}

export function scopeIdPrimitivesPost(http: HttpClient, rootUrl: string, params: ScopeIdPrimitivesPost$Params, context?: HttpContext): Observable<StrictHttpResponse<(IntegerPrimitive | FloatPrimitive | BoolPrimitive | StringPrimitive | PrimitiveList)>> {
  const rb = new RequestBuilder(rootUrl, scopeIdPrimitivesPost.PATH, 'post');
  if (params) {
    rb.path('scope_id', params.scope_id, {});
    rb.body(params.body, 'application/json');
  }

  return http.request(
    rb.build({ responseType: 'json', accept: 'application/json', context })
  ).pipe(
    filter((r: any): r is HttpResponse<any> => r instanceof HttpResponse),
    map((r: HttpResponse<any>) => {
      return r as StrictHttpResponse<(IntegerPrimitive | FloatPrimitive | BoolPrimitive | StringPrimitive | PrimitiveList)>;
    })
  );
}

scopeIdPrimitivesPost.PATH = '/{scope_id}/primitives';
