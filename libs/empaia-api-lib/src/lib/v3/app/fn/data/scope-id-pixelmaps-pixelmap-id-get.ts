/* tslint:disable */
/* eslint-disable */
import { HttpClient, HttpContext, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { StrictHttpResponse } from '../../strict-http-response';
import { RequestBuilder } from '../../request-builder';

import { ContinuousPixelmap } from '../../models/continuous-pixelmap';
import { DiscretePixelmap } from '../../models/discrete-pixelmap';
import { NominalPixelmap } from '../../models/nominal-pixelmap';

export interface ScopeIdPixelmapsPixelmapIdGet$Params {

/**
 * Pixelmap ID
 */
  pixelmap_id: string;
  scope_id: string;
}

export function scopeIdPixelmapsPixelmapIdGet(http: HttpClient, rootUrl: string, params: ScopeIdPixelmapsPixelmapIdGet$Params, context?: HttpContext): Observable<StrictHttpResponse<(ContinuousPixelmap | DiscretePixelmap | NominalPixelmap)>> {
  const rb = new RequestBuilder(rootUrl, scopeIdPixelmapsPixelmapIdGet.PATH, 'get');
  if (params) {
    rb.path('pixelmap_id', params.pixelmap_id, {});
    rb.path('scope_id', params.scope_id, {});
  }

  return http.request(
    rb.build({ responseType: 'json', accept: 'application/json', context })
  ).pipe(
    filter((r: any): r is HttpResponse<any> => r instanceof HttpResponse),
    map((r: HttpResponse<any>) => {
      return r as StrictHttpResponse<(ContinuousPixelmap | DiscretePixelmap | NominalPixelmap)>;
    })
  );
}

scopeIdPixelmapsPixelmapIdGet.PATH = '/{scope_id}/pixelmaps/{pixelmap_id}';
