/* tslint:disable */
/* eslint-disable */
import { HttpClient, HttpContext, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { StrictHttpResponse } from '../../strict-http-response';
import { RequestBuilder } from '../../request-builder';

import { ArrowAnnotation } from '../../models/arrow-annotation';
import { CircleAnnotation } from '../../models/circle-annotation';
import { LineAnnotation } from '../../models/line-annotation';
import { PointAnnotation } from '../../models/point-annotation';
import { PolygonAnnotation } from '../../models/polygon-annotation';
import { RectangleAnnotation } from '../../models/rectangle-annotation';

export interface ScopeIdAnnotationsAnnotationIdGet$Params {

/**
 * Annotation ID
 */
  annotation_id: string;
  scope_id: string;
  with_classes?: boolean;
}

export function scopeIdAnnotationsAnnotationIdGet(http: HttpClient, rootUrl: string, params: ScopeIdAnnotationsAnnotationIdGet$Params, context?: HttpContext): Observable<StrictHttpResponse<(PointAnnotation | LineAnnotation | ArrowAnnotation | CircleAnnotation | RectangleAnnotation | PolygonAnnotation)>> {
  const rb = new RequestBuilder(rootUrl, scopeIdAnnotationsAnnotationIdGet.PATH, 'get');
  if (params) {
    rb.path('annotation_id', params.annotation_id, {});
    rb.path('scope_id', params.scope_id, {});
    rb.query('with_classes', params.with_classes, {});
  }

  return http.request(
    rb.build({ responseType: 'json', accept: 'application/json', context })
  ).pipe(
    filter((r: any): r is HttpResponse<any> => r instanceof HttpResponse),
    map((r: HttpResponse<any>) => {
      return r as StrictHttpResponse<(PointAnnotation | LineAnnotation | ArrowAnnotation | CircleAnnotation | RectangleAnnotation | PolygonAnnotation)>;
    })
  );
}

scopeIdAnnotationsAnnotationIdGet.PATH = '/{scope_id}/annotations/{annotation_id}';
