/* tslint:disable */
/* eslint-disable */
import { HttpClient, HttpContext, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { StrictHttpResponse } from '../../strict-http-response';
import { RequestBuilder } from '../../request-builder';

import { IdObject } from '../../models/id-object';

export interface ScopeIdCollectionsCollectionIdItemsItemIdDelete$Params {

/**
 * Collection ID
 */
  collection_id: string;

/**
 * Collection Item ID
 */
  item_id: string;
  scope_id: string;
}

export function scopeIdCollectionsCollectionIdItemsItemIdDelete(http: HttpClient, rootUrl: string, params: ScopeIdCollectionsCollectionIdItemsItemIdDelete$Params, context?: HttpContext): Observable<StrictHttpResponse<IdObject>> {
  const rb = new RequestBuilder(rootUrl, scopeIdCollectionsCollectionIdItemsItemIdDelete.PATH, 'delete');
  if (params) {
    rb.path('collection_id', params.collection_id, {});
    rb.path('item_id', params.item_id, {});
    rb.path('scope_id', params.scope_id, {});
  }

  return http.request(
    rb.build({ responseType: 'json', accept: 'application/json', context })
  ).pipe(
    filter((r: any): r is HttpResponse<any> => r instanceof HttpResponse),
    map((r: HttpResponse<any>) => {
      return r as StrictHttpResponse<IdObject>;
    })
  );
}

scopeIdCollectionsCollectionIdItemsItemIdDelete.PATH = '/{scope_id}/collections/{collection_id}/items/{item_id}';
