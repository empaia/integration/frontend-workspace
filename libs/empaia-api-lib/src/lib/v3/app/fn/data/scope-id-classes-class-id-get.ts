/* tslint:disable */
/* eslint-disable */
import { HttpClient, HttpContext, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { StrictHttpResponse } from '../../strict-http-response';
import { RequestBuilder } from '../../request-builder';

import { Class } from '../../models/class';

export interface ScopeIdClassesClassIdGet$Params {

/**
 * Class ID
 */
  class_id: string;
  scope_id: string;
}

export function scopeIdClassesClassIdGet(http: HttpClient, rootUrl: string, params: ScopeIdClassesClassIdGet$Params, context?: HttpContext): Observable<StrictHttpResponse<Class>> {
  const rb = new RequestBuilder(rootUrl, scopeIdClassesClassIdGet.PATH, 'get');
  if (params) {
    rb.path('class_id', params.class_id, {});
    rb.path('scope_id', params.scope_id, {});
  }

  return http.request(
    rb.build({ responseType: 'json', accept: 'application/json', context })
  ).pipe(
    filter((r: any): r is HttpResponse<any> => r instanceof HttpResponse),
    map((r: HttpResponse<any>) => {
      return r as StrictHttpResponse<Class>;
    })
  );
}

scopeIdClassesClassIdGet.PATH = '/{scope_id}/classes/{class_id}';
