/* tslint:disable */
/* eslint-disable */
import { HttpClient, HttpContext, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { StrictHttpResponse } from '../../strict-http-response';
import { RequestBuilder } from '../../request-builder';

import { ItemQuery } from '../../models/item-query';
import { ItemQueryList } from '../../models/item-query-list';

export interface ScopeIdCollectionsCollectionIdItemsQueryPut$Params {

/**
 * Collection ID
 */
  collection_id: string;
  scope_id: string;
  skip?: number;
  limit?: number;
      body: ItemQuery
}

export function scopeIdCollectionsCollectionIdItemsQueryPut(http: HttpClient, rootUrl: string, params: ScopeIdCollectionsCollectionIdItemsQueryPut$Params, context?: HttpContext): Observable<StrictHttpResponse<ItemQueryList>> {
  const rb = new RequestBuilder(rootUrl, scopeIdCollectionsCollectionIdItemsQueryPut.PATH, 'put');
  if (params) {
    rb.path('collection_id', params.collection_id, {});
    rb.path('scope_id', params.scope_id, {});
    rb.query('skip', params.skip, {});
    rb.query('limit', params.limit, {});
    rb.body(params.body, 'application/json');
  }

  return http.request(
    rb.build({ responseType: 'json', accept: 'application/json', context })
  ).pipe(
    filter((r: any): r is HttpResponse<any> => r instanceof HttpResponse),
    map((r: HttpResponse<any>) => {
      return r as StrictHttpResponse<ItemQueryList>;
    })
  );
}

scopeIdCollectionsCollectionIdItemsQueryPut.PATH = '/{scope_id}/collections/{collection_id}/items/query';
