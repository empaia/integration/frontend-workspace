/* tslint:disable */
/* eslint-disable */
import { HttpClient, HttpContext, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { StrictHttpResponse } from '../../strict-http-response';
import { RequestBuilder } from '../../request-builder';

import { AnnotationUniqueClassesQuery } from '../../models/annotation-unique-classes-query';
import { UniqueClassValues } from '../../models/unique-class-values';

export interface ScopeIdAnnotationsQueryUniqueClassValuesPut$Params {
  scope_id: string;
      body: AnnotationUniqueClassesQuery
}

export function scopeIdAnnotationsQueryUniqueClassValuesPut(http: HttpClient, rootUrl: string, params: ScopeIdAnnotationsQueryUniqueClassValuesPut$Params, context?: HttpContext): Observable<StrictHttpResponse<UniqueClassValues>> {
  const rb = new RequestBuilder(rootUrl, scopeIdAnnotationsQueryUniqueClassValuesPut.PATH, 'put');
  if (params) {
    rb.path('scope_id', params.scope_id, {});
    rb.body(params.body, 'application/json');
  }

  return http.request(
    rb.build({ responseType: 'json', accept: 'application/json', context })
  ).pipe(
    filter((r: any): r is HttpResponse<any> => r instanceof HttpResponse),
    map((r: HttpResponse<any>) => {
      return r as StrictHttpResponse<UniqueClassValues>;
    })
  );
}

scopeIdAnnotationsQueryUniqueClassValuesPut.PATH = '/{scope_id}/annotations/query/unique-class-values';
