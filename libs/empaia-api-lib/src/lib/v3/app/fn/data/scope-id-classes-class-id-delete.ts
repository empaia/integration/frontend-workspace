/* tslint:disable */
/* eslint-disable */
import { HttpClient, HttpContext, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { StrictHttpResponse } from '../../strict-http-response';
import { RequestBuilder } from '../../request-builder';

import { IdObject } from '../../models/id-object';

export interface ScopeIdClassesClassIdDelete$Params {

/**
 * Class ID
 */
  class_id: string;
  scope_id: string;
}

export function scopeIdClassesClassIdDelete(http: HttpClient, rootUrl: string, params: ScopeIdClassesClassIdDelete$Params, context?: HttpContext): Observable<StrictHttpResponse<IdObject>> {
  const rb = new RequestBuilder(rootUrl, scopeIdClassesClassIdDelete.PATH, 'delete');
  if (params) {
    rb.path('class_id', params.class_id, {});
    rb.path('scope_id', params.scope_id, {});
  }

  return http.request(
    rb.build({ responseType: 'json', accept: 'application/json', context })
  ).pipe(
    filter((r: any): r is HttpResponse<any> => r instanceof HttpResponse),
    map((r: HttpResponse<any>) => {
      return r as StrictHttpResponse<IdObject>;
    })
  );
}

scopeIdClassesClassIdDelete.PATH = '/{scope_id}/classes/{class_id}';
