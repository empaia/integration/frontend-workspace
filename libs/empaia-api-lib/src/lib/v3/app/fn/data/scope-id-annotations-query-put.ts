/* tslint:disable */
/* eslint-disable */
import { HttpClient, HttpContext, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { StrictHttpResponse } from '../../strict-http-response';
import { RequestBuilder } from '../../request-builder';

import { AnnotationList } from '../../models/annotation-list';
import { AnnotationQuery } from '../../models/annotation-query';

export interface ScopeIdAnnotationsQueryPut$Params {
  scope_id: string;
  with_classes?: boolean;
  with_low_npp_centroids?: boolean;
  skip?: number;
  limit?: number;
      body: AnnotationQuery
}

export function scopeIdAnnotationsQueryPut(http: HttpClient, rootUrl: string, params: ScopeIdAnnotationsQueryPut$Params, context?: HttpContext): Observable<StrictHttpResponse<AnnotationList>> {
  const rb = new RequestBuilder(rootUrl, scopeIdAnnotationsQueryPut.PATH, 'put');
  if (params) {
    rb.path('scope_id', params.scope_id, {});
    rb.query('with_classes', params.with_classes, {});
    rb.query('with_low_npp_centroids', params.with_low_npp_centroids, {});
    rb.query('skip', params.skip, {});
    rb.query('limit', params.limit, {});
    rb.body(params.body, 'application/json');
  }

  return http.request(
    rb.build({ responseType: 'json', accept: 'application/json', context })
  ).pipe(
    filter((r: any): r is HttpResponse<any> => r instanceof HttpResponse),
    map((r: HttpResponse<any>) => {
      return r as StrictHttpResponse<AnnotationList>;
    })
  );
}

scopeIdAnnotationsQueryPut.PATH = '/{scope_id}/annotations/query';
