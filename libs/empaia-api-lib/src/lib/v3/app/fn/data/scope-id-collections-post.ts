/* tslint:disable */
/* eslint-disable */
import { HttpClient, HttpContext, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { StrictHttpResponse } from '../../strict-http-response';
import { RequestBuilder } from '../../request-builder';

import { Collection } from '../../models/collection';
import { PostArrowCollection } from '../../models/post-arrow-collection';
import { PostBoolCollection } from '../../models/post-bool-collection';
import { PostCirceCollection } from '../../models/post-circe-collection';
import { PostClassCollection } from '../../models/post-class-collection';
import { PostContinuousPixelmapCollection } from '../../models/post-continuous-pixelmap-collection';
import { PostDiscretePixelmapCollection } from '../../models/post-discrete-pixelmap-collection';
import { PostFloatCollection } from '../../models/post-float-collection';
import { PostIdCollection } from '../../models/post-id-collection';
import { PostIntegerCollection } from '../../models/post-integer-collection';
import { PostLineCollection } from '../../models/post-line-collection';
import { PostNestedCollection } from '../../models/post-nested-collection';
import { PostNominalPixelmapCollection } from '../../models/post-nominal-pixelmap-collection';
import { PostPointCollection } from '../../models/post-point-collection';
import { PostPolygonCollection } from '../../models/post-polygon-collection';
import { PostRectangleCollection } from '../../models/post-rectangle-collection';
import { PostSlideCollection } from '../../models/post-slide-collection';
import { PostStringCollection } from '../../models/post-string-collection';

export interface ScopeIdCollectionsPost$Params {
  scope_id: string;
      body: (PostPointCollection | PostLineCollection | PostArrowCollection | PostCirceCollection | PostRectangleCollection | PostPolygonCollection | PostClassCollection | PostIntegerCollection | PostFloatCollection | PostBoolCollection | PostStringCollection | PostContinuousPixelmapCollection | PostDiscretePixelmapCollection | PostNominalPixelmapCollection | PostSlideCollection | PostIdCollection | PostNestedCollection)
}

export function scopeIdCollectionsPost(http: HttpClient, rootUrl: string, params: ScopeIdCollectionsPost$Params, context?: HttpContext): Observable<StrictHttpResponse<Collection>> {
  const rb = new RequestBuilder(rootUrl, scopeIdCollectionsPost.PATH, 'post');
  if (params) {
    rb.path('scope_id', params.scope_id, {});
    rb.body(params.body, 'application/json');
  }

  return http.request(
    rb.build({ responseType: 'json', accept: 'application/json', context })
  ).pipe(
    filter((r: any): r is HttpResponse<any> => r instanceof HttpResponse),
    map((r: HttpResponse<any>) => {
      return r as StrictHttpResponse<Collection>;
    })
  );
}

scopeIdCollectionsPost.PATH = '/{scope_id}/collections';
