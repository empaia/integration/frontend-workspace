/* tslint:disable */
/* eslint-disable */
import { HttpClient, HttpContext, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { StrictHttpResponse } from '../../strict-http-response';
import { RequestBuilder } from '../../request-builder';

import { BoolPrimitive } from '../../models/bool-primitive';
import { FloatPrimitive } from '../../models/float-primitive';
import { IntegerPrimitive } from '../../models/integer-primitive';
import { StringPrimitive } from '../../models/string-primitive';

export interface ScopeIdPrimitivesPrimitiveIdGet$Params {

/**
 * Primitive ID
 */
  primitive_id: string;
  scope_id: string;
}

export function scopeIdPrimitivesPrimitiveIdGet(http: HttpClient, rootUrl: string, params: ScopeIdPrimitivesPrimitiveIdGet$Params, context?: HttpContext): Observable<StrictHttpResponse<(IntegerPrimitive | FloatPrimitive | BoolPrimitive | StringPrimitive)>> {
  const rb = new RequestBuilder(rootUrl, scopeIdPrimitivesPrimitiveIdGet.PATH, 'get');
  if (params) {
    rb.path('primitive_id', params.primitive_id, {});
    rb.path('scope_id', params.scope_id, {});
  }

  return http.request(
    rb.build({ responseType: 'json', accept: 'application/json', context })
  ).pipe(
    filter((r: any): r is HttpResponse<any> => r instanceof HttpResponse),
    map((r: HttpResponse<any>) => {
      return r as StrictHttpResponse<(IntegerPrimitive | FloatPrimitive | BoolPrimitive | StringPrimitive)>;
    })
  );
}

scopeIdPrimitivesPrimitiveIdGet.PATH = '/{scope_id}/primitives/{primitive_id}';
