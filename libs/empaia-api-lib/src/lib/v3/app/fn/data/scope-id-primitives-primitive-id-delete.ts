/* tslint:disable */
/* eslint-disable */
import { HttpClient, HttpContext, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { StrictHttpResponse } from '../../strict-http-response';
import { RequestBuilder } from '../../request-builder';

import { IdObject } from '../../models/id-object';

export interface ScopeIdPrimitivesPrimitiveIdDelete$Params {

/**
 * Primitive ID
 */
  primitive_id: string;
  scope_id: string;
}

export function scopeIdPrimitivesPrimitiveIdDelete(http: HttpClient, rootUrl: string, params: ScopeIdPrimitivesPrimitiveIdDelete$Params, context?: HttpContext): Observable<StrictHttpResponse<IdObject>> {
  const rb = new RequestBuilder(rootUrl, scopeIdPrimitivesPrimitiveIdDelete.PATH, 'delete');
  if (params) {
    rb.path('primitive_id', params.primitive_id, {});
    rb.path('scope_id', params.scope_id, {});
  }

  return http.request(
    rb.build({ responseType: 'json', accept: 'application/json', context })
  ).pipe(
    filter((r: any): r is HttpResponse<any> => r instanceof HttpResponse),
    map((r: HttpResponse<any>) => {
      return r as StrictHttpResponse<IdObject>;
    })
  );
}

scopeIdPrimitivesPrimitiveIdDelete.PATH = '/{scope_id}/primitives/{primitive_id}';
