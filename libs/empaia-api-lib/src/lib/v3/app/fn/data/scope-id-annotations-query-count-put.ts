/* tslint:disable */
/* eslint-disable */
import { HttpClient, HttpContext, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { StrictHttpResponse } from '../../strict-http-response';
import { RequestBuilder } from '../../request-builder';

import { AnnotationCountResponse } from '../../models/annotation-count-response';
import { AnnotationQuery } from '../../models/annotation-query';

export interface ScopeIdAnnotationsQueryCountPut$Params {
  scope_id: string;
      body: AnnotationQuery
}

export function scopeIdAnnotationsQueryCountPut(http: HttpClient, rootUrl: string, params: ScopeIdAnnotationsQueryCountPut$Params, context?: HttpContext): Observable<StrictHttpResponse<AnnotationCountResponse>> {
  const rb = new RequestBuilder(rootUrl, scopeIdAnnotationsQueryCountPut.PATH, 'put');
  if (params) {
    rb.path('scope_id', params.scope_id, {});
    rb.body(params.body, 'application/json');
  }

  return http.request(
    rb.build({ responseType: 'json', accept: 'application/json', context })
  ).pipe(
    filter((r: any): r is HttpResponse<any> => r instanceof HttpResponse),
    map((r: HttpResponse<any>) => {
      return r as StrictHttpResponse<AnnotationCountResponse>;
    })
  );
}

scopeIdAnnotationsQueryCountPut.PATH = '/{scope_id}/annotations/query/count';
