/* tslint:disable */
/* eslint-disable */
import { HttpClient, HttpContext, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { StrictHttpResponse } from '../../strict-http-response';
import { RequestBuilder } from '../../request-builder';

import { Class } from '../../models/class';
import { ClassListResponse } from '../../models/class-list-response';
import { PostClass } from '../../models/post-class';
import { PostClassList } from '../../models/post-class-list';

export interface ScopeIdClassesPost$Params {
  scope_id: string;
      body: (PostClassList | PostClass)
}

export function scopeIdClassesPost(http: HttpClient, rootUrl: string, params: ScopeIdClassesPost$Params, context?: HttpContext): Observable<StrictHttpResponse<(Class | ClassListResponse)>> {
  const rb = new RequestBuilder(rootUrl, scopeIdClassesPost.PATH, 'post');
  if (params) {
    rb.path('scope_id', params.scope_id, {});
    rb.body(params.body, 'application/json');
  }

  return http.request(
    rb.build({ responseType: 'json', accept: 'application/json', context })
  ).pipe(
    filter((r: any): r is HttpResponse<any> => r instanceof HttpResponse),
    map((r: HttpResponse<any>) => {
      return r as StrictHttpResponse<(Class | ClassListResponse)>;
    })
  );
}

scopeIdClassesPost.PATH = '/{scope_id}/classes';
