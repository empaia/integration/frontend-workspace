/* tslint:disable */
/* eslint-disable */
import { HttpClient, HttpContext, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { StrictHttpResponse } from '../../strict-http-response';
import { RequestBuilder } from '../../request-builder';

import { PixelmapQuery } from '../../models/pixelmap-query';
import { UniqueReferences } from '../../models/unique-references';

export interface ScopeIdPixelmapsQueryUniqueReferencesPut$Params {
  scope_id: string;
      body: PixelmapQuery
}

export function scopeIdPixelmapsQueryUniqueReferencesPut(http: HttpClient, rootUrl: string, params: ScopeIdPixelmapsQueryUniqueReferencesPut$Params, context?: HttpContext): Observable<StrictHttpResponse<UniqueReferences>> {
  const rb = new RequestBuilder(rootUrl, scopeIdPixelmapsQueryUniqueReferencesPut.PATH, 'put');
  if (params) {
    rb.path('scope_id', params.scope_id, {});
    rb.body(params.body, 'application/json');
  }

  return http.request(
    rb.build({ responseType: 'json', accept: 'application/json', context })
  ).pipe(
    filter((r: any): r is HttpResponse<any> => r instanceof HttpResponse),
    map((r: HttpResponse<any>) => {
      return r as StrictHttpResponse<UniqueReferences>;
    })
  );
}

scopeIdPixelmapsQueryUniqueReferencesPut.PATH = '/{scope_id}/pixelmaps/query/unique-references';
