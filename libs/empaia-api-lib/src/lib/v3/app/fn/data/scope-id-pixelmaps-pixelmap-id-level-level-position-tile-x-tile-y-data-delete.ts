/* tslint:disable */
/* eslint-disable */
import { HttpClient, HttpContext, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { StrictHttpResponse } from '../../strict-http-response';
import { RequestBuilder } from '../../request-builder';


export interface ScopeIdPixelmapsPixelmapIdLevelLevelPositionTileXTileYDataDelete$Params {
  scope_id: string;

/**
 * Pixelmap ID
 */
  pixelmap_id: string;

/**
 * Zoom level
 */
  level: number;

/**
 * Tile number horizontally
 */
  tile_x: number;

/**
 * Tile number vertically
 */
  tile_y: number;
}

export function scopeIdPixelmapsPixelmapIdLevelLevelPositionTileXTileYDataDelete(http: HttpClient, rootUrl: string, params: ScopeIdPixelmapsPixelmapIdLevelLevelPositionTileXTileYDataDelete$Params, context?: HttpContext): Observable<StrictHttpResponse<void>> {
  const rb = new RequestBuilder(rootUrl, scopeIdPixelmapsPixelmapIdLevelLevelPositionTileXTileYDataDelete.PATH, 'delete');
  if (params) {
    rb.path('scope_id', params.scope_id, {});
    rb.path('pixelmap_id', params.pixelmap_id, {});
    rb.path('level', params.level, {});
    rb.path('tile_x', params.tile_x, {});
    rb.path('tile_y', params.tile_y, {});
  }

  return http.request(
    rb.build({ responseType: 'text', accept: '*/*', context })
  ).pipe(
    filter((r: any): r is HttpResponse<any> => r instanceof HttpResponse),
    map((r: HttpResponse<any>) => {
      return (r as HttpResponse<any>).clone({ body: undefined }) as StrictHttpResponse<void>;
    })
  );
}

scopeIdPixelmapsPixelmapIdLevelLevelPositionTileXTileYDataDelete.PATH = '/{scope_id}/pixelmaps/{pixelmap_id}/level/{level}/position/{tile_x}/{tile_y}/data';
