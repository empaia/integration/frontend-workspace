/* tslint:disable */
/* eslint-disable */
import { HttpClient, HttpContext, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { StrictHttpResponse } from '../../strict-http-response';
import { RequestBuilder } from '../../request-builder';

import { AnnotationListResponse } from '../../models/annotation-list-response';
import { ArrowAnnotation } from '../../models/arrow-annotation';
import { BoolPrimitive } from '../../models/bool-primitive';
import { CircleAnnotation } from '../../models/circle-annotation';
import { Class } from '../../models/class';
import { ClassListResponse } from '../../models/class-list-response';
import { Collection } from '../../models/collection';
import { CollectionList } from '../../models/collection-list';
import { ContinuousPixelmap } from '../../models/continuous-pixelmap';
import { DiscretePixelmap } from '../../models/discrete-pixelmap';
import { FloatPrimitive } from '../../models/float-primitive';
import { IdObject } from '../../models/id-object';
import { IntegerPrimitive } from '../../models/integer-primitive';
import { LineAnnotation } from '../../models/line-annotation';
import { Message } from '../../models/message';
import { NominalPixelmap } from '../../models/nominal-pixelmap';
import { PixelmapList } from '../../models/pixelmap-list';
import { PointAnnotation } from '../../models/point-annotation';
import { PolygonAnnotation } from '../../models/polygon-annotation';
import { PostArrowAnnotation } from '../../models/post-arrow-annotation';
import { PostArrowAnnotationsItems } from '../../models/post-arrow-annotations-items';
import { PostArrowCollection } from '../../models/post-arrow-collection';
import { PostBoolCollection } from '../../models/post-bool-collection';
import { PostBoolPrimitive } from '../../models/post-bool-primitive';
import { PostBoolPrimitivesItems } from '../../models/post-bool-primitives-items';
import { PostCirceCollection } from '../../models/post-circe-collection';
import { PostCircleAnnotation } from '../../models/post-circle-annotation';
import { PostCircleAnnotationsItems } from '../../models/post-circle-annotations-items';
import { PostClass } from '../../models/post-class';
import { PostClassCollection } from '../../models/post-class-collection';
import { PostClassesItems } from '../../models/post-classes-items';
import { PostCollections } from '../../models/post-collections';
import { PostContinuousPixelmap } from '../../models/post-continuous-pixelmap';
import { PostContinuousPixelmapCollection } from '../../models/post-continuous-pixelmap-collection';
import { PostContinuousPixelmapsItems } from '../../models/post-continuous-pixelmaps-items';
import { PostDiscretePixelmap } from '../../models/post-discrete-pixelmap';
import { PostDiscretePixelmapCollection } from '../../models/post-discrete-pixelmap-collection';
import { PostDiscretePixelmapsItems } from '../../models/post-discrete-pixelmaps-items';
import { PostFloatCollection } from '../../models/post-float-collection';
import { PostFloatPrimitive } from '../../models/post-float-primitive';
import { PostFloatPrimitivesItems } from '../../models/post-float-primitives-items';
import { PostIdCollection } from '../../models/post-id-collection';
import { PostIdObjects } from '../../models/post-id-objects';
import { PostIntegerCollection } from '../../models/post-integer-collection';
import { PostIntegerPrimitive } from '../../models/post-integer-primitive';
import { PostIntegerPrimitivesItems } from '../../models/post-integer-primitives-items';
import { PostLineAnnotation } from '../../models/post-line-annotation';
import { PostLineAnnotationsItems } from '../../models/post-line-annotations-items';
import { PostLineCollection } from '../../models/post-line-collection';
import { PostNestedCollection } from '../../models/post-nested-collection';
import { PostNominalPixelmap } from '../../models/post-nominal-pixelmap';
import { PostNominalPixelmapCollection } from '../../models/post-nominal-pixelmap-collection';
import { PostNominalPixelmapsItems } from '../../models/post-nominal-pixelmaps-items';
import { PostPointAnnotation } from '../../models/post-point-annotation';
import { PostPointAnnotationItems } from '../../models/post-point-annotation-items';
import { PostPointCollection } from '../../models/post-point-collection';
import { PostPolygonAnnotation } from '../../models/post-polygon-annotation';
import { PostPolygonAnnotationsItems } from '../../models/post-polygon-annotations-items';
import { PostPolygonCollection } from '../../models/post-polygon-collection';
import { PostRectangleAnnotation } from '../../models/post-rectangle-annotation';
import { PostRectangleAnnotationsItems } from '../../models/post-rectangle-annotations-items';
import { PostRectangleCollection } from '../../models/post-rectangle-collection';
import { PostSlideCollection } from '../../models/post-slide-collection';
import { PostSlideItems } from '../../models/post-slide-items';
import { PostStringCollection } from '../../models/post-string-collection';
import { PostStringPrimitive } from '../../models/post-string-primitive';
import { PostStringPrimitivesItems } from '../../models/post-string-primitives-items';
import { PrimitiveList } from '../../models/primitive-list';
import { RectangleAnnotation } from '../../models/rectangle-annotation';
import { SlideItem } from '../../models/slide-item';
import { SlideItemList } from '../../models/slide-item-list';
import { StringPrimitive } from '../../models/string-primitive';

export interface ScopeIdCollectionsCollectionIdItemsPost$Params {

/**
 * Collection ID
 */
  collection_id: string;
  scope_id: string;
      body: (PostPointAnnotationItems | PostLineAnnotationsItems | PostArrowAnnotationsItems | PostCircleAnnotationsItems | PostRectangleAnnotationsItems | PostPolygonAnnotationsItems | PostClassesItems | PostIntegerPrimitivesItems | PostFloatPrimitivesItems | PostBoolPrimitivesItems | PostStringPrimitivesItems | PostContinuousPixelmapsItems | PostDiscretePixelmapsItems | PostNominalPixelmapsItems | PostSlideItems | PostIdObjects | PostCollections | PostPointAnnotation | PostLineAnnotation | PostArrowAnnotation | PostCircleAnnotation | PostRectangleAnnotation | PostPolygonAnnotation | PostClass | PostIntegerPrimitive | PostFloatPrimitive | PostBoolPrimitive | PostStringPrimitive | PostContinuousPixelmap | PostDiscretePixelmap | PostNominalPixelmap | SlideItem | IdObject | PostPointCollection | PostLineCollection | PostArrowCollection | PostCirceCollection | PostRectangleCollection | PostPolygonCollection | PostClassCollection | PostIntegerCollection | PostFloatCollection | PostBoolCollection | PostStringCollection | PostContinuousPixelmapCollection | PostDiscretePixelmapCollection | PostNominalPixelmapCollection | PostSlideCollection | PostIdCollection | PostNestedCollection)
}

export function scopeIdCollectionsCollectionIdItemsPost(http: HttpClient, rootUrl: string, params: ScopeIdCollectionsCollectionIdItemsPost$Params, context?: HttpContext): Observable<StrictHttpResponse<(Message | PointAnnotation | LineAnnotation | ArrowAnnotation | CircleAnnotation | RectangleAnnotation | PolygonAnnotation | AnnotationListResponse | IntegerPrimitive | FloatPrimitive | BoolPrimitive | StringPrimitive | PrimitiveList | Class | ClassListResponse | ContinuousPixelmap | DiscretePixelmap | NominalPixelmap | PixelmapList | SlideItem | SlideItemList | Collection | CollectionList)>> {
  const rb = new RequestBuilder(rootUrl, scopeIdCollectionsCollectionIdItemsPost.PATH, 'post');
  if (params) {
    rb.path('collection_id', params.collection_id, {});
    rb.path('scope_id', params.scope_id, {});
    rb.body(params.body, 'application/json');
  }

  return http.request(
    rb.build({ responseType: 'json', accept: 'application/json', context })
  ).pipe(
    filter((r: any): r is HttpResponse<any> => r instanceof HttpResponse),
    map((r: HttpResponse<any>) => {
      return r as StrictHttpResponse<(Message | PointAnnotation | LineAnnotation | ArrowAnnotation | CircleAnnotation | RectangleAnnotation | PolygonAnnotation | AnnotationListResponse | IntegerPrimitive | FloatPrimitive | BoolPrimitive | StringPrimitive | PrimitiveList | Class | ClassListResponse | ContinuousPixelmap | DiscretePixelmap | NominalPixelmap | PixelmapList | SlideItem | SlideItemList | Collection | CollectionList)>;
    })
  );
}

scopeIdCollectionsCollectionIdItemsPost.PATH = '/{scope_id}/collections/{collection_id}/items';
