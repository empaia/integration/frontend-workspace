/* tslint:disable */
/* eslint-disable */
import { HttpClient, HttpContext, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { StrictHttpResponse } from '../../strict-http-response';
import { RequestBuilder } from '../../request-builder';

import { ClassList } from '../../models/class-list';
import { ClassQuery } from '../../models/class-query';

export interface ScopeIdClassesQueryPut$Params {
  scope_id: string;
  with_unique_class_values?: boolean;
  skip?: number;
  limit?: number;
      body: ClassQuery
}

export function scopeIdClassesQueryPut(http: HttpClient, rootUrl: string, params: ScopeIdClassesQueryPut$Params, context?: HttpContext): Observable<StrictHttpResponse<ClassList>> {
  const rb = new RequestBuilder(rootUrl, scopeIdClassesQueryPut.PATH, 'put');
  if (params) {
    rb.path('scope_id', params.scope_id, {});
    rb.query('with_unique_class_values', params.with_unique_class_values, {});
    rb.query('skip', params.skip, {});
    rb.query('limit', params.limit, {});
    rb.body(params.body, 'application/json');
  }

  return http.request(
    rb.build({ responseType: 'json', accept: 'application/json', context })
  ).pipe(
    filter((r: any): r is HttpResponse<any> => r instanceof HttpResponse),
    map((r: HttpResponse<any>) => {
      return r as StrictHttpResponse<ClassList>;
    })
  );
}

scopeIdClassesQueryPut.PATH = '/{scope_id}/classes/query';
