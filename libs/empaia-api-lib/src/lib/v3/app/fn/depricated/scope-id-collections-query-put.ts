/* tslint:disable */
/* eslint-disable */
import { HttpClient, HttpContext, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { StrictHttpResponse } from '../../strict-http-response';
import { RequestBuilder } from '../../request-builder';

import { CollectionList } from '../../models/collection-list';
import { CollectionQuery } from '../../models/collection-query';

export interface ScopeIdCollectionsQueryPut$Params {
  scope_id: string;
  skip?: number;
  limit?: number;
      body: CollectionQuery
}

export function scopeIdCollectionsQueryPut(http: HttpClient, rootUrl: string, params: ScopeIdCollectionsQueryPut$Params, context?: HttpContext): Observable<StrictHttpResponse<CollectionList>> {
  const rb = new RequestBuilder(rootUrl, scopeIdCollectionsQueryPut.PATH, 'put');
  if (params) {
    rb.path('scope_id', params.scope_id, {});
    rb.query('skip', params.skip, {});
    rb.query('limit', params.limit, {});
    rb.body(params.body, 'application/json');
  }

  return http.request(
    rb.build({ responseType: 'json', accept: 'application/json', context })
  ).pipe(
    filter((r: any): r is HttpResponse<any> => r instanceof HttpResponse),
    map((r: HttpResponse<any>) => {
      return r as StrictHttpResponse<CollectionList>;
    })
  );
}

scopeIdCollectionsQueryPut.PATH = '/{scope_id}/collections/query';
