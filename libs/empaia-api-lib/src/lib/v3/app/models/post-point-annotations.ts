/* tslint:disable */
/* eslint-disable */
import { PostPointAnnotation } from '../models/post-point-annotation';
export interface PostPointAnnotations {

  /**
   * List of point annotations
   */
  items: Array<PostPointAnnotation>;
}
