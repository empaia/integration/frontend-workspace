/* tslint:disable */
/* eslint-disable */
import { PostClass } from '../models/post-class';
export interface PostClassList {

  /**
   * List of classes
   */
  items: Array<PostClass>;
}
