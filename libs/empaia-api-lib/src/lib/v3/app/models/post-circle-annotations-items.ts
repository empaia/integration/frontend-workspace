/* tslint:disable */
/* eslint-disable */
import { PostCircleAnnotation } from '../models/post-circle-annotation';
export interface PostCircleAnnotationsItems {

  /**
   * List of circle annotations
   */
  items?: (Array<PostCircleAnnotation> | null);
}
