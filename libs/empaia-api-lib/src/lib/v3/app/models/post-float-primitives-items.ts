/* tslint:disable */
/* eslint-disable */
import { PostFloatPrimitive } from '../models/post-float-primitive';
export interface PostFloatPrimitivesItems {

  /**
   * List of float primitives
   */
  items?: (Array<PostFloatPrimitive> | null);
}
