/* tslint:disable */
/* eslint-disable */
import { PostNominalPixelmap } from '../models/post-nominal-pixelmap';
export interface PostNominalPixelmaps {

  /**
   * List of nominal pixelmaps
   */
  items: Array<PostNominalPixelmap>;
}
