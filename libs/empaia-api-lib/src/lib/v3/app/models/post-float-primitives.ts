/* tslint:disable */
/* eslint-disable */
import { PostFloatPrimitive } from '../models/post-float-primitive';
export interface PostFloatPrimitives {

  /**
   * List of float primitives
   */
  items: Array<PostFloatPrimitive>;
}
