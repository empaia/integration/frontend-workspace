/* tslint:disable */
/* eslint-disable */
import { PostArrowCollection } from '../models/post-arrow-collection';
import { PostBoolCollection } from '../models/post-bool-collection';
import { PostCirceCollection } from '../models/post-circe-collection';
import { PostClassCollection } from '../models/post-class-collection';
import { PostContinuousPixelmapCollection } from '../models/post-continuous-pixelmap-collection';
import { PostDiscretePixelmapCollection } from '../models/post-discrete-pixelmap-collection';
import { PostFloatCollection } from '../models/post-float-collection';
import { PostIdCollection } from '../models/post-id-collection';
import { PostIntegerCollection } from '../models/post-integer-collection';
import { PostLineCollection } from '../models/post-line-collection';
import { PostNestedCollection } from '../models/post-nested-collection';
import { PostNominalPixelmapCollection } from '../models/post-nominal-pixelmap-collection';
import { PostPointCollection } from '../models/post-point-collection';
import { PostPolygonCollection } from '../models/post-polygon-collection';
import { PostRectangleCollection } from '../models/post-rectangle-collection';
import { PostSlideCollection } from '../models/post-slide-collection';
import { PostStringCollection } from '../models/post-string-collection';
export interface PostCollections {

  /**
   * List of items
   */
  items?: (Array<(PostPointCollection | PostLineCollection | PostArrowCollection | PostCirceCollection | PostRectangleCollection | PostPolygonCollection | PostClassCollection | PostIntegerCollection | PostFloatCollection | PostBoolCollection | PostStringCollection | PostContinuousPixelmapCollection | PostDiscretePixelmapCollection | PostNominalPixelmapCollection | PostSlideCollection | PostIdCollection | PostNestedCollection)> | null);
}
