/* tslint:disable */
/* eslint-disable */
import { BoolPrimitive } from '../models/bool-primitive';
import { FloatPrimitive } from '../models/float-primitive';
import { IntegerPrimitive } from '../models/integer-primitive';
import { StringPrimitive } from '../models/string-primitive';
export interface PrimitiveList {

  /**
   * Count of all items
   */
  item_count: number;

  /**
   * List of items
   */
  items: Array<(IntegerPrimitive | FloatPrimitive | BoolPrimitive | StringPrimitive)>;
}
