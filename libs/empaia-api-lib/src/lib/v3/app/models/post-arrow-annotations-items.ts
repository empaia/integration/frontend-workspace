/* tslint:disable */
/* eslint-disable */
import { PostArrowAnnotation } from '../models/post-arrow-annotation';
export interface PostArrowAnnotationsItems {

  /**
   * List of arrow annotations
   */
  items?: (Array<PostArrowAnnotation> | null);
}
