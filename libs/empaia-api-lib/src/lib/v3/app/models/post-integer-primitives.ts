/* tslint:disable */
/* eslint-disable */
import { PostIntegerPrimitive } from '../models/post-integer-primitive';
export interface PostIntegerPrimitives {

  /**
   * List of integer primitives
   */
  items: Array<PostIntegerPrimitive>;
}
