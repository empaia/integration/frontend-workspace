/* tslint:disable */
/* eslint-disable */
import { PostPolygonAnnotation } from '../models/post-polygon-annotation';
export interface PostPolygonAnnotationsItems {

  /**
   * List of polygon annotations
   */
  items?: (Array<PostPolygonAnnotation> | null);
}
