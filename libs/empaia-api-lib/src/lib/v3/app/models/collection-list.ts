/* tslint:disable */
/* eslint-disable */
import { Collection } from '../models/collection';
export interface CollectionList {

  /**
   * Count of items.
   */
  item_count: number;

  /**
   * List of items.
   */
  items: Array<Collection>;
}
