/* tslint:disable */
/* eslint-disable */
import { PostRectangleAnnotation } from '../models/post-rectangle-annotation';
export interface PostRectangleAnnotations {

  /**
   * List of rectangle annotations
   */
  items: Array<PostRectangleAnnotation>;
}
