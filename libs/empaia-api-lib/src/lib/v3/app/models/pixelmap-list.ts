/* tslint:disable */
/* eslint-disable */
import { ContinuousPixelmap } from '../models/continuous-pixelmap';
import { DiscretePixelmap } from '../models/discrete-pixelmap';
import { NominalPixelmap } from '../models/nominal-pixelmap';
export interface PixelmapList {

  /**
   * Count of all items
   */
  item_count: number;

  /**
   * List of items
   */
  items: Array<(ContinuousPixelmap | DiscretePixelmap | NominalPixelmap)>;
}
