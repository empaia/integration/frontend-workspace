/* tslint:disable */
/* eslint-disable */
import { PostRectangleAnnotation } from '../models/post-rectangle-annotation';
export interface PostRectangleAnnotationsItems {

  /**
   * List of rectangle annotations
   */
  items?: (Array<PostRectangleAnnotation> | null);
}
