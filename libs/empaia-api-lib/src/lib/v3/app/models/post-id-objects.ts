/* tslint:disable */
/* eslint-disable */
import { IdObject } from '../models/id-object';
export interface PostIdObjects {

  /**
   * List of items
   */
  items: Array<IdObject>;
}
