/* tslint:disable */
/* eslint-disable */
import { PostBoolPrimitive } from '../models/post-bool-primitive';
export interface PostBoolPrimitivesItems {

  /**
   * List of bool primitives
   */
  items?: (Array<PostBoolPrimitive> | null);
}
