/* tslint:disable */
/* eslint-disable */
import { PostClass } from '../models/post-class';
export interface PostClassesItems {

  /**
   * List of classes
   */
  items?: (Array<PostClass> | null);
}
