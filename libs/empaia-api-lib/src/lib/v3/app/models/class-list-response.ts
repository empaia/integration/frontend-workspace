/* tslint:disable */
/* eslint-disable */
import { Class } from '../models/class';
export interface ClassListResponse {

  /**
   * Count of all items
   */
  item_count: number;

  /**
   * List of items
   */
  items: Array<Class>;
}
