/* tslint:disable */
/* eslint-disable */
import { SlideItem } from '../models/slide-item';
export interface PostSlideItems {

  /**
   * List of items
   */
  items?: (Array<SlideItem> | null);
}
