/* tslint:disable */
/* eslint-disable */
import { ArrowAnnotation } from '../models/arrow-annotation';
import { BoolPrimitive } from '../models/bool-primitive';
import { CircleAnnotation } from '../models/circle-annotation';
import { Class } from '../models/class';
import { Collection } from '../models/collection';
import { ContinuousPixelmap } from '../models/continuous-pixelmap';
import { DiscretePixelmap } from '../models/discrete-pixelmap';
import { FloatPrimitive } from '../models/float-primitive';
import { IntegerPrimitive } from '../models/integer-primitive';
import { LineAnnotation } from '../models/line-annotation';
import { NominalPixelmap } from '../models/nominal-pixelmap';
import { PointAnnotation } from '../models/point-annotation';
import { PolygonAnnotation } from '../models/polygon-annotation';
import { RectangleAnnotation } from '../models/rectangle-annotation';
import { StringPrimitive } from '../models/string-primitive';
export interface ItemQueryList {

  /**
   * Count of all items
   */
  item_count: number;

  /**
   * Items returned by item query
   */
  items: (Array<IntegerPrimitive> | Array<FloatPrimitive> | Array<BoolPrimitive> | Array<StringPrimitive> | Array<PointAnnotation> | Array<LineAnnotation> | Array<ArrowAnnotation> | Array<CircleAnnotation> | Array<RectangleAnnotation> | Array<PolygonAnnotation> | Array<Class> | Array<ContinuousPixelmap> | Array<DiscretePixelmap> | Array<NominalPixelmap> | Array<Collection>);
}
