/* tslint:disable */
/* eslint-disable */
import { PostStringPrimitive } from '../models/post-string-primitive';
export interface PostStringPrimitivesItems {

  /**
   * List of string primitives
   */
  items?: (Array<PostStringPrimitive> | null);
}
