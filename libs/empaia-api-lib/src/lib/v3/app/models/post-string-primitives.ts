/* tslint:disable */
/* eslint-disable */
import { PostStringPrimitive } from '../models/post-string-primitive';
export interface PostStringPrimitives {

  /**
   * List of string primitives
   */
  items: Array<PostStringPrimitive>;
}
