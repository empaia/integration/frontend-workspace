/* tslint:disable */
/* eslint-disable */
import { ArrowAnnotation } from '../models/arrow-annotation';
import { BoolPrimitive } from '../models/bool-primitive';
import { CircleAnnotation } from '../models/circle-annotation';
import { Class } from '../models/class';
import { CollectionItemType } from '../models/collection-item-type';
import { CollectionReferenceType } from '../models/collection-reference-type';
import { ContinuousPixelmap } from '../models/continuous-pixelmap';
import { DataCreatorType } from '../models/data-creator-type';
import { DiscretePixelmap } from '../models/discrete-pixelmap';
import { FloatPrimitive } from '../models/float-primitive';
import { IdObject } from '../models/id-object';
import { IntegerPrimitive } from '../models/integer-primitive';
import { LineAnnotation } from '../models/line-annotation';
import { NominalPixelmap } from '../models/nominal-pixelmap';
import { PointAnnotation } from '../models/point-annotation';
import { PolygonAnnotation } from '../models/polygon-annotation';
import { RectangleAnnotation } from '../models/rectangle-annotation';
import { SlideItem } from '../models/slide-item';
import { StringPrimitive } from '../models/string-primitive';
export interface Collection {

  /**
   * Creator Id
   */
  creator_id: string;

  /**
   * Creator type
   */
  creator_type: DataCreatorType;

  /**
   * Collection description
   */
  description?: (string | null);

  /**
   * ID of type UUID4 (only needed in post if external Ids enabled)
   */
  id?: (string | null);

  /**
   * Flag to mark a collection as immutable
   */
  is_locked?: (boolean | null);

  /**
   * The number of items in the collection
   */
  item_count?: (number | null);

  /**
   * Ids of items in collection
   */
  item_ids?: (Array<string> | null);

  /**
   * Item type of collection
   */
  item_type: CollectionItemType;

  /**
   * Items of the collection
   */
  items?: (Array<PointAnnotation> | Array<LineAnnotation> | Array<ArrowAnnotation> | Array<CircleAnnotation> | Array<RectangleAnnotation> | Array<PolygonAnnotation> | Array<Class> | Array<IntegerPrimitive> | Array<FloatPrimitive> | Array<BoolPrimitive> | Array<StringPrimitive> | Array<ContinuousPixelmap> | Array<DiscretePixelmap> | Array<NominalPixelmap> | Array<SlideItem> | Array<IdObject> | Array<Collection> | null);

  /**
   * Collection name
   */
  name?: (string | null);

  /**
   * Id of the object referenced by this collection
   */
  reference_id?: (string | null);

  /**
   * Refrence type
   */
  reference_type?: (CollectionReferenceType | null);

  /**
   * Collection type
   */
  type: any;
}
