/* tslint:disable */
/* eslint-disable */
import { PostPolygonAnnotation } from '../models/post-polygon-annotation';
export interface PostPolygonAnnotations {

  /**
   * List of polygon annotations
   */
  items: Array<PostPolygonAnnotation>;
}
