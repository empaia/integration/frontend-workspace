/* tslint:disable */
/* eslint-disable */
import { PostCircleAnnotation } from '../models/post-circle-annotation';
export interface PostCircleAnnotations {

  /**
   * List of circle annotations
   */
  items: Array<PostCircleAnnotation>;
}
