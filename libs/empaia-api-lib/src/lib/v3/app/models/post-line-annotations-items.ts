/* tslint:disable */
/* eslint-disable */
import { PostLineAnnotation } from '../models/post-line-annotation';
export interface PostLineAnnotationsItems {

  /**
   * List of line annotations
   */
  items?: (Array<PostLineAnnotation> | null);
}
