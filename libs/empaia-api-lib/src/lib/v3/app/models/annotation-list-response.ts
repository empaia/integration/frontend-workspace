/* tslint:disable */
/* eslint-disable */
import { ArrowAnnotation } from '../models/arrow-annotation';
import { CircleAnnotation } from '../models/circle-annotation';
import { LineAnnotation } from '../models/line-annotation';
import { PointAnnotation } from '../models/point-annotation';
import { PolygonAnnotation } from '../models/polygon-annotation';
import { RectangleAnnotation } from '../models/rectangle-annotation';
export interface AnnotationListResponse {

  /**
   * Count of all items
   */
  item_count: number;

  /**
   * List of items
   */
  items: Array<(PointAnnotation | LineAnnotation | ArrowAnnotation | CircleAnnotation | RectangleAnnotation | PolygonAnnotation)>;
}
