/* tslint:disable */
/* eslint-disable */
import { HttpClient, HttpContext } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { BaseService } from '../base-service';
import { ApiConfiguration } from '../api-configuration';
import { StrictHttpResponse } from '../strict-http-response';

import { CollectionList } from '../models/collection-list';
import { scopeIdCollectionsQueryPut } from '../fn/depricated/scope-id-collections-query-put';
import { ScopeIdCollectionsQueryPut$Params } from '../fn/depricated/scope-id-collections-query-put';

@Injectable({ providedIn: 'root' })
export class DepricatedService extends BaseService {
  constructor(config: ApiConfiguration, http: HttpClient) {
    super(config, http);
  }

  /** Path part for operation `scopeIdCollectionsQueryPut()` */
  static readonly ScopeIdCollectionsQueryPutPath = '/{scope_id}/collections/query';

  /**
   * Query collections.
   *
   * **ENDPOINT IS DEPRICATED**
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `scopeIdCollectionsQueryPut()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  scopeIdCollectionsQueryPut$Response(params: ScopeIdCollectionsQueryPut$Params, context?: HttpContext): Observable<StrictHttpResponse<CollectionList>> {
    return scopeIdCollectionsQueryPut(this.http, this.rootUrl, params, context);
  }

  /**
   * Query collections.
   *
   * **ENDPOINT IS DEPRICATED**
   *
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `scopeIdCollectionsQueryPut$Response()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  scopeIdCollectionsQueryPut(params: ScopeIdCollectionsQueryPut$Params, context?: HttpContext): Observable<CollectionList> {
    return this.scopeIdCollectionsQueryPut$Response(params, context).pipe(
      map((r: StrictHttpResponse<CollectionList>): CollectionList => r.body)
    );
  }

}
