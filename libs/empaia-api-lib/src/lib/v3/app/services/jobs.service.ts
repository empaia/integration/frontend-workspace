/* tslint:disable */
/* eslint-disable */
import { HttpClient, HttpContext } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { BaseService } from '../base-service';
import { ApiConfiguration } from '../api-configuration';
import { StrictHttpResponse } from '../strict-http-response';

import { Job } from '../models/job';
import { JobList } from '../models/job-list';
import { JobSlideList } from '../models/job-slide-list';
import { scopeIdJobsGet } from '../fn/jobs/scope-id-jobs-get';
import { ScopeIdJobsGet$Params } from '../fn/jobs/scope-id-jobs-get';
import { scopeIdJobsJobIdDelete } from '../fn/jobs/scope-id-jobs-job-id-delete';
import { ScopeIdJobsJobIdDelete$Params } from '../fn/jobs/scope-id-jobs-job-id-delete';
import { scopeIdJobsJobIdFinalizePut } from '../fn/jobs/scope-id-jobs-job-id-finalize-put';
import { ScopeIdJobsJobIdFinalizePut$Params } from '../fn/jobs/scope-id-jobs-job-id-finalize-put';
import { scopeIdJobsJobIdGet } from '../fn/jobs/scope-id-jobs-job-id-get';
import { ScopeIdJobsJobIdGet$Params } from '../fn/jobs/scope-id-jobs-job-id-get';
import { scopeIdJobsJobIdInputsInputKeyDelete } from '../fn/jobs/scope-id-jobs-job-id-inputs-input-key-delete';
import { ScopeIdJobsJobIdInputsInputKeyDelete$Params } from '../fn/jobs/scope-id-jobs-job-id-inputs-input-key-delete';
import { scopeIdJobsJobIdInputsInputKeyPut } from '../fn/jobs/scope-id-jobs-job-id-inputs-input-key-put';
import { ScopeIdJobsJobIdInputsInputKeyPut$Params } from '../fn/jobs/scope-id-jobs-job-id-inputs-input-key-put';
import { scopeIdJobsJobIdOutputsOutputKeyDelete } from '../fn/jobs/scope-id-jobs-job-id-outputs-output-key-delete';
import { ScopeIdJobsJobIdOutputsOutputKeyDelete$Params } from '../fn/jobs/scope-id-jobs-job-id-outputs-output-key-delete';
import { scopeIdJobsJobIdOutputsOutputKeyPut } from '../fn/jobs/scope-id-jobs-job-id-outputs-output-key-put';
import { ScopeIdJobsJobIdOutputsOutputKeyPut$Params } from '../fn/jobs/scope-id-jobs-job-id-outputs-output-key-put';
import { scopeIdJobsJobIdRunPut } from '../fn/jobs/scope-id-jobs-job-id-run-put';
import { ScopeIdJobsJobIdRunPut$Params } from '../fn/jobs/scope-id-jobs-job-id-run-put';
import { scopeIdJobsJobIdSlidesGet } from '../fn/jobs/scope-id-jobs-job-id-slides-get';
import { ScopeIdJobsJobIdSlidesGet$Params } from '../fn/jobs/scope-id-jobs-job-id-slides-get';
import { scopeIdJobsJobIdStopPut } from '../fn/jobs/scope-id-jobs-job-id-stop-put';
import { ScopeIdJobsJobIdStopPut$Params } from '../fn/jobs/scope-id-jobs-job-id-stop-put';
import { scopeIdJobsPost } from '../fn/jobs/scope-id-jobs-post';
import { ScopeIdJobsPost$Params } from '../fn/jobs/scope-id-jobs-post';

@Injectable({ providedIn: 'root' })
export class JobsService extends BaseService {
  constructor(config: ApiConfiguration, http: HttpClient) {
    super(config, http);
  }

  /** Path part for operation `scopeIdJobsGet()` */
  static readonly ScopeIdJobsGetPath = '/{scope_id}/jobs';

  /**
   * Get jobs.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `scopeIdJobsGet()` instead.
   *
   * This method doesn't expect any request body.
   */
  scopeIdJobsGet$Response(params: ScopeIdJobsGet$Params, context?: HttpContext): Observable<StrictHttpResponse<JobList>> {
    return scopeIdJobsGet(this.http, this.rootUrl, params, context);
  }

  /**
   * Get jobs.
   *
   *
   *
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `scopeIdJobsGet$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  scopeIdJobsGet(params: ScopeIdJobsGet$Params, context?: HttpContext): Observable<JobList> {
    return this.scopeIdJobsGet$Response(params, context).pipe(
      map((r: StrictHttpResponse<JobList>): JobList => r.body)
    );
  }

  /** Path part for operation `scopeIdJobsPost()` */
  static readonly ScopeIdJobsPostPath = '/{scope_id}/jobs';

  /**
   * Create job.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `scopeIdJobsPost()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  scopeIdJobsPost$Response(params: ScopeIdJobsPost$Params, context?: HttpContext): Observable<StrictHttpResponse<Job>> {
    return scopeIdJobsPost(this.http, this.rootUrl, params, context);
  }

  /**
   * Create job.
   *
   *
   *
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `scopeIdJobsPost$Response()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  scopeIdJobsPost(params: ScopeIdJobsPost$Params, context?: HttpContext): Observable<Job> {
    return this.scopeIdJobsPost$Response(params, context).pipe(
      map((r: StrictHttpResponse<Job>): Job => r.body)
    );
  }

  /** Path part for operation `scopeIdJobsJobIdInputsInputKeyPut()` */
  static readonly ScopeIdJobsJobIdInputsInputKeyPutPath = '/{scope_id}/jobs/{job_id}/inputs/{input_key}';

  /**
   * Update input of job.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `scopeIdJobsJobIdInputsInputKeyPut()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  scopeIdJobsJobIdInputsInputKeyPut$Response(params: ScopeIdJobsJobIdInputsInputKeyPut$Params, context?: HttpContext): Observable<StrictHttpResponse<Job>> {
    return scopeIdJobsJobIdInputsInputKeyPut(this.http, this.rootUrl, params, context);
  }

  /**
   * Update input of job.
   *
   *
   *
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `scopeIdJobsJobIdInputsInputKeyPut$Response()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  scopeIdJobsJobIdInputsInputKeyPut(params: ScopeIdJobsJobIdInputsInputKeyPut$Params, context?: HttpContext): Observable<Job> {
    return this.scopeIdJobsJobIdInputsInputKeyPut$Response(params, context).pipe(
      map((r: StrictHttpResponse<Job>): Job => r.body)
    );
  }

  /** Path part for operation `scopeIdJobsJobIdInputsInputKeyDelete()` */
  static readonly ScopeIdJobsJobIdInputsInputKeyDeletePath = '/{scope_id}/jobs/{job_id}/inputs/{input_key}';

  /**
   * Remove input from job.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `scopeIdJobsJobIdInputsInputKeyDelete()` instead.
   *
   * This method doesn't expect any request body.
   */
  scopeIdJobsJobIdInputsInputKeyDelete$Response(params: ScopeIdJobsJobIdInputsInputKeyDelete$Params, context?: HttpContext): Observable<StrictHttpResponse<Job>> {
    return scopeIdJobsJobIdInputsInputKeyDelete(this.http, this.rootUrl, params, context);
  }

  /**
   * Remove input from job.
   *
   *
   *
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `scopeIdJobsJobIdInputsInputKeyDelete$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  scopeIdJobsJobIdInputsInputKeyDelete(params: ScopeIdJobsJobIdInputsInputKeyDelete$Params, context?: HttpContext): Observable<Job> {
    return this.scopeIdJobsJobIdInputsInputKeyDelete$Response(params, context).pipe(
      map((r: StrictHttpResponse<Job>): Job => r.body)
    );
  }

  /** Path part for operation `scopeIdJobsJobIdOutputsOutputKeyPut()` */
  static readonly ScopeIdJobsJobIdOutputsOutputKeyPutPath = '/{scope_id}/jobs/{job_id}/outputs/{output_key}';

  /**
   * Update output of job.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `scopeIdJobsJobIdOutputsOutputKeyPut()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  scopeIdJobsJobIdOutputsOutputKeyPut$Response(params: ScopeIdJobsJobIdOutputsOutputKeyPut$Params, context?: HttpContext): Observable<StrictHttpResponse<Job>> {
    return scopeIdJobsJobIdOutputsOutputKeyPut(this.http, this.rootUrl, params, context);
  }

  /**
   * Update output of job.
   *
   *
   *
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `scopeIdJobsJobIdOutputsOutputKeyPut$Response()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  scopeIdJobsJobIdOutputsOutputKeyPut(params: ScopeIdJobsJobIdOutputsOutputKeyPut$Params, context?: HttpContext): Observable<Job> {
    return this.scopeIdJobsJobIdOutputsOutputKeyPut$Response(params, context).pipe(
      map((r: StrictHttpResponse<Job>): Job => r.body)
    );
  }

  /** Path part for operation `scopeIdJobsJobIdOutputsOutputKeyDelete()` */
  static readonly ScopeIdJobsJobIdOutputsOutputKeyDeletePath = '/{scope_id}/jobs/{job_id}/outputs/{output_key}';

  /**
   * Update output of job.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `scopeIdJobsJobIdOutputsOutputKeyDelete()` instead.
   *
   * This method doesn't expect any request body.
   */
  scopeIdJobsJobIdOutputsOutputKeyDelete$Response(params: ScopeIdJobsJobIdOutputsOutputKeyDelete$Params, context?: HttpContext): Observable<StrictHttpResponse<Job>> {
    return scopeIdJobsJobIdOutputsOutputKeyDelete(this.http, this.rootUrl, params, context);
  }

  /**
   * Update output of job.
   *
   *
   *
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `scopeIdJobsJobIdOutputsOutputKeyDelete$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  scopeIdJobsJobIdOutputsOutputKeyDelete(params: ScopeIdJobsJobIdOutputsOutputKeyDelete$Params, context?: HttpContext): Observable<Job> {
    return this.scopeIdJobsJobIdOutputsOutputKeyDelete$Response(params, context).pipe(
      map((r: StrictHttpResponse<Job>): Job => r.body)
    );
  }

  /** Path part for operation `scopeIdJobsJobIdGet()` */
  static readonly ScopeIdJobsJobIdGetPath = '/{scope_id}/jobs/{job_id}';

  /**
   * Get job.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `scopeIdJobsJobIdGet()` instead.
   *
   * This method doesn't expect any request body.
   */
  scopeIdJobsJobIdGet$Response(params: ScopeIdJobsJobIdGet$Params, context?: HttpContext): Observable<StrictHttpResponse<Job>> {
    return scopeIdJobsJobIdGet(this.http, this.rootUrl, params, context);
  }

  /**
   * Get job.
   *
   *
   *
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `scopeIdJobsJobIdGet$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  scopeIdJobsJobIdGet(params: ScopeIdJobsJobIdGet$Params, context?: HttpContext): Observable<Job> {
    return this.scopeIdJobsJobIdGet$Response(params, context).pipe(
      map((r: StrictHttpResponse<Job>): Job => r.body)
    );
  }

  /** Path part for operation `scopeIdJobsJobIdDelete()` */
  static readonly ScopeIdJobsJobIdDeletePath = '/{scope_id}/jobs/{job_id}';

  /**
   * Remove job.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `scopeIdJobsJobIdDelete()` instead.
   *
   * This method doesn't expect any request body.
   */
  scopeIdJobsJobIdDelete$Response(params: ScopeIdJobsJobIdDelete$Params, context?: HttpContext): Observable<StrictHttpResponse<Job>> {
    return scopeIdJobsJobIdDelete(this.http, this.rootUrl, params, context);
  }

  /**
   * Remove job.
   *
   *
   *
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `scopeIdJobsJobIdDelete$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  scopeIdJobsJobIdDelete(params: ScopeIdJobsJobIdDelete$Params, context?: HttpContext): Observable<Job> {
    return this.scopeIdJobsJobIdDelete$Response(params, context).pipe(
      map((r: StrictHttpResponse<Job>): Job => r.body)
    );
  }

  /** Path part for operation `scopeIdJobsJobIdRunPut()` */
  static readonly ScopeIdJobsJobIdRunPutPath = '/{scope_id}/jobs/{job_id}/run';

  /**
   * Run job.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `scopeIdJobsJobIdRunPut()` instead.
   *
   * This method doesn't expect any request body.
   */
  scopeIdJobsJobIdRunPut$Response(params: ScopeIdJobsJobIdRunPut$Params, context?: HttpContext): Observable<StrictHttpResponse<Job>> {
    return scopeIdJobsJobIdRunPut(this.http, this.rootUrl, params, context);
  }

  /**
   * Run job.
   *
   *
   *
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `scopeIdJobsJobIdRunPut$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  scopeIdJobsJobIdRunPut(params: ScopeIdJobsJobIdRunPut$Params, context?: HttpContext): Observable<Job> {
    return this.scopeIdJobsJobIdRunPut$Response(params, context).pipe(
      map((r: StrictHttpResponse<Job>): Job => r.body)
    );
  }

  /** Path part for operation `scopeIdJobsJobIdFinalizePut()` */
  static readonly ScopeIdJobsJobIdFinalizePutPath = '/{scope_id}/jobs/{job_id}/finalize';

  /**
   * Finalize job.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `scopeIdJobsJobIdFinalizePut()` instead.
   *
   * This method doesn't expect any request body.
   */
  scopeIdJobsJobIdFinalizePut$Response(params: ScopeIdJobsJobIdFinalizePut$Params, context?: HttpContext): Observable<StrictHttpResponse<Job>> {
    return scopeIdJobsJobIdFinalizePut(this.http, this.rootUrl, params, context);
  }

  /**
   * Finalize job.
   *
   *
   *
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `scopeIdJobsJobIdFinalizePut$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  scopeIdJobsJobIdFinalizePut(params: ScopeIdJobsJobIdFinalizePut$Params, context?: HttpContext): Observable<Job> {
    return this.scopeIdJobsJobIdFinalizePut$Response(params, context).pipe(
      map((r: StrictHttpResponse<Job>): Job => r.body)
    );
  }

  /** Path part for operation `scopeIdJobsJobIdStopPut()` */
  static readonly ScopeIdJobsJobIdStopPutPath = '/{scope_id}/jobs/{job_id}/stop';

  /**
   * Stop job.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `scopeIdJobsJobIdStopPut()` instead.
   *
   * This method doesn't expect any request body.
   */
  scopeIdJobsJobIdStopPut$Response(params: ScopeIdJobsJobIdStopPut$Params, context?: HttpContext): Observable<StrictHttpResponse<boolean>> {
    return scopeIdJobsJobIdStopPut(this.http, this.rootUrl, params, context);
  }

  /**
   * Stop job.
   *
   *
   *
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `scopeIdJobsJobIdStopPut$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  scopeIdJobsJobIdStopPut(params: ScopeIdJobsJobIdStopPut$Params, context?: HttpContext): Observable<boolean> {
    return this.scopeIdJobsJobIdStopPut$Response(params, context).pipe(
      map((r: StrictHttpResponse<boolean>): boolean => r.body)
    );
  }

  /** Path part for operation `scopeIdJobsJobIdSlidesGet()` */
  static readonly ScopeIdJobsJobIdSlidesGetPath = '/{scope_id}/jobs/{job_id}/slides';

  /**
   * Get slides used in a given job.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `scopeIdJobsJobIdSlidesGet()` instead.
   *
   * This method doesn't expect any request body.
   */
  scopeIdJobsJobIdSlidesGet$Response(params: ScopeIdJobsJobIdSlidesGet$Params, context?: HttpContext): Observable<StrictHttpResponse<JobSlideList>> {
    return scopeIdJobsJobIdSlidesGet(this.http, this.rootUrl, params, context);
  }

  /**
   * Get slides used in a given job.
   *
   *
   *
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `scopeIdJobsJobIdSlidesGet$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  scopeIdJobsJobIdSlidesGet(params: ScopeIdJobsJobIdSlidesGet$Params, context?: HttpContext): Observable<JobSlideList> {
    return this.scopeIdJobsJobIdSlidesGet$Response(params, context).pipe(
      map((r: StrictHttpResponse<JobSlideList>): JobSlideList => r.body)
    );
  }

}
