/* tslint:disable */
/* eslint-disable */
import { HttpClient, HttpContext } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { BaseService } from '../base-service';
import { ApiConfiguration } from '../api-configuration';
import { StrictHttpResponse } from '../strict-http-response';

import { AppUiStorage } from '../models/app-ui-storage';
import { scopeIdAppUiStorageScopeGet } from '../fn/storage/scope-id-app-ui-storage-scope-get';
import { ScopeIdAppUiStorageScopeGet$Params } from '../fn/storage/scope-id-app-ui-storage-scope-get';
import { scopeIdAppUiStorageScopePut } from '../fn/storage/scope-id-app-ui-storage-scope-put';
import { ScopeIdAppUiStorageScopePut$Params } from '../fn/storage/scope-id-app-ui-storage-scope-put';
import { scopeIdAppUiStorageUserGet } from '../fn/storage/scope-id-app-ui-storage-user-get';
import { ScopeIdAppUiStorageUserGet$Params } from '../fn/storage/scope-id-app-ui-storage-user-get';
import { scopeIdAppUiStorageUserPut } from '../fn/storage/scope-id-app-ui-storage-user-put';
import { ScopeIdAppUiStorageUserPut$Params } from '../fn/storage/scope-id-app-ui-storage-user-put';

@Injectable({ providedIn: 'root' })
export class StorageService extends BaseService {
  constructor(config: ApiConfiguration, http: HttpClient) {
    super(config, http);
  }

  /** Path part for operation `scopeIdAppUiStorageUserGet()` */
  static readonly ScopeIdAppUiStorageUserGetPath = '/{scope_id}/app-ui-storage/user';

  /**
   * Get app ui storage for user of given scope.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `scopeIdAppUiStorageUserGet()` instead.
   *
   * This method doesn't expect any request body.
   */
  scopeIdAppUiStorageUserGet$Response(params: ScopeIdAppUiStorageUserGet$Params, context?: HttpContext): Observable<StrictHttpResponse<AppUiStorage>> {
    return scopeIdAppUiStorageUserGet(this.http, this.rootUrl, params, context);
  }

  /**
   * Get app ui storage for user of given scope.
   *
   *
   *
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `scopeIdAppUiStorageUserGet$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  scopeIdAppUiStorageUserGet(params: ScopeIdAppUiStorageUserGet$Params, context?: HttpContext): Observable<AppUiStorage> {
    return this.scopeIdAppUiStorageUserGet$Response(params, context).pipe(
      map((r: StrictHttpResponse<AppUiStorage>): AppUiStorage => r.body)
    );
  }

  /** Path part for operation `scopeIdAppUiStorageUserPut()` */
  static readonly ScopeIdAppUiStorageUserPutPath = '/{scope_id}/app-ui-storage/user';

  /**
   * Put app ui storage for user of given scope.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `scopeIdAppUiStorageUserPut()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  scopeIdAppUiStorageUserPut$Response(params: ScopeIdAppUiStorageUserPut$Params, context?: HttpContext): Observable<StrictHttpResponse<AppUiStorage>> {
    return scopeIdAppUiStorageUserPut(this.http, this.rootUrl, params, context);
  }

  /**
   * Put app ui storage for user of given scope.
   *
   *
   *
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `scopeIdAppUiStorageUserPut$Response()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  scopeIdAppUiStorageUserPut(params: ScopeIdAppUiStorageUserPut$Params, context?: HttpContext): Observable<AppUiStorage> {
    return this.scopeIdAppUiStorageUserPut$Response(params, context).pipe(
      map((r: StrictHttpResponse<AppUiStorage>): AppUiStorage => r.body)
    );
  }

  /** Path part for operation `scopeIdAppUiStorageScopeGet()` */
  static readonly ScopeIdAppUiStorageScopeGetPath = '/{scope_id}/app-ui-storage/scope';

  /**
   * Get app ui storage for scope.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `scopeIdAppUiStorageScopeGet()` instead.
   *
   * This method doesn't expect any request body.
   */
  scopeIdAppUiStorageScopeGet$Response(params: ScopeIdAppUiStorageScopeGet$Params, context?: HttpContext): Observable<StrictHttpResponse<AppUiStorage>> {
    return scopeIdAppUiStorageScopeGet(this.http, this.rootUrl, params, context);
  }

  /**
   * Get app ui storage for scope.
   *
   *
   *
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `scopeIdAppUiStorageScopeGet$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  scopeIdAppUiStorageScopeGet(params: ScopeIdAppUiStorageScopeGet$Params, context?: HttpContext): Observable<AppUiStorage> {
    return this.scopeIdAppUiStorageScopeGet$Response(params, context).pipe(
      map((r: StrictHttpResponse<AppUiStorage>): AppUiStorage => r.body)
    );
  }

  /** Path part for operation `scopeIdAppUiStorageScopePut()` */
  static readonly ScopeIdAppUiStorageScopePutPath = '/{scope_id}/app-ui-storage/scope';

  /**
   * Put app ui storage for scope.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `scopeIdAppUiStorageScopePut()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  scopeIdAppUiStorageScopePut$Response(params: ScopeIdAppUiStorageScopePut$Params, context?: HttpContext): Observable<StrictHttpResponse<AppUiStorage>> {
    return scopeIdAppUiStorageScopePut(this.http, this.rootUrl, params, context);
  }

  /**
   * Put app ui storage for scope.
   *
   *
   *
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `scopeIdAppUiStorageScopePut$Response()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  scopeIdAppUiStorageScopePut(params: ScopeIdAppUiStorageScopePut$Params, context?: HttpContext): Observable<AppUiStorage> {
    return this.scopeIdAppUiStorageScopePut$Response(params, context).pipe(
      map((r: StrictHttpResponse<AppUiStorage>): AppUiStorage => r.body)
    );
  }

}
