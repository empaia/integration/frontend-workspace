/* tslint:disable */
/* eslint-disable */
import { HttpClient, HttpContext } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { BaseService } from '../base-service';
import { ApiConfiguration } from '../api-configuration';
import { StrictHttpResponse } from '../strict-http-response';

import { scopeIdSlidesGet } from '../fn/slides/scope-id-slides-get';
import { ScopeIdSlidesGet$Params } from '../fn/slides/scope-id-slides-get';
import { scopeIdSlidesSlideIdInfoGet } from '../fn/slides/scope-id-slides-slide-id-info-get';
import { ScopeIdSlidesSlideIdInfoGet$Params } from '../fn/slides/scope-id-slides-slide-id-info-get';
import { scopeIdSlidesSlideIdLabelMaxSizeMaxXMaxYGet } from '../fn/slides/scope-id-slides-slide-id-label-max-size-max-x-max-y-get';
import { ScopeIdSlidesSlideIdLabelMaxSizeMaxXMaxYGet$Params } from '../fn/slides/scope-id-slides-slide-id-label-max-size-max-x-max-y-get';
import { scopeIdSlidesSlideIdMacroMaxSizeMaxXMaxYGet } from '../fn/slides/scope-id-slides-slide-id-macro-max-size-max-x-max-y-get';
import { ScopeIdSlidesSlideIdMacroMaxSizeMaxXMaxYGet$Params } from '../fn/slides/scope-id-slides-slide-id-macro-max-size-max-x-max-y-get';
import { scopeIdSlidesSlideIdRegionLevelLevelStartStartXStartYSizeSizeXSizeYGet } from '../fn/slides/scope-id-slides-slide-id-region-level-level-start-start-x-start-y-size-size-x-size-y-get';
import { ScopeIdSlidesSlideIdRegionLevelLevelStartStartXStartYSizeSizeXSizeYGet$Params } from '../fn/slides/scope-id-slides-slide-id-region-level-level-start-start-x-start-y-size-size-x-size-y-get';
import { scopeIdSlidesSlideIdThumbnailMaxSizeMaxXMaxYGet } from '../fn/slides/scope-id-slides-slide-id-thumbnail-max-size-max-x-max-y-get';
import { ScopeIdSlidesSlideIdThumbnailMaxSizeMaxXMaxYGet$Params } from '../fn/slides/scope-id-slides-slide-id-thumbnail-max-size-max-x-max-y-get';
import { scopeIdSlidesSlideIdTileLevelLevelTileTileXTileYGet } from '../fn/slides/scope-id-slides-slide-id-tile-level-level-tile-tile-x-tile-y-get';
import { ScopeIdSlidesSlideIdTileLevelLevelTileTileXTileYGet$Params } from '../fn/slides/scope-id-slides-slide-id-tile-level-level-tile-tile-x-tile-y-get';
import { SlideInfo } from '../models/slide-info';
import { SlideList } from '../models/slide-list';

@Injectable({ providedIn: 'root' })
export class SlidesService extends BaseService {
  constructor(config: ApiConfiguration, http: HttpClient) {
    super(config, http);
  }

  /** Path part for operation `scopeIdSlidesGet()` */
  static readonly ScopeIdSlidesGetPath = '/{scope_id}/slides';

  /**
   * Get slides of current scope.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `scopeIdSlidesGet()` instead.
   *
   * This method doesn't expect any request body.
   */
  scopeIdSlidesGet$Response(params: ScopeIdSlidesGet$Params, context?: HttpContext): Observable<StrictHttpResponse<SlideList>> {
    return scopeIdSlidesGet(this.http, this.rootUrl, params, context);
  }

  /**
   * Get slides of current scope.
   *
   *
   *
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `scopeIdSlidesGet$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  scopeIdSlidesGet(params: ScopeIdSlidesGet$Params, context?: HttpContext): Observable<SlideList> {
    return this.scopeIdSlidesGet$Response(params, context).pipe(
      map((r: StrictHttpResponse<SlideList>): SlideList => r.body)
    );
  }

  /** Path part for operation `scopeIdSlidesSlideIdInfoGet()` */
  static readonly ScopeIdSlidesSlideIdInfoGetPath = '/{scope_id}/slides/{slide_id}/info';

  /**
   * Get slide info.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `scopeIdSlidesSlideIdInfoGet()` instead.
   *
   * This method doesn't expect any request body.
   */
  scopeIdSlidesSlideIdInfoGet$Response(params: ScopeIdSlidesSlideIdInfoGet$Params, context?: HttpContext): Observable<StrictHttpResponse<SlideInfo>> {
    return scopeIdSlidesSlideIdInfoGet(this.http, this.rootUrl, params, context);
  }

  /**
   * Get slide info.
   *
   *
   *
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `scopeIdSlidesSlideIdInfoGet$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  scopeIdSlidesSlideIdInfoGet(params: ScopeIdSlidesSlideIdInfoGet$Params, context?: HttpContext): Observable<SlideInfo> {
    return this.scopeIdSlidesSlideIdInfoGet$Response(params, context).pipe(
      map((r: StrictHttpResponse<SlideInfo>): SlideInfo => r.body)
    );
  }

  /** Path part for operation `scopeIdSlidesSlideIdTileLevelLevelTileTileXTileYGet()` */
  static readonly ScopeIdSlidesSlideIdTileLevelLevelTileTileXTileYGetPath = '/{scope_id}/slides/{slide_id}/tile/level/{level}/tile/{tile_x}/{tile_y}';

  /**
   * Get slide tile.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `scopeIdSlidesSlideIdTileLevelLevelTileTileXTileYGet()` instead.
   *
   * This method doesn't expect any request body.
   */
  scopeIdSlidesSlideIdTileLevelLevelTileTileXTileYGet$Response(params: ScopeIdSlidesSlideIdTileLevelLevelTileTileXTileYGet$Params, context?: HttpContext): Observable<StrictHttpResponse<Blob>> {
    return scopeIdSlidesSlideIdTileLevelLevelTileTileXTileYGet(this.http, this.rootUrl, params, context);
  }

  /**
   * Get slide tile.
   *
   *
   *
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `scopeIdSlidesSlideIdTileLevelLevelTileTileXTileYGet$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  scopeIdSlidesSlideIdTileLevelLevelTileTileXTileYGet(params: ScopeIdSlidesSlideIdTileLevelLevelTileTileXTileYGet$Params, context?: HttpContext): Observable<Blob> {
    return this.scopeIdSlidesSlideIdTileLevelLevelTileTileXTileYGet$Response(params, context).pipe(
      map((r: StrictHttpResponse<Blob>): Blob => r.body)
    );
  }

  /** Path part for operation `scopeIdSlidesSlideIdRegionLevelLevelStartStartXStartYSizeSizeXSizeYGet()` */
  static readonly ScopeIdSlidesSlideIdRegionLevelLevelStartStartXStartYSizeSizeXSizeYGetPath = '/{scope_id}/slides/{slide_id}/region/level/{level}/start/{start_x}/{start_y}/size/{size_x}/{size_y}';

  /**
   * Get slide region.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `scopeIdSlidesSlideIdRegionLevelLevelStartStartXStartYSizeSizeXSizeYGet()` instead.
   *
   * This method doesn't expect any request body.
   */
  scopeIdSlidesSlideIdRegionLevelLevelStartStartXStartYSizeSizeXSizeYGet$Response(params: ScopeIdSlidesSlideIdRegionLevelLevelStartStartXStartYSizeSizeXSizeYGet$Params, context?: HttpContext): Observable<StrictHttpResponse<Blob>> {
    return scopeIdSlidesSlideIdRegionLevelLevelStartStartXStartYSizeSizeXSizeYGet(this.http, this.rootUrl, params, context);
  }

  /**
   * Get slide region.
   *
   *
   *
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `scopeIdSlidesSlideIdRegionLevelLevelStartStartXStartYSizeSizeXSizeYGet$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  scopeIdSlidesSlideIdRegionLevelLevelStartStartXStartYSizeSizeXSizeYGet(params: ScopeIdSlidesSlideIdRegionLevelLevelStartStartXStartYSizeSizeXSizeYGet$Params, context?: HttpContext): Observable<Blob> {
    return this.scopeIdSlidesSlideIdRegionLevelLevelStartStartXStartYSizeSizeXSizeYGet$Response(params, context).pipe(
      map((r: StrictHttpResponse<Blob>): Blob => r.body)
    );
  }

  /** Path part for operation `scopeIdSlidesSlideIdMacroMaxSizeMaxXMaxYGet()` */
  static readonly ScopeIdSlidesSlideIdMacroMaxSizeMaxXMaxYGetPath = '/{scope_id}/slides/{slide_id}/macro/max_size/{max_x}/{max_y}';

  /**
   * Get slide macro image.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `scopeIdSlidesSlideIdMacroMaxSizeMaxXMaxYGet()` instead.
   *
   * This method doesn't expect any request body.
   */
  scopeIdSlidesSlideIdMacroMaxSizeMaxXMaxYGet$Response(params: ScopeIdSlidesSlideIdMacroMaxSizeMaxXMaxYGet$Params, context?: HttpContext): Observable<StrictHttpResponse<Blob>> {
    return scopeIdSlidesSlideIdMacroMaxSizeMaxXMaxYGet(this.http, this.rootUrl, params, context);
  }

  /**
   * Get slide macro image.
   *
   *
   *
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `scopeIdSlidesSlideIdMacroMaxSizeMaxXMaxYGet$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  scopeIdSlidesSlideIdMacroMaxSizeMaxXMaxYGet(params: ScopeIdSlidesSlideIdMacroMaxSizeMaxXMaxYGet$Params, context?: HttpContext): Observable<Blob> {
    return this.scopeIdSlidesSlideIdMacroMaxSizeMaxXMaxYGet$Response(params, context).pipe(
      map((r: StrictHttpResponse<Blob>): Blob => r.body)
    );
  }

  /** Path part for operation `scopeIdSlidesSlideIdLabelMaxSizeMaxXMaxYGet()` */
  static readonly ScopeIdSlidesSlideIdLabelMaxSizeMaxXMaxYGetPath = '/{scope_id}/slides/{slide_id}/label/max_size/{max_x}/{max_y}';

  /**
   * Get slide label image.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `scopeIdSlidesSlideIdLabelMaxSizeMaxXMaxYGet()` instead.
   *
   * This method doesn't expect any request body.
   */
  scopeIdSlidesSlideIdLabelMaxSizeMaxXMaxYGet$Response(params: ScopeIdSlidesSlideIdLabelMaxSizeMaxXMaxYGet$Params, context?: HttpContext): Observable<StrictHttpResponse<Blob>> {
    return scopeIdSlidesSlideIdLabelMaxSizeMaxXMaxYGet(this.http, this.rootUrl, params, context);
  }

  /**
   * Get slide label image.
   *
   *
   *
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `scopeIdSlidesSlideIdLabelMaxSizeMaxXMaxYGet$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  scopeIdSlidesSlideIdLabelMaxSizeMaxXMaxYGet(params: ScopeIdSlidesSlideIdLabelMaxSizeMaxXMaxYGet$Params, context?: HttpContext): Observable<Blob> {
    return this.scopeIdSlidesSlideIdLabelMaxSizeMaxXMaxYGet$Response(params, context).pipe(
      map((r: StrictHttpResponse<Blob>): Blob => r.body)
    );
  }

  /** Path part for operation `scopeIdSlidesSlideIdThumbnailMaxSizeMaxXMaxYGet()` */
  static readonly ScopeIdSlidesSlideIdThumbnailMaxSizeMaxXMaxYGetPath = '/{scope_id}/slides/{slide_id}/thumbnail/max_size/{max_x}/{max_y}';

  /**
   * Get slide thumbnail.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `scopeIdSlidesSlideIdThumbnailMaxSizeMaxXMaxYGet()` instead.
   *
   * This method doesn't expect any request body.
   */
  scopeIdSlidesSlideIdThumbnailMaxSizeMaxXMaxYGet$Response(params: ScopeIdSlidesSlideIdThumbnailMaxSizeMaxXMaxYGet$Params, context?: HttpContext): Observable<StrictHttpResponse<Blob>> {
    return scopeIdSlidesSlideIdThumbnailMaxSizeMaxXMaxYGet(this.http, this.rootUrl, params, context);
  }

  /**
   * Get slide thumbnail.
   *
   *
   *
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `scopeIdSlidesSlideIdThumbnailMaxSizeMaxXMaxYGet$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  scopeIdSlidesSlideIdThumbnailMaxSizeMaxXMaxYGet(params: ScopeIdSlidesSlideIdThumbnailMaxSizeMaxXMaxYGet$Params, context?: HttpContext): Observable<Blob> {
    return this.scopeIdSlidesSlideIdThumbnailMaxSizeMaxXMaxYGet$Response(params, context).pipe(
      map((r: StrictHttpResponse<Blob>): Blob => r.body)
    );
  }

}
