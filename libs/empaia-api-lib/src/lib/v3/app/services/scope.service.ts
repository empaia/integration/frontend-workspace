/* tslint:disable */
/* eslint-disable */
import { HttpClient, HttpContext } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { BaseService } from '../base-service';
import { ApiConfiguration } from '../api-configuration';
import { StrictHttpResponse } from '../strict-http-response';

import { ExtendedScope } from '../models/extended-scope';
import { scopeIdGet } from '../fn/scope/scope-id-get';
import { ScopeIdGet$Params } from '../fn/scope/scope-id-get';

@Injectable({ providedIn: 'root' })
export class ScopeService extends BaseService {
  constructor(config: ApiConfiguration, http: HttpClient) {
    super(config, http);
  }

  /** Path part for operation `scopeIdGet()` */
  static readonly ScopeIdGetPath = '/{scope_id}';

  /**
   * Get scope metadata.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `scopeIdGet()` instead.
   *
   * This method doesn't expect any request body.
   */
  scopeIdGet$Response(params: ScopeIdGet$Params, context?: HttpContext): Observable<StrictHttpResponse<ExtendedScope>> {
    return scopeIdGet(this.http, this.rootUrl, params, context);
  }

  /**
   * Get scope metadata.
   *
   *
   *
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `scopeIdGet$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  scopeIdGet(params: ScopeIdGet$Params, context?: HttpContext): Observable<ExtendedScope> {
    return this.scopeIdGet$Response(params, context).pipe(
      map((r: StrictHttpResponse<ExtendedScope>): ExtendedScope => r.body)
    );
  }

}
