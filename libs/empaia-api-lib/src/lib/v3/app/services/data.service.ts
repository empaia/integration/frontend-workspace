/* tslint:disable */
/* eslint-disable */
import { HttpClient, HttpContext } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { BaseService } from '../base-service';
import { ApiConfiguration } from '../api-configuration';
import { StrictHttpResponse } from '../strict-http-response';

import { AnnotationCountResponse } from '../models/annotation-count-response';
import { AnnotationList } from '../models/annotation-list';
import { AnnotationListResponse } from '../models/annotation-list-response';
import { AnnotationQueryPosition } from '../models/annotation-query-position';
import { AnnotationViewerList } from '../models/annotation-viewer-list';
import { ArrowAnnotation } from '../models/arrow-annotation';
import { BoolPrimitive } from '../models/bool-primitive';
import { CircleAnnotation } from '../models/circle-annotation';
import { Class } from '../models/class';
import { ClassesDict } from '../models/classes-dict';
import { ClassList } from '../models/class-list';
import { ClassListResponse } from '../models/class-list-response';
import { Collection } from '../models/collection';
import { CollectionList } from '../models/collection-list';
import { ContinuousPixelmap } from '../models/continuous-pixelmap';
import { DiscretePixelmap } from '../models/discrete-pixelmap';
import { FloatPrimitive } from '../models/float-primitive';
import { IdObject } from '../models/id-object';
import { IntegerPrimitive } from '../models/integer-primitive';
import { ItemQueryList } from '../models/item-query-list';
import { LineAnnotation } from '../models/line-annotation';
import { Message } from '../models/message';
import { NominalPixelmap } from '../models/nominal-pixelmap';
import { PixelmapList } from '../models/pixelmap-list';
import { PointAnnotation } from '../models/point-annotation';
import { PolygonAnnotation } from '../models/polygon-annotation';
import { PrimitiveList } from '../models/primitive-list';
import { RectangleAnnotation } from '../models/rectangle-annotation';
import { scopeIdAnnotationsAnnotationIdDelete } from '../fn/data/scope-id-annotations-annotation-id-delete';
import { ScopeIdAnnotationsAnnotationIdDelete$Params } from '../fn/data/scope-id-annotations-annotation-id-delete';
import { scopeIdAnnotationsAnnotationIdGet } from '../fn/data/scope-id-annotations-annotation-id-get';
import { ScopeIdAnnotationsAnnotationIdGet$Params } from '../fn/data/scope-id-annotations-annotation-id-get';
import { scopeIdAnnotationsAnnotationIdQueryPut } from '../fn/data/scope-id-annotations-annotation-id-query-put';
import { ScopeIdAnnotationsAnnotationIdQueryPut$Params } from '../fn/data/scope-id-annotations-annotation-id-query-put';
import { scopeIdAnnotationsPost } from '../fn/data/scope-id-annotations-post';
import { ScopeIdAnnotationsPost$Params } from '../fn/data/scope-id-annotations-post';
import { scopeIdAnnotationsQueryCountPut } from '../fn/data/scope-id-annotations-query-count-put';
import { ScopeIdAnnotationsQueryCountPut$Params } from '../fn/data/scope-id-annotations-query-count-put';
import { scopeIdAnnotationsQueryPut } from '../fn/data/scope-id-annotations-query-put';
import { ScopeIdAnnotationsQueryPut$Params } from '../fn/data/scope-id-annotations-query-put';
import { scopeIdAnnotationsQueryUniqueClassValuesPut } from '../fn/data/scope-id-annotations-query-unique-class-values-put';
import { ScopeIdAnnotationsQueryUniqueClassValuesPut$Params } from '../fn/data/scope-id-annotations-query-unique-class-values-put';
import { scopeIdAnnotationsQueryUniqueReferencesPut } from '../fn/data/scope-id-annotations-query-unique-references-put';
import { ScopeIdAnnotationsQueryUniqueReferencesPut$Params } from '../fn/data/scope-id-annotations-query-unique-references-put';
import { scopeIdAnnotationsQueryViewerPut } from '../fn/data/scope-id-annotations-query-viewer-put';
import { ScopeIdAnnotationsQueryViewerPut$Params } from '../fn/data/scope-id-annotations-query-viewer-put';
import { scopeIdClassesClassIdDelete } from '../fn/data/scope-id-classes-class-id-delete';
import { ScopeIdClassesClassIdDelete$Params } from '../fn/data/scope-id-classes-class-id-delete';
import { scopeIdClassesClassIdGet } from '../fn/data/scope-id-classes-class-id-get';
import { ScopeIdClassesClassIdGet$Params } from '../fn/data/scope-id-classes-class-id-get';
import { scopeIdClassesPost } from '../fn/data/scope-id-classes-post';
import { ScopeIdClassesPost$Params } from '../fn/data/scope-id-classes-post';
import { scopeIdClassesQueryPut } from '../fn/data/scope-id-classes-query-put';
import { ScopeIdClassesQueryPut$Params } from '../fn/data/scope-id-classes-query-put';
import { scopeIdClassesQueryUniqueReferencesPut } from '../fn/data/scope-id-classes-query-unique-references-put';
import { ScopeIdClassesQueryUniqueReferencesPut$Params } from '../fn/data/scope-id-classes-query-unique-references-put';
import { scopeIdClassNamespacesGet } from '../fn/data/scope-id-class-namespaces-get';
import { ScopeIdClassNamespacesGet$Params } from '../fn/data/scope-id-class-namespaces-get';
import { scopeIdCollectionsCollectionIdDelete } from '../fn/data/scope-id-collections-collection-id-delete';
import { ScopeIdCollectionsCollectionIdDelete$Params } from '../fn/data/scope-id-collections-collection-id-delete';
import { scopeIdCollectionsCollectionIdGet } from '../fn/data/scope-id-collections-collection-id-get';
import { ScopeIdCollectionsCollectionIdGet$Params } from '../fn/data/scope-id-collections-collection-id-get';
import { scopeIdCollectionsCollectionIdItemsItemIdDelete } from '../fn/data/scope-id-collections-collection-id-items-item-id-delete';
import { ScopeIdCollectionsCollectionIdItemsItemIdDelete$Params } from '../fn/data/scope-id-collections-collection-id-items-item-id-delete';
import { scopeIdCollectionsCollectionIdItemsPost } from '../fn/data/scope-id-collections-collection-id-items-post';
import { ScopeIdCollectionsCollectionIdItemsPost$Params } from '../fn/data/scope-id-collections-collection-id-items-post';
import { scopeIdCollectionsCollectionIdItemsQueryPut } from '../fn/data/scope-id-collections-collection-id-items-query-put';
import { ScopeIdCollectionsCollectionIdItemsQueryPut$Params } from '../fn/data/scope-id-collections-collection-id-items-query-put';
import { scopeIdCollectionsCollectionIdItemsQueryUniqueReferencesPut } from '../fn/data/scope-id-collections-collection-id-items-query-unique-references-put';
import { ScopeIdCollectionsCollectionIdItemsQueryUniqueReferencesPut$Params } from '../fn/data/scope-id-collections-collection-id-items-query-unique-references-put';
import { scopeIdCollectionsPost } from '../fn/data/scope-id-collections-post';
import { ScopeIdCollectionsPost$Params } from '../fn/data/scope-id-collections-post';
import { scopeIdCollectionsQueryUniqueReferencesPut } from '../fn/data/scope-id-collections-query-unique-references-put';
import { ScopeIdCollectionsQueryUniqueReferencesPut$Params } from '../fn/data/scope-id-collections-query-unique-references-put';
import { scopeIdPixelmapsPixelmapIdDelete } from '../fn/data/scope-id-pixelmaps-pixelmap-id-delete';
import { ScopeIdPixelmapsPixelmapIdDelete$Params } from '../fn/data/scope-id-pixelmaps-pixelmap-id-delete';
import { scopeIdPixelmapsPixelmapIdGet } from '../fn/data/scope-id-pixelmaps-pixelmap-id-get';
import { ScopeIdPixelmapsPixelmapIdGet$Params } from '../fn/data/scope-id-pixelmaps-pixelmap-id-get';
import { scopeIdPixelmapsPixelmapIdLevelLevelPositionStartStartXStartYEndEndXEndYDataGet$Any } from '../fn/data/scope-id-pixelmaps-pixelmap-id-level-level-position-start-start-x-start-y-end-end-x-end-y-data-get-any';
import { ScopeIdPixelmapsPixelmapIdLevelLevelPositionStartStartXStartYEndEndXEndYDataGet$Any$Params } from '../fn/data/scope-id-pixelmaps-pixelmap-id-level-level-position-start-start-x-start-y-end-end-x-end-y-data-get-any';
import { scopeIdPixelmapsPixelmapIdLevelLevelPositionStartStartXStartYEndEndXEndYDataGet$Json } from '../fn/data/scope-id-pixelmaps-pixelmap-id-level-level-position-start-start-x-start-y-end-end-x-end-y-data-get-json';
import { ScopeIdPixelmapsPixelmapIdLevelLevelPositionStartStartXStartYEndEndXEndYDataGet$Json$Params } from '../fn/data/scope-id-pixelmaps-pixelmap-id-level-level-position-start-start-x-start-y-end-end-x-end-y-data-get-json';
import { scopeIdPixelmapsPixelmapIdLevelLevelPositionStartStartXStartYEndEndXEndYDataPut } from '../fn/data/scope-id-pixelmaps-pixelmap-id-level-level-position-start-start-x-start-y-end-end-x-end-y-data-put';
import { ScopeIdPixelmapsPixelmapIdLevelLevelPositionStartStartXStartYEndEndXEndYDataPut$Params } from '../fn/data/scope-id-pixelmaps-pixelmap-id-level-level-position-start-start-x-start-y-end-end-x-end-y-data-put';
import { scopeIdPixelmapsPixelmapIdLevelLevelPositionTileXTileYDataDelete } from '../fn/data/scope-id-pixelmaps-pixelmap-id-level-level-position-tile-x-tile-y-data-delete';
import { ScopeIdPixelmapsPixelmapIdLevelLevelPositionTileXTileYDataDelete$Params } from '../fn/data/scope-id-pixelmaps-pixelmap-id-level-level-position-tile-x-tile-y-data-delete';
import { scopeIdPixelmapsPixelmapIdLevelLevelPositionTileXTileYDataGet } from '../fn/data/scope-id-pixelmaps-pixelmap-id-level-level-position-tile-x-tile-y-data-get';
import { ScopeIdPixelmapsPixelmapIdLevelLevelPositionTileXTileYDataGet$Params } from '../fn/data/scope-id-pixelmaps-pixelmap-id-level-level-position-tile-x-tile-y-data-get';
import { scopeIdPixelmapsPixelmapIdLevelLevelPositionTileXTileYDataPut } from '../fn/data/scope-id-pixelmaps-pixelmap-id-level-level-position-tile-x-tile-y-data-put';
import { ScopeIdPixelmapsPixelmapIdLevelLevelPositionTileXTileYDataPut$Params } from '../fn/data/scope-id-pixelmaps-pixelmap-id-level-level-position-tile-x-tile-y-data-put';
import { scopeIdPixelmapsPost } from '../fn/data/scope-id-pixelmaps-post';
import { ScopeIdPixelmapsPost$Params } from '../fn/data/scope-id-pixelmaps-post';
import { scopeIdPixelmapsQueryPut } from '../fn/data/scope-id-pixelmaps-query-put';
import { ScopeIdPixelmapsQueryPut$Params } from '../fn/data/scope-id-pixelmaps-query-put';
import { scopeIdPixelmapsQueryUniqueReferencesPut } from '../fn/data/scope-id-pixelmaps-query-unique-references-put';
import { ScopeIdPixelmapsQueryUniqueReferencesPut$Params } from '../fn/data/scope-id-pixelmaps-query-unique-references-put';
import { scopeIdPrimitivesPost } from '../fn/data/scope-id-primitives-post';
import { ScopeIdPrimitivesPost$Params } from '../fn/data/scope-id-primitives-post';
import { scopeIdPrimitivesPrimitiveIdDelete } from '../fn/data/scope-id-primitives-primitive-id-delete';
import { ScopeIdPrimitivesPrimitiveIdDelete$Params } from '../fn/data/scope-id-primitives-primitive-id-delete';
import { scopeIdPrimitivesPrimitiveIdGet } from '../fn/data/scope-id-primitives-primitive-id-get';
import { ScopeIdPrimitivesPrimitiveIdGet$Params } from '../fn/data/scope-id-primitives-primitive-id-get';
import { scopeIdPrimitivesQueryPut } from '../fn/data/scope-id-primitives-query-put';
import { ScopeIdPrimitivesQueryPut$Params } from '../fn/data/scope-id-primitives-query-put';
import { scopeIdPrimitivesQueryUniqueReferencesPut } from '../fn/data/scope-id-primitives-query-unique-references-put';
import { ScopeIdPrimitivesQueryUniqueReferencesPut$Params } from '../fn/data/scope-id-primitives-query-unique-references-put';
import { SlideItem } from '../models/slide-item';
import { SlideItemList } from '../models/slide-item-list';
import { StringPrimitive } from '../models/string-primitive';
import { UniqueClassValues } from '../models/unique-class-values';
import { UniqueReferences } from '../models/unique-references';

@Injectable({ providedIn: 'root' })
export class DataService extends BaseService {
  constructor(config: ApiConfiguration, http: HttpClient) {
    super(config, http);
  }

  /** Path part for operation `scopeIdAnnotationsQueryPut()` */
  static readonly ScopeIdAnnotationsQueryPutPath = '/{scope_id}/annotations/query';

  /**
   * Query annotations.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `scopeIdAnnotationsQueryPut()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  scopeIdAnnotationsQueryPut$Response(params: ScopeIdAnnotationsQueryPut$Params, context?: HttpContext): Observable<StrictHttpResponse<AnnotationList>> {
    return scopeIdAnnotationsQueryPut(this.http, this.rootUrl, params, context);
  }

  /**
   * Query annotations.
   *
   *
   *
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `scopeIdAnnotationsQueryPut$Response()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  scopeIdAnnotationsQueryPut(params: ScopeIdAnnotationsQueryPut$Params, context?: HttpContext): Observable<AnnotationList> {
    return this.scopeIdAnnotationsQueryPut$Response(params, context).pipe(
      map((r: StrictHttpResponse<AnnotationList>): AnnotationList => r.body)
    );
  }

  /** Path part for operation `scopeIdAnnotationsQueryCountPut()` */
  static readonly ScopeIdAnnotationsQueryCountPutPath = '/{scope_id}/annotations/query/count';

  /**
   * Get count of queried annotation items.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `scopeIdAnnotationsQueryCountPut()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  scopeIdAnnotationsQueryCountPut$Response(params: ScopeIdAnnotationsQueryCountPut$Params, context?: HttpContext): Observable<StrictHttpResponse<AnnotationCountResponse>> {
    return scopeIdAnnotationsQueryCountPut(this.http, this.rootUrl, params, context);
  }

  /**
   * Get count of queried annotation items.
   *
   *
   *
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `scopeIdAnnotationsQueryCountPut$Response()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  scopeIdAnnotationsQueryCountPut(params: ScopeIdAnnotationsQueryCountPut$Params, context?: HttpContext): Observable<AnnotationCountResponse> {
    return this.scopeIdAnnotationsQueryCountPut$Response(params, context).pipe(
      map((r: StrictHttpResponse<AnnotationCountResponse>): AnnotationCountResponse => r.body)
    );
  }

  /** Path part for operation `scopeIdAnnotationsQueryUniqueClassValuesPut()` */
  static readonly ScopeIdAnnotationsQueryUniqueClassValuesPutPath = '/{scope_id}/annotations/query/unique-class-values';

  /**
   * Get unique class values of queried annotations.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `scopeIdAnnotationsQueryUniqueClassValuesPut()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  scopeIdAnnotationsQueryUniqueClassValuesPut$Response(params: ScopeIdAnnotationsQueryUniqueClassValuesPut$Params, context?: HttpContext): Observable<StrictHttpResponse<UniqueClassValues>> {
    return scopeIdAnnotationsQueryUniqueClassValuesPut(this.http, this.rootUrl, params, context);
  }

  /**
   * Get unique class values of queried annotations.
   *
   *
   *
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `scopeIdAnnotationsQueryUniqueClassValuesPut$Response()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  scopeIdAnnotationsQueryUniqueClassValuesPut(params: ScopeIdAnnotationsQueryUniqueClassValuesPut$Params, context?: HttpContext): Observable<UniqueClassValues> {
    return this.scopeIdAnnotationsQueryUniqueClassValuesPut$Response(params, context).pipe(
      map((r: StrictHttpResponse<UniqueClassValues>): UniqueClassValues => r.body)
    );
  }

  /** Path part for operation `scopeIdAnnotationsQueryUniqueReferencesPut()` */
  static readonly ScopeIdAnnotationsQueryUniqueReferencesPutPath = '/{scope_id}/annotations/query/unique-references';

  /**
   * Get unique references of queried annotations.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `scopeIdAnnotationsQueryUniqueReferencesPut()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  scopeIdAnnotationsQueryUniqueReferencesPut$Response(params: ScopeIdAnnotationsQueryUniqueReferencesPut$Params, context?: HttpContext): Observable<StrictHttpResponse<UniqueReferences>> {
    return scopeIdAnnotationsQueryUniqueReferencesPut(this.http, this.rootUrl, params, context);
  }

  /**
   * Get unique references of queried annotations.
   *
   *
   *
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `scopeIdAnnotationsQueryUniqueReferencesPut$Response()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  scopeIdAnnotationsQueryUniqueReferencesPut(params: ScopeIdAnnotationsQueryUniqueReferencesPut$Params, context?: HttpContext): Observable<UniqueReferences> {
    return this.scopeIdAnnotationsQueryUniqueReferencesPut$Response(params, context).pipe(
      map((r: StrictHttpResponse<UniqueReferences>): UniqueReferences => r.body)
    );
  }

  /** Path part for operation `scopeIdAnnotationsQueryViewerPut()` */
  static readonly ScopeIdAnnotationsQueryViewerPutPath = '/{scope_id}/annotations/query/viewer';

  /**
   * Query annotations for viewing purposes.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `scopeIdAnnotationsQueryViewerPut()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  scopeIdAnnotationsQueryViewerPut$Response(params: ScopeIdAnnotationsQueryViewerPut$Params, context?: HttpContext): Observable<StrictHttpResponse<AnnotationViewerList>> {
    return scopeIdAnnotationsQueryViewerPut(this.http, this.rootUrl, params, context);
  }

  /**
   * Query annotations for viewing purposes.
   *
   *
   *
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `scopeIdAnnotationsQueryViewerPut$Response()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  scopeIdAnnotationsQueryViewerPut(params: ScopeIdAnnotationsQueryViewerPut$Params, context?: HttpContext): Observable<AnnotationViewerList> {
    return this.scopeIdAnnotationsQueryViewerPut$Response(params, context).pipe(
      map((r: StrictHttpResponse<AnnotationViewerList>): AnnotationViewerList => r.body)
    );
  }

  /** Path part for operation `scopeIdAnnotationsAnnotationIdQueryPut()` */
  static readonly ScopeIdAnnotationsAnnotationIdQueryPutPath = '/{scope_id}/annotations/{annotation_id}/query';

  /**
   * Get the postion of an annotation in the result query.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `scopeIdAnnotationsAnnotationIdQueryPut()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  scopeIdAnnotationsAnnotationIdQueryPut$Response(params: ScopeIdAnnotationsAnnotationIdQueryPut$Params, context?: HttpContext): Observable<StrictHttpResponse<AnnotationQueryPosition>> {
    return scopeIdAnnotationsAnnotationIdQueryPut(this.http, this.rootUrl, params, context);
  }

  /**
   * Get the postion of an annotation in the result query.
   *
   *
   *
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `scopeIdAnnotationsAnnotationIdQueryPut$Response()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  scopeIdAnnotationsAnnotationIdQueryPut(params: ScopeIdAnnotationsAnnotationIdQueryPut$Params, context?: HttpContext): Observable<AnnotationQueryPosition> {
    return this.scopeIdAnnotationsAnnotationIdQueryPut$Response(params, context).pipe(
      map((r: StrictHttpResponse<AnnotationQueryPosition>): AnnotationQueryPosition => r.body)
    );
  }

  /** Path part for operation `scopeIdAnnotationsPost()` */
  static readonly ScopeIdAnnotationsPostPath = '/{scope_id}/annotations';

  /**
   * Post annotations.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `scopeIdAnnotationsPost()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  scopeIdAnnotationsPost$Response(params: ScopeIdAnnotationsPost$Params, context?: HttpContext): Observable<StrictHttpResponse<(PointAnnotation | LineAnnotation | ArrowAnnotation | CircleAnnotation | RectangleAnnotation | PolygonAnnotation | AnnotationListResponse)>> {
    return scopeIdAnnotationsPost(this.http, this.rootUrl, params, context);
  }

  /**
   * Post annotations.
   *
   *
   *
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `scopeIdAnnotationsPost$Response()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  scopeIdAnnotationsPost(params: ScopeIdAnnotationsPost$Params, context?: HttpContext): Observable<(PointAnnotation | LineAnnotation | ArrowAnnotation | CircleAnnotation | RectangleAnnotation | PolygonAnnotation | AnnotationListResponse)> {
    return this.scopeIdAnnotationsPost$Response(params, context).pipe(
      map((r: StrictHttpResponse<(PointAnnotation | LineAnnotation | ArrowAnnotation | CircleAnnotation | RectangleAnnotation | PolygonAnnotation | AnnotationListResponse)>): (PointAnnotation | LineAnnotation | ArrowAnnotation | CircleAnnotation | RectangleAnnotation | PolygonAnnotation | AnnotationListResponse) => r.body)
    );
  }

  /** Path part for operation `scopeIdAnnotationsAnnotationIdGet()` */
  static readonly ScopeIdAnnotationsAnnotationIdGetPath = '/{scope_id}/annotations/{annotation_id}';

  /**
   * Get annotation by ID.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `scopeIdAnnotationsAnnotationIdGet()` instead.
   *
   * This method doesn't expect any request body.
   */
  scopeIdAnnotationsAnnotationIdGet$Response(params: ScopeIdAnnotationsAnnotationIdGet$Params, context?: HttpContext): Observable<StrictHttpResponse<(PointAnnotation | LineAnnotation | ArrowAnnotation | CircleAnnotation | RectangleAnnotation | PolygonAnnotation)>> {
    return scopeIdAnnotationsAnnotationIdGet(this.http, this.rootUrl, params, context);
  }

  /**
   * Get annotation by ID.
   *
   *
   *
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `scopeIdAnnotationsAnnotationIdGet$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  scopeIdAnnotationsAnnotationIdGet(params: ScopeIdAnnotationsAnnotationIdGet$Params, context?: HttpContext): Observable<(PointAnnotation | LineAnnotation | ArrowAnnotation | CircleAnnotation | RectangleAnnotation | PolygonAnnotation)> {
    return this.scopeIdAnnotationsAnnotationIdGet$Response(params, context).pipe(
      map((r: StrictHttpResponse<(PointAnnotation | LineAnnotation | ArrowAnnotation | CircleAnnotation | RectangleAnnotation | PolygonAnnotation)>): (PointAnnotation | LineAnnotation | ArrowAnnotation | CircleAnnotation | RectangleAnnotation | PolygonAnnotation) => r.body)
    );
  }

  /** Path part for operation `scopeIdAnnotationsAnnotationIdDelete()` */
  static readonly ScopeIdAnnotationsAnnotationIdDeletePath = '/{scope_id}/annotations/{annotation_id}';

  /**
   * Delete annotation by ID.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `scopeIdAnnotationsAnnotationIdDelete()` instead.
   *
   * This method doesn't expect any request body.
   */
  scopeIdAnnotationsAnnotationIdDelete$Response(params: ScopeIdAnnotationsAnnotationIdDelete$Params, context?: HttpContext): Observable<StrictHttpResponse<IdObject>> {
    return scopeIdAnnotationsAnnotationIdDelete(this.http, this.rootUrl, params, context);
  }

  /**
   * Delete annotation by ID.
   *
   *
   *
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `scopeIdAnnotationsAnnotationIdDelete$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  scopeIdAnnotationsAnnotationIdDelete(params: ScopeIdAnnotationsAnnotationIdDelete$Params, context?: HttpContext): Observable<IdObject> {
    return this.scopeIdAnnotationsAnnotationIdDelete$Response(params, context).pipe(
      map((r: StrictHttpResponse<IdObject>): IdObject => r.body)
    );
  }

  /** Path part for operation `scopeIdClassesQueryPut()` */
  static readonly ScopeIdClassesQueryPutPath = '/{scope_id}/classes/query';

  /**
   * Query classes.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `scopeIdClassesQueryPut()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  scopeIdClassesQueryPut$Response(params: ScopeIdClassesQueryPut$Params, context?: HttpContext): Observable<StrictHttpResponse<ClassList>> {
    return scopeIdClassesQueryPut(this.http, this.rootUrl, params, context);
  }

  /**
   * Query classes.
   *
   *
   *
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `scopeIdClassesQueryPut$Response()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  scopeIdClassesQueryPut(params: ScopeIdClassesQueryPut$Params, context?: HttpContext): Observable<ClassList> {
    return this.scopeIdClassesQueryPut$Response(params, context).pipe(
      map((r: StrictHttpResponse<ClassList>): ClassList => r.body)
    );
  }

  /** Path part for operation `scopeIdClassesQueryUniqueReferencesPut()` */
  static readonly ScopeIdClassesQueryUniqueReferencesPutPath = '/{scope_id}/classes/query/unique-references';

  /**
   * Get unique references of queried classes.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `scopeIdClassesQueryUniqueReferencesPut()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  scopeIdClassesQueryUniqueReferencesPut$Response(params: ScopeIdClassesQueryUniqueReferencesPut$Params, context?: HttpContext): Observable<StrictHttpResponse<UniqueReferences>> {
    return scopeIdClassesQueryUniqueReferencesPut(this.http, this.rootUrl, params, context);
  }

  /**
   * Get unique references of queried classes.
   *
   *
   *
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `scopeIdClassesQueryUniqueReferencesPut$Response()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  scopeIdClassesQueryUniqueReferencesPut(params: ScopeIdClassesQueryUniqueReferencesPut$Params, context?: HttpContext): Observable<UniqueReferences> {
    return this.scopeIdClassesQueryUniqueReferencesPut$Response(params, context).pipe(
      map((r: StrictHttpResponse<UniqueReferences>): UniqueReferences => r.body)
    );
  }

  /** Path part for operation `scopeIdClassesPost()` */
  static readonly ScopeIdClassesPostPath = '/{scope_id}/classes';

  /**
   * Post classes.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `scopeIdClassesPost()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  scopeIdClassesPost$Response(params: ScopeIdClassesPost$Params, context?: HttpContext): Observable<StrictHttpResponse<(Class | ClassListResponse)>> {
    return scopeIdClassesPost(this.http, this.rootUrl, params, context);
  }

  /**
   * Post classes.
   *
   *
   *
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `scopeIdClassesPost$Response()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  scopeIdClassesPost(params: ScopeIdClassesPost$Params, context?: HttpContext): Observable<(Class | ClassListResponse)> {
    return this.scopeIdClassesPost$Response(params, context).pipe(
      map((r: StrictHttpResponse<(Class | ClassListResponse)>): (Class | ClassListResponse) => r.body)
    );
  }

  /** Path part for operation `scopeIdClassesClassIdGet()` */
  static readonly ScopeIdClassesClassIdGetPath = '/{scope_id}/classes/{class_id}';

  /**
   * Get class by ID.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `scopeIdClassesClassIdGet()` instead.
   *
   * This method doesn't expect any request body.
   */
  scopeIdClassesClassIdGet$Response(params: ScopeIdClassesClassIdGet$Params, context?: HttpContext): Observable<StrictHttpResponse<Class>> {
    return scopeIdClassesClassIdGet(this.http, this.rootUrl, params, context);
  }

  /**
   * Get class by ID.
   *
   *
   *
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `scopeIdClassesClassIdGet$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  scopeIdClassesClassIdGet(params: ScopeIdClassesClassIdGet$Params, context?: HttpContext): Observable<Class> {
    return this.scopeIdClassesClassIdGet$Response(params, context).pipe(
      map((r: StrictHttpResponse<Class>): Class => r.body)
    );
  }

  /** Path part for operation `scopeIdClassesClassIdDelete()` */
  static readonly ScopeIdClassesClassIdDeletePath = '/{scope_id}/classes/{class_id}';

  /**
   * Delete class by ID.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `scopeIdClassesClassIdDelete()` instead.
   *
   * This method doesn't expect any request body.
   */
  scopeIdClassesClassIdDelete$Response(params: ScopeIdClassesClassIdDelete$Params, context?: HttpContext): Observable<StrictHttpResponse<IdObject>> {
    return scopeIdClassesClassIdDelete(this.http, this.rootUrl, params, context);
  }

  /**
   * Delete class by ID.
   *
   *
   *
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `scopeIdClassesClassIdDelete$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  scopeIdClassesClassIdDelete(params: ScopeIdClassesClassIdDelete$Params, context?: HttpContext): Observable<IdObject> {
    return this.scopeIdClassesClassIdDelete$Response(params, context).pipe(
      map((r: StrictHttpResponse<IdObject>): IdObject => r.body)
    );
  }

  /** Path part for operation `scopeIdClassNamespacesGet()` */
  static readonly ScopeIdClassNamespacesGetPath = '/{scope_id}/class-namespaces';

  /**
   * Get class namespaces.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `scopeIdClassNamespacesGet()` instead.
   *
   * This method doesn't expect any request body.
   */
  scopeIdClassNamespacesGet$Response(params: ScopeIdClassNamespacesGet$Params, context?: HttpContext): Observable<StrictHttpResponse<{
[key: string]: ClassesDict;
}>> {
    return scopeIdClassNamespacesGet(this.http, this.rootUrl, params, context);
  }

  /**
   * Get class namespaces.
   *
   *
   *
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `scopeIdClassNamespacesGet$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  scopeIdClassNamespacesGet(params: ScopeIdClassNamespacesGet$Params, context?: HttpContext): Observable<{
[key: string]: ClassesDict;
}> {
    return this.scopeIdClassNamespacesGet$Response(params, context).pipe(
      map((r: StrictHttpResponse<{
[key: string]: ClassesDict;
}>): {
[key: string]: ClassesDict;
} => r.body)
    );
  }

  /** Path part for operation `scopeIdCollectionsQueryUniqueReferencesPut()` */
  static readonly ScopeIdCollectionsQueryUniqueReferencesPutPath = '/{scope_id}/collections/query/unique-references';

  /**
   * Get unique references of queried collections.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `scopeIdCollectionsQueryUniqueReferencesPut()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  scopeIdCollectionsQueryUniqueReferencesPut$Response(params: ScopeIdCollectionsQueryUniqueReferencesPut$Params, context?: HttpContext): Observable<StrictHttpResponse<UniqueReferences>> {
    return scopeIdCollectionsQueryUniqueReferencesPut(this.http, this.rootUrl, params, context);
  }

  /**
   * Get unique references of queried collections.
   *
   *
   *
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `scopeIdCollectionsQueryUniqueReferencesPut$Response()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  scopeIdCollectionsQueryUniqueReferencesPut(params: ScopeIdCollectionsQueryUniqueReferencesPut$Params, context?: HttpContext): Observable<UniqueReferences> {
    return this.scopeIdCollectionsQueryUniqueReferencesPut$Response(params, context).pipe(
      map((r: StrictHttpResponse<UniqueReferences>): UniqueReferences => r.body)
    );
  }

  /** Path part for operation `scopeIdCollectionsCollectionIdItemsQueryPut()` */
  static readonly ScopeIdCollectionsCollectionIdItemsQueryPutPath = '/{scope_id}/collections/{collection_id}/items/query';

  /**
   * Query items of collection.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `scopeIdCollectionsCollectionIdItemsQueryPut()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  scopeIdCollectionsCollectionIdItemsQueryPut$Response(params: ScopeIdCollectionsCollectionIdItemsQueryPut$Params, context?: HttpContext): Observable<StrictHttpResponse<ItemQueryList>> {
    return scopeIdCollectionsCollectionIdItemsQueryPut(this.http, this.rootUrl, params, context);
  }

  /**
   * Query items of collection.
   *
   *
   *
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `scopeIdCollectionsCollectionIdItemsQueryPut$Response()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  scopeIdCollectionsCollectionIdItemsQueryPut(params: ScopeIdCollectionsCollectionIdItemsQueryPut$Params, context?: HttpContext): Observable<ItemQueryList> {
    return this.scopeIdCollectionsCollectionIdItemsQueryPut$Response(params, context).pipe(
      map((r: StrictHttpResponse<ItemQueryList>): ItemQueryList => r.body)
    );
  }

  /** Path part for operation `scopeIdCollectionsCollectionIdItemsQueryUniqueReferencesPut()` */
  static readonly ScopeIdCollectionsCollectionIdItemsQueryUniqueReferencesPutPath = '/{scope_id}/collections/{collection_id}/items/query/unique-references';

  /**
   * Get unique references of queried items of a collection.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `scopeIdCollectionsCollectionIdItemsQueryUniqueReferencesPut()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  scopeIdCollectionsCollectionIdItemsQueryUniqueReferencesPut$Response(params: ScopeIdCollectionsCollectionIdItemsQueryUniqueReferencesPut$Params, context?: HttpContext): Observable<StrictHttpResponse<UniqueReferences>> {
    return scopeIdCollectionsCollectionIdItemsQueryUniqueReferencesPut(this.http, this.rootUrl, params, context);
  }

  /**
   * Get unique references of queried items of a collection.
   *
   *
   *
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `scopeIdCollectionsCollectionIdItemsQueryUniqueReferencesPut$Response()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  scopeIdCollectionsCollectionIdItemsQueryUniqueReferencesPut(params: ScopeIdCollectionsCollectionIdItemsQueryUniqueReferencesPut$Params, context?: HttpContext): Observable<UniqueReferences> {
    return this.scopeIdCollectionsCollectionIdItemsQueryUniqueReferencesPut$Response(params, context).pipe(
      map((r: StrictHttpResponse<UniqueReferences>): UniqueReferences => r.body)
    );
  }

  /** Path part for operation `scopeIdCollectionsPost()` */
  static readonly ScopeIdCollectionsPostPath = '/{scope_id}/collections';

  /**
   * Post collection.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `scopeIdCollectionsPost()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  scopeIdCollectionsPost$Response(params: ScopeIdCollectionsPost$Params, context?: HttpContext): Observable<StrictHttpResponse<Collection>> {
    return scopeIdCollectionsPost(this.http, this.rootUrl, params, context);
  }

  /**
   * Post collection.
   *
   *
   *
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `scopeIdCollectionsPost$Response()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  scopeIdCollectionsPost(params: ScopeIdCollectionsPost$Params, context?: HttpContext): Observable<Collection> {
    return this.scopeIdCollectionsPost$Response(params, context).pipe(
      map((r: StrictHttpResponse<Collection>): Collection => r.body)
    );
  }

  /** Path part for operation `scopeIdCollectionsCollectionIdGet()` */
  static readonly ScopeIdCollectionsCollectionIdGetPath = '/{scope_id}/collections/{collection_id}';

  /**
   * Get collection by ID.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `scopeIdCollectionsCollectionIdGet()` instead.
   *
   * This method doesn't expect any request body.
   */
  scopeIdCollectionsCollectionIdGet$Response(params: ScopeIdCollectionsCollectionIdGet$Params, context?: HttpContext): Observable<StrictHttpResponse<Collection>> {
    return scopeIdCollectionsCollectionIdGet(this.http, this.rootUrl, params, context);
  }

  /**
   * Get collection by ID.
   *
   *
   *
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `scopeIdCollectionsCollectionIdGet$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  scopeIdCollectionsCollectionIdGet(params: ScopeIdCollectionsCollectionIdGet$Params, context?: HttpContext): Observable<Collection> {
    return this.scopeIdCollectionsCollectionIdGet$Response(params, context).pipe(
      map((r: StrictHttpResponse<Collection>): Collection => r.body)
    );
  }

  /** Path part for operation `scopeIdCollectionsCollectionIdDelete()` */
  static readonly ScopeIdCollectionsCollectionIdDeletePath = '/{scope_id}/collections/{collection_id}';

  /**
   * Delete collection by ID.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `scopeIdCollectionsCollectionIdDelete()` instead.
   *
   * This method doesn't expect any request body.
   */
  scopeIdCollectionsCollectionIdDelete$Response(params: ScopeIdCollectionsCollectionIdDelete$Params, context?: HttpContext): Observable<StrictHttpResponse<IdObject>> {
    return scopeIdCollectionsCollectionIdDelete(this.http, this.rootUrl, params, context);
  }

  /**
   * Delete collection by ID.
   *
   *
   *
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `scopeIdCollectionsCollectionIdDelete$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  scopeIdCollectionsCollectionIdDelete(params: ScopeIdCollectionsCollectionIdDelete$Params, context?: HttpContext): Observable<IdObject> {
    return this.scopeIdCollectionsCollectionIdDelete$Response(params, context).pipe(
      map((r: StrictHttpResponse<IdObject>): IdObject => r.body)
    );
  }

  /** Path part for operation `scopeIdCollectionsCollectionIdItemsPost()` */
  static readonly ScopeIdCollectionsCollectionIdItemsPostPath = '/{scope_id}/collections/{collection_id}/items';

  /**
   * Add new items to existing collection.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `scopeIdCollectionsCollectionIdItemsPost()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  scopeIdCollectionsCollectionIdItemsPost$Response(params: ScopeIdCollectionsCollectionIdItemsPost$Params, context?: HttpContext): Observable<StrictHttpResponse<(Message | PointAnnotation | LineAnnotation | ArrowAnnotation | CircleAnnotation | RectangleAnnotation | PolygonAnnotation | AnnotationListResponse | IntegerPrimitive | FloatPrimitive | BoolPrimitive | StringPrimitive | PrimitiveList | Class | ClassListResponse | ContinuousPixelmap | DiscretePixelmap | NominalPixelmap | PixelmapList | SlideItem | SlideItemList | Collection | CollectionList)>> {
    return scopeIdCollectionsCollectionIdItemsPost(this.http, this.rootUrl, params, context);
  }

  /**
   * Add new items to existing collection.
   *
   *
   *
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `scopeIdCollectionsCollectionIdItemsPost$Response()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  scopeIdCollectionsCollectionIdItemsPost(params: ScopeIdCollectionsCollectionIdItemsPost$Params, context?: HttpContext): Observable<(Message | PointAnnotation | LineAnnotation | ArrowAnnotation | CircleAnnotation | RectangleAnnotation | PolygonAnnotation | AnnotationListResponse | IntegerPrimitive | FloatPrimitive | BoolPrimitive | StringPrimitive | PrimitiveList | Class | ClassListResponse | ContinuousPixelmap | DiscretePixelmap | NominalPixelmap | PixelmapList | SlideItem | SlideItemList | Collection | CollectionList)> {
    return this.scopeIdCollectionsCollectionIdItemsPost$Response(params, context).pipe(
      map((r: StrictHttpResponse<(Message | PointAnnotation | LineAnnotation | ArrowAnnotation | CircleAnnotation | RectangleAnnotation | PolygonAnnotation | AnnotationListResponse | IntegerPrimitive | FloatPrimitive | BoolPrimitive | StringPrimitive | PrimitiveList | Class | ClassListResponse | ContinuousPixelmap | DiscretePixelmap | NominalPixelmap | PixelmapList | SlideItem | SlideItemList | Collection | CollectionList)>): (Message | PointAnnotation | LineAnnotation | ArrowAnnotation | CircleAnnotation | RectangleAnnotation | PolygonAnnotation | AnnotationListResponse | IntegerPrimitive | FloatPrimitive | BoolPrimitive | StringPrimitive | PrimitiveList | Class | ClassListResponse | ContinuousPixelmap | DiscretePixelmap | NominalPixelmap | PixelmapList | SlideItem | SlideItemList | Collection | CollectionList) => r.body)
    );
  }

  /** Path part for operation `scopeIdCollectionsCollectionIdItemsItemIdDelete()` */
  static readonly ScopeIdCollectionsCollectionIdItemsItemIdDeletePath = '/{scope_id}/collections/{collection_id}/items/{item_id}';

  /**
   * Delete collection item from existing collection.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `scopeIdCollectionsCollectionIdItemsItemIdDelete()` instead.
   *
   * This method doesn't expect any request body.
   */
  scopeIdCollectionsCollectionIdItemsItemIdDelete$Response(params: ScopeIdCollectionsCollectionIdItemsItemIdDelete$Params, context?: HttpContext): Observable<StrictHttpResponse<IdObject>> {
    return scopeIdCollectionsCollectionIdItemsItemIdDelete(this.http, this.rootUrl, params, context);
  }

  /**
   * Delete collection item from existing collection.
   *
   *
   *
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `scopeIdCollectionsCollectionIdItemsItemIdDelete$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  scopeIdCollectionsCollectionIdItemsItemIdDelete(params: ScopeIdCollectionsCollectionIdItemsItemIdDelete$Params, context?: HttpContext): Observable<IdObject> {
    return this.scopeIdCollectionsCollectionIdItemsItemIdDelete$Response(params, context).pipe(
      map((r: StrictHttpResponse<IdObject>): IdObject => r.body)
    );
  }

  /** Path part for operation `scopeIdPrimitivesQueryPut()` */
  static readonly ScopeIdPrimitivesQueryPutPath = '/{scope_id}/primitives/query';

  /**
   * Query primitives.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `scopeIdPrimitivesQueryPut()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  scopeIdPrimitivesQueryPut$Response(params: ScopeIdPrimitivesQueryPut$Params, context?: HttpContext): Observable<StrictHttpResponse<PrimitiveList>> {
    return scopeIdPrimitivesQueryPut(this.http, this.rootUrl, params, context);
  }

  /**
   * Query primitives.
   *
   *
   *
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `scopeIdPrimitivesQueryPut$Response()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  scopeIdPrimitivesQueryPut(params: ScopeIdPrimitivesQueryPut$Params, context?: HttpContext): Observable<PrimitiveList> {
    return this.scopeIdPrimitivesQueryPut$Response(params, context).pipe(
      map((r: StrictHttpResponse<PrimitiveList>): PrimitiveList => r.body)
    );
  }

  /** Path part for operation `scopeIdPrimitivesQueryUniqueReferencesPut()` */
  static readonly ScopeIdPrimitivesQueryUniqueReferencesPutPath = '/{scope_id}/primitives/query/unique-references';

  /**
   * Get unique references of queried primitives.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `scopeIdPrimitivesQueryUniqueReferencesPut()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  scopeIdPrimitivesQueryUniqueReferencesPut$Response(params: ScopeIdPrimitivesQueryUniqueReferencesPut$Params, context?: HttpContext): Observable<StrictHttpResponse<UniqueReferences>> {
    return scopeIdPrimitivesQueryUniqueReferencesPut(this.http, this.rootUrl, params, context);
  }

  /**
   * Get unique references of queried primitives.
   *
   *
   *
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `scopeIdPrimitivesQueryUniqueReferencesPut$Response()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  scopeIdPrimitivesQueryUniqueReferencesPut(params: ScopeIdPrimitivesQueryUniqueReferencesPut$Params, context?: HttpContext): Observable<UniqueReferences> {
    return this.scopeIdPrimitivesQueryUniqueReferencesPut$Response(params, context).pipe(
      map((r: StrictHttpResponse<UniqueReferences>): UniqueReferences => r.body)
    );
  }

  /** Path part for operation `scopeIdPrimitivesPost()` */
  static readonly ScopeIdPrimitivesPostPath = '/{scope_id}/primitives';

  /**
   * Post primitives.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `scopeIdPrimitivesPost()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  scopeIdPrimitivesPost$Response(params: ScopeIdPrimitivesPost$Params, context?: HttpContext): Observable<StrictHttpResponse<(IntegerPrimitive | FloatPrimitive | BoolPrimitive | StringPrimitive | PrimitiveList)>> {
    return scopeIdPrimitivesPost(this.http, this.rootUrl, params, context);
  }

  /**
   * Post primitives.
   *
   *
   *
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `scopeIdPrimitivesPost$Response()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  scopeIdPrimitivesPost(params: ScopeIdPrimitivesPost$Params, context?: HttpContext): Observable<(IntegerPrimitive | FloatPrimitive | BoolPrimitive | StringPrimitive | PrimitiveList)> {
    return this.scopeIdPrimitivesPost$Response(params, context).pipe(
      map((r: StrictHttpResponse<(IntegerPrimitive | FloatPrimitive | BoolPrimitive | StringPrimitive | PrimitiveList)>): (IntegerPrimitive | FloatPrimitive | BoolPrimitive | StringPrimitive | PrimitiveList) => r.body)
    );
  }

  /** Path part for operation `scopeIdPrimitivesPrimitiveIdGet()` */
  static readonly ScopeIdPrimitivesPrimitiveIdGetPath = '/{scope_id}/primitives/{primitive_id}';

  /**
   * Get primitive by ID.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `scopeIdPrimitivesPrimitiveIdGet()` instead.
   *
   * This method doesn't expect any request body.
   */
  scopeIdPrimitivesPrimitiveIdGet$Response(params: ScopeIdPrimitivesPrimitiveIdGet$Params, context?: HttpContext): Observable<StrictHttpResponse<(IntegerPrimitive | FloatPrimitive | BoolPrimitive | StringPrimitive)>> {
    return scopeIdPrimitivesPrimitiveIdGet(this.http, this.rootUrl, params, context);
  }

  /**
   * Get primitive by ID.
   *
   *
   *
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `scopeIdPrimitivesPrimitiveIdGet$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  scopeIdPrimitivesPrimitiveIdGet(params: ScopeIdPrimitivesPrimitiveIdGet$Params, context?: HttpContext): Observable<(IntegerPrimitive | FloatPrimitive | BoolPrimitive | StringPrimitive)> {
    return this.scopeIdPrimitivesPrimitiveIdGet$Response(params, context).pipe(
      map((r: StrictHttpResponse<(IntegerPrimitive | FloatPrimitive | BoolPrimitive | StringPrimitive)>): (IntegerPrimitive | FloatPrimitive | BoolPrimitive | StringPrimitive) => r.body)
    );
  }

  /** Path part for operation `scopeIdPrimitivesPrimitiveIdDelete()` */
  static readonly ScopeIdPrimitivesPrimitiveIdDeletePath = '/{scope_id}/primitives/{primitive_id}';

  /**
   * Delete primitive by ID.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `scopeIdPrimitivesPrimitiveIdDelete()` instead.
   *
   * This method doesn't expect any request body.
   */
  scopeIdPrimitivesPrimitiveIdDelete$Response(params: ScopeIdPrimitivesPrimitiveIdDelete$Params, context?: HttpContext): Observable<StrictHttpResponse<IdObject>> {
    return scopeIdPrimitivesPrimitiveIdDelete(this.http, this.rootUrl, params, context);
  }

  /**
   * Delete primitive by ID.
   *
   *
   *
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `scopeIdPrimitivesPrimitiveIdDelete$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  scopeIdPrimitivesPrimitiveIdDelete(params: ScopeIdPrimitivesPrimitiveIdDelete$Params, context?: HttpContext): Observable<IdObject> {
    return this.scopeIdPrimitivesPrimitiveIdDelete$Response(params, context).pipe(
      map((r: StrictHttpResponse<IdObject>): IdObject => r.body)
    );
  }

  /** Path part for operation `scopeIdPixelmapsPost()` */
  static readonly ScopeIdPixelmapsPostPath = '/{scope_id}/pixelmaps';

  /**
   * Post pixelmaps.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `scopeIdPixelmapsPost()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  scopeIdPixelmapsPost$Response(params: ScopeIdPixelmapsPost$Params, context?: HttpContext): Observable<StrictHttpResponse<(ContinuousPixelmap | DiscretePixelmap | NominalPixelmap | PixelmapList)>> {
    return scopeIdPixelmapsPost(this.http, this.rootUrl, params, context);
  }

  /**
   * Post pixelmaps.
   *
   *
   *
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `scopeIdPixelmapsPost$Response()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  scopeIdPixelmapsPost(params: ScopeIdPixelmapsPost$Params, context?: HttpContext): Observable<(ContinuousPixelmap | DiscretePixelmap | NominalPixelmap | PixelmapList)> {
    return this.scopeIdPixelmapsPost$Response(params, context).pipe(
      map((r: StrictHttpResponse<(ContinuousPixelmap | DiscretePixelmap | NominalPixelmap | PixelmapList)>): (ContinuousPixelmap | DiscretePixelmap | NominalPixelmap | PixelmapList) => r.body)
    );
  }

  /** Path part for operation `scopeIdPixelmapsQueryPut()` */
  static readonly ScopeIdPixelmapsQueryPutPath = '/{scope_id}/pixelmaps/query';

  /**
   * Query pixelmaps.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `scopeIdPixelmapsQueryPut()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  scopeIdPixelmapsQueryPut$Response(params: ScopeIdPixelmapsQueryPut$Params, context?: HttpContext): Observable<StrictHttpResponse<PixelmapList>> {
    return scopeIdPixelmapsQueryPut(this.http, this.rootUrl, params, context);
  }

  /**
   * Query pixelmaps.
   *
   *
   *
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `scopeIdPixelmapsQueryPut$Response()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  scopeIdPixelmapsQueryPut(params: ScopeIdPixelmapsQueryPut$Params, context?: HttpContext): Observable<PixelmapList> {
    return this.scopeIdPixelmapsQueryPut$Response(params, context).pipe(
      map((r: StrictHttpResponse<PixelmapList>): PixelmapList => r.body)
    );
  }

  /** Path part for operation `scopeIdPixelmapsQueryUniqueReferencesPut()` */
  static readonly ScopeIdPixelmapsQueryUniqueReferencesPutPath = '/{scope_id}/pixelmaps/query/unique-references';

  /**
   * Get unique references of queried pixelmaps.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `scopeIdPixelmapsQueryUniqueReferencesPut()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  scopeIdPixelmapsQueryUniqueReferencesPut$Response(params: ScopeIdPixelmapsQueryUniqueReferencesPut$Params, context?: HttpContext): Observable<StrictHttpResponse<UniqueReferences>> {
    return scopeIdPixelmapsQueryUniqueReferencesPut(this.http, this.rootUrl, params, context);
  }

  /**
   * Get unique references of queried pixelmaps.
   *
   *
   *
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `scopeIdPixelmapsQueryUniqueReferencesPut$Response()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  scopeIdPixelmapsQueryUniqueReferencesPut(params: ScopeIdPixelmapsQueryUniqueReferencesPut$Params, context?: HttpContext): Observable<UniqueReferences> {
    return this.scopeIdPixelmapsQueryUniqueReferencesPut$Response(params, context).pipe(
      map((r: StrictHttpResponse<UniqueReferences>): UniqueReferences => r.body)
    );
  }

  /** Path part for operation `scopeIdPixelmapsPixelmapIdGet()` */
  static readonly ScopeIdPixelmapsPixelmapIdGetPath = '/{scope_id}/pixelmaps/{pixelmap_id}';

  /**
   * Get pixelmap by ID.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `scopeIdPixelmapsPixelmapIdGet()` instead.
   *
   * This method doesn't expect any request body.
   */
  scopeIdPixelmapsPixelmapIdGet$Response(params: ScopeIdPixelmapsPixelmapIdGet$Params, context?: HttpContext): Observable<StrictHttpResponse<(ContinuousPixelmap | DiscretePixelmap | NominalPixelmap)>> {
    return scopeIdPixelmapsPixelmapIdGet(this.http, this.rootUrl, params, context);
  }

  /**
   * Get pixelmap by ID.
   *
   *
   *
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `scopeIdPixelmapsPixelmapIdGet$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  scopeIdPixelmapsPixelmapIdGet(params: ScopeIdPixelmapsPixelmapIdGet$Params, context?: HttpContext): Observable<(ContinuousPixelmap | DiscretePixelmap | NominalPixelmap)> {
    return this.scopeIdPixelmapsPixelmapIdGet$Response(params, context).pipe(
      map((r: StrictHttpResponse<(ContinuousPixelmap | DiscretePixelmap | NominalPixelmap)>): (ContinuousPixelmap | DiscretePixelmap | NominalPixelmap) => r.body)
    );
  }

  /** Path part for operation `scopeIdPixelmapsPixelmapIdDelete()` */
  static readonly ScopeIdPixelmapsPixelmapIdDeletePath = '/{scope_id}/pixelmaps/{pixelmap_id}';

  /**
   * Delete pixelmap by ID.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `scopeIdPixelmapsPixelmapIdDelete()` instead.
   *
   * This method doesn't expect any request body.
   */
  scopeIdPixelmapsPixelmapIdDelete$Response(params: ScopeIdPixelmapsPixelmapIdDelete$Params, context?: HttpContext): Observable<StrictHttpResponse<IdObject>> {
    return scopeIdPixelmapsPixelmapIdDelete(this.http, this.rootUrl, params, context);
  }

  /**
   * Delete pixelmap by ID.
   *
   *
   *
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `scopeIdPixelmapsPixelmapIdDelete$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  scopeIdPixelmapsPixelmapIdDelete(params: ScopeIdPixelmapsPixelmapIdDelete$Params, context?: HttpContext): Observable<IdObject> {
    return this.scopeIdPixelmapsPixelmapIdDelete$Response(params, context).pipe(
      map((r: StrictHttpResponse<IdObject>): IdObject => r.body)
    );
  }

  /** Path part for operation `scopeIdPixelmapsPixelmapIdLevelLevelPositionTileXTileYDataGet()` */
  static readonly ScopeIdPixelmapsPixelmapIdLevelLevelPositionTileXTileYDataGetPath = '/{scope_id}/pixelmaps/{pixelmap_id}/level/{level}/position/{tile_x}/{tile_y}/data';

  /**
   * Get pixelmap tile.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `scopeIdPixelmapsPixelmapIdLevelLevelPositionTileXTileYDataGet()` instead.
   *
   * This method doesn't expect any request body.
   */
  scopeIdPixelmapsPixelmapIdLevelLevelPositionTileXTileYDataGet$Response(params: ScopeIdPixelmapsPixelmapIdLevelLevelPositionTileXTileYDataGet$Params, context?: HttpContext): Observable<StrictHttpResponse<Blob>> {
    return scopeIdPixelmapsPixelmapIdLevelLevelPositionTileXTileYDataGet(this.http, this.rootUrl, params, context);
  }

  /**
   * Get pixelmap tile.
   *
   *
   *
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `scopeIdPixelmapsPixelmapIdLevelLevelPositionTileXTileYDataGet$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  scopeIdPixelmapsPixelmapIdLevelLevelPositionTileXTileYDataGet(params: ScopeIdPixelmapsPixelmapIdLevelLevelPositionTileXTileYDataGet$Params, context?: HttpContext): Observable<Blob> {
    return this.scopeIdPixelmapsPixelmapIdLevelLevelPositionTileXTileYDataGet$Response(params, context).pipe(
      map((r: StrictHttpResponse<Blob>): Blob => r.body)
    );
  }

  /** Path part for operation `scopeIdPixelmapsPixelmapIdLevelLevelPositionTileXTileYDataPut()` */
  static readonly ScopeIdPixelmapsPixelmapIdLevelLevelPositionTileXTileYDataPutPath = '/{scope_id}/pixelmaps/{pixelmap_id}/level/{level}/position/{tile_x}/{tile_y}/data';

  /**
   * Post pixelmap tile.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `scopeIdPixelmapsPixelmapIdLevelLevelPositionTileXTileYDataPut()` instead.
   *
   * This method doesn't expect any request body.
   */
  scopeIdPixelmapsPixelmapIdLevelLevelPositionTileXTileYDataPut$Response(params: ScopeIdPixelmapsPixelmapIdLevelLevelPositionTileXTileYDataPut$Params, context?: HttpContext): Observable<StrictHttpResponse<void>> {
    return scopeIdPixelmapsPixelmapIdLevelLevelPositionTileXTileYDataPut(this.http, this.rootUrl, params, context);
  }

  /**
   * Post pixelmap tile.
   *
   *
   *
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `scopeIdPixelmapsPixelmapIdLevelLevelPositionTileXTileYDataPut$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  scopeIdPixelmapsPixelmapIdLevelLevelPositionTileXTileYDataPut(params: ScopeIdPixelmapsPixelmapIdLevelLevelPositionTileXTileYDataPut$Params, context?: HttpContext): Observable<void> {
    return this.scopeIdPixelmapsPixelmapIdLevelLevelPositionTileXTileYDataPut$Response(params, context).pipe(
      map((r: StrictHttpResponse<void>): void => r.body)
    );
  }

  /** Path part for operation `scopeIdPixelmapsPixelmapIdLevelLevelPositionTileXTileYDataDelete()` */
  static readonly ScopeIdPixelmapsPixelmapIdLevelLevelPositionTileXTileYDataDeletePath = '/{scope_id}/pixelmaps/{pixelmap_id}/level/{level}/position/{tile_x}/{tile_y}/data';

  /**
   * Delete pixelmap tile.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `scopeIdPixelmapsPixelmapIdLevelLevelPositionTileXTileYDataDelete()` instead.
   *
   * This method doesn't expect any request body.
   */
  scopeIdPixelmapsPixelmapIdLevelLevelPositionTileXTileYDataDelete$Response(params: ScopeIdPixelmapsPixelmapIdLevelLevelPositionTileXTileYDataDelete$Params, context?: HttpContext): Observable<StrictHttpResponse<void>> {
    return scopeIdPixelmapsPixelmapIdLevelLevelPositionTileXTileYDataDelete(this.http, this.rootUrl, params, context);
  }

  /**
   * Delete pixelmap tile.
   *
   *
   *
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `scopeIdPixelmapsPixelmapIdLevelLevelPositionTileXTileYDataDelete$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  scopeIdPixelmapsPixelmapIdLevelLevelPositionTileXTileYDataDelete(params: ScopeIdPixelmapsPixelmapIdLevelLevelPositionTileXTileYDataDelete$Params, context?: HttpContext): Observable<void> {
    return this.scopeIdPixelmapsPixelmapIdLevelLevelPositionTileXTileYDataDelete$Response(params, context).pipe(
      map((r: StrictHttpResponse<void>): void => r.body)
    );
  }

  /** Path part for operation `scopeIdPixelmapsPixelmapIdLevelLevelPositionStartStartXStartYEndEndXEndYDataGet()` */
  static readonly ScopeIdPixelmapsPixelmapIdLevelLevelPositionStartStartXStartYEndEndXEndYDataGetPath = '/{scope_id}/pixelmaps/{pixelmap_id}/level/{level}/position/start/{start_x}/{start_y}/end/{end_x}/{end_y}/data';

  /**
   * Bulk tile retrieval for a pixelmap.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `scopeIdPixelmapsPixelmapIdLevelLevelPositionStartStartXStartYEndEndXEndYDataGet$Json()` instead.
   *
   * This method doesn't expect any request body.
   */
  scopeIdPixelmapsPixelmapIdLevelLevelPositionStartStartXStartYEndEndXEndYDataGet$Json$Response(params: ScopeIdPixelmapsPixelmapIdLevelLevelPositionStartStartXStartYEndEndXEndYDataGet$Json$Params, context?: HttpContext): Observable<StrictHttpResponse<any>> {
    return scopeIdPixelmapsPixelmapIdLevelLevelPositionStartStartXStartYEndEndXEndYDataGet$Json(this.http, this.rootUrl, params, context);
  }

  /**
   * Bulk tile retrieval for a pixelmap.
   *
   *
   *
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `scopeIdPixelmapsPixelmapIdLevelLevelPositionStartStartXStartYEndEndXEndYDataGet$Json$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  scopeIdPixelmapsPixelmapIdLevelLevelPositionStartStartXStartYEndEndXEndYDataGet$Json(params: ScopeIdPixelmapsPixelmapIdLevelLevelPositionStartStartXStartYEndEndXEndYDataGet$Json$Params, context?: HttpContext): Observable<any> {
    return this.scopeIdPixelmapsPixelmapIdLevelLevelPositionStartStartXStartYEndEndXEndYDataGet$Json$Response(params, context).pipe(
      map((r: StrictHttpResponse<any>): any => r.body)
    );
  }

  /**
   * Bulk tile retrieval for a pixelmap.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `scopeIdPixelmapsPixelmapIdLevelLevelPositionStartStartXStartYEndEndXEndYDataGet$Any()` instead.
   *
   * This method doesn't expect any request body.
   */
  scopeIdPixelmapsPixelmapIdLevelLevelPositionStartStartXStartYEndEndXEndYDataGet$Any$Response(params: ScopeIdPixelmapsPixelmapIdLevelLevelPositionStartStartXStartYEndEndXEndYDataGet$Any$Params, context?: HttpContext): Observable<StrictHttpResponse<Blob>> {
    return scopeIdPixelmapsPixelmapIdLevelLevelPositionStartStartXStartYEndEndXEndYDataGet$Any(this.http, this.rootUrl, params, context);
  }

  /**
   * Bulk tile retrieval for a pixelmap.
   *
   *
   *
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `scopeIdPixelmapsPixelmapIdLevelLevelPositionStartStartXStartYEndEndXEndYDataGet$Any$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  scopeIdPixelmapsPixelmapIdLevelLevelPositionStartStartXStartYEndEndXEndYDataGet$Any(params: ScopeIdPixelmapsPixelmapIdLevelLevelPositionStartStartXStartYEndEndXEndYDataGet$Any$Params, context?: HttpContext): Observable<Blob> {
    return this.scopeIdPixelmapsPixelmapIdLevelLevelPositionStartStartXStartYEndEndXEndYDataGet$Any$Response(params, context).pipe(
      map((r: StrictHttpResponse<Blob>): Blob => r.body)
    );
  }

  /** Path part for operation `scopeIdPixelmapsPixelmapIdLevelLevelPositionStartStartXStartYEndEndXEndYDataPut()` */
  static readonly ScopeIdPixelmapsPixelmapIdLevelLevelPositionStartStartXStartYEndEndXEndYDataPutPath = '/{scope_id}/pixelmaps/{pixelmap_id}/level/{level}/position/start/{start_x}/{start_y}/end/{end_x}/{end_y}/data';

  /**
   * Bulk tile upload for a pixelmap.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `scopeIdPixelmapsPixelmapIdLevelLevelPositionStartStartXStartYEndEndXEndYDataPut()` instead.
   *
   * This method doesn't expect any request body.
   */
  scopeIdPixelmapsPixelmapIdLevelLevelPositionStartStartXStartYEndEndXEndYDataPut$Response(params: ScopeIdPixelmapsPixelmapIdLevelLevelPositionStartStartXStartYEndEndXEndYDataPut$Params, context?: HttpContext): Observable<StrictHttpResponse<void>> {
    return scopeIdPixelmapsPixelmapIdLevelLevelPositionStartStartXStartYEndEndXEndYDataPut(this.http, this.rootUrl, params, context);
  }

  /**
   * Bulk tile upload for a pixelmap.
   *
   *
   *
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `scopeIdPixelmapsPixelmapIdLevelLevelPositionStartStartXStartYEndEndXEndYDataPut$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  scopeIdPixelmapsPixelmapIdLevelLevelPositionStartStartXStartYEndEndXEndYDataPut(params: ScopeIdPixelmapsPixelmapIdLevelLevelPositionStartStartXStartYEndEndXEndYDataPut$Params, context?: HttpContext): Observable<void> {
    return this.scopeIdPixelmapsPixelmapIdLevelLevelPositionStartStartXStartYEndEndXEndYDataPut$Response(params, context).pipe(
      map((r: StrictHttpResponse<void>): void => r.body)
    );
  }

}
