/* tslint:disable */
/* eslint-disable */
import { HttpClient, HttpContext } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { BaseService } from '../base-service';
import { ApiConfiguration } from '../api-configuration';
import { StrictHttpResponse } from '../strict-http-response';

import { Case } from '../models/case';
import { CaseList } from '../models/case-list';
import { casesCaseIdGet } from '../fn/cases/cases-case-id-get';
import { CasesCaseIdGet$Params } from '../fn/cases/cases-case-id-get';
import { casesCaseIdSlidesGet } from '../fn/cases/cases-case-id-slides-get';
import { CasesCaseIdSlidesGet$Params } from '../fn/cases/cases-case-id-slides-get';
import { casesGet } from '../fn/cases/cases-get';
import { CasesGet$Params } from '../fn/cases/cases-get';
import { SlideList } from '../models/slide-list';

@Injectable({ providedIn: 'root' })
export class CasesService extends BaseService {
  constructor(config: ApiConfiguration, http: HttpClient) {
    super(config, http);
  }

  /** Path part for operation `casesGet()` */
  static readonly CasesGetPath = '/cases';

  /**
   * .
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `casesGet()` instead.
   *
   * This method doesn't expect any request body.
   */
  casesGet$Response(params?: CasesGet$Params, context?: HttpContext): Observable<StrictHttpResponse<CaseList>> {
    return casesGet(this.http, this.rootUrl, params, context);
  }

  /**
   * .
   *
   *
   *
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `casesGet$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  casesGet(params?: CasesGet$Params, context?: HttpContext): Observable<CaseList> {
    return this.casesGet$Response(params, context).pipe(
      map((r: StrictHttpResponse<CaseList>): CaseList => r.body)
    );
  }

  /** Path part for operation `casesCaseIdGet()` */
  static readonly CasesCaseIdGetPath = '/cases/{case_id}';

  /**
   * .
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `casesCaseIdGet()` instead.
   *
   * This method doesn't expect any request body.
   */
  casesCaseIdGet$Response(params: CasesCaseIdGet$Params, context?: HttpContext): Observable<StrictHttpResponse<Case>> {
    return casesCaseIdGet(this.http, this.rootUrl, params, context);
  }

  /**
   * .
   *
   *
   *
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `casesCaseIdGet$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  casesCaseIdGet(params: CasesCaseIdGet$Params, context?: HttpContext): Observable<Case> {
    return this.casesCaseIdGet$Response(params, context).pipe(
      map((r: StrictHttpResponse<Case>): Case => r.body)
    );
  }

  /** Path part for operation `casesCaseIdSlidesGet()` */
  static readonly CasesCaseIdSlidesGetPath = '/cases/{case_id}/slides';

  /**
   * .
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `casesCaseIdSlidesGet()` instead.
   *
   * This method doesn't expect any request body.
   */
  casesCaseIdSlidesGet$Response(params: CasesCaseIdSlidesGet$Params, context?: HttpContext): Observable<StrictHttpResponse<SlideList>> {
    return casesCaseIdSlidesGet(this.http, this.rootUrl, params, context);
  }

  /**
   * .
   *
   *
   *
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `casesCaseIdSlidesGet$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  casesCaseIdSlidesGet(params: CasesCaseIdSlidesGet$Params, context?: HttpContext): Observable<SlideList> {
    return this.casesCaseIdSlidesGet$Response(params, context).pipe(
      map((r: StrictHttpResponse<SlideList>): SlideList => r.body)
    );
  }

}
