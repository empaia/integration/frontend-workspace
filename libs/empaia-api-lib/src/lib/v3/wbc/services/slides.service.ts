/* tslint:disable */
/* eslint-disable */
import { HttpClient, HttpContext } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { BaseService } from '../base-service';
import { ApiConfiguration } from '../api-configuration';
import { StrictHttpResponse } from '../strict-http-response';

import { SlideInfo } from '../models/slide-info';
import { slidesSlideIdInfoGet } from '../fn/slides/slides-slide-id-info-get';
import { SlidesSlideIdInfoGet$Params } from '../fn/slides/slides-slide-id-info-get';
import { slidesSlideIdLabelMaxSizeMaxXMaxYGet } from '../fn/slides/slides-slide-id-label-max-size-max-x-max-y-get';
import { SlidesSlideIdLabelMaxSizeMaxXMaxYGet$Params } from '../fn/slides/slides-slide-id-label-max-size-max-x-max-y-get';
import { slidesSlideIdMacroMaxSizeMaxXMaxYGet } from '../fn/slides/slides-slide-id-macro-max-size-max-x-max-y-get';
import { SlidesSlideIdMacroMaxSizeMaxXMaxYGet$Params } from '../fn/slides/slides-slide-id-macro-max-size-max-x-max-y-get';
import { slidesSlideIdThumbnailMaxSizeMaxXMaxYGet } from '../fn/slides/slides-slide-id-thumbnail-max-size-max-x-max-y-get';
import { SlidesSlideIdThumbnailMaxSizeMaxXMaxYGet$Params } from '../fn/slides/slides-slide-id-thumbnail-max-size-max-x-max-y-get';
import { slidesSlideIdTileLevelLevelTileTileXTileYGet } from '../fn/slides/slides-slide-id-tile-level-level-tile-tile-x-tile-y-get';
import { SlidesSlideIdTileLevelLevelTileTileXTileYGet$Params } from '../fn/slides/slides-slide-id-tile-level-level-tile-tile-x-tile-y-get';

@Injectable({ providedIn: 'root' })
export class SlidesService extends BaseService {
  constructor(config: ApiConfiguration, http: HttpClient) {
    super(config, http);
  }

  /** Path part for operation `slidesSlideIdInfoGet()` */
  static readonly SlidesSlideIdInfoGetPath = '/slides/{slide_id}/info';

  /**
   * .
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `slidesSlideIdInfoGet()` instead.
   *
   * This method doesn't expect any request body.
   */
  slidesSlideIdInfoGet$Response(params: SlidesSlideIdInfoGet$Params, context?: HttpContext): Observable<StrictHttpResponse<SlideInfo>> {
    return slidesSlideIdInfoGet(this.http, this.rootUrl, params, context);
  }

  /**
   * .
   *
   *
   *
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `slidesSlideIdInfoGet$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  slidesSlideIdInfoGet(params: SlidesSlideIdInfoGet$Params, context?: HttpContext): Observable<SlideInfo> {
    return this.slidesSlideIdInfoGet$Response(params, context).pipe(
      map((r: StrictHttpResponse<SlideInfo>): SlideInfo => r.body)
    );
  }

  /** Path part for operation `slidesSlideIdTileLevelLevelTileTileXTileYGet()` */
  static readonly SlidesSlideIdTileLevelLevelTileTileXTileYGetPath = '/slides/{slide_id}/tile/level/{level}/tile/{tile_x}/{tile_y}';

  /**
   * .
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `slidesSlideIdTileLevelLevelTileTileXTileYGet()` instead.
   *
   * This method doesn't expect any request body.
   */
  slidesSlideIdTileLevelLevelTileTileXTileYGet$Response(params: SlidesSlideIdTileLevelLevelTileTileXTileYGet$Params, context?: HttpContext): Observable<StrictHttpResponse<Blob>> {
    return slidesSlideIdTileLevelLevelTileTileXTileYGet(this.http, this.rootUrl, params, context);
  }

  /**
   * .
   *
   *
   *
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `slidesSlideIdTileLevelLevelTileTileXTileYGet$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  slidesSlideIdTileLevelLevelTileTileXTileYGet(params: SlidesSlideIdTileLevelLevelTileTileXTileYGet$Params, context?: HttpContext): Observable<Blob> {
    return this.slidesSlideIdTileLevelLevelTileTileXTileYGet$Response(params, context).pipe(
      map((r: StrictHttpResponse<Blob>): Blob => r.body)
    );
  }

  /** Path part for operation `slidesSlideIdMacroMaxSizeMaxXMaxYGet()` */
  static readonly SlidesSlideIdMacroMaxSizeMaxXMaxYGetPath = '/slides/{slide_id}/macro/max_size/{max_x}/{max_y}';

  /**
   * .
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `slidesSlideIdMacroMaxSizeMaxXMaxYGet()` instead.
   *
   * This method doesn't expect any request body.
   */
  slidesSlideIdMacroMaxSizeMaxXMaxYGet$Response(params: SlidesSlideIdMacroMaxSizeMaxXMaxYGet$Params, context?: HttpContext): Observable<StrictHttpResponse<Blob>> {
    return slidesSlideIdMacroMaxSizeMaxXMaxYGet(this.http, this.rootUrl, params, context);
  }

  /**
   * .
   *
   *
   *
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `slidesSlideIdMacroMaxSizeMaxXMaxYGet$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  slidesSlideIdMacroMaxSizeMaxXMaxYGet(params: SlidesSlideIdMacroMaxSizeMaxXMaxYGet$Params, context?: HttpContext): Observable<Blob> {
    return this.slidesSlideIdMacroMaxSizeMaxXMaxYGet$Response(params, context).pipe(
      map((r: StrictHttpResponse<Blob>): Blob => r.body)
    );
  }

  /** Path part for operation `slidesSlideIdLabelMaxSizeMaxXMaxYGet()` */
  static readonly SlidesSlideIdLabelMaxSizeMaxXMaxYGetPath = '/slides/{slide_id}/label/max_size/{max_x}/{max_y}';

  /**
   * .
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `slidesSlideIdLabelMaxSizeMaxXMaxYGet()` instead.
   *
   * This method doesn't expect any request body.
   */
  slidesSlideIdLabelMaxSizeMaxXMaxYGet$Response(params: SlidesSlideIdLabelMaxSizeMaxXMaxYGet$Params, context?: HttpContext): Observable<StrictHttpResponse<Blob>> {
    return slidesSlideIdLabelMaxSizeMaxXMaxYGet(this.http, this.rootUrl, params, context);
  }

  /**
   * .
   *
   *
   *
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `slidesSlideIdLabelMaxSizeMaxXMaxYGet$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  slidesSlideIdLabelMaxSizeMaxXMaxYGet(params: SlidesSlideIdLabelMaxSizeMaxXMaxYGet$Params, context?: HttpContext): Observable<Blob> {
    return this.slidesSlideIdLabelMaxSizeMaxXMaxYGet$Response(params, context).pipe(
      map((r: StrictHttpResponse<Blob>): Blob => r.body)
    );
  }

  /** Path part for operation `slidesSlideIdThumbnailMaxSizeMaxXMaxYGet()` */
  static readonly SlidesSlideIdThumbnailMaxSizeMaxXMaxYGetPath = '/slides/{slide_id}/thumbnail/max_size/{max_x}/{max_y}';

  /**
   * .
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `slidesSlideIdThumbnailMaxSizeMaxXMaxYGet()` instead.
   *
   * This method doesn't expect any request body.
   */
  slidesSlideIdThumbnailMaxSizeMaxXMaxYGet$Response(params: SlidesSlideIdThumbnailMaxSizeMaxXMaxYGet$Params, context?: HttpContext): Observable<StrictHttpResponse<Blob>> {
    return slidesSlideIdThumbnailMaxSizeMaxXMaxYGet(this.http, this.rootUrl, params, context);
  }

  /**
   * .
   *
   *
   *
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `slidesSlideIdThumbnailMaxSizeMaxXMaxYGet$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  slidesSlideIdThumbnailMaxSizeMaxXMaxYGet(params: SlidesSlideIdThumbnailMaxSizeMaxXMaxYGet$Params, context?: HttpContext): Observable<Blob> {
    return this.slidesSlideIdThumbnailMaxSizeMaxXMaxYGet$Response(params, context).pipe(
      map((r: StrictHttpResponse<Blob>): Blob => r.body)
    );
  }

}
