/* tslint:disable */
/* eslint-disable */
import { HttpClient, HttpContext } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { BaseService } from '../base-service';
import { ApiConfiguration } from '../api-configuration';
import { StrictHttpResponse } from '../strict-http-response';

import { AppList } from '../models/app-list';
import { appsQueryPut } from '../fn/apps/apps-query-put';
import { AppsQueryPut$Params } from '../fn/apps/apps-query-put';

@Injectable({ providedIn: 'root' })
export class AppsService extends BaseService {
  constructor(config: ApiConfiguration, http: HttpClient) {
    super(config, http);
  }

  /** Path part for operation `appsQueryPut()` */
  static readonly AppsQueryPutPath = '/apps/query';

  /**
   * .
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `appsQueryPut()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  appsQueryPut$Response(params: AppsQueryPut$Params, context?: HttpContext): Observable<StrictHttpResponse<AppList>> {
    return appsQueryPut(this.http, this.rootUrl, params, context);
  }

  /**
   * .
   *
   *
   *
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `appsQueryPut$Response()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  appsQueryPut(params: AppsQueryPut$Params, context?: HttpContext): Observable<AppList> {
    return this.appsQueryPut$Response(params, context).pipe(
      map((r: StrictHttpResponse<AppList>): AppList => r.body)
    );
  }

}
