/* tslint:disable */
/* eslint-disable */
import { HttpClient, HttpContext } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { BaseService } from '../base-service';
import { ApiConfiguration } from '../api-configuration';
import { StrictHttpResponse } from '../strict-http-response';

import { AppUiConfiguration } from '../models/app-ui-configuration';
import { ExaminationList } from '../models/examination-list';
import { examinationsExaminationIdAppUiConfigGet } from '../fn/examinations/examinations-examination-id-app-ui-config-get';
import { ExaminationsExaminationIdAppUiConfigGet$Params } from '../fn/examinations/examinations-examination-id-app-ui-config-get';
import { examinationsExaminationIdClosePut } from '../fn/examinations/examinations-examination-id-close-put';
import { ExaminationsExaminationIdClosePut$Params } from '../fn/examinations/examinations-examination-id-close-put';
import { examinationsExaminationIdFrontendTokenGet } from '../fn/examinations/examinations-examination-id-frontend-token-get';
import { ExaminationsExaminationIdFrontendTokenGet$Params } from '../fn/examinations/examinations-examination-id-frontend-token-get';
import { examinationsExaminationIdGet } from '../fn/examinations/examinations-examination-id-get';
import { ExaminationsExaminationIdGet$Params } from '../fn/examinations/examinations-examination-id-get';
import { examinationsExaminationIdScopePut } from '../fn/examinations/examinations-examination-id-scope-put';
import { ExaminationsExaminationIdScopePut$Params } from '../fn/examinations/examinations-examination-id-scope-put';
import { examinationsPut } from '../fn/examinations/examinations-put';
import { ExaminationsPut$Params } from '../fn/examinations/examinations-put';
import { examinationsQueryPut } from '../fn/examinations/examinations-query-put';
import { ExaminationsQueryPut$Params } from '../fn/examinations/examinations-query-put';
import { FrontendToken } from '../models/frontend-token';
import { ScopeTokenAndScopeId } from '../models/scope-token-and-scope-id';
import { WorkbenchServiceApiV3CustomModelsExaminationsExamination } from '../models/workbench-service-api-v-3-custom-models-examinations-examination';

@Injectable({ providedIn: 'root' })
export class ExaminationsService extends BaseService {
  constructor(config: ApiConfiguration, http: HttpClient) {
    super(config, http);
  }

  /** Path part for operation `examinationsExaminationIdGet()` */
  static readonly ExaminationsExaminationIdGetPath = '/examinations/{examination_id}';

  /**
   * .
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `examinationsExaminationIdGet()` instead.
   *
   * This method doesn't expect any request body.
   */
  examinationsExaminationIdGet$Response(params: ExaminationsExaminationIdGet$Params, context?: HttpContext): Observable<StrictHttpResponse<WorkbenchServiceApiV3CustomModelsExaminationsExamination>> {
    return examinationsExaminationIdGet(this.http, this.rootUrl, params, context);
  }

  /**
   * .
   *
   *
   *
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `examinationsExaminationIdGet$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  examinationsExaminationIdGet(params: ExaminationsExaminationIdGet$Params, context?: HttpContext): Observable<WorkbenchServiceApiV3CustomModelsExaminationsExamination> {
    return this.examinationsExaminationIdGet$Response(params, context).pipe(
      map((r: StrictHttpResponse<WorkbenchServiceApiV3CustomModelsExaminationsExamination>): WorkbenchServiceApiV3CustomModelsExaminationsExamination => r.body)
    );
  }

  /** Path part for operation `examinationsExaminationIdClosePut()` */
  static readonly ExaminationsExaminationIdClosePutPath = '/examinations/{examination_id}/close';

  /**
   * .
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `examinationsExaminationIdClosePut()` instead.
   *
   * This method doesn't expect any request body.
   */
  examinationsExaminationIdClosePut$Response(params: ExaminationsExaminationIdClosePut$Params, context?: HttpContext): Observable<StrictHttpResponse<WorkbenchServiceApiV3CustomModelsExaminationsExamination>> {
    return examinationsExaminationIdClosePut(this.http, this.rootUrl, params, context);
  }

  /**
   * .
   *
   *
   *
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `examinationsExaminationIdClosePut$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  examinationsExaminationIdClosePut(params: ExaminationsExaminationIdClosePut$Params, context?: HttpContext): Observable<WorkbenchServiceApiV3CustomModelsExaminationsExamination> {
    return this.examinationsExaminationIdClosePut$Response(params, context).pipe(
      map((r: StrictHttpResponse<WorkbenchServiceApiV3CustomModelsExaminationsExamination>): WorkbenchServiceApiV3CustomModelsExaminationsExamination => r.body)
    );
  }

  /** Path part for operation `examinationsExaminationIdFrontendTokenGet()` */
  static readonly ExaminationsExaminationIdFrontendTokenGetPath = '/examinations/{examination_id}/frontend-token';

  /**
   * .
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `examinationsExaminationIdFrontendTokenGet()` instead.
   *
   * This method doesn't expect any request body.
   */
  examinationsExaminationIdFrontendTokenGet$Response(params: ExaminationsExaminationIdFrontendTokenGet$Params, context?: HttpContext): Observable<StrictHttpResponse<FrontendToken>> {
    return examinationsExaminationIdFrontendTokenGet(this.http, this.rootUrl, params, context);
  }

  /**
   * .
   *
   *
   *
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `examinationsExaminationIdFrontendTokenGet$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  examinationsExaminationIdFrontendTokenGet(params: ExaminationsExaminationIdFrontendTokenGet$Params, context?: HttpContext): Observable<FrontendToken> {
    return this.examinationsExaminationIdFrontendTokenGet$Response(params, context).pipe(
      map((r: StrictHttpResponse<FrontendToken>): FrontendToken => r.body)
    );
  }

  /** Path part for operation `examinationsExaminationIdAppUiConfigGet()` */
  static readonly ExaminationsExaminationIdAppUiConfigGetPath = '/examinations/{examination_id}/app-ui-config';

  /**
   * .
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `examinationsExaminationIdAppUiConfigGet()` instead.
   *
   * This method doesn't expect any request body.
   */
  examinationsExaminationIdAppUiConfigGet$Response(params: ExaminationsExaminationIdAppUiConfigGet$Params, context?: HttpContext): Observable<StrictHttpResponse<AppUiConfiguration>> {
    return examinationsExaminationIdAppUiConfigGet(this.http, this.rootUrl, params, context);
  }

  /**
   * .
   *
   *
   *
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `examinationsExaminationIdAppUiConfigGet$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  examinationsExaminationIdAppUiConfigGet(params: ExaminationsExaminationIdAppUiConfigGet$Params, context?: HttpContext): Observable<AppUiConfiguration> {
    return this.examinationsExaminationIdAppUiConfigGet$Response(params, context).pipe(
      map((r: StrictHttpResponse<AppUiConfiguration>): AppUiConfiguration => r.body)
    );
  }

  /** Path part for operation `examinationsExaminationIdScopePut()` */
  static readonly ExaminationsExaminationIdScopePutPath = '/examinations/{examination_id}/scope';

  /**
   * .
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `examinationsExaminationIdScopePut()` instead.
   *
   * This method doesn't expect any request body.
   */
  examinationsExaminationIdScopePut$Response(params: ExaminationsExaminationIdScopePut$Params, context?: HttpContext): Observable<StrictHttpResponse<ScopeTokenAndScopeId>> {
    return examinationsExaminationIdScopePut(this.http, this.rootUrl, params, context);
  }

  /**
   * .
   *
   *
   *
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `examinationsExaminationIdScopePut$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  examinationsExaminationIdScopePut(params: ExaminationsExaminationIdScopePut$Params, context?: HttpContext): Observable<ScopeTokenAndScopeId> {
    return this.examinationsExaminationIdScopePut$Response(params, context).pipe(
      map((r: StrictHttpResponse<ScopeTokenAndScopeId>): ScopeTokenAndScopeId => r.body)
    );
  }

  /** Path part for operation `examinationsQueryPut()` */
  static readonly ExaminationsQueryPutPath = '/examinations/query';

  /**
   * .
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `examinationsQueryPut()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  examinationsQueryPut$Response(params: ExaminationsQueryPut$Params, context?: HttpContext): Observable<StrictHttpResponse<ExaminationList>> {
    return examinationsQueryPut(this.http, this.rootUrl, params, context);
  }

  /**
   * .
   *
   *
   *
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `examinationsQueryPut$Response()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  examinationsQueryPut(params: ExaminationsQueryPut$Params, context?: HttpContext): Observable<ExaminationList> {
    return this.examinationsQueryPut$Response(params, context).pipe(
      map((r: StrictHttpResponse<ExaminationList>): ExaminationList => r.body)
    );
  }

  /** Path part for operation `examinationsPut()` */
  static readonly ExaminationsPutPath = '/examinations';

  /**
   * .
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `examinationsPut()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  examinationsPut$Response(params: ExaminationsPut$Params, context?: HttpContext): Observable<StrictHttpResponse<WorkbenchServiceApiV3CustomModelsExaminationsExamination>> {
    return examinationsPut(this.http, this.rootUrl, params, context);
  }

  /**
   * .
   *
   *
   *
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `examinationsPut$Response()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  examinationsPut(params: ExaminationsPut$Params, context?: HttpContext): Observable<WorkbenchServiceApiV3CustomModelsExaminationsExamination> {
    return this.examinationsPut$Response(params, context).pipe(
      map((r: StrictHttpResponse<WorkbenchServiceApiV3CustomModelsExaminationsExamination>): WorkbenchServiceApiV3CustomModelsExaminationsExamination => r.body)
    );
  }

}
