/* tslint:disable */
/* eslint-disable */
import { HttpClient, HttpContext } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { BaseService } from '../base-service';
import { ApiConfiguration } from '../api-configuration';
import { StrictHttpResponse } from '../strict-http-response';

import { TagListOutput } from '../models/tag-list-output';
import { tagsGet } from '../fn/tags/tags-get';
import { TagsGet$Params } from '../fn/tags/tags-get';

@Injectable({ providedIn: 'root' })
export class TagsService extends BaseService {
  constructor(config: ApiConfiguration, http: HttpClient) {
    super(config, http);
  }

  /** Path part for operation `tagsGet()` */
  static readonly TagsGetPath = '/tags';

  /**
   * .
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `tagsGet()` instead.
   *
   * This method doesn't expect any request body.
   */
  tagsGet$Response(params?: TagsGet$Params, context?: HttpContext): Observable<StrictHttpResponse<TagListOutput>> {
    return tagsGet(this.http, this.rootUrl, params, context);
  }

  /**
   * .
   *
   *
   *
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `tagsGet$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  tagsGet(params?: TagsGet$Params, context?: HttpContext): Observable<TagListOutput> {
    return this.tagsGet$Response(params, context).pipe(
      map((r: StrictHttpResponse<TagListOutput>): TagListOutput => r.body)
    );
  }

}
