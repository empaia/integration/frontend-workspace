/* tslint:disable */
/* eslint-disable */
import { HttpClient, HttpContext } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { BaseService } from '../base-service';
import { ApiConfiguration } from '../api-configuration';
import { StrictHttpResponse } from '../strict-http-response';

import { configurationPreprocessingTriggersGet } from '../fn/configuration/configuration-preprocessing-triggers-get';
import { ConfigurationPreprocessingTriggersGet$Params } from '../fn/configuration/configuration-preprocessing-triggers-get';
import { configurationPreprocessingTriggersPost } from '../fn/configuration/configuration-preprocessing-triggers-post';
import { ConfigurationPreprocessingTriggersPost$Params } from '../fn/configuration/configuration-preprocessing-triggers-post';
import { configurationPreprocessingTriggersPreprocessingTriggerIdDelete } from '../fn/configuration/configuration-preprocessing-triggers-preprocessing-trigger-id-delete';
import { ConfigurationPreprocessingTriggersPreprocessingTriggerIdDelete$Params } from '../fn/configuration/configuration-preprocessing-triggers-preprocessing-trigger-id-delete';
import { configurationPreprocessingTriggersPreprocessingTriggerIdGet } from '../fn/configuration/configuration-preprocessing-triggers-preprocessing-trigger-id-get';
import { ConfigurationPreprocessingTriggersPreprocessingTriggerIdGet$Params } from '../fn/configuration/configuration-preprocessing-triggers-preprocessing-trigger-id-get';
import { ExtendedPreprocessingTrigger } from '../models/extended-preprocessing-trigger';
import { ExtendedPreprocessingTriggerList } from '../models/extended-preprocessing-trigger-list';

@Injectable({ providedIn: 'root' })
export class ConfigurationService extends BaseService {
  constructor(config: ApiConfiguration, http: HttpClient) {
    super(config, http);
  }

  /** Path part for operation `configurationPreprocessingTriggersGet()` */
  static readonly ConfigurationPreprocessingTriggersGetPath = '/configuration/preprocessing-triggers';

  /**
   * .
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `configurationPreprocessingTriggersGet()` instead.
   *
   * This method doesn't expect any request body.
   */
  configurationPreprocessingTriggersGet$Response(params?: ConfigurationPreprocessingTriggersGet$Params, context?: HttpContext): Observable<StrictHttpResponse<ExtendedPreprocessingTriggerList>> {
    return configurationPreprocessingTriggersGet(this.http, this.rootUrl, params, context);
  }

  /**
   * .
   *
   *
   *
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `configurationPreprocessingTriggersGet$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  configurationPreprocessingTriggersGet(params?: ConfigurationPreprocessingTriggersGet$Params, context?: HttpContext): Observable<ExtendedPreprocessingTriggerList> {
    return this.configurationPreprocessingTriggersGet$Response(params, context).pipe(
      map((r: StrictHttpResponse<ExtendedPreprocessingTriggerList>): ExtendedPreprocessingTriggerList => r.body)
    );
  }

  /** Path part for operation `configurationPreprocessingTriggersPost()` */
  static readonly ConfigurationPreprocessingTriggersPostPath = '/configuration/preprocessing-triggers';

  /**
   * .
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `configurationPreprocessingTriggersPost()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  configurationPreprocessingTriggersPost$Response(params: ConfigurationPreprocessingTriggersPost$Params, context?: HttpContext): Observable<StrictHttpResponse<ExtendedPreprocessingTrigger>> {
    return configurationPreprocessingTriggersPost(this.http, this.rootUrl, params, context);
  }

  /**
   * .
   *
   *
   *
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `configurationPreprocessingTriggersPost$Response()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  configurationPreprocessingTriggersPost(params: ConfigurationPreprocessingTriggersPost$Params, context?: HttpContext): Observable<ExtendedPreprocessingTrigger> {
    return this.configurationPreprocessingTriggersPost$Response(params, context).pipe(
      map((r: StrictHttpResponse<ExtendedPreprocessingTrigger>): ExtendedPreprocessingTrigger => r.body)
    );
  }

  /** Path part for operation `configurationPreprocessingTriggersPreprocessingTriggerIdGet()` */
  static readonly ConfigurationPreprocessingTriggersPreprocessingTriggerIdGetPath = '/configuration/preprocessing-triggers/{preprocessing_trigger_id}';

  /**
   * .
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `configurationPreprocessingTriggersPreprocessingTriggerIdGet()` instead.
   *
   * This method doesn't expect any request body.
   */
  configurationPreprocessingTriggersPreprocessingTriggerIdGet$Response(params: ConfigurationPreprocessingTriggersPreprocessingTriggerIdGet$Params, context?: HttpContext): Observable<StrictHttpResponse<ExtendedPreprocessingTrigger>> {
    return configurationPreprocessingTriggersPreprocessingTriggerIdGet(this.http, this.rootUrl, params, context);
  }

  /**
   * .
   *
   *
   *
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `configurationPreprocessingTriggersPreprocessingTriggerIdGet$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  configurationPreprocessingTriggersPreprocessingTriggerIdGet(params: ConfigurationPreprocessingTriggersPreprocessingTriggerIdGet$Params, context?: HttpContext): Observable<ExtendedPreprocessingTrigger> {
    return this.configurationPreprocessingTriggersPreprocessingTriggerIdGet$Response(params, context).pipe(
      map((r: StrictHttpResponse<ExtendedPreprocessingTrigger>): ExtendedPreprocessingTrigger => r.body)
    );
  }

  /** Path part for operation `configurationPreprocessingTriggersPreprocessingTriggerIdDelete()` */
  static readonly ConfigurationPreprocessingTriggersPreprocessingTriggerIdDeletePath = '/configuration/preprocessing-triggers/{preprocessing_trigger_id}';

  /**
   * .
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `configurationPreprocessingTriggersPreprocessingTriggerIdDelete()` instead.
   *
   * This method doesn't expect any request body.
   */
  configurationPreprocessingTriggersPreprocessingTriggerIdDelete$Response(params: ConfigurationPreprocessingTriggersPreprocessingTriggerIdDelete$Params, context?: HttpContext): Observable<StrictHttpResponse<ExtendedPreprocessingTrigger>> {
    return configurationPreprocessingTriggersPreprocessingTriggerIdDelete(this.http, this.rootUrl, params, context);
  }

  /**
   * .
   *
   *
   *
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `configurationPreprocessingTriggersPreprocessingTriggerIdDelete$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  configurationPreprocessingTriggersPreprocessingTriggerIdDelete(params: ConfigurationPreprocessingTriggersPreprocessingTriggerIdDelete$Params, context?: HttpContext): Observable<ExtendedPreprocessingTrigger> {
    return this.configurationPreprocessingTriggersPreprocessingTriggerIdDelete$Response(params, context).pipe(
      map((r: StrictHttpResponse<ExtendedPreprocessingTrigger>): ExtendedPreprocessingTrigger => r.body)
    );
  }

}
