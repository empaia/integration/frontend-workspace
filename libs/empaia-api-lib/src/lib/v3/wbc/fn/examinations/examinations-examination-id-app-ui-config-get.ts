/* tslint:disable */
/* eslint-disable */
import { HttpClient, HttpContext, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { StrictHttpResponse } from '../../strict-http-response';
import { RequestBuilder } from '../../request-builder';

import { AppUiConfiguration } from '../../models/app-ui-configuration';

export interface ExaminationsExaminationIdAppUiConfigGet$Params {

/**
 * Examination ID
 */
  examination_id: string;
}

export function examinationsExaminationIdAppUiConfigGet(http: HttpClient, rootUrl: string, params: ExaminationsExaminationIdAppUiConfigGet$Params, context?: HttpContext): Observable<StrictHttpResponse<AppUiConfiguration>> {
  const rb = new RequestBuilder(rootUrl, examinationsExaminationIdAppUiConfigGet.PATH, 'get');
  if (params) {
    rb.path('examination_id', params.examination_id, {});
  }

  return http.request(
    rb.build({ responseType: 'json', accept: 'application/json', context })
  ).pipe(
    filter((r: any): r is HttpResponse<any> => r instanceof HttpResponse),
    map((r: HttpResponse<any>) => {
      return r as StrictHttpResponse<AppUiConfiguration>;
    })
  );
}

examinationsExaminationIdAppUiConfigGet.PATH = '/examinations/{examination_id}/app-ui-config';
