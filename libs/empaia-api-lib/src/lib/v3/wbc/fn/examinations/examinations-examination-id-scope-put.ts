/* tslint:disable */
/* eslint-disable */
import { HttpClient, HttpContext, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { StrictHttpResponse } from '../../strict-http-response';
import { RequestBuilder } from '../../request-builder';

import { ScopeTokenAndScopeId } from '../../models/scope-token-and-scope-id';

export interface ExaminationsExaminationIdScopePut$Params {

/**
 * Examination ID
 */
  examination_id: string;
}

export function examinationsExaminationIdScopePut(http: HttpClient, rootUrl: string, params: ExaminationsExaminationIdScopePut$Params, context?: HttpContext): Observable<StrictHttpResponse<ScopeTokenAndScopeId>> {
  const rb = new RequestBuilder(rootUrl, examinationsExaminationIdScopePut.PATH, 'put');
  if (params) {
    rb.path('examination_id', params.examination_id, {});
  }

  return http.request(
    rb.build({ responseType: 'json', accept: 'application/json', context })
  ).pipe(
    filter((r: any): r is HttpResponse<any> => r instanceof HttpResponse),
    map((r: HttpResponse<any>) => {
      return r as StrictHttpResponse<ScopeTokenAndScopeId>;
    })
  );
}

examinationsExaminationIdScopePut.PATH = '/examinations/{examination_id}/scope';
