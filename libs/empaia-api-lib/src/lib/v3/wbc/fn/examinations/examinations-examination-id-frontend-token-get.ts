/* tslint:disable */
/* eslint-disable */
import { HttpClient, HttpContext, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { StrictHttpResponse } from '../../strict-http-response';
import { RequestBuilder } from '../../request-builder';

import { FrontendToken } from '../../models/frontend-token';

export interface ExaminationsExaminationIdFrontendTokenGet$Params {

/**
 * Examination ID
 */
  examination_id: string;
}

export function examinationsExaminationIdFrontendTokenGet(http: HttpClient, rootUrl: string, params: ExaminationsExaminationIdFrontendTokenGet$Params, context?: HttpContext): Observable<StrictHttpResponse<FrontendToken>> {
  const rb = new RequestBuilder(rootUrl, examinationsExaminationIdFrontendTokenGet.PATH, 'get');
  if (params) {
    rb.path('examination_id', params.examination_id, {});
  }

  return http.request(
    rb.build({ responseType: 'json', accept: 'application/json', context })
  ).pipe(
    filter((r: any): r is HttpResponse<any> => r instanceof HttpResponse),
    map((r: HttpResponse<any>) => {
      return r as StrictHttpResponse<FrontendToken>;
    })
  );
}

examinationsExaminationIdFrontendTokenGet.PATH = '/examinations/{examination_id}/frontend-token';
