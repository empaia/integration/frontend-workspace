/* tslint:disable */
/* eslint-disable */
import { HttpClient, HttpContext, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { StrictHttpResponse } from '../../strict-http-response';
import { RequestBuilder } from '../../request-builder';

import { PostExamination } from '../../models/post-examination';
import { WorkbenchServiceApiV3CustomModelsExaminationsExamination } from '../../models/workbench-service-api-v-3-custom-models-examinations-examination';

export interface ExaminationsPut$Params {
      body: PostExamination
}

export function examinationsPut(http: HttpClient, rootUrl: string, params: ExaminationsPut$Params, context?: HttpContext): Observable<StrictHttpResponse<WorkbenchServiceApiV3CustomModelsExaminationsExamination>> {
  const rb = new RequestBuilder(rootUrl, examinationsPut.PATH, 'put');
  if (params) {
    rb.body(params.body, 'application/json');
  }

  return http.request(
    rb.build({ responseType: 'json', accept: 'application/json', context })
  ).pipe(
    filter((r: any): r is HttpResponse<any> => r instanceof HttpResponse),
    map((r: HttpResponse<any>) => {
      return r as StrictHttpResponse<WorkbenchServiceApiV3CustomModelsExaminationsExamination>;
    })
  );
}

examinationsPut.PATH = '/examinations';
