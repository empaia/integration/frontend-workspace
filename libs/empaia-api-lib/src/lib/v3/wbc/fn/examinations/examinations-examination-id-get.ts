/* tslint:disable */
/* eslint-disable */
import { HttpClient, HttpContext, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { StrictHttpResponse } from '../../strict-http-response';
import { RequestBuilder } from '../../request-builder';

import { WorkbenchServiceApiV3CustomModelsExaminationsExamination } from '../../models/workbench-service-api-v-3-custom-models-examinations-examination';

export interface ExaminationsExaminationIdGet$Params {

/**
 * Examination ID
 */
  examination_id: string;
}

export function examinationsExaminationIdGet(http: HttpClient, rootUrl: string, params: ExaminationsExaminationIdGet$Params, context?: HttpContext): Observable<StrictHttpResponse<WorkbenchServiceApiV3CustomModelsExaminationsExamination>> {
  const rb = new RequestBuilder(rootUrl, examinationsExaminationIdGet.PATH, 'get');
  if (params) {
    rb.path('examination_id', params.examination_id, {});
  }

  return http.request(
    rb.build({ responseType: 'json', accept: 'application/json', context })
  ).pipe(
    filter((r: any): r is HttpResponse<any> => r instanceof HttpResponse),
    map((r: HttpResponse<any>) => {
      return r as StrictHttpResponse<WorkbenchServiceApiV3CustomModelsExaminationsExamination>;
    })
  );
}

examinationsExaminationIdGet.PATH = '/examinations/{examination_id}';
