/* tslint:disable */
/* eslint-disable */
import { HttpClient, HttpContext, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { StrictHttpResponse } from '../../strict-http-response';
import { RequestBuilder } from '../../request-builder';

import { SlideList } from '../../models/slide-list';

export interface CasesCaseIdSlidesGet$Params {

/**
 * EMPAIA case ID
 */
  case_id: string;
  skip?: number;
  limit?: number;
}

export function casesCaseIdSlidesGet(http: HttpClient, rootUrl: string, params: CasesCaseIdSlidesGet$Params, context?: HttpContext): Observable<StrictHttpResponse<SlideList>> {
  const rb = new RequestBuilder(rootUrl, casesCaseIdSlidesGet.PATH, 'get');
  if (params) {
    rb.path('case_id', params.case_id, {});
    rb.query('skip', params.skip, {});
    rb.query('limit', params.limit, {});
  }

  return http.request(
    rb.build({ responseType: 'json', accept: 'application/json', context })
  ).pipe(
    filter((r: any): r is HttpResponse<any> => r instanceof HttpResponse),
    map((r: HttpResponse<any>) => {
      return r as StrictHttpResponse<SlideList>;
    })
  );
}

casesCaseIdSlidesGet.PATH = '/cases/{case_id}/slides';
