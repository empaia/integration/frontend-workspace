/* tslint:disable */
/* eslint-disable */
import { HttpClient, HttpContext, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { StrictHttpResponse } from '../../strict-http-response';
import { RequestBuilder } from '../../request-builder';

import { Case } from '../../models/case';

export interface CasesCaseIdGet$Params {

/**
 * EMPAIA case ID
 */
  case_id: string;
}

export function casesCaseIdGet(http: HttpClient, rootUrl: string, params: CasesCaseIdGet$Params, context?: HttpContext): Observable<StrictHttpResponse<Case>> {
  const rb = new RequestBuilder(rootUrl, casesCaseIdGet.PATH, 'get');
  if (params) {
    rb.path('case_id', params.case_id, {});
  }

  return http.request(
    rb.build({ responseType: 'json', accept: 'application/json', context })
  ).pipe(
    filter((r: any): r is HttpResponse<any> => r instanceof HttpResponse),
    map((r: HttpResponse<any>) => {
      return r as StrictHttpResponse<Case>;
    })
  );
}

casesCaseIdGet.PATH = '/cases/{case_id}';
