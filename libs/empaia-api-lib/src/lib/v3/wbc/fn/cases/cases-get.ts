/* tslint:disable */
/* eslint-disable */
import { HttpClient, HttpContext, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { StrictHttpResponse } from '../../strict-http-response';
import { RequestBuilder } from '../../request-builder';

import { CaseList } from '../../models/case-list';

export interface CasesGet$Params {
  skip?: number;
  limit?: number;
}

export function casesGet(http: HttpClient, rootUrl: string, params?: CasesGet$Params, context?: HttpContext): Observable<StrictHttpResponse<CaseList>> {
  const rb = new RequestBuilder(rootUrl, casesGet.PATH, 'get');
  if (params) {
    rb.query('skip', params.skip, {});
    rb.query('limit', params.limit, {});
  }

  return http.request(
    rb.build({ responseType: 'json', accept: 'application/json', context })
  ).pipe(
    filter((r: any): r is HttpResponse<any> => r instanceof HttpResponse),
    map((r: HttpResponse<any>) => {
      return r as StrictHttpResponse<CaseList>;
    })
  );
}

casesGet.PATH = '/cases';
