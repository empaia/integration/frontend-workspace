/* tslint:disable */
/* eslint-disable */
import { HttpClient, HttpContext, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { StrictHttpResponse } from '../../strict-http-response';
import { RequestBuilder } from '../../request-builder';

import { SlideInfo } from '../../models/slide-info';

export interface SlidesSlideIdInfoGet$Params {

/**
 * Slide ID
 */
  slide_id: string;
}

export function slidesSlideIdInfoGet(http: HttpClient, rootUrl: string, params: SlidesSlideIdInfoGet$Params, context?: HttpContext): Observable<StrictHttpResponse<SlideInfo>> {
  const rb = new RequestBuilder(rootUrl, slidesSlideIdInfoGet.PATH, 'get');
  if (params) {
    rb.path('slide_id', params.slide_id, {});
  }

  return http.request(
    rb.build({ responseType: 'json', accept: 'application/json', context })
  ).pipe(
    filter((r: any): r is HttpResponse<any> => r instanceof HttpResponse),
    map((r: HttpResponse<any>) => {
      return r as StrictHttpResponse<SlideInfo>;
    })
  );
}

slidesSlideIdInfoGet.PATH = '/slides/{slide_id}/info';
