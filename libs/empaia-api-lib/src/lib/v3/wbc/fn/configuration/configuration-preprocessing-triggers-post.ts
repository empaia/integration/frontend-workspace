/* tslint:disable */
/* eslint-disable */
import { HttpClient, HttpContext, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { StrictHttpResponse } from '../../strict-http-response';
import { RequestBuilder } from '../../request-builder';

import { ExtendedPreprocessingTrigger } from '../../models/extended-preprocessing-trigger';
import { PostPreprocessingTrigger } from '../../models/post-preprocessing-trigger';

export interface ConfigurationPreprocessingTriggersPost$Params {
      body: PostPreprocessingTrigger
}

export function configurationPreprocessingTriggersPost(http: HttpClient, rootUrl: string, params: ConfigurationPreprocessingTriggersPost$Params, context?: HttpContext): Observable<StrictHttpResponse<ExtendedPreprocessingTrigger>> {
  const rb = new RequestBuilder(rootUrl, configurationPreprocessingTriggersPost.PATH, 'post');
  if (params) {
    rb.body(params.body, 'application/json');
  }

  return http.request(
    rb.build({ responseType: 'json', accept: 'application/json', context })
  ).pipe(
    filter((r: any): r is HttpResponse<any> => r instanceof HttpResponse),
    map((r: HttpResponse<any>) => {
      return r as StrictHttpResponse<ExtendedPreprocessingTrigger>;
    })
  );
}

configurationPreprocessingTriggersPost.PATH = '/configuration/preprocessing-triggers';
