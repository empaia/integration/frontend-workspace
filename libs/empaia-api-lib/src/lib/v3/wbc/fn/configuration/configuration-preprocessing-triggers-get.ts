/* tslint:disable */
/* eslint-disable */
import { HttpClient, HttpContext, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { StrictHttpResponse } from '../../strict-http-response';
import { RequestBuilder } from '../../request-builder';

import { ExtendedPreprocessingTriggerList } from '../../models/extended-preprocessing-trigger-list';

export interface ConfigurationPreprocessingTriggersGet$Params {
}

export function configurationPreprocessingTriggersGet(http: HttpClient, rootUrl: string, params?: ConfigurationPreprocessingTriggersGet$Params, context?: HttpContext): Observable<StrictHttpResponse<ExtendedPreprocessingTriggerList>> {
  const rb = new RequestBuilder(rootUrl, configurationPreprocessingTriggersGet.PATH, 'get');
  if (params) {
  }

  return http.request(
    rb.build({ responseType: 'json', accept: 'application/json', context })
  ).pipe(
    filter((r: any): r is HttpResponse<any> => r instanceof HttpResponse),
    map((r: HttpResponse<any>) => {
      return r as StrictHttpResponse<ExtendedPreprocessingTriggerList>;
    })
  );
}

configurationPreprocessingTriggersGet.PATH = '/configuration/preprocessing-triggers';
