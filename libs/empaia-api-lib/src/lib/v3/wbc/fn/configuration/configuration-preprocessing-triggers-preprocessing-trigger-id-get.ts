/* tslint:disable */
/* eslint-disable */
import { HttpClient, HttpContext, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { StrictHttpResponse } from '../../strict-http-response';
import { RequestBuilder } from '../../request-builder';

import { ExtendedPreprocessingTrigger } from '../../models/extended-preprocessing-trigger';

export interface ConfigurationPreprocessingTriggersPreprocessingTriggerIdGet$Params {
  preprocessing_trigger_id: string;
}

export function configurationPreprocessingTriggersPreprocessingTriggerIdGet(http: HttpClient, rootUrl: string, params: ConfigurationPreprocessingTriggersPreprocessingTriggerIdGet$Params, context?: HttpContext): Observable<StrictHttpResponse<ExtendedPreprocessingTrigger>> {
  const rb = new RequestBuilder(rootUrl, configurationPreprocessingTriggersPreprocessingTriggerIdGet.PATH, 'get');
  if (params) {
    rb.path('preprocessing_trigger_id', params.preprocessing_trigger_id, {});
  }

  return http.request(
    rb.build({ responseType: 'json', accept: 'application/json', context })
  ).pipe(
    filter((r: any): r is HttpResponse<any> => r instanceof HttpResponse),
    map((r: HttpResponse<any>) => {
      return r as StrictHttpResponse<ExtendedPreprocessingTrigger>;
    })
  );
}

configurationPreprocessingTriggersPreprocessingTriggerIdGet.PATH = '/configuration/preprocessing-triggers/{preprocessing_trigger_id}';
