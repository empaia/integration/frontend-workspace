/* tslint:disable */
/* eslint-disable */
import { HttpClient, HttpContext, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { StrictHttpResponse } from '../../strict-http-response';
import { RequestBuilder } from '../../request-builder';

import { AppList } from '../../models/app-list';
import { AppQuery } from '../../models/app-query';

export interface AppsQueryPut$Params {
      body: AppQuery
}

export function appsQueryPut(http: HttpClient, rootUrl: string, params: AppsQueryPut$Params, context?: HttpContext): Observable<StrictHttpResponse<AppList>> {
  const rb = new RequestBuilder(rootUrl, appsQueryPut.PATH, 'put');
  if (params) {
    rb.body(params.body, 'application/json');
  }

  return http.request(
    rb.build({ responseType: 'json', accept: 'application/json', context })
  ).pipe(
    filter((r: any): r is HttpResponse<any> => r instanceof HttpResponse),
    map((r: HttpResponse<any>) => {
      return r as StrictHttpResponse<AppList>;
    })
  );
}

appsQueryPut.PATH = '/apps/query';
