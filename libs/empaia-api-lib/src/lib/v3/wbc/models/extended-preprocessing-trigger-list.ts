/* tslint:disable */
/* eslint-disable */
import { ExtendedPreprocessingTrigger } from '../models/extended-preprocessing-trigger';
export interface ExtendedPreprocessingTriggerList {

  /**
   * Count of items
   */
  item_count: number;
  items: Array<ExtendedPreprocessingTrigger>;
}
