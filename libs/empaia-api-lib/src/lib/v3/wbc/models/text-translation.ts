/* tslint:disable */
/* eslint-disable */
import { Language } from '../models/language';
export interface TextTranslation {

  /**
   * Language abbreviation
   */
  lang: Language;

  /**
   * Translated tag name
   */
  text: string;
}
