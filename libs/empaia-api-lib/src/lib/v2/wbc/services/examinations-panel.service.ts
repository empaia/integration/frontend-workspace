/* tslint:disable */
/* eslint-disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { BaseService } from '../base-service';
import { ApiConfiguration } from '../api-configuration';
import { StrictHttpResponse } from '../strict-http-response';
import { RequestBuilder } from '../request-builder';
import { Observable } from 'rxjs';
import { map, filter } from 'rxjs/operators';

import { AppListInput } from '../models/app-list-input';
import { AppUiConfiguration } from '../models/app-ui-configuration';
import { Examination } from '../models/examination';
import { FrontendToken } from '../models/frontend-token';
import { PostAppResponse } from '../models/post-app-response';
import { ScopeTokenAndScopeId } from '../models/scope-token-and-scope-id';

@Injectable({
  providedIn: 'root',
})
export class ExaminationsPanelService extends BaseService {
  constructor(
    config: ApiConfiguration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * Path part for operation examinationsExaminationIdClosePut
   */
  static readonly ExaminationsExaminationIdClosePutPath = '/examinations/{examination_id}/close';

  /**
   * .
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `examinationsExaminationIdClosePut()` instead.
   *
   * This method doesn't expect any request body.
   */
  examinationsExaminationIdClosePut$Response(params: {

    /**
     * Examination ID
     */
    examination_id: string;
  }): Observable<StrictHttpResponse<Examination>> {

    const rb = new RequestBuilder(this.rootUrl, ExaminationsPanelService.ExaminationsExaminationIdClosePutPath, 'put');
    if (params) {
      rb.path('examination_id', params.examination_id, {});
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json'
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<Examination>;
      })
    );
  }

  /**
   * .
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `examinationsExaminationIdClosePut$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  examinationsExaminationIdClosePut(params: {

    /**
     * Examination ID
     */
    examination_id: string;
  }): Observable<Examination> {

    return this.examinationsExaminationIdClosePut$Response(params).pipe(
      map((r: StrictHttpResponse<Examination>) => r.body as Examination)
    );
  }

  /**
   * Path part for operation examinationsExaminationIdAppsGet
   */
  static readonly ExaminationsExaminationIdAppsGetPath = '/examinations/{examination_id}/apps';

  /**
   * .
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `examinationsExaminationIdAppsGet()` instead.
   *
   * This method doesn't expect any request body.
   */
  examinationsExaminationIdAppsGet$Response(params: {

    /**
     * Examination ID
     */
    examination_id: string;
  }): Observable<StrictHttpResponse<AppListInput>> {

    const rb = new RequestBuilder(this.rootUrl, ExaminationsPanelService.ExaminationsExaminationIdAppsGetPath, 'get');
    if (params) {
      rb.path('examination_id', params.examination_id, {});
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json'
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<AppListInput>;
      })
    );
  }

  /**
   * .
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `examinationsExaminationIdAppsGet$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  examinationsExaminationIdAppsGet(params: {

    /**
     * Examination ID
     */
    examination_id: string;
  }): Observable<AppListInput> {

    return this.examinationsExaminationIdAppsGet$Response(params).pipe(
      map((r: StrictHttpResponse<AppListInput>) => r.body as AppListInput)
    );
  }

  /**
   * Path part for operation examinationsExaminationIdAppsAppIdFrontendTokenGet
   */
  static readonly ExaminationsExaminationIdAppsAppIdFrontendTokenGetPath = '/examinations/{examination_id}/apps/{app_id}/frontend-token';

  /**
   * .
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `examinationsExaminationIdAppsAppIdFrontendTokenGet()` instead.
   *
   * This method doesn't expect any request body.
   */
  examinationsExaminationIdAppsAppIdFrontendTokenGet$Response(params: {

    /**
     * Examination ID
     */
    examination_id: string;

    /**
     * App ID
     */
    app_id: string;
  }): Observable<StrictHttpResponse<FrontendToken>> {

    const rb = new RequestBuilder(this.rootUrl, ExaminationsPanelService.ExaminationsExaminationIdAppsAppIdFrontendTokenGetPath, 'get');
    if (params) {
      rb.path('examination_id', params.examination_id, {});
      rb.path('app_id', params.app_id, {});
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json'
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<FrontendToken>;
      })
    );
  }

  /**
   * .
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `examinationsExaminationIdAppsAppIdFrontendTokenGet$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  examinationsExaminationIdAppsAppIdFrontendTokenGet(params: {

    /**
     * Examination ID
     */
    examination_id: string;

    /**
     * App ID
     */
    app_id: string;
  }): Observable<FrontendToken> {

    return this.examinationsExaminationIdAppsAppIdFrontendTokenGet$Response(params).pipe(
      map((r: StrictHttpResponse<FrontendToken>) => r.body as FrontendToken)
    );
  }

  /**
   * Path part for operation examinationsExaminationIdAppsAppIdAppUiConfigGet
   */
  static readonly ExaminationsExaminationIdAppsAppIdAppUiConfigGetPath = '/examinations/{examination_id}/apps/{app_id}/app-ui-config';

  /**
   * .
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `examinationsExaminationIdAppsAppIdAppUiConfigGet()` instead.
   *
   * This method doesn't expect any request body.
   */
  examinationsExaminationIdAppsAppIdAppUiConfigGet$Response(params: {

    /**
     * Examination ID
     */
    examination_id: string;

    /**
     * App ID
     */
    app_id: string;
  }): Observable<StrictHttpResponse<AppUiConfiguration>> {

    const rb = new RequestBuilder(this.rootUrl, ExaminationsPanelService.ExaminationsExaminationIdAppsAppIdAppUiConfigGetPath, 'get');
    if (params) {
      rb.path('examination_id', params.examination_id, {});
      rb.path('app_id', params.app_id, {});
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json'
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<AppUiConfiguration>;
      })
    );
  }

  /**
   * .
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `examinationsExaminationIdAppsAppIdAppUiConfigGet$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  examinationsExaminationIdAppsAppIdAppUiConfigGet(params: {

    /**
     * Examination ID
     */
    examination_id: string;

    /**
     * App ID
     */
    app_id: string;
  }): Observable<AppUiConfiguration> {

    return this.examinationsExaminationIdAppsAppIdAppUiConfigGet$Response(params).pipe(
      map((r: StrictHttpResponse<AppUiConfiguration>) => r.body as AppUiConfiguration)
    );
  }

  /**
   * Path part for operation examinationsExaminationIdAppsAppIdPut
   */
  static readonly ExaminationsExaminationIdAppsAppIdPutPath = '/examinations/{examination_id}/apps/{app_id}';

  /**
   * .
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `examinationsExaminationIdAppsAppIdPut()` instead.
   *
   * This method doesn't expect any request body.
   */
  examinationsExaminationIdAppsAppIdPut$Response(params: {

    /**
     * Examination ID
     */
    examination_id: string;

    /**
     * App ID
     */
    app_id: string;
  }): Observable<StrictHttpResponse<PostAppResponse>> {

    const rb = new RequestBuilder(this.rootUrl, ExaminationsPanelService.ExaminationsExaminationIdAppsAppIdPutPath, 'put');
    if (params) {
      rb.path('examination_id', params.examination_id, {});
      rb.path('app_id', params.app_id, {});
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json'
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<PostAppResponse>;
      })
    );
  }

  /**
   * .
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `examinationsExaminationIdAppsAppIdPut$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  examinationsExaminationIdAppsAppIdPut(params: {

    /**
     * Examination ID
     */
    examination_id: string;

    /**
     * App ID
     */
    app_id: string;
  }): Observable<PostAppResponse> {

    return this.examinationsExaminationIdAppsAppIdPut$Response(params).pipe(
      map((r: StrictHttpResponse<PostAppResponse>) => r.body as PostAppResponse)
    );
  }

  /**
   * Path part for operation examinationsExaminationIdAppsAppIdScopePut
   */
  static readonly ExaminationsExaminationIdAppsAppIdScopePutPath = '/examinations/{examination_id}/apps/{app_id}/scope';

  /**
   * .
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `examinationsExaminationIdAppsAppIdScopePut()` instead.
   *
   * This method doesn't expect any request body.
   */
  examinationsExaminationIdAppsAppIdScopePut$Response(params: {

    /**
     * Examination ID
     */
    examination_id: string;

    /**
     * App ID
     */
    app_id: string;
  }): Observable<StrictHttpResponse<ScopeTokenAndScopeId>> {

    const rb = new RequestBuilder(this.rootUrl, ExaminationsPanelService.ExaminationsExaminationIdAppsAppIdScopePutPath, 'put');
    if (params) {
      rb.path('examination_id', params.examination_id, {});
      rb.path('app_id', params.app_id, {});
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json'
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<ScopeTokenAndScopeId>;
      })
    );
  }

  /**
   * .
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `examinationsExaminationIdAppsAppIdScopePut$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  examinationsExaminationIdAppsAppIdScopePut(params: {

    /**
     * Examination ID
     */
    examination_id: string;

    /**
     * App ID
     */
    app_id: string;
  }): Observable<ScopeTokenAndScopeId> {

    return this.examinationsExaminationIdAppsAppIdScopePut$Response(params).pipe(
      map((r: StrictHttpResponse<ScopeTokenAndScopeId>) => r.body as ScopeTokenAndScopeId)
    );
  }

}
