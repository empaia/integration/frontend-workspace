/* tslint:disable */
/* eslint-disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { BaseService } from '../base-service';
import { ApiConfiguration } from '../api-configuration';
import { StrictHttpResponse } from '../strict-http-response';
import { RequestBuilder } from '../request-builder';
import { Observable } from 'rxjs';
import { map, filter } from 'rxjs/operators';

import { SlideInfo } from '../models/slide-info';

@Injectable({
  providedIn: 'root',
})
export class SlidesPanelService extends BaseService {
  constructor(
    config: ApiConfiguration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * Path part for operation slidesSlideIdInfoGet
   */
  static readonly SlidesSlideIdInfoGetPath = '/slides/{slide_id}/info';

  /**
   * .
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `slidesSlideIdInfoGet()` instead.
   *
   * This method doesn't expect any request body.
   */
  slidesSlideIdInfoGet$Response(params: {

    /**
     * Slide ID
     */
    slide_id: string;
  }): Observable<StrictHttpResponse<SlideInfo>> {

    const rb = new RequestBuilder(this.rootUrl, SlidesPanelService.SlidesSlideIdInfoGetPath, 'get');
    if (params) {
      rb.path('slide_id', params.slide_id, {});
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json'
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<SlideInfo>;
      })
    );
  }

  /**
   * .
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `slidesSlideIdInfoGet$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  slidesSlideIdInfoGet(params: {

    /**
     * Slide ID
     */
    slide_id: string;
  }): Observable<SlideInfo> {

    return this.slidesSlideIdInfoGet$Response(params).pipe(
      map((r: StrictHttpResponse<SlideInfo>) => r.body as SlideInfo)
    );
  }

  /**
   * Path part for operation slidesSlideIdTileLevelLevelTileTileXTileYGet
   */
  static readonly SlidesSlideIdTileLevelLevelTileTileXTileYGetPath = '/slides/{slide_id}/tile/level/{level}/tile/{tile_x}/{tile_y}';

  /**
   * .
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `slidesSlideIdTileLevelLevelTileTileXTileYGet()` instead.
   *
   * This method doesn't expect any request body.
   */
  slidesSlideIdTileLevelLevelTileTileXTileYGet$Response(params: {

    /**
     * Slide ID
     */
    slide_id: string;

    /**
     * Zoom level
     */
    level: number;

    /**
     * Tile number horizontally
     */
    tile_x: number;

    /**
     * Tile number vertically
     */
    tile_y: number;

    /**
     * Image format (e.g. bmp, gif, jpeg, png, tiff). For raw image data choose tiff.
     */
    image_format?: string;

    /**
     * Image quality (Only for specific formats. For Jpeg files compression is always lossy.         For tiff files &#x27;deflate&#x27; compression is used by default. Set to 0 to compress lossy with &#x27;jpeg&#x27;)
     */
    image_quality?: number;

    /**
     * List of requested image channels. By default all channels are returned.
     */
    image_channels?: Array<number>;

    /**
     * Padding color as 24-bit hex-string
     */
    padding_color?: string;

    /**
     * Z coordinate / stack
     */
    'z'?: number;
  }): Observable<StrictHttpResponse<Blob>> {

    const rb = new RequestBuilder(this.rootUrl, SlidesPanelService.SlidesSlideIdTileLevelLevelTileTileXTileYGetPath, 'get');
    if (params) {
      rb.path('slide_id', params.slide_id, {});
      rb.path('level', params.level, {});
      rb.path('tile_x', params.tile_x, {});
      rb.path('tile_y', params.tile_y, {});
      rb.query('image_format', params.image_format, {});
      rb.query('image_quality', params.image_quality, {});
      rb.query('image_channels', params.image_channels, {});
      rb.query('padding_color', params.padding_color, {});
      rb.query('z', params['z'], {});
    }

    return this.http.request(rb.build({
      responseType: 'blob',
      accept: 'image/*'
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<Blob>;
      })
    );
  }

  /**
   * .
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `slidesSlideIdTileLevelLevelTileTileXTileYGet$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  slidesSlideIdTileLevelLevelTileTileXTileYGet(params: {

    /**
     * Slide ID
     */
    slide_id: string;

    /**
     * Zoom level
     */
    level: number;

    /**
     * Tile number horizontally
     */
    tile_x: number;

    /**
     * Tile number vertically
     */
    tile_y: number;

    /**
     * Image format (e.g. bmp, gif, jpeg, png, tiff). For raw image data choose tiff.
     */
    image_format?: string;

    /**
     * Image quality (Only for specific formats. For Jpeg files compression is always lossy.         For tiff files &#x27;deflate&#x27; compression is used by default. Set to 0 to compress lossy with &#x27;jpeg&#x27;)
     */
    image_quality?: number;

    /**
     * List of requested image channels. By default all channels are returned.
     */
    image_channels?: Array<number>;

    /**
     * Padding color as 24-bit hex-string
     */
    padding_color?: string;

    /**
     * Z coordinate / stack
     */
    'z'?: number;
  }): Observable<Blob> {

    return this.slidesSlideIdTileLevelLevelTileTileXTileYGet$Response(params).pipe(
      map((r: StrictHttpResponse<Blob>) => r.body as Blob)
    );
  }

  /**
   * Path part for operation slidesSlideIdMacroMaxSizeMaxXMaxYGet
   */
  static readonly SlidesSlideIdMacroMaxSizeMaxXMaxYGetPath = '/slides/{slide_id}/macro/max_size/{max_x}/{max_y}';

  /**
   * .
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `slidesSlideIdMacroMaxSizeMaxXMaxYGet()` instead.
   *
   * This method doesn't expect any request body.
   */
  slidesSlideIdMacroMaxSizeMaxXMaxYGet$Response(params: {

    /**
     * Slide ID
     */
    slide_id: string;

    /**
     * Maximum width of image
     */
    max_x: number;

    /**
     * Maximum height of image
     */
    max_y: number;

    /**
     * Image format (e.g. bmp, gif, jpeg, png, tiff). For raw image data choose tiff.
     */
    image_format?: string;

    /**
     * Image quality (Only for specific formats. For Jpeg files compression is always lossy.         For tiff files &#x27;deflate&#x27; compression is used by default. Set to 0 to compress lossy with &#x27;jpeg&#x27;)
     */
    image_quality?: number;
  }): Observable<StrictHttpResponse<Blob>> {

    const rb = new RequestBuilder(this.rootUrl, SlidesPanelService.SlidesSlideIdMacroMaxSizeMaxXMaxYGetPath, 'get');
    if (params) {
      rb.path('slide_id', params.slide_id, {});
      rb.path('max_x', params.max_x, {});
      rb.path('max_y', params.max_y, {});
      rb.query('image_format', params.image_format, {});
      rb.query('image_quality', params.image_quality, {});
    }

    return this.http.request(rb.build({
      responseType: 'blob',
      accept: 'image/*'
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<Blob>;
      })
    );
  }

  /**
   * .
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `slidesSlideIdMacroMaxSizeMaxXMaxYGet$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  slidesSlideIdMacroMaxSizeMaxXMaxYGet(params: {

    /**
     * Slide ID
     */
    slide_id: string;

    /**
     * Maximum width of image
     */
    max_x: number;

    /**
     * Maximum height of image
     */
    max_y: number;

    /**
     * Image format (e.g. bmp, gif, jpeg, png, tiff). For raw image data choose tiff.
     */
    image_format?: string;

    /**
     * Image quality (Only for specific formats. For Jpeg files compression is always lossy.         For tiff files &#x27;deflate&#x27; compression is used by default. Set to 0 to compress lossy with &#x27;jpeg&#x27;)
     */
    image_quality?: number;
  }): Observable<Blob> {

    return this.slidesSlideIdMacroMaxSizeMaxXMaxYGet$Response(params).pipe(
      map((r: StrictHttpResponse<Blob>) => r.body as Blob)
    );
  }

  /**
   * Path part for operation slidesSlideIdLabelMaxSizeMaxXMaxYGet
   */
  static readonly SlidesSlideIdLabelMaxSizeMaxXMaxYGetPath = '/slides/{slide_id}/label/max_size/{max_x}/{max_y}';

  /**
   * .
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `slidesSlideIdLabelMaxSizeMaxXMaxYGet()` instead.
   *
   * This method doesn't expect any request body.
   */
  slidesSlideIdLabelMaxSizeMaxXMaxYGet$Response(params: {

    /**
     * Slide ID
     */
    slide_id: string;

    /**
     * Maximum width of image
     */
    max_x: number;

    /**
     * Maximum height of image
     */
    max_y: number;

    /**
     * Image format (e.g. bmp, gif, jpeg, png, tiff). For raw image data choose tiff.
     */
    image_format?: string;

    /**
     * Image quality (Only for specific formats. For Jpeg files compression is always lossy.         For tiff files &#x27;deflate&#x27; compression is used by default. Set to 0 to compress lossy with &#x27;jpeg&#x27;)
     */
    image_quality?: number;
  }): Observable<StrictHttpResponse<Blob>> {

    const rb = new RequestBuilder(this.rootUrl, SlidesPanelService.SlidesSlideIdLabelMaxSizeMaxXMaxYGetPath, 'get');
    if (params) {
      rb.path('slide_id', params.slide_id, {});
      rb.path('max_x', params.max_x, {});
      rb.path('max_y', params.max_y, {});
      rb.query('image_format', params.image_format, {});
      rb.query('image_quality', params.image_quality, {});
    }

    return this.http.request(rb.build({
      responseType: 'blob',
      accept: 'image/*'
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<Blob>;
      })
    );
  }

  /**
   * .
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `slidesSlideIdLabelMaxSizeMaxXMaxYGet$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  slidesSlideIdLabelMaxSizeMaxXMaxYGet(params: {

    /**
     * Slide ID
     */
    slide_id: string;

    /**
     * Maximum width of image
     */
    max_x: number;

    /**
     * Maximum height of image
     */
    max_y: number;

    /**
     * Image format (e.g. bmp, gif, jpeg, png, tiff). For raw image data choose tiff.
     */
    image_format?: string;

    /**
     * Image quality (Only for specific formats. For Jpeg files compression is always lossy.         For tiff files &#x27;deflate&#x27; compression is used by default. Set to 0 to compress lossy with &#x27;jpeg&#x27;)
     */
    image_quality?: number;
  }): Observable<Blob> {

    return this.slidesSlideIdLabelMaxSizeMaxXMaxYGet$Response(params).pipe(
      map((r: StrictHttpResponse<Blob>) => r.body as Blob)
    );
  }

  /**
   * Path part for operation slidesSlideIdThumbnailMaxSizeMaxXMaxYGet
   */
  static readonly SlidesSlideIdThumbnailMaxSizeMaxXMaxYGetPath = '/slides/{slide_id}/thumbnail/max_size/{max_x}/{max_y}';

  /**
   * .
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `slidesSlideIdThumbnailMaxSizeMaxXMaxYGet()` instead.
   *
   * This method doesn't expect any request body.
   */
  slidesSlideIdThumbnailMaxSizeMaxXMaxYGet$Response(params: {

    /**
     * Slide ID
     */
    slide_id: string;

    /**
     * Maximum width of image
     */
    max_x: number;

    /**
     * Maximum height of image
     */
    max_y: number;

    /**
     * Image format (e.g. bmp, gif, jpeg, png, tiff). For raw image data choose tiff.
     */
    image_format?: string;

    /**
     * Image quality (Only for specific formats. For Jpeg files compression is always lossy.         For tiff files &#x27;deflate&#x27; compression is used by default. Set to 0 to compress lossy with &#x27;jpeg&#x27;)
     */
    image_quality?: number;
  }): Observable<StrictHttpResponse<Blob>> {

    const rb = new RequestBuilder(this.rootUrl, SlidesPanelService.SlidesSlideIdThumbnailMaxSizeMaxXMaxYGetPath, 'get');
    if (params) {
      rb.path('slide_id', params.slide_id, {});
      rb.path('max_x', params.max_x, {});
      rb.path('max_y', params.max_y, {});
      rb.query('image_format', params.image_format, {});
      rb.query('image_quality', params.image_quality, {});
    }

    return this.http.request(rb.build({
      responseType: 'blob',
      accept: 'image/*'
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<Blob>;
      })
    );
  }

  /**
   * .
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `slidesSlideIdThumbnailMaxSizeMaxXMaxYGet$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  slidesSlideIdThumbnailMaxSizeMaxXMaxYGet(params: {

    /**
     * Slide ID
     */
    slide_id: string;

    /**
     * Maximum width of image
     */
    max_x: number;

    /**
     * Maximum height of image
     */
    max_y: number;

    /**
     * Image format (e.g. bmp, gif, jpeg, png, tiff). For raw image data choose tiff.
     */
    image_format?: string;

    /**
     * Image quality (Only for specific formats. For Jpeg files compression is always lossy.         For tiff files &#x27;deflate&#x27; compression is used by default. Set to 0 to compress lossy with &#x27;jpeg&#x27;)
     */
    image_quality?: number;
  }): Observable<Blob> {

    return this.slidesSlideIdThumbnailMaxSizeMaxXMaxYGet$Response(params).pipe(
      map((r: StrictHttpResponse<Blob>) => r.body as Blob)
    );
  }

}
