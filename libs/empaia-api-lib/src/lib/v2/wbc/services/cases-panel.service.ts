/* tslint:disable */
/* eslint-disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { BaseService } from '../base-service';
import { ApiConfiguration } from '../api-configuration';
import { StrictHttpResponse } from '../strict-http-response';
import { RequestBuilder } from '../request-builder';
import { Observable } from 'rxjs';
import { map, filter } from 'rxjs/operators';

import { AppListOutput } from '../models/app-list-output';
import { Case } from '../models/case';
import { CaseList } from '../models/case-list';
import { Examination } from '../models/examination';
import { ExaminationList } from '../models/examination-list';
import { SlideList } from '../models/slide-list';

@Injectable({
  providedIn: 'root',
})
export class CasesPanelService extends BaseService {
  constructor(
    config: ApiConfiguration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * Path part for operation casesGet
   */
  static readonly CasesGetPath = '/cases';

  /**
   * .
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `casesGet()` instead.
   *
   * This method doesn't expect any request body.
   */
  casesGet$Response(params?: {
    skip?: number;
    limit?: number;
  }): Observable<StrictHttpResponse<CaseList>> {

    const rb = new RequestBuilder(this.rootUrl, CasesPanelService.CasesGetPath, 'get');
    if (params) {
      rb.query('skip', params.skip, {});
      rb.query('limit', params.limit, {});
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json'
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<CaseList>;
      })
    );
  }

  /**
   * .
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `casesGet$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  casesGet(params?: {
    skip?: number;
    limit?: number;
  }): Observable<CaseList> {

    return this.casesGet$Response(params).pipe(
      map((r: StrictHttpResponse<CaseList>) => r.body as CaseList)
    );
  }

  /**
   * Path part for operation casesCaseIdGet
   */
  static readonly CasesCaseIdGetPath = '/cases/{case_id}';

  /**
   * .
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `casesCaseIdGet()` instead.
   *
   * This method doesn't expect any request body.
   */
  casesCaseIdGet$Response(params: {

    /**
     * EMPAIA case ID
     */
    case_id: string;
  }): Observable<StrictHttpResponse<Case>> {

    const rb = new RequestBuilder(this.rootUrl, CasesPanelService.CasesCaseIdGetPath, 'get');
    if (params) {
      rb.path('case_id', params.case_id, {});
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json'
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<Case>;
      })
    );
  }

  /**
   * .
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `casesCaseIdGet$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  casesCaseIdGet(params: {

    /**
     * EMPAIA case ID
     */
    case_id: string;
  }): Observable<Case> {

    return this.casesCaseIdGet$Response(params).pipe(
      map((r: StrictHttpResponse<Case>) => r.body as Case)
    );
  }

  /**
   * Path part for operation casesCaseIdSlidesGet
   */
  static readonly CasesCaseIdSlidesGetPath = '/cases/{case_id}/slides';

  /**
   * .
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `casesCaseIdSlidesGet()` instead.
   *
   * This method doesn't expect any request body.
   */
  casesCaseIdSlidesGet$Response(params: {

    /**
     * EMPAIA case ID
     */
    case_id: string;
    skip?: number;
    limit?: number;
  }): Observable<StrictHttpResponse<SlideList>> {

    const rb = new RequestBuilder(this.rootUrl, CasesPanelService.CasesCaseIdSlidesGetPath, 'get');
    if (params) {
      rb.path('case_id', params.case_id, {});
      rb.query('skip', params.skip, {});
      rb.query('limit', params.limit, {});
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json'
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<SlideList>;
      })
    );
  }

  /**
   * .
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `casesCaseIdSlidesGet$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  casesCaseIdSlidesGet(params: {

    /**
     * EMPAIA case ID
     */
    case_id: string;
    skip?: number;
    limit?: number;
  }): Observable<SlideList> {

    return this.casesCaseIdSlidesGet$Response(params).pipe(
      map((r: StrictHttpResponse<SlideList>) => r.body as SlideList)
    );
  }

  /**
   * Path part for operation casesCaseIdExaminationsGet
   */
  static readonly CasesCaseIdExaminationsGetPath = '/cases/{case_id}/examinations';

  /**
   * .
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `casesCaseIdExaminationsGet()` instead.
   *
   * This method doesn't expect any request body.
   */
  casesCaseIdExaminationsGet$Response(params: {

    /**
     * EMPAIA case ID
     */
    case_id: string;
    skip?: number;
    limit?: number;
  }): Observable<StrictHttpResponse<ExaminationList>> {

    const rb = new RequestBuilder(this.rootUrl, CasesPanelService.CasesCaseIdExaminationsGetPath, 'get');
    if (params) {
      rb.path('case_id', params.case_id, {});
      rb.query('skip', params.skip, {});
      rb.query('limit', params.limit, {});
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json'
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<ExaminationList>;
      })
    );
  }

  /**
   * .
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `casesCaseIdExaminationsGet$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  casesCaseIdExaminationsGet(params: {

    /**
     * EMPAIA case ID
     */
    case_id: string;
    skip?: number;
    limit?: number;
  }): Observable<ExaminationList> {

    return this.casesCaseIdExaminationsGet$Response(params).pipe(
      map((r: StrictHttpResponse<ExaminationList>) => r.body as ExaminationList)
    );
  }

  /**
   * Path part for operation casesCaseIdExaminationsPost
   */
  static readonly CasesCaseIdExaminationsPostPath = '/cases/{case_id}/examinations';

  /**
   * .
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `casesCaseIdExaminationsPost()` instead.
   *
   * This method doesn't expect any request body.
   */
  casesCaseIdExaminationsPost$Response(params: {

    /**
     * EMPAIA case ID
     */
    case_id: string;
  }): Observable<StrictHttpResponse<Examination>> {

    const rb = new RequestBuilder(this.rootUrl, CasesPanelService.CasesCaseIdExaminationsPostPath, 'post');
    if (params) {
      rb.path('case_id', params.case_id, {});
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json'
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<Examination>;
      })
    );
  }

  /**
   * .
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `casesCaseIdExaminationsPost$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  casesCaseIdExaminationsPost(params: {

    /**
     * EMPAIA case ID
     */
    case_id: string;
  }): Observable<Examination> {

    return this.casesCaseIdExaminationsPost$Response(params).pipe(
      map((r: StrictHttpResponse<Examination>) => r.body as Examination)
    );
  }

  /**
   * Path part for operation casesCaseIdAppsGet
   */
  static readonly CasesCaseIdAppsGetPath = '/cases/{case_id}/apps';

  /**
   * .
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `casesCaseIdAppsGet()` instead.
   *
   * This method doesn't expect any request body.
   */
  casesCaseIdAppsGet$Response(params: {

    /**
     * EMPAIA case ID
     */
    case_id: string;
  }): Observable<StrictHttpResponse<AppListOutput>> {

    const rb = new RequestBuilder(this.rootUrl, CasesPanelService.CasesCaseIdAppsGetPath, 'get');
    if (params) {
      rb.path('case_id', params.case_id, {});
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json'
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<AppListOutput>;
      })
    );
  }

  /**
   * .
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `casesCaseIdAppsGet$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  casesCaseIdAppsGet(params: {

    /**
     * EMPAIA case ID
     */
    case_id: string;
  }): Observable<AppListOutput> {

    return this.casesCaseIdAppsGet$Response(params).pipe(
      map((r: StrictHttpResponse<AppListOutput>) => r.body as AppListOutput)
    );
  }

}
