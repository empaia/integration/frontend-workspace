export { CasesPanelService } from './services/cases-panel.service';
export { SlidesPanelService } from './services/slides-panel.service';
export { ExaminationsPanelService } from './services/examinations-panel.service';
