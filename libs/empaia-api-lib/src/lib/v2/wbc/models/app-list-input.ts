/* tslint:disable */
/* eslint-disable */
import { AppInput } from './app-input';
export interface AppListInput {

  /**
   * Number of apps (not affected by skip/limit pagination)
   */
  item_count: number;

  /**
   * List of apps (affected by skip/limit pagination)
   */
  items: Array<AppInput>;
}
