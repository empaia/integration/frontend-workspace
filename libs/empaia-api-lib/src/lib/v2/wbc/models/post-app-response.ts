/* tslint:disable */
/* eslint-disable */
export interface PostAppResponse {

  /**
   * App ID
   */
  app_id: string;

  /**
   * Examination ID
   */
  examination_id: string;
}
