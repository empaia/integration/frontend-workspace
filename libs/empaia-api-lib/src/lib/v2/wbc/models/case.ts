/* tslint:disable */
/* eslint-disable */
import { CaseCreatorType } from './case-creator-type';
import { Examination } from './examination';
export interface Case {

  /**
   * List of all blocks of slides in case
   */
  blocks: Array<string>;

  /**
   * Timestamp (milliseconds) when the case was created
   */
  created_at: number;

  /**
   * Creator ID
   */
  creator_id: string;

  /**
   * Creator type
   */
  creator_type: CaseCreatorType;

  /**
   * Flag indicating whether the case and all underlying slide files and mappings have been deleted
   */
  deleted: (boolean | null);

  /**
   * Case description
   */
  description: (string | null);

  /**
   * Examinations in case
   */
  examinations: Array<Examination>;

  /**
   * ID
   */
  id: string;

  /**
   * Local ID provided by AP-LIS
   */
  local_id: (string | null);

  /**
   * Base URL of Medical Data Service instance that generated empaia_id
   */
  mds_url: (string | null);

  /**
   * Number of slides in case
   */
  slides_count: number;

  /**
   * List of all stains of all slides in case
   */
  stains: {
};

  /**
   * List of tissue of all slides in case
   */
  tissues: {
};

  /**
   * Timestamp (milliseconds) when the case was last updated
   */
  updated_at: number;
}
