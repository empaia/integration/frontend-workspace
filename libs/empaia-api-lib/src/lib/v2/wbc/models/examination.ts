/* tslint:disable */
/* eslint-disable */
import { ExaminationCreatorType } from './examination-creator-type';
import { ExaminationState } from './examination-state';
export interface Examination {

  /**
   * List of app IDs in examination
   */
  apps: Array<string>;

  /**
   * Case ID
   */
  case_id: string;

  /**
   * Timestamp (milliseconds) when the examination was created
   */
  created_at: number;

  /**
   * Creator ID
   */
  creator_id: string;

  /**
   * Creator Type
   */
  creator_type: ExaminationCreatorType;

  /**
   * ID
   */
  id: string;

  /**
   * List of job IDs in examination
   */
  jobs: Array<string>;

  /**
   * Number of jobs (including finished) in examination
   */
  jobs_count: number;

  /**
   * Number of finished jobs in examination
   */
  jobs_count_finished: number;

  /**
   * Examination state
   */
  state: ExaminationState;

  /**
   * Timestamp (milliseconds) when the examination was last updated
   */
  updated_at: number;
}
