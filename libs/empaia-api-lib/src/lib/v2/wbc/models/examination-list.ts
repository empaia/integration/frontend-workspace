/* tslint:disable */
/* eslint-disable */
import { Examination } from './examination';
export interface ExaminationList {

  /**
   * Number of examinations (not affected by skip/limit pagination) in case
   */
  item_count: number;

  /**
   * List of examinations (affected by skip/limit pagination) in case
   */
  items: Array<Examination>;
}
