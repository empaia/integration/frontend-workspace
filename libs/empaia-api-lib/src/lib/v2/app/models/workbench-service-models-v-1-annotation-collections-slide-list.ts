/* tslint:disable */
/* eslint-disable */
import { SlideItem } from './slide-item';
export interface WorkbenchServiceModelsV1AnnotationCollectionsSlideList {

  /**
   * Count of all items
   */
  item_count: number;

  /**
   * List of items
   */
  items: Array<SlideItem>;
}
