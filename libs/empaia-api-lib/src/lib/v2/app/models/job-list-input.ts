/* tslint:disable */
/* eslint-disable */
import { JobInput } from './job-input';

/**
 * Job query result.
 */
export interface JobListInput {

  /**
   * Number of Jobs as queried without skip and limit applied
   */
  item_count: number;

  /**
   * List of Job items as queried with skip and limit applied
   */
  items: Array<JobInput>;
}
