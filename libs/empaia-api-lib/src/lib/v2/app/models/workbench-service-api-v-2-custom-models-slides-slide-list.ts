/* tslint:disable */
/* eslint-disable */
import { SlideInput } from './slide-input';
export interface WorkbenchServiceApiV2CustomModelsSlidesSlideList {

  /**
   * Number of slides (not affected by skip/limit pagination)
   */
  item_count: number;

  /**
   * List of slides (affected by skip/limit pagination)
   */
  items: Array<SlideInput>;
}
