/* tslint:disable */
/* eslint-disable */
import { CollectionItemType } from './collection-item-type';
import { CollectionReferenceType } from './collection-reference-type';
import { DataCreatorType } from './data-creator-type';
import { IdObject } from './id-object';
import { PostArrowAnnotation } from './post-arrow-annotation';
import { PostBoolPrimitive } from './post-bool-primitive';
import { PostCircleAnnotation } from './post-circle-annotation';
import { PostClass } from './post-class';
import { PostFloatPrimitive } from './post-float-primitive';
import { PostIntegerPrimitive } from './post-integer-primitive';
import { PostLineAnnotation } from './post-line-annotation';
import { PostPointAnnotation } from './post-point-annotation';
import { PostPolygonAnnotation } from './post-polygon-annotation';
import { PostRectangleAnnotation } from './post-rectangle-annotation';
import { PostStringPrimitive } from './post-string-primitive';
import { SlideItem } from './slide-item';
export interface PostCollection {

  /**
   * Creator Id
   */
  creator_id: string;

  /**
   * Creator type
   */
  creator_type: DataCreatorType;

  /**
   * Collection description
   */
  description?: (string | null);

  /**
   * ID of type UUID4 (only needed in post if external Ids enabled)
   */
  id?: (string | null);

  /**
   * The type of items in the collection
   */
  item_type: CollectionItemType;

  /**
   * Items of the collection
   */
  items?: (Array<PostPointAnnotation> | Array<PostLineAnnotation> | Array<PostArrowAnnotation> | Array<PostCircleAnnotation> | Array<PostRectangleAnnotation> | Array<PostPolygonAnnotation> | Array<PostClass> | Array<PostIntegerPrimitive> | Array<PostFloatPrimitive> | Array<PostBoolPrimitive> | Array<PostStringPrimitive> | Array<SlideItem> | Array<IdObject> | Array<PostCollection> | null);

  /**
   * Collection name
   */
  name?: (string | null);

  /**
   * Id of the object referenced by this collection
   */
  reference_id?: (string | null);

  /**
   * Refrence type
   */
  reference_type?: (CollectionReferenceType | null);

  /**
   * Collection type
   */
  type: any;
}
