/* tslint:disable */
/* eslint-disable */
import { IdObject } from './id-object';
import { PostArrowAnnotation } from './post-arrow-annotation';
import { PostBoolPrimitive } from './post-bool-primitive';
import { PostCircleAnnotation } from './post-circle-annotation';
import { PostClass } from './post-class';
import { PostCollection } from './post-collection';
import { PostFloatPrimitive } from './post-float-primitive';
import { PostIntegerPrimitive } from './post-integer-primitive';
import { PostLineAnnotation } from './post-line-annotation';
import { PostPointAnnotation } from './post-point-annotation';
import { PostPolygonAnnotation } from './post-polygon-annotation';
import { PostRectangleAnnotation } from './post-rectangle-annotation';
import { PostStringPrimitive } from './post-string-primitive';
import { SlideItem } from './slide-item';
export interface PostItemList {

  /**
   * Item list
   */
  items: (Array<PostPointAnnotation> | Array<PostLineAnnotation> | Array<PostArrowAnnotation> | Array<PostCircleAnnotation> | Array<PostRectangleAnnotation> | Array<PostPolygonAnnotation> | Array<PostClass> | Array<PostIntegerPrimitive> | Array<PostFloatPrimitive> | Array<PostBoolPrimitive> | Array<PostStringPrimitive> | Array<SlideItem> | Array<IdObject> | Array<PostCollection>);
}
