/* tslint:disable */
/* eslint-disable */
import { PostBoolPrimitive } from './post-bool-primitive';
import { PostFloatPrimitive } from './post-float-primitive';
import { PostIntegerPrimitive } from './post-integer-primitive';
import { PostStringPrimitive } from './post-string-primitive';
export interface PostPrimitiveList {

  /**
   * List of primitives (of same type, e.g. integer)
   */
  items: (Array<PostIntegerPrimitive> | Array<PostFloatPrimitive> | Array<PostBoolPrimitive> | Array<PostStringPrimitive>);
}
