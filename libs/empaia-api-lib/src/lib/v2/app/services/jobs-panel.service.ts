/* tslint:disable */
/* eslint-disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { BaseService } from '../base-service';
import { ApiConfiguration } from '../api-configuration';
import { StrictHttpResponse } from '../strict-http-response';
import { RequestBuilder } from '../request-builder';
import { Observable } from 'rxjs';
import { map, filter } from 'rxjs/operators';

import { IdObject } from '../models/id-object';
import { JobInput } from '../models/job-input';
import { JobListInput } from '../models/job-list-input';
import { JobSlideListInput } from '../models/job-slide-list-input';
import { PostJob } from '../models/post-job';

@Injectable({
  providedIn: 'root',
})
export class JobsPanelService extends BaseService {
  constructor(
    config: ApiConfiguration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * Path part for operation scopeIdJobsGet
   */
  static readonly ScopeIdJobsGetPath = '/{scope_id}/jobs';

  /**
   * Get jobs.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `scopeIdJobsGet()` instead.
   *
   * This method doesn't expect any request body.
   */
  scopeIdJobsGet$Response(params: {
    scope_id: string;
  }): Observable<StrictHttpResponse<JobListInput>> {

    const rb = new RequestBuilder(this.rootUrl, JobsPanelService.ScopeIdJobsGetPath, 'get');
    if (params) {
      rb.path('scope_id', params.scope_id, {});
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json'
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<JobListInput>;
      })
    );
  }

  /**
   * Get jobs.
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `scopeIdJobsGet$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  scopeIdJobsGet(params: {
    scope_id: string;
  }): Observable<JobListInput> {

    return this.scopeIdJobsGet$Response(params).pipe(
      map((r: StrictHttpResponse<JobListInput>) => r.body as JobListInput)
    );
  }

  /**
   * Path part for operation scopeIdJobsPost
   */
  static readonly ScopeIdJobsPostPath = '/{scope_id}/jobs';

  /**
   * Create job.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `scopeIdJobsPost()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  scopeIdJobsPost$Response(params: {
    scope_id: string;
    body: PostJob
  }): Observable<StrictHttpResponse<JobInput>> {

    const rb = new RequestBuilder(this.rootUrl, JobsPanelService.ScopeIdJobsPostPath, 'post');
    if (params) {
      rb.path('scope_id', params.scope_id, {});
      rb.body(params.body, 'application/json');
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json'
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<JobInput>;
      })
    );
  }

  /**
   * Create job.
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `scopeIdJobsPost$Response()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  scopeIdJobsPost(params: {
    scope_id: string;
    body: PostJob
  }): Observable<JobInput> {

    return this.scopeIdJobsPost$Response(params).pipe(
      map((r: StrictHttpResponse<JobInput>) => r.body as JobInput)
    );
  }

  /**
   * Path part for operation scopeIdJobsOutOfScopeGet
   */
  static readonly ScopeIdJobsOutOfScopeGetPath = '/{scope_id}/jobs-out-of-scope';

  /**
   * Get jobs out of scope.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `scopeIdJobsOutOfScopeGet()` instead.
   *
   * This method doesn't expect any request body.
   */
  scopeIdJobsOutOfScopeGet$Response(params: {
    scope_id: string;
  }): Observable<StrictHttpResponse<JobListInput>> {

    const rb = new RequestBuilder(this.rootUrl, JobsPanelService.ScopeIdJobsOutOfScopeGetPath, 'get');
    if (params) {
      rb.path('scope_id', params.scope_id, {});
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json'
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<JobListInput>;
      })
    );
  }

  /**
   * Get jobs out of scope.
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `scopeIdJobsOutOfScopeGet$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  scopeIdJobsOutOfScopeGet(params: {
    scope_id: string;
  }): Observable<JobListInput> {

    return this.scopeIdJobsOutOfScopeGet$Response(params).pipe(
      map((r: StrictHttpResponse<JobListInput>) => r.body as JobListInput)
    );
  }

  /**
   * Path part for operation scopeIdJobsJobIdInputsInputKeyPut
   */
  static readonly ScopeIdJobsJobIdInputsInputKeyPutPath = '/{scope_id}/jobs/{job_id}/inputs/{input_key}';

  /**
   * Update input of job.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `scopeIdJobsJobIdInputsInputKeyPut()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  scopeIdJobsJobIdInputsInputKeyPut$Response(params: {
    scope_id: string;

    /**
     * Id of job that should be updated
     */
    job_id: string;

    /**
     * Identifier for the input to update
     */
    input_key: string;
    body: IdObject
  }): Observable<StrictHttpResponse<JobInput>> {

    const rb = new RequestBuilder(this.rootUrl, JobsPanelService.ScopeIdJobsJobIdInputsInputKeyPutPath, 'put');
    if (params) {
      rb.path('scope_id', params.scope_id, {});
      rb.path('job_id', params.job_id, {});
      rb.path('input_key', params.input_key, {});
      rb.body(params.body, 'application/json');
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json'
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<JobInput>;
      })
    );
  }

  /**
   * Update input of job.
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `scopeIdJobsJobIdInputsInputKeyPut$Response()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  scopeIdJobsJobIdInputsInputKeyPut(params: {
    scope_id: string;

    /**
     * Id of job that should be updated
     */
    job_id: string;

    /**
     * Identifier for the input to update
     */
    input_key: string;
    body: IdObject
  }): Observable<JobInput> {

    return this.scopeIdJobsJobIdInputsInputKeyPut$Response(params).pipe(
      map((r: StrictHttpResponse<JobInput>) => r.body as JobInput)
    );
  }

  /**
   * Path part for operation scopeIdJobsJobIdInputsInputKeyDelete
   */
  static readonly ScopeIdJobsJobIdInputsInputKeyDeletePath = '/{scope_id}/jobs/{job_id}/inputs/{input_key}';

  /**
   * Remove input from job.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `scopeIdJobsJobIdInputsInputKeyDelete()` instead.
   *
   * This method doesn't expect any request body.
   */
  scopeIdJobsJobIdInputsInputKeyDelete$Response(params: {
    scope_id: string;

    /**
     * Id of job that should be updated
     */
    job_id: string;

    /**
     * Identifier for the input to remove
     */
    input_key: string;
  }): Observable<StrictHttpResponse<JobInput>> {

    const rb = new RequestBuilder(this.rootUrl, JobsPanelService.ScopeIdJobsJobIdInputsInputKeyDeletePath, 'delete');
    if (params) {
      rb.path('scope_id', params.scope_id, {});
      rb.path('job_id', params.job_id, {});
      rb.path('input_key', params.input_key, {});
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json'
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<JobInput>;
      })
    );
  }

  /**
   * Remove input from job.
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `scopeIdJobsJobIdInputsInputKeyDelete$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  scopeIdJobsJobIdInputsInputKeyDelete(params: {
    scope_id: string;

    /**
     * Id of job that should be updated
     */
    job_id: string;

    /**
     * Identifier for the input to remove
     */
    input_key: string;
  }): Observable<JobInput> {

    return this.scopeIdJobsJobIdInputsInputKeyDelete$Response(params).pipe(
      map((r: StrictHttpResponse<JobInput>) => r.body as JobInput)
    );
  }

  /**
   * Path part for operation scopeIdJobsJobIdDelete
   */
  static readonly ScopeIdJobsJobIdDeletePath = '/{scope_id}/jobs/{job_id}';

  /**
   * Remove job.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `scopeIdJobsJobIdDelete()` instead.
   *
   * This method doesn't expect any request body.
   */
  scopeIdJobsJobIdDelete$Response(params: {
    scope_id: string;

    /**
     * Id of job that should be updated
     */
    job_id: string;
  }): Observable<StrictHttpResponse<JobInput>> {

    const rb = new RequestBuilder(this.rootUrl, JobsPanelService.ScopeIdJobsJobIdDeletePath, 'delete');
    if (params) {
      rb.path('scope_id', params.scope_id, {});
      rb.path('job_id', params.job_id, {});
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json'
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<JobInput>;
      })
    );
  }

  /**
   * Remove job.
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `scopeIdJobsJobIdDelete$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  scopeIdJobsJobIdDelete(params: {
    scope_id: string;

    /**
     * Id of job that should be updated
     */
    job_id: string;
  }): Observable<JobInput> {

    return this.scopeIdJobsJobIdDelete$Response(params).pipe(
      map((r: StrictHttpResponse<JobInput>) => r.body as JobInput)
    );
  }

  /**
   * Path part for operation scopeIdJobsJobIdRunPut
   */
  static readonly ScopeIdJobsJobIdRunPutPath = '/{scope_id}/jobs/{job_id}/run';

  /**
   * Run job.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `scopeIdJobsJobIdRunPut()` instead.
   *
   * This method doesn't expect any request body.
   */
  scopeIdJobsJobIdRunPut$Response(params: {
    scope_id: string;

    /**
     * Id of job that should be executed
     */
    job_id: string;
  }): Observable<StrictHttpResponse<JobInput>> {

    const rb = new RequestBuilder(this.rootUrl, JobsPanelService.ScopeIdJobsJobIdRunPutPath, 'put');
    if (params) {
      rb.path('scope_id', params.scope_id, {});
      rb.path('job_id', params.job_id, {});
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json'
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<JobInput>;
      })
    );
  }

  /**
   * Run job.
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `scopeIdJobsJobIdRunPut$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  scopeIdJobsJobIdRunPut(params: {
    scope_id: string;

    /**
     * Id of job that should be executed
     */
    job_id: string;
  }): Observable<JobInput> {

    return this.scopeIdJobsJobIdRunPut$Response(params).pipe(
      map((r: StrictHttpResponse<JobInput>) => r.body as JobInput)
    );
  }

  /**
   * Path part for operation scopeIdJobsJobIdStopPut
   */
  static readonly ScopeIdJobsJobIdStopPutPath = '/{scope_id}/jobs/{job_id}/stop';

  /**
   * Stop job.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `scopeIdJobsJobIdStopPut()` instead.
   *
   * This method doesn't expect any request body.
   */
  scopeIdJobsJobIdStopPut$Response(params: {
    scope_id: string;

    /**
     * Id of job that should be executed
     */
    job_id: string;
  }): Observable<StrictHttpResponse<boolean>> {

    const rb = new RequestBuilder(this.rootUrl, JobsPanelService.ScopeIdJobsJobIdStopPutPath, 'put');
    if (params) {
      rb.path('scope_id', params.scope_id, {});
      rb.path('job_id', params.job_id, {});
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json'
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return (r as HttpResponse<any>).clone({ body: String((r as HttpResponse<any>).body) === 'true' }) as StrictHttpResponse<boolean>;
      })
    );
  }

  /**
   * Stop job.
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `scopeIdJobsJobIdStopPut$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  scopeIdJobsJobIdStopPut(params: {
    scope_id: string;

    /**
     * Id of job that should be executed
     */
    job_id: string;
  }): Observable<boolean> {

    return this.scopeIdJobsJobIdStopPut$Response(params).pipe(
      map((r: StrictHttpResponse<boolean>) => r.body as boolean)
    );
  }

  /**
   * Path part for operation scopeIdJobsJobIdSlidesGet
   */
  static readonly ScopeIdJobsJobIdSlidesGetPath = '/{scope_id}/jobs/{job_id}/slides';

  /**
   * Get slides used in a given job.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `scopeIdJobsJobIdSlidesGet()` instead.
   *
   * This method doesn't expect any request body.
   */
  scopeIdJobsJobIdSlidesGet$Response(params: {
    scope_id: string;

    /**
     * Id of job that should be executed
     */
    job_id: string;
  }): Observable<StrictHttpResponse<JobSlideListInput>> {

    const rb = new RequestBuilder(this.rootUrl, JobsPanelService.ScopeIdJobsJobIdSlidesGetPath, 'get');
    if (params) {
      rb.path('scope_id', params.scope_id, {});
      rb.path('job_id', params.job_id, {});
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json'
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<JobSlideListInput>;
      })
    );
  }

  /**
   * Get slides used in a given job.
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `scopeIdJobsJobIdSlidesGet$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  scopeIdJobsJobIdSlidesGet(params: {
    scope_id: string;

    /**
     * Id of job that should be executed
     */
    job_id: string;
  }): Observable<JobSlideListInput> {

    return this.scopeIdJobsJobIdSlidesGet$Response(params).pipe(
      map((r: StrictHttpResponse<JobSlideListInput>) => r.body as JobSlideListInput)
    );
  }

}
