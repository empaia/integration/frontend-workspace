import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ApiModule as WbsAppApiV2Module } from './v2/app/api.module';
import { ApiModule as WbsWbcApiV2Module } from './v2/wbc/api.module';
import { ApiModule as WbsAppApiV3Module } from './v3/app/api.module';
import { ApiModule as WbsWbcApiV3Module } from './v3/wbc/api.module';

// App V2 Services
export { ScopePanelService as AppV2ScopePanelService } from './v2/app/services/scope-panel.service';
export { JobsPanelService as AppV2JobsPanelService } from './v2/app/services/jobs-panel.service';
export { SlidesPanelService as AppV2SlidesPanelService } from './v2/app/services/slides-panel.service';
export { DataPanelService as AppV2DataPanelService } from './v2/app/services/data-panel.service';
export { ApiConfiguration as AppV2ApiConfiguration } from './v2/app/api-configuration';

// Wbc V2 Services
export { CasesPanelService as WbcV2CasesPanelService } from './v2/wbc/services/cases-panel.service';
export { SlidesPanelService as WbcV2SlidesPanelService } from './v2/wbc/services/slides-panel.service';
export { ExaminationsPanelService as WbcV2ExaminationsPanelService } from './v2/wbc/services/examinations-panel.service';
export { ApiConfiguration as WbcV2ApiConfiguration } from './v2/wbc/api-configuration';

// App V3 Services
export { ScopeService as AppV3ScopeService } from './v3/app/services/scope.service';
export { JobsService as AppV3JobsService } from './v3/app/services/jobs.service';
export { SlidesService as AppV3SlidesService } from './v3/app/services/slides.service';
export { DataService as AppV3DataService } from './v3/app/services/data.service';
export { StorageService as AppV3StorageService } from './v3/app/services/storage.service';
export { ApiConfiguration as AppV3ApiConfiguration } from './v3/app/api-configuration';

// Wbc V3 Services
export { CasesService as WbcV3CasesService } from './v3/wbc/services';
export { SlidesService as WbcV3SlidesService } from './v3/wbc/services';
export { ExaminationsService as WbcV3ExaminationsService } from './v3/wbc/services';
export { AppsService as WbcV3AppsService } from './v3/wbc/services';
export { TagsService as WbcV3TagsService } from './v3/wbc/services';
export { ConfigurationService as WbcV3ConfigurationService } from './v3/wbc/services';
export { ApiConfiguration as WbcV3ApiConfiguration } from './v3/wbc/api-configuration';

// App V2 Models
export { AnnotationCountResponse as AppV2AnnotationCountResponse } from './v2/app/models/annotation-count-response';
export { AnnotationList as AppV2AnnotationList } from './v2/app/models/annotation-list';
export { AnnotationListResponse as AppV2AnnotationListResponse } from './v2/app/models/annotation-list-response';
export { AnnotationQuery as AppV2AnnotationQuery } from './v2/app/models/annotation-query';
export { AnnotationQueryPosition as AppV2AnnotationQueryPosition } from './v2/app/models/annotation-query-position';
export { AnnotationReferenceType as AppV2AnnotationReferenceType } from './v2/app/models/annotation-reference-type';
export { AnnotationType as AppV2AnnotationType } from './v2/app/models/annotation-type';
export { AnnotationUniqueClassesQuery as AppV2AnnotationUniqueClassesQuery } from './v2/app/models/annotation-unique-classes-query';
export { AnnotationViewerList as AppV2AnnotationViewerList } from './v2/app/models/annotation-viewer-list';
export { AnnotationViewerQuery as AppV2AnnotationViewerQuery } from './v2/app/models/annotation-viewer-query';
export { ArrowAnnotation as AppV2ArrowAnnotation } from './v2/app/models/arrow-annotation';
export { BoolPrimitive as AppV2BoolPrimitive } from './v2/app/models/bool-primitive';
export { CircleAnnotation as AppV2CircleAnnotation } from './v2/app/models/circle-annotation';
export { Class as AppV2Class } from './v2/app/models/class';
export { ClassList as AppV2ClassList } from './v2/app/models/class-list';
export { ClassListResponse as AppV2ClassListResponse } from './v2/app/models/class-list-response';
export { ClassQuery as AppV2ClassQuery } from './v2/app/models/class-query';
export { ClassReferenceType as AppV2ClassReferenceType } from './v2/app/models/class-reference-type';
export { ClassesDict as AppV2ClassesDict } from './v2/app/models/classes-dict';
export { Collection as AppV2Collection } from './v2/app/models/collection';
export { CollectionItemType as AppV2CollectionItemType } from './v2/app/models/collection-item-type';
export { CollectionList as AppV2CollectionList } from './v2/app/models/collection-list';
export { CollectionQuery as AppV2CollectionQuery } from './v2/app/models/collection-query';
export { CollectionReferenceType as AppV2CollectionReferenceType } from './v2/app/models/collection-reference-type';
export { DataCreatorType as AppV2DataCreatorType } from './v2/app/models/data-creator-type';
export { ExaminationState as AppV2ExaminationState } from './v2/app/models/examination-state';
export { ExtendedScope as AppV2ExtendedScope } from './v2/app/models/extended-scope';
export { FloatPrimitive as AppV2FloatPrimitive } from './v2/app/models/float-primitive';
export { HttpValidationError as AppV2HttpValidationError } from './v2/app/models/http-validation-error';
export { IdObject as AppV2IdObject } from './v2/app/models/id-object';
export { IntegerPrimitive as AppV2IntegerPrimitive } from './v2/app/models/integer-primitive';
export { ItemQuery as AppV2ItemQuery } from './v2/app/models/item-query';
export { ItemQueryList as AppV2ItemQueryList } from './v2/app/models/item-query-list';
export { JobInput as AppV2JobInput } from './v2/app/models/job-input';
export { JobCreatorType as AppV2JobCreatorType } from './v2/app/models/job-creator-type';
export { JobListInput as AppV2JobListInput } from './v2/app/models/job-list-input';
export { JobSlideListInput as AppV2JobSlideListInput } from './v2/app/models/job-slide-list-input';
export { JobStatus as AppV2JobStatus } from './v2/app/models/job-status';
export { LineAnnotation as AppV2LineAnnotation } from './v2/app/models/line-annotation';
export { Message as AppV2Message } from './v2/app/models/message';
export { PointAnnotation as AppV2PointAnnotation } from './v2/app/models/point-annotation';
export { PolygonAnnotation as AppV2PolygonAnnotation } from './v2/app/models/polygon-annotation';
export { PostAnnotationList as AppV2PostAnnotationList } from './v2/app/models/post-annotation-list';
export { PostArrowAnnotation as AppV2PostArrowAnnotation } from './v2/app/models/post-arrow-annotation';
export { PostBoolPrimitive as AppV2PostBoolPrimitive } from './v2/app/models/post-bool-primitive';
export { PostCircleAnnotation as AppV2PostCircleAnnotation } from './v2/app/models/post-circle-annotation';
export { PostClass as AppV2PostClass } from './v2/app/models/post-class';
export { PostClassList as AppV2PostClassList } from './v2/app/models/post-class-list';
export { PostCollection as AppV2PostCollection } from './v2/app/models/post-collection';
export { PostFloatPrimitive as AppV2PostFloatPrimitive } from './v2/app/models/post-float-primitive';
export { PostIntegerPrimitive as AppV2PostIntegerPrimitive } from './v2/app/models/post-integer-primitive';
export { PostItemList as AppV2PostItemList } from './v2/app/models/post-item-list';
export { PostJob as AppV2PostJob } from './v2/app/models/post-job';
export { PostLineAnnotation as AppV2PostLineAnnotation } from './v2/app/models/post-line-annotation';
export { PostPointAnnotation as AppV2PostPointAnnotation } from './v2/app/models/post-point-annotation';
export { PostPolygonAnnotation as AppV2PostPolygonAnnotation } from './v2/app/models/post-polygon-annotation';
export { PostPrimitiveList as AppV2PostPrimitiveList } from './v2/app/models/post-primitive-list';
export { PostRectangleAnnotation as AppV2PostRectangleAnnotation } from './v2/app/models/post-rectangle-annotation';
export { PostStringPrimitive as AppV2PostStringPrimitive } from './v2/app/models/post-string-primitive';
export { PrimitiveList as AppV2PrimitiveList } from './v2/app/models/primitive-list';
export { PrimitiveQuery as AppV2PrimitiveQuery } from './v2/app/models/primitive-query';
export { PrimitiveReferenceType as AppV2PrimitiveReferenceType } from './v2/app/models/primitive-reference-type';
export { PrimitiveType as AppV2PrimitiveType } from './v2/app/models/primitive-type';
export { RectangleAnnotation as AppV2RectangleAnnotation } from './v2/app/models/rectangle-annotation';
export { SlideInput as AppV2SlideInput } from './v2/app/models/slide-input';
export { SlideChannel as AppV2SlideChannel } from './v2/app/models/slide-channel';
export { SlideColor as AppV2SlideColor } from './v2/app/models/slide-color';
export { SlideExtent as AppV2SlideExtent } from './v2/app/models/slide-extent';
export { SlideInfo as AppV2SlideInfo } from './v2/app/models/slide-info';
export { SlideItem as AppV2SlideItem } from './v2/app/models/slide-item';
export { SlideLevel as AppV2SlideLevel } from './v2/app/models/slide-level';
export { SlidePixelSizeNm as AppV2SlidePixelSizeNm } from './v2/app/models/slide-pixel-size-nm';
export { StringPrimitive as AppV2StringPrimitive } from './v2/app/models/string-primitive';
export { TagMapping as AppV2TagMapping } from './v2/app/models/tag-mapping';
export { UniqueClassValues as AppV2UniqueClassValues } from './v2/app/models/unique-class-values';
export { UniqueReferences as AppV2UniqueReferences } from './v2/app/models/unique-references';
export { ValidationError as AppV2ValidationError } from './v2/app/models/validation-error';
export { Viewport as AppV2Viewport } from './v2/app/models/viewport';
export { WorkbenchServiceApiV2CustomModelsSlidesSlideList } from './v2/app/models/workbench-service-api-v-2-custom-models-slides-slide-list';
export { WorkbenchServiceModelsV1AnnotationCollectionsSlideList } from './v2/app/models/workbench-service-models-v-1-annotation-collections-slide-list';

// Wbc V2 Models
export { AppInput as WbcV2AppInput } from './v2/wbc/models/app-input';
export { AppOutput as WbcV2AppOutput } from './v2/wbc/models/app-output';
export { AppListInput as WbcV2AppListInput } from './v2/wbc/models/app-list-input';
export { AppListOutput as WbcV2AppListOutput } from './v2/wbc/models/app-list-output';
export { AppUiConfigSrcPolicies as WbcV2AppUiConfigSrcPolicies } from './v2/wbc/models/app-ui-config-src-policies';
export { AppUiConfiguration as WbcV2AppUiConfiguration } from './v2/wbc/models/app-ui-configuration';
export { AppUiCspConfiguration as WbcV2AppUiCspConfiguration } from './v2/wbc/models/app-ui-csp-configuration';
export { AppUiIframeConfiguration as WbcV2AppUiIframeConfiguration } from './v2/wbc/models/app-ui-iframe-configuration';
export { AppUiTested as WbcV2AppUiTested } from './v2/wbc/models/app-ui-tested';
export { Browser as WbcV2Browser } from './v2/wbc/models/browser';
export { Case as WbcV2Case } from './v2/wbc/models/case';
export { CaseCreatorType as WbcV2CaseCreatorType } from './v2/wbc/models/case-creator-type';
export { CaseList as WbcV2CaseList } from './v2/wbc/models/case-list';
export { Examination as WbcV2Examination } from './v2/wbc/models/examination';
export { ExaminationCreatorType as WbcV2ExaminationCreatorType } from './v2/wbc/models/examination-creator-type';
export { ExaminationList as WbcV2ExaminationList } from './v2/wbc/models/examination-list';
export { ExaminationState as WbcV2ExaminationState } from './v2/wbc/models/examination-state';
export { FrontendToken as WbcV2FrontendToken } from './v2/wbc/models/frontend-token';
export { HttpValidationError as WbcV2HttpValidationError } from './v2/wbc/models/http-validation-error';
export { Message as WbcV2Message } from './v2/wbc/models/message';
export { Os as WbcV2Os } from './v2/wbc/models/os';
export { PostAppResponse as WbcV2PostAppResponse } from './v2/wbc/models/post-app-response';
export { ScopeTokenAndScopeId as WbcV2ScopeTokenAndScopeId } from './v2/wbc/models/scope-token-and-scope-id';
export { Slide as WbcV2Slide } from './v2/wbc/models/slide';
export { SlideChannel as WbcV2SlideChannel } from './v2/wbc/models/slide-channel';
export { SlideColor as WbcV2SlideColor } from './v2/wbc/models/slide-color';
export { SlideExtent as WbcV2SlideExtent } from './v2/wbc/models/slide-extent';
export { SlideInfo as WbcV2SlideInfo } from './v2/wbc/models/slide-info';
export { SlideLevel as WbcV2SlideLevel } from './v2/wbc/models/slide-level';
export { SlideList as WbcV2SlideList } from './v2/wbc/models/slide-list';
export { SlidePixelSizeNm as WbcV2SlidePixelSizeNm } from './v2/wbc/models/slide-pixel-size-nm';
export { TagMapping as WbcV2TagMapping } from './v2/wbc/models/tag-mapping';
export { ValidationError as WbcV2ValidationError } from './v2/wbc/models/validation-error';

// App V3 Models
export { AnnotationCountResponse as AppV3AnnotationCountResponse } from './v3/app/models/annotation-count-response';
export { AnnotationList as AppV3AnnotationList } from './v3/app/models/annotation-list';
export { AnnotationListResponse as AppV3AnnotationListResponse } from './v3/app/models/annotation-list-response';
export { AnnotationQuery as AppV3AnnotationQuery } from './v3/app/models/annotation-query';
export { AnnotationQueryPosition as AppV3AnnotationQueryPosition } from './v3/app/models/annotation-query-position';
export { AnnotationReferenceType as AppV3AnnotationReferenceType } from './v3/app/models/annotation-reference-type';
export { AnnotationType as AppV3AnnotationType } from './v3/app/models/annotation-type';
export { AnnotationUniqueClassesQuery as AppV3AnnotationUniqueClassesQuery } from './v3/app/models/annotation-unique-classes-query';
export { AnnotationViewerList as AppV3AnnotationViewerList } from './v3/app/models/annotation-viewer-list';
export { AnnotationViewerQuery as AppV3AnnotationViewerQuery } from './v3/app/models/annotation-viewer-query';
export { AppUiStorage as AppV3AppUiStorage } from './v3/app/models/app-ui-storage';
export { ArrowAnnotation as AppV3ArrowAnnotation } from './v3/app/models/arrow-annotation';
export { BoolPrimitive as AppV3BoolPrimitive } from './v3/app/models/bool-primitive';
export { CircleAnnotation as AppV3CircleAnnotation } from './v3/app/models/circle-annotation';
export { Class as AppV3Class } from './v3/app/models/class';
export { ClassList as AppV3ClassList } from './v3/app/models/class-list';
export { ClassListResponse as AppV3ClassListResponse } from './v3/app/models/class-list-response';
export { ClassQuery as AppV3ClassQuery } from './v3/app/models/class-query';
export { ClassReferenceType as AppV3ClassReferenceType } from './v3/app/models/class-reference-type';
export { ClassesDict as AppV3ClassesDict } from './v3/app/models/classes-dict';
export { Collection as AppV3Collection } from './v3/app/models/collection';
export { CollectionItemType as AppV3CollectionItemType } from './v3/app/models/collection-item-type';
export { CollectionList as AppV3CollectionList } from './v3/app/models/collection-list';
export { CollectionQuery as AppV3CollectionQuery } from './v3/app/models/collection-query';
export { CollectionReferenceType as AppV3CollectionReferenceType } from './v3/app/models/collection-reference-type';
export { ContinuousPixelmap as AppV3ContinuousPixelmap } from './v3/app/models/continuous-pixelmap';
export { ContinuousPixelmapElementType as AppV3ContinuousPixelmapElementType } from './v3/app/models/continuous-pixelmap-element-type';
export { DataCreatorType as AppV3DataCreatorType } from './v3/app/models/data-creator-type';
export { DiscretePixelmap as AppV3DiscretePixelmap } from './v3/app/models/discrete-pixelmap';
export { DiscretePixelmapElementType as AppV3DiscretePixelmapElementType } from './v3/app/models/discrete-pixelmap-element-type';
export { ExaminationState as AppV3ExaminationState } from './v3/app/models/examination-state';
export { ExtendedScope as AppV3ExtendedScope } from './v3/app/models/extended-scope';
export { FloatPrimitive as AppV3FloatPrimitive } from './v3/app/models/float-primitive';
export { HttpValidationError as AppV3HttpValidationError } from './v3/app/models/http-validation-error';
export { IdObject as AppV3IdObject } from './v3/app/models/id-object';
export { IntegerPrimitive as AppV3IntegerPrimitive } from './v3/app/models/integer-primitive';
export { ItemQuery as AppV3ItemQuery } from './v3/app/models/item-query';
export { ItemQueryList as AppV3ItemQueryList } from './v3/app/models/item-query-list';
export { Job as AppV3Job } from './v3/app/models/job';
export { JobCreatorType as AppV3JobCreatorType } from './v3/app/models/job-creator-type';
export { JobList as AppV3JobList } from './v3/app/models/job-list';
export { JobMode as AppV3JobMode } from './v3/app/models/job-mode';
export { JobSlideList as AppV3JobSlideList } from './v3/app/models/job-slide-list';
export { JobStatus as AppV3JobStatus } from './v3/app/models/job-status';
export { JobValidationStatus as AppV3JobValidationStatus } from './v3/app/models/job-validation-status';
export { LineAnnotation as AppV3LineAnnotation } from './v3/app/models/line-annotation';
export { Message as AppV3Message } from './v3/app/models/message';
export { NominalPixelmap as AppV3NominalPixelmap } from './v3/app/models/nominal-pixelmap';
export { NominalPixelmapElementType as AppV3NominalPixelmapElementType } from './v3/app/models/nominal-pixelmap-element-type';
export { NumberClassMapping as AppV3NumberClassMapping } from './v3/app/models/number-class-mapping';
export { PointAnnotation as AppV3PointAnnotation } from './v3/app/models/point-annotation';
export { PolygonAnnotation as AppV3PolygonAnnotation } from './v3/app/models/polygon-annotation';
export { PostArrowAnnotation as AppV3PostArrowAnnotation } from './v3/app/models/post-arrow-annotation';
export { PostArrowAnnotations as AppV3PostArrowAnnotations } from './v3/app/models/post-arrow-annotations';
export { PostArrowCollection as AppV3PostArrowCollection } from './v3/app/models/post-arrow-collection';
export { PostBoolCollection as AppV3PostBoolCollection } from './v3/app/models/post-bool-collection';
export { PostBoolPrimitive as AppV3PostBoolPrimitive } from './v3/app/models/post-bool-primitive';
export { PostCirceCollection as AppV3PostCirceCollection } from './v3/app/models/post-circe-collection';
export { PostCircleAnnotation as AppV3PostCircleAnnotation } from './v3/app/models/post-circle-annotation';
export { PostClass as AppV3PostClass } from './v3/app/models/post-class';
export { PostClassCollection as AppV3PostClassCollection } from './v3/app/models/post-class-collection';
export { PostClassList as AppV3PostClassList } from './v3/app/models/post-class-list';
export { PostClassesItems as AppV3PostClassesItems } from './v3/app/models/post-classes-items';
export { PostCollections as AppV3PostCollections } from './v3/app/models/post-collections';
export { PostContinuousPixelmap as AppV3PostContinuousPixelmap } from './v3/app/models/post-continuous-pixelmap';
export { PostContinuousPixelmaps as AppV3PostContinuousPixelmaps } from './v3/app/models/post-continuous-pixelmaps';
export { PostDiscretePixelmap as AppV3PostDiscretePixelmap } from './v3/app/models/post-discrete-pixelmap';
export { PostDiscretePixelmaps as AppV3PostDiscretePixelmaps } from './v3/app/models/post-discrete-pixelmaps';
export { PostFloatCollection as AppV3PostFloatCollection } from './v3/app/models/post-float-collection';
export { PostFloatPrimitive as AppV3PostFloatPrimitive } from './v3/app/models/post-float-primitive';
export { PostIdCollection as AppV3PostIdCollection } from './v3/app/models/post-id-collection';
export { PostIdObjects as AppV3PostIdObjects } from './v3/app/models/post-id-objects';
export { PostIntegerCollection as AppV3PostIntegerCollection } from './v3/app/models/post-integer-collection';
export { PostIntegerPrimitive as AppV3PostIntegerPrimitive } from './v3/app/models/post-integer-primitive';
export { PostJob as AppV3PostJob } from './v3/app/models/post-job';
export { PostLineAnnotation as AppV3PostLineAnnotation } from './v3/app/models/post-line-annotation';
export { PostLineCollection as AppV3PostLineCollection } from './v3/app/models/post-line-collection';
export { PostNestedCollection as AppV3PostNestedCollection } from './v3/app/models/post-nested-collection';
export { PostNominalPixelmap as AppV3PostNominalPixelmap } from './v3/app/models/post-nominal-pixelmap';
export { PostNominalPixelmaps as AppV3PostNominalPixelmaps } from './v3/app/models/post-nominal-pixelmaps';
export { PostPointAnnotation as AppV3PostPointAnnotation } from './v3/app/models/post-point-annotation';
export { PostPointCollection as AppV3PostPointCollection } from './v3/app/models/post-point-collection';
export { PostPolygonAnnotation as AppV3PostPolygonAnnotation } from './v3/app/models/post-polygon-annotation';
export { PostPolygonCollection as AppV3PostPolygonCollection } from './v3/app/models/post-polygon-collection';
export { PostRectangleAnnotation as AppV3PostRectangleAnnotation } from './v3/app/models/post-rectangle-annotation';
export { PostRectangleCollection as AppV3PostRectangleCollection } from './v3/app/models/post-rectangle-collection';
export { PostSlideCollection as AppV3PostSlideCollection } from './v3/app/models/post-slide-collection';
export { PostSlideItems as AppV3PostSlideItems } from './v3/app/models/post-slide-items';
export { PostStringCollection as AppV3PostStringCollection } from './v3/app/models/post-string-collection';
export { PostStringPrimitive as AppV3PostStringPrimitive } from './v3/app/models/post-string-primitive';
export { PrimitiveList as AppV3PrimitiveList } from './v3/app/models/primitive-list';
export { PrimitiveQuery as AppV3PrimitiveQuery } from './v3/app/models/primitive-query';
export { PrimitiveReferenceType as AppV3PrimitiveReferenceType } from './v3/app/models/primitive-reference-type';
export { PrimitiveType as AppV3PrimitiveType } from './v3/app/models/primitive-type';
export { RectangleAnnotation as AppV3RectangleAnnotation } from './v3/app/models/rectangle-annotation';
export { Slide as AppV3Slide } from './v3/app/models/slide';
export { SlideChannel as AppV3SlideChannel } from './v3/app/models/slide-channel';
export { SlideColor as AppV3SlideColor } from './v3/app/models/slide-color';
export { SlideExtent as AppV3SlideExtent } from './v3/app/models/slide-extent';
export { SlideInfo as AppV3SlideInfo } from './v3/app/models/slide-info';
export { SlideItem as AppV3SlideItem } from './v3/app/models/slide-item';
export { SlideLevel as AppV3SlideLevel } from './v3/app/models/slide-level';
export { SlidePixelSizeNm as AppV3SlidePixelSizeNm } from './v3/app/models/slide-pixel-size-nm';
export { PixelmapLevel as AppV3PixelmapLevel } from './v3/app/models/pixelmap-level';
export { PixelmapList as AppV3PixelmapList } from './v3/app/models/pixelmap-list';
export { PixelmapQuery as AppV3PixelmapQuery } from './v3/app/models/pixelmap-query';
export { PixelmapReferenceType as AppV3PixelmapReferenceType } from './v3/app/models/pixelmap-reference-type';
export { PixelmapType as AppV3PixelmapType } from './v3/app/models/pixelmap-type';
export { StringPrimitive as AppV3StringPrimitive } from './v3/app/models/string-primitive';
export { TagMapping as AppV3TagMapping } from './v3/app/models/tag-mapping';
export { UniqueClassValues as AppV3UniqueClassValues } from './v3/app/models/unique-class-values';
export { UniqueReferences as AppV3UniqueReferences } from './v3/app/models/unique-references';
export { ValidationError as AppV3ValidationError } from './v3/app/models/validation-error';
export { Viewport as AppV3Viewport } from './v3/app/models/viewport';
export { SlideList as AppV3SlideList } from './v3/app/models/slide-list';
export { PostArrowAnnotationsItems as AppV3PostArrowAnnotationsItems } from './v3/app/models/post-arrow-annotations-items';
export { PostCircleAnnotations as AppV3PostCircleAnnotations } from './v3/app/models/post-circle-annotations';
export { PostCircleAnnotationsItems as AppV3PostCircleAnnotationsItems } from './v3/app/models/post-circle-annotations-items';
export { PostLineAnnotations as AppV3PostLineAnnotations } from './v3/app/models/post-line-annotations';
export { PostLineAnnotationsItems as AppV3PostLineAnnotationsItems } from './v3/app/models/post-line-annotations-items';
export { PostPointAnnotations as AppV3PostPointAnnotations } from './v3/app/models/post-point-annotations';
export { PostPointAnnotationItems as AppV3PostPointAnnotationItems } from './v3/app/models/post-point-annotation-items';
export { PostPolygonAnnotations as AppV3PostPolygonAnnotations } from './v3/app/models/post-polygon-annotations';
export { PostPolygonAnnotationsItems as AppV3PostPolygonAnnotationsItems } from './v3/app/models/post-polygon-annotations-items';
export { PostRectangleAnnotations as AppV3PostRectangleAnnotations } from './v3/app/models/post-rectangle-annotations';
export { PostRectangleAnnotationsItems as AppV3PostRectangleAnnotationsItems } from './v3/app/models/post-rectangle-annotations-items';
export { PostBoolPrimitives as AppV3PostBoolPrimitives } from './v3/app/models/post-bool-primitives';
export { PostBoolPrimitivesItems as AppV3PostBoolPrimitivesItems } from './v3/app/models/post-bool-primitives-items';
export { PostFloatPrimitives as AppV3PostFloatPrimitives } from './v3/app/models/post-float-primitives';
export { PostFloatPrimitivesItems as AppV3PostFloatPrimitivesItems } from './v3/app/models/post-float-primitives-items';
export { PostIntegerPrimitives as AppV3PostIntegerPrimitives } from './v3/app/models/post-integer-primitives';
export { PostIntegerPrimitivesItems as AppV3PostIntegerPrimitivesItems } from './v3/app/models/post-integer-primitives-items';
export { PostStringPrimitives as AppV3PostStringPrimitives } from './v3/app/models/post-string-primitives';
export { PostStringPrimitivesItems as AppV3PostStringPrimitivesItems } from './v3/app/models/post-string-primitives-items';
export { SlideItemList as AppV3SlideItemList } from './v3/app/models/slide-item-list';

// Wbc V3 Models
export { AppInput as WbcV3AppInput } from './v3/wbc/models/app-input';
export { AppOutput as WbcV3AppOutput } from './v3/wbc/models/app-output';
export { AppList as WbcV3AppList } from './v3/wbc/models/app-list';
export { AppQuery as WbcV3AppQuery } from './v3/wbc/models/app-query';
export { AppTagInput as WbcV3AppTagInput } from './v3/wbc/models/app-tag-input';
export { AppTagOutput as Wbc3AppTagOutput } from './v3/wbc/models/app-tag-output';
export { AppUiConfigSrcPolicies as WbcV3AppUiConfigSrcPolicies } from './v3/wbc/models/app-ui-config-src-policies';
export { AppUiConfiguration as WbcV3AppUiConfiguration } from './v3/wbc/models/app-ui-configuration';
export { AppUiCspConfiguration as WbcV3AppUiCspConfiguration } from './v3/wbc/models/app-ui-csp-configuration';
export { AppUiIframeConfiguration as WbcV3AppUiIframeConfiguration } from './v3/wbc/models/app-ui-iframe-configuration';
export { AppUiTested as WbcV3AppUiTested } from './v3/wbc/models/app-ui-tested';
export { Browser as WbcV3Browser } from './v3/wbc/models/browser';
export { Case as WbcV3Case } from './v3/wbc/models/case';
export { CaseCreatorType as WbcV3CaseCreatorType } from './v3/wbc/models/case-creator-type';
export { CaseList as WbcV3CaseList } from './v3/wbc/models/case-list';
export { ExaminationCreatorType as WbcV3ExaminationCreatorType } from './v3/wbc/models/examination-creator-type';
export { ExaminationList as WbcV3ExaminationList } from './v3/wbc/models/examination-list';
export { ExaminationQuery as WbcV3ExaminationQuery } from './v3/wbc/models/examination-query';
export { ExaminationState as WbcV3ExaminationState } from './v3/wbc/models/examination-state';
export { ExtendedPreprocessingTrigger as WbcV3ExtendedPreprocessingTrigger } from './v3/wbc/models/extended-preprocessing-trigger';
export { ExtendedPreprocessingTriggerList as WbcV3ExtendedPreprocessingTriggerList } from './v3/wbc/models/extended-preprocessing-trigger-list';
export { FrontendToken as WbcV3FrontendToken } from './v3/wbc/models/frontend-token';
export { HttpValidationError as WbcV3HttpValidationError } from './v3/wbc/models/http-validation-error';
export { Job as WbcV3Job } from './v3/wbc/models/job';
export { JobCreatorType as WbcV3JobCreatorType } from './v3/wbc/models/job-creator-type';
export { JobMode as WbcV3JobMode } from './v3/wbc/models/job-mode';
export { JobStatus as WbcV3JobStatus } from './v3/wbc/models/job-status';
export { JobValidationStatus as WbcV3JobValidationStatus } from './v3/wbc/models/job-validation-status';
export { Language as WbcV3Language } from './v3/wbc/models/language';
export { Message as WbcV3Message } from './v3/wbc/models/message';
export { Os as WbcV3Os } from './v3/wbc/models/os';
export { PostExamination as WbcV3PostExamination } from './v3/wbc/models/post-examination';
export { PostPreprocessingTrigger as WbcV3PostPreprocessingTrigger } from './v3/wbc/models/post-preprocessing-trigger';
export { PreprocessingProgress as WbcV3PreprocessingProgress } from './v3/wbc/models/preprocessing-progress';
export { ProprocessingTriggerCreatorType as WbcV3ProprocessingTriggerCreatorType } from './v3/wbc/models/proprocessing-trigger-creator-type';
export { ScopeTokenAndScopeId as WbcV3ScopeTokenAndScopeId } from './v3/wbc/models/scope-token-and-scope-id';
export { Slide as WbcV3Slide } from './v3/wbc/models/slide';
export { SlideChannel as WbcV3SlideChannel } from './v3/wbc/models/slide-channel';
export { SlideColor as WbcV3SlideColor } from './v3/wbc/models/slide-color';
export { SlideExtent as WbcV3SlideExtent } from './v3/wbc/models/slide-extent';
export { SlideInfo as WbcV3SlideInfo } from './v3/wbc/models/slide-info';
export { SlideLevel as WbcV3SlideLevel } from './v3/wbc/models/slide-level';
export { SlideList as WbcV3SlideList } from './v3/wbc/models/slide-list';
export { SlidePixelSizeNm as WbcV3SlidePixelSizeNm } from './v3/wbc/models/slide-pixel-size-nm';
export { TagListInput as WbcV3TagListInput } from './v3/wbc/models/tag-list-input';
export { TagListOutput as WbcV3TagListOutput } from './v3/wbc/models/tag-list-output';
export { TagMapping as WbcV3TagMapping } from './v3/wbc/models/tag-mapping';
export { TextTranslation as WbcV3TextTranslation } from './v3/wbc/models/text-translation';
export { ValidationError as WbcV3ValidationError } from './v3/wbc/models/validation-error';
export { WorkbenchServiceApiV3CustomModelsExaminationsExamination as WbcV3WorkbenchServiceApiV3CustomModelsExaminationsExamination } from './v3/wbc/models/workbench-service-api-v-3-custom-models-examinations-examination';
export { WorkbenchServiceModelsV3ExaminationExamination as WbcV3WorkbenchServiceModelsV3ExaminationExamination } from './v3/wbc/models/workbench-service-models-v-3-examination-examination';


export {
  WbsAppApiV2Module,
  WbsAppApiV3Module,
  WbsWbcApiV2Module,
  WbsWbcApiV3Module,
};

@NgModule({
  imports: [
    CommonModule,
    WbsAppApiV2Module,
    WbsAppApiV3Module,
    WbsWbcApiV2Module,
    WbsWbcApiV3Module,
  ],
  exports: [
    WbsAppApiV2Module,
    WbsAppApiV3Module,
    WbsWbcApiV2Module,
    WbsWbcApiV3Module,
  ]
})
export class EmpaiaApiLibModule {}
