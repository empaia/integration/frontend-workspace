import { calculateHexAlpha } from './pixelmap-rendering-collection';

function createHexRgbColors(colorSpecifier: string): ReadonlyArray<string> {
  const n = colorSpecifier.length / 6;
  const colors = new Array<string>(n);
  let i = 0;

  while (i < n) {
    colors[i] = '#' + colorSpecifier.slice(i * 6, ++i * 6);
  }

  return colors;
}

export const Tableau9 = createHexRgbColors('4e79a7f28e2ce1575976b7b2edc949af7aa1ff9da79c755fbab0ab');
export const Vibrant = createHexRgbColors('ee77330077bb33bbeeee3377cc3311009988bbbbbb');

function interpolateBasicAlpha(t: number, negative: string, positive: string): string {
  t = Math.max(0, Math.min(1, t));
  const sign = t < 0.5 ? negative : positive;
  return `#${sign}${calculateHexAlpha(t, 0)}`;
}

export function interpolateBuRdAlpha(t: number): string {
  return interpolateBasicAlpha(t, '0061a7', 'b80a00');
}

export function interpolatePuOrAlpha(t: number): string {
  return interpolateBasicAlpha(t, '30005d', 'ed8100');
}

export function interpolateCividisAlpha(t: number): string {
  return interpolateBasicAlpha(t, '002051', 'fde945');
}

export function interpolatePlasmaAlpha(t: number): string {
  return interpolateBasicAlpha(t, '7000ab', 'fda326');
}
