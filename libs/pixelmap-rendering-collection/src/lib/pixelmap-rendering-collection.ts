/* eslint-disable @typescript-eslint/no-explicit-any */
// **constance's**
const BLACK = '#000000';

// **types**
export type PixelMapDataType = Uint8Array
| Uint16Array
| Uint32Array
| Int8Array
| Int16Array
| Int32Array
| Float32Array
| Float64Array;

export type PixelMapBigDataType = BigUint64Array | BigInt64Array;

export type ColorMapFn = (t: number) => string;

// **interfaces**
export interface ElementColorMapping {
  [key: string]: string;
}

// **classes**
export class EnhancedMap<K, V> extends Map<K, V> {
  public pop(): V {
    const [key, value] = this.entries().next().value;
    this.delete(key);
    return value;
  }

  public popMany(count: number): V[] {
    const iterator = this.entries();
    const removedValues: V[] = [];
    let iteratorResult = iterator.next();
    let index = 0;

    while (index < count && !iteratorResult.done) {
      const [key, value] = iteratorResult.value;
      this.delete(key);
      removedValues.push(value);
      index++;
      iteratorResult = iterator.next();
    }

    return removedValues;
  }
}

export class CanvasDrawCache {
  private static _cache: CanvasDrawCache;
  private _valueColorMap = new EnhancedMap<string, string>();
  private _cacheSize = 524288; // 4MB --> 4194304 Byte / 8 Byte (64Bit) = 524288

  // eslint-disable-next-line @typescript-eslint/no-empty-function
  private constructor() {}

  public static getCanvasDrawCache(): CanvasDrawCache {
    if (!this._cache) {
      this._cache = new CanvasDrawCache();
    }

    return this._cache;
  }

  public set cacheSize(val: number) {
    this._cacheSize = val;
  }

  public get cacheSize(): number {
    return this._cacheSize;
  }

  public addValueColor(value: PixelMapDataType | PixelMapBigDataType, color: string): void {
    this._valueColorMap.set(value.toString(), color);

    const popCount = this._valueColorMap.size - this._cacheSize;

    if (popCount > 0) {
      this._valueColorMap.popMany(popCount);
    }
  }

  public getValueColor(value: PixelMapDataType | PixelMapBigDataType): string | undefined {
    return this._valueColorMap.get(value.toString());
  }

  public hasValueColor(value: PixelMapDataType | PixelMapBigDataType): boolean {
    return this._valueColorMap.has(value.toString());
  }

  public removeValueColor(value: PixelMapDataType | PixelMapBigDataType): boolean {
    return this._valueColorMap.delete(value.toString());
  }

  public clearCache(): void {
    this._valueColorMap.clear();
  }
}

export class PixelMapTileCache {
  private static _cache: PixelMapTileCache;
  private _dataCache = new EnhancedMap<string, PixelMapDataType | PixelMapBigDataType>();
  private _canvasCache = new EnhancedMap<string, HTMLCanvasElement>();
  private _cacheSize = 200;

  // eslint-disable-next-line @typescript-eslint/no-empty-function
  private constructor() {}

  public static getPixelMapTileCache(): PixelMapTileCache {
    if (!this._cache) {
      this._cache = new PixelMapTileCache();
    }

    return this._cache;
  }

  public set cacheSize(val: number) {
    this._cacheSize = val;
  }

  public get cacheSize(): number {
    return this._cacheSize;
  }

  public addTileDataToCache(data: PixelMapDataType | PixelMapBigDataType, level: number, x: number, y: number): void {
    this._dataCache.set(`${level}${x}${y}`, data);

    const popCount = this._dataCache.size - this._cacheSize;

    if (popCount > 0) {
      this._dataCache.popMany(popCount);
    }
  }

  public getTileDataFromCache(level: number, x: number, y: number): PixelMapDataType | PixelMapBigDataType | undefined {
    return this._dataCache.get(`${level}${x}${y}`);
  }

  public hasTileData(level: number, x: number, y: number): boolean {
    return this._dataCache.has(`${level}${x}${y}`);
  }

  public removeTileDataFromCache(level: number, x: number, y: number): boolean {
    return this._dataCache.delete(`${level}${x}${y}`);
  }

  public clearTileDataCache(): void {
    this._dataCache.clear();
  }

  public addCanvasToCache(canvas: HTMLCanvasElement, channel: number, level: number, x: number, y: number): void {
    this._canvasCache.set(`${channel}${level}${x}${y}`, canvas);

    const popCount = this._canvasCache.size - this._cacheSize * 3;

    if (popCount > 0) {
      this._canvasCache.popMany(popCount);
    }
  }

  public getCanvasFromCache(channel: number, level: number, x: number, y: number): HTMLCanvasElement | undefined {
    return this._canvasCache.get(`${channel}${level}${x}${y}`);
  }

  public hasCanvas(channel: number, level: number, x: number, y: number): boolean {
    return this._canvasCache.has(`${channel}${level}${x}${y}`);
  }

  public removeCanvasFromCache(channel: number, level: number, x: number, y: number): boolean {
    return this._canvasCache.delete(`${channel}${level}${x}${y}`);
  }

  public clearCanvasCache(): void {
    this._canvasCache.clear();
  }

  public clearCache(): void {
    this.clearTileDataCache();
    this.clearCanvasCache();
  }
}

export class PixelMapCredentialManager {
  private static _manager: PixelMapCredentialManager;

  private _colorMapFn?: ColorMapFn | ElementColorMapping;
  private _invert?: boolean;

  // eslint-disable-next-line @typescript-eslint/no-empty-function
  private constructor() {}

  public static getPixelMapCredentialManager(): PixelMapCredentialManager {
    if (!this._manager) {
      this._manager = new PixelMapCredentialManager();
    }

    return this._manager;
  }

  public set colorMapFn(val: ColorMapFn | ElementColorMapping | undefined) {
    this._colorMapFn = val;
  }

  public get colorMapFn(): ColorMapFn | ElementColorMapping | undefined {
    return this._colorMapFn;
  }

  public set inversion(val: boolean | undefined) {
    this._invert = val;
  }

  public get inversion(): boolean | undefined {
    return this._invert;
  }

  public setAllCredentials(
    colorMapFn?: ColorMapFn | ElementColorMapping,
    inversion?: boolean,
  ): void {
    this._colorMapFn = colorMapFn;
    this._invert = inversion;
  }

  public clear(): void {
    this._colorMapFn = undefined;
    this._invert = undefined;
  }
}

// **functions**
export function drawColorMapImage(
  canvas: HTMLCanvasElement,
  data: PixelMapDataType,
  // canvasDrawCache: CanvasDrawCache,
  channel: number,
  min: number,
  max: number,
  colorMapFunction: ((t: number) => string),
  invert: boolean,
  neutral?: number,
): void;
export function drawColorMapImage(
  canvas: HTMLCanvasElement,
  data: PixelMapBigDataType,
  // canvasDrawCache: CanvasDrawCache,
  channel: number,
  min: bigint,
  max: bigint,
  colorMapFunction: ((t: number) => string),
  invert: boolean,
  neutral?: bigint,
): void;
export function drawColorMapImage(
  canvas: HTMLCanvasElement,
  data: any,
  // canvasDrawCache: CanvasDrawCache,
  channel: number,
  min: any,
  max: any,
  colorMapFunction: ((t: number) => string),
  invert: boolean,
  neutral?: any,
): void {
  const ctx = canvas.getContext('2d');

  if (!ctx) {
    throw Error('Canvas 2d context can not be created');
  }

  ctx.clearRect(0, 0, canvas.width, canvas.height);

  const start = canvas.width * canvas.height * channel;
  const end = canvas.width * canvas.height * (channel + 1);

  let xStart = 0;
  let width = 0;
  let rgb: string;

  for (let i = start; i < end; i++) {
    const pixel = data[i];
    rgb = calculateRgbValue(pixel, min, max, colorMapFunction, invert, neutral);

    // if (canvasDrawCache.hasValueColor(pixel)) {
    //   rgb = canvasDrawCache.getValueColor(pixel) as string;
    // } else {
    //   rgb = calculateRgbValue(pixel, min, max, colorMapFunction, invert, neutral);
    //   canvasDrawCache.addValueColor(pixel, rgb);
    // }

    // const y = Math.floor((i - start) / canvas.width);
    // const x = i % canvas.width;
    // ctx.fillStyle = rgb;
    // ctx.fillRect(x, y, 1, 1);

    width++;
    const y = Math.floor((i - start) / canvas.width);

    if (pixel !== data[i+1]) {
      ctx.fillStyle = rgb;
      ctx.fillRect(xStart, y, width, 1);
      xStart = (i+1) % canvas.width;
      width = 0;
    } else if ((i+1) % canvas.width === 0) {
      ctx.fillStyle = rgb;
      ctx.fillRect(xStart, y, width, 1);
      xStart = (i+1) % canvas.width;
      width = 0;
    }
  }
}

export function drawColorMapImageFromMap(
  canvas: HTMLCanvasElement,
  data: PixelMapDataType,
  channel: number,
  elementMap: ElementColorMapping,
): void;
export function drawColorMapImageFromMap(
  canvas: HTMLCanvasElement,
  data: PixelMapBigDataType,
  channel: number,
  elementMap: ElementColorMapping,
): void;
export function drawColorMapImageFromMap(
  canvas: HTMLCanvasElement,
  data: any,
  channel: number,
  elementMap: ElementColorMapping,
): void {
  const ctx = canvas.getContext('2d');

  if (!ctx) {
    throw Error('Canvas 2d context can not be created');
  }

  ctx.clearRect(0, 0, canvas.width, canvas.height);

  const start = canvas.width * canvas.height * channel;
  const end = canvas.width * canvas.height * (channel + 1);
  const defaultColor = BLACK;
  let rgb: string;
  let xStart = 0;
  let width = 0;

  for (let i = start; i < end; i++) {
    const pixel = data[i];
    rgb = elementMap[pixel] ?? defaultColor;

    width++;
    const y = Math.floor((i - start) / canvas.width);

    if (pixel !== data[i+1]) {
      ctx.fillStyle = rgb;
      ctx.fillRect(xStart, y, width, 1);
      xStart = (i+1) % canvas.width;
      width = 0;
    } else if ((i+1) % canvas.width === 0) {
      ctx.fillStyle = rgb;
      ctx.fillRect(xStart, y, width, 1);
      xStart = (i+1) % canvas.width;
      width = 0;
    }
  }
}

export function calculateRgbValue(
  value: number,
  min: number,
  max: number,
  colorMapFunction: (t: number) => string,
  invert?: boolean,
  neutral?: number,
): string;
export function calculateRgbValue(
  value: bigint,
  min: bigint,
  max: bigint,
  colorMapFunction: (t: number) => string,
  invert?: boolean,
  neutral?: bigint,
): string;
export function calculateRgbValue(
  value: any,
  min: any,
  max: any,
  colorMapFunction: (t: number) => string,
  invert?: boolean,
  neutral?: any,
): string {
  let normal = neutral ? normalizeNeutral(value, min, max, neutral) : normalize(value, min, max);
  normal = invert ? 1 - normal : normal;
  return colorMapFunction(normal);
  // return neutral ? addAlphaToRgbString(colorMapFunction(normal), calculateAlpha(normal, transparencyFactor)) : colorMapFunction(normal);
}

export function addAlphaToRgbString(rgb: string, alpha: number): string {
  const first = rgb.indexOf('(');
  const second = rgb.indexOf(')');

  return rgb.slice(0, first).concat('a', rgb.slice(first, second).concat(', ', alpha.toString(), ')'));
}

export function addAlphaToRgbHexString(rgb: string, alpha: string): string {
  return rgb + alpha;
}

export function calculateAlpha(value: number, offset = 1.0): number {
  if (value < 0.0 || value > 1.0) {
    throw Error('Value is not normalized');
  }

  return offset + Math.abs(0.5 - value) * 2;
  // return offset + (value < 0.5 ? 1 - normalize(value, 0.0, 0.5) : normalize(value, 0.5, 1.0));
}

export function calculateHexAlpha(value: number, offset = 1.0): string {
  if (offset < 0.0 || offset > 1.0) {
    throw Error('Value is not normalized');
  }

  const alphaValue = calculateAlpha(value, offset) * 255;
  let alpha = Math.round(Math.max(0, Math.min(255, alphaValue))).toString(16);
  if (alpha.length < 2) {
    alpha = '0' + alpha;
  }
  return alpha;
}

export function normalize(value: number, min: number, max: number): number;
export function normalize(value: bigint, min: bigint, max: bigint): number;
export function normalize(value: any, min: any, max: any): number {
  return Math.max(Math.min(Number(value - min) / Number(max - min), 1.0), 0.0);
}

export function normalizeNeutral(value: number, min: number, max: number, neutral: number): number;
export function normalizeNeutral(value: bigint, min: bigint, max: bigint, neutral: bigint): number;
export function normalizeNeutral(value: any, min: any, max: any, neutral: any): number {
  return value < neutral
    ? neutral !== min
      ? normalize(value, min, neutral) / 2.0
      : 0
    : neutral !== max
      ? normalize(value, neutral, max) / 2.0 + 0.5
      : 1;
}

export function isPixelMapBigDataType(data: PixelMapDataType | PixelMapBigDataType): data is PixelMapBigDataType {
  return data instanceof BigInt64Array || data instanceof BigUint64Array;
}
