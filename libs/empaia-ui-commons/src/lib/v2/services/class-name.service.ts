/* eslint-disable @typescript-eslint/no-explicit-any */
import { Injectable } from '@angular/core';
import { ClassDictionaries, ClassEntity, ClassesDict } from '../../shared/models/classes.models';

@Injectable({
  providedIn: 'root'
})
export class ClassNameConversionService {
  public getClassEntries(obj: ClassDictionaries | ClassesDict): string[] {
    return Object.entries(obj).map(([k ,v]) => {
      return v instanceof Object ? v['name'] ? k : k + '.' + this.getClassEntries(v): k;
    });
  }

  public getNamespaces(classDictionaries: ClassDictionaries): string[] {
    return this.getClassEntries(classDictionaries).map(e => e.slice(0, e.lastIndexOf('.')));
  }

  public getNamespaceKeys(classDictionaries: ClassDictionaries): string[] {
    return Object.keys(classDictionaries);
  }

  public getClassNames(classes: ClassDictionaries | ClassesDict): any[] {
    return Object.entries(classes).map(([k, v]) => v instanceof Object ? v['name'] ? k : this.getClassNames(v) : k);
  }

  public getClassObjects(classes: ClassesDict): object[] {
    return Object.values(classes.classes);
  }

  public getClassName(classEntity: any): string {
    return classEntity['name'];
  }

  public getClassDescription(classEntity: any): string | undefined {
    return classEntity['description'];
  }

  private flatClass(r: any[], a: any[]): any[] {
    if (Array.isArray(a[0])) {
      return a.reduce(this.flatClass.bind(this), r);
    }
    r.push(a);
    return r;
  }

  public flatClassNames(classNames: any[]): any[] {
    return classNames.reduce(this.flatClass.bind(this), []);
  }

  public getClasses(classDictionaries: ClassDictionaries): ClassEntity[] {
    const namespaces = this.getNamespaces(classDictionaries);
    const namespaceKeys = this.getNamespaceKeys(classDictionaries);
    const classNames = this.flatClassNames(this.getClassNames(classDictionaries));
    const classes: ClassEntity[] = [];

    namespaces.forEach((namespace, i) => {
      const classEntities = this.getClassObjects(classDictionaries[namespaceKeys[i]]);
      classNames[i].forEach((c: string, j: number) => {
        classes.push({ id: namespace +'.' + c, name: this.getClassName(classEntities[j]), description: this.getClassDescription(classEntities[j]) });
      });
    });

    // add undefined class (null)
    classes.push({ id: 'null', name: 'Undefined' });

    return classes;
  }
}
