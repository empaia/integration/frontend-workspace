import { ModuleWithProviders, NgModule, Optional, SkipSelf } from '@angular/core';
import { EadService } from './shared/services/ead.service';
import { PixelMapsConversionService } from './v3/services/pixel-maps-conversion.service';
import { ClassNameConversionService as ClassNameConversionServiceV3 } from './v3/services/class-name.service';
import { AnnotationsColorMapsService } from './shared/services/annotations-color-maps.service';
import { CommonModule } from '@angular/common';
import { CommonUiModule } from './shared/common-ui/common-ui.module';
import { ClassNameConversionService as ClassNameConversionServiceV2 } from './v2/services/class-name.service';

// shared between all version exports
export * from './shared/models/annotations-color-maps.models';
export * from './shared/models/ead.models';
export * from './shared/models/scope.models';
export * from './shared/models/classes.models';
export * from './shared/models/http-headers.models';

export { WsiTileRequester } from './shared/classes/wsi-tile-requester';

// v2 exports
export {
  ClassNameConversionServiceV2,
};

// v3 exports
export * from './v3/models/pixel-maps.models';

// export { readTiffArrayBuffer } from './v3/services/reader-service';
export { PixelMapTileRequester } from './v3/classes/pixel-map-tile-requester';
export { PixelMapsTileService } from './v3/services/pixel-maps-tile.service';
export {
  AnnotationsColorMapsService,
  ClassNameConversionServiceV3,
  EadService,
  PixelMapsConversionService,
  CommonUiModule,
};


@NgModule({
  imports: [
    CommonModule,
  ],
  providers: [
    AnnotationsColorMapsService,
    ClassNameConversionServiceV3,
    EadService,
    PixelMapsConversionService,
    ClassNameConversionServiceV2,
  ],
  exports: [
    CommonUiModule,
  ],
})
export class EmpaiaUiCommonsModule {
  static forRoot(): ModuleWithProviders<EmpaiaUiCommonsModule> {
    return {
      ngModule: EmpaiaUiCommonsModule,
    };
  }

  constructor(
  @Optional() @SkipSelf() parentModule: EmpaiaUiCommonsModule,
  ) {
    if (parentModule) {
      throw new Error('EmpaiaUiCommonsModule is already loaded. Import in your base AppModule only!');
    }
  }
}
