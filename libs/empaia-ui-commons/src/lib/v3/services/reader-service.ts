// import { fromArrayBuffer, GeoTIFF, GeoTIFFImage } from 'geotiff';
// import { TypedArray } from 'geotiff/dist-module/geotiffimage';

// Don't delete that, in case we want to reuse tiff formate
// data loading

// export async function readTiffArrayBuffer(
//   arrayBuffer: ArrayBuffer
// ): Promise<Uint8Array[] | Uint16Array[] | Uint32Array[]> {
//   const geotiff: GeoTIFF = await fromArrayBuffer(arrayBuffer);
//   const image: GeoTIFFImage = await geotiff.getImage();
//   const rasters: TypedArray | TypedArray[] = await image.readRasters();

//   if (
//     rasters.every(
//       (raster) =>
//         raster instanceof Uint8Array ||
//         raster instanceof Uint16Array ||
//         raster instanceof Uint32Array
//     )
//   ) {
//     return rasters as Uint8Array[] | Uint16Array[] | Uint32Array[];
//   } else {
//     throw new Error('not supported raster type');
//   }
// }
