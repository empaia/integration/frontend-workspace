/* eslint-disable @typescript-eslint/no-explicit-any */
import { Injectable } from '@angular/core';
import { ApiPixelMap, ColorMapFn, ElementClassMapping, LevelDetailsMap, PixelMap, RECOMMENDED_PIXEL_MAP_COLOR_MAP_ID, isApiNominalPixelMap, isContinuousPixelMap, isDiscretePixelMap } from '../models/pixel-maps.models';
import { addAlphaToRgbHexString, calculateRgbValue } from 'pixelmap-rendering-collection';
import { CategoricalColorMap, RenderingHint } from '../../empaia-ui-commons.module';
import { Dictionary } from '@ngrx/entity';
import { ClassEntity } from '../../shared/models/classes.models';
import { ChannelClassMapping } from 'slide-viewer';
import { AppV3NumberClassMapping } from 'empaia-api-lib';

@Injectable({
  providedIn: 'root'
})
export class PixelMapsConversionService {
  public addColorToDiscreteElementClassMapping(
    min: number,
    max: number,
    mapping?: ElementClassMapping[],
    colorMap?: ColorMapFn,
    invert?: boolean,
    neutral?: number,
  ): ElementClassMapping[] | undefined;
  public addColorToDiscreteElementClassMapping(
    min: bigint,
    max: bigint,
    mapping?: ElementClassMapping[],
    colorMap?: ColorMapFn,
    invert?: boolean,
    neutral?: bigint,
  ): ElementClassMapping[] | undefined;
  public addColorToDiscreteElementClassMapping(
    min: any,
    max: any,
    mapping?: ElementClassMapping[],
    colorMap?: ColorMapFn,
    invert?: boolean,
    neutral?: any,
  ): ElementClassMapping[] | undefined {
    if (!mapping || !colorMap) {
      return undefined;
    }
    return mapping.map(elem => ({ ...elem, color: calculateRgbValue(elem.number_value, min, max, colorMap, invert, neutral )}));
  }

  public addColorToNominalElementClassMapping(
    mapping?: ElementClassMapping[],
    colorMap?: readonly string[],
    invert?: boolean,
    neutral?: number | null,
    renderingHints?: RenderingHint[],
    useHints?: boolean,
  ): ElementClassMapping[] | undefined;
  public addColorToNominalElementClassMapping(
    mapping?: ElementClassMapping[],
    colorMap?: readonly string[],
    invert?: boolean,
    neutral?: bigint | null,
    renderingHints?: RenderingHint[],
    useHints?: boolean,
  ): ElementClassMapping[] | undefined;
  public addColorToNominalElementClassMapping(
    mapping?: ElementClassMapping[],
    colorMap?: readonly string[],
    invert?: boolean,
    neutral?: any | null,
    renderingHints?: RenderingHint[],
    useHints?: boolean,
  ): ElementClassMapping[] | undefined {
    if (!mapping) { return undefined; }

    return renderingHints?.length && useHints
      ? this.addColorToNominalElementClassMappingWithHint(
        renderingHints,
        mapping,
        neutral,
      )
      : this.addColorToNominalElementClassMappingWithoutHint(
        mapping,
        colorMap,
        invert,
        neutral,
      );
  }

  public addColorToNominalElementClassMappingWithoutHint(
    mapping?: ElementClassMapping[],
    colorMap?: readonly string[],
    invert?: boolean,
    neutral?: number | null,
  ): ElementClassMapping[] | undefined;
  public addColorToNominalElementClassMappingWithoutHint(
    mapping?: ElementClassMapping[],
    colorMap?: readonly string[],
    invert?: boolean,
    neutral?: bigint | null,
  ): ElementClassMapping[] | undefined;
  public addColorToNominalElementClassMappingWithoutHint(
    mapping?: ElementClassMapping[],
    colorMap?: readonly string[],
    invert?: boolean,
    neutral?: any | null,
  ): ElementClassMapping[] | undefined {
    if (!mapping || !colorMap) {
      return undefined;
    }
    return mapping.map((elem, i) => ({
      ...elem,
      color: addAlphaToRgbHexString(colorMap[invert ? (colorMap.length - 1) - i % colorMap.length : i % colorMap.length], elem.number_value === neutral ? '00' : 'ff')
    }));
  }

  public addColorToNominalElementClassMappingWithHint(
    renderingHints: RenderingHint[],
    mapping?: ElementClassMapping[],
    neutral?: number | null
  ): ElementClassMapping[] | undefined;
  public addColorToNominalElementClassMappingWithHint(
    renderingHints: RenderingHint[],
    mapping?: ElementClassMapping[],
    neutral?: bigint | null
  ): ElementClassMapping[] | undefined;
  public addColorToNominalElementClassMappingWithHint(
    renderingHints: RenderingHint[],
    mapping?: ElementClassMapping[],
    neutral?: any | null
  ): ElementClassMapping[] | undefined {
    if (!mapping || !renderingHints.length) {
      return undefined;
    }

    return mapping.map(elem => {
      const found = renderingHints.find(hint => hint.class_value === elem.class_value);
      return {
        ...elem,
        color: elem.number_value === neutral ? '#00000000' : found?.color ?? '#ffffffff'
      };
    }).sort((a: ElementClassMapping, b: ElementClassMapping) => {
      const foundA = renderingHints.find(hint => hint.class_value === a.class_value);
      const foundB = renderingHints.find(hint => hint.class_value === b.class_value);
      const indexA = foundA ? renderingHints.indexOf(foundA) : -1;
      const indexB = foundB ? renderingHints.indexOf(foundB) : -1;
      return indexA < 0 ? 1 : indexB < 0 ? -1 : indexA - indexB;
    });
  }

  public convertRenderingHintsToCategoricalColorMap(renderingHints: RenderingHint[]): CategoricalColorMap {
    const colorMap = renderingHints.map(hint => hint.color);
    return { id: RECOMMENDED_PIXEL_MAP_COLOR_MAP_ID, colorMap };
  }

  public convertValuesOfPixelMap(pixelMap: PixelMap): PixelMap {
    const tmpPixelMap = { ...pixelMap };

    if (pixelMap.element_type === 'int64' || pixelMap.element_type === 'uint64') {
      tmpPixelMap.neutral_value = tmpPixelMap.neutral_value !== null || tmpPixelMap.neutral_value !== undefined
        ? BigInt(tmpPixelMap.neutral_value as number | bigint)
        : tmpPixelMap.neutral_value;
      if (isContinuousPixelMap(tmpPixelMap) || isDiscretePixelMap(tmpPixelMap)) {
        tmpPixelMap.min_value = tmpPixelMap.min_value !== null || tmpPixelMap.min_value !== undefined
          ? BigInt(tmpPixelMap.min_value)
          : tmpPixelMap.min_value;
        tmpPixelMap.max_value = tmpPixelMap.max_value !== null || tmpPixelMap.max_value !== undefined
          ? BigInt(tmpPixelMap.max_value)
          : tmpPixelMap.max_value;
      }
    }

    return tmpPixelMap;
  }

  public convertPixelMapLevelsToMap(pixelMap: ApiPixelMap): PixelMap {
    return { ...pixelMap, levels: pixelMap.levels.reduce((acc, curr) => ({ ...acc, [curr.slide_level]: { ...curr } }), {}) } as PixelMap;
  }

  public convertPixelMapLevels(pixelMap: ApiPixelMap): LevelDetailsMap {
    return pixelMap.levels.reduce((acc, curr) => ({ ...acc, [curr.slide_level]: { ...curr }}), {});
  }

  public addNameToChannelClassMapping(pixelMap: PixelMap, classesDict: Dictionary<ClassEntity>): ChannelClassMapping[] | undefined {
    return pixelMap.channel_class_mapping?.map(ccm => ({ ...ccm, name: classesDict[ccm.class_value]?.name as string }));
  }

  public convertPixelMapChannelClassMapping(pixelMap: PixelMap, classesDict: Dictionary<ClassEntity>): PixelMap {
    return { ...pixelMap, channel_class_mapping: this.addNameToChannelClassMapping(pixelMap, classesDict)};
  }

  public convertApiPixelMapToAppPixelMap(
    pixelMap: ApiPixelMap,
    classesDict: Dictionary<ClassEntity>,
    renderingHints: RenderingHint[]
  ): PixelMap {
    if (isApiNominalPixelMap(pixelMap)) {
      pixelMap?.element_class_mapping?.sort((a: AppV3NumberClassMapping, b: AppV3NumberClassMapping) => {
        const foundA = renderingHints.find(hint => hint.class_value === a.class_value);
        const foundB = renderingHints.find(hint => hint.class_value === b.class_value);
        const indexA = foundA ? renderingHints.indexOf(foundA) : -1;
        const indexB = foundB ? renderingHints.indexOf(foundB) : -1;
        return indexA < 0 ? 1 : indexB < 0 ? -1 : indexA - indexB;
      });
    }
    let tmp = this.convertPixelMapLevelsToMap(pixelMap);
    tmp = this.convertPixelMapChannelClassMapping(tmp, classesDict);
    return this.convertValuesOfPixelMap(tmp);
  }

  public calculateMaxLevel(levels: LevelDetailsMap): number {
    const keys = Object.keys(levels).map(k => Number(k));
    return Math.max(...keys);
  }
}
