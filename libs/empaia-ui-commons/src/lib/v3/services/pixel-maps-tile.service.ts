import { EMPTY, MonoTypeOperatorFunction, Observable, catchError, mergeMap } from 'rxjs';
import { ScopeServiceInterface } from '../../shared/models/scope.models';
import { HttpErrorResponse } from '@angular/common/http';
import { HttpHeader } from '../../shared/models/http-headers.models';

export class PixelMapsTileService {

  constructor(
    private scopeService: ScopeServiceInterface,
    private headers: HttpHeader[],
    private wbsUrl: string,
    private retryFunction: MonoTypeOperatorFunction<Blob>,
  ) { }

  public fetchTileData(pixelMapId: string, level: number, x: number, y: number): Observable<ArrayBuffer> {
    return new Observable<Blob>(sub => {
      const srcUrl = `${this.wbsUrl}/${this.scopeService.scopeId}/pixelmaps/${pixelMapId}/level/${level}/position/${x}/${y}/data`;
      const client = new XMLHttpRequest();
      client.open('GET', srcUrl);

      for (const header of this.headers) {
        client.setRequestHeader(header.key, header.value());
      }

      client.responseType = 'blob';

      client.addEventListener('loadend', function() {
        if (client.status === 200) {
          const data = this.response as Blob;
          if (data) {
            sub.next(data);
            sub.complete();
          } else {
            sub.error(new Error('Error during fetching pixelmap tile data!'));
          }
        } else {
          const error = new HttpErrorResponse({
            status: client.status,
            statusText: client.statusText,
            url: client.responseURL
          });
          sub.error(error);
        }
      }, { passive: false });

      client.addEventListener('error', function() {
        sub.error(new Error('Connection error while fetching pixelmap tile data!'));
      }, { passive: false });

      client.send();
    }).pipe(
      this.retryFunction,
      mergeMap(response => response.arrayBuffer()),
      catchError(() => EMPTY)
    );
  }
}
