/* eslint-disable @typescript-eslint/ban-ts-comment */
import { ImageTile, Tile } from 'ol';
import TileState from 'ol/TileState';
import { firstValueFrom } from 'rxjs';
import { ImageInfo, PixelMapTileRequesterInterface } from 'slide-viewer';
import { PixelMap, convertArrayBufferToPixelMapData, isContinuousPixelMap, isDiscretePixelMap, isNominalPixelMap } from '../models/pixel-maps.models';
import { PixelMapsTileService } from '../services/pixel-maps-tile.service';
import { CanvasDrawCache, PixelMapBigDataType, PixelMapCredentialManager, PixelMapDataType, PixelMapTileCache, drawColorMapImage, drawColorMapImageFromMap } from 'pixelmap-rendering-collection';

export class PixelMapTileRequester implements PixelMapTileRequesterInterface {
  constructor(
    private slideImageInfo: ImageInfo,
    private pixelMap: PixelMap,
    private pixelMapService: PixelMapsTileService,
    public channel: number,
    public hasArtificialLevel = false,
  ) {}

  public async tileLoadFunction(tile: Tile): Promise<void> {
    const tileCoords = tile.getTileCoord();
    const level = this.slideImageInfo.numberOfLevels - tileCoords[0] - (this.hasArtificialLevel ? 0 : 1);

    if (isNaN(level)) {
      throw Error('Error in Slide Map Tile Requester - Tile level invalid!');
    }

    if (level < 0) {
      tile.setState(TileState.EMPTY);
    }

    const x = tileCoords[1];
    const y = tileCoords[2];

    if (
      !this.pixelMap.levels[level]
      || this.pixelMap.levels[level]?.position_min_x !== null && x < this.pixelMap.levels[level]?.position_min_x
      || this.pixelMap.levels[level]?.position_min_y !== null && y < this.pixelMap.levels[level]?.position_min_y
      || this.pixelMap.levels[level]?.position_max_x !== null && x > this.pixelMap.levels[level]?.position_max_x
      || this.pixelMap.levels[level]?.position_max_y !== null && y > this.pixelMap.levels[level]?.position_max_y
    ) {
      tile.setState(TileState.EMPTY);
      return;
    }

    const pixelMapTileCache = PixelMapTileCache.getPixelMapTileCache();

    let pixelMapData: PixelMapDataType | PixelMapBigDataType;
    let canvas: HTMLCanvasElement;

    if (pixelMapTileCache.hasTileData(level, x, y)) {
      pixelMapData = pixelMapTileCache.getTileDataFromCache(level, x, y) as PixelMapDataType | PixelMapBigDataType;
    } else {
      const buffer = await firstValueFrom(this.pixelMapService.fetchTileData(
        this.pixelMap.id as string,
        level,
        x,
        y,
      ), { defaultValue: new ArrayBuffer(0) });

      if (!buffer?.byteLength) {
        tile.setState(TileState.EMPTY);
        return;
      }

      pixelMapData = convertArrayBufferToPixelMapData(buffer, this.pixelMap);
      pixelMapTileCache.addTileDataToCache(pixelMapData, level, x, y);
    }

    if (pixelMapTileCache.hasCanvas(this.channel, level, x, y)) {
      canvas = pixelMapTileCache.getCanvasFromCache(this.channel, level, x, y) as HTMLCanvasElement;
    } else {
      canvas = document.createElement('canvas');
      canvas.width = this.pixelMap.tilesize;
      canvas.height = this.pixelMap.tilesize;
      pixelMapTileCache.addCanvasToCache(canvas, this.channel, level, x, y);
    }

    const pixelMapCredentialManager = PixelMapCredentialManager.getPixelMapCredentialManager();

    if (
      pixelMapCredentialManager.colorMapFn
      && typeof pixelMapCredentialManager.colorMapFn === 'function'
      && (isContinuousPixelMap(this.pixelMap) || isDiscretePixelMap(this.pixelMap))
    ) {
      // TODO: check if there is a better way as to ignore
      drawColorMapImage(
        canvas,
        // @ts-ignore
        pixelMapData,
        // canvasDrawCache,
        this.channel,
        this.pixelMap.min_value,
        this.pixelMap.max_value,
        pixelMapCredentialManager.colorMapFn,
        pixelMapCredentialManager.inversion,
        this.pixelMap.neutral_value,
      );
    } else if (
      pixelMapCredentialManager.colorMapFn
      && typeof pixelMapCredentialManager.colorMapFn !== 'function'
      && isNominalPixelMap(this.pixelMap)
    ) {
      drawColorMapImageFromMap(
        canvas,
        // @ts-ignore
        pixelMapData,
        this.channel,
        pixelMapCredentialManager.colorMapFn,
      );
    } else {
      tile.setState(TileState.EMPTY);
      return;
    }

    (tile as ImageTile).setImage(canvas);
  }
}
