/* eslint-disable @nx/enforce-module-boundaries */
import {
  AppV3ContinuousPixelmap,
  AppV3DiscretePixelmap,
  AppV3NominalPixelmap,
  AppV3NumberClassMapping,
  AppV3ContinuousPixelmapElementType as ContinuousPixelmapElementType,
  AppV3DiscretePixelmapElementType as DiscretePixelmapElementType,
} from 'empaia-api-lib';
import { ChannelClassMapping } from 'slide-viewer';
import { PixelMapTileRequester } from '../classes/pixel-map-tile-requester';
import {
  ElementColorMapping,
  PixelMapBigDataType,
  PixelMapDataType,
} from 'pixelmap-rendering-collection';

export {
  AppV3ContinuousPixelmapElementType as ContinuousPixelmapElementType,
  AppV3DiscretePixelmapElementType as DiscretePixelmapElementType,
  AppV3NominalPixelmapElementType as NominalPixelmapElementType,
  AppV3PostContinuousPixelmap as PostContinuousPixelmap,
  AppV3PostContinuousPixelmaps as PostContinuousPixelmaps,
  AppV3PostDiscretePixelmap as PostDiscretePixelmap,
  AppV3PostDiscretePixelmaps as PostDiscretePixelmaps,
  AppV3PostNominalPixelmap as PostNominalPixelmap,
  AppV3PostNominalPixelmaps as PostNominalPixelmaps,
  AppV3PixelmapLevel as PixelmapLevel,
  AppV3PixelmapList as PixelmapList,
  AppV3PixelmapQuery as PixelmapQuery,
  AppV3PixelmapReferenceType as PixelmapReferenceType,
  AppV3PixelmapType as PixelmapType,
} from 'empaia-api-lib';

export const pixelMapType = [
  'continuous_pixelmap',
  'discrete_pixelmap',
  'nominal_pixelmap',
] as const;
export type PixelMapType = typeof pixelMapType[number];

export interface ElementClassMapping extends AppV3NumberClassMapping {
  color?: string;
}

export interface ContinuousPixelMap
  extends Omit<
  AppV3ContinuousPixelmap,
  'max_value' | 'min_value' | 'neutral_value' | 'levels'
  > {
  channel_class_mapping?: ChannelClassMapping[];
  max_value: number | bigint;
  min_value: number | bigint;
  neutral_value?: number | bigint | null;
  levels: LevelDetailsMap;
  type: PixelMapType;
}

export interface DiscretePixelMap
  extends Omit<
  AppV3DiscretePixelmap,
  'max_value' | 'min_value' | 'neutral_value' | 'levels'
  > {
  channel_class_mapping?: ChannelClassMapping[];
  max_value: number | bigint;
  min_value: number | bigint;
  neutral_value?: number | bigint | null;
  element_class_mapping?: ElementClassMapping[];
  levels: LevelDetailsMap;
  type: PixelMapType;
}

export interface NominalPixelMap
  extends Omit<AppV3NominalPixelmap, 'neutral_value' | 'levels'> {
  channel_class_mapping?: ChannelClassMapping[];
  neutral_value?: number | bigint | null;
  element_class_mapping: ElementClassMapping[];
  levels: LevelDetailsMap;
  type: PixelMapType;
}

export interface LevelDetails {
  position_min_x?: number | null;
  position_max_x?: number | null;
  position_min_y?: number | null;
  position_max_y?: number | null;
}

export interface LevelDetailsMap {
  [key: number]: LevelDetails;
}

export interface TileRequester {
  id: number;
  tileRequester: PixelMapTileRequester;
}

export type ColorMapFn = (t: number) => string;

export type PixelMap = ContinuousPixelMap | DiscretePixelMap | NominalPixelMap;

export function isNominalPixelMap(
  pixelMap: PixelMap
): pixelMap is NominalPixelMap {
  return (
    'element_class_mapping' in pixelMap && pixelMap.type === 'nominal_pixelmap'
  );
}

export function isDiscretePixelMap(
  pixelMap: PixelMap
): pixelMap is DiscretePixelMap {
  return (
    'min_value' in pixelMap &&
    'max_value' in pixelMap &&
    pixelMap.type === 'discrete_pixelmap'
  );
}

export function isContinuousPixelMap(
  pixelMap: PixelMap
): pixelMap is ContinuousPixelMap {
  return (
    'min_value' in pixelMap &&
    'max_value' in pixelMap &&
    pixelMap.type === 'continuous_pixelmap'
  );
}

export function hasElementClassMapping(
  pixelMap: PixelMap
): pixelMap is DiscretePixelMap | NominalPixelMap {
  return (
    'element_class_mapping' in pixelMap && !!pixelMap.element_class_mapping
  );
}

export type ApiPixelMap =
  | AppV3ContinuousPixelmap
  | AppV3DiscretePixelmap
  | AppV3NominalPixelmap;

export function isApiNominalPixelMap(
  pixelMap: ApiPixelMap
): pixelMap is AppV3NominalPixelmap {
  return (
    'element_class_mapping' in pixelMap && pixelMap.type === 'nominal_pixelmap'
  );
}

export function isApiDiscretePixelMap(
  pixelMap: ApiPixelMap
): pixelMap is AppV3DiscretePixelmap {
  return (
    'min_value' in pixelMap &&
    'max_value' in pixelMap &&
    pixelMap.type === 'discrete_pixelmap'
  );
}

export function isApiContinuousPixelMap(
  pixelMap: ApiPixelMap
): pixelMap is AppV3ContinuousPixelmap {
  return (
    'min_value' in pixelMap &&
    'max_value' in pixelMap &&
    pixelMap.type === 'continuous_pixelmap'
  );
}

export function convertArrayBufferToPixelMapData(
  buffer: ArrayBuffer,
  pixelMap: PixelMap
): PixelMapDataType | PixelMapBigDataType {
  switch (pixelMap.element_type) {
    case DiscretePixelmapElementType.Uint8:
      return new Uint8Array(buffer);
    case DiscretePixelmapElementType.Uint16:
      return new Uint16Array(buffer);
    case DiscretePixelmapElementType.Uint32:
      return new Uint32Array(buffer);
    case DiscretePixelmapElementType.Uint64:
      return new BigUint64Array(buffer);
    case DiscretePixelmapElementType.Int8:
      return new Int8Array(buffer);
    case DiscretePixelmapElementType.Int16:
      return new Int16Array(buffer);
    case DiscretePixelmapElementType.Int32:
      return new Int32Array(buffer);
    case DiscretePixelmapElementType.Int64:
      return new BigInt64Array(buffer);
    case ContinuousPixelmapElementType.Float32:
      return new Float32Array(buffer);
    case ContinuousPixelmapElementType.Float64:
      return new Float64Array(buffer);
    default:
      throw Error('Datatype is not supported!');
  }
}

export function convertElementClassMappingToElementColorMapping(
  mapping?: ElementClassMapping[]
): ElementColorMapping | undefined {
  return mapping?.reduce(
    (acc, curr) => ({ ...acc, [curr.number_value.toString()]: curr.color }),
    {}
  );
}

export const RECOMMENDED_PIXEL_MAP_COLOR_MAP_ID = 'Recommended';
