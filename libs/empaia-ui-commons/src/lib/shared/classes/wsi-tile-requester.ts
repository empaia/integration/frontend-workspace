import { TileConnector, WSIFileFormat } from 'slide-viewer';
import { HttpHeader } from '../models/http-headers.models';
import { EMPTY, MonoTypeOperatorFunction, Observable, catchError, map } from 'rxjs';
import { HttpErrorResponse } from '@angular/common/http';

export class WsiTileRequester implements TileConnector {
  constructor(
    private slideId: string,
    private baseUrl: string,
    private headers: HttpHeader[],
    private retryFunction?: MonoTypeOperatorFunction<Blob>
  ) {}

  // public async getSlideTileMultichannel(level: number, x: number, y: number): Promise<RawMultichannelData> {

  //   const observable: Observable<Blob> = this.slidesService.scopeIdSlidesSlideIdTileLevelLevelTileTileXTileYGet(
  //     {
  //       scope_id: this.imageSourceUrl,
  //       slide_id: this.imageId,
  //       level: level,
  //       tile_x:x,
  //       tile_y:y,
  //       image_format: WSIFileFormat.TIFF
  //     }
  //   ).pipe(
  //     this.retryFunction
  //   );

  //   const blob: Blob = await firstValueFrom(observable);
  //   const buffer:ArrayBuffer = await blob.arrayBuffer();
  //   const arrayBuffer: Uint8Array[] | Uint16Array[] | Uint32Array[] = await readTiffArrayBuffer(buffer);
  //   return { data : arrayBuffer };
  // }

  public getSlideTileBlob(level: number, x: number, y: number): Observable<Blob> {
    return new Observable<Blob>(sub => {
      const srcUrl = `${this.baseUrl}/slides/${this.slideId}/tile/level/${level}/tile/${x}/${y}?image_format=${WSIFileFormat.JPG}`;
      const client = new XMLHttpRequest();
      client.open('GET', srcUrl);

      for (const header of this.headers) {
        client.setRequestHeader(header.key, header.value());
      }

      client.responseType = 'blob';

      client.addEventListener('loadend', function () {
        if (client.status === 200) {
          const data = this.response as Blob;
          if (data) {
            sub.next(data);
            sub.complete();
          } else {
            sub.error(new Error('Error during fetching tiles!'));
          }
        } else {
          const error = new HttpErrorResponse({
            status: client.status,
            statusText: client.statusText,
            url: client.responseURL
          });
          sub.error(error);
        }
      }, { passive: false });

      client.addEventListener('error', function() {
        sub.error(new Error('Connection error while fetching wsi tiles!'));
      }, { passive: false });

      client.send();
    }).pipe(
      this.retryFunction ?? map(response => response),
      catchError(() => EMPTY)
    );
  }
}
