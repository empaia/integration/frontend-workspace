import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { RenderingHint } from '../../../models/ead.models';

@Component({
  selector: 'app-annotation-class-color-item',
  templateUrl: './annotation-class-color-item.component.html',
  styleUrls: ['./annotation-class-color-item.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AnnotationClassColorItemComponent {
  @Input() public annotationRendering!: RenderingHint[];

  public trackByAnnotationRendering(_index: number, annotationRendering: RenderingHint): string {
    return annotationRendering.class_value;
  }
}
