import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { MatSelectChange } from '@angular/material/select';
import { AnnotationColorMap } from '../../../models/annotations-color-maps.models';
import { MatSlideToggleChange } from '@angular/material/slide-toggle';

@Component({
  selector: 'app-annotation-class-color-settings',
  templateUrl: './annotation-class-color-settings.component.html',
  styleUrls: ['./annotation-class-color-settings.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AnnotationClassColorSettingsComponent {
  @Input() public annotationColorMaps!: AnnotationColorMap[];
  @Input() public selectedAnnotationColorMap?: string | undefined;
  @Input() public annotationColorMapInversion!: boolean;
  @Input() public annotationColorMapInversionDisableState = false;

  @Output() public changeAnnotationColorMap = new EventEmitter<string>();
  @Output() public changeAnnotationColorMapInversion = new EventEmitter<boolean>();

  public onAnnotationRenderingHintChanged(event: MatSelectChange): void {
    const selected = event.value;
    this.changeAnnotationColorMap.emit(selected);
  }

  public onInversionChanged(event: MatSlideToggleChange): void {
    const invert = event.checked;
    this.changeAnnotationColorMapInversion.emit(invert);
  }
}
