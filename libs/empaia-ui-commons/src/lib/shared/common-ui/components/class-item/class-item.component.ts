import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { ClassEntity, ClassSelector } from '../../../models/classes.models';
import { MatCheckboxChange } from '@angular/material/checkbox';

@Component({
  selector: 'app-class-item',
  templateUrl: './class-item.component.html',
  styleUrls: ['./class-item.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ClassItemComponent {
  @Input() public classItem!: ClassEntity;
  @Input() public classColor!: string;
  @Input() public selected!: boolean;

  @Output() public changeSelection = new EventEmitter<ClassSelector>();

  public onCheckboxChange(change: MatCheckboxChange): void {
    const classSelector: ClassSelector = {
      id: this.classItem.id,
      checked: change.checked
    };
    this.changeSelection.emit(classSelector);
  }
}
