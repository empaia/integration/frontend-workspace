import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AnnotationClassColorItemComponent } from './components/annotation-class-color-item/annotation-class-color-item.component';
import { AnnotationClassColorSettingsComponent } from './components/annotation-class-color-settings/annotation-class-color-settings.component';
import { MaterialModule } from '../../material/material.module';
import { ClassItemComponent } from './components/class-item/class-item.component';
import { CopyToClipboardDirective } from './directives/copy-to-clipboard.directive';
import { AnnotationClassToStringPipe } from './pipes/annotation-class-to-string.pipe';

const COMPONENTS = [
  AnnotationClassColorItemComponent,
  AnnotationClassColorSettingsComponent,
  ClassItemComponent,
];

const DIRECTIVES = [
  CopyToClipboardDirective,
];

const PIPES = [
  AnnotationClassToStringPipe,
];

@NgModule({
  declarations: [
    COMPONENTS,
    DIRECTIVES,
    PIPES,
  ],
  imports: [
    CommonModule,
    MaterialModule,
  ],
  exports: [
    COMPONENTS,
    DIRECTIVES,
    PIPES,
  ]
})
export class CommonUiModule { }
