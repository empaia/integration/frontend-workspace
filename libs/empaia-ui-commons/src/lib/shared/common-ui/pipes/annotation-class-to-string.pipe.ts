import { Pipe, PipeTransform } from '@angular/core';
import { ClassEntity } from '../../models/classes.models';

@Pipe({
  name: 'annotationClassToString'
})
export class AnnotationClassToStringPipe implements PipeTransform {

  transform(annotationClass: ClassEntity): string {
    return `
    Class Value: ${annotationClass.id}
    Class Name: ${annotationClass.name}
    ${annotationClass.description ? 'Description: ' + annotationClass.description : ''}
    `;
  }

}
