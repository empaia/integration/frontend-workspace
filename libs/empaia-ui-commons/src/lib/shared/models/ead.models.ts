export interface EmpaiaAppDescriptionCore {
  $shema: string;
  name: string;
  name_short: string;
  namespace: string;
  description: string;
}

export interface EmpaiaAppDescriptionV1 extends EmpaiaAppDescriptionCore {
  inputs: object;
  outputs: object;
}

export interface EmpaiaAppDescriptionV3 extends EmpaiaAppDescriptionCore {
  io: object;
  modes: Modes;
  rendering?: Rendering;
}

export interface Modes {
  standalone?: ModeIOs;
  preprocessing?: ModeIOs;
  postprocessing?: ModeIOContainer;
}

export interface ModeIOs {
  inputs: string[];
  outputs: string[];
}

export interface ModeIOContainer extends ModeIOs {
  containerized: boolean;
}

export enum EmpaiaAppDescriptionVersion {
  VERSION_1,
  VERSION_3,
}

export interface InputKey {
  inputKey: string;
  inCollection: number;
}

export interface TypeInputKey extends InputKey {
  type: EadInputType;
}

export interface Rendering {
  annotations?: RenderingHint[];
  nominal_pixelmaps?: RenderingHint[];
}

export interface RenderingHint {
  class_value: string;
  color: string;
  color_selection?: string;
  color_hover?: string;
}

export const v3Modes = [
  'standalone',
  'preprocessing',
  'postprocessing'
] as const;

export type EmpaiaAppDescriptionV3Mode = typeof v3Modes[number];

export const v3RoiModes = [
  'single',
  'multiple'
] as const;

export type EadRoiInputType = typeof v3RoiModes[number];

export type EadInputTypeWsi = 'wsi';
export type EadInputTypeCollection = 'collection';
export const annotationTypes = ['rectangle', 'polygon', 'circle'] as const;
export type EadInputTypeAnnotation = typeof annotationTypes[number];

export type EadInputType = EadInputTypeWsi | EadInputTypeCollection | EadInputTypeAnnotation;

export function isWsi(object: string): object is EadInputTypeWsi {
  return object === 'wsi';
}

export function isCollection(object: string): object is EadInputTypeCollection {
  return object === 'collection';
}

export function isAnnotation(object: string): object is EadInputTypeAnnotation {
  return annotationTypes.includes(object as EadInputTypeAnnotation);
}

export function getEmpaiaAppDescriptionVersion(ead: object): EmpaiaAppDescriptionVersion {
  return 'io' in ead && 'modes' in ead ? EmpaiaAppDescriptionVersion.VERSION_3 : EmpaiaAppDescriptionVersion.VERSION_1;
}
