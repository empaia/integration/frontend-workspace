import { Tableau9, Vibrant } from 'pixelmap-rendering-collection';
import { ClassColorMapping } from 'slide-viewer';
import { RenderingHint } from './ead.models';

export interface CategoricalColorMap {
  id: string;
  colorMap: readonly string[];
}

export const DEFAULT_ANNOTATION_COLOR_MAPS: CategoricalColorMap[] = [
  { id: 'Vibrant', colorMap: Vibrant },
  { id: 'Tableau9', colorMap: Tableau9 }
];

export interface AnnotationColorMap {
  id: string;
  renderingHints: RenderingHint[];
}

export function convertAnnotationRenderingHintsToClassColorMapping(annotationRendering: RenderingHint[]): ClassColorMapping {
  return annotationRendering.reduce((acc, curr) => ({
    ...acc,
    [curr.class_value]: {
      color: curr.color,
      colorSelection: curr.color_selection ?? curr.color,
      colorHover: curr.color_hover ?? curr.color
    }
  }), {});
}

export function createAnnotationRenderingHints(annotationRendering: RenderingHint[], colorMap: CategoricalColorMap, invert = false): RenderingHint [] {
  const colorSelection = colorMap.colorMap[invert ? 0 : colorMap.colorMap.length - 1];
  return annotationRendering.map((ar, i) => ({
    class_value: ar.class_value,
    color: colorMap.colorMap[invert ? (colorMap.colorMap.length - 2) - i % (colorMap.colorMap.length - 1) : i % (colorMap.colorMap.length - 1)],
    color_selection: colorSelection,
    color_hover: colorSelection,
    // color: colorMap.colorMap[invert ? (colorMap.colorMap.length - 1) - i % colorMap.colorMap.length : i % colorMap.colorMap.length],
    // color_selection: colorMap.colorMap[invert ? (colorMap.colorMap.length - 1) - (i + 1) % colorMap.colorMap.length : (i + 1) % colorMap.colorMap.length],
    // color_hover: colorMap.colorMap[invert ? (colorMap.colorMap.length - 1) - (i + 1) % colorMap.colorMap.length : (i + 1) % colorMap.colorMap.length]
  }));
}

export function createAnnotationRenderingHintsFromClasses(classes: string[], colorMap: CategoricalColorMap, invert = false): RenderingHint[] {
  const colorSelection = colorMap.colorMap[invert ? 0 : colorMap.colorMap.length - 1];
  return classes.map((c, i) => ({
    class_value: c,
    color: colorMap.colorMap[invert ? (colorMap.colorMap.length - 2) - i % (colorMap.colorMap.length - 1) : i % (colorMap.colorMap.length - 1)],
    color_selection: colorSelection,
    color_hover: colorSelection,
    // color: colorMap.colorMap[invert ? (colorMap.colorMap.length - 1) - i % colorMap.colorMap.length : i % colorMap.colorMap.length],
    // color_selection: colorMap.colorMap[invert ? (colorMap.colorMap.length - 1) - (i + 1) % colorMap.colorMap.length : (i + 1) % colorMap.colorMap.length],
    // color_hover: colorMap.colorMap[invert ? (colorMap.colorMap.length - 1) - (i + 1) % colorMap.colorMap.length : (i + 1) % colorMap.colorMap.length]
  }));
}

export const RECOMMENDED_ANNOTATION_COLOR_MAP_ID = 'Recommended';
