export interface ScopeServiceInterface {
  get scopeId(): string;
  set scopeId(val: string);
}
