export interface ClassEntity {
  id: string;
  name: string;
  description?: string;
}

export interface ClassesDict {
  classes: object;
}

export interface ClassDictionaries {
  [p: string]: ClassesDict;
}

export interface ClassSelector {
  id: string;
  checked: boolean;
}
