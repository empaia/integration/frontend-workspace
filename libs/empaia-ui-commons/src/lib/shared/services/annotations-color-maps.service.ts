import { Injectable } from '@angular/core';
import { EadService } from './ead.service';
import { EmpaiaAppDescriptionV3 } from '../models/ead.models';
import {
  AnnotationColorMap,
  DEFAULT_ANNOTATION_COLOR_MAPS,
  RECOMMENDED_ANNOTATION_COLOR_MAP_ID,
  createAnnotationRenderingHints,
  createAnnotationRenderingHintsFromClasses
} from '../models/annotations-color-maps.models';

@Injectable({
  providedIn: 'root'
})
export class AnnotationsColorMapsService {
  constructor(private readonly eadService: EadService) {}

  public getAllV3AnnotationRenderingHints(ead: EmpaiaAppDescriptionV3, classes: string[], invert?: boolean): AnnotationColorMap[] {
    const annotationColorMaps: AnnotationColorMap[] = [];

    const eadRenderingHints = this.eadService.getV3AnnotationRenderingHints(ead);
    if (eadRenderingHints && eadRenderingHints.length) {
      annotationColorMaps.push({ id: RECOMMENDED_ANNOTATION_COLOR_MAP_ID, renderingHints: eadRenderingHints });

      DEFAULT_ANNOTATION_COLOR_MAPS.forEach(colorMap => {
        const renderingHints = createAnnotationRenderingHints(eadRenderingHints, colorMap, invert);
        annotationColorMaps.push({ id: colorMap.id, renderingHints });
      });
    } else {
      DEFAULT_ANNOTATION_COLOR_MAPS.forEach(colorMap => {
        const renderingHints = createAnnotationRenderingHintsFromClasses(classes, colorMap, invert);
        annotationColorMaps.push({ id: colorMap.id, renderingHints });
      });
    }

    return annotationColorMaps;
  }
}
