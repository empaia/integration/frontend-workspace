/* eslint-disable @typescript-eslint/no-explicit-any */
import { Injectable } from '@angular/core';
import { EadInputType, EadInputTypeAnnotation, EadRoiInputType, EmpaiaAppDescriptionV1, EmpaiaAppDescriptionV3, EmpaiaAppDescriptionV3Mode, InputKey, RenderingHint, TypeInputKey, annotationTypes, isCollection } from '../models/ead.models';

@Injectable({
  providedIn: 'root'
})
export class EadService {
  public isV1Ead(ead: object): ead is EmpaiaAppDescriptionV1 {
    return 'inputs' in ead;
  }

  public isV3Ead(ead: object): ead is EmpaiaAppDescriptionV3 {
    return 'io' in ead;
  }

  public getV1Inputs(ead: EmpaiaAppDescriptionV1): string[] {
    return Object.keys(ead.inputs);
  }

  public getV1WsiInputKey(ead: EmpaiaAppDescriptionV1): string | undefined {
    return this.searchForType('wsi', ead.inputs);
  }

  public getV1AnnotationsInputKey(ead: EmpaiaAppDescriptionV1): InputKey | undefined {
    return this.searchForTypeWithCollection('rectangle', ead.inputs);
  }

  public getV1TypeInputKey(type: EadInputType, ead: EmpaiaAppDescriptionV1): InputKey | undefined {
    const keys = this.getV1Inputs(ead);

    for (const key of keys) {
      const result = this.searchForTypeWithCollectionAndKey(type, key, ead.inputs);
      if (result) {
        return result;
      }
    }

    return undefined;
  }

  public getV1TypeInputKeyWithClassConstraints(type: EadInputType, ead: EmpaiaAppDescriptionV1, classConstraints: string[]): InputKey | undefined {
    const keys = this.getV1Inputs(ead);

    for (const key of keys) {
      const result = this.searchForTypeWithCollectionClassConstraintAndKey(type, key, classConstraints, ead.inputs);
      if (result) {
        return result;
      }
    }

    return undefined;
  }

  public getV1AnnotationInputTypes(ead: EmpaiaAppDescriptionV1): EadInputTypeAnnotation[] {
    const types = annotationTypes.filter(type => this.getV1TypeInputKey(type, ead));

    return types;
  }

  public isV1CompatibleWithInputType(ead: EmpaiaAppDescriptionV1, type: EadInputType): boolean {
    return !!this.getV1TypeInputKey(type, ead);
  }

  public hasAnyV1CompatibleInputType(ead: EmpaiaAppDescriptionV1, types: EadInputType[]): boolean {
    return types.some(type => this.isV1CompatibleWithInputType(ead, type));
  }

  public hasV1AnnotationTypeClassConstraints(ead: EmpaiaAppDescriptionV1, type: EadInputType, classConstraints: string[]): boolean | undefined {
    return !!this.getV1TypeInputKeyWithClassConstraints(type, ead, classConstraints);
  }

  public getV3Inputs(ead: EmpaiaAppDescriptionV3, mode: EmpaiaAppDescriptionV3Mode): string[] {
    return ead.modes[mode]?.inputs as string[];
  }

  public getV3Outputs(ead: EmpaiaAppDescriptionV3, mode: EmpaiaAppDescriptionV3Mode): string[] {
    return ead.modes[mode]?.outputs as string[];
  }

  public getV3TypeInputKeyWithClassConstraints(type: EadInputType, ead: EmpaiaAppDescriptionV3, mode: EmpaiaAppDescriptionV3Mode, classConstraints: string[]): InputKey | undefined {
    const keys = this.getV3Inputs(ead, mode);

    for (const key of keys) {
      const result = this.searchForTypeWithCollectionClassConstraintAndKey(type, key, classConstraints, ead.io);
      if (result) {
        return result;
      }
    }

    return undefined;
  }


  public hasV3AnnotationTypeClassConstraints(ead: EmpaiaAppDescriptionV3, mode: EmpaiaAppDescriptionV3Mode, type: EadInputType, classConstraints: string[]): boolean | undefined {
    return !!this.getV3TypeInputKeyWithClassConstraints(type, ead, mode, classConstraints);
  }


  public getV3WsiInputKey(ead: EmpaiaAppDescriptionV3, mode: EmpaiaAppDescriptionV3Mode): string | undefined {
    return this.getV3TypeInputKey('wsi', ead, mode)?.inputKey;
  }

  public getV3AnnotationsInputKey(ead: EmpaiaAppDescriptionV3, mode: EmpaiaAppDescriptionV3Mode): InputKey | undefined {
    return this.getV3TypeInputKey('rectangle', ead, mode);
  }

  public getV3TypeInputKey(type: EadInputType, ead: EmpaiaAppDescriptionV3, mode: EmpaiaAppDescriptionV3Mode): InputKey | undefined {
    const keys = this.getV3Inputs(ead, mode);

    for (const key of keys) {
      const result = this.searchForTypeWithCollectionAndKey(type, key, ead.io);
      if (result) {
        return result;
      }
    }

    return undefined;
  }

  public getV3TypeInputKeys(type: EadInputType, ead: EmpaiaAppDescriptionV3, mode: EmpaiaAppDescriptionV3Mode): InputKey[] {
    const keys = this.getV3Inputs(ead, mode);
    const results: InputKey[] = [];

    for (const key of keys) {
      const result = this.searchForTypeWithCollectionAndKey(type, key, ead.io);
      if (result) {
        results.push(result);
      }
    }

    return results;
  }

  public getV3TypesInputKeys(types: EadInputType[], ead: EmpaiaAppDescriptionV3, mode: EmpaiaAppDescriptionV3Mode): InputKey [] {
    let results: InputKey[] = [];

    for (const type of types) {
      results = results.concat(this.getV3TypeInputKeys(type, ead, mode));
    }

    return results;
  }

  public getV3TypeInputKeyWithType(type: EadInputType, ead: EmpaiaAppDescriptionV3, mode: EmpaiaAppDescriptionV3Mode): TypeInputKey | undefined {
    const keys = this.getV3Inputs(ead, mode);

    for (const key of keys) {
      const result = this.searchForTypeWithCollectionAndKey(type, key, ead.io);
      if (result) {
        return { ...result, type };
      }
    }

    return undefined;
  }

  public getV3TypeInputKeysWithType(type: EadInputType, ead: EmpaiaAppDescriptionV3, mode: EmpaiaAppDescriptionV3Mode): TypeInputKey[] {
    const keys = this.getV3Inputs(ead, mode);
    const results: TypeInputKey[] = [];

    for (const key of keys) {
      const result = this.searchForTypeWithCollectionAndKey(type, key, ead.io);
      if (result) {
        results.push({ ...result, type });
      }
    }

    return results;
  }

  public getV3TypesInputKeysWithTypes(types: EadInputType[], ead: EmpaiaAppDescriptionV3, mode: EmpaiaAppDescriptionV3Mode): TypeInputKey[] {
    let results: TypeInputKey[] = [];

    for (const type of types) {
      results = results.concat(this.getV3TypeInputKeysWithType(type, ead, mode));
    }

    return results;
  }

  public getV3AnnotationInputTypes(ead: EmpaiaAppDescriptionV3, mode: EmpaiaAppDescriptionV3Mode): EadInputTypeAnnotation[] {
    const types = annotationTypes.filter(type => this.getV3TypeInputKey(type, ead, mode));

    return types;
  }

  public getV3AnnotationRenderingHints(ead: EmpaiaAppDescriptionV3): RenderingHint[] | undefined {
    return ead.rendering?.annotations;
  }

  public getV3PixelMapsRenderingHints(ead: EmpaiaAppDescriptionV3): RenderingHint[] | undefined {
    return ead.rendering?.nominal_pixelmaps;
  }

  public isV3CompatibleWithInputType(ead: EmpaiaAppDescriptionV3, mode: EmpaiaAppDescriptionV3Mode, type: EadInputType): boolean {
    return !!this.getV3TypeInputKey(type, ead, mode);
  }

  public hasAnyV3CompatibleInputType(ead: EmpaiaAppDescriptionV3, mode: EmpaiaAppDescriptionV3Mode, types: EadInputType[]): boolean {
    return types.some(type => this.isV3CompatibleWithInputType(ead, mode, type));
  }

  public isV3MultiInputCompatible(ead: EmpaiaAppDescriptionV3, mode: EmpaiaAppDescriptionV3Mode, type: EadInputType): boolean {
    const result = this.getV3TypeInputKey(type, ead, mode);
    return result ? result.inCollection === 1 : false;
  }

  public hasAnyV3CompatibleMultiInputType(ead: EmpaiaAppDescriptionV3, mode: EmpaiaAppDescriptionV3Mode, types: EadInputType[]): boolean {
    return types.some(type => this.isV3MultiInputCompatible(ead, mode, type));
  }

  public getV3InputMode(ead: EmpaiaAppDescriptionV3, mode: EmpaiaAppDescriptionV3Mode): EadRoiInputType {
    const types = this.getV3AnnotationInputTypes(ead, mode);
    const results = this.getV3TypesInputKeys(types, ead, mode);

    return mode === 'preprocessing' ? 'single' : results.every(result => result.inCollection === 1) ? 'multiple' : 'single';
  }

  public hasV3MultiAndSingleInputs(ead: EmpaiaAppDescriptionV3, mode: EmpaiaAppDescriptionV3Mode, types: EadInputType[]): boolean {
    const results = this.getV3TypesInputKeys(types, ead, mode);

    const single = results.some(result => result.inCollection === 0);
    const multi = results.some(result => result.inCollection === 1);

    return single && multi;
  }

  public hasV3NestedInputCollections(ead: EmpaiaAppDescriptionV3, mode: EmpaiaAppDescriptionV3Mode, types: EadInputType[]): boolean {
    const results = this.getV3TypesInputKeys(types, ead, mode);

    return results.some(result => result.inCollection > 1);
  }

  public getV3Modes(ead: EmpaiaAppDescriptionV3): EmpaiaAppDescriptionV3Mode[] {
    return Object.keys(ead.modes) as EmpaiaAppDescriptionV3Mode[];
  }

  private searchForType(type: EadInputType, inputs?: object): string | undefined {
    if (inputs) {
      for (const [key ,value] of Object.entries(inputs)) {
        if (value['type'] === type) {
          return key;
        }
      }
    }

    return undefined;
  }

  private searchForTypeWithCollection(type: EadInputType, inputs?: object): InputKey | undefined {
    if (inputs) {
      for (const [key, value] of Object.entries(inputs)) {
        if (value['type'] === type) {
          return { inputKey: key, inCollection: 0 };
        } else if (isCollection(value['type'])) {
          const found = this.searchForTypeWithCollection(type, value);
          if (found) {
            found.inputKey = key;
            ++found.inCollection;
            return found;
          }
        }
      }
    }

    return undefined;
  }

  private searchForTypeWithCollectionAndKey(type: EadInputType, key: string, inputs?: any): InputKey | undefined {
    if (inputs && inputs[key]) {
      const input = inputs[key];
      if (input['type'] === type) {
        return { inputKey: key, inCollection: 0 };
      } else if (isCollection(input['type'])) {
        const found = this.searchForTypeWithCollection(type, input);
        if (found) {
          found.inputKey = key;
          ++found.inCollection;
          return found;
        }
      }
    }

    return undefined;
  }

  private searchForTypeWithCollectionAndClassConstraint(type: EadInputType, classConstraints: string[], inputs?: object): InputKey | undefined {
    if (inputs) {
      for (const [key, value] of Object.entries(inputs)) {
        if (value['type'] === type && this.checkMatchingClassConstraints(classConstraints, value['classes'])) {
          return { inputKey: key, inCollection: 0 };
        } else if (isCollection(value['type'])) {
          const found = this.searchForTypeWithCollectionAndClassConstraint(type, classConstraints, value);
          if (found) {
            found.inputKey = key;
            ++found.inCollection;
            return found;
          }
        }
      }
    }

    return undefined;
  }

  private searchForTypeWithCollectionClassConstraintAndKey(type: EadInputType, key: string, classConstraints: string[], inputs?: any): InputKey | undefined {
    if (inputs && inputs[key]) {
      const input = inputs[key];
      if (input['type'] === type && this.checkMatchingClassConstraints(classConstraints, input['classes'])) {
        return { inputKey: key, inCollection: 0 };
      } else if (isCollection(input['type'])) {
        const found = this.searchForTypeWithCollectionAndClassConstraint(type, classConstraints, input);
        if (found) {
          found.inputKey = key;
          ++found.inCollection;
          return found;
        }
      }
    }

    return undefined;
  }

  private checkMatchingClassConstraints(classConstraints: string[], classes?: string[]): boolean | undefined {
    return classes?.some(c => classConstraints.includes(c));
  }
}
