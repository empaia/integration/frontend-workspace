import { createAction, props } from '@ngrx/store';
import { IdWbsUrl } from './wbs-url.model';

export const sendWbsUrl = createAction(
  '[AI/Wbs-Url] Send Wbs Url',
  props<{ idWbsUrl: IdWbsUrl }>()
);

export const removeWbsUrls = createAction(
  '[AI/Wbs-Url] Remove Wbs Url',
  props<{ ids: string[] }>()
);
