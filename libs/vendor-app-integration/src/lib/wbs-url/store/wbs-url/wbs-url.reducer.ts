import { createReducer, on } from '@ngrx/store';
import { IdWbsUrl } from './wbs-url.model';
import * as WbsUrlActions from './wbs-url.actions';
import { createEntityAdapter, EntityState } from '@ngrx/entity';

export const WBS_URL_FEATURE_KEY = 'wbsUrl';

export type State = EntityState<IdWbsUrl>;

export const wbsUrlAdapter = createEntityAdapter<IdWbsUrl>();

export const initialState: State = wbsUrlAdapter.getInitialState();

export const reducer = createReducer(
  initialState,
  on(
    WbsUrlActions.sendWbsUrl,
    (state, { idWbsUrl }): State =>
      wbsUrlAdapter.upsertOne(idWbsUrl, {
        ...state,
      })
  ),
  on(
    WbsUrlActions.removeWbsUrls,
    (state, { ids }): State =>
      wbsUrlAdapter.removeMany(ids, {
        ...state,
      })
  )
);
