import { Injectable } from '@angular/core';
import { Actions, concatLatestFrom, createEffect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import * as WbsUrlActions from './wbs-url.actions';
import * as TargetActions from '../../../target/store/target/target.actions';
import * as TargetSelectors from '../../../target/store/target/target.selectors';
import { distinctUntilChanged, map } from 'rxjs/operators';
import { compareDistinct } from '../../../shared/helper/rxjs-operations';

@Injectable()
export class WbsUrlEffects {
  sendWbsUrl$ = createEffect(
    () => {
      return this.actions$.pipe(
        ofType(WbsUrlActions.sendWbsUrl),
        map((action) => action.idWbsUrl),
        concatLatestFrom(() => [
          this.store.select(TargetSelectors.selectTargetEntities),
        ]),
        map(([appWbsUrl, targetEntities]) => {
          const appTarget = targetEntities[appWbsUrl.id];
          if (appTarget && appTarget.target && appTarget.target.contentWindow) {
            appTarget.target.contentWindow.postMessage(appWbsUrl.wbsUrl, '*');
          }
        })
      );
    },
    { dispatch: false }
  );

  // remove wbsUrl if the corresponding target was removed
  removeWbsUrl$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(TargetActions.removeTargets),
      map((action) => action.ids),
      distinctUntilChanged(compareDistinct),
      map((appIds) => WbsUrlActions.removeWbsUrls({ ids: appIds }))
    );
  });

  constructor(private actions$: Actions, private store: Store) {}
}
