import { createSelector } from '@ngrx/store';
import {
  State as ModuleState,
  selectWbsUrlFeatureState,
} from '../wbs-url-feature.state';
import { wbsUrlAdapter } from './wbs-url.reducer';

const { selectEntities } = wbsUrlAdapter.getSelectors();

export const selectWbsUrlState = createSelector(
  selectWbsUrlFeatureState,
  (state: ModuleState) => state.wbsUrl
);

/* export const selectWbsUrl = createSelector(
  selectWbsUrlState,
  (state) => state.wbsUrl
); */

export const selectWbsUrlEntities = createSelector(
  selectWbsUrlState,
  selectEntities
);
