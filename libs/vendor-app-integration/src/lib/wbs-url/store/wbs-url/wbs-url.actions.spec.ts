import * as fromWbsUrl from './wbs-url.actions';

describe('loadWbsUrls', () => {
  it('should return an action', () => {
    expect(fromWbsUrl.sendWbsUrl({ idWbsUrl: undefined }).type).toBe(
      '[AI/Wbs-Url] Send Wbs Url'
    );
  });
});
