/* eslint-disable @typescript-eslint/no-explicit-any */
export interface WbsUrl {
  url: string;
  type: 'wbsUrl';
}

export interface WbsUrlReady {
  type: 'wbsUrlReady';
}

export const WBS_URL_TYPE = 'wbsUrl';
export const WBS_URL_READY_TYPE = 'wbsUrlReady';

export function isTypeOfWbsUrlReady(object: any): object is WbsUrlReady {
  return 'type' in object && object['type'] === WBS_URL_READY_TYPE;
}

export interface IdWbsUrl {
  id: string;
  wbsUrl: WbsUrl;
}
