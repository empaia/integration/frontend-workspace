import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TargetIntegrationComponent } from './components/target-integration/target-integration.component';
import { StoreModule } from '@ngrx/store';
import {
  reducers,
  TARGET_MODULE_FEATURE_KEY,
} from './store/target-feature.state';
import { EffectsModule } from '@ngrx/effects';

@NgModule({
  declarations: [TargetIntegrationComponent],
  imports: [
    CommonModule,
    StoreModule.forFeature(TARGET_MODULE_FEATURE_KEY, reducers),
    EffectsModule.forFeature([]),
  ],
  exports: [TargetIntegrationComponent],
})
export class TargetModule {}
