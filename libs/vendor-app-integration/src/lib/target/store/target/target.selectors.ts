import { createSelector } from '@ngrx/store';
import {
  State as ModuleState,
  selectTargetFeatureState,
} from '../target-feature.state';
import { targetAdapter } from './target.reducer';

const { selectEntities } = targetAdapter.getSelectors();

export const selectTargetState = createSelector(
  selectTargetFeatureState,
  (state: ModuleState) => state.target
);

/* export const selectTargetElement = createSelector(
  selectTargetState,
  (state) => state.target
); */

export const selectTargetEntities = createSelector(
  selectTargetState,
  selectEntities
);
