import { Action, combineReducers, createFeatureSelector } from '@ngrx/store';
import * as fromTarget from './target/target.reducer';

export const TARGET_MODULE_FEATURE_KEY = 'targetModuleFeature';

export const selectTargetFeatureState = createFeatureSelector<State>(
  TARGET_MODULE_FEATURE_KEY
);

export interface State {
  [fromTarget.TARGET_FEATURE_KEY]: fromTarget.State;
}

export function reducers(state: State | undefined, action: Action) {
  return combineReducers({
    [fromTarget.TARGET_FEATURE_KEY]: fromTarget.reducer,
  })(state, action);
}
