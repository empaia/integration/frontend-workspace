import { SafeUrlPipe } from './safe-url.pipe';
import { ɵDomSanitizerImpl } from '@angular/platform-browser';

describe('SafeUrlPipe', () => {
  it('create an instance', () => {
    const pipe = new SafeUrlPipe(new ɵDomSanitizerImpl(document));
    expect(pipe).toBeTruthy();
  });
});
