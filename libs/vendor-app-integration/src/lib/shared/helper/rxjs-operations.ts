export function compareDistinct<T>(prev: T[], curr: T[]): boolean {
  return (
    prev.length === curr.length &&
    prev.every((val, index) => val === curr[index])
  );
}
