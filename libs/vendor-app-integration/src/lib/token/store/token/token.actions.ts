import { createAction, props } from '@ngrx/store';
import { IdToken } from './token.model';

export const sendToken = createAction(
  '[AI/Token] Send Token',
  props<{ idToken: IdToken }>()
);

export const removeTokens = createAction(
  '[AI/Token] Remove Token',
  props<{ ids: string[] }>()
);
