import { Injectable } from '@angular/core';
import { Actions, concatLatestFrom, createEffect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import * as TokenActions from './token.actions';
import * as TargetActions from '../../../target/store/target/target.actions';
import * as TargetSelectors from '../../../target/store/target/target.selectors';
import { distinctUntilChanged, map } from 'rxjs/operators';
import { compareDistinct } from '../../../shared/helper/rxjs-operations';

@Injectable()
export class TokenEffects {
  sendToken$ = createEffect(
    () => {
      return this.actions$.pipe(
        ofType(TokenActions.sendToken),
        map((action) => action.idToken),
        concatLatestFrom(() => [
          this.store.select(TargetSelectors.selectTargetEntities),
        ]),
        map(([appToken, targetEntities]) => {
          const appTarget = targetEntities[appToken.id];
          if (appTarget && appTarget.target && appTarget.target.contentWindow) {
            appTarget.target.contentWindow.postMessage(appToken.token, '*');
          }
        })
      );
    },
    { dispatch: false }
  );

  // remove token if the corresponding target was removed
  removeToken$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(TargetActions.removeTargets),
      map((action) => action.ids),
      distinctUntilChanged(compareDistinct),
      map((ids) => TokenActions.removeTokens({ ids }))
    );
  });

  constructor(private actions$: Actions, private store: Store) {}
}
