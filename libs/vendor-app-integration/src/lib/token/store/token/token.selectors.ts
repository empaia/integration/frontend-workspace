import { createSelector } from '@ngrx/store';
import {
  State as ModuleState,
  selectTokenFeatureState,
} from '../token-feature.state';
import { tokenAdapter } from './token.reducer';

const { selectEntities } = tokenAdapter.getSelectors();

export const selectTokenState = createSelector(
  selectTokenFeatureState,
  (state: ModuleState) => state.token
);

/* export const selectToken = createSelector(
  selectTokenState,
  (state) => state.token
); */

export const selectTokenEntities = createSelector(
  selectTokenState,
  selectEntities
);
