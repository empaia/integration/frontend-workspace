import { createReducer, on } from '@ngrx/store';
import { IdToken } from './token.model';
import * as TokenActions from './token.actions';
import { createEntityAdapter, EntityState } from '@ngrx/entity';

export const TOKEN_FEATURE_KEY = 'token';

export type State = EntityState<IdToken>;

export const tokenAdapter = createEntityAdapter<IdToken>();

export const initialState: State = tokenAdapter.getInitialState();

export const reducer = createReducer(
  initialState,
  on(
    TokenActions.sendToken,
    (state, { idToken }): State =>
      tokenAdapter.upsertOne(idToken, {
        ...state,
      })
  ),
  on(
    TokenActions.removeTokens,
    (state, { ids }): State =>
      tokenAdapter.removeMany(ids, {
        ...state,
      })
  )
);
