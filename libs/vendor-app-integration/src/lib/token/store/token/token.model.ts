/* eslint-disable @typescript-eslint/no-explicit-any */
export interface Token {
  value: string;
  type: 'token';
}

export interface Request {
  type: 'tokenRequest';
}

export interface TokenReady {
  type: 'tokenReady';
}

export const TOKEN_TYPE = 'token';
export const TOKEN_REQUEST_TYPE = 'tokenRequest';
export const TOKEN_READY_TYPE = 'tokenReady';

export function isTypeOfTokenRequest(object: any): object is Request {
  return 'type' in object && object['type'] === TOKEN_REQUEST_TYPE;
}

export function isTypeOfTokenReady(object: any): object is TokenReady {
  return 'type' in object && object['type'] === TOKEN_READY_TYPE;
}

export interface IdToken {
  id: string;
  token: Token;
}
