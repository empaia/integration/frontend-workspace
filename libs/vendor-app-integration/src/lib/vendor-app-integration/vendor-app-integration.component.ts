import {
  Component,
  EventEmitter,
  HostListener,
  Input,
  Output,
} from '@angular/core';
import { Store } from '@ngrx/store';
import {
  Request,
  IdToken,
  TokenReady,
  TokenActions,
  isTypeOfTokenRequest,
  isTypeOfTokenReady,
} from '../token/store';
import { IdTarget, Target } from '../target/store/target/target.model';
import { TargetActions } from '../target/store';
import {
  IdScope,
  ScopeReady,
  ScopeActions,
  isTypeOfScopeReady,
} from '../scope/store';
import {
  IdWbsUrl,
  WbsUrlReady,
  WbsUrlActions,
  isTypeOfWbsUrlReady,
} from '../wbs-url/store';

@Component({
  selector: 'ngx-vendor-app-integration',
  templateUrl: './vendor-app-integration.component.html',
  styleUrls: ['./vendor-app-integration.component.scss'],
})
export class VendorAppIntegrationComponent {
  @Input() public appUrl!: string;

  @Input() public id!: string;
  @Input() public sandboxParameters!: string;

  @Input() public set scope(val: IdScope) {
    if (val) {
      this.store.dispatch(ScopeActions.sendScope({ idScope: val }));
    }
  }

  @Input() public set token(val: IdToken) {
    if (val) {
      this.store.dispatch(TokenActions.sendToken({ idToken: val }));
    }
  }

  @Input() public set wbsUrl(val: IdWbsUrl) {
    if (val) {
      this.store.dispatch(WbsUrlActions.sendWbsUrl({ idWbsUrl: val }));
    }
  }

  @Input() public set removedApps(val: string[]) {
    if (val && val.length) {
      this.store.dispatch(TargetActions.removeTargets({ ids: val }));
    }
  }

  @Output() receiveTokenRequest = new EventEmitter<Request>();

  @Output() receiveScopeReady = new EventEmitter<ScopeReady>();

  @Output() receiveTokenReady = new EventEmitter<TokenReady>();

  @Output() receiveWbsUrlReady = new EventEmitter<WbsUrlReady>();

  constructor(private store: Store) { }

  @HostListener('window:message', ['$event'])
  onMessage(event: MessageEvent): void {
    const iFrame: HTMLIFrameElement = document.getElementById(
      `embeddedApp-${this.id}`
    ) as HTMLIFrameElement;
    if (
      event.source &&
      iFrame?.contentWindow &&
      // event.origin === 'null' && // for some reason the origin isn't null anymore
      event.source === iFrame.contentWindow
    ) {
      this.receiveData(event);
    }
  }

  private receiveData(message: MessageEvent): void {
    if (isTypeOfTokenRequest(message.data)) {
      this.receiveTokenRequest.emit(message.data);
    } else if (isTypeOfScopeReady(message.data)) {
      this.receiveScopeReady.emit(message.data);
    } else if (isTypeOfTokenReady(message.data)) {
      this.receiveTokenReady.emit(message.data);
    } else if (isTypeOfWbsUrlReady(message.data)) {
      this.receiveWbsUrlReady.emit(message.data);
    }
  }

  setTarget(target: Target): void {
    const idTarget: IdTarget = {
      id: this.id,
      target,
    };
    this.store.dispatch(TargetActions.setTarget({ idTarget }));
  }
}
