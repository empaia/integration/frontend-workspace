import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';
import {
  reducers,
  SCOPE_MODULE_FEATURE_KEY,
} from './store/scope-feature.state';
import { EffectsModule } from '@ngrx/effects';
import { ScopeEffects } from './store';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    StoreModule.forFeature(SCOPE_MODULE_FEATURE_KEY, reducers),
    EffectsModule.forFeature([ScopeEffects]),
  ],
})
export class ScopeModule {}
