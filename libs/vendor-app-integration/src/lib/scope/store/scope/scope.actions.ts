import { createAction, props } from '@ngrx/store';
import { IdScope } from './scope.model';

export const sendScope = createAction(
  '[AI/Scope] Send Scope',
  props<{ idScope: IdScope }>()
);

export const removeScopes = createAction(
  '[AI/Scope] Remove Scope',
  props<{ ids: string[] }>()
);
