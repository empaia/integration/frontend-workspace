import { createSelector } from '@ngrx/store';
import {
  State as ModuleState,
  selectScopeFeatureState,
} from '../scope-feature.state';
import { scopeAdapter } from './scope.reducer';

const { selectEntities } = scopeAdapter.getSelectors();

export const selectScopeState = createSelector(
  selectScopeFeatureState,
  (state: ModuleState) => state.scope
);

/* export const selectScope = createSelector(
  selectScopeState,
  (state) => state.scope
); */

export const selectScopeEntities = createSelector(
  selectScopeState,
  selectEntities
);
