import { createReducer, on } from '@ngrx/store';
import { IdScope } from './scope.model';
import * as ScopeActions from './scope.actions';
import { createEntityAdapter, EntityState } from '@ngrx/entity';

export const SCOPE_FEATURE_KEY = 'scope';

export type State = EntityState<IdScope>;

export const scopeAdapter = createEntityAdapter<IdScope>();

export const initialState: State = scopeAdapter.getInitialState();

export const reducer = createReducer(
  initialState,
  on(
    ScopeActions.sendScope,
    (state, { idScope }): State =>
      scopeAdapter.upsertOne(idScope, {
        ...state,
      })
  ),
  on(
    ScopeActions.removeScopes,
    (state, { ids }): State =>
      scopeAdapter.removeMany(ids, {
        ...state,
      })
  )
);
