import { Injectable } from '@angular/core';
import { Actions, concatLatestFrom, createEffect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import * as ScopeActions from './scope.actions';
import * as TargetActions from '../../../target/store/target/target.actions';
import * as TargetSelectors from '../../../target/store/target/target.selectors';
import { distinctUntilChanged, map } from 'rxjs/operators';
import { compareDistinct } from '../../../shared/helper/rxjs-operations';

@Injectable()
export class ScopeEffects {
  sendScope$ = createEffect(
    () => {
      return this.actions$.pipe(
        ofType(ScopeActions.sendScope),
        map((action) => action.idScope),
        concatLatestFrom(() => [
          this.store.select(TargetSelectors.selectTargetEntities),
        ]),
        map(([appScope, targetEntities]) => {
          const appTarget = targetEntities[appScope.id];
          if (appTarget && appTarget.target && appTarget.target.contentWindow) {
            appTarget.target.contentWindow.postMessage(appScope.scope, '*');
          }
        })
      );
    },
    { dispatch: false }
  );

  // remove scope if the corresponding target was removed
  removeScope$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(TargetActions.removeTargets),
      map((action) => action.ids),
      distinctUntilChanged(compareDistinct),
      map((appIds) => ScopeActions.removeScopes({ ids: appIds }))
    );
  });

  constructor(private actions$: Actions, private store: Store) {}
}
