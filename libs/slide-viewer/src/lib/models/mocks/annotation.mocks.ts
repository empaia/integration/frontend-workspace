import {
  IAnnotation,
  IAnnotationCircle,
  IAnnotationRectangle,
} from '../annotation';

export const mockAnnotations: IAnnotation[] = [
  {
    id: '79b5c009-c0e6-4bfe-b232-429f9dd5dbe9',
    name: 'TestCirc2',
    createdAt: new Date('2021-01-27T14:01:05.000Z'),
    updatedAt: new Date('2021-01-27T14:01:05.000Z'),
    description: '🤖🤖🤖',
    creatorId: 'ADD_VALID_USER_ID',
    referenceId: 'id-Ventana',
    referenceType: 'wsi',
    nppCreated: 28674.334763948496,
    annotationType: 'circle',
    radius: 4814,
    center: [7773, 68515],
  } as IAnnotationCircle,
  {
    id: '8881846e-d9a7-40a0-84e0-e966a3355c02',
    name: 'test rect 2',
    createdAt: new Date('2021-01-27T14:52:34.000Z'),
    updatedAt: new Date('2021-01-27T14:52:34.000Z'),
    description: null,
    creatorId: 'ADD_VALID_USER_ID',
    referenceId: 'id-Ventana',
    referenceType: 'wsi',
    nppCreated: 28674.334763948496,
    annotationType: 'rectangle',
    coordinates: [
      [23450, 49111],
      [34632, 49111],
      [34632, 61280],
      [23450, 61280],
      [23450, 49111],
    ],
  } as IAnnotationRectangle,
  {
    id: 'cb672af7-d3ec-4dcf-9997-781ed75eff49',
    name: 'TestCirc',
    createdAt: new Date('2021-01-26T17:13:03.000Z'),
    updatedAt: new Date('2021-01-26T17:13:03.000Z'),
    description: null,
    creatorId: 'ADD_VALID_USER_ID',
    referenceId: 'id-Ventana',
    referenceType: 'wsi',
    nppCreated: 28674.334763948496,
    annotationType: 'circle',
    radius: 6952,
    center: [9527, 64898],
  } as IAnnotationCircle,
];
