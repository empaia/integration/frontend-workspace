import { DataCreatorType } from './creator';

export enum ClassReferenceType {
  Annotation = 'annotation',
}


export interface Class {

  /**
   * ID
   */
  id?: string | null;

  /**
   * Creator ID
   */
  creator_id?: string;

  /**
   * Creator type
   */
  creator_type?: DataCreatorType;


  /**
   * Flag to mark a class as immutable
   */
  is_locked?: boolean | null;

  /**
   * ID of referenced annotation
   */
  reference_id?: string;

  /**
   * Reference type
   */
  reference_type?: string | null;

  /**
   * Class
   */
  type?: 'class';

  /**
   * Taken from EMPAIA App Description / global class value
   */
  value?: string;
}
