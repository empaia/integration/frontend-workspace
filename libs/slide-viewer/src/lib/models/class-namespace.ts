export interface ClassNamespace {
  /**
   * The class description
   */
  description?: string;

  /**
   * The class name
   */
  name: string;

  /**
   * The class value
   */
  value?: string;
}
