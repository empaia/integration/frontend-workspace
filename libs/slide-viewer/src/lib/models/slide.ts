import { Tile } from 'ol';

/**
 * public interface defining a Slide ressource
 */
export interface Slide {
  slideId: string;
  resolver: TileResolverInterface;
  imageInfo: ImageInfo;
}

export interface ImageInfo {
  width: number;
  height: number;
  tileSizes: number[][];
  npp: number;
  numberOfLevels: number;
  resolutions: number[];
  levels?: Level[];
  numberOfOverZoomLevels?: number;
  channels: Channel[];
  channel_depth?: number | null;
}

export interface Channel {
  color: Color;
  id: number;
  name: string;
}

export interface ChannelInfo {
  channel: Channel,
  enabled: boolean,
  min: number,
  max: number,
  gamma: number,
  contrast: number
}


export interface Color {
  a: number;
  b: number;
  g: number;
  r: number;
}


export interface Level {
  dimensions: number[];
  downsample: number;
  npp?: number;
  tiles?: number[];
}

/**
 * Sets the tile URL function of the source.
 * TileSource sources use a function of this type to get the url that provides a tile for a given tile coordinate.
 */
export type TileResolverFn = (coords: number[]) => string;

/**
 * the App is responsible for implementing TileConnector functions to fetch image tiles
 * connector functions are called inside the TileResolver
 * the implementation of this function prepares tile data for be inside with the viewer
 * https://openlayers.org/en/latest/apidoc/module-ol_Tile.html#~LoadFunction
 */
export type TileLoaderFn = (p0: Tile) => void;

/**
 * public interface which the slide viewer library uses
 * to resolve deep zoom tiles
 */
export interface TileResolverInterface {
  tileResolverFn: TileResolverFn | null;
  tileLoaderFn: TileLoaderFn | null;
  imageId: string;
  hasArtificialLevel?: boolean;
}
