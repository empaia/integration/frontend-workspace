export const ANNOTATION_FOCUS_ERROR_MSG = 'Cannot focus annotation! Annotation is outside of image boundaries.';
export const ANNOTATION_CREATION_ERROR_MSG = 'Cannot create annotation! Annotation is outside of image boundaries.';
export const ANNOTATION_INTERSECT_ERROR_MSG = 'Annotation is self intersecting.';

export type SvmErrorMessage =
  | typeof ANNOTATION_FOCUS_ERROR_MSG
  | typeof ANNOTATION_CREATION_ERROR_MSG
  | typeof ANNOTATION_INTERSECT_ERROR_MSG
  ;