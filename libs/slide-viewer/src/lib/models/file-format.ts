export enum WSIFileFormat {
  TIF = 'tif',
  TIFF = 'tiff',
  JPG = 'jpg',
}
