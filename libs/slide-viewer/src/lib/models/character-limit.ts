interface AnnotationCharacterLimit {
  name: number;
  description: number;
}

// could be expanded for more items
// to set character limits
interface CharacterLimit {
  annotation?: AnnotationCharacterLimit;
}

export {
  AnnotationCharacterLimit,
  CharacterLimit,
};
