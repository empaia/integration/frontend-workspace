// put enum in own file to mitigate https://github.com/kulshekhar/ts-jest/issues/944

enum AnnotationType {
  CIRCLE = 'circle',
  RECTANGLE = 'rectangle',
  POLYGON = 'polygon',
  POINT = 'point',
  // the following types are not yet implemented!
  ARROW = 'arrow',
  LINE = 'line',
}
export { AnnotationType };
