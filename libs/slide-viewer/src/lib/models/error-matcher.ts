import { ErrorStateMatcher } from '@angular/material/core';
import { UntypedFormControl, FormGroupDirective, NgForm } from '@angular/forms';

export class AnnotationCardErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: UntypedFormControl | null, _form: FormGroupDirective | NgForm | null): boolean {
    const required = control.hasError('required');
    return required ? !!(control && control.invalid && control.touched) : !!(control && control.invalid && control.dirty);
  }
}
