import { Fill, Style } from 'ol/style';

// export type ColorName = 'blue'|'turquoise'|'dark_green'|'light_green'|'red'|'magenta'|'dark_orange'|'orange'|'yellow'|'light_gray';
// export type ColorSequenceRecord = Record<ColorName, Array<string>>;

// export interface AnnotationPalette {
//   hover: string;
//   roi: string;
//   drawing: string;
//   disabled: string;
//   annotationClasses: string[];
// }

export const featureStyleMode = [
  'DEFAULT',
  'HOVER',
  'SELECTION',
] as const;
export type FeatureStyleMode = typeof featureStyleMode[number];
// export type FeatureStyleMode = 'DEFAULT'|'DRAWING'|'HOVER'| 'DISABLED' |'ANNOTATION_CLASSES';
export type FeatureStyleEntry = [Style, Style];
export type FeatureStylesList = Array<FeatureStyleEntry>;
export type FeatureStyle = Record<FeatureStyleMode, FeatureStyleEntry>;

export interface FeatureStyleMapping {
  [key: string]: FeatureStyle;
}

export interface ColorState {
  color: string;
  colorSelection: string;
  colorHover: string;
}

export interface ClassColorMapping {
  [key: string]: ColorState;
}

export const DEFAULT_TRANSPARENT_FILL = new Fill({ color: 'rgba(255, 255, 255, 0)' });

// for our v2 apps and the pdl1 app augment the annotations with no classes to
// classes: [ 'null' ]
// instead of using null in the v2 apps use 'null' as a string
// when sending it to the backend in a query parse it with JSON.parse()
// JSON.parse('null') --> null
// Do that in annotation conversation fromApi method. This way we don't have
// any performance losses
