import { Observable } from 'rxjs';

export type MultiChannelDataType = Uint8Array[] | Uint16Array[] | Uint32Array[] | number;

export interface RawMultichannelData {
  data: MultiChannelDataType;
}

export interface TileConnector {
  // getSlideTileMultichannel(level: number, x: number, y: number): Promise<RawMultichannelData>;
  getSlideTileBlob(level: number, x: number, y:number): Observable<Blob>
}
