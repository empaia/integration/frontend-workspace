interface ErrorAttributes {
  [key: string]: string;
}

export interface FormErrors {
  [key: string]: ErrorAttributes;
}

export const ANNOTATION_FORM_ERROR_MESSAGES: FormErrors = {
  title: {
    required: 'Annotation must contain a title',
    maxlength: 'The title exceeds the maximum character length',
  },
  description: {
    maxlength: 'The description exceeds the maximum character length',
  }
};
