import { AnnotationType } from './annotation-type';
import { Class } from './class';
import { DataCreatorType } from './creator';

// export enum AnnotationType {
//   CIRCLE = 'circle',
//   RECTANGLE = 'rectangle',
//   POLYGON = 'polygon',
//   POINT = 'point',
//   // the following types are not yet implemented!
//   ARROW = 'arrow',
//   LINE = 'line',
// }

export enum AnnotationReferenceType {
  Wsi = 'wsi',
}

/**
 * public interface - defines a base Annotation
 * concrete Annotation classes should implement an
 * extended interface of this base interface
 *
 * App needs to define a conversion between Annotation API types and
 * SlideViewer Annotation types
 * see file: annotation-conversion.ts in demo app for example
 */
export interface IAnnotation {
  id?: string | null; // set by api
  createdAt?: Date; // set by api
  updatedAt?: Date; // set by api

  name?: string; // titel - set in pop up after drawing an annotation
  description?: string | null; // set in pop up after drawing an annotation
  creatorId?: string; // creatorID must be present in creators input property
  creatorType?: DataCreatorType;
  referenceId?: string; // currently referenceId is assumed to be a slideId
  nppCreated?: number;
  nppViewing?: number[] | null;
  centroid?: number[] | null;

  annotationType: AnnotationType;
  center?: number[]; // expects an array with 2 elements

  classes?: Array<Class>;
  referenceType?: string;

  isLocked?: boolean | null;
}

/**
 * public interface - defines a circular Annotation
 */
export interface IAnnotationCircle extends IAnnotation {
  radius: number;
}

/**
 * public interface - defines a rectangular Annotation
 */
export interface IAnnotationRectangle extends IAnnotation {
  coordinates: number[][]; // 2d array - number[][]
}

/**
 * public interface - defines a polygonal Annotation
 */
export interface IAnnotationPolygon extends IAnnotation {
  coordinates: number[][]; // 2d array - number[][]
}

/**
 * public interface - defines a point Annotation
 */
export interface IAnnotationPoint extends IAnnotation {
  coordinates: number[]; // 1d array - number[]
}

/**
 * implementation of Annotation interface
 * sets all base properties via constructor parameter
 */
export class Annotation implements IAnnotation {
  constructor(opt: Annotation) {
    this.name = opt.name;
    this.description = opt.description;
    this.creatorId = opt.creatorId;
    this.creatorType = opt.creatorType;
    this.referenceId = opt.referenceId;
    this.annotationType = opt.annotationType;
    this.center = opt.center;

    this.referenceType = opt.referenceType;
    this.nppCreated = opt.nppCreated;
    // this.classIds = opt.classIds;
    this.classes = opt.classes;
    this.isLocked = opt.isLocked;
  }
  id?: string;
  createdAt?: Date;
  updatedAt?: Date;
  name?: string;
  description?: string;
  creatorId?: string;
  creatorType?: DataCreatorType;
  referenceId?: string;
  nppCreated?: number;
  nppViewing?: number[];
  centroid?: number[];
  annotationType: AnnotationType;
  center?: number[];
  classes?: Class[];
  referenceType?: string;
  isLocked?: boolean;
}

/**
 * Circle annotation implementation
 */
export class AnnotationCircle extends Annotation {
  constructor(opt: AnnotationCircle) {
    super(opt);
    this.annotationType = AnnotationType.CIRCLE;
    this.radius = opt.radius;
  }
  radius: number;
}

/**
 * Rectangle annotation implementation
 */
export class AnnotationRectangle extends Annotation {
  constructor(opt: AnnotationRectangle) {
    super(opt);
    this.annotationType = AnnotationType.RECTANGLE;
    this.coordinates = opt.coordinates;
  }
  coordinates: number[][];
}

/**
 * Polygon annotation implementation
 */
export class AnnotationPolygon extends Annotation {
  constructor(opt: AnnotationPolygon) {
    super(opt);
    this.annotationType = AnnotationType.POLYGON;
    this.coordinates = opt.coordinates;
  }
  coordinates: number[][];
}

/**
 * Point annotation implementation
 */
export class AnnotationPoint extends Annotation {
  constructor(opt: AnnotationPoint) {
    super(opt);
    this.annotationType = AnnotationType.POINT;
    this.coordinates = opt.coordinates;
  }
  coordinates: number[];
}

/**
 * That's for annotation from the api
 * in certain occasions we need the id
 * set
 */
export interface AnnotationEntity extends Annotation {
  id: string;
}

export interface AnnotationsWithUpdates {
  annotations: Annotation[];
  updatedAnnotationIds: string[];
}
