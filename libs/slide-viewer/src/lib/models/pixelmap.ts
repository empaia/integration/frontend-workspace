import Tile from 'ol/Tile';

export interface PixelMapTileRequesterInterface {
  tileLoadFunction(tile: Tile): void;
}

export interface ChannelClassMapping {
  class_value: string;
  number_value: number;
  name: string;
}

export interface PixelMapInput {
  tileSize: number;
  tileRequesters: PixelMapTileRequesterInterface[];
  channelClassMapping?: ChannelClassMapping[];
}

export interface PixelMapUpdate {
  channels: number[];
}

export interface OpacityChange {
  channel: number;
  opacity: number;
}

export interface ZoomToLevel {
  level: number;
}

export const DEFAULT_LAYER_OPACITY = 0.5;
