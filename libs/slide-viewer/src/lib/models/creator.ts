
export enum DataCreatorType {
  Job = 'job',
  User = 'user',
  Scope = 'scope'
}
