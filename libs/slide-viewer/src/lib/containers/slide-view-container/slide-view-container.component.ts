import { Component, ChangeDetectionStrategy, EventEmitter, Output, ViewChild, NgZone } from '@angular/core';
import { asyncScheduler, Observable } from 'rxjs';
import { distinctUntilChanged, filter, observeOn } from 'rxjs/operators';
import { IAnnotation, AnnotationsWithUpdates } from '../../models/annotation';
import { Store } from '@ngrx/store';

import {
  AnnotationSelectors,
} from '../../store/annotations';
import { SlideSelectors } from '../../store/slide';
import { MouseInteractionType, ToolbarInteractionType } from '../../models/interaction';
import { UiActions, UiSelectors } from '../../store/ui';
import { ClusterSelectors } from '../../store/cluster';
import { CurrentView, ZoomView } from '../../models/ui';
import { Slide } from '../../models/slide';
import { SvmErrorMessage } from '../../models/errors';
import { CharacterLimit } from '../../models/character-limit';
import { CharacterConfigSelectors } from '../../store/character-config';
import { compareDistinct } from '../../models/rxjs-operations';
import { MatMenuTrigger } from '@angular/material/menu';
import { ToolbarComponent } from '../../components/toolbar/toolbar.component';
import { AnnotationHighlightConfig } from '../../config/annotation-highlight.config';
import { OpacityChange, PixelMapInput, PixelMapUpdate, ZoomToLevel } from '../../models/pixelmap';
import { PixelMapsActions, PixelMapsSelectors } from '../../store/pixel-maps';
import { FeatureStyleMapping } from '../../models/feature-style';
import { FeaturesSelectors } from '../../store/features';

@Component({
  selector: 'svm-slide-view-container',
  templateUrl: './slide-view-container.component.html',
  styleUrls: ['./slide-view-container.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SlideViewContainerComponent {
  public slide$: Observable<Slide>;

  public annotations$: Observable<AnnotationsWithUpdates>;
  public discardAnnotations$: Observable<string[]>;
  public clearAnnotations$: Observable<boolean>;
  public highlightedAnnotations$: Observable<Array<string>>;
  public focusedAnnotationId$: Observable<string>;
  public hiddenAnnotationIds$: Observable<string[]>;

  public selectedDrawTool$: Observable<MouseInteractionType>;
  public currentUiTool$: Observable<MouseInteractionType>;
  public deactivateToolbar$: Observable<boolean>;
  public showAnnotationBar$: Observable<boolean>;
  public annotationStrokeWidth$: Observable<number>;
  public featureStyleMapping$: Observable<FeatureStyleMapping>;

  public zoomView$: Observable<ZoomView>;
  public updateViewer$: Observable<number>;

  public clusterDistance$: Observable<number>;
  public annotationCentroids$: Observable<number[][]>;

  public autoSetAnnotationTitle$: Observable<boolean>;
  public renderHideNavigationButton$: Observable<boolean>;

  public allowedInteractionTypes$: Observable<ToolbarInteractionType[] | undefined>;

  public characterConfig$: Observable<CharacterLimit | undefined>;
  public annotationHighlightConfig$: Observable<AnnotationHighlightConfig>;

  public examinationState$: Observable<boolean | undefined>;

  public pixelMapInput$: Observable<PixelMapInput>;
  public pixelMapUpdate$: Observable<PixelMapUpdate>;
  public channelOpacity$: Observable<OpacityChange | undefined>;
  public zoomToPixelMapLevel$: Observable<ZoomToLevel>;

  @Output() public viewerDoubleClicked = new EventEmitter();
  @Output() public annotationCreated = new EventEmitter<IAnnotation>();
  @Output() public annotationHover = new EventEmitter<Array<string>>();
  @Output() public errorMessage = new EventEmitter<SvmErrorMessage>();
  @Output() public moveEnd = new EventEmitter<CurrentView>();
  @Output() public toolSelected = new EventEmitter<ToolbarInteractionType>();
  @Output() public annotationClick = new EventEmitter<string[]>();

  public id = 'map1'; // Math.random().toString();
  public id2 = 'map2';

  @ViewChild(ToolbarComponent) toolbar!: ToolbarComponent;
  private readonly TOOLBAR_TOGGLE_TIMEOUT = 50;

  constructor(
    private store: Store,
    private ngZone: NgZone,
  ) {
    this.slide$ = this.store.select(SlideSelectors.selectSlide).pipe(
      distinctUntilChanged((prev, curr) => prev?.slideId === curr?.slideId),
    );

    this.zoomView$ = this.store.select(UiSelectors.selectZoomView);

    this.annotations$ = this.store
      .select(AnnotationSelectors.selectRenderAnnotationsWithUpdate)
      .pipe(
        filter(a => !!a.annotations),
      );

    this.discardAnnotations$ = this.store.select(AnnotationSelectors.selectDiscardAnnotationIds);

    this.clearAnnotations$ = this.store.select(AnnotationSelectors.selectClearState).pipe(
      observeOn(asyncScheduler)
    );

    this.highlightedAnnotations$ = this.store.select(AnnotationSelectors.selectHighlighted);

    this.focusedAnnotationId$ = this.store.select(AnnotationSelectors.selectFocused);

    this.hiddenAnnotationIds$ = this.store
      .select(AnnotationSelectors.selectHiddenAnnotationIds)
      .pipe(
        distinctUntilChanged(
          compareDistinct
        )
      );

    this.currentUiTool$ = this.store.select(UiSelectors.selectUiTool);

    this.selectedDrawTool$ = this.store.select(UiSelectors.selectUiDrawTool);

    this.clusterDistance$ = this.store.select(ClusterSelectors.selectClusterDistance);

    this.annotationCentroids$ = this.store.select(AnnotationSelectors.selectCentroids);

    this.showAnnotationBar$ = this.store.select(UiSelectors.selectAnnotationBarVisibility);

    this.updateViewer$ = this.store.select(UiSelectors.selectUpdateViewer);

    this.autoSetAnnotationTitle$ = this.store.select(UiSelectors.selectAutoAnnotationTitle);

    this.renderHideNavigationButton$ = this.store.select(UiSelectors.selectRenderHideNavigationButton);

    this.allowedInteractionTypes$ = this.store.select(UiSelectors.selectAllowedInteractionTypes);

    this.characterConfig$ = this.store.select(CharacterConfigSelectors.selectCharacterConfig);

    this.annotationHighlightConfig$ = this.store.select(UiSelectors.selectAnnotationHighlightConfig);

    this.examinationState$ = this.store.select(UiSelectors.selectExaminationState);

    this.pixelMapInput$ = this.store.select(PixelMapsSelectors.selectPixelMapInput);

    this.pixelMapUpdate$ = this.store.select(PixelMapsSelectors.selectPixelMapUpdate);

    this.channelOpacity$ = this.store.select(PixelMapsSelectors.selectChannelOpacity);

    this.annotationStrokeWidth$ = this.store.select(UiSelectors.selectAnnotationStrokeWidth);

    this.featureStyleMapping$ = this.store.select(FeaturesSelectors.selectFeatureStyleMapping);

    this.zoomToPixelMapLevel$ = this.store.select(PixelMapsSelectors.selectZoomToLevel);
  }

  public featureHover(ids: string[]): void {
    this.annotationHover.emit(ids);
    // TODO: Check if necessary
    // this.store.dispatch(AnnotationActions.highlightAnnotations({ ids }));
  }

  public annotationCreate(annotation: IAnnotation): void {
    this.annotationCreated.emit(annotation);
  }

  public toolSelectionChanged(mode: ToolbarInteractionType): void {
    this.toolSelected.emit(mode);
    this.store.dispatch(UiActions.setUiToolMode({ toolMode: { left: mode } }));
  }

  public viewChanged(view: CurrentView): void {
    this.moveEnd.emit(view);
  }

  public zoomViewChanged(view: ZoomView): void {
    this.store.dispatch(UiActions.zoomviewChanged({ zoomview: view }));
  }

  public onErrorMessage(message: SvmErrorMessage): void {
    this.errorMessage.emit(message);
  }

  public onViewerToolbarChange(): void {
    setTimeout(
      (triggers: MatMenuTrigger[]) => {
        triggers?.forEach(t => t?.updatePosition());
      },
      this.TOOLBAR_TOGGLE_TIMEOUT,
      [this.toolbar?.annotationTrigger, this.toolbar?.pixelMapTrigger]
    );
    this.viewerDoubleClicked.emit();
  }

  public onChannelOpacityChanged(channelOpacity: OpacityChange): void {
    this.ngZone.run(() => {
      this.store.dispatch(PixelMapsActions.sVMSetChannelOpacity({ channelOpacity }));
    });
  }

  public onAnnotationStrokeWidthChanged(annotationStrokeWidth: number): void {
    this.ngZone.run(() => {
      this.store.dispatch(UiActions.setAnnotationStrokeWidth({ annotationStrokeWidth }));
    });
  }
}
