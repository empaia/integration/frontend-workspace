import {
  Component,
  Input,
  Output,
  EventEmitter,
  ChangeDetectionStrategy,
  OnDestroy,
} from '@angular/core';
import { UntilDestroy } from '@ngneat/until-destroy';
import { Slide } from '../../models/slide';
import { IAnnotation, AnnotationEntity } from '../../models/annotation';
import { CurrentView } from '../../models/ui';
import { logger, LOG_LEVEL } from '../../tools/logger';

import { Store } from '@ngrx/store';
import { SlideActions } from '../../store/slide';
import {
  AnnotationActions,
  AnnotationSelectors
} from '../../store/annotations';
import { UiActions } from '../../store/ui';
import { SlideViewerModuleConfiguration } from '../../config/module-config';
import { CLEAR_STORE } from '../../store/meta.reducer';
import { ToolbarInteractionType } from '../../models/interaction';
import { ClusterActions } from '../../store/cluster';
import { UiConfig } from '../../models/viewer-config';
import { CharacterLimit } from '../../models/character-limit';
import { CharacterConfigActions } from '../../store/character-config';
import { Subject } from 'rxjs';
import { filter, takeUntil } from 'rxjs/operators';
import { AnnotationHighlightConfig } from '../../config/annotation-highlight.config';
import { PixelMapsActions } from '../../store/pixel-maps';
import { ClassColorMapping } from '../../models/feature-style';
import { PixelMapInput, PixelMapUpdate, ZoomToLevel } from '../../models/pixelmap';
import { FeaturesActions } from '../../store/features';

@UntilDestroy()
@Component({
  selector: 'svm-slide-viewer',
  templateUrl: './slide-viewer.component.html',
  styleUrls: ['./slide-viewer.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SlideViewerComponent implements OnDestroy {
  private destroy$ = new Subject();

  @Output() public annotationAdded = new EventEmitter<Partial<IAnnotation>>();
  @Output() public annotationHover = new EventEmitter<string[]>();
  @Output() public annotationClick = new EventEmitter<string[]>();
  @Output() public annotationErrorMessage = new EventEmitter<string>();

  @Output() public slideMoveEnd = new EventEmitter<CurrentView>();
  // @Output() public selectedSlideChanged = new EventEmitter<string>();
  @Output() public doubleClicked = new EventEmitter();
  @Output() public toolSelected = new EventEmitter<ToolbarInteractionType>();
  @Output() public requestAnnotations = new EventEmitter<string[]>();

  @Input()
  public set slide(val: Slide) {
    this.store.dispatch(SlideActions.addSlide({ slide: val }));
  }

  @Input()
  public set selectedSlideId(val: string) {
    this.store.dispatch(SlideActions.selectSlideId({ slideId: val }));
  }

  @Input()
  public set annotationIds(val: string[]) {
    this.store.dispatch(
      AnnotationActions.setAnnotationIds({ annotationIds: val })
    );
  }

  @Input()
  public set annotations(val: Array<AnnotationEntity>) {
    if (val && val.length) {
      this.store.dispatch(
        AnnotationActions.setAnnotations({ annotations: val })
      );
    }
  }

  @Input()
  public set hideAnnotationIds(val: string[]) {
    if (val) {
      this.store.dispatch(AnnotationActions.hideAnnotations({ hiddenIds: val }));
    }
  }

  @Input()
  public set removeAnnotations(val: string[]) {
    if (val) {
      this.store.dispatch(AnnotationActions.hideAnnotations({ hiddenIds: val }));
    }
  }

  @Input()
  public set clearAnnotations(val: boolean) {
    if (val) {
      this.store.dispatch(AnnotationActions.clearAnnotations());
    }
  }

  @Input()
  public set highlightAnnotations(val: Array<string>) {
    if (val) {
      this.store.dispatch(AnnotationActions.highlightAnnotations({ ids: val }));
    }
  }

  @Input()
  public set focusAnnotation(val: string) {
    this.store.dispatch(AnnotationActions.focusAnnotation({ id: val }));
  }

  @Input() public set clusterDistance(distance: number) {
    if (distance) {
      this.store.dispatch(ClusterActions.setClusterDistance({
        clusterDistance: distance
      }));
    }
  }

  @Input() public set annotationCentroidsForClustering(clusters: number[][]) {
    if (clusters) {
      this.store.dispatch(AnnotationActions.setCentroids({
        centroids: clusters
      }));
    }
  }

  @Input() public set uiConfig(config: UiConfig) {
    if (config) {
      this.store.dispatch(
        UiActions.setUIConfig({
          uiConfig: config
        })
      );
    }
  }

  @Input() public set updateSlideViewer(value: number) {
    this.store.dispatch(UiActions.setUpdateViewer({ update: value }));
  }

  @Input() public set interactionType(interactionType: ToolbarInteractionType) {
    this.store.dispatch(UiActions.setUiToolMode({ toolMode: { left: interactionType } }));
  }

  @Input() public set allowedInteractionTypes(allowedTypes: ToolbarInteractionType[]) {
    this.store.dispatch(UiActions.setAllowedInteractionTypes({ allowedTypes }));
  }

  @Input() public set characterConfig(config: CharacterLimit) {
    this.store.dispatch(CharacterConfigActions.setCharacterConfig({ config }));
  }

  @Input() public set examinationState(examinationClosed: boolean) {
    if (examinationClosed !== null && examinationClosed !== undefined) {
      this.store.dispatch(UiActions.setExaminationState({ examinationClosed }));
    }
  }

  @Input() public set annotationHighlightConfig(annotationHighlightConfig: AnnotationHighlightConfig) {
    this.store.dispatch(UiActions.setAnnotationHighlightConfig({ annotationHighlightConfig }));
  }

  @Input() public set pixelMapInput(pixelMapInput: PixelMapInput) {
    this.store.dispatch(PixelMapsActions.sVMSetPixelMapInput({ pixelMapInput }));
  }

  @Input() public set pixelMapUpdate(pixelMapUpdate: PixelMapUpdate) {
    if (pixelMapUpdate) {
      this.store.dispatch(PixelMapsActions.sVMUpdatePixelMap({ pixelMapUpdate }));
    }
  }

  @Input() public set classColorMapping(classColorMapping: ClassColorMapping) {
    if (classColorMapping) {
      this.store.dispatch(FeaturesActions.setClassColorMapping({ classColorMapping }));
    }
  }

  @Input() public set zoomToPixelMapLevel(zoomToLevel: ZoomToLevel) {
    if (zoomToLevel) {
      this.store.dispatch(PixelMapsActions.sVMZoomToPixelMapLevel({ zoomToLevel }));
    }
  }

  constructor(
    private store: Store,
    public config: SlideViewerModuleConfiguration,
  ) {
    this.store
      .select(AnnotationSelectors.selectMissingCachedAnnotations)
      .pipe(
        takeUntil(this.destroy$),
        filter(ids => !!ids.missingAnnotations.length)
      )
      .subscribe(ids => this.requestAnnotations.emit(ids.missingAnnotations));

    if (config.logging === true) {
      logger.setLevel(LOG_LEVEL.VERBOSE);
    }
  }

  ngOnDestroy(): void {
    this.destroy$.next(null);
    this.store.dispatch(CLEAR_STORE());
  }

}
