// import { SlideViewerComponent } from './slide-viewer.component';
// import { createComponentFactory, Spectator } from '@ngneat/spectator';
// import { MaterialModule } from '../../material/material.module';
// import { SvmStoreModule } from '../../store/svm-store.module';
// import { SlideViewContainerComponent } from '../slide-view-container/slide-view-container.component';
// import { SlideViewerModuleConfiguration } from '../../config/module-config';
// import { MockComponents } from 'ng-mocks';
// import { StoreModule } from '@ngrx/store';
// import { EffectsModule } from '@ngrx/effects';
// import { AnnotationEntity } from '../../models/annotation';
// import { AnnotationType } from '../../models/annotation-type';

describe('SlideViewerComponent', () => {
  // const annotationMap: Map<string, AnnotationEntity> = new Map<string, AnnotationEntity>();

  // let spectator: Spectator<SlideViewerComponent>;
  // const createComponent = createComponentFactory({
  //   component: SlideViewerComponent,
  //   imports: [
  //     MaterialModule,
  //     SvmStoreModule,
  //     StoreModule.forRoot({}),
  //     EffectsModule.forRoot([]),
  //   ],
  //   declarations: [
  //     MockComponents(
  //       SlideViewContainerComponent
  //     ),
  //   ],
  //   providers: [
  //     SlideViewerModuleConfiguration
  //   ]
  // });

  // beforeEach(() => {
  //   spectator = createComponent();
  //   annotationMap.clear();
  //   for (let i = 0; i < 150000; i++) {
  //     annotationMap.set(i.toString(), {id: i.toString(), annotationType: AnnotationType.POINT});
  //   }
  // });

  // it('should create the component', () => {
  //   expect(spectator.component).toBeDefined();
  // });

  // it('should miss all 3 passed annotation ids', () => {
  //   let output;
  //   const annotationIds: string[] = [];
  //   for (let i = 0; i < 3; i++) {
  //     annotationIds.push(i.toString());
  //   }
  //   spectator.output('requestAnnotations').subscribe(result => output = result);
  //   spectator.setInput('annotationIds', annotationIds);
  //   expect(output).toEqual(annotationIds);
  // });

  // it('should overflow buffer, but all should stay in memory', () => {
  //   let output;
  //   spectator.output('requestAnnotations').subscribe(result => output = result);
  //   spectator.setInput('annotations', [...annotationMap.values()]);
  //   spectator.setInput('annotationIds', [...annotationMap.keys()]);
  //   // undefined because it's filtered in the pipe
  //   expect(output).toEqual(undefined);
  // });

  it.todo('Write tests');
});
