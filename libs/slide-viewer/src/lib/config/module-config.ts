import { Injectable } from '@angular/core';


@Injectable({
  providedIn: 'root',
})
export class SlideViewerModuleConfiguration {
  logging = false;
}

export interface SlideViewerModuleConfigurationParams {
  logging?: boolean;
}
