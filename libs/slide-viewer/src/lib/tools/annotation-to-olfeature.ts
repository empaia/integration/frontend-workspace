import { Feature } from 'ol';
import { Coordinate } from 'ol/coordinate';
import { Circle, Geometry, Point, Polygon } from 'ol/geom';
import {
  IAnnotation,
  AnnotationCircle,
  AnnotationPoint,
  AnnotationPolygon,
  AnnotationRectangle,
} from '../models/annotation';
import { AnnotationType } from '../models/annotation-type';
import { AxisConverter } from './axis-converter';

export class AnnotationToOlFeature {
  public static annotationToFeature(annotation: IAnnotation): Feature<Geometry> {
    switch (annotation.annotationType) {
      case AnnotationType.CIRCLE:
        return this.annotationToCircleFeat(annotation as AnnotationCircle);
      case AnnotationType.RECTANGLE:
        return this.annotationToPolygonFeat(annotation as AnnotationRectangle);
      case AnnotationType.POLYGON:
        return this.annotationToPolygonFeat(annotation as AnnotationPolygon);
      case AnnotationType.POINT:
        return this.annotationToPointFeat(annotation as AnnotationPoint);
      default: {
        throw new Error(
          `[LayerComponent] Unknown Annotation in annotationToFeature ${annotation?.annotationType}`
        );
      }
    }
  }

  public static annotationToCircleFeat(a: AnnotationCircle): Feature<Geometry> {
    return new Feature({
      geometry: new Circle(
        AxisConverter.invertYAxis(a.center),
        a.radius
      ),
    });
  }

  public static annotationToPolygonFeat(
    a: AnnotationRectangle | AnnotationPolygon,
  ): Feature<Geometry> {
    /**
     * OL expects following array shape:
     * [[[x0, y0], [x1, y1], [x2, y2], [x3, y3]]]
     */
    const coords: Coordinate[][] = [...[...[AxisConverter.invertYAxisOfArray(a.coordinates)]]];
    // logger.log(`[Layer Component] Poly coords`, coords);
    const feature = new Feature({
      geometry: new Polygon(coords),
    });
    return feature;
  }

  public static annotationToPointFeat(a: AnnotationPoint): Feature<Geometry> {
    return new Feature({
      geometry: new Point(AxisConverter.invertYAxis(a.coordinates)),
    });
  }

  public static addAnnotationInfoToFeature(
    a: IAnnotation,
    f: Feature<Geometry>
  ): void {
    f.setId(a.id);
    f.setProperties({ annotation: { ...a } });
  }
}
