import { ImageTile, Tile } from 'ol';
import { ImageInfo, TileResolverInterface } from '../models/slide';
import { TileConnector } from '../models/tile-connector';
import { firstValueFrom } from 'rxjs';

export class TileResolver implements TileResolverInterface {

  constructor(
    private loader: TileConnector,
    public imageId: string,
    public imageInfo: ImageInfo,
    public hasArtificialLevel = false,
    // public multichannel: boolean = false,
  ) {  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  public tileResolverFn: any = null;

  public tileLoaderFn = async (tile: Tile) => {
    const tileCoords = tile.getTileCoord();
    let level = this.imageInfo.numberOfLevels - tileCoords[0] - (this.hasArtificialLevel ? 0 : 1);

    if (isNaN(level)) {
      throw new Error('Error in Tile Resolver - Tile level invalid!');
    }

    if (level < 0) {
      level = 0;
    }
    const x = tileCoords[1];
    const y = tileCoords[2];

    // if (this.multichannel) {
    //   const tileData: RawMultichannelData = await this.loader.getSlideTileMultichannel(level, x, y);
    //   const resultImageData = processData(tileData.data, this.imageInfo.channels);
    //   return ((tile as ImageTile).getImage() as HTMLImageElement).src = getUriFromImageData(resultImageData);
    // } else {
    //   const blob: Blob = await this.loader.getSlideTileBlob(level, x, y);
    //   return ((tile as ImageTile).getImage() as HTMLImageElement).src = URL.createObjectURL(blob);
    // }

    const blob = await firstValueFrom(this.loader.getSlideTileBlob(level, x, y), { defaultValue: new Blob() });
    return ((tile as ImageTile).getImage() as HTMLImageElement).src = URL.createObjectURL(blob);
  };
}
