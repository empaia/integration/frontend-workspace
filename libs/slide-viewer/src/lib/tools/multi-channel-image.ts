import { Channel } from '../models/slide';
import { MultiChannelDataType } from '../models/tile-connector';
import { SlideChannelDataCache } from '../services/slide-channel-data-cache';

export function processData(data: MultiChannelDataType, channels: Array<Channel>): ImageData {
  let resultImageData = new ImageData(data['width'], data['height']);

  const channelsInfo = SlideChannelDataCache.channelsInfo.getValue();

  channels.forEach((channel: Channel, index: number) => {
    if (channelsInfo && channelsInfo[index]) {
      // if (client['collectHistogramData']) {
      //   SlideChannelDataCache.setHistogramDataForChannel(channelsInfo[index].channel.name, data[index]);
      // }
      const channelInfo = channelsInfo[index];
      if (channelInfo.enabled) {
        const normalizedUintArray = this.normalizeUnitArrayAndColorizeLayer(data[index], channelInfo.min, channelInfo.max, data['width'], data['height'], channelInfo.channel);
        resultImageData = this.sumTwoImageData(resultImageData, normalizedUintArray);
      }
    } else {
      resultImageData = this.sumTwoImageData(resultImageData, this.colorImageDataFromUintArray(data[index], data['width'], data['height'], channel));
    }
  });
  return resultImageData;
}

export function normalizeUnitArrayAndColorizeLayer(data: Uint8Array | Uint16Array | Uint32Array, min: number, max: number, width: number, height: number, channel: Channel): ImageData {
  if (data.length !== width * height) {
    throw new Error('Input data does not match specified image dimensions.');
  }

  if (max === min) {
    throw new Error('Max and min values cannot be the same.');
  }

  if (!channel || !channel.color || typeof channel.color.r !== 'number' || typeof channel.color.g !== 'number' || typeof channel.color.b !== 'number') {
    throw new Error('Channel or color properties are missing or invalid.');
  }

  const imageData = new ImageData(width, height);

  for (let i = 0; i < data.length; i++) {
    const norm = Math.trunc((data[i] - min) / (max - min) * 255);

    imageData.data[4 * i] = norm * channel.color.r / 255;
    imageData.data[4 * i + 1] = norm * channel.color.g / 255;
    imageData.data[4 * i + 2] = norm * channel.color.b / 255;
    imageData.data[4 * i + 3] = 255;
  }

  return imageData;
}


export function colorImageDataFromUintArray(data: Uint32Array | Uint16Array | Uint8Array, width: number, height: number, channel: Channel): ImageData {
  if (data.length !== width * height) {
    throw new Error('Input data does not match specified image dimensions.');
  }

  const imageData = new ImageData(width, height);
  let divisor;

  switch(data.constructor) {
    case Uint8Array:
      divisor = 255;
      break;
    case Uint16Array:
      divisor = 65535;
      break;
    case Uint32Array:
      divisor = 4294967295;
      break;
    default:
      throw new Error('Unsupported data type. Only Uint8Array, Uint16Array, and Uint32Array are supported.');
  }

  for (let i = 0; i < data.length; i++) {
    imageData.data[4 * i] = Math.round((data[i] * channel.color.r) / divisor);
    imageData.data[4 * i + 1] = Math.round((data[i] * channel.color.g) / divisor);
    imageData.data[4 * i + 2] = Math.round((data[i] * channel.color.b) / divisor);
    imageData.data[4 * i + 3] = 255;
  }
  return imageData;
}

// get image data from Uint8Array and convert to DataURL
export function getUriFromImageData(data: ImageData): string {
  const canvas = document.createElement('CANVAS') as HTMLCanvasElement;
  canvas.width = data.width;
  canvas.height = data.height;
  const ctx = canvas.getContext('2d');
  ctx.putImageData(data, 0, 0);
  return canvas.toDataURL();
}

export function sumTwoImageData(sum: ImageData, imageData: ImageData): ImageData {
  if (sum.data.length !== imageData.data.length) {
    throw new Error('Input ImageData objects have different dimensions.');
  }

  for (let i = 0; i < imageData.data.length; i += 4) {
    sum.data[i] = Math.min(Math.max(sum.data[i] + imageData.data[i], 0), 255); // R value
    sum.data[i + 1] = Math.min(Math.max(sum.data[i + 1] + imageData.data[i + 1], 0), 255); // G value
    sum.data[i + 2] = Math.min(Math.max(sum.data[i + 2] + imageData.data[i + 2], 0), 255); // B value
    sum.data[i + 3] = 255; // A  value
  }

  return sum;
}
