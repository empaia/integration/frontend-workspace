import { EntityCachingAdapter } from '../models/caching.models';
import { createEntityCachingAdapter } from './caching-adapter';
import { DEFAULT_ENTITY_CACHE_SIZE } from '../models/caching.constants';

describe('CachingAdapter', () => {
  interface State {
    text: string;
  }

  it('create instance of EntityCachingAdapter', () => {
    const stringCachingAdapter: EntityCachingAdapter<string> = createEntityCachingAdapter<string>(
      (str) => hashCode(str)
    );

    const state = stringCachingAdapter.getInitialState();
    expect(state.addedEntityIds).toBeUndefined();
    expect(state.cacheSizeSoftConstraint).toBe(DEFAULT_ENTITY_CACHE_SIZE);
    expect(state.entityCache.size).toBe(0);
  });

  it('create caching adapter with user defined cache size', () => {
    const stringCachingAdapter: EntityCachingAdapter<string> = createEntityCachingAdapter<string>(
      (str) => hashCode(str)
    );

    const state = stringCachingAdapter.getInitialState(undefined, 100);
    expect(state.addedEntityIds).toBeUndefined();
    expect(state.cacheSizeSoftConstraint).toBe(100);
    expect(state.entityCache.size).toBe(0);
  });

  it('create caching adapter and add/remove entities', () => {
    const stringCachingAdapter: EntityCachingAdapter<string> = createEntityCachingAdapter<string>(
      (str) => hashCode(str).toString()
    );

    const initialState: State = {
      text: 'test',
    };
    const state = stringCachingAdapter.getInitialState(initialState);
    stringCachingAdapter.addOne('new_string', state);
    const cachedStrings = stringCachingAdapter.getSelectors().selectAll(state);
    expect(cachedStrings.length).toBe(1);
    expect(cachedStrings[0]).toEqual('new_string');

    stringCachingAdapter.removeOne(hashCode('new_string').toString(), state);
    expect(stringCachingAdapter.getSelectors().selectAll(state).length).toBe(0);
  });

  it('create caching adapter and add/remove entities', () => {
    const stringCachingAdapter: EntityCachingAdapter<string> = createEntityCachingAdapter<string>(
      (str) => hashCode(str).toString()
    );

    const initialState: State = {
      text: 'test',
    };
    const state = stringCachingAdapter.getInitialState(initialState);
    stringCachingAdapter.addOne('new_string', state);
    const cachedStrings = stringCachingAdapter.getSelectors().selectAll(state);
    expect(cachedStrings.length).toBe(1);
    expect(cachedStrings[0]).toEqual('new_string');

    stringCachingAdapter.removeOne(hashCode('new_string').toString(), state);
    expect(stringCachingAdapter.getSelectors().selectAll(state).length).toBe(0);
  });

  it('create caching adapter and add/remove many entities', () => {
    const stringCachingAdapter: EntityCachingAdapter<string> = createEntityCachingAdapter<string>(
      (str) => hashCode(str).toString()
    );

    const initialState: State = {
      text: 'test',
    };
    const state = stringCachingAdapter.getInitialState(initialState);
    
    const stringArray = [...Array(10).keys()].map(n => `new_string${n}`);
    stringCachingAdapter.addMany(stringArray, state);
    const cachedStrings = stringCachingAdapter.getSelectors().selectAll(state);
    expect(cachedStrings.length).toBe(10);
    expect(cachedStrings[0]).toEqual('new_string0');
    expect(cachedStrings[9]).toEqual('new_string9');

    stringCachingAdapter.removeOne(hashCode('new_string1').toString(), state);
    expect(stringCachingAdapter.getSelectors().selectAll(state).length).toBe(9);

    stringCachingAdapter.removeMany([hashCode('new_string0').toString(), hashCode('new_string9').toString()], state);
    expect(stringCachingAdapter.getSelectors().selectAll(state).length).toBe(7);

    stringCachingAdapter.removeAll(state);
    expect(stringCachingAdapter.getSelectors().selectAll(state).length).toBe(0);
  });

  function hashCode (str){
    let hash = 0;
    if (str.length == 0) return hash;
    for (let i = 0; i < str.length; i++) {
      const char = str.charCodeAt(i);
      hash = ((hash<<5)-hash)+char;
      hash = hash & hash;
    }
    return hash;
  }

});

