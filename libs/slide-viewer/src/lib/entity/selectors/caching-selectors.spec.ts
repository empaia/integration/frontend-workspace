import { createEntityCachingAdapter, EntityCachingAdapter } from '..';
import { CachingSelectors } from './caching-selectors';

describe('CachingSelectors', () => {
  it('should create an instance', () => {
    expect(new CachingSelectors()).toBeTruthy();
  });

  it('create caching adapter and add/remove entities', () => {
    const stringCachingAdapter: EntityCachingAdapter<string> = createEntityCachingAdapter<string>(
      (str) => str
    );

    const state = stringCachingAdapter.getInitialState({});
    stringCachingAdapter.addMany(['test1', 'test2', 'test3'], state);
    const selectors = stringCachingAdapter.getSelectors();
    expect(selectors.selectAll(state).length).toBe(3);
    expect(selectors.selectIds(state)).toEqual(['test1', 'test2', 'test3']);
    expect(selectors.selectTotal(state)).toBe(3);
  });
});
