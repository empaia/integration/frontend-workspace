import { Directive, EventEmitter, HostListener, Input, Output } from '@angular/core';

@Directive({
  selector: '[svmCloseElement]'
})
export class CloseElementDirective {
  @Input() open = false;
  @Output() closeElement = new EventEmitter<void>();
  private isOutSide = true;

  @HostListener('window:click')
  onMouseClick(): void {
    if (this.open && this.isOutSide) {
      this.closeElement.emit();
    }
  }

  @HostListener('mouseenter')
  onMouseEnter(): void {
    this.isOutSide = false;
  }

  @HostListener('mouseleave')
  onMouseLeave(): void {
    this.isOutSide = true;
  }
}
