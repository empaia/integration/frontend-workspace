import {
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  EventEmitter,
  Input, OnChanges,
  OnDestroy,
  Output, SimpleChanges,
  ViewChild
} from '@angular/core';
import {
  DrawType,
  InteractionType,
  ToolbarInteractionType,
} from '../../models/interaction';
import { AfterViewInit } from '@angular/core';
import { MatMenuTrigger } from "@angular/material/menu";
import { MatButtonToggleChange } from '@angular/material/button-toggle';
import { Subject, takeUntil } from 'rxjs';
import { OpacityChange, PixelMapInput } from '../../models/pixelmap';

@Component({
  selector: 'svm-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ToolbarComponent implements AfterViewInit, OnChanges, OnDestroy {

  private _renderNavigationButton: boolean;

  @Input() public set renderHideNavigationButton(val: boolean) {
    this._renderNavigationButton = val;
  }

  @Input() public activeTool: ToolbarInteractionType = InteractionType.Move;

  @Input() public allowedTools: ToolbarInteractionType[] | undefined = undefined;

  @Input() public isExaminationClosed: boolean | undefined = undefined;

  @Input() public pixelMapInput!: PixelMapInput;

  @Input() public annotationStrokeWidth!: number;

  @Output() public selectedTool = new EventEmitter<ToolbarInteractionType>();
  @Output() public toggleAppNavigation = new EventEmitter<void>();
  @Output() public channelOpacityChanged = new EventEmitter<OpacityChange>();
  @Output() public strokeWidthChanged = new EventEmitter<number>();

  private destroy$ = new Subject();

  public navigationHidden = false;
  public annotationClassesPalettesMenuVisible = false;
  public pixelMapOpacityMenuVisible = false;
  public allowedToolsSet: Set<ToolbarInteractionType>;

  readonly DrawType = DrawType;
  readonly InteractionType = InteractionType;

  @ViewChild('hideNavigation') hideNavigationButton!: ElementRef;
  @ViewChild('annotationClassesPalettesTrigger') annotationTrigger: MatMenuTrigger;
  @ViewChild('pixelMapOpacityTrigger') pixelMapTrigger: MatMenuTrigger;

  ngAfterViewInit(): void {
    this.hideNavigationButton.nativeElement.style.display = this._renderNavigationButton? 'block' : 'none';
    this.annotationTrigger.menuOpened.pipe(takeUntil(this.destroy$)).subscribe(() =>
      this.annotationClassesPalettesMenuVisible = true
    );
    this.annotationTrigger.menuClosed.pipe(takeUntil(this.destroy$)).subscribe(() =>
      this.annotationClassesPalettesMenuVisible = false
    );
    this.pixelMapTrigger.menuOpened.pipe(takeUntil(this.destroy$)).subscribe(() =>
      this.pixelMapOpacityMenuVisible = true
    );
    this.pixelMapTrigger.menuClosed.pipe(takeUntil(this.destroy$)).subscribe(() =>
      this.pixelMapOpacityMenuVisible = false
    );
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes['allowedTools']) {
      this.allowedToolsSet = new Set<ToolbarInteractionType>(this.allowedTools);
    }
  }

  ngOnDestroy(): void {
    this.destroy$.next(null);
  }

  onToggleSelectionChange(event: MatButtonToggleChange): void {
    this.activeTool = event.value;
    this.selectedTool.emit(this.activeTool);
  }

  toggleNavigation() {
    this.navigationHidden = !this.navigationHidden;
    this.toggleAppNavigation.emit();
  }

  annotationStrokeWidthSliderChanged(value: number): void {
    this.strokeWidthChanged.emit(value);
  }

  // isCurrentAnnotationColorPalette(annotationPalette: AnnotationPalette): boolean {
  //   const index = this.featureStyleService.getAnnotationPalettes().indexOf(annotationPalette);
  //   return index === this.featureStyleService.getAnnotationPaletteIndex();
  // }

  // activateAnnotationColorPalette(annotationPalette: AnnotationPalette, $event: MouseEvent): void {
  //   $event.stopPropagation();
  //   const index = this.featureStyleService.getAnnotationPalettes().indexOf(annotationPalette);
  //   this.featureStyleService.setAnnotationPaletteIndex(index);
  // }

  onClosePalette(): void {
    if (this.annotationTrigger.menuOpen) {
      this.annotationTrigger.closeMenu();
    }
  }

  onCloseOpacity(): void {
    if (this.pixelMapTrigger.menuOpen) {
      this.pixelMapTrigger.closeMenu();
    }
  }
}
