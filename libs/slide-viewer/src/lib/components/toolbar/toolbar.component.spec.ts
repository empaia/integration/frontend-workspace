
import {
  createComponentFactory,
  Spectator,
} from '@ngneat/spectator/jest';

import { ToolbarComponent } from './toolbar.component';
import { MaterialModule } from '../../material/material.module';
import { MatMenuModule } from "@angular/material/menu";
import { MockComponents, MockDeclarations } from 'ng-mocks';
import { CloseElementDirective } from '../../directives/close-element.directive';
import { PixelMapOpacityMenuComponent } from '../pixel-map-opacity-menu/pixel-map-opacity-menu.component';
import { ToolbarInteractionType } from '../../models/interaction';

describe('ToolbarComponent', () => {
  let spectator: Spectator<ToolbarComponent>;
  const createComponent = createComponentFactory({
    component: ToolbarComponent,
    imports: [MaterialModule, MatMenuModule],
    declarations: [
      MockDeclarations(
        CloseElementDirective
      ),
      MockComponents(
        PixelMapOpacityMenuComponent,
      )
    ]
  });

  beforeEach(
    () =>
      (spectator = createComponent({
        props: {
          allowedToolsSet: new Set<ToolbarInteractionType>()
          // selectedTool: selectTool
        },
      }))
  );

  it('should exist', () => {
    expect(spectator.component).toBeDefined();
  });

  // it('should select toolbar draw mode', async () => {
  //   const selectTool = jest.fn();

  //   spectator.output<string>('activeTool').subscribe(result =>
  //     selectTool(result)
  //   );

  //   // TODO: re implement test with spectator
  //   // getByRole('button')
  //   // const circleButton = screen.getByTestId('tool-circle');
  //   // const btn0 = spectator.query(byTextContent('id: id-Leica', { selector: 'div' }));
  //   // expect(circleButton).toBeTruthy();
  //   // fireEvent.click(circleButton);
  //   // expect(selectTool).toHaveBeenCalledWith(DrawType.Circle);
  //   // const selButton = screen.getByRole('button', { pressed: true });
  //   // expect(circleButton.firstChild).toBe(selButton);
  //   // expect(circleButton.getAttribute('aria-pressed')).toBe('true');
  // });
});
