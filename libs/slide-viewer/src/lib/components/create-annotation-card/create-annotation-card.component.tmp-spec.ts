import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateAnnotationCardComponent } from './create-annotation-card.component';

describe('CreateAnnotationCardComponent', () => {
  let component: CreateAnnotationCardComponent;
  let fixture: ComponentFixture<CreateAnnotationCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CreateAnnotationCardComponent],
      teardown: { destroyAfterEach: false },
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateAnnotationCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
