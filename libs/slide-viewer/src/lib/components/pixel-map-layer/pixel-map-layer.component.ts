/* eslint-disable @typescript-eslint/ban-ts-comment */
import { AfterViewInit, ChangeDetectionStrategy, Component, Input, OnDestroy, OnInit } from '@angular/core';
import { Map } from 'ol';
import TileLayer from 'ol/layer/Tile';
import { XYZ } from 'ol/source';
import { MapService } from '../../services/map.service';
import { MapIdService } from '../../services/map-id.service';
import { PROJECTION_KEY } from '../../models/types';
import TileGrid from 'ol/tilegrid/TileGrid';
import { Slide } from '../../models/slide';
import { DEFAULT_LAYER_OPACITY, OpacityChange, PixelMapInput, PixelMapTileRequesterInterface, PixelMapUpdate, ZoomToLevel } from '../../models/pixelmap';


@Component({
  selector: 'svm-pixel-map-layer',
  templateUrl: './pixel-map-layer.component.html',
  styleUrls: ['./pixel-map-layer.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PixelMapLayerComponent implements OnInit, AfterViewInit, OnDestroy {
  private map: Map;
  private layers: TileLayer<XYZ>[] = [];

  @Input() public mapId: string;

  private _slide: Slide;
  @Input() public set slide(val: Slide) {
    if (val && this.pixelMapInput) {
      this.initSlideMap(val, this.pixelMapInput);
    } else {
      this.removeLayers();
    }

    this._slide = val;
  }

  public get slide(): Slide {
    return this._slide;
  }

  private _pixelMapInput: PixelMapInput;
  @Input() public set pixelMapInput(val: PixelMapInput) {
    if (val && this.slide) {
      this.initSlideMap(this.slide, val);
    } else {
      this.removeLayers();
    }

    this._pixelMapInput = val;
  }

  public get pixelMapInput(): PixelMapInput {
    return this._pixelMapInput;
  }

  @Input() public set pixelMapUpdate(val: PixelMapUpdate) {
    if (val) {
      val.channels.forEach(channel => this.layers[channel]?.getSource().refresh());
    }
  }

  @Input() public set channelOpacity(val: OpacityChange) {
    if (val) {
      this.layers[val.channel].setOpacity(val.opacity);
    }
  }

  @Input() public set zoomToLevel(val: ZoomToLevel) {
    if (val && this.map) {
      this.map.getView().setZoom(val.level);
    }
  }

  constructor(
    private mapService: MapService,
    private mapIdService: MapIdService,
  ) {}

  public ngOnInit(): void {
    this.mapIdService.setId(this.mapId);
    this.map = this.mapService.getMap(this.mapId);

    if (!this.mapId) {
      this.mapId = 'map';
    }
  }

  public ngAfterViewInit(): void {
    if (this.slide && this.pixelMapInput) {
      this.initSlideMap(this.slide, this.pixelMapInput);
    }
  }

  public ngOnDestroy(): void {
    this.removeLayers();
  }

  private initSlideMap(slide: Slide, pixelMapInput: PixelMapInput): void {
    if (!this.map) {
      this.mapIdService.setId(this.mapId);
      this.map = this.mapService.getMap(this.mapId);
    }

    this.setNewSources(slide, pixelMapInput);

    this.map.render();
  }

  private setNewSources(slide: Slide, pixelMapInput: PixelMapInput): void {
    this.mapService.setImageSources(this.mapId, slide.slideId);
    this.removeLayers();

    pixelMapInput.tileRequesters.forEach((tileRequester, i) => {
      const source = this.createXYZSource(slide, pixelMapInput.tileSize, tileRequester);

      const layer = new TileLayer<XYZ>({
        source,
        preload: Infinity,
      });

      layer.setZIndex(i + 1);
      layer.setOpacity(DEFAULT_LAYER_OPACITY);
      this.map.addLayer(layer);
      this.layers.push(layer);
    });
  }

  private createXYZSource(slide: Slide, tileSize: number, tileRequester: PixelMapTileRequesterInterface): XYZ {
    if (!slide.imageInfo.width || !slide.imageInfo.height || !tileSize) {
      throw new Error('Unable to load image size!');
    }

    const minZoom = slide.imageInfo.resolutions.length - slide.imageInfo.numberOfLevels - (slide.resolver.hasArtificialLevel ? 1 : 0);
    const tileGrid = new TileGrid({
      resolutions: slide.imageInfo.resolutions,
      extent: [0, -slide.imageInfo.height, slide.imageInfo.width, 0],
      origin: [0, 0],
      tileSize: tileSize,
      minZoom,
    });

    return new XYZ({
      tileGrid,
      projection: PROJECTION_KEY,
      url: 'DUMMY_URL',
      transition: 0,
      tileLoadFunction: tileRequester.tileLoadFunction.bind(tileRequester),
    });
  }

  private removeLayer(channel: number): void {
    if (this.layers) {
      this.map.removeLayer(this.layers[channel]);
      this.layers.splice(channel, 1);
    }
  }

  private removeLayers(): void {
    if (this.layers) {
      this.layers.forEach(layer => this.map.removeLayer(layer));
      this.layers = [];
    }
  }
}
