import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ComponentFactoryResolver,
  ComponentRef,
  EventEmitter,
  Host, HostListener,
  Input,
  OnDestroy,
  Optional,
  Output,
  Renderer2,
  ViewContainerRef,
} from '@angular/core';
import { Feature, Overlay } from 'ol';
import OlMap from 'ol/Map';
import { Draw } from 'ol/interaction';
import VectorLayer from 'ol/layer/Vector';
import VectorSource from 'ol/source/Vector';

import { MapService } from '../../services/map.service';
import { MapIdService } from '../../services/map-id.service';
import { DrawType, MouseInteractionType } from '../../models/interaction';
import { OpenLayersDrawFactory } from '../../tools/ol-draw-factory';
import { DrawEvent } from 'ol/interaction/Draw';
import { IAnnotationCard, Popup } from '../../models/ui';
import { IAnnotation, AnnotationReferenceType } from '../../models/annotation';
import { CreateAnnotationCardComponent } from '../create-annotation-card/create-annotation-card.component';
import { GeometryToAnnotation } from '../../tools/geometry-to-annotation';
import { ViewerCalculationsService } from '../../services/viewer-calculations.service';
import { ANNOTATION_CREATION_ERROR_MSG, ANNOTATION_INTERSECT_ERROR_MSG, SvmErrorMessage } from '../../models/errors';
import { AxisConverter } from '../../tools/axis-converter';
import { Geometry, Polygon } from 'ol/geom';
import { take } from 'rxjs/operators';
import { AnnotationCharacterLimit } from '../../models/character-limit';
import { FeatureStyleMapping } from '../../models/feature-style';

@Component({
  selector: 'svm-annotation-create',
  templateUrl: './annotation-create.component.html',
  styleUrls: ['./annotation-create.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AnnotationCreateComponent implements AfterViewInit, OnDestroy {
  @Input() mapId: string;
  @Input() slideId: string;
  @Input() autoSetAnnotationTitle: boolean;

  private _drawMode: MouseInteractionType;
  @Input() public set drawMode(val: MouseInteractionType) {
    this._drawMode = val;
    this.setDrawMode(val);
  }
  public get drawMode() {
    return this._drawMode;
  }

  private _featureStyleMapping: FeatureStyleMapping;
  @Input() public set featureStyleMapping(val: FeatureStyleMapping) {
    if (val) {
      this._featureStyleMapping = val;
    }
  }
  public get featureStyleMapping() {
    return this._featureStyleMapping;
  }

  @Input() annotationCharacterConfig: AnnotationCharacterLimit | undefined;

  @Output() public annotationCreated = new EventEmitter<Partial<IAnnotation>>();
  @Output() public errorMessage = new EventEmitter<SvmErrorMessage>();
  @Output() public toggleAlternateDrawMode = new EventEmitter<boolean>();

  private viewerHost: OlMap;
  private drawLeft: Draw; // OlDraw
  private drawRight: Draw; // OlDraw
  private source: VectorSource;
  private layer: VectorLayer<VectorSource>;
  private cardOverlay: Overlay;
  private tempFeature: Feature<Geometry>;

  private currentDrawMode: MouseInteractionType;

  private cardRef: ComponentRef<CreateAnnotationCardComponent>;

  private roiCount = 0;

  constructor(
    private mapService: MapService,
    @Host()
    @Optional()
    private mapIdService: MapIdService,
    private calculations: ViewerCalculationsService,
    private renderer: Renderer2,
    private viewContainerRef: ViewContainerRef,
    private componentFactoryResolver: ComponentFactoryResolver,
    private changeDetectorRef: ChangeDetectorRef,
  ) { }

  ngAfterViewInit(): void {
    this.viewerHost = this.mapService.getMap(this.mapIdService || this.mapId);
    // this.instance = new Draw({});
    this.createLayerSource();
  }

  ngOnDestroy() {
    // set draw mode to undefined, so the openlayers draw mode
    // is set to move when changing slides
    this.setDrawMode(undefined);
  }

  private createLayerSource(): void {
    this.source = new VectorSource({ wrapX: false });
    this.layer = new VectorLayer({
      source: this.source,
    });

    // this.viewerHost = this.mapService.getMap(this.mapIdService || this.mapId);

    this.layer.setVisible(true);
    this.layer.setZIndex(10);

    this.viewerHost.addLayer(this.layer);
  }

  @HostListener('document:keydown', ['$event'])
  onKeyPressed(event: KeyboardEvent): void {
    if (event.code === 'Escape' && this.drawLeft && this.drawLeft.getActive()) {
      this.drawLeft.abortDrawing();
    }

    if (event.code === 'Escape' && this.drawRight && this.drawRight.getActive()) {
      this.drawRight.abortDrawing();
    }
  }

  private setDrawMode(mode: MouseInteractionType): void {
    // clear previous interaction before setting new draw mode
    this.disableDraw();

    this.currentDrawMode = mode;
    if (!mode?.left && !mode?.right) {
      return;
    }

    if (mode.left) {
      const openLayersDrawLeft = OpenLayersDrawFactory.fromDrawType(
        mode.left as DrawType,
        this.source,
        mode
      );
      this.drawLeft = openLayersDrawLeft.getDraw();
      this.viewerHost.addInteraction(this.drawLeft);
    }

    if (mode.right) {
      const openLayersDrawRight = OpenLayersDrawFactory.fromDrawType(
        mode.right as DrawType,
        this.source,
        mode
      );
      this.drawRight = openLayersDrawRight.getDraw();
      this.viewerHost.addInteraction(this.drawRight);
    }

    this.drawLeft.on('drawend', (event: DrawEvent) => this.onDrawEnd(event));
  }

  private disableDraw(): void {

    if (this.viewerHost) {
      this.viewerHost.removeInteraction(this.drawLeft);
    }

    if (this.drawLeft) {
      this.drawLeft.abortDrawing();
      this.drawLeft.setActive(false);
    }

    if (this.drawRight) {
      this.drawRight.abortDrawing();
      this.drawRight.setActive(false);
    }

    if (this.tempFeature && this.source.hasFeature(this.tempFeature)) {
      this.source.removeFeature(this.tempFeature);
    }
    // this.source.clear();
    if (this.cardRef) {
      this.cardRef.destroy();
      this.cardRef = undefined;
    }
  }

  onDrawEnd(event: DrawEvent): void {
    /**
     * user should not be able to start two drawings at once
     * so we remove the interaction until annotation creation
     * is finished(pop up window is closed)
     * */
    this.viewerHost.removeInteraction(this.drawLeft);
    this.viewerHost.removeInteraction(this.drawRight);
    this.tempFeature = event.feature;
    const annotationClasses = Object
      .keys(this.featureStyleMapping)
      .filter(c => c.toLowerCase().endsWith('roi') || c.toLowerCase().endsWith('null'))
      .sort((a, _b) => a.toLowerCase().endsWith('roi') ? -1 : 1);
    const style = this.featureStyleMapping[annotationClasses[0]]?.DEFAULT;
    this.tempFeature.setStyle(style);

    let geometry = event.feature.getGeometry();
    const extent = this.calculations.getImageExtentFromSourceLayers(this.viewerHost.getLayers());

    // rearrange polygon vertices if they're out of bounds
    if (geometry instanceof Polygon && !this.calculations.isGeometryInExtent(geometry, extent)) {
      geometry = this.calculations.rearrangeOutOfBoundsPolygonVertices(geometry, extent);
    }

    if (geometry instanceof Polygon && this.calculations.polygonIsSelfIntersecting(geometry)) {
      this.emitAnnotationErrorMessage(ANNOTATION_INTERSECT_ERROR_MSG);
      return;
    }

    if (!this.calculations.isGeometryInExtent(geometry, extent)) {
      this.emitAnnotationErrorMessage(ANNOTATION_CREATION_ERROR_MSG);
      return;
    }

    // if the both mouse buttons should draw different annotations, it must be differentiated which button drew
    // which kind of DrawType
    const annotation = GeometryToAnnotation.geometryToAnnotation(geometry, this.currentDrawMode.left as DrawType);
    annotation.nppCreated = this.calculations.nppForView(this.viewerHost.getView());
    annotation.nppViewing = this.calculations.nppExtrema(this.viewerHost.getView());
    annotation.centroid = this.calculations.centroidForGeometry(geometry);

    const cardParentElement = this.createAnnotationCard(annotation, event);

    const popUp = new Popup(
      // because the y-axis is already converted, but the coordinate system
      // needs negative y-values
      AxisConverter.invertYAxis(annotation.center),
      cardParentElement
    );
    this.addOverlayPopup(popUp);

    this.changeDetectorRef.markForCheck();
  }

  private emitAnnotationErrorMessage(error: SvmErrorMessage) {
    this.layer.dispose();
    this.createLayerSource();
    this.errorMessage.emit(error);
    this.setDrawMode(this._drawMode);
  }

  private createAnnotationCard(annotation: IAnnotation, event: DrawEvent) {
    const cardParentElement = this.renderer.createElement('div') as HTMLDivElement;

    this.cardRef = this.createCard();

    this.renderer.appendChild(
      cardParentElement,
      this.cardRef.location.nativeElement
    );

    this.cardRef.instance.cancelClick.pipe(take(1)).subscribe(() => {
      this.cardRef.destroy();
    });
    this.cardRef.instance.saveClick.subscribe((value: IAnnotationCard) => {
      this.setAnnotationCredentials(annotation, value.titel, value.description);
      this.cardRef.destroy();
    });
    this.cardRef.onDestroy(() => {
      // destroys the overlay(popup) of this card
      this.viewerHost.removeOverlay(this.cardOverlay);
      // let user use same draw type again after closing pop up
      this.viewerHost.addInteraction(this.drawLeft);
      // remove feature from draw layer as it should be in annotation layer after saving
      if (this.source.hasFeature(event.feature)) {
        this.source.removeFeature(event.feature);
      }
    });
    return cardParentElement;
  }

  private createCard(): ComponentRef<CreateAnnotationCardComponent> {
    const componentFactory = this.componentFactoryResolver.resolveComponentFactory(
      CreateAnnotationCardComponent
    );
    const componentRef = this.viewContainerRef.createComponent(
      componentFactory
    );
    componentRef.instance.annotationCharacterConfig = this.annotationCharacterConfig;
    if (this.autoSetAnnotationTitle) {
      componentRef.instance.defaultTitle = 'ROI_' + this.roiCount++;
    }

    return componentRef;
  }

  public addOverlayPopup(popup: Popup) {
    // const popupAlreadyExists = this.overlays.has(popup.getId());
    if (this.cardOverlay) {
      this.viewerHost.removeOverlay(this.cardOverlay);
    }

    this.cardOverlay = new Overlay({
      element: popup.getElement(),
      autoPan: {
        animation: {
          duration: 250,
        }
      },
      position: popup.getCoordinates(),
      positioning: 'center-center'
    });

    this.viewerHost.addOverlay(this.cardOverlay);
    // this.overlays.set(popup.getId(), overlay);
  }

  private setAnnotationCredentials(annotation: IAnnotation, title: string, description: string): void {
    annotation.name = title;
    annotation.description = description;
    annotation.referenceId = this.slideId;
    annotation.referenceType = AnnotationReferenceType.Wsi;
    this.annotationCreated.emit(annotation);
    this.tempFeature = undefined;
  }
}
