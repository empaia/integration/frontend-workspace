import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { DEFAULT_LAYER_OPACITY, OpacityChange, PixelMapInput } from '../../models/pixelmap';
import { MatSliderDragEvent } from '@angular/material/slider';

@Component({
  selector: 'svm-pixel-map-opacity-menu',
  templateUrl: './pixel-map-opacity-menu.component.html',
  styleUrls: ['./pixel-map-opacity-menu.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PixelMapOpacityMenuComponent {
  @Input() public pixelMapInput!: PixelMapInput;

  @Output() public channelOpacityChanged = new EventEmitter<OpacityChange>();

  public readonly DEFAULT_OPACITY = DEFAULT_LAYER_OPACITY;

  public onLayerChannelOpacityChanged(channel: number, event: MatSliderDragEvent): void {
    this.channelOpacityChanged.emit({
      channel,
      opacity: event.value,
    });
  }
}
