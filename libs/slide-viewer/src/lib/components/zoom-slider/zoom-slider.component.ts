import {
  AfterViewInit,
  Component,
  ElementRef,
  Input,
} from '@angular/core';
import { Zoom, ZoomSlider } from 'ol/control';
import OlMap from 'ol/Map';
import { MapService } from '../../services/map.service';
import { MapIdService } from '../../services/map-id.service';

@Component({
  selector: 'svm-zoom-slider',
  templateUrl: './zoom-slider.component.html',
  styleUrls: ['./zoom-slider.component.scss'],
})
export class ZoomSliderComponent implements AfterViewInit {
  @Input() mapId: string;
  @Input() duration = 500;

  private viewerHost: OlMap;
  private slider: ZoomSlider;
  private buttons: Zoom;

  constructor(
    private mapService: MapService,
    // @Host()
    // @Optional()
    private mapIdService: MapIdService,
    // @Optional()
    // private viewerComponent: ViewerComponent,
    private elementRef: ElementRef
  ) {}

  ngAfterViewInit(): void {
    this.initZoomSlider();
  }

  initZoomSlider(): void {
    this.viewerHost = this.mapService.getMap(this.mapIdService || this.mapId);

    const target = this.elementRef.nativeElement.parentElement
      ? this.elementRef.nativeElement
      : null;

    this.slider = new ZoomSlider({
      duration: this.duration,
    });
    // this.slider.setTarget(this.viewerHost.getTarget());
    this.slider.setTarget(target);
    this.viewerHost.addControl(this.slider);

    this.buttons = new Zoom({
      // target: this.viewerHost.getTarget(),
      // target: target,
      duration: this.duration,
    });
    this.buttons.setTarget(target);
    this.viewerHost.addControl(this.buttons);
  }
}
