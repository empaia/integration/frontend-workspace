import {
  AfterViewInit,
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Host,
  Input,
  OnDestroy,
  Output,
} from '@angular/core';
import { Feature, Map as OlMap, MapBrowserEvent } from 'ol';
import { FeatureLike } from 'ol/Feature';
import VectorSource from 'ol/source/Vector';
import VectorImageLayer from 'ol/layer/VectorImage';
import { MapService } from '../../services/map.service';
import { MapIdService } from '../../services/map-id.service';
import { IAnnotation, AnnotationsWithUpdates } from '../../models/annotation';
import { logger } from '../../tools/logger';
import { AnnotationToOlFeature } from '../../tools/annotation-to-olfeature';
import { ViewerCalculationsService } from '../../services/viewer-calculations.service';
import { ANNOTATION_FOCUS_ERROR_MSG, SvmErrorMessage } from '../../models/errors';
import { Geometry } from 'ol/geom';
import { FeatureStyleEntry, FeatureStyleMapping } from '../../models/feature-style';
import { AnnotationHighlightConfig, DEFAULT_ANNOTATION_HIGHLIGHT_CONFIG, HighlightType } from '../../config/annotation-highlight.config';
import { Subject } from 'rxjs';
import { Style } from 'ol/style';

/**
 * Layer that displays annotations on a Slide.
 * Reacts to hide/show/hover events of Annotation List.
 * Handles Zooming to an annotation
 */
@Component({
  selector: 'svm-annotation-layer',
  templateUrl: './annotation-layer.component.html',
  styleUrls: ['./annotation-layer.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AnnotationLayerComponent implements AfterViewInit, OnDestroy {
  private viewerHost: OlMap;
  private source: VectorSource;
  private layer: VectorImageLayer<VectorSource>;

  // viewer hover (internal)
  private _hoveredFeaturesList = new Array<Feature<Geometry>>();
  // viewer selection (internal)
  private _selectedFeaturesList = new Array<Feature<Geometry>>();
  // external highlight (sidenav list)
  private _highlightFeaturesList = new Array<Feature<Geometry>>();

  private destroy$ = new Subject();

  // set parameter of EventEmitter to true to trigger the change detection
  // and avoiding ExpressionChangedAfterItHasBeenCheckedError
  @Output() public errorMessage = new EventEmitter<SvmErrorMessage>(true);

  @Input() public set addAnnotations(val: AnnotationsWithUpdates) {
    if (val?.annotations?.length) {
      this.addNewAnnotations(val.annotations, val.updatedAnnotationIds);
    }
  }

  @Input() public set removeAnnotations(val: string[]) {
    if (val?.length) {
      this.removeFeatures(val);
    }
  }

  @Input() public set clearAnnotations(val: boolean) {
    if (val) {
      this.removeAllFeatures();
    }
  }

  @Input() public set hideAnnotations(val: string[]) {
    if (val?.length) {
      this.removeFeatures(val);
    }
  }

  private _featureStyleMapping: FeatureStyleMapping;
  @Input() public set featureStyleMapping(val: FeatureStyleMapping) {
    if (val) {
      this._featureStyleMapping = val;
      if (this.layer) {
        this.onFeatureStyleChange();
      }
    }
  }
  public get featureStyleMapping() {
    return this._featureStyleMapping;
  }

  private _highlightedFeatures: string[];
  @Input() public set highlightedFeatures(val: string[]) {
    if (val) {
      this.highlightFeatures(val);
    } else {
      this.resetStyleOfClickedFeatures();
      this.resetStyleOfHoverFeatures();
    }
  }
  public get highlightedFeatures(): string[] {
    return this._highlightedFeatures;
  }

  private _focusFeature: string;
  @Input() public set focusFeature(val: string) {
    if (!val) {
      return;
    }
    this.zoomToAnnotation(val);
  }
  public get focusFeature(): string {
    return this._focusFeature;
  }

  @Input() public highlightConfig: AnnotationHighlightConfig = DEFAULT_ANNOTATION_HIGHLIGHT_CONFIG;

  @Output() public readonly hoveredFeatures = new EventEmitter<string[]>();
  @Output() public readonly clickedFeature = new EventEmitter<string[]>();

  constructor(
    private mapService: MapService,
    @Host()
    private mapIdService: MapIdService,
    private viewerCalculationService: ViewerCalculationsService,
  ) { }


  public ngAfterViewInit(): void {
    const id = this.mapIdService;
    this.viewerHost = this.mapService.getMap(id);
    this.initializeLayer();

    this.initializeEventListener();
  }

  public ngOnDestroy(): void {
    this.removeAllFeatures();
    this.destroy$.next(null);
  }

  private initializeLayer() {
    this.source = new VectorSource({
      useSpatialIndex: false,
      wrapX: false,
    });

    const styleFeature = (feature: FeatureLike): void | Style | Style[] => {
      let style: FeatureStyleEntry;
      const annotationClass = feature.get('annotation')?.['classes']?.[0]?.['value'];
      if (annotationClass) {
        style = this.featureStyleMapping[annotationClass].DEFAULT;
        style = [style[0].clone(), style[1].clone()];
      }
      return style;
    };

    this.layer = new VectorImageLayer({
      renderBuffer: 10,
      source: this.source,
      style: styleFeature.bind(this),
    });

    this.layer.setZIndex(101);
    this.viewerHost.addLayer(this.layer);
  }

  private initializeEventListener() {
    switch(this.highlightConfig.highlightType) {
      case HighlightType.CLICK:
        this.setHighlightOnClickEventListener();
        break;
      case HighlightType.OVER:
        this.setHighlightOnHoverEventListener();
        break;
      default:
        this.setHighlightOnHoverEventListener();
    }
  }

  private setHighlightOnHoverEventListener(): void {
    let throttleFlag = true;
    this.viewerHost.on('pointermove', (event: MapBrowserEvent<UIEvent>) => {
      if(!event.dragging) {
        // we trigger hover function every 50ms to reduce computational expensive operations
        if(throttleFlag) {
          this.onFeatureHover(event);
          throttleFlag = false;
          setTimeout(() => throttleFlag = true, 50);
        }
      }
      return true;
    });
    this.viewerHost.on('singleclick', (event: MapBrowserEvent<UIEvent>) => {
      this.onFeatureClick(event);
      return true;
    });
  }

  private setHighlightOnClickEventListener(): void {
    this.viewerHost.on('singleclick', (event: MapBrowserEvent<UIEvent>) => {
      this.onFeatureClick(event);
      return true;
    });
  }

  public addNewAnnotations(annotationList: Array<IAnnotation>, updatedAnnotationIds: string[]): void {
    if (!this.source) {
      return;
    }

    // only remove annotations we want to update
    this.removeFeatures(updatedAnnotationIds);
    const features = this.transformAnnotationsToFeatures(annotationList);
    this.source.addFeatures(features);
  }

  public transformAnnotationsToFeatures(annotations: IAnnotation[]): Feature<Geometry>[] {
    return annotations.map(annotation => {
      const feature = AnnotationToOlFeature.annotationToFeature(annotation);
      AnnotationToOlFeature.addAnnotationInfoToFeature(annotation, feature);
      return feature;
    });
  }

  public highlightFeatures(annotationIds: string[]) {
    switch (this.highlightConfig.highlightType) {
      case HighlightType.CLICK:
        this.resetStyleOfClickedFeatures();
        this.highlightClickAnnotation(annotationIds[0]);
        break;
      case HighlightType.OVER:
        this.resetStyleOfHoverFeatures();
        this.highlightHoverAnnotations(annotationIds);
        break;
      default:
        this.resetStyleOfHoverFeatures();
        this.highlightHoverAnnotations(annotationIds);
    }
  }

  private highlightHoverAnnotations(ids: string[]): void {
    ids.forEach((id) => {
      if (id && this.source) {
        const feature = this.source.getFeatureById(id) as Feature<Geometry>;
        if (feature) {
          this._hoveredFeaturesList.push(feature);
          const annotationClass = feature.get('annotation')?.['classes']?.[0]?.['value'];
          let style = this.featureStyleMapping[annotationClass]?.HOVER;
          if (style) {
            style = [style[0].clone(), style[1].clone()];
          }
          feature.setStyle(style);
        }
      }
    });
  }

  public removeAllFeatures(): void {
    if (!this.source) {
      return;
    }
    this.source.clear(true);
  }

  public removeFeatures(annotationIds: string[]): void {
    annotationIds.forEach(annotationId => {
      const feature = this.source.getFeatureById(annotationId) as Feature<Geometry>;
      if (feature) {
        this.source.removeFeature(feature);
      }
    });
  }

  private zoomToAnnotation(annotationId: string): void {
    if (annotationId) {
      const feat = this.source.getFeatureById(annotationId) as Feature<Geometry>;
      // if the feature is found zoom to it
      if (feat) {
        const featGeometry = feat.getGeometry();
        const imageExtent = this.viewerCalculationService.getImageExtentFromSourceLayers(this.viewerHost.getLayers());

        if (this.viewerCalculationService.isGeometryInExtent(featGeometry, imageExtent)) {
          const view = this.viewerHost.getView();
          view.fit(featGeometry.getExtent());
        } else {
          // annotation is outside the image boundaries
          this.errorMessage.emit(ANNOTATION_FOCUS_ERROR_MSG);
        }
      } else {
        logger.error(
          '[LayerComponent] Focused annotation not Found! id:',
          annotationId
        );
      }
    }
  }

  private onFeatureStyleChange(): void {
    this.layer.changed();
  }

  private onFeatureHover(event: MapBrowserEvent<UIEvent>): void {
    const prev = new Set<string>(
      this._hoveredFeaturesList.map((f) => f.getId() as string)
    );
    const curr = new Set<string>();

    try {
      this.viewerHost.forEachFeatureAtPixel(event.pixel, (feat: FeatureLike) => {
        // the point indicating that we are in draw mode does not have a id set
        // every other annotation should have an id set via addAnnotationInfoToFeature function
        const id = feat.getId() as string;
        if (id) {
          curr.add(id);
        }
      });
    } catch (ignoreError) {
      // the hover method has no possibility to check whether a feature was discarded
      // or is still on the map. if the feature does not exist, it will throw a
      // type error that will be ignored.
    }

    const same = this.sameHighlightSets(prev, curr);
    if (same) {
      return;
    }

    this.resetStyleOfHoverFeatures();
    this.highlightHoverAnnotations(Array.from(curr));

    // notify sidenav annotation list about hovered annotation
    this.hoveredFeatures.emit(curr.size > 0 ? [...curr] : []);
  }

  private sameHighlightSets(prev: Set<string>, current: Set<string>): boolean {
    if (prev.size !== current.size) {
      return false;
    }
    for (const a of prev) {
      if (!current.has(a)) {
        return false;
      }
    }
    return true;
  }

  private onFeatureClick(event: MapBrowserEvent<UIEvent>): void {
    this.resetStyleOfClickedFeatures();
    // only add features to annotation with an id. Otherwise it's a cluster
    this._selectedFeaturesList = this.viewerHost.getFeaturesAtPixel(event.pixel).filter(f => f.getId()) as Feature<Geometry>[];
    const annotationId = this._selectedFeaturesList[0]?.getId() as string;
    this.highlightClickAnnotation(annotationId);
    const annotations = new Set<string>(
      this._selectedFeaturesList.map(f => f.getId() as string)
    );
    this.clickedFeature.emit(annotations.size ? [...annotations] : []);
  }

  private highlightClickAnnotation(id: string): void {
    if (id && this.source) {
      const feature = this.source.getFeatureById(id) as Feature<Geometry>;
      const annotationClass = feature?.get('annotation')?.['classes']?.[0]?.['value'];
      let style = this.featureStyleMapping[annotationClass]?.SELECTION;
      if (style) {
        style = [style[0].clone(), style[1].clone()];
      }
      feature?.setStyle(style);
      this._selectedFeaturesList.push(feature);
    }
  }

  private resetStyleOfHoverFeatures(): void {
    this._hoveredFeaturesList.forEach(f => f?.setStyle());
    this._hoveredFeaturesList = [];
  }

  private resetStyleOfClickedFeatures(): void {
    this._selectedFeaturesList.forEach(f => f?.setStyle());
    this._selectedFeaturesList = [];
  }
}
