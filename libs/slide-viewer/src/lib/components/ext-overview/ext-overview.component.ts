import {
  AfterViewInit,
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  EventEmitter,
  Host,
  Input,
  OnDestroy,
  OnInit,
  Optional,
  Output,
} from '@angular/core';

import Feature from 'ol/Feature';
import { fromExtent } from 'ol/geom/Polygon';
import { getCenter, containsCoordinate, Extent } from 'ol/extent';
import { View, Map, MapBrowserEvent } from 'ol';
import { XYZ } from 'ol/source';
import { Style, Stroke, Fill } from 'ol/style';
import TileLayer from 'ol/layer/Tile';
import VectorLayer from 'ol/layer/Vector';
import VectorSource from 'ol/source/Vector';

import {
  ElementSize,
  ResizableElementService
} from '../../services/resizable-element.service';
import { Slide } from '../../models/slide';
import { ViewObserver, ZoomView } from '../../models/ui';
import { MapService } from '../../services/map.service';
import { MapIdService } from '../../services/map-id.service';
import { PROJECTION_KEY, ZOOMBOX_ID } from '../../models/types';
import { SlideSourceLoader } from '../../services/slide-source';

import { filter } from 'rxjs/operators';
import { Observable, Subscription } from 'rxjs';
import { Geometry } from 'ol/geom';

@Component({
  selector: 'svm-ext-overview',
  templateUrl: './ext-overview.component.html',
  styleUrls: ['./ext-overview.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ExtOverviewComponent implements OnInit, OnDestroy, AfterViewInit {

  @Input() mapId: string;

  private _slide: Slide;
  @Input() public get slide() {
    return this._slide;
  }

  public set slide(val: Slide) {
    this._slide = val;
    this.initializeSource(val);
  }

  private _zoomView: ZoomView;
  @Input() public get zoomView() {
    return this._zoomView;
  }

  public set zoomView(val: ZoomView) {
    if (val) {
      this.updateBoundingBox(val);
    }
    this._zoomView = val;
  }

  @Output() public readonly zoomViewUpdated = new EventEmitter<ZoomView>();

  private viewerHost: Map;
  private overviewMap: Map;
  private source: XYZ;

  private elementSize$: Observable<ElementSize>;
  private elementSizeSub$: Subscription;

  private layer: TileLayer<XYZ>;
  // rectangle indicating current viewport
  private zoomBoxFeature: Feature<Geometry>;
  // vector layer to draw viewport indicator on
  private vectorLayer: VectorLayer<VectorSource>;

  private readonly viewPadding = 15;

  constructor(
    private mapService: MapService,
    @Host()
    @Optional()
    private mapIdService: MapIdService,
    private elementRef: ElementRef,
    @Host()
    @Optional()
    private sizeService: ResizableElementService,
  ) {
    this.elementSize$ = this.sizeService ? this.sizeService.style : null;
  }

  ngOnInit(): void {
    const target = this.elementRef.nativeElement.parentElement ? this.elementRef.nativeElement.firstElementChild : null;

    this.overviewMap = new Map({
      controls: [],
      target: target,
    });

    this.overviewMap.getInteractions().forEach(i => {
      i.setActive(false);
    });

    this.elementSizeSub$ = this.elementSize$?.pipe(
      filter(p => p !== undefined),
      filter(() => !!this.overviewMap),
    ).subscribe((s) => {
      this.updateSize(s.width, s.height);
    });
  }

  ngAfterViewInit(): void {
    const id = this.mapIdService || this.mapId;
    this.viewerHost = this.mapService.getMap(id);

    // initialize on remount as setter runs before map layer can initialize
    this.initializeSource(this.slide);
  }

  ngOnDestroy(): void {
    this.clearOverview();

    if(this.elementSizeSub$) {
      this.elementSizeSub$.unsubscribe();
    }
  }

  initializeSource(slide: Slide): void {
    if (slide) {
      this.source = SlideSourceLoader.createXYZSource(slide);
      this.createInstanceFromSource(this.source);
    } else {
      this.source = undefined;
      if (this.overviewMap) {
        this.clearOverview();
      }
    }
  }

  private updateBoundingBox(zoomView: ZoomView) {
    if (this.vectorLayer !== undefined && zoomView.receivers.includes(ViewObserver.OverviewMap)) {
      this.zoomBoxFeature?.setGeometry(fromExtent(zoomView.extent));
    }
  }

  private updateSize(width: number, height: number) {
    if (this.overviewMap) {
      this.overviewMap.getTargetElement().style.height
        = `${height - this.viewPadding}px`;
      this.overviewMap.getTargetElement().style.width
        = `${width - this.viewPadding}px`;

      this.overviewMap.setSize([width, height]);
      this.overviewMap.updateSize();
    }
  }

  private clearOverview(): void {
    this.overviewMap.getControls().forEach(control => {
      this.overviewMap.removeControl(control);
    });
    // we need to make a copy here to prevent reindexing of map layers
    const layers = [...this.overviewMap.getLayers().getArray()];
    layers.forEach((layer) => this.overviewMap.removeLayer(layer));
  }

  private createInstanceFromSource(source: XYZ) {
    if (!this.viewerHost) { return; }

    // view
    this.clearOverview();
    const extent = source.getTileGrid().getExtent();

    this.overviewMap.setView(new View({
      center: getCenter(extent),
      extent: extent,
      projection: PROJECTION_KEY,
      enableRotation: false,
      constrainResolution: true,
      showFullExtent: true,
      resolution: source.getTileGrid().getResolution(1),
    }));

    // events
    this.overviewMap.on('pointerdrag', (event: MapBrowserEvent<UIEvent>) => this.dragZoomBox(event));
    this.overviewMap.on('singleclick', (event: MapBrowserEvent<UIEvent>) => this.dragZoomBox(event));

    // create tile and vector layer
    this.layer = new TileLayer({
      source,
      preload: Infinity,
    });
    this.layer.getSource().refresh();

    const darkStroke = new Style({
      fill: new Fill({
        color: [255, 255, 255, 0]
      }),
      stroke: new Stroke({
        color: [255, 0, 0, 0.9],
        width: 1,
        //lineDash: [2,4]
      }),
    });
    this.vectorLayer = new VectorLayer({
      source: new VectorSource(),
      style: [darkStroke],
    });
    this.vectorLayer.getSource().refresh();

    this.vectorLayer.setVisible(true);
    this.overviewMap.addLayer(this.layer);
    this.overviewMap.addLayer(this.vectorLayer);

    // create and add zoombox to vector layer
    this.zoomBoxFeature = new Feature({ id: ZOOMBOX_ID, geometry: fromExtent(extent) });
    this.zoomBoxFeature.setId(ZOOMBOX_ID);
    this.vectorLayer.getSource().addFeature(this.zoomBoxFeature);

    this.overviewMap.getView().fit(extent, { size: this.overviewMap.getSize() });
    setTimeout(() => this.overviewMap.updateSize(), 200);
  }

  private dragZoomBox(event: MapBrowserEvent<UIEvent>): void {
    const featureExtent = this.zoomBoxFeature?.getGeometry().getExtent();
    if (featureExtent !== undefined) {
      const featureCenter = getCenter(featureExtent);
      const extent = this.source.getTileGrid().getExtent();
      if (containsCoordinate(extent, event.coordinate)) {
        // we are inside the extent so we have to adjust x an y
        const deltaX = event.coordinate[0] - featureCenter[0];
        const deltaY = event.coordinate[1] - featureCenter[1];
        this.zoomBoxFeature?.getGeometry().translate(deltaX, deltaY);
      } else {
        // cursor ist outside the extent
        let deltaX = 0, deltaY = 0;
        if (event.coordinate[0] < extent[0] || event.coordinate[0] > extent[2]) {
          // we left the x boundaries
          deltaY = event.coordinate[1] - featureCenter[1];
        }
        if (event.coordinate[1] < extent[1] || event.coordinate[1] > extent[3]) {
          // we left the y boundaries
          deltaX = event.coordinate[0] - featureCenter[0];
        }
        if (deltaX != 0 && deltaY != 0) {
          // we need to set it back to zero, if we are out of bounds in x and y direction
          deltaX = 0;
          deltaY = 0;
        }
        this.zoomBoxFeature?.getGeometry().translate(deltaX, deltaY);
      }
      this.emitZoomView(featureExtent);
    }
  }

  public onMouseWheelEvent(event: WheelEvent) {
    const featureExtent = this.zoomBoxFeature?.getGeometry()?.getExtent();

    if (!featureExtent) { return; } //abort zoom if no slide loaded

    if (event.deltaY < 0) {
      // we do not want to go beyond layer zero; 1024px in each direction seems o be a good choice here
      if ((Math.abs(featureExtent[2] - featureExtent[0]) > 1024
        && Math.abs(featureExtent[3] - featureExtent[1]) > 1024)) {
        // zoom in by 25%
        this.zoomBoxFeature?.getGeometry().scale(0.75);
        this.emitZoomView(featureExtent);
      }
    } else {
      // zoom out by 25%
      this.zoomBoxFeature?.getGeometry().scale(1.25);
      this.emitZoomView(featureExtent);
    }
  }

  private emitZoomView(featureExtent: Extent) {

    this.viewerHost.getView().fit(featureExtent);
    this.viewerHost.updateSize();

    // this.zoomViewUpdated.emit({
    //   extent: [
    //     featureExtent[0],
    //     featureExtent[1],
    //     featureExtent[2],
    //     featureExtent[3]
    //   ],
    //   resolution: this.overviewMap.getView().getResolution(),
    //   sender: ViewObserver.OverviewMap,
    //   receivers: [ViewObserver.MainMap],
    // });
  }
}
