import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MeasurementToolComponent } from './measurement-tool.component';

describe('MeasurementToolComponent', () => {
  let component: MeasurementToolComponent;
  let fixture: ComponentFixture<MeasurementToolComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [MeasurementToolComponent],
      teardown: { destroyAfterEach: false },
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MeasurementToolComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
