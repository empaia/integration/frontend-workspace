import {
  AfterViewInit,
  ChangeDetectionStrategy,
  Component,
  Host,
  Input,
  OnDestroy
} from '@angular/core';
import { Feature, Map } from 'ol';
import VectorSource from 'ol/source/Vector';
import VectorLayer from 'ol/layer/Vector';
import { Cluster } from 'ol/source';
import { MapService } from '../../services/map.service';
import { MapIdService } from '../../services/map-id.service';
import { ClusterStyleService } from '../../services/cluster-style.service';
import { Geometry, Point } from 'ol/geom';
import { AxisConverter } from '../../tools/axis-converter';

@Component({
  selector: 'svm-annotation-cluster-layer',
  templateUrl: './annotation-cluster-layer.component.html',
  styleUrls: ['./annotation-cluster-layer.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AnnotationClusterLayerComponent implements AfterViewInit, OnDestroy {
  private viewerHost: Map;
  private source: VectorSource;
  private layer: VectorLayer<VectorSource>;
  private cluster: Cluster;
  private _annotationCentroids: number[][];
  private _clusterDistance: number;

  @Input() set clusterDistance(val: number) {
    if (!val) {
      return;
    }
    this._clusterDistance = val;
    this.setClusterDistance(val);
  }
  @Input() public get annotationCentroids(): number[][] {
    return this._annotationCentroids;
  }
  public set annotationCentroids(val: number[][]) {
    if (!val || val.length <= 0) {
      this.removeAllFeatures();
      return;
    }
    this.removeAllFeatures();
    this._annotationCentroids = val;
    this.addClusters(val);
  }

  constructor(
    private mapService: MapService,
    @Host()
    private mapIdService: MapIdService,
    private clusterStyleService: ClusterStyleService,
  ) { }

  ngAfterViewInit(): void {
    const id = this.mapIdService;
    this.viewerHost = this.mapService.getMap(id);
    this.initialiseLayer();
  }

  ngOnDestroy(): void {
    this.removeAllFeatures();
  }

  private initialiseLayer(): void {
    this.source = new VectorSource({
      wrapX: false
    });
    this.cluster = new Cluster({
      distance: this._clusterDistance,
      source: this.source,
    });

    this.layer = new VectorLayer({
      source: this.cluster,
      style: (feature) => {
        const size = feature.get('features').length;
        const style = this.clusterStyleService.getClusterStyle(size);
        if (!style) {
          this.clusterStyleService.setClusterStyle(size);
        }
        return this.clusterStyleService.getClusterStyle(size);
      }
    });
    this.layer.setZIndex(101);
    this.viewerHost.addLayer(this.layer);
  }

  private addClusters(clusters: number[][]): void {
    if (!this.source) {
      return;
    }
    const features = clusters.map(this.convertCoordinatesToFeature);
    this.source.addFeatures(features);
  }

  private removeAllFeatures(): void {
    if (!this.source) {
      return;
    }
    this.source.clear(true);
  }

  private setClusterDistance(distance: number): void {
    if (!this.cluster) {
      return;
    }
    this.cluster.setDistance(distance);
  }

  private convertCoordinatesToFeature(coordinates: number[]): Feature<Geometry> {
    return new Feature(new Point(AxisConverter.invertYAxis(coordinates)));
  }
}
