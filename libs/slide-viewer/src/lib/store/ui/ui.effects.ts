
import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';

import { map } from 'rxjs/operators';
import { focusAnnotation } from '../annotations/annotation.actions';

import * as UiActions from './ui.actions';
import { SlideActions } from '../slide';
import { InteractionType } from '../../models/interaction';

@Injectable()
export class UiEffects {
  loadUis$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(UiActions.viewportChanged),
      // invalidate focused annotation if viewport is moved
      map(() => focusAnnotation({ id: undefined }))
    );
  });

  // reset interaction mode to move when slide was added
  resetInteraction$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(SlideActions.selectedSlideChanged),
      map(() => UiActions.setUiToolMode({
        toolMode: {
          left: InteractionType.Move
        }
      })),
    );
  });

  constructor(private actions$: Actions) { }
}
