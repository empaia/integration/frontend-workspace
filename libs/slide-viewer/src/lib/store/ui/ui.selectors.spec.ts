import { selectAnnotationBarVisibility } from './ui.selectors';
import { initialState } from './ui.reducer';

describe('Ui Selectors', () => {
  describe('AnnotationBar Selector', () => {
    it('should select the annotationBa to be true', () => {
      const result = selectAnnotationBarVisibility.projector(initialState);
      expect(result).toBe(true);
    });
  });
});
