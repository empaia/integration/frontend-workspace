import { createAction, props } from '@ngrx/store';
import { MouseInteractionType, ToolbarInteractionType } from '../../models/interaction';
import { CurrentView, ZoomView } from '../../models/ui';
import { UiConfig } from '../../models/viewer-config';
import { AnnotationHighlightConfig } from '../../config/annotation-highlight.config';

export const setUiToolMode = createAction(
  '[SVM Ui] Set Tool Mode',
  props<{ toolMode: MouseInteractionType }>()
);

export const viewportChanged = createAction(
  '[SVM Ui] Viewport Changed',
  props<{ view: CurrentView }>()
);

export const zoomviewChanged = createAction(
  '[SVM Ui] Zoomview Changed',
  props<{ zoomview: ZoomView }>()
);

export const setUIConfig = createAction(
  '[SVM Ui] Set Ui Config',
  props<{ uiConfig: UiConfig }>()
);

export const setAnnotationBarVisibility = createAction(
  '[SVM Ui] Set AnnotationBar Visibility',
  props<{ showAnnotationBar: boolean }>()
);

export const setAutoAnnotationTitle = createAction(
  '[SVM Ui] Set Auto Annotation Title',
  props<{ autoSetAnnotationTitle: boolean }>()
);

export const setRenderHideNavigationButton = createAction(
  '[SVM Ui] Set Render Hide Navigation Button',
  props<{ renderHideNavigationButton: boolean }>()
);

export const setUpdateViewer = createAction(
  '[SVM Ui] Update Viewer',
  props<{ update: number }>()
);

export const setAllowedInteractionTypes = createAction(
  '[SVM Ui] Set Allowed Draw Types',
  props<{ allowedTypes: ToolbarInteractionType[] | undefined }>()
);

export const setExaminationState = createAction(
  '[SVM Ui] Set Examination State',
  props<{ examinationClosed: boolean }>()
);

export const setAnnotationHighlightConfig = createAction(
  '[SVM Ui] Set Annotation Hover Config',
  props<{ annotationHighlightConfig: AnnotationHighlightConfig }>()
);

export const setAnnotationStrokeWidth = createAction(
  '[SVM Ui] Set Annotation Stroke Width',
  props<{ annotationStrokeWidth: number }>(),
);
