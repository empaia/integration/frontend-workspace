import { reducer, initialState } from './ui.reducer';
import { setAnnotationBarVisibility } from './ui.actions';

describe('Ui Reducer', () => {
  describe('an unknown action', () => {
    it('should return the previous state', () => {
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      const action = {} as any;
      const result = reducer(initialState, action);
      expect(result).toBe(initialState);
    });

    it('should return the default state', () => {
      const action = { type: 'Unknown' };
      const state = reducer(initialState, action);
      expect(state).toBe(initialState);
    });
  });

  describe('an annotationbar action', () => {
    it('should return false for showAnnotationBar state', () => {
      const action = setAnnotationBarVisibility({ showAnnotationBar: false });
      const state = reducer(initialState, action);
      expect(state).toMatchObject({
        ...initialState,
        config: {
          showAnnotationBar: false,
        },
      });
    });

    it('should return true for showAnnotationBar state', () => {
      const action = setAnnotationBarVisibility({ showAnnotationBar: true });
      const state = reducer(initialState, action);
      expect(state).not.toBe(initialState);
      expect(state).toMatchObject(initialState);
    });

    it('should return null for an null parameter in showAnnotationBar', () => {
      const action = setAnnotationBarVisibility({ showAnnotationBar: null });
      const state = reducer(initialState, action);
      expect(state).not.toBe(initialState);
      expect(state).toMatchObject({
        ...initialState,
        config: {
          showAnnotationBar: null,
        },
      });
    });

    it('should return undefined for an undefined parameter in showAnnotationBar', () => {
      const action = setAnnotationBarVisibility({
        showAnnotationBar: undefined,
      });
      const state = reducer(initialState, action);
      expect(state).not.toBe(initialState);
      expect(state).toMatchObject({
        ...initialState,
        config: {
          showAnnotationBar: undefined,
        },
      });
    });
  });

  it.todo('Implement tests for current view in Ui Reducer');
});
