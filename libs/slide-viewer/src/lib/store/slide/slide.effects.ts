import { Injectable } from '@angular/core';
import { Actions, concatLatestFrom, createEffect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import * as SlideActions from './slide.actions';
import * as SlideSelectors from './slide.selectors';
import { filter, map, switchMap } from 'rxjs/operators';

@Injectable()
export class SlideEffects {

  // only set an new slide when the selected slide id
  // differs from the one that is in memory
  addSlide$ = createEffect(() => {
    return this.action$.pipe(
      ofType(SlideActions.addSlide),
      map(action => action.slide),
      filter(slide => !!slide),
      concatLatestFrom(() => [
        this.store.select(SlideSelectors.selectSlide),
        this.store.select(SlideSelectors.selectSlideId),
      ]),
      filter(([slide, selectedSlide, selectedId]) =>
        !selectedId
        || selectedSlide?.slideId !== selectedId
        || selectedId !== slide.slideId
      ),
      switchMap(([slide]) => [
        SlideActions.selectedSlideChanged({ slide }),
        SlideActions.addSlideReady({ slide })
      ])
    );
  });

  // clear current slide when the selected slide id is undefined
  clearSlide$ = createEffect(() => {
    return this.action$.pipe(
      ofType(SlideActions.selectSlideId),
      map(action => action.slideId),
      filter(slideId => !slideId),
      map(() => SlideActions.addSlideReady({ slide: undefined }))
    );
  });

  constructor(
    private readonly action$: Actions,
    private readonly store: Store,
  ) {}
}
