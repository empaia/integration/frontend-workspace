import { createSelector } from '@ngrx/store';
import { selectModuleFeatureState, ModuleState } from '../module.state';
import * as fromSlide from './slide.reducer';

export const selectSlideFeatureState = createSelector(
  selectModuleFeatureState,
  (state: ModuleState) => state.svmSlide
);

export const selectSlide = createSelector(
  selectSlideFeatureState,
  (state: fromSlide.State) => {
    return state?.slide;
  }
);

export const selectSlideSourceReady = createSelector(
  selectSlideFeatureState,
  (state: fromSlide.State) => state.loaded
);

export const selectSlideId = createSelector(
  selectSlideFeatureState,
  (state) => state.slideId
);
