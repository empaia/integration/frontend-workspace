import { createAction, props } from '@ngrx/store';
import { Slide } from '../../models/slide';

export const addSlide = createAction(
  '[SVM Slide] Add Slide',
  props<{ slide: Slide }>()
);

export const addSlideReady = createAction(
  '[SVM Slide] Add Slide Ready',
  props<{ slide: Slide }>()
);

export const selectSlideId = createAction(
  '[SVM Slide] Select Slide Id',
  props<{ slideId: string }>()
);

export const selectedSlideChanged = createAction(
  '[SVM Slide] Selected Slide Changed',
  props<{ slide: Slide }>()
);
