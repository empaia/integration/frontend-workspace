import * as SlideActions from './slide.actions';
import * as SlideSelectors from './slide.selectors';
import * as SlideFeature from './slide.reducer';
export * from './slide.effects';

export { SlideActions, SlideSelectors, SlideFeature };
