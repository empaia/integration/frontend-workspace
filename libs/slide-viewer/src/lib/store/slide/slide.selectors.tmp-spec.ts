import * as fromSlide from './slide.reducer';
import { selectSlideFeatureState } from './slide.selectors';

describe('Slide Selectors', () => {
  it('should select the feature state', () => {
    const result = selectSlideFeatureState({
      [fromSlide.svmSlideFeatureKey]: {},
    });

    expect(result).toEqual({});
  });
});
