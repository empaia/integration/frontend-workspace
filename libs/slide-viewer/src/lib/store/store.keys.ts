export const SVM_FEATURE_KEY = 'SlideViewerModuleStore';

export const SVM_SLIDE_FEATURE_KEY = 'svmSlide';
export const SVM_ANNOTATIONS_FEATURE_KEY = 'svmAnnotations';
export const SVM_UI_FEATURE_KEY = 'svmUiState';
export const SVN_CLUSTER_FEATURE_KEY = 'svmCluster';
export const SVM_CHARACTER_CONFIG_KEY = 'svmCharacterConfig';
export const SVM_PIXEL_MAPS_FEATURE_KEY = 'svmPixelMaps';
export const SVM_FEATURES_FEATURE_KEY = 'svmFeatures';
