import { Action, createReducer, on } from '@ngrx/store';
import * as ClusterActions from './cluster.actions';

export interface State {
  clusterDistance: number;
}

export const initialState: State = {
  clusterDistance: undefined,
};


const clusterReducer = createReducer(
  initialState,
  on(ClusterActions.setClusterDistance, (state, { clusterDistance }): State => ({
    ...state,
    clusterDistance
  })),
);

export function reducer(state: State | undefined, action: Action) {
  return clusterReducer(state, action);
}
