import { Injectable } from '@angular/core';
import { Actions, concatLatestFrom, createEffect, ofType } from '@ngrx/effects';
import { FeatureStyleService } from '../../services/feature-style-service';
import { FeaturesActions } from './features.actions';
import * as UiActions from '../ui/ui.actions';
import * as FeaturesSelectors from './features.selectors';
import * as UiSelectors from '../ui/ui.selectors';
import { map } from 'rxjs/operators';
import { DEFAULT_TRANSPARENT_FILL } from '../../models/feature-style';
import { Store } from '@ngrx/store';



@Injectable()
export class FeaturesEffects {

  convertColorToFeatureMapping$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(FeaturesActions.setClassColorMapping),
      map(action => action.classColorMapping),
      concatLatestFrom(() => [
        this.store.select(UiSelectors.selectAnnotationStrokeWidth)
      ]),
      map(([classColorMapping, strokeWidth]) =>
        this.featureStyleService.createFeatureStyleMapping(classColorMapping, DEFAULT_TRANSPARENT_FILL, strokeWidth)
      ),
      map(featureStyleMapping => FeaturesActions.setFeatureStyleMapping({ featureStyleMapping }))
    );
  });

  // update feature style mapping when annotation
  // stroke width was changed
  updateFeatureStyleMapping$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(UiActions.setAnnotationStrokeWidth),
      map(action => action.annotationStrokeWidth),
      concatLatestFrom(() => [
        this.store.select(FeaturesSelectors.selectFeatureStyleMapping)
      ]),
      map(([annotationStrokeWidth, featureStyleMapping]) =>
        this.featureStyleService.updateFeatureStyleMapping(featureStyleMapping, DEFAULT_TRANSPARENT_FILL, annotationStrokeWidth)
      ),
      map(featureStyleMapping => FeaturesActions.setFeatureStyleMapping({ featureStyleMapping }))
    );
  });

  constructor(
    private readonly actions$: Actions,
    private readonly store: Store,
    private readonly featureStyleService: FeatureStyleService,
  ) {}
}
