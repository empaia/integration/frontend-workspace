import { createSelector } from '@ngrx/store';
import { selectModuleFeatureState } from '../module.state';
import { SVM_FEATURES_FEATURE_KEY } from '../store.keys';

export const selectFeaturesState = createSelector(
  selectModuleFeatureState,
  (state) => state[SVM_FEATURES_FEATURE_KEY]
);

export const selectFeatureStyleMapping = createSelector(
  selectFeaturesState,
  (state) => state.featureStyleMapping
);
