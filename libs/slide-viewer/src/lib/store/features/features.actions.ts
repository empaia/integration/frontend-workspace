import { createActionGroup, emptyProps, props } from '@ngrx/store';
import { ClassColorMapping, FeatureStyleMapping } from '../../models/feature-style';

export const FeaturesActions = createActionGroup({
  source: '[SVM] Features Actions',
  events: {
    'Set Class Color Mapping': props<{ classColorMapping: ClassColorMapping }>(),
    'Set Feature Style Mapping': props<{ featureStyleMapping: FeatureStyleMapping }>(),
    'Clear Features': emptyProps(),
  }
});
