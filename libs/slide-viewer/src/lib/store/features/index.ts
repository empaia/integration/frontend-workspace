import { FeaturesActions } from './features.actions';
import * as Feature from './features.reducer';
import * as FeaturesSelectors from './features.selectors';
export * from './features.effects';

export { FeaturesActions, Feature, FeaturesSelectors };
