import { Action, createReducer, on } from '@ngrx/store';
import { FeatureStyleMapping } from '../../models/feature-style';
import { FeaturesActions } from './features.actions';


export interface State {
  featureStyleMapping: FeatureStyleMapping;
}

export const initialState: State = {
  featureStyleMapping: {}
};

const featureReducer = createReducer(
  initialState,
  on(FeaturesActions.setFeatureStyleMapping, (state, { featureStyleMapping }): State => ({
    ...state,
    featureStyleMapping,
  })),
  on(FeaturesActions.clearFeatures, (state): State => ({
    ...state,
    ...initialState,
  }))
);

export function reducer(state: State | undefined, action: Action): State {
  return featureReducer(state, action);
}
