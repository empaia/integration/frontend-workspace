import { Action, createReducer, on } from '@ngrx/store';
import { OpacityChange, PixelMapInput, PixelMapUpdate, ZoomToLevel } from '../../models/pixelmap';
import { PixelMapsActions } from './pixel-maps.actions';

export interface State {
  pixelMapInput?: PixelMapInput | undefined;
  pixelMapUpdate?: PixelMapUpdate | undefined;
  channelOpacity?: OpacityChange | undefined;
  zoomToLevel?: ZoomToLevel | undefined;
}

export const initialState: State = {
  pixelMapInput: undefined,
  pixelMapUpdate: undefined,
  channelOpacity: undefined,
  zoomToLevel: undefined,
};

export const slideMapReducer = createReducer(
  initialState,
  on(PixelMapsActions.sVMSetPixelMapInput, (state, { pixelMapInput }): State => ({
    ...state,
    pixelMapInput,
  })),
  on(PixelMapsActions.sVMUpdatePixelMap, (state, { pixelMapUpdate }): State => ({
    ...state,
    pixelMapUpdate,
  })),
  on(PixelMapsActions.sVMSetChannelOpacity, (state, { channelOpacity }): State => ({
    ...state,
    channelOpacity,
  })),
  on(PixelMapsActions.sVMZoomToPixelMapLevelSuccess, (state, { zoomToLevel }): State => ({
    ...state,
    zoomToLevel,
  })),
);

export function reducer(state: State | undefined, action: Action): State {
  return slideMapReducer(state, action);
}
