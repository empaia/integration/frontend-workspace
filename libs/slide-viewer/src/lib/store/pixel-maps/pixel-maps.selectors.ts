import { createSelector } from '@ngrx/store';
import { selectModuleFeatureState } from '../module.state';
import { SVM_PIXEL_MAPS_FEATURE_KEY } from '../store.keys';

export const selectPixelMapsState = createSelector(
  selectModuleFeatureState,
  (state) => state[SVM_PIXEL_MAPS_FEATURE_KEY]
);

export const selectPixelMapInput = createSelector(
  selectPixelMapsState,
  (state) => state.pixelMapInput
);

export const selectPixelMapUpdate = createSelector(
  selectPixelMapsState,
  (state) => state.pixelMapUpdate
);

export const selectChannelOpacity = createSelector(
  selectPixelMapsState,
  (state) => state.channelOpacity
);

export const selectZoomToLevel = createSelector(
  selectPixelMapsState,
  (state) => state.zoomToLevel
);
