import { createActionGroup, props } from '@ngrx/store';
import { OpacityChange, PixelMapInput, PixelMapUpdate, ZoomToLevel } from '../../models/pixelmap';

export const PixelMapsActions = createActionGroup({
  source: 'SVM Pixel Maps Actions',
  events: {
    'SVM Set Pixel Map Input': props<{ pixelMapInput?: PixelMapInput }>(),
    'SVM Update Pixel Map': props<{ pixelMapUpdate: PixelMapUpdate }>(),
    'SVM Set Channel Opacity': props<{ channelOpacity?: OpacityChange }>(),
    'SVM Zoom To Pixel Map Level': props<{ zoomToLevel: ZoomToLevel }>(),
    'SVM Zoom To Pixel Map Level Success': props<{ zoomToLevel: ZoomToLevel }>(),
  }
});
