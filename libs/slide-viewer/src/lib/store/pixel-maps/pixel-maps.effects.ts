import { Injectable } from '@angular/core';
import { Actions, concatLatestFrom, createEffect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { PixelMapsActions } from './pixel-maps.actions';
import { filter, map } from 'rxjs/operators';
import * as SlideSelectors from '../slide/slide.selectors';
import { ZoomToLevel } from '../../models/pixelmap';



@Injectable()
export class PixelMapsEffects {

  // convert zoom level to openlayers zoom level
  convertZoomToLevel$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(PixelMapsActions.sVMZoomToPixelMapLevel),
      map(action => action.zoomToLevel),
      concatLatestFrom(() => [
        this.store.select(SlideSelectors.selectSlide)
      ]),
      filter(([zoomToLevel, slide]) => !!zoomToLevel && !!slide),
      map(([zoomToLevel, slide]) => {
        const olZoomTo: ZoomToLevel = {
          level: slide.imageInfo.numberOfLevels - zoomToLevel.level - (slide.resolver.hasArtificialLevel ? 0 : 1)
        };
        return olZoomTo;
      }),
      map(zoomToLevel => PixelMapsActions.sVMZoomToPixelMapLevelSuccess({ zoomToLevel }))
    );
  });

  constructor(
    private readonly store: Store,
    private readonly actions$: Actions
  ) {}
}
