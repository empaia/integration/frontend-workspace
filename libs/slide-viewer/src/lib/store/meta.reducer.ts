import { Action, ActionReducer, createAction } from '@ngrx/store';
import { ModuleState } from './module.state';

// clear state on module destroy - triggered in ngDestroy
export const CLEAR_STORE = createAction(
  '[SVM] CLEAR STORE',
);
export function clearState(reducer: ActionReducer<ModuleState>) {
  return function (state: ModuleState, action: Action): ModuleState {
    if (action.type === CLEAR_STORE.type) {
      state = undefined;
    }
    return reducer(state, action);
  };
}
