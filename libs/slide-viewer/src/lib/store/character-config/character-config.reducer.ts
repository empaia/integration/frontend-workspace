import { Action, createReducer, on } from '@ngrx/store';
import * as CharacterConfigActions from './character-config.actions';
import { CharacterLimit } from '../../models/character-limit';

export interface State {
  config: CharacterLimit;
}

export const initialState: State = {
  config: {},
};


export const characterConfigReducer = createReducer(
  initialState,
  on(CharacterConfigActions.setCharacterConfig, (state, { config }): State => ({
    ...state,
    config
  })),
);

export function reducer(state: State | undefined, action: Action) {
  return characterConfigReducer(state, action);
}
