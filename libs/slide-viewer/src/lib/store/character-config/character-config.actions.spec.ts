import * as fromCharacterConfig from './character-config.actions';

describe('loadCharacterConfigs', () => {
  it('should return an action', () => {
    expect(fromCharacterConfig.setCharacterConfig({
      config: {
        annotation: {
          name: 1,
          description: 1,
        }
      }
    }).type).toBe('[SVM CharacterConfig] Set Character Config');
  });
});
