import { createAction, props } from '@ngrx/store';
import { CharacterLimit } from '../../models/character-limit';

export const setCharacterConfig = createAction(
  '[SVM CharacterConfig] Set Character Config',
  props<{ config: CharacterLimit }>()
);




