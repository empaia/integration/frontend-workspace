import { AnnotationMappingService } from './../../services/annotation-mapping.service';
import { Injectable } from '@angular/core';
import { Actions, concatLatestFrom, createEffect, ofType } from '@ngrx/effects';
import * as AnnotationsActions from './annotation.actions';
import * as AnnotationsSelectors from './annotation.selectors';
import { Store } from '@ngrx/store';
import { filter, map } from 'rxjs/operators';

@Injectable()
export class AnnotationEffects {

  addAnnotationIds$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(AnnotationsActions.setAnnotationIds),
      map(action => action.annotationIds),
      map(ids => this.annotationIdSet.addMany(ids)),
      map(() => AnnotationsActions.setAnnotationIdsReady())
    );
  });

  // we need to make place for the newly requested annotations, so we clean up the cache if needed
  checkCleanUpCache$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(AnnotationsActions.setAnnotationIdsReady),
      concatLatestFrom(() => [
        this.store.select(AnnotationsSelectors.selectMissingCachedAnnotations),
        this.store.select(AnnotationsSelectors.selectCurrentAnnotationCacheSize),
        this.store.select(AnnotationsSelectors.selectMaxAnnotationCacheSize)
      ]),
      filter(([_action, cache, cacheSize, maxSize]) => cacheSize + cache.missingAnnotations.length > maxSize),
      map(([_action, cache, cacheSize]) => cacheSize - cache.cachedAnnotations.length),
      filter(index => index > 0),
      map(index => this.annotationIdSet.discard(index)),
      map(annotationIds => AnnotationsActions.discardAnnotations({ annotationIds }))
    );
  });

  clearAnnotations$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(AnnotationsActions.clearAnnotations),
      map(() => this.annotationIdSet.clear())
    );
  }, { dispatch: false });

  constructor(
    private actions$: Actions,
    private store: Store,
    private annotationIdSet: AnnotationMappingService,
  ) {}
}
