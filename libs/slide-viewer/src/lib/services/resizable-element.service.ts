import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

export interface ElementSize {
  width: number;
  height: number;
}

@Injectable()
export class ResizableElementService {
  private elementSize$ = new BehaviorSubject<ElementSize | undefined>(
    undefined
  );

  setStyle(e: ElementSize) {
    this.elementSize$.next(e);
  }

  get style(): Observable<ElementSize> {
    return this.elementSize$.asObservable();
  }
}
