import { TestBed, inject } from '@angular/core/testing';
import { ResizableElementService } from './resizable-element.service';

describe('Service: OverviewSize', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ResizableElementService],
      teardown: { destroyAfterEach: false },
    });
  });

  it('should ...', inject(
    [ResizableElementService],
    (service: ResizableElementService) => {
      expect(service).toBeTruthy();
    }
  ));
});
