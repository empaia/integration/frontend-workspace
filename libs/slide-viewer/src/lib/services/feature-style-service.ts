import { Injectable } from '@angular/core';
import { Fill, Stroke, Style } from 'ol/style';
import { Circle as CircleStyle } from 'ol/style';
import {
  ClassColorMapping,
  ColorState,
  FeatureStyle,
  FeatureStyleEntry,
  FeatureStyleMapping,
} from '../models/feature-style';
import { Color } from 'ol/color';
import { ColorLike } from 'ol/colorlike';

@Injectable({
  providedIn: 'root'
})
export class FeatureStyleService {

  private calculateCircleStrokeWidth(strokeWidth: number): number {
    return Math.max(2, strokeWidth);
  }

  private createStyle(color: string | Color | ColorLike, transparentFill: Fill, strokeWidth: number): Style {
    return new Style({
      fill: transparentFill,
      stroke: new Stroke({ color, width: strokeWidth }),
    });
  }

  private createCircleStyle(color: string | Color | ColorLike, transparentFill: Fill, circleRadius: number, strokeWidth: number): Style {
    return new Style({
      image: new CircleStyle({
        fill: transparentFill,
        radius: circleRadius,
        stroke: new Stroke({ color, width: this.calculateCircleStrokeWidth(strokeWidth) }),
      })
    });
  }

  private createStyleEntryColor(color: string | Color | ColorLike, transparentFill: Fill, circleRadius: number, strokeWidth: number): FeatureStyleEntry {
    return [
      this.createStyle(color, transparentFill, strokeWidth),
      this.createCircleStyle(color, transparentFill, circleRadius, strokeWidth)
    ];
  }

  private createFeatureStyle(colorState: ColorState, transparentFill: Fill, circleRadius: number, strokeWidth: number): FeatureStyle {
    return {
      DEFAULT: this.createStyleEntryColor(colorState.color, transparentFill, circleRadius, strokeWidth),
      HOVER: this.createStyleEntryColor(colorState.colorHover, transparentFill, circleRadius, strokeWidth),
      SELECTION: this.createStyleEntryColor(colorState.colorSelection, transparentFill, circleRadius, strokeWidth),
    };
  }

  public createFeatureStyleMapping(classColorMapping: ClassColorMapping, transparentFill: Fill, strokeWidth = 1, circleRadius = 5): FeatureStyleMapping {
    return Object
      .entries(classColorMapping)
      .reduce((acc, curr) => ({
        ...acc,
        [curr[0]]: this.createFeatureStyle(curr[1], transparentFill, circleRadius, strokeWidth)
      }), {});
  }

  public updateFeatureStyle(featureStyle: FeatureStyle, transparentFill: Fill, strokeWidth: number, circleRadius: number): FeatureStyle {
    return {
      DEFAULT: this.createStyleEntryColor(featureStyle.DEFAULT[0].getStroke().getColor(), transparentFill, circleRadius, strokeWidth),
      HOVER: this.createStyleEntryColor(featureStyle.HOVER[0].getStroke().getColor(), transparentFill, circleRadius, strokeWidth),
      SELECTION: this.createStyleEntryColor(featureStyle.SELECTION[0].getStroke().getColor(), transparentFill, circleRadius, strokeWidth),
    };
  }

  public updateFeatureStyleMapping(featureStyleMapping: FeatureStyleMapping, transparentFill: Fill, strokeWidth = 1, circleRadius = 5): FeatureStyleMapping {
    // we have to create a new feature style map, because the old one and all its entries
    // are read only due to the state management of ngrx
    return Object
      .entries(featureStyleMapping)
      .reduce((acc, curr) => ({
        ...acc,
        [curr[0]]: this.updateFeatureStyle(curr[1], transparentFill, strokeWidth, circleRadius)
      }), {});
  }
}
