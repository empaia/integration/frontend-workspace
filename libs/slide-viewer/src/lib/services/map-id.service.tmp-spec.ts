import { TestBed, inject } from '@angular/core/testing';
import { MapIdService } from './map-id.service';

describe('Service: MapId', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MapIdService],
      teardown: { destroyAfterEach: false },
    });
  });

  it('should ...', inject([MapIdService], (service: MapIdService) => {
    expect(service).toBeTruthy();
  }));
});
