import { Injectable } from '@angular/core';
import { Control } from 'ol/control';
import OlMap from 'ol/Map';
import { MapIdService } from './map-id.service';
import { defaults, DragPan } from 'ol/interaction';
import { noModifierKeys } from 'ol/events/condition';
import { Kinetic } from 'ol';

@Injectable({
  providedIn: 'root',
})
export class MapService {
  /**
   * List of Openlayer map objects [ol.Map](https://openlayers.org/en/latest/apidoc/module-ol_Map-Map.html)
   */
  private olMaps = new Map<string, OlMap>();
  private usedImageSources = new Map<string, Array<string>>();

  /**
   * Create an empty map
   *
   * @param id map id
   * @returns [ol.Map](https://openlayers.org/en/latest/apidoc/module-ol_Map-Map.html) the map
   */
  private createMap(id: string): OlMap {
    const map = new OlMap({
      target: id,
      controls: new Array<Control>(),
      interactions: defaults({ dragPan: false })
        .extend([
          new DragPan({
            condition: noModifierKeys,
            // default kinetic values from openlayers
            // found under: node_modules/ol/interaction.js line 84
            kinetic: new Kinetic(-0.005, 0.05, 100)
          })
        ])
    });
    return map;
  }

  /**
   * Get a map. If it doesn't exist it will be created.
   *
   * @param id id of the map or an objet with a getId method (from mapid service), default 'map'
   */
  getMap(id: string | MapIdService): OlMap | undefined {
    id = (id instanceof MapIdService ? id.getId() : id) || 'map';

    // Create map if not exist
    if (!this.olMaps.has(id)) {
      this.olMaps.set(id, this.createMap(id));
      this.usedImageSources.set(id, []);
    }
    // return the map
    return this.olMaps.get(id);
  }

  getImageSources(id: string | MapIdService): Array<string> | undefined {
    id = (id instanceof MapIdService ? id.getId() : id) || 'map';

    // return the image source ids
    return this.usedImageSources.get(id);
  }

  setImageSources(mapId: string | MapIdService, sourceId: string) {
    mapId = mapId instanceof MapIdService ? mapId.getId() : mapId;

    this.usedImageSources.set(mapId, [sourceId]);
  }

  /** Get all maps
   * NB: to access the complete list of maps you should use the ngAfterViewInit() method to have all maps instanced.
   *
   * @return the list of maps
   */
  getMaps(): Map<string, OlMap> {
    return this.olMaps;
  }

  /** Get all maps
   * NB: to access the complete list of maps you should use the ngAfterViewInit() method to have all maps instanced.
   *
   * @return array of maps
   */
  getArrayMaps(): Array<OlMap> {
    return Object.values(this.olMaps.values());
  }
}
