import { BehaviorSubject } from 'rxjs';

import { TypedArray } from 'geotiff';
import { ChannelInfo } from '../models/slide';

/**
 * Cache and share data globally
 */
export abstract class SlideChannelDataCache {

  private static _histogramData: Map<string,Map<number,number>> = new Map<string,Map<number,number>>();

  private static slideChannelDataCache: Map<string, TypedArray | TypedArray[]> = new Map<string, Uint8Array[] | Uint16Array[] | Uint32Array[]>();

  public static channelsInfo = new BehaviorSubject<Array<ChannelInfo>>([]);

  public static clear() {
    SlideChannelDataCache.slideChannelDataCache.clear();
    this._histogramData = new Map<string,Map<number,number>>();
  }

  public static get(key: string): TypedArray | TypedArray[] {
    return SlideChannelDataCache.slideChannelDataCache.get(key);
  }

  public static set(key: string, value: TypedArray | TypedArray[]): void {
    SlideChannelDataCache.slideChannelDataCache.set(key, value);
  }

  public static getHistogramDataForChannel(channelName: string): Map<number,number> {
    return this._histogramData.has(channelName) ? this._histogramData.get(channelName) : new Map();
  }

  public static setHistogramDataForChannel(channelName: string, data: Uint16Array | Uint8Array | Uint32Array): void {

    data.forEach((item: number) => {
      if (!this._histogramData.has(channelName)) {
        const newChannelMap: Map<number, number> = new Map<number, number>();
        newChannelMap.set(item, 1);
        this._histogramData.set(channelName, newChannelMap);
      } else {
        const currentChannelMap: Map<number, number> = this._histogramData.get(channelName);
        if (currentChannelMap.has(item)) {
          currentChannelMap.set(item, currentChannelMap.get(item) + 1); // increment count
        } else {
          currentChannelMap.set(item, 1); // initialize count
        }
      }
    });
  }
}
