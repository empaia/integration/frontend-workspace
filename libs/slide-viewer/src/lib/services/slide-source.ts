import { TileDebug, XYZ } from 'ol/source';
import TileGrid from 'ol/tilegrid/TileGrid';
import { PROJECTION_KEY } from '../models/types';
import { Slide } from '../models/slide';

/**
 *
 */
export class SlideSourceLoader {
  //public id: string; // slide id
  private static createTileGrid(slide: Slide): TileGrid {
    if (!slide.imageInfo.width || !slide.imageInfo.height) {
      throw new Error('Unable to load images size');
    }

    const minZoom = slide.imageInfo.resolutions.length
      - slide.imageInfo.numberOfLevels
      - (slide.resolver.hasArtificialLevel ? 1 : 0)
      - (slide.imageInfo.numberOfOverZoomLevels - 1) ?? 0;
    const tileGrid = new TileGrid({
      resolutions: slide.imageInfo.resolutions,
      extent: [0, -slide.imageInfo.height, slide.imageInfo.width, 0],
      origin: [0, 0],
      tileSizes: slide.imageInfo.tileSizes,
      minZoom,
    });

    return tileGrid;
  }

  public static createXYZSource(slide: Slide): XYZ {
    const tileGrid = this.createTileGrid(slide);

    const source = new XYZ({
      tileGrid,
      projection: PROJECTION_KEY,
      url: 'DUMMY_URL', // there needs to be something set here to use tileLoadFn
      transition: 0,
    });

    if (slide.resolver.tileResolverFn) {
      source.setTileUrlFunction(slide.resolver.tileResolverFn);
    } else if (slide.resolver.tileLoaderFn) {
      source.setTileLoadFunction(slide.resolver.tileLoaderFn);
    } else {
      throw new Error('No function to load tiles set. \n Implement either tileResolverFn or tileLoadFn');
    }

    return source;
  }

  // create an tile grid with outlines and coordinates for debugging
  public static createTileDebug(slide: Slide): TileDebug {
    const tileGrid = this.createTileGrid(slide);

    const debugSource = new TileDebug({
      tileGrid,
      projection: PROJECTION_KEY,
    });

    return debugSource;
  }
}
