import { ModuleWithProviders, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { ReactiveFormsModule } from '@angular/forms';
import { MatMenuModule } from '@angular/material/menu';
import { MaterialModule } from './material/material.module';
import { ResizableModule } from 'angular-resizable-element';
import {
  TileResolverInterface,
  Slide,
  ImageInfo,
  Level,
  Channel,
  ChannelInfo,
  Color
} from './models/slide';
import {
  PixelMapTileRequesterInterface,
  PixelMapInput,
  PixelMapUpdate,
  ChannelClassMapping,
  ZoomToLevel,
} from './models/pixelmap';
import { TileResolver } from './tools/tile-resolver';
import {
  IAnnotation,
  IAnnotationCircle,
  IAnnotationPoint,
  IAnnotationPolygon,
  IAnnotationRectangle,
  AnnotationRectangle,
  AnnotationCircle,
  AnnotationPolygon,
  AnnotationPoint,
  AnnotationReferenceType,
  AnnotationEntity,
} from './models/annotation';
import { UiConfig } from './models/viewer-config';
import { TileConnector, RawMultichannelData } from './models/tile-connector';
import { WSIFileFormat } from './models/file-format';
import {
  AnnotationCharacterLimit,
  CharacterLimit,
} from './models/character-limit';


/** module config */
import {
  SlideViewerModuleConfigurationParams,
  SlideViewerModuleConfiguration
} from './config/module-config';

/* containers */
import { SlideViewerComponent } from './containers/slide-viewer/slide-viewer.component';
import { SlideViewContainerComponent } from './containers/slide-view-container/slide-view-container.component';

/* view components */
//import { OverviewComponent } from './components/overview/overview.component';
import { ScaleLineComponent } from './components/scale-line/scale-line.component';
//import { ZoomSliderComponent } from './components/zoom-slider/zoom-slider.component';
import { ViewerComponent } from './components/viewer/viewer.component';
import { ExtOverviewComponent } from './components/ext-overview/ext-overview.component';
import { DndContainerComponent } from './components/dnd-container/dnd-container.component';
import { CreateAnnotationCardComponent } from './components/create-annotation-card/create-annotation-card.component';
import { ToolbarComponent } from './components/toolbar/toolbar.component';
import { AnnotationLayerComponent } from './components/annotation-layer/annotation-layer.component';
import { CurrentView, ZoomView } from './models/ui';

import { StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { SvmStoreModule } from './store/svm-store.module';
import { AnnotationCreateComponent } from './components/annotation-create/annotation-create.component';
import { MeasurementToolComponent } from './components/measurement-tool/measurement-tool.component';
import { AnnotationType } from './models/annotation-type';
import { LetDirective, PushPipe } from '@ngrx/component';
import { ExtZoomSliderComponent } from './components/ext-zoom-slider/ext-zoom-slider.component';
import { AnnotationClusterLayerComponent } from './components/annotation-cluster-layer/annotation-cluster-layer.component';
import { ToolbarInteractionType, InteractionType, DrawType } from './models/interaction';
import { DataCreatorType } from './models/creator';
import { Class } from './models/class';
import { CloseElementDirective } from './directives/close-element.directive';
import { AnnotationHighlightConfig, HighlightType } from './config/annotation-highlight.config';
import { PixelMapLayerComponent } from './components/pixel-map-layer/pixel-map-layer.component';
import { PixelMapOpacityMenuComponent } from './components/pixel-map-opacity-menu/pixel-map-opacity-menu.component';
import { ClassColorMapping, ColorState } from './models/feature-style';

export {
  /**
   * Component exports
   * TODO: do not export components that are not for the public api
   */
  SlideViewerComponent,
  /**
   * Interface exports
   * TODO: do not export interfaces that are not for the public api
   */
  TileResolverInterface,
  TileResolver,
  PixelMapTileRequesterInterface,
  ChannelClassMapping,
  PixelMapInput,
  PixelMapUpdate,
  ZoomToLevel,
  IAnnotation as Annotation,
  AnnotationCircle,
  AnnotationRectangle,
  AnnotationPolygon,
  AnnotationPoint,
  AnnotationType,
  AnnotationReferenceType,
  AnnotationEntity,
  IAnnotationCircle,
  IAnnotationPoint,
  IAnnotationPolygon,
  IAnnotationRectangle,
  Class,
  DataCreatorType,
  Slide,
  Channel,
  ChannelInfo,
  ImageInfo,
  Level,
  Color,
  TileConnector,
  RawMultichannelData,
  WSIFileFormat,
  CurrentView,
  ZoomView,
  ToolbarInteractionType,
  InteractionType,
  DrawType,
  UiConfig,
  AnnotationCharacterLimit,
  CharacterLimit,
  AnnotationHighlightConfig,
  HighlightType,
  ColorState,
  ClassColorMapping,
};

/**
 * Module Components
 */
const COMPONENTS = [
  SlideViewContainerComponent,
  ViewerComponent,
  //OverviewComponent,
  ExtOverviewComponent,
  ScaleLineComponent,
  //ZoomSliderComponent,
  ExtZoomSliderComponent,
  DndContainerComponent,
  CreateAnnotationCardComponent,
  ToolbarComponent,
  AnnotationLayerComponent,
  AnnotationCreateComponent,
  SlideViewerComponent,
  MeasurementToolComponent,
  AnnotationClusterLayerComponent,
  PixelMapLayerComponent,
  PixelMapOpacityMenuComponent,
];

@NgModule({
  declarations: [...COMPONENTS, CloseElementDirective,],
  imports: [
    CommonModule,
    MaterialModule,
    MatMenuModule,
    ReactiveFormsModule,
    DragDropModule,
    ResizableModule,
    SvmStoreModule,
    StoreModule.forRoot({
      /* EMPTY Root store */
    }),
    StoreDevtoolsModule.instrument({
      name: 'Slide Viewer Module'
      // In a production build you would want to disable the Store Devtools
      // logOnly: environment.production,
      ,connectInZone: true}),
    LetDirective,
    PushPipe,
  ],
  exports: [
    SlideViewerComponent
  ],
  providers: [
    SlideViewerModuleConfiguration,
  ]
})
export class SlideViewerModule {
  static forRoot(params: SlideViewerModuleConfigurationParams = {}): ModuleWithProviders<SlideViewerModule> {
    return {
      ngModule: SlideViewerModule,
      providers: [
        {
          provide: SlideViewerModuleConfiguration,
          useValue: params
        }
      ]
    };
  }
}
