# SlideViewer Changelog 

# 2.12.2 (2025-02-12)

### Fixes

* adjusted slide store

# 2.12.1 (2024-07-16)

### Refactor

* upgraded to angular 17

# 2.12.0 (2024-01-29)

### Features

* added zoom to pixel map level functionality

# 2.11.2 (2024-01-11)

### Refactor

* updated to angular 16

# 2.11.1 (2024-01-05)

### Refactor

* adjusted position of annotation toolbar and overview window

# 2.11.0 (2023-12-22)

### Features

* added pixelmap rendering
* added interface for colorize annotations

# 2.10.4 (2023-09-11)

### Refactor

* removed unused multi channel components

# 2.10.0 (2023-06-06)

### Features

* rearrange vertices of polygons that are out-of-bounds

# 2.9.0 (2023-05-12)

### Refactor / Features

* annotations can be updated
* added disabled color class

# 2.8.4 (2023-04-21)

### Fixes

* annotations with no classes can be shown when class values are not provided

# 2.8.1 (2023-03-24)

### Refactor

* annotations with no classes will be added to class color with value null (if available)
* clicking on an annotation will only highlight the first array element

# 2.8.0 (2023-03-20)

### Features

* updated to Angular 15

# 2.7.3 (2023-03-07)

### Fixes

* feature color classes accepts null as a valid class id for annotations that have no classes

# 2.7.2 (2023-01-20)

### Fixes

* Cluster features can't be selected by clicking

# 2.7.0 (2022-12-22)

### Feature

* Added config for highlighting type. Annotations can be highlighted via hover (default) or via clicking

# 2.6.5 (2022-11-03)

### Refactor

* Removed deprecated `@angular/flex-layout` library for normal flex layout

# 2.6.2 (2022-10-13)

### Refactor

* Updated dependencies

# 2.6.0 (2022-09-19)

### Features

* Slide-Viewer allows over zooming layers

# 2.5.2 (2022-08-16)

### Refactor

* Altered the background color for a better visualisation, that the zoom slider is one ui control instead of separate floating ui elements

# 2.5.1 (2022-08-15)

### Bug Fixes

* tooltips will now be rendered with a z-index of 1000 to be above all menus

# 2.5.0 (2022-08-03)

### Features

* annotation draw tools will be deactivated when examination is marked as closed

# 2.4.2 (2022-07-29)

### Refactor

* dragging speed for the left mouse button was set back to the state of version 2.4.0 and the right mouse button was adjusted accordingly

# 2.4.1 (2022-07-25)

### Bug Fixes

* dragging slide with left mouse button works with normal speed, instead of double speed

# 2.4.0 (2022-07-12)

### Features

* added alternative draw mode for polygon annotations via additional button

# [2.2.0](https://gitlab.cc-asp.fraunhofer.de/empaia/frontend/frontend-workspace/compare/slide-viewer-2.1.0...slide-viewer-2.2.0) (2021-05-28)


### Bug Fixes

* **svm:** fix draw mode when toggling toolbar ([0d4ed9e](https://gitlab.cc-asp.fraunhofer.de/empaia/frontend/frontend-workspace/commit/0d4ed9effebf0610de3454fa7378618f26eb89b9))
* add store clear on ngDestroy ([4eef478](https://gitlab.cc-asp.fraunhofer.de/empaia/frontend/frontend-workspace/commit/4eef478e087819ef3962df02a3827a6ef0dd49e3))
* double emmision of newly created annotation on viewer reload ([fdd4e5d](https://gitlab.cc-asp.fraunhofer.de/empaia/frontend/frontend-workspace/commit/fdd4e5ded1b05d942cf403724a6b11a4ad868689))
* reinitialisation of module ([22da94e](https://gitlab.cc-asp.fraunhofer.de/empaia/frontend/frontend-workspace/commit/22da94ed4fb6403d87dc5288cf2b330a9d11dcfa))


### Features

* **svm:** started working on annotation styling ([e1dc57b](https://gitlab.cc-asp.fraunhofer.de/empaia/frontend/frontend-workspace/commit/e1dc57b97b932a1c182c308062126b7c79e3f01e))
* emit selected tool ([8a1e401](https://gitlab.cc-asp.fraunhofer.de/empaia/frontend/frontend-workspace/commit/8a1e4019adc1f096f01edda3c12a631720459661))
* hide all ui elements if no slide is set ([757a014](https://gitlab.cc-asp.fraunhofer.de/empaia/frontend/frontend-workspace/commit/757a0141dd062eb9f84258c15c28ef3b53630863)), closes [#229](https://gitlab.cc-asp.fraunhofer.de/empaia/frontend/frontend-workspace/issues/229)



# [2.1.0](https://gitlab.cc-asp.fraunhofer.de/empaia/frontend/frontend-workspace/compare/slide-viewer-2.0.0...slide-viewer-2.1.0) (2021-05-11)


### Bug Fixes

* release fix ([8fcac52](https://gitlab.cc-asp.fraunhofer.de/empaia/frontend/frontend-workspace/commit/8fcac52d2dd64d993fc9aa939d50a1ce218b9440))


### Features

* release fix ([fde49d2](https://gitlab.cc-asp.fraunhofer.de/empaia/frontend/frontend-workspace/commit/fde49d20d5ac7239a56713192db407fcb9c8af18))



# [1.0.0](https://gitlab.cc-asp.fraunhofer.de/empaia/frontend/frontend-workspace/compare/slide-viewer-1.0.0...slide-viewer-2.0.0) (2021-05-11)


### Bug Fixes

* change drawing of circle and rectangle to freehand ([9deb23a](https://gitlab.cc-asp.fraunhofer.de/empaia/frontend/frontend-workspace/commit/9deb23aeb917d638fc7bfe729add9e2a0aa763f8))
* **SVM:** cluster will now correct initialize the cluster distance ([ba0996a](https://gitlab.cc-asp.fraunhofer.de/empaia/frontend/frontend-workspace/commit/ba0996a3180d76db9d95e3217e0b407cf2d52811))
* **SVM&ATS:** annotation-toolbar can be switch on or off by the app ([c2ba3d9](https://gitlab.cc-asp.fraunhofer.de/empaia/frontend/frontend-workspace/commit/c2ba3d9dc178360f1419f04dc6389b6eb6520985))
* **SVM&ATS:** focus annotation error message appears now numerous times ([ba55355](https://gitlab.cc-asp.fraunhofer.de/empaia/frontend/frontend-workspace/commit/ba553556d6e1438f45c2ab2aef9bd187ee9b6024))
* annotation popup is back to the right position ([955b622](https://gitlab.cc-asp.fraunhofer.de/empaia/frontend/frontend-workspace/commit/955b622892707d3a5576f2d0b28405069519c78f))
* coordinate system, origin is now the upper left corner ([ff32e4a](https://gitlab.cc-asp.fraunhofer.de/empaia/frontend/frontend-workspace/commit/ff32e4a4d2e6f6cdf20727c38194836c45ef1281))
* Extent of the slide-viewer module were adjusted and works now as intended ([1082c5d](https://gitlab.cc-asp.fraunhofer.de/empaia/frontend/frontend-workspace/commit/1082c5d77a7261e5498313715bcefc8ffe6ed8da))
* Fixed building production issue with AxisConverter ([ec427c2](https://gitlab.cc-asp.fraunhofer.de/empaia/frontend/frontend-workspace/commit/ec427c2751b060adbc6450f5e33ad8af834d68f7))
* open slidenav when other rail buttons are clicked ([cd97f0c](https://gitlab.cc-asp.fraunhofer.de/empaia/frontend/frontend-workspace/commit/cd97f0c23cefd60f3064e5464198f438ce7a770b))
* remove last coordinate of polygon ([8c8c676](https://gitlab.cc-asp.fraunhofer.de/empaia/frontend/frontend-workspace/commit/8c8c676786b90324d5cf6e8bd5fb0260aa4d86ec))
* restrict creation of and zooming to annotations outside image boundaries ([7b2969c](https://gitlab.cc-asp.fraunhofer.de/empaia/frontend/frontend-workspace/commit/7b2969cb6152a822c11f5a9074cde093dac72f5e))
* update viewer when sidenav is toggled ([b825897](https://gitlab.cc-asp.fraunhofer.de/empaia/frontend/frontend-workspace/commit/b82589769aa2b9c21e880ba2ccaee43328c860db))
* y-axis convertation will be computed in the slide-viewer ([9ed5ddb](https://gitlab.cc-asp.fraunhofer.de/empaia/frontend/frontend-workspace/commit/9ed5ddb59bddc18cd42ce8f83969bdae773ced78))
* zoom in/out when no slide is loaded ([c556b20](https://gitlab.cc-asp.fraunhofer.de/empaia/frontend/frontend-workspace/commit/c556b20c2401c04f5809d041fe98cc5538d78d50))
* error handling for annotation outside image boundaries ([5c7a316](https://gitlab.cc-asp.fraunhofer.de/empaia/frontend/frontend-workspace/commit/5c7a31652abd245cc5e0f51952e17dcc4bb03bd8))
* rename scss files to prevent name clashing with app styles ([1f09136](https://gitlab.cc-asp.fraunhofer.de/empaia/frontend/frontend-workspace/commit/1f091365d233a23439ea438d66a87aa717fe584c))
* set annotation toolbar size to 48px instead of font size line height. set 48px as button height scss variable. ([721ee2b](https://gitlab.cc-asp.fraunhofer.de/empaia/frontend/frontend-workspace/commit/721ee2b931228de6fb4f8be5dc6541b893673fa9))


### Features

* add centroid calculation in viewer ([557b39b](https://gitlab.cc-asp.fraunhofer.de/empaia/frontend/frontend-workspace/commit/557b39b107b06cffa5399e12b83b388ab8646ed9))
* add global style definintion to allow postitiong of overview and toolbar by enclosing app ([54f2f43](https://gitlab.cc-asp.fraunhofer.de/empaia/frontend/frontend-workspace/commit/54f2f43b7c5963754540a92957543bf2dcc5cd99))
* add outlined-icons ([9337d65](https://gitlab.cc-asp.fraunhofer.de/empaia/frontend/frontend-workspace/commit/9337d655925b88195851882e821f4d941a6c0939))
* add slide viewer module config ([9d66340](https://gitlab.cc-asp.fraunhofer.de/empaia/frontend/frontend-workspace/commit/9d66340c8a4ed0f334df9a46c0910f3533c5d7cc))
* added default npp_viewing value for manual annotation ([e28419e](https://gitlab.cc-asp.fraunhofer.de/empaia/frontend/frontend-workspace/commit/e28419eb2b49d9ad6601b37d4faae00ec83156ad))
* emit map double click event from module ([77de39d](https://gitlab.cc-asp.fraunhofer.de/empaia/frontend/frontend-workspace/commit/77de39db4ee7298f6d7a81d0b853a4ee0fe76189))
* forbid intersecting polygon annotations ([d98554b](https://gitlab.cc-asp.fraunhofer.de/empaia/frontend/frontend-workspace/commit/d98554bd296a6388b0882f603e376ef4ec51d387))
* implemented calculaton of centroid ([ca94f91](https://gitlab.cc-asp.fraunhofer.de/empaia/frontend/frontend-workspace/commit/ca94f9199ec7ae2ca7e8e4884bdc10dfe0542306))
* zoom slider and scale bar layout can now be set from enclosign app. ([c399fd9](https://gitlab.cc-asp.fraunhofer.de/empaia/frontend/frontend-workspace/commit/c399fd936b5becd864727ac3439aa4adee637c48))
* Added npp range as an input property to the slide-viewer ([14a7b6e](https://gitlab.cc-asp.fraunhofer.de/empaia/frontend/frontend-workspace/commit/14a7b6e9b2db852bbfa7fc7b8b2683a5ca328267))
* create scss mixin that can be imported from enclosing app to overwrite theme styles of module ([cbaf735](https://gitlab.cc-asp.fraunhofer.de/empaia/frontend/frontend-workspace/commit/cbaf735b338a2f3256dac49a34b5891987cac1da))
* added clustering feature for annotations ([9b78d1f](https://gitlab.cc-asp.fraunhofer.de/empaia/frontend/frontend-workspace/commit/9b78d1f6431dc3d518ff432aa04f650ad40ef374))
* added annotation clustering ([bdbb959](https://gitlab.cc-asp.fraunhofer.de/empaia/frontend/frontend-workspace/commit/bdbb959f6923a45a8896d2e252b7dfa1a90a99b7))


* refactor!: class name for slide background changed ([36cc797](https://gitlab.cc-asp.fraunhofer.de/empaia/frontend/frontend-workspace/commit/36cc797cc2666ddf6c0c4bb51af435f74d140ebc))
* feat!: add material theme styling options ([e111ff8](https://gitlab.cc-asp.fraunhofer.de/empaia/frontend/frontend-workspace/commit/e111ff8a160f71c19c41c49fca638202a471713c))


### BREAKING CHANGES

* class name .slide-background was renamed to .svm-slide-background.
If you need a bg other than white replace this class name to set a background color.
* module needs an explicitly set material theme now



# [1.0.0](https://gitlab.cc-asp.fraunhofer.de/empaia/frontend/frontend-workspace/compare/slide-viewer-0.1.1...slide-viewer-1.0.0) (2021-04-09)

### Features

* create scss mixin that can be imported from enclosing app to overwrite theme styles of module ([cbaf735](https://gitlab.cc-asp.fraunhofer.de/empaia/frontend/frontend-workspace/commit/cbaf735b338a2f3256dac49a34b5891987cac1da))

*  add material theme styling options ([e111ff8](https://gitlab.cc-asp.fraunhofer.de/empaia/frontend/frontend-workspace/commit/e111ff8a160f71c19c41c49fca638202a471713c))
### BREAKING CHANGES

* module needs an explictly set material theme now
  * see [readme](./README.md) for instructions

# [0.2.0](https://gitlab.cc-asp.fraunhofer.de/empaia/frontend/frontend-workspace/compare/slide-viewer-0.1.1...slide-viewer-0.2.0) (2021-04-09)


### Bug Fixes

* error handling for annotation outside image boundaries ([5c7a316](https://gitlab.cc-asp.fraunhofer.de/empaia/frontend/frontend-workspace/commit/5c7a31652abd245cc5e0f51952e17dcc4bb03bd8))
* set annotation toolbar size to 48px instead of font size line height. set 48px as button height scss variable. ([721ee2b](https://gitlab.cc-asp.fraunhofer.de/empaia/frontend/frontend-workspace/commit/721ee2b931228de6fb4f8be5dc6541b893673fa9))
* Fixed building production issue with AxisConverter ([ec427c2](https://gitlab.cc-asp.fraunhofer.de/empaia/frontend/frontend-workspace/commit/ec427c2751b060adbc6450f5e33ad8af834d68f7))
* restrict creation of and zooming to annotations outside image boundaries ([7b2969c](https://gitlab.cc-asp.fraunhofer.de/empaia/frontend/frontend-workspace/commit/7b2969cb6152a822c11f5a9074cde093dac72f5e))
* rename scss files to prevent name clashing with app styles ([1f09136](https://gitlab.cc-asp.fraunhofer.de/empaia/frontend/frontend-workspace/commit/1f091365d233a23439ea438d66a87aa717fe584c))
* annotation popup is back to the right position ([955b622](https://gitlab.cc-asp.fraunhofer.de/empaia/frontend/frontend-workspace/commit/955b622892707d3a5576f2d0b28405069519c78f))
* coordinate system, origin is now the upper left corner ([ff32e4a](https://gitlab.cc-asp.fraunhofer.de/empaia/frontend/frontend-workspace/commit/ff32e4a4d2e6f6cdf20727c38194836c45ef1281))
* Extent of the slide-viewer module were adjusted and works now as intended ([1082c5d](https://gitlab.cc-asp.fraunhofer.de/empaia/frontend/frontend-workspace/commit/1082c5d77a7261e5498313715bcefc8ffe6ed8da))
* y-axis conversion will be computed in the slide-viewer ([9ed5ddb](https://gitlab.cc-asp.fraunhofer.de/empaia/frontend/frontend-workspace/commit/9ed5ddb59bddc18cd42ce8f83969bdae773ced78))


### Features

* restrict creation of and zooming to annotations outside image boundaries ([7b2969c](https://gitlab.cc-asp.fraunhofer.de/empaia/frontend/frontend-workspace/commit/7b2969cb6152a822c11f5a9074cde093dac72f5e))
* add slide viewer module config ([9d66340](https://gitlab.cc-asp.fraunhofer.de/empaia/frontend/frontend-workspace/commit/9d66340c8a4ed0f334df9a46c0910f3533c5d7cc))
  * logging is now turned of by default
  * if you need the module console logs turn on with `SlideViewerModule.forRoot({ logging: true })`


* new ZoomSlider Implementation - now uses material design
* ZoomSlider is now mirrored
* background color of slide and overview map is now white
* Overview map: Synchronize host view on dragging


## [0.1.1](https://gitlab.cc-asp.fraunhofer.de/empaia/frontend/frontend-workspace/compare/slide-viewer-0.1.0...slide-viewer-0.1.1) (2021-03-24)


* test release pipeline

# 0.1.0 (2021-03-22)

* test nx semver package ([7433f80](https://gitlab.cc-asp.fraunhofer.de/empaia/frontend/frontend-workspace/commit/7433f80a690294f20babd3e0c3e237deb7498508))


## 21-03-19
patch:
 - removed delay of the zoom to function

## 21-03-11
feature:
  - Added external overview map with fixed viewport, panning and zooming function
  - Implemented communication between viewer component and overview map component

### v 0.1.0

breaking:
- new `TileResolver` interface function: tileLoaderFn
- tileLoaderFn makes it possible to set headers on tile requests. see: https://openlayers.org/en/latest/apidoc/module-ol_Tile.html#~LoadFunction 
- see `helper\wbs-server-tile-connector.ts` for implementation example
- implement either `tileResolverFn` or `tileLoaderFn`. set the not implemented one to `null`
- if both are implemented `tileResolverFn` will be used

feat:
 - zoom controls and toolbar are now located at  the bottom and centered 

other: 
 - bug fixes
